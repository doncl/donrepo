//
//  ViewController.swift
//  AutoGrowTextViewScaffold
//
//  Created by Don Clore on 2/2/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  let textVertPad : CGFloat = 10.0
  var textViewHeight : NSLayoutConstraint!
  let textView : UITextView = UITextView()

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    textView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(textView)

    textView.text = "Add some inital text"
    textView.backgroundColor = .gray
    textView.isEditable = true
    textView.delegate = self
    
    NSLayoutConstraint.activate([
      textView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100.0),
      textView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6, constant: 0),
      textView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
    ])
    

    let size : CGSize
    if let font = textView.font {
      let ns = NSString(string: textView.text)
      size = ns.size(withAttributes: [NSAttributedStringKey.font : font as Any])
    } else {
      size = CGSize(width: view.frame.width * 0.6, height: 100.0)
    }
    
    textViewHeight = textView.heightAnchor.constraint(equalToConstant: size.height +
      textVertPad * 2)

    textViewHeight.isActive = true
  }
}

extension ViewController : UITextViewDelegate {
  func textViewDidChange(_ textView: UITextView) {
    let contentSize = textView.contentSize
    textViewHeight.constant = contentSize.height
    self.textView.setNeedsLayout()
  }
}


