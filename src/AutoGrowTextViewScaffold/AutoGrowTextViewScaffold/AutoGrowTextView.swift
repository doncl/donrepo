//
//  AutoGrowTextView.swift
//  AutoGrowTextViewScaffold
//
//  Created by Don Clore on 2/2/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class AutoGrowTextView: UIControl {

  private var heightConstraint : NSLayoutConstraint!
  private let _textVue : UITextView = UITextView()
  
  
  override var bounds: CGRect {
    get {
      return super.bounds
    }
    set {
      super.bounds = newValue
      _textVue.frame = newValue
      let size = _textVue.contentSize
      //heightConstraint.constant = size.height
      layoutIfNeeded()
    }
  }
  
  var text : String {
    get {
      return _textVue.text
    }
    set {
      _textVue.text = newValue
    }
  }
  
  var attributedText : NSAttributedString {
    get {
      return _textVue.attributedText
    }
    set {
      _textVue.attributedText = newValue
    }
  }
  
  var delegate : UITextViewDelegate? {
    get {
      return _textVue.delegate
    }
    set {
      _textVue.delegate = newValue
    }
  }
  
  init() {
    super.init(frame: CGRect.zero)
    addSubview(_textVue)
    _textVue.backgroundColor = .red
    _textVue.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      _textVue.topAnchor.constraint(equalTo: self.topAnchor),
      _textVue.leadingAnchor.constraint(equalTo: self.leadingAnchor),
      _textVue.trailingAnchor.constraint(equalTo: self.trailingAnchor),
      //self.bottomAnchor.constraint(equalTo: _textVue.bottomAnchor),
    ])
    
//    heightConstraint = _textVue.heightAnchor.constraint(equalToConstant: 100.0)
//    heightConstraint.isActive = true
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
