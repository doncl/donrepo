import UIKit

class RWTCountryListViewController: UITableViewController, RWTCountryResultsControllerDelegate {
    var countryDetailViewController: RWTCountryDetailViewController? = nil
    var countries = RWTCountry.countries()
    var searchController: UISearchController? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        self.clearsSelectionOnViewWillAppear = false
        self.preferredContentSize = CGSize(width: 320.0, height: 600.0)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Countries"

        let controllers = splitViewController!.viewControllers
        countryDetailViewController =
            (controllers[controllers.endIndex-1] as! UINavigationController).topViewController
            as? RWTCountryDetailViewController

        // Set the details controller with the
        // first country in the array
        let country = countries[0] as! RWTCountry
        countryDetailViewController?.country = country
        addSearchBar()
    }

    // #pragma mark - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            var country: RWTCountry? = nil
            if searchController!.active {
                let resultsController = searchController!.searchResultsController as!
                        RWTCountryResultsController

                if let indexPath = resultsController.tableView.indexPathForSelectedRow {
                    country = resultsController.filteredCountries[indexPath.row] as? RWTCountry
                }

            } else {
                if let indexPath = tableView.indexPathForSelectedRow {
                    country = countries[indexPath.row] as? RWTCountry
                }
            }
            let navController = segue.destinationViewController as! UINavigationController
            let topViewController = navController.topViewController as! RWTCountryDetailViewController
            topViewController.country = country
        }
    }

    // #pragma mark - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath)
        -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
                as! RWTCountryTableViewCell

        let country = countries[indexPath.row]as! RWTCountry

        cell.configureCellForCountry(country)

      return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        let country = countries[indexPath.row] as! RWTCountry
        countryDetailViewController?.country = country
    }

    func addSearchBar() {
        let resultsController = RWTCountryResultsController()
        resultsController.countries = countries
        resultsController.delegate = self

        searchController = UISearchController(searchResultsController: resultsController)

        searchController!.searchResultsUpdater = resultsController

        searchController!.searchBar.frame =
                CGRect(x: searchController!.searchBar.frame.origin.x,
                       y: searchController!.searchBar.frame.origin.y,
                       width: searchController!.searchBar.frame.size.width,
                       height: 44.0)

        tableView.tableHeaderView = searchController!.searchBar
        definesPresentationContext = true
    }

    // MARK: RWTCountryResultsControllerDelegate
    func searchCountrySelected() {
        performSegueWithIdentifier("showDetail", sender: nil)
    }

}
