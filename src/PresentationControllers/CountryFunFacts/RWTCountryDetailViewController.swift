import UIKit

class RWTCountryDetailViewController: UIViewController,
        UISplitViewControllerDelegate, UIPopoverPresentationControllerDelegate {
  
    @IBOutlet var flagImageView: UIImageView!
    @IBOutlet var quizQuestionLabel: UILabel!
    @IBOutlet var answer1Button: UIButton!
    @IBOutlet var answer2Button: UIButton!
    @IBOutlet var answer3Button: UIButton!
    @IBOutlet var answer4Button: UIButton!

    var masterPopoverController: UIPopoverController? = nil
    var country: RWTCountry? {
        didSet {
            configureView()

            if masterPopoverController != nil {
                masterPopoverController!.dismissPopoverAnimated(true)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if splitViewController!.displayMode == .PrimaryHidden {
            addCountryListButton()
        }

        configureView()
    }

    func addCountryListButton() {
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            let countriesButton: UIBarButtonItem =
                UIBarButtonItem(title: "Countries",
                style: UIBarButtonItemStyle.Plain,
                target: self.splitViewController,
                action: Selector("toggleMasterVisible:"))

            navigationItem.leftBarButtonItem = countriesButton
        }
    }

    func configureView() {
        if country != nil {
            self.title = country!.countryName;

            if flagImageView != nil {
                let image: UIImage = UIImage(named: country!.imageName)!;
                flagImageView.image = image;
            }

            if quizQuestionLabel != nil {
                quizQuestionLabel.text = country!.quizQuestion;
            }

            if answer1Button != nil {
                answer1Button.setTitle(country!.quizAnswers[0],forState:UIControlState.Normal)
            }

            if answer2Button != nil {
                answer2Button.setTitle(country!.quizAnswers[1], forState:UIControlState.Normal)
            }

            if answer3Button != nil {
                answer3Button.setTitle(country!.quizAnswers[2], forState:UIControlState.Normal)
            }

            if answer4Button != nil {
                answer4Button.setTitle(country!.quizAnswers[3],forState:UIControlState.Normal)
            }
        }

        let detailsButton: UIBarButtonItem = UIBarButtonItem(title: "Facts", style: .Plain,
                target: self, action: #selector(RWTCountryDetailViewController.displayFacts(_:)))

        navigationItem.rightBarButtonItem = detailsButton

    }

    override func updateViewConstraints() {
        super.updateViewConstraints()
        quizQuestionLabel.preferredMaxLayoutWidth = view.bounds.size.width - 40
    }


    @IBAction func quizAnswerButtonPressed(sender: UIButton) {
        let buttonTitle = sender.titleLabel?.text // 1
        var message = ""
        if buttonTitle == country!.correctAnswer {  // 2
            message = "You answered correctly!"
        } else {
            message = "That answer is incorrect, please try again."
        }

        let alert = UIAlertController(title: nil, message: message, preferredStyle: .ActionSheet) // 3

        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: {(alert: UIAlertAction!) in
            print("You tapped OK")
        }))  // 4

        alert.popoverPresentationController?.sourceView = view
        alert.popoverPresentationController?.sourceRect = sender.frame

        presentViewController(alert, animated: true, completion: nil)  // 5
    }

    // #pragma mark - Split view

    func splitViewController(svc: UISplitViewController,
        willChangeToDisplayMode displayMode: UISplitViewControllerDisplayMode) {

        if displayMode == .AllVisible {
            navigationItem.leftBarButtonItem = nil;
        } else {
            let barButtonItem = svc.displayModeButtonItem()
            navigationItem.leftBarButtonItem = barButtonItem
        }
    }

    func splitViewController(splitViewController: UISplitViewController,
        collapseSecondaryViewController secondaryViewController: UIViewController,
        ontoPrimaryViewController primaryViewController: UIViewController) -> Bool  {

      return true
    }

    func displayFacts(sender: UIBarButtonItem) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

        let contentViewController: RWTCountryPopoverViewController =
            storyBoard.instantiateViewControllerWithIdentifier("RWTCountryPopoverViewController")
            as! RWTCountryPopoverViewController

        contentViewController.country = country // 1

        contentViewController.modalPresentationStyle = .Popover // 2

        let detailPopover: UIPopoverPresentationController =
            contentViewController.popoverPresentationController!

        detailPopover.barButtonItem = sender // 3
        detailPopover.permittedArrowDirections = UIPopoverArrowDirection.Any

        detailPopover.delegate = self
        presentViewController(contentViewController, animated: true, completion: nil)  //4
    }

    // MARK: UIAdaptivePresentationControllerDelegate
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController)
                    -> UIModalPresentationStyle {
        return .None
    }

    func presentationController(controller: UIPresentationController,
        viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
        let navController = UINavigationController(rootViewController: controller.presentedViewController)

        return navController
    }
}





