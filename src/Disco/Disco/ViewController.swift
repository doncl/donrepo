//
//  ViewController.swift
//  Disco
//
//  Created by Don Clore on 7/18/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit
import DiscreteSlider

class ViewController: UIViewController {

  @IBOutlet var slider: DiscreteSlider!
  @IBOutlet var sliderValue: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
    
    slider.values = ["Everything", "Val2", "Mentions", "Val3", "Nothing"]
    slider.discreteValue = 0
    sliderValue.text = "0"
    slider.doHaptic = true
  }


  @IBAction func sliderValueChanged(_ sender: DiscreteSlider) {
    sliderValue.text = String(sender.discreteValue)
  }
}

