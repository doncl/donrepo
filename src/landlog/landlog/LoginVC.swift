//
//  ViewController.swift
//  landlog
//
//  Created by Don Clore on 2/28/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import GGLCore
import GGLSignIn
import SnapKit

class LoginVC: UIViewController {
  var keyLogo : UIImageView = UIImageView()
  var userNameField : UITextField = UITextField()
  var password : UITextField = UITextField()
  var errorLabel : UILabel = UILabel()
  var fbSignInButton : FBSDKLoginButton!
  var fbParent : UIView = UIView()
  var googleSignInButton : UIButton = UIButton()
  var loginButton : UIButton = UIButton()
  var registerButton : UIButton = UIButton()
  var forgotPasswordLabel : UILabel = UILabel()
  var forgotPasswordEmailTextField : UITextField?
  var justRegistered : Bool = false

  // VerticalSizeClass == .compact
  struct VerticalCompact {
    static let landscapePadding : CGFloat = 5.0
    static let verticalPadding : CGFloat = 3
    static let oauthButtonWidthNominal : CGFloat = 260.0
    static let keyWidthNominal : CGFloat = 35.0
    static let keyHeightNominal : CGFloat = 66.0
    static let keyFromTopNominal : CGFloat = 25.0
    static let loginFromUserNominal : CGFloat = 19.0
    static let keyLanscapeFromTopPercent : CGFloat = 0.015
    static let buttonHeightLandscapePercent : CGFloat = 0.1
    static let textFieldLandscapeHeightPercent : CGFloat = 0.12
    static let fbFromKeyNominal : CGFloat = 22.0
    static let googleImageInsets = UIEdgeInsets(top: 0.0, left: -34.0, bottom: 0.0, right: 0.0)
    static let googleLabelInsets = UIEdgeInsets(top: 0.0, left: -14.0, bottom: 0.0, right: 0.0)
    static let loginButtonWidth : CGFloat = 127.0
    static let registerFromLoginNominal : CGFloat = 23.0
    static let iPhone6ReferenceHeight : CGFloat = 667.0
  }
  
  struct VerticalRegular {
    static let padding = 10
    static let verticalPadding : CGFloat = 5
    static let buttonWidth : CGFloat = 262.5
    static let forgotPasswordWidth = 150
    static let keyWidth : CGFloat = 53.0
    static let keyHeight : CGFloat = 106.0
    static let keyFromTopPercent : CGFloat = 0.095
    static let buttonHeightPercent : CGFloat = 0.06
    static let textFieldHeightPercent : CGFloat = 0.066
    static let fbFromKey : CGFloat = 28.0
    static let googleImageInsets = UIEdgeInsets(top: 0.0, left: -114.0, bottom: 0.0, right: 0.0)
    static let googleLabelInsets = UIEdgeInsets(top: 0.0, left: -46.0, bottom: 0.0, right: 0.0)
    static let loginButtonWidth : CGFloat = 127.0
    static let loginButtonHeight : CGFloat = 40.0
    static let passwordToLogin : CGFloat = 21.0
    static let loginToForgot : CGFloat = 22.0
    static let forgotToAccount : CGFloat = 8.0
    static let registerButtonWidth : CGFloat = 260.0
    static let registerButtonHeight : CGFloat = 40.0
    static let iPhone6ReferenceHeight : CGFloat = 667.0
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    createView()
  }

  func createView() {
    guard let keyWindow = UIApplication.shared.keyWindow else {
      print("We cannot lay this screen out without knowing the key window size")
      return
    }
    
    fbSignInButton = FBSDKLoginButton(frame: CGRect.zero)
    
    let screenHeight = keyWindow.frame.height
    let screenWidth = keyWindow.frame.width
    view.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
    
    // MAVEN KEY
    keyLogo.image = #imageLiteral(resourceName: "MavenKeyClearBack")
    keyLogo.contentMode = .scaleAspectFit
    view.addSubview(keyLogo)

    // FACEBOOK SIGNIN BUTTON - GOOGLE SIGNIN BUTTON
    fbSignInButton.accessibilityIdentifier = "FacebookSignIn"
    fbSignInButton.clipsToBounds = true
    fbParent.backgroundColor = .clear
    
    view.addSubview(fbParent)
    fbParent.addSubview(fbSignInButton)
    fbSignInButton.readPermissions = ["public_profile", "email", "user_friends"]

    view.addSubview(googleSignInButton)
    googleSignInButton.accessibilityIdentifier = "GoogleSignIn"
    
    googleSignInButton.setImage(#imageLiteral(resourceName: "google").withRenderingMode(.alwaysOriginal), for: .normal)
    googleSignInButton.setTitle("Login with Google", for: .normal)
    googleSignInButton.titleLabel?.font = UIFont.preferredBoldFont(forTextStyle: .footnote)
    googleSignInButton.backgroundColor = Colors.grey238
    googleSignInButton.setTitleColor(Colors.grey74, for: .normal)
    googleSignInButton.addBorder(color: Colors.grey204)
    googleSignInButton.layer.cornerRadius = 4.0
    googleSignInButton.imageEdgeInsets = VerticalCompact.googleImageInsets
    googleSignInButton.titleEdgeInsets = VerticalCompact.googleLabelInsets
    googleSignInButton.titleLabel?.textAlignment = .left
    
    //ERROR LABEL
    view.addSubview(errorLabel)
    errorLabel.numberOfLines = 0
    errorLabel.isHidden = false
    errorLabel.text = "Some error text"
    errorLabel.textColor = .red
    errorLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
    errorLabel.textAlignment = .center
    
    //USERNAME FIELD
    view.addSubview(userNameField)

    userNameField.placeholder = "Username"
    userNameField.accessibilityIdentifier = "Username"
    userNameField.autocorrectionType = .no
    userNameField.autocapitalizationType = .none
    userNameField.backgroundColor = Colors.grey247
    userNameField.autocapitalizationType = UITextAutocapitalizationType.none
    
    //PASSWORD FIELD
    view.addSubview(password)
    password.placeholder = "Password"
    password.accessibilityIdentifier = "Password"
    password.isSecureTextEntry = true
    password.autocorrectionType = .no
    password.autocapitalizationType = .none
    password.backgroundColor = Colors.grey247
    password.autocapitalizationType = .none
    
    //LOGIN BUTTON
    loginButton.accessibilityIdentifier = "Login"
    view.addSubview(loginButton)
    
    loginButton.backgroundColor = Colors.grey74
    loginButton.setTitle("Login", for: UIControlState())
    loginButton.showsTouchWhenHighlighted = true

    
    //FORGOT PASSWORD label
    forgotPasswordLabel.accessibilityIdentifier = "ForgotPassword"
    view.addSubview(forgotPasswordLabel)
    forgotPasswordLabel.backgroundColor = UIColor.clear
    forgotPasswordLabel.text = "Forgot Password?"
    forgotPasswordLabel.textColor = Colors.grey74
    forgotPasswordLabel.font = UIFont.preferredBoldFont(forTextStyle: .footnote)
    forgotPasswordLabel.textAlignment = .left
  
    //CREATE AN ACCOUNT BUTTON
    registerButton.accessibilityIdentifier = "Register"
    view.addSubview(registerButton)
    
    registerButton.backgroundColor = .white
    registerButton.setTitle("Create an Account", for: UIControlState())
    registerButton.showsTouchWhenHighlighted = true
    registerButton.titleLabel?.font = UIFont.preferredBoldFont(forTextStyle: .footnote)
    registerButton.titleLabel?.textAlignment = .center
    registerButton.setTitleColor(Colors.grey74, for: .normal)
    registerButton.addBorder(color: Colors.grey204)
    registerButton.addTarget(self, action: #selector(LoginVC.registerButtonTouched),
     for: .touchUpInside)
    
    if traitCollection.verticalSizeClass == .compact {
      remakeConstraintsForCompactVerticalSizeClass(screenWidth: screenWidth,
        screenHeight: screenHeight)
    } else if traitCollection.verticalSizeClass == .regular {
      remakeConstraintsForRegularVerticalSizeClass(screenWidth: screenWidth,
        screenHeight: screenHeight)
    }
  }
  
  func registerButtonTouched() {
    print("register clicked")
  }
  
  //MARK: Regular Vertical Size Class
  func remakeConstraintsForRegularVerticalSizeClass(screenWidth : CGFloat,
    screenHeight : CGFloat) {
    
    let keyScaleFactor = screenHeight / VerticalRegular.iPhone6ReferenceHeight
    let buttonHeight = screenHeight * VerticalRegular.buttonHeightPercent
    let textFieldHeight = screenHeight * VerticalRegular.textFieldHeightPercent
    
    let keyTop = screenHeight * VerticalRegular.keyFromTopPercent
    
    //MARK: KEY regular vertical size class
    keyLogo.snp.remakeConstraints {(make) -> Void in
      make.centerX.equalTo(view.snp.centerX)
      //N.B. do not constrain key to topLayoutGuide - this will play havoc with the layout
      // when the keyboard causes entire view to translate up, and the user does anything to
      // trigger a layout pass, e.g. touching the password field when they're on username.
      make.top.equalTo(view).offset(keyTop)
      make.width.equalTo(VerticalRegular.keyWidth * keyScaleFactor)
      make.height.equalTo(VerticalRegular.keyHeight * keyScaleFactor)
    }
  
    //MARK: Facebook signin button - regular vertical size class
    fbParent.snp.remakeConstraints { (make) -> Void in
      make.width.equalTo(VerticalRegular.buttonWidth)
      make.height.equalTo(buttonHeight)
      make.top.equalTo(keyLogo.snp.bottom).offset(VerticalRegular.fbFromKey)
      make.centerX.equalTo(view)
    }
    
    fbSignInButton.snp.remakeConstraints { (make) -> Void in
      make.edges.equalTo(fbParent)
    }
    
    //MARK: Google signin button - Regular Vertical Size Class
    googleSignInButton.snp.remakeConstraints { (make) -> Void in
      make.centerX.equalTo(view)
      make.height.equalTo(buttonHeight)
      make.width.equalTo(VerticalRegular.buttonWidth)
      make.top.equalTo(fbSignInButton.snp.bottom).offset(VerticalRegular.padding)
    }
    
    //MARK: Error Label - Regular Vertical Size Class
    errorLabel.snp.remakeConstraints { (make) -> Void in
      make.centerX.equalTo(view)
      make.width.equalTo(VerticalRegular.buttonWidth)
      make.height.equalTo(buttonHeight)
      make.top.equalTo(googleSignInButton.snp.bottom).offset(VerticalRegular.verticalPadding)
    }
    
    //MARK: UsernameField - Regular vertical size class
    userNameField.snp.remakeConstraints { (make) -> Void in
      make.width.equalTo(VerticalRegular.buttonWidth)
      make.centerX.equalTo(view)
      make.top.equalTo(errorLabel.snp.bottom)
      make.height.equalTo(textFieldHeight)
    }
    
    userNameField.setBottomBorder(Colors.grey204, width: VerticalRegular.buttonWidth,
      height: textFieldHeight)
    
    //MARK: Password field - Regular vertical size class
    password.snp.remakeConstraints { (make) -> Void in
      make.width.equalTo(VerticalRegular.buttonWidth)
      make.centerX.equalTo(view)
      make.top.equalTo(userNameField.snp.bottom).offset(5)
      make.height.equalTo(textFieldHeight)
    }
    
    password.setBottomBorder(Colors.grey204, width: VerticalRegular.buttonWidth,
     height: textFieldHeight)
    
    //MARK:  Login button - Regular vertical size class
    loginButton.snp.remakeConstraints { (make) -> Void in
      make.top.equalTo(password.snp.bottom).offset(VerticalRegular.passwordToLogin)
      make.height.equalTo(VerticalRegular.loginButtonHeight)
      make.width.equalTo(VerticalRegular.loginButtonWidth)
      make.centerX.equalTo(view)
    }
    
    //MARK: Forgot Password label - Regular vertical size class
    forgotPasswordLabel.snp.remakeConstraints { (make) -> Void in
      make.centerX.equalTo(view)
      make.top.equalTo(loginButton.snp.bottom).offset(VerticalRegular.loginToForgot)
      make.height.equalTo(textFieldHeight)
      make.width.equalTo(VerticalRegular.forgotPasswordWidth)
    }
    
    //MARK: CREATE AN ACCOUNT BUTTON - Regular vertical size class
    registerButton.snp.makeConstraints { (make) -> Void in
      make.centerX.equalTo(view)
      make.top.equalTo(forgotPasswordLabel.snp.bottom).offset(VerticalRegular.forgotToAccount)
      make.width.equalTo(VerticalRegular.registerButtonWidth)
      make.height.equalTo(VerticalRegular.registerButtonHeight)
    }
  }

  //MARK:  COMPACT Vertical Size Class
  func remakeConstraintsForCompactVerticalSizeClass(screenWidth : CGFloat,
    screenHeight : CGFloat) {

    let midPoint = screenWidth / 2.0

    let scaleFactor = screenWidth / 667.0
    let buttonHeight = screenHeight * VerticalCompact.buttonHeightLandscapePercent
    let textFieldHeight = screenHeight * VerticalCompact.textFieldLandscapeHeightPercent

    let keyTop = scaleFactor * VerticalCompact.keyFromTopNominal
    let oauthButtonWidth = scaleFactor * VerticalCompact.oauthButtonWidthNominal
    let fbFromKey = scaleFactor * VerticalCompact.fbFromKeyNominal

    let keyWidth = scaleFactor * VerticalCompact.keyWidthNominal
    let keyHeight = scaleFactor * VerticalCompact.keyHeightNominal

    let loginFromUser = scaleFactor * VerticalCompact.loginFromUserNominal
    let registerFromLogin = scaleFactor * VerticalCompact.registerFromLoginNominal

    //MARK: KEY
    keyLogo.snp.remakeConstraints {(make) -> Void in
      make.centerX.equalTo(view)
      //N.B. do not constrain key to topLayoutGuide - this will play havoc with the layout
      // when the keyboard causes entire view to translate up, and the user does anything to
      // trigger a layout pass, e.g. touching the password field when they're on username.
      make.top.equalTo(view).offset(keyTop)
      make.width.equalTo(keyWidth)
      make.height.equalTo(keyHeight)
    }

    //MARK: FACEBOOK
    fbParent.snp.remakeConstraints { (make) -> Void in
      let x = midPoint - oauthButtonWidth - VerticalCompact.landscapePadding
      make.leading.equalTo(x)
      make.width.equalTo(oauthButtonWidth)
      make.height.equalTo(buttonHeight)
      make.top.equalTo(keyLogo.snp.bottom).offset(fbFromKey)
    }

    fbSignInButton.snp.remakeConstraints { (make) -> Void in
      make.edges.equalTo(fbParent)
    }

    //MARK: GOOGLE signin
    googleSignInButton.imageEdgeInsets = VerticalCompact.googleImageInsets
    googleSignInButton.titleEdgeInsets = VerticalCompact.googleLabelInsets
    googleSignInButton.titleLabel?.textAlignment = .left

    googleSignInButton.snp.remakeConstraints { (make) -> Void in
      make.leading.equalTo(midPoint + VerticalCompact.landscapePadding)
      make.height.equalTo(buttonHeight)
      make.width.equalTo(fbSignInButton)
      make.bottom.equalTo(fbSignInButton)
    }

    // MARK: ERROR label
    errorLabel.snp.remakeConstraints { (make) -> Void in
      make.centerX.equalTo(view)
      make.height.equalTo(buttonHeight)
      make.top.equalTo(fbSignInButton.snp.bottom)
    }

    //MARK: USERNAME field
    userNameField.snp.remakeConstraints { (make) -> Void in
      make.width.equalTo(fbSignInButton)
      make.leading.equalTo(fbSignInButton)
      make.top.equalTo(errorLabel.snp.bottom).offset(VerticalCompact.verticalPadding)
      make.height.equalTo(textFieldHeight)
    }

    userNameField.setBottomBorder(Colors.grey204, width: oauthButtonWidth,
      height: textFieldHeight)

    //MARK: PASSWORD field
    password.snp.remakeConstraints { (make) -> Void in
      make.leading.equalTo(googleSignInButton)
      make.width.equalTo(userNameField)
      make.top.equalTo(userNameField)
      make.height.equalTo(textFieldHeight)
    }

    password.setBottomBorder(Colors.grey204, width: oauthButtonWidth,
      height: textFieldHeight)

    //MARK: LOGIN field
    loginButton.snp.remakeConstraints { (make) -> Void in
      make.top.equalTo(userNameField.snp.bottom).offset(loginFromUser)
      make.height.equalTo(fbSignInButton)
      make.width.equalTo(VerticalCompact.loginButtonWidth)
      make.trailing.equalTo(userNameField)
    }

    //MARK: FORGOT PASSWORD label
    forgotPasswordLabel.snp.remakeConstraints { (make) -> Void in
      make.leading.equalTo(password)
      make.centerY.equalTo(loginButton)
      make.height.equalTo(textFieldHeight)
    }

    forgotPasswordLabel.setContentHuggingPriority(999.0, for: .horizontal)

    //MARK: CREATE AN ACCOUNT button
    registerButton.snp.remakeConstraints { (make) -> Void in
      make.centerX.equalTo(view)
      make.top.equalTo(loginButton.snp.bottom).offset(registerFromLogin)
      make.leading.equalTo(loginButton)
      make.trailing.equalTo(forgotPasswordLabel)
      make.height.equalTo(loginButton)
    }
  }
  
  func clearConstraints() {
    keyLogo.snp.removeConstraints()
    fbParent.snp.removeConstraints()
    googleSignInButton.snp.removeConstraints()
    errorLabel.snp.removeConstraints()
    userNameField.snp.removeConstraints()
    password.snp.removeConstraints()
    loginButton.snp.removeConstraints()
    forgotPasswordLabel.snp.removeConstraints()
    registerButton.snp.removeConstraints()
  }
  
  override func willTransition(to newCollection: UITraitCollection,
    with coordinator: UIViewControllerTransitionCoordinator) {
    
    guard let keyWindow = UIApplication.shared.keyWindow else {
      print("We cannot lay this screen out without knowing the key window size")
      return
    }
    
    let screenHeight = keyWindow.frame.height
    let screenWidth = keyWindow.frame.width
  
    clearConstraints()
    coordinator.animateAlongsideTransition(in: view,
      animation: { (context : UIViewControllerTransitionCoordinatorContext) in
        
        if newCollection.verticalSizeClass == .compact {
          self.remakeConstraintsForCompactVerticalSizeClass(screenWidth: screenHeight,
            screenHeight: screenWidth)
        } else {
          self.remakeConstraintsForRegularVerticalSizeClass(screenWidth: screenHeight,
            screenHeight: screenWidth)
        }
        
    },  completion: { (context : UIViewControllerTransitionCoordinatorContext) in
    })
  }
}

extension UIFont {
  
  /* Thanks Jeffrey Thomas!
   // http://stackoverflow.com/questions/24107066/nsstring-boundingrectwithsizeoptionsattributescontext-not-usable-in-swift
   */
  func sizeOfString (string: String, constrainedToWidth width: Double) -> CGSize {
    return string.boundingRect(with: CGSize(width: width, height: DBL_MAX),
                               options: NSStringDrawingOptions.usesLineFragmentOrigin,
                               attributes: [NSFontAttributeName: self],
                               context: nil).size
  }
  
  class func preferredBoldFont(forTextStyle style: UIFontTextStyle) -> UIFont {
    return preferredFont(forTextStyle: style, withSymbolicTrait: .traitBold)
  }
  
  class func preferredItalicFont(forTextStyle style: UIFontTextStyle) -> UIFont {
    return preferredFont(forTextStyle: style, withSymbolicTrait: .traitItalic)
  }
  
  fileprivate class func preferredFont(forTextStyle style: UIFontTextStyle,
                                       withSymbolicTrait trait : UIFontDescriptorSymbolicTraits) -> UIFont {
    
    let fontDesc = UIFontDescriptor.preferredFontDescriptor(withTextStyle: style)
    let traitDesc = fontDesc.withSymbolicTraits(trait)
    return UIFont(descriptor: traitDesc!, size: 0)
  }
}

extension UIButton{
  
  func makeRoundWith(text: String, color : UIColor){
    setTitle(text, for: UIControlState())
    setTitleColor(color, for: UIControlState())
    layer.cornerRadius = 5
    showsTouchWhenHighlighted = true
  }
  
  func addBorder(color: UIColor){
    layer.borderColor = color.cgColor
    layer.borderWidth = 1
  }
  
}

extension UITextField {
  
  func setPadding(){
    let padding = UIView.init(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
    self.leftView = padding
    self.leftViewMode = .always
  }
  
  func setRoundedCorners(){
    self.layer.cornerRadius = 5
  }
  
  func setBorderColor(_ color : UIColor){
    self.layer.borderWidth = 1
    self.layer.borderColor = color.cgColor
  }
  
  func setBottomBorder(_ color : UIColor, width: CGFloat, height : CGFloat) {
    let border = CALayer()
    let borderWidth = CGFloat(1.0)
    border.borderColor = color.cgColor
    border.frame = CGRect(x: 0, y: height - borderWidth, width: width, height: height)
    
    border.borderWidth = width
    layer.addSublayer(border)
    layer.masksToBounds = true
  }
}
