#  fuboTVHomework README
## Diary of a mad developer....
So, I'm actually writing this as I'm doing this, which I hope is the next best thing to you guys sitting over my shoulder, watching me flop sweat over the keyboard.

### Beginnings of the app...

* I created an Xcode Single View iOS Project in Swift, Universal App.   I told it to add Unit Testing, but not UI testing.   [I think this link is appropriate here](https://www.hackingwithswift.com/img/merchandise/exasperated-4.jpg)

* I noticed in the instructions that you needed a LaunchScreen icon; to wit: "On startup, shows the fuboTV logo for 3-5 seconds.  (Google "fuboTV logo” and choose one that you think is good).   You get to decide how the logo is shown and for how long."  

* So here's the thing.....in iOS land, usually the goal is to get the user into the main content as quickly as you possibly can, with a minimum of branding and so forth.   Usually the problem is the opposite, things are spending waaay tooo long before launching, and usually because you lacked restraint when using third party dynamic framework dependencies - those things have to be completely fixed up (i.e. all the addresses in them have to be resolved - ouch) - before the app hits main().   The goal is 400 ms. or less, 'cause that's how long the SpringBoard animation (the iOS 'Desktop') takes when  a user touches something.   If you can beat 400 ms, then the app will appear to launch instantly. 

* However, all good rules have exceptions, and you guys are always right :), and all that, so....let's see if we can seamlessly have the 'real' launchscreen segue into a faux launchscreen that looks identically, and
hangs around for 3 seconds.    I'll be busy making my server call in background during that time, so that's cool, although I definitely have to account for the UX for when the server takes LONGER than 3 seconds to respond.

* Next I started Googling for fuboTV images.   I'm sort of finding that the images on the net have alpha channels (transparency) in them, and the Lords At Apple have decreed that the AppIcon (the one that is used for desktop, store images, notifications, blah, blah, must NOT have alpha channel, and they reject the app, you get demerits, hang head in shame, etc.  I have a tool for removing alpha channel, but it (as it sometimes does) rendered the images I had decided on unusable, so I decided not to worry about it, since this thing is never going through App Store submission.   I hope you guys feel the same way, and can take it on faith that I understand the issue, and I've seen this movie before, and I just judged that you don't want me to spend time wrangling with resources for this.

* Next I curl'ed the endpoint:  

    curl http://stac.fubo-qa.net/stac/1/get_site?platform=test_1 | python -m json.tool > testServerResponse.json
    
and I created a 'Resources' group under the 'fuboTVHomeworkTests' group, and added it to it - I'll use it for unit test(s).     

* Next I decided to do the AppIcon.   I did the required 18 images manually for my last homework project (our designers do them for Maven), but I decided to just download a tool for this.   Turns out there are a lot of choices in the Mac App Store, some quite expensive (one app which gets poor reviews wanted 20 bucks), a lot of free ones with no reviews, and....whatever.   I decided to try App Icon Gear, which reportedly (according to reviews) doesn't fix the alpha channel problem, but we don't care for this project, right?   So it sort of had the fewest negative reviews, and I was getting tired of messing with it, so...

* (1:36pm)  I decided to quit messing around with tools that just messed things up, and just do it all manually.   I downloaded an SVG version of the logo, threw it into Sketch, exported it as a 1024 x whatever png with no background, then put it in Pixelmator, and adjusted the canvas size to 1024x1024.   Then I threw it into a folder, and started making copies, resizing into the 18 different sizes using Preview, which is probably a stone age sledgehammer compared to what designers use, but whatever.... I got my app Icons, and Xcode didn't issue any warnings about sizes, done.

*  I made another copy of the 1024 image to use for the LaunchScreen storyboard, slapped some constraints on it, white background, done.
* I made a custom UIView called 'LogoView' to use for the faux LaunchScreen that persists for 4 seconds, and started doing it all in code - you guys didn't specify I had to use IB for this, and sometimes it's just easier to do the imperative code.

* (2:07) - I got the faux launchscreen thingy working, and it fades out nicely into my main VC, without any jarring hitches between the real launchscreen and faux one.  But I noticed my App Icons are knackered - they're showing up black on the emulator, which means....ugh alpha channel has to be removed.   I think I'll leave this for the end, although it is distracting me. 
* (2:10) - lunch and I have a 3:30 PT appointment.

* (5:18) - back from PT, and it came back to me what the issue was with the icons.   I dug out one of our app icons for the Maven iOS app, and sure enough, the background was all opaque white.   So I remade-by-hand all the fuboTV icons, stuck them in the asset catalog, ran the nascent app, and all is good.     That was easy :).

### Data model stuff
* alright, time to write some Codable / File handling code, or rather, to shamelessly steal it from where I've done this before.    I copied JSONCoding.swift, Net.swift, and Storable.swift over from the SwiftLingo project.   This is just for hitting the Net, doing JSON deserialization, and storing it to the filesystem (in the .caches dir).     There's nothing magical or interview-worthy of any of this code, although I'm delighted to explain every line at length, if it helps.

* (6:36 pm) - about have the model written. Need to implement Equatable on it, so we can tell when data is changed, and refrain from reloading if data is no different.   I think my better half may be calling me to do something important, like watch Saturday night TV.   

* (10:33pm) - watching TV with wife, sitting with 13 inch MBP in lap.   Finished unit tests - some adapted from Lingo homework, but some new - main thing is now I can show with tests that I can deserialize the SiteResponseData properly, and I can detect when it has changed, or when it remains the same, so we can avoid unnecessary reloading of the UI. 
* (11:15pm) - discovered that, while my mock was working fine, my regular call to the network was failing.   A bit of headscratching revealed the URI http://stac.fubo-qa.net/stac/1/get_site?platform=test_1 is insecure :).   OK, so I configured a security exception in my plist - this will never ship to App Store, so it's a non-issue.    Anyway, data is flowing through the pipes now, tests are working, caching is working, and I'm gonna call it a night.  Tomorrow I'll build some UI.   

### UI
* so next day, from about 11 to 2:30 - it's a table view, with a custom header to do the page title, I just stuck labels in the cells, and put Core Animation custom borders on the labels, and made the labels smaller than the cells, in order to get the illusion that the cells were padded from each other per the diagram in the assignment.   If I'd used CollectionView, you have minimuminteritem spacing property - maybe I would have done that, if I'd thought about it.
* I added pull-to-refresh for the table, per the ask. 
* what else?   It seems reasonably close to the desired layout on iPad and iPhone, landscape and layout.  
* extensive use of POP (Protocol Oriented Programming) to share label centering, constraints, coloring and other implementation code, since the header, gridcell and carousel cell were almost identical for this homework assignment.  

I thought about doing a lot fancier stuff, but I thought....maybe it's a better message to send that I have discipline, and can follow directions, and I'm not just doing this to amuse myself (although it was a fun little assignment).

As regards feedback, I'd be delighted to do something much more involved, if you needed or need that to understand what you've got with me.   I....don't know if that works for other candidates, though.   Engineers that excel at logic puzzles and whiteboarding might be less inclined to want to do an assignment like this.   It's perfect for me, though.....allows me to work around the stage fright issue, and show that I can get things done.    I have had prospective employers say that they think they have to use the same process for each candidate, to be fair, but I don't think I agree with that proposition, respectfully.   It's my belief that the process should be tailored to each candidate and to the company's needs to actually get to the bottom of what strengths and deficits the candidate possesses.  People are complicated an messy, and I think what you want to know is:

** does the candidate have integrity?  (this is hardest to determine)
** can the candidate do the job?   (this is also hard).

For me, it works for me to do work that's as close as possible to the real thing for me to display.   I'm TOTALLY willing to really do a lot of work on my own time to get around the normal interview process.   But I don't kid myself that that's going to work for all your candidates.

There's an old saw about developers that it's a virtue for engineers to be lazy, and to be arrogant, but....I'm pretty sure I don't agree with the first, and I sure hope I don't embody the second.   But...my truth is not everybody's.  

Thank you, to all of you, for this opportunity, and I look forward to hearing back.

best,
Don Clore


