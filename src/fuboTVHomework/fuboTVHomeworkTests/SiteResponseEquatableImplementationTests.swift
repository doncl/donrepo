//
//  SiteResponseEquatableImplementationTests.swift
//  fuboTVHomeworkTests
//
//  Created by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import XCTest
@testable import fuboTVHomework

class SiteResponseEquatableImplementationTests: XCTestCase {
  func testEqual() {
    getResourceData(success: { sr1 in
      guard let data1 = sr1.data else {
        XCTFail("Unexpected nil data")
        return
      }
      self.getResourceData(success: { sr2 in
        guard let data2 = sr2.data else {
          XCTFail("Unexpected nil data")
          return
        }
        let equal = data1 == data2
        XCTAssertTrue(equal)
      })
    })
  }
  
  func testNotEqual() {
    getResourceData(success: { sr1 in
      guard let data1 = sr1.data else {
        XCTFail("Unexpected nil data")
        return
      }
      self.getResourceData(success: { sr2 in
        guard let data2 = sr2.data else {
          XCTFail("Unexpected nil data")
          return
        }
        var equal = data1 == data2
        XCTAssertTrue(equal)
        
        var mutable = data2
        mutable.pages![0].contents![0].data!.maxItems = 25
        equal = data1 == mutable
        XCTAssertFalse(equal)
        
        mutable = data2
        mutable.pages![0].contents![0].data!.analyticsKeys!.container = "Faux value"
        equal = data1 == mutable
        XCTAssertFalse(equal)
      })
    })
  }

}

extension SiteResponseEquatableImplementationTests {
  private func getResourceData(success: (SiteResponse) -> ()) {
    let resourceName = "TestServerResponse"
    let bundle = Bundle(for: SiteResponseEquatableImplementationTests.self)
    
    guard let url = bundle.url(forResource: resourceName, withExtension: "json") else {
      XCTFail("Could not find \(resourceName).json")
      return
    }
    let contentData : Data
    do {
      contentData = try Data(contentsOf: url)
    } catch let err {
      XCTFail("Failed to load \(resourceName) from bundle, err = \(err)")
      return
    }
    
    SiteResponse.jsonDecode(contentData, success: { siteResponse in
      success(siteResponse)
    }, failure: { err in
      XCTFail(err)
      return
    })
  }
}
