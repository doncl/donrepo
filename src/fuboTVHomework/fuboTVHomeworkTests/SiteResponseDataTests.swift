//
//  SiteResponseDataTests.swift
//  fuboTVHomeworkTests
//
//  Created by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import XCTest
@testable import fuboTVHomework

class SiteResponseDataTests: XCTestCase {
  var siteResponse : SiteResponse?
  
  func testDeserialization() {
    getResourceData(success: { siteResponse in
      self.siteResponse = siteResponse
    })
    guard let siteResponse = siteResponse else {
      XCTFail("Unable to setup test")
      return
    }
    XCTAssertEqual(200, siteResponse.getCode())
    guard let data = siteResponse.data else {
      XCTFail("Unexpected nil response data")
      return
    }
    let pages = data.getPages()
    XCTAssertEqual(1, pages.count)
    let page = pages[0]
    guard let version = page.version else {
      XCTFail("Unexpected nil version")
      return
    }
    XCTAssertEqual(1, version)
    
    guard let type = page.type else {
      XCTFail("Unexpexcted nil type")
      return
    }
    
    XCTAssertEqual("home_1", type)
    
    guard let title = page.title else {
      XCTFail("Unexpected nil title")
      return
    }
    
    guard let englishTitle = title.englishTitle else {
      XCTFail("Unexpected nil englishTitle")
      return
    }
    
    XCTAssertEqual("Home", englishTitle)
    
    guard let contents = page.contents else {
      XCTFail("Unexpected nil page contents")
      return
    }
    // There should be 3 containers
    XCTAssertEqual(3, contents.count)
    let grids = contents.filter({$0.type != nil && $0.type! == .grid})
    XCTAssertEqual(1, grids.count)
    
    validateTheGrid(grid: grids[0])
    
    let carousels = contents.filter({$0.type != nil && $0.type! == .carousel})
    XCTAssertEqual(2, carousels.count)
    
    guard let liveSportsCarousel =
      carousels.first(where: {$0.data?.title?.englishTitle != nil &&
        $0.data!.title!.englishTitle! == "Live Sports"}) else {
          
      XCTFail("Unexpected - live sports Carousel not found")
      return
    }
    
    validateCarousel(carousel: liveSportsCarousel, expectedDataSourceName: "sports_live")

    guard let popularSeriesCarousel =
      carousels.first(where: {$0.data?.title?.englishTitle != nil &&
        $0.data!.title!.englishTitle! == "Popular Series"}) else {
          
        XCTFail("Unexpected - popular series carousel not found")
        return
    }
    validateCarousel(carousel: popularSeriesCarousel, expectedDataSourceName: "popular_shows")
  }
  
  private func validateTheGrid(grid: SiteResponseItemContainer) {
    guard let data = grid.data else {
      XCTFail("Unexpected nil grid data")
      return
    }
    XCTAssertEqual(8, data.getMaxItems())
    guard let analyticsKeys = data.analyticsKeys else {
      XCTFail("Unexpected nil grid analyticsKeys")
      return
    }
    guard let container = analyticsKeys.container else {
      XCTFail("Unexpected nil grid analyticsKeys.container")
      return
    }
    XCTAssertEqual("foo", container)
    
    guard let seeMore = analyticsKeys.seeMore else {
      XCTFail("Unexpected nil grid analyticsKeys.seeMore")
      return
    }
    XCTAssertEqual("bar", seeMore)
    
    guard let dataSource = data.dataSource else {
      XCTFail("Unexpected nil grid dataSource")
      return 
    }
    guard let name = dataSource.name else {
      XCTFail("Unexpected nil grid dataSource.name")
      return
    }
    guard let refresh = dataSource.refresh else {
      XCTFail("Unexpected nil grid dataSource.refresh")
      return
    }
    XCTAssertEqual("popular_shows", name)
    XCTAssertEqual(false, refresh)
    
    guard let title = data.title else {
      XCTFail("Unexpected nil grid title")
      return
    }
    guard let englishTitle = title.englishTitle else {
      XCTFail("Unexpected nil englishTitle on grid")
      return
    }
    XCTAssertEqual("Popular Series", englishTitle)
    
    guard let renderer = data.renderer else {
      XCTFail("Unexpected rendere on grid")
      return
    }
    guard let rendererName = renderer.name else {
      XCTFail("Unexpected nil grid renderer.name")
      return
    }
    XCTAssertEqual("content_item", rendererName)
    
    guard let rendererScale = renderer.scale else {
      XCTFail("Unexpected nil grid renderer.scale")
      return
    }
    XCTAssertEqual(1.0, rendererScale)
  }
  
  private func validateCarousel(carousel: SiteResponseItemContainer,
                                expectedDataSourceName: String) {

    guard let data = carousel.data else {
      XCTFail("Unexpected nil carousel data")
      return
    }
    XCTAssertEqual(8, data.getMaxItems())
    guard let analyticsKeys = data.analyticsKeys else {
      XCTFail("Unexpected nil carousel analyticsKeys")
      return
    }
    guard let container = analyticsKeys.container else {
      XCTFail("Unexpected nil carousel analyticsKeys.container")
      return
    }
    XCTAssertEqual("foo", container)
    
    guard let seeMore = analyticsKeys.seeMore else {
      XCTFail("Unexpected nil carousel analyticsKeys.seeMore")
      return
    }
    XCTAssertEqual("bar", seeMore)
    
    guard let dataSource = data.dataSource else {
      XCTFail("Unexpected nil carousel dataSource")
      return
    }
    guard let name = dataSource.name else {
      XCTFail("Unexpected nil carousel dataSource.name")
      return
    }
    guard let refresh = dataSource.refresh else {
      XCTFail("Unexpected nil carousel dataSource.refresh")
      return
    }
    XCTAssertEqual(expectedDataSourceName, name)
    XCTAssertEqual(false, refresh)
    
    guard let renderer = data.renderer else {
      XCTFail("Unexpected renderer on carousel")
      return
    }
    guard let rendererName = renderer.name else {
      XCTFail("Unexpected nil carousel renderer.name")
      return
    }
    XCTAssertEqual("content_item", rendererName)
    
    guard let rendererScale = renderer.scale else {
      XCTFail("Unexpected nil carousel renderer.scale")
      return
    }
    XCTAssertEqual(1.0, rendererScale)
  }
}

extension SiteResponseDataTests {
  private func getResourceData(success: (SiteResponse) -> ()) {
    let resourceName = "TestServerResponse"
    let bundle = Bundle(for: SiteResponseDataTests.self)
    
    guard let url = bundle.url(forResource: resourceName, withExtension: "json") else {
      XCTFail("Could not find \(resourceName).json")
      return
    }
    let contentData : Data
    do {
      contentData = try Data(contentsOf: url)
    } catch let err {
      XCTFail("Failed to load \(resourceName) from bundle, err = \(err)")
      return
    }
    
    SiteResponse.jsonDecode(contentData, success: { siteResponse in
      success(siteResponse)
    }, failure: { err in
      XCTFail(err)
      return
    })
  }
}
