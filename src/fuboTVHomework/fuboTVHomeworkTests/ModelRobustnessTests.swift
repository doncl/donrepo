//
//  ModelRobustnessTests.swift
//  fuboTVHomeworkTests
//
//  Created by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import XCTest
@testable import fuboTVHomework

/// PRECONDITION: N.B. - for these tests to succeed, the Test scheme must have an argument of
/// "testing" passed to the target.
///
/// The sole and entire purpose of this set of tests is just to demonstrate that the Model can
/// still deliver data when the network is not available.
class ModelRobustnessTests: XCTestCase {
  var receivedNotification : Bool = false
  var resourceData : SiteResponseData!
  
  /// Set up observer for data change notification, and installs Network mock.
  override func setUp() {
    receivedNotification = false
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(ModelRobustnessTests.dataChanged(_:)),
                                           name: Model.modelRefreshedFromNet, object: nil)
    super.setUp()
    
    getResourceData(success: { sr in
      guard let data = sr.data else {
        XCTFail("Unexpected nil siteresponsedata")
        return
      }
      self.resourceData = data
    })
    
    let mock = NetMock()
    mock.forceFail = false
    Net.sharedInstance = NetMock()
  }
  
  
  /// Clears the observer and the received notification flag.
  override func tearDown() {
    NotificationCenter.default.removeObserver(self)
    receivedNotification = false
  }
  
  /// Initial test demonstrates that the mock works, and returns an empty set, and that this
  /// causes a notification to get fired, since the empty set is different than what's in the
  /// bundle.
  func testEmpty() {
    Model.clearCache(failure: { err in
      XCTFail("Failed to clear cache error = \(err)")
      return
    })
    
    // In this case, the cache is cleared, so it will build data from the bundle, and then hit
    // the net to check for fresh data.   Since the mock is in play, and gives it an empty set,
    // it will be empty.
    let model = Model.sharedInstance
    model.secondPhaseInitializer()
    
    XCTAssertTrue(model.responseData == nil)
    
    // But it will have received data change notification.
    XCTAssertTrue(receivedNotification)
  }
  
  /// Sets up some test data from a test bundle, clears cache and proves that the Model returns
  /// the version the NetMock delivered, and the notification is received.
  func testThatChangedValueCausesNotification() {
    Model.clearCache(failure: { err in
      XCTFail("Failed to clear cache error = \(err)")
      return
    })
    
    guard let netMock = Net.sharedInstance as? NetMock else {
      XCTFail("Something very wrong - Net shared instance is not a mock")
      return
    }
    
    var mockData = SiteResponse()
    mockData.code = 200
    mockData.data = resourceData
    netMock.mockData = mockData
    let model = Model.sharedInstance
    model.secondPhaseInitializer()
    guard let modelResponseData = model.responseData else {
      XCTFail("Unexpected nil modelResponse data")
      return
    }
    XCTAssertEqual(modelResponseData, resourceData)
    XCTAssertTrue(receivedNotification, "Failed to receive data change notification")
  }
  
  @objc func dataChanged(_ note: Notification) {
    receivedNotification = true
  }
}

extension ModelRobustnessTests {
  private func getResourceData(success: (SiteResponse) -> ()) {
    let resourceName = "TestServerResponse"
    let bundle = Bundle(for: ModelRobustnessTests.self)
    
    guard let url = bundle.url(forResource: resourceName, withExtension: "json") else {
      XCTFail("Could not find \(resourceName).json")
      return
    }
    let contentData : Data
    do {
      contentData = try Data(contentsOf: url)
    } catch let err {
      XCTFail("Failed to load \(resourceName) from bundle, err = \(err)")
      return
    }
    
    SiteResponse.jsonDecode(contentData, success: { siteResponse in
      success(siteResponse)
    }, failure: { err in
      XCTFail(err)
      return
    })
  }
}
/// Simple mock of my Net class - allows me to simulate an empty return value, or a network
/// failure, or really any desired result.
private class NetMock : InterWebs {
  var mockData : SiteResponse?
  var forceFail : Bool = false
  
  func getNetworkData(uri: String, success: @escaping (Data?) -> (),
                      failure: @escaping (String) -> ()) {
    
    if forceFail {
      failure("Forced failure")
      return
    }

    guard let mockData = mockData else {
      success(nil)
      return
    }
    SiteResponse.jsonEncode(mockData, success: { s  in
      guard let data = s.data(using: .utf8) else {
        XCTFail("Error converting string to data")
        return
      }
      success(data)
    }, failure: { err in
      XCTFail("Error serializing mock site data = \(err)")
        return
    })
  }
}


