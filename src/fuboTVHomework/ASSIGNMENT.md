#  Assignment from fuboTV 
## (Just so I can review it without having to look at email.)

Introduction
============

The goal of this project is to allow an applicant to quickly demonstrate professional ability and work style with the tools and work setting most familiar to the applicant.  We recognize that the standard whiteboard-and-problem-solving format of in-office interviews doesn’t represent normal work conditions and isn’t a good test of general professional ability.

This project should take at most a few hours, and the work output of this assignment has no commercial value to fuboTV.  (We are not trying to get candidates to work for us for free.)

We ask that you provide all artifacts of the work output to us under the Apache License 2.0 to eliminate concerns about code provenance in the records of our recruiting process.   You as the author of the work output continue to own the copyright and have full rights to do with it what you wish.  The license can be found here : https://www.apache.org/licenses/LICENSE-2.0   Please place a copy of the license in the root directory of the work output.  You do not need to add a licensing header to each file.

The Assignment
==============

1) Pretend the problem outlined below is a problem given to you at work.  
2) Solve the problem using the same approaches, coding style, testing, error-handling, comments, file layout, etc, that you would do in a professional setting.
3) Have fun!  This isn’t meant to be stressful - it’s meant to let you demonstrate your professional skills in the environment of your choice.  We just want to know what you’re like to work with on a code / design / architecture level.
3) When complete, provide the complete set of artifacts to us in a tar file.  We expect that we will be able to untar the file, and using the same tools that you used, build and run the solution.   For example, if this is an iOS assignment, we expect to have a working Xcode project that we can load, build and run.  If Java, we’d expect a maven, ant or gradle-based project.  If golang, a project in conventional golang project format that works with standard golang toolchain.

iOS Problem - “Rectangles and Squares”
======================================

One of the things FuboTV applications do is present collections of available shows to users in various navigable containers, such as carousels, grids, lists, etc.  FuboTV has a service called STAC that returns a descriptor for a page/screen as a JSON object.  This descriptor has a list of pages, and for each page, a list of containers, each with metadata about that container (data source, style…).

Your job will be to build an iOS app that will call the service and based on the response, draw a screen based on the response.   The response can contain two types of container (carousel_1 and grid_1) and to keep it simple, we’d like you to draw a rectangle to represent a carousel_1 container, or a square to represent a grid_1 container.

Please build an iOS app from “scratch” - new empty project - that:

1) On startup, shows the fuboTV logo for 3-5 seconds.  (Google "fuboTV logo” and choose one that you think is good).   You get to decide how the logo is shown and for how long.

2) Once past the logo screen, the user will see a screen with a single button that says “Go”.  You get to decide where and how that button works.

3) When the user hits the “Go” button, the app will fetch data from the following URL

http://stac.fubo-qa.net/stac/1/get_site?platform=test_1

and draw the screen based on the configuration for page type ‘home_1’.  The response can have other page types.   It's possible that there is no 'home_1' definition, and the app should do something appropriate.

The page definition is a list of “containers”.  There are two types of container (carousel_1 and grid_1) and we’d like you to draw the carousel_1 as a rectangle (maybe pink? you choose) and grid_1 as a square (use a different color).  AS a bonus, think about where the title might go...  Don’t worry about the rest of details specific to each container.

If the response was something like this (omitting unimportant elements) :

{
"status" : "success",
"data" : {
"pages" : [ {
"type" : "home_1",
"title" : {
"EN" : "Home"
},
"contents" : [ {
"data" : {
"title" : {
"EN" : "Popular Series"
},
},
"type" : "carousel_1"
}, {
"data" : {
"title" : {
"EN" : "Live Sports"
},
},
"type" : "carousel_1"
}, {
"data" : {
"title" : {
"EN" : “Upcoming Movies"
},
},
"type" : "grid_1"
} ]
} ]
}
}

We’d expect the drawn screen to be something like

======================================
|                                    |
|  ===============================   |
|  |       Popular Series        |   |
|  ===============================   |
|                                    |
|  ===============================   |
|  |         Live Sports         |   |
|  ===============================   |
|                                    |
|  ===============================   |
|  |                             |   |
|  |                             |   |
|  |                             |   |
|  |       Upcoming Movies       |   |
|  |                             |   |
|  |                             |   |
|  |                             |   |
|  ===============================   |
|                                    |
======================================

That’s really it.  For additional fun, maybe figure out a nice way to let the user refresh the page, but please don’t spend excessive time on this (unless you want to :)

Thanks, and we look forward to seeing the results.

