//
//  GridCell.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/26/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class GridCell: UITableViewCell, FontSizingAndCenteringTableCell {
  var label : UILabel = UILabel()
  let minimumHeight : CGFloat = 200.0
  
  var bigFontStyle: UIFontTextStyle {
    get {
      return .caption1
    }
  }
  
  var littleFontStyle: UIFontTextStyle {
    get {
      return .caption2
    }
  }
  
  var data : SiteResponseContainerData? {
    didSet {
      setSiteResponseContainerData(data)
    }
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    backgroundColor = .clear
    
    centerAndSizeLabel(self, labelHeight: minimumHeight, labelColor:  #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1) )    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    traitCollectionChangeImpl(previousTraitCollection)
  }
}
