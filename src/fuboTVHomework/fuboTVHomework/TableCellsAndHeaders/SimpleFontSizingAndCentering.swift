//
//  FontSizing.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/26/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

// Just a very simple Adaptive UI thing that gives a crude implementation to users for
// resetting their fonts when traitcollection changes.
protocol SimpleFontSizingAndCentering : class  {
  var label : UILabel {get}
  var bigFontStyle : UIFontTextStyle {get}
  var littleFontStyle : UIFontTextStyle {get}
  var traitCollection : UITraitCollection {get}
  
  func getFontFor(traitCollection: UITraitCollection) -> UIFont
  func traitCollectionChangeImpl(_ previousTraitCollection: UITraitCollection?)
  func centerAndSizeLabel(_ view : UIView, labelHeight: CGFloat, labelColor: UIColor, labelBorder : Bool)
  func setLabelText(_ text: String?)
}

extension SimpleFontSizingAndCentering {
  func getFontFor(traitCollection: UITraitCollection) -> UIFont {
    if traitCollection.horizontalSizeClass == .regular &&
      traitCollection.verticalSizeClass == .regular {
      
      return UIFont.preferredFont(forTextStyle: .headline, compatibleWith: traitCollection)
    } else {
      return UIFont.preferredFont(forTextStyle: .subheadline, compatibleWith: traitCollection)
    }
  }

  func traitCollectionChangeImpl(_ previousTraitCollection: UITraitCollection?) {
    if let previous = previousTraitCollection {
      if previous == traitCollection {
        return
      }
    }
    label.font = getFontFor(traitCollection: traitCollection)
  }
  
  func centerAndSizeLabel(_ view : UIView, labelHeight: CGFloat, labelColor: UIColor, labelBorder : Bool = true) {
    view.addSubview(label)
    label.backgroundColor = labelColor
    label.translatesAutoresizingMaskIntoConstraints = false
    
    if labelBorder {
      label.layer.borderWidth = 1.5
      label.layer.borderColor = #colorLiteral(red: 0.6475411057, green: 0.6475411057, blue: 0.6475411057, alpha: 1).cgColor
    }
  
    let heightConstraint =  view.heightAnchor.constraint(equalToConstant: labelHeight + 30.0)
    heightConstraint.priority = UILayoutPriority.defaultLow
    
    NSLayoutConstraint.activate([
      label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20.0),
      label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20.0),
      label.centerYAnchor.constraint(equalTo: view.centerYAnchor),
      label.heightAnchor.constraint(equalToConstant: labelHeight),
      heightConstraint,
    ])
    label.setContentCompressionResistancePriority(UILayoutPriority.required, for: .horizontal)
    label.setContentCompressionResistancePriority(UILayoutPriority.required, for: .vertical)    
  }
  
  func setLabelText(_ text : String?) {
    label.text = text
    label.textAlignment = .center
    label.sizeToFit()
    label.setNeedsLayout()
    label.setNeedsDisplay()    
  }
}

protocol FontSizingAndCenteringTableCell : SimpleFontSizingAndCentering {
  var data : SiteResponseContainerData? {get  set}
  func setSiteResponseContainerData(_ newData: SiteResponseContainerData?)
}

extension FontSizingAndCenteringTableCell {
  func setSiteResponseContainerData(_ newData: SiteResponseContainerData?) {
    guard let data = data, let title = data.title else {
      label.text = ""
      return
    }
    label.textColor = .black
    label.font = getFontFor(traitCollection: traitCollection)
    // This is where we would check the current locale, or whatever.
    setLabelText(title.getEnglishTitle())
  }
}
