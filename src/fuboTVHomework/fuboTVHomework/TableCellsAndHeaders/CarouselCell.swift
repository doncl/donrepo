//
//  CarouselCell.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/26/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class CarouselCell: UITableViewCell, FontSizingAndCenteringTableCell {
  var bigFontStyle: UIFontTextStyle {
    return UIFontTextStyle.caption1
  }
  
  var littleFontStyle: UIFontTextStyle {
    return UIFontTextStyle.caption2
  }
  
  var label : UILabel = UILabel()
  var data : SiteResponseContainerData? {
    didSet {
      setSiteResponseContainerData(data)
    }
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    backgroundColor = .clear
    centerAndSizeLabel(self, labelHeight: 40.0, labelColor:  #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1) )
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    traitCollectionChangeImpl(previousTraitCollection)
  }

}
