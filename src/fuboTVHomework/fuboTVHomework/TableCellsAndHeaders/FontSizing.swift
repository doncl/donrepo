//
//  FontSizing.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/26/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

// Just a very simple Adaptive UI thing that gives a crude implementation to users for
// resetting their fonts when traitcollection changes.
protocol SimpleFontSizingAndCentering : class {
  var label : UILabel {get}
  var bigFontStyle : UIFontTextStyle {get}
  var littleFontStyle : UIFontTextStyle {get}
  var traitCollection : UITraitCollection {get}
  
  func getFontFor(traitCollection: UITraitCollection) -> UIFont
  func traitCollectionChangeImpl(_ previousTraitCollection: UITraitCollection?)
}

extension SimpleFontSizingAndCentering {
  func getFontFor(traitCollection: UITraitCollection) -> UIFont {
    if traitCollection.horizontalSizeClass == .regular &&
      traitCollection.verticalSizeClass == .regular {
      
      return UIFont.preferredFont(forTextStyle: .headline, compatibleWith: traitCollection)
    } else {
      return UIFont.preferredFont(forTextStyle: .subheadline, compatibleWith: traitCollection)
    }
  }

  func traitCollectionChangeImpl(_ previousTraitCollection: UITraitCollection?) {
    if let previous = previousTraitCollection {
      if previous == traitCollection {
        return
      }
    }
    label.font = getFontFor(traitCollection: traitCollection)
  }
}
