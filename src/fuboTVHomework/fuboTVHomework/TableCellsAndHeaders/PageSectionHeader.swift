//
//  PageSectionHeader.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/26/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class PageSectionHeader: UITableViewHeaderFooterView, SimpleFontSizingAndCentering {
  var bigFontStyle: UIFontTextStyle {
    get {
      return .headline
    }
  }
  var littleFontStyle: UIFontTextStyle {
    get {
      return .subheadline
    }
  }
  
  var label : UILabel = UILabel()
  
  var text : String? {
    set (newValue) {
      setLabelText(newValue)
    }
    get {
      return label.text
    }
  }
  
  override init(reuseIdentifier: String?) {
    super.init(reuseIdentifier: reuseIdentifier)
    label.textColor = .black
    label.textAlignment = .center
    
    centerAndSizeLabel(self, labelHeight: 30.0, labelColor: .clear, labelBorder: false)
    
    layer.borderWidth = 0.5
    layer.borderColor = #colorLiteral(red: 0.6475411057, green: 0.6475411057, blue: 0.6475411057, alpha: 0.749066289).cgColor
  }
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    traitCollectionChangeImpl(previousTraitCollection)
  }
}
