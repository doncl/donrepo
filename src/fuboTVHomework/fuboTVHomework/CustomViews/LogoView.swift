//
//  LogoView.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

// This class exists solely to meet the assignment requirement of:
// 1) On startup, shows the fuboTV logo for 3-5 seconds.
// (Google "fuboTV logo” and choose one that you think is good).
// You get to decide how the logo is shown and for how long.
//
// In other words, it's a faux launchscreen, like Uber used to do, which is perfectly reasonable.
class LogoView: UIView {
  let imageWidthRatio : CGFloat = 0.75  // three quarters of the width.
  let logo : UIImageView = UIImageView(image: UIImage(imageLiteralResourceName: "fuboTVLogo"))
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .white
    
    logo.translatesAutoresizingMaskIntoConstraints = false
    addSubview(logo)
    
    NSLayoutConstraint.activate([
      logo.centerXAnchor.constraint(equalTo: centerXAnchor),
      logo.centerYAnchor.constraint(equalTo: centerYAnchor),
      logo.heightAnchor.constraint(equalTo: logo.widthAnchor, multiplier: 1.0),
      logo.widthAnchor.constraint(equalTo: widthAnchor, multiplier: imageWidthRatio)
    ])
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
