//
//  SiteResponseData.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation


/// This is the actual payload inside the response from the server.  The protocols are as follows:
///
/// Codable - built-in Swift protocol that facilitates JSON serialization/deserialization.
/// JSONCoding - My own POP-based protocol that beefs up Codable to make things behave more the
///              way I want, providing a comment shared implementation.
/// Storable - also POP-based protocol extension that allows the type to save and load from
///             documents and .caches directories on the device.
/// Equatable - so that you can tell when data from the server has changed.  There's a unit
///             test that verifies this works correctly, although not exhaustively.  This is the
///             only one implemented in an extension because the others are POP-based.
///
struct SiteResponseData :  Codable, JSONCoding, Storable {
  
  // MARK: Necessary boilerplate to get the POP-based benefits of Storable and JSONCoding
  typealias CodableType = SiteResponseData
  typealias StorableType = SiteResponseData
  
  // This is actually the only thing in this struct that gets data transferred in and out of,
  // ironically. 
  var pages : [SiteResponseDataPage]?
}

extension SiteResponseData {
  func getPages() -> [SiteResponseDataPage] {
    guard let pages = pages else {
      return []
    }
    return pages
  }
}

extension SiteResponseData : Equatable {
  static func == (lhs: SiteResponseData, rhs: SiteResponseData) -> Bool {
    // OK, the really coolio way to do this is to implement Hashable protocol on all the
    // sub-objects, which is kind of a pain, because you can't just add scalar hashes together
    // and be confident of uniqueness.  I usually do something like...bit shift each successive
    // scalar property hashValue one to the left, which gives a better shot at uniqueness, but
    // there's a dead-simple inefficient way to implement equality, and I'm going to do that
    // here - not efficient, not pretty, but good enough.
    let lPages = lhs.getPages()
    let rPages = rhs.getPages()
    
    for lPage in lPages {
      guard rPages.contains(where: { $0 == lPage}) else {
        return false
      }
    }
    
    for rPage in rPages {
      guard lPages.contains(where: { $0 == rPage}) else {
        return false
      }
    }
    return true
  }
}
