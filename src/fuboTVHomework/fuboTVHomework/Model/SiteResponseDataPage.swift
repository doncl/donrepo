//
//  SiteResponseDataPage.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation


/// A page of data from the response.  The sample I have only has one page, but presumably
/// there could be multiple.   It has a type, title, and version, and an array of
/// 'containers', which can be either carousels or grids.
struct SiteResponseDataPage  : Codable {
  
  /// MARK: properties
  var title : SiteResponseTitleProperty?
  var type : String?
  var version : Int?
  var contents : [SiteResponseItemContainer]?
}

/// MARK: - helpers to avoid unwrapping optionals all the time. 
extension SiteResponseDataPage {
  func getContents() -> [SiteResponseItemContainer] {
    guard let contents = contents else {
      return []
    }
    return contents
  }
}

// MARK: - Equatable implementation
extension SiteResponseDataPage : Equatable {
  static func == (lhs: SiteResponseDataPage, rhs: SiteResponseDataPage) -> Bool {
    guard lhs.type == rhs.type, lhs.version == rhs.version, lhs.title == rhs.title else {
      return false
    }
    let lContents = lhs.getContents()
    let rContents = rhs.getContents()
    if lContents.count != rContents.count {
      return false
    }
    // This is the same lame expediency used to avoid having to implement Hashable
    for lContent in lContents {
      guard rContents.contains(where: {$0 == lContent}) else {
        return false
      }
    }
    
    for rContent in rContents {
      guard lContents.contains(where: {$0 == rContent }) else {
        return false
      }
    }
    return true
  }
}
