//
//  Model.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

// The idea here is to represent the data coming from server, but also to cache it in the app's
// .caches storage.
public class Model {
  let serverUriString = "http://stac.fubo-qa.net/stac/1/get_site?platform=test_1"
  
  static let cachedDataName = "cached.model"
  static let modelRefreshedFromNet = Notification.Name("model_refreshed_from_net_key")
  
  static var sharedInstance : Model = Model()
  
  var responseData : SiteResponseData?
  
  private init() {
    secondPhaseInitializer()
  }
  
  // Two-phase initializer - this weakens encapsulation of the singleton, but makes it easy to
  // unit test; seemed like a worthwhile tradeoff.
  public func secondPhaseInitializer() {
    
    // If we can restore from the cache, just do that first.
    _ = restoreModelFromCache()
    
    // Now check with the network to see if we have a fresh copy.
    buildModelFromInterWebs()
  }
  
  private func restoreModelFromCache() -> Bool {
    guard let data =
      SiteResponseData.retrieve(Model.cachedDataName, from: .caches) else {
        return false
    }
    responseData = data
    return true
  }
    
  private func storeNetworkData(data : SiteResponseData?) {
    if data == nil {
      self.responseData = nil
      NotificationCenter.default.post(name: Model.modelRefreshedFromNet, object: nil)
      return
    }
    if responseData == data {
      return  // no change.
    }
    
    self.responseData = data
    self.responseData!.store(to: .caches, as: Model.cachedDataName, success: {
    }, failure: { err in
      print("Error storing responseData to cache = \(err)")
      // continue on, not fatal.
    })
    // Notify listeners we got fresh stuff. 
    NotificationCenter.default.post(name: Model.modelRefreshedFromNet, object: nil)
  }

  /// Hits the network to build the model.
  func buildModelFromInterWebs(_ completion: (() ->())? = nil) {
    Net.sharedInstance.getNetworkData(uri: serverUriString, success: { data in
      guard let data = data else {
        print("nil data returned from net call")
        self.storeNetworkData(data: nil)
        completion?()
        return
      }
      SiteResponse.jsonDecode(data, success: { siteResponse in
        guard let siteResponseData = siteResponse.data else {
          print("siteResponse has nil data")
          completion?()
          return
        }
        if false == Thread.isMainThread {
          DispatchQueue.main.async {
            self.storeNetworkData(data: siteResponseData)
            completion?()
          }
        } else {
          // Do it synchronously.  This is mainly the unit test case, where Net.sharedInstance is
          // a mock.  We need synchronous behavior to test whether the notification was fired or
          // not.
          self.storeNetworkData(data: siteResponseData)
          completion?()
        }

      }, failure: { errString in
        print("Failed getting model from network - error = \(errString), falling back to cache if possible")
        completion?()
        return
      })
      
    }, failure: { err in
      print("Failed getting model from network - error = \(err), falling back to cached version")
      completion?()
    })
  }
}

public extension Model {
  class func clearCache(failure: (String) ->() ) {
    SiteResponseData.remove(Model.cachedDataName, from: .caches, failure: failure)
    Model.sharedInstance.responseData = nil 
  }
}

