//
//  siteResponse.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation


/// The overall uber POSO (Plain Old Swift Object, or maybe it should be POSS, for Plain Old
/// Swift Struct) that we deserialize the server response into.
/// Uses Swift Codable, and my own JSONCoding for deserialization.
struct SiteResponse : Codable, JSONCoding {
  typealias CodableType = SiteResponse
  
  // N.B. - the code is never used, I could omit it.   We don't even get to
  // deserializing this object from JSON unless HTTPURLResponse returned a 200, FWIW, just
  // trying to explain why it's not being utilized.
  var code : Int?
  var data : SiteResponseData?
}

extension SiteResponse {
  func getCode() -> Int {
    guard let code = code else {
      return 500
    }
    return code
  }
}
