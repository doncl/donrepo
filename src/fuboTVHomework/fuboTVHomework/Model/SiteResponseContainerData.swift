//
//  SiteResponseContainerData.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

/// This is the data for the 'container' as described in the assignment.  I elected to put all the
/// sub-objects in one file, which you couldn't do in some objects like Java, but Swift
/// allows it, and leaves it to the discretion of the engineer as to how to organize this.
/// It was my judgement that I had broken this thing into enough separate files - any more and
/// I'd be scrambling to find stuff.  There's a tradeoff, I believe, between
/// componentization/decomposition and being able to keep it all in your head, and see the
/// forest as a whole, without having to scramble through all the trees.   Just my opinion.
struct SiteResponseContainerData : Codable {
  /// This is the Codable way of explaining to the system that the JSON property is spelled
  /// differently than your property name - allows us keep using Swift conventions in property
  /// naming.   I'm a lot less passionate about naming and naming conventions than many
  /// engineers, I'm just 'give me the rules you care about in your shop, and I'll follow them'.
  private enum CodingKeys : String, CodingKey {
    case analyticsKeys = "analytics_keys"
    case dataSource = "data_source"
    case maxItems = "max_items"
    case renderer = "renderer"
    case title = "title"
  }
  var analyticsKeys : SiteResponseAnalyticsKeys?
  var dataSource : SiteResponseDataSource?
  var maxItems : Int?
  var renderer : SiteResponseRenderer?
  var title : SiteResponseTitleProperty?
  
}

//MARK: - Equatable implementation
extension SiteResponseContainerData : Equatable {
  static func == (lhs: SiteResponseContainerData, rhs: SiteResponseContainerData) -> Bool {
    return lhs.analyticsKeys == rhs.analyticsKeys &&
      lhs.dataSource == rhs.dataSource &&
      lhs.getMaxItems() == rhs.getMaxItems() &&
      lhs.renderer == rhs.renderer &&
      lhs.title == rhs.title
  }
}

extension SiteResponseContainerData {
  func getMaxItems() -> Int {
    guard let maxItems = maxItems else {
      return 0
    }
    return maxItems
  }  
}

// MARK: SiteResponseContainerData sub-object property definitions.   They all are simple enough
// to synthesize their own Equatable implementations.   I don't understand their semantics
// enough to even take a stab at faux comments, sorry. 
struct SiteResponseAnalyticsKeys : Codable, Equatable {
  private enum CodingKeys : String, CodingKey {
    case seeMore = "see_more"
    case container = "container"
  }
  var container : String?
  var seeMore : String?
}

struct SiteResponseDataSource : Codable, Equatable {
  var name : String?
  var refresh : Bool?
}

struct SiteResponseRenderer : Codable, Equatable {
  var name : String?
  var scale : Float?
}
