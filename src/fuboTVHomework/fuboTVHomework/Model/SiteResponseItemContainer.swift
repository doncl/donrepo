//
//  SiteResponseItemContainer.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

enum SiteResponseItemContainerType : String, Codable {
  case carousel = "carousel_1"
  case grid = "grid_1"
}


/// This is the 'container' alluded to in the homework assignment.   It can either be a carousel
/// or a grid.  It just has a type, and then a data object.
struct SiteResponseItemContainer : Codable {
  
  var type : SiteResponseItemContainerType?
  var data : SiteResponseContainerData?
  
}

// MARK: - Equatable implementation
extension SiteResponseItemContainer : Equatable {
  static func == (lhs: SiteResponseItemContainer, rhs: SiteResponseItemContainer) -> Bool {
    guard let lType = lhs.type, let rType = rhs.type else {
      // Not sure what it means if there's no type, but I'm putting a stake in the ground and
      // saying they're not equal.
      return false
    }
    guard lType == rType else {
      return false
    }
    
    guard let lData = lhs.data, let rData = rhs.data else {
      // similar to above.
      return false
    }
    return lData == rData
  }
}
