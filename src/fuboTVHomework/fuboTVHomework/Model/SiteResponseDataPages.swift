//
//  SiteResponseDataPages.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

struct SiteResponseDataPage  : Codable, Equatable {
  static func == (lhs: SiteResponseDataPage, rhs: SiteResponseDataPage) -> Bool {
    guard lhs.type == rhs.type, lhs.version == rhs.version else {
      return false
    }
    return true  // TODO: Implement equatable for SiteResponseTitleProperty
  }
  
  var title : SiteResponseTitleProperty?
  var type : String?
  var version : Int?
  var contents : [SiteResponseItemContainer]?
}

extension SiteResponseDataPage {
  func getContents() -> [SiteResponseItemContainer] {
    guard let contents = contents else {
      return []
    }
    return contents
  }
}


