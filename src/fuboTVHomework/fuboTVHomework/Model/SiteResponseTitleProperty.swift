//
//  SiteResponseTitleProperty.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

struct SiteResponseTitleProperty : Codable, Hashable {
  var hashValue: Int {
    var hash : Int = 0
    if let englishTitle = englishTitle {
      hash += englishTitle.hashValue
    }
    if let frenchTitle = frenchTitle {
      hash += frenchTitle.hashValue
    }
    return hash
  }
  
  private enum CodingKeys : String, CodingKey {
    // So, the idea here is that other languages could be added besides English.
    case englishTitle = "EN"
    case frenchTitle = "FR"
  }
  var englishTitle : String?
  var frenchTitle : String?  // possibly doesn't exist like this, just showing as example.
}

extension SiteResponseTitleProperty  {
  func getEnglishTitle() -> String {
    guard let englishTitle = englishTitle else {
      return ""
    }
    return englishTitle
  }
  
  func getFrenchTitle() -> String {
    guard let frenchTitle = frenchTitle else {
      return ""
    }
    return frenchTitle
  }
}
