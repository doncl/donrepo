//
//  RootVC.swift
//  fuboTVHomework
//
//  Created by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class RootVC: UIViewController {
  let gridCellId : String = "gridCellId"
  let carouselCellId : String = "carouselCellId"
  let headerId : String = "headerId"
  let animationDuration : TimeInterval = 2.0
  let animationDelay : TimeInterval = 3.0
  let fauxLaunchImage : LogoView = LogoView(frame: .zero)
  var tableSetup : Bool = false
  
  let table : UITableView = UITableView()
  private let refresh : UIRefreshControl = UIRefreshControl()
  
  override func viewDidLoad() {
    super.viewDidLoad()
  
    table.translatesAutoresizingMaskIntoConstraints = false
    table.separatorColor = .clear
    fauxLaunchImage.translatesAutoresizingMaskIntoConstraints = false
    view.insertSubview(fauxLaunchImage, at: 0)
    view.insertSubview(table, belowSubview: fauxLaunchImage)
    
    table.contentInset = UIEdgeInsets(top: 30.0, left: 0.0, bottom: 0.0, right: 0.0)

    table.refreshControl = refresh
    refresh.addTarget(self, action: #selector(RootVC.iNeedRefreshment(_:)), for: .valueChanged)
    
    NSLayoutConstraint.activate([
      // TABLE buddy
      table.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      table.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      table.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      table.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
      
      // Faux LaunchScreen buddy.
      fauxLaunchImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      fauxLaunchImage.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      fauxLaunchImage.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      fauxLaunchImage.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
    table.rowHeight = UITableViewAutomaticDimension
    table.estimatedRowHeight = 100.0
    table.sectionHeaderHeight = UITableViewAutomaticDimension
    table.estimatedSectionHeaderHeight = 100.0
    
    table.register(GridCell.self, forCellReuseIdentifier: gridCellId)
    table.register(CarouselCell.self, forCellReuseIdentifier: carouselCellId)
    table.register(PageSectionHeader.self, forHeaderFooterViewReuseIdentifier: headerId)
    table.dataSource = self
    table.delegate = self
    table.reloadData()
    
    NotificationCenter.default.addObserver(self, selector: #selector(RootVC.networkDataRefreshed(_:)),
                                           name: Model.modelRefreshedFromNet, object: nil)
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    let xform = CGAffineTransform(scaleX: 0.001, y: 0.001)
      .concatenating(CGAffineTransform(rotationAngle: .pi * 0.8))
    
    UIView.animate(withDuration: animationDuration, delay: animationDelay,
                   options: [.curveEaseInOut], animations: {
                    
      self.fauxLaunchImage.alpha = 0.0
      self.fauxLaunchImage.transform = xform
    }, completion: { _ in
      self.fauxLaunchImage.removeFromSuperview()
    })
  }
}

// MARK : Notification stuff.
extension RootVC {
  @objc func networkDataRefreshed(_ note: Notification) {
    table.reloadData()
  }
  
  @objc func iNeedRefreshment(_ sender: Any) {
    Model.sharedInstance.buildModelFromInterWebs({ [weak self] in
      self?.refresh.endRefreshing()
    })
  }
}

extension RootVC : UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    guard let responseData = Model.sharedInstance.responseData else {
      return 0
    }
    return responseData.getPages().count
  }
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let page = returnCorrectPage(for: section) else {
      return 0
    }
    return page.getContents().count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let page = returnCorrectPage(for: indexPath.section) else {
      fatalError("logic completely broken here - this should never happen")
    }
    let contents = page.getContents()
    guard contents.count > indexPath.row else {
      fatalError("This should never happen - because of numberOfRowsInSection impl")
    }
    let content = contents[indexPath.row]
    guard let type = content.type, let data = content.data else {
      fatalError("This should never happen - Model won't allow it")
    }
    let cell : UITableViewCell
    if type == .grid {
      let gridCell = table.dequeueReusableCell(withIdentifier: gridCellId, for: indexPath) as! GridCell
      gridCell.data = data
      cell = gridCell
    } else {
      let carouselCell = table.dequeueReusableCell(withIdentifier: carouselCellId, for: indexPath) as! CarouselCell
      carouselCell.data = data
      cell = carouselCell
    }
    return cell
  }
  
  private func returnCorrectPage(for section: Int) -> SiteResponseDataPage? {
    guard let responseData = Model.sharedInstance.responseData else {
      return nil
    }
    let pages = responseData.getPages()
    guard pages.count > section else {
      return nil
    }

    return pages[section]
  }
}

extension RootVC : UITableViewDelegate {
  func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
    view.tintColor = tableView.backgroundColor
  }
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    guard let page = returnCorrectPage(for: section) else {
      fatalError("Cannot happen if number of sections in tableView is implemented correctly")
    }
    let text : String = page.title?.getEnglishTitle() ?? ""
    guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerId)
      as? PageSectionHeader else {
        
      return nil
    }
    
    header.text = text
    return header
  }
}



