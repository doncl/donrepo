//
//  Net.swift
//  fuboTVHomework
//
//  Created by Don Clore on 06/27/2018
//  Shamelessly copied for fuboTV homework by Don Clore on 8/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

/// Protocol to make it easier to mock this class for unit testing.
protocol InterWebs {
  func getNetworkData(uri : String, success: @escaping (Data?) -> (),
                      failure : @escaping (String) -> ())
}

/// Simple network GET tool.
class Net : InterWebs {
  
  // To make it easy to mock, this sharedInstance is visible and mutable.
  static var sharedInstance : InterWebs = Net()
  
  private init() {    
  }
  
  
  /// The one and only utility entrypoint to this struct.  A simple GET mechanism, using
  /// URLSession
  ///
  /// - Parameters:
  ///   - uri: The network resource to GET, in string form.
  ///   - success: Success callback with Data.
  ///   - failure: A description of the error.
  func getNetworkData(uri : String, success: @escaping (Data?) -> (),
    failure : @escaping (String) -> ()) {
    
    let session = URLSession(configuration: .default)
    let headers : [String : String] = [
      "Accept" : "application/json",
      ]
    
    guard let url : URL = URL(string: uri) else {
      return
    }
    let request = NSMutableURLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                      timeoutInterval: 3000)
    
    request.httpMethod = "GET"
    for header in headers {
      request.addValue(header.1, forHTTPHeaderField: header.0)
    }
    
    let task = session.dataTask(with: request as URLRequest) { (data, response, error) -> () in
      
      if let error = error {
        failure(error.localizedDescription)
        return
      }
      if let httpResp = response as? HTTPURLResponse {
        if httpResp.statusCode != 200 {
          failure("Request failed, code = \(httpResp.statusCode)")
          return
        }
      }
      guard let data = data else {
        failure("Request returned nil data")
        return
      }
      success(data)
    }
    task.resume()
  }
}
