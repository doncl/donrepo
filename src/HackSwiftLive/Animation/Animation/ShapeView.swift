//
//  ShapeView.swift
//  Animation
//
//  Created by Paul Hudson on 25/10/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import UIKit

class ShapeView: UIView {
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }

    override func layoutSubviews() {
        guard let layer = layer as? CAShapeLayer else { return }

        layer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 20, height: 20)).cgPath
        layer.strokeColor = UIColor.red.cgColor
        layer.fillColor = nil
        layer.lineWidth = 10
        layer.lineDashPattern = [12, 6]
    }
}
