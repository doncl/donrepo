//
//  ViewController.swift
//  Animation
//
//  Created by Paul Hudson on 25/10/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var visualEffectView: UIVisualEffectView!
    @IBOutlet var progressSegments: UISegmentedControl!
    @IBOutlet var progressSlider: UISlider!
    
    @IBOutlet var imageView1: UIImageView!
    @IBOutlet var imageView2: UIImageView!
    @IBOutlet var shapeView: ShapeView!

    @IBOutlet var settingsLabelStart: UILabel!
    @IBOutlet var settingsLabelEnd: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Animation Playground"
        visualEffectView.effect = nil

        let play = UIBarButtonItem(barButtonSystemItem: .play, target: self, action: #selector(playAnimation))

        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addAnimation))

        let reverse = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reverseAnimation))


        navigationItem.rightBarButtonItems = [play, reverse, add]
    }

    override func viewDidLayoutSubviews() {
    }

    @IBAction func imageTapped(_ sender: Any) {
        print("\(Date()): Image was tapped!")
    }

    @objc func playAnimation() {

    }

    @IBAction func percentageChanged(_ sender: UISegmentedControl) {
    }

    @IBAction func sliderDragged(_ sender: UISlider) {
    }

    @objc func addAnimation() {
    }

    @objc func reverseAnimation() {
    }
}

