//
//  Item.swift
//  ThingSaver
//
//  Created by Paul Hudson on 26/10/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import Foundation

protocol Item {
    init(filename: String)
}
