//
//  NavigationBarStyling.swift
//  ThingSaver
//
//  Created by Paul Hudson on 26/10/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import Foundation
import UIKit

protocol NavigationBarStyling: class {
     func setCustomTitle(str: String)
}

extension NavigationBarStyling where Self: UIViewController {
    func setCustomTitle(str: String) {
        navigationController?.navigationBar.isTranslucent = false
        title = str

        if let font = UIFont(name: "AvenirNext-Heavy", size: 30) {
            let attrs = [NSAttributedStringKey.font: font]
            navigationController?.navigationBar.titleTextAttributes = attrs
        }
    }
}
