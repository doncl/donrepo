//
//  TeamMember.swift
//  ScrumTeam
//
//  Created by Paul Hudson on 24/10/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import Foundation

protocol TeamMember {
    var name: String { get set }
    var favoriteTextEditor: TextEditor { get set }

    func wantsToAttendMeeting() -> Bool
}

extension TeamMember {
    func wantsToAttendMeeting() -> Bool {
        return true
    }
}
