//
//  ProductOwner.swift
//  ScrumTeam
//
//  Created by Paul Hudson on 24/10/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import Foundation

struct ProductOwner: TeamMember {
    var name: String
    var favoriteTextEditor: TextEditor
    var stressLevel = 0
}
