//
//  Commit.swift
//  FastSwift
//
//  Created by Paul Hudson on 20/10/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import Foundation

class Entry {
    var url: String
    var htmlURL: String
    var name: String
    var email: String
    var date: Date
    var positionInHistory = ""

    init(url: String, htmlURL: String, name: String, email: String, date: Date) {
        self.url = url
        self.htmlURL = htmlURL
        self.name = name
        self.email = email
        self.date = date
    }
}
