//
//  ViewController.swift
//  FastSwift
//
//  Created by Paul Hudson on 20/10/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    var entries = [Entry]()
    var commitsPerAuthor = [String: Int]()
    var uniqueAuthors = [String]()
    var appleCommits = 0
    var commitTimes = [Int: Int]()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadJSON()
    }

    func loadJSON() {
        guard let url = Bundle.main.url(forResource: "commits", withExtension: "json") else {
            fatalError("Unable to find JSON in project.")
        }

        guard let contents = try? String(contentsOf: url) else {
            fatalError("Unable to load JSON from file.")
        }

        let json = JSON(parseJSON: contents)

        for value in json.arrayValue {
            let url = value["url"].stringValue
            let htmlURL = value["html_url"].stringValue
            let name = value["commit"]["author"]["name"].stringValue
            let email = value["commit"]["author"]["email"].stringValue
            let date = value["commit"]["author"]["date"].stringValue

            let formatter = ISO8601DateFormatter()
            let parsedDate = formatter.date(from: date) ?? Date()

            let entry = Entry(url: url, htmlURL: htmlURL, name: name, email: email, date: parsedDate)
            entries.append(entry)
        }

        // calculate commits per author
        for entry in entries {
            if commitsPerAuthor[entry.name] == nil {
                commitsPerAuthor[entry.name] = 1
            } else {
                commitsPerAuthor[entry.name] = commitsPerAuthor[entry.name]! + 1
            }
        }

        // calculate unique authors
        for entry in entries {
            if !uniqueAuthors.contains(entry.name) {
                uniqueAuthors.append(entry.name)
            }
        }

        // calculate Apple commits
        for entry in entries {
            if entry.email.hasSuffix("apple.com") {
                appleCommits += 1
            }
        }
        
        // calculate popular commit times
        for entry in entries {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH"
            let hourString = formatter.string(from: entry.date)

            if let hourInt = Int(hourString) {
                if commitTimes[hourInt] == nil {
                    commitTimes[hourInt] = 1
                } else {
                    commitTimes[hourInt] = commitTimes[hourInt]! + 1
                }
            }
        }

        // calculate where each commit was in its day
        for entry in entries {
            var before = 0
            var after = 0

            for innerEntry in entries {
                let hourFormatter = DateFormatter()
                hourFormatter.dateFormat = "HH"

                let thisHour = Int(hourFormatter.string(from: entry.date)) ?? 0
                let innerHour = Int(hourFormatter.string(from: innerEntry.date)) ?? 0

                if thisHour == innerHour {
                    if entry.date < innerEntry.date {
                        after += 1
                    } else {
                        before += 1
                    }
                }
            }

            entry.positionInHistory = "\(before + 1)/\(before + after + 1)"
        }
    }
}

