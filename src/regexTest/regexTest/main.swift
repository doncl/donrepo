//
//  main.swift
//  regexTest
//
//  Created by Don Clore on 2/12/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

var s =
"""
  \\n > A good definition must also be one that Big Chocolate cannot co-opt, as they have done with bean-to-bar.\\n\\n
"""


print(s)

let ns = NSString(string: s)
var range = NSMakeRange(0, ns.length)

let regex = try! NSRegularExpression(pattern: "^\\\\n\\s++\\>\\s+[^\n]*\\\\n++$", options: [])


let results = regex.matches(in: s, options: [], range: range)
print(results.count)

print("foo")


}
