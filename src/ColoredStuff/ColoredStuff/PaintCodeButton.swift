//
//  PaintCodeButton.swift
//  ColoredStuff
//
//  Created by Don Clore on 11/16/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit


enum PaintCodeButtonDrawType: Int {
    case diversity = 0
    case discrepancy = 1
    case variation = 2
    case inconsistency = 3
}

@IBDesignable
class PaintCodeButton: UIButton {

    var paintCodeButtonDrawType: PaintCodeButtonDrawType = .diversity

    @IBInspectable var drawType: Int {
        get {
            return paintCodeButtonDrawType.rawValue
        }
        set (newValue) {
            paintCodeButtonDrawType = PaintCodeButtonDrawType(rawValue: newValue) ?? .diversity
        }
    }

    override var intrinsicContentSize: CGSize {
        return CGSize(width: 150, height: 150)
    }


    override func draw(_ rect: CGRect) {
        super.draw(rect)
        switch paintCodeButtonDrawType {

        case .diversity:
            ColoredStuff.drawDiversity(frame: rect, resizing: ColoredStuff.ResizingBehavior.aspectFit)
        case .discrepancy:
            ColoredStuff.drawDiscrepancy(frame: rect, resizing: ColoredStuff.ResizingBehavior.aspectFit)
        case .variation:
            ColoredStuff.drawVariation(frame: rect, resizing: ColoredStuff.ResizingBehavior.aspectFit)
        case .inconsistency:
            ColoredStuff.drawInconsistency(frame:rect, resizing: ColoredStuff.ResizingBehavior.aspectFit)
        }
    }

}
