//
//  ColoredThingViewController.swift
//  ColoredStuff
//
//  Created by Don Clore on 11/16/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

@IBDesignable
class ColoredThingViewController: UIViewController {
    var paintCodeButtonDrawType: PaintCodeButtonDrawType = .diversity

    @IBInspectable var drawType: Int {
        get {
            return paintCodeButtonDrawType.rawValue
        }
        set (newValue) {
            paintCodeButtonDrawType = PaintCodeButtonDrawType(rawValue: newValue) ?? .diversity
        }
    }

    override func loadView() {
        let coloredThingView = ColoredThingView()
        coloredThingView.drawType = drawType
        view = coloredThingView
    }
}
