//
//  ViewController.swift
//  CVSpacing
//
//  Created by Don Clore on 10/7/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var cv : UICollectionView
    var layout : UICollectionViewFlowLayout
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        layout = UICollectionViewFlowLayout()
        cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        layout = UICollectionViewFlowLayout()
        cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        let nib = UINib(nibName: "TicketCell", bundle: nil)
        cv.register(nib, forCellWithReuseIdentifier: "TicketCell")
        layout.scrollDirection = UICollectionView.ScrollDirection.vertical
        layout.minimumInteritemSpacing = 5.0
        layout.minimumLineSpacing = 15.0
        layout.sectionHeadersPinToVisibleBounds = true
        layout.itemSize = CGSize(width: 170, height: 326)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(cv)
        cv.backgroundColor = .lightGray
        cv.frame = view.bounds
        cv.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,
                               UIView.AutoresizingMask.flexibleHeight]

        cv.delegate = self
        cv.dataSource = self

        cv.reloadData()
    }
}

extension ViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 32
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TicketCell", for: indexPath) as? TicketCell else {
            fatalError("foo")
        }
        cell.bottomHeightConstraint.constant = 101.27
        cell.middleHeightConstraint.constant = 33.27

        cell.imageView.image = UIImage(named: "ArmyOfDarkness")
        return cell
    }
    
    
}

extension ViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let distanceToTop: CGFloat = 15
        let distanceToBottom: CGFloat = distanceToTop
        
        let edgeDistance: CGFloat = 10
        return UIEdgeInsets(top: distanceToTop, left: edgeDistance, bottom: distanceToBottom, right: edgeDistance)
    }
}

