//
//  UniversalCell.swift
//  fuboTV
//
//  Created by Venkatesh Jujjavarapu on 9/27/17.
//  Copyright © 2017 Fubo. All rights reserved.
//

import UIKit


class UniversalCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var networklogoImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var liveView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        prepareForReuse()

        titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        titleLabel.textColor = .regularDark

        subTitleLabel.font = UIFont.systemFont(ofSize: 14)
        subTitleLabel.textColor = UIColor.textLightGray

        statusLabel.font = UIFont.boldSystemFont(ofSize: 18)

        liveView?.backgroundColor = .clear
        liveView?.isHidden = true
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        titleLabel?.text = nil
        subTitleLabel?.text = nil
        networklogoImageView?.image = nil
        statusLabel?.text = nil
        timeLabel?.text = nil
        imageView?.image = nil

        imageView?.isHidden = false
        titleLabel?.isHidden = false
        subTitleLabel?.isHidden = false
        networklogoImageView?.isHidden = false
        statusLabel?.isHidden = false
        timeLabel?.isHidden = false
        durationLabel?.isHidden = false
        liveView?.isHidden = false
    }

//    func setNetworkLogo(urlStr: String) {
//        loadImage(with: urlStr, into: networklogoImageView, useImagePlaceHolder: false, useGradient: false)
//    }
//
//    func setNetworkLogo(urlStr: String, scale: CGFloat) {
//        loadImage(with: urlStr, into: networklogoImageView, scaledTo: scale, useImagePlaceHolder: false, useGradient: false)
//    }
//
//    func setPosterImage(urlStr: String) {
//        loadImage(with: urlStr, into: imageView)
//    }
}

extension UniversalCell {
//    func loadImage(with str: String, into imView: UIImageView, scaledTo scale: CGFloat = 1, useImagePlaceHolder: Bool = true, useGradient: Bool = true) {
//        DispatchQueue.global(qos: .background).async {
//            guard let url = URL(string: str) else { return }
//
//            DispatchQueue.main.async {
//                imView.getImage(request: ImageRequest(url: url), useImagePlaceHolder: useImagePlaceHolder, useGradient: useGradient)
//            }
//        }
//    }
}
