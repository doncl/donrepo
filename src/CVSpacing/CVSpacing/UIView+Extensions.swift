//
//  UIView+Extensions.swift
//  fuboTV
//
//  Created by Jibril Gueye on 9/22/17.
//  Copyright © 2017 fuboTV Inc. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addCornerRadiusAnimation(from: CGFloat, to: CGFloat, duration: CFTimeInterval) {
        let animation = CABasicAnimation(keyPath: "cornerRadius")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.fromValue = from
        animation.toValue = to
        animation.duration = duration

        layer.add(animation, forKey: "cornerRadius")
        layer.cornerRadius = to
    }

    var interfaceIdiom: UIUserInterfaceIdiom {
        return UIDevice.current.userInterfaceIdiom
    }

//    func placeFullscreenView(_ fullscreenView: UIView) {
//        addSubview(fullscreenView)
//        constrain(toAllEdges: fullscreenView)
//    }

    func screenSize() -> CGSize {
        return UIApplication.shared.keyWindow?.bounds.size ?? UIScreen.main.bounds.size
    }

    func addDropShadow() {
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8).cgColor
        layer.shadowOpacity = 1
        layer.shadowRadius = 4
    }
}

extension UIView {

    /// performs animation with specific timing function.
    static func animate(withDuration duration: TimeInterval, timingFunction: CAMediaTimingFunction, animations: @escaping () -> Void, completion: ((Bool) -> Void)? = nil) {
        CATransaction.begin()
        CATransaction.setAnimationTimingFunction(timingFunction)
        UIView.animate(withDuration: duration, animations: animations, completion: completion)
        CATransaction.commit()
    }
}

extension CGAffineTransform {
    init(scale: CGFloat) {
        self.init(scaleX: scale, y: scale)
    }
}
