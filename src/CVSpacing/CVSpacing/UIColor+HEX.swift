//
//  UIColor.swift
//  fuboTV
//
//  Created by Jibril Gueye on 11/17/17.
//  Copyright © 2017 Fubo. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var color: UIColor {
        let scanner = Scanner(string: self)
        scanner.scanLocation = 0

        var rgbValue: UInt64 = 0

        scanner.scanHexInt64(&rgbValue)

        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff

        return UIColor(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}

extension UIColor {

    var toHexString: String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0

        getRed(&r, green: &g, blue: &b, alpha: &a)

        return String(
            format: "%02X%02X%02X",
            Int(r * 0xff),
            Int(g * 0xff),
            Int(b * 0xff)
        )
    }

    static var random: UIColor {
        return UIColor(
            red: CGFloat(arc4random()) / CGFloat(UInt32.max),
            green: CGFloat(arc4random()) / CGFloat(UInt32.max),
            blue: CGFloat(arc4random()) / CGFloat(UInt32.max),
            alpha: 1
        )
    }

    // MARK: - V3 Colors
    // MARK: Main Colors
    static var mainWhite = "FFFFFF".color
    static var fuboOrange = "FA4616".color
    static var fuboBlue = "1C9FD0".color
    static var clouds = "ecf0f1".color
    static var fuboDark = "242424".color
    static var warningRed = "EE2828".color

    // MARK: Darks
    static var mobileBlack = "1C1C1C".color
    static var regularDark = "2B2D2E".color
    static var mediumDark = "3A3E3F".color
    static var lightDark = "535657".color
    static var midDarkGray = "7B7B7B".color
    static var ticketSubheadingGray = "9B9B9B".color

    // MARK: Text Colors
    static var textExtraLightGray = "C4C0B8".color
    static var textLightGray = "79746C".color

    // MARK: - V2 Colors
    static var textBlueGreen = "00ACC1".color
    static var textGrey = "999999".color
    static var evilGrey = "666666".color
    static var fuboLightGrey = "B3B3B3".color
    static var darkBorder = "23262E".color
    static var fuboOrangeV2 = "B3B3B3".color
    static var darkGreyBlue = "2F333D".color
    static var fuboRed = "f44336".color
    static var buttonBorder = "1D2026".color
//    static var interstitial = "343738".color
    static var skeletonLight = "EBE9E6".color

    final private func isLight() -> Bool {
        // algorithm from: http://www.w3.org/WAI/ER/WD-AERT/#color-contrast
        guard let components = cgColor.components,
            components.count >= 3 else { return false }
        let brightness = ((components[0] * 299) + (components[1] * 587) + (components[2] * 114)) / 1000
        return !(brightness < 0.5)
    }

    final var complementaryColor: UIColor {
        return isLight() ? darker : lighter
    }

    final private var lighter: UIColor {
        return adjust(by: 1.35)
    }

    final private var darker: UIColor {
        return adjust(by: 0.84)
    }

    final private func adjust(by percent: CGFloat) -> UIColor {
        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return UIColor(hue: h, saturation: s, brightness: b * percent, alpha: a)
    }

    func makeGradient() -> [UIColor] {
        return [self, complementaryColor, self]
    }
}
