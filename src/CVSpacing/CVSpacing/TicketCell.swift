//
//  Ticket.swift
//  fuboTV
//
//  Created by Venkatesh Jujjavarapu on 9/14/17.
//  Copyright © 2017 fuboTV Inc. All rights reserved.
//

import UIKit

class TicketCell: UniversalCell {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var midView: UIView?
    @IBOutlet weak var bottomView: UIView!

    @IBOutlet weak var utilityView: UIView!

    @IBOutlet weak var topWidth: NSLayoutConstraint!
    @IBOutlet weak var midWidth: NSLayoutConstraint!
    @IBOutlet weak var bottomWidth: NSLayoutConstraint!

    @IBOutlet weak var bottomViewFeatured: UIStackView!
    @IBOutlet weak var middleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomHeightConstraint: NSLayoutConstraint!

    lazy var gradient: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.colors = [
            UIColor(red: 0.14, green: 0.14, blue: 0.14, alpha: 0).cgColor,
            UIColor(red: 0.14, green: 0.15, blue: 0.15, alpha: 0.5).cgColor
        ]
        gradient.locations = [0, 1]
        gradient.startPoint = CGPoint(x: 1, y: 0)
        gradient.endPoint = CGPoint(x: 0, y: 1)
        return gradient
    }()

    override func awakeFromNib() {
        super.awakeFromNib()

        midView?.backgroundColor = UIColor.regularDark
        backgroundColor = .clear
        topView?.backgroundColor = .darkGray
        contentView.backgroundColor = .clear
        bottomView.addDropShadow()
    }

//    func disableSkeletonView() {
//        imageView.disableSkeleton()
//    }

    func enableSkeletonView() {
        var tmpRect = bounds
        tmpRect.size.width = bounds.width
        imageView.frame = tmpRect
        gradient.frame = tmpRect

//        imageView.enableSkeleton()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        utilityView.subviews.forEach({ $0.removeFromSuperview() })
    }
}

extension TicketCell {
    func adjustTo(specs: TicketCellSpecs) {
        adjustConstraints(bottom: specs.bottomHeight, middle: specs.middleHeight)
    }

    func adjustConstraints(bottom: CGFloat, middle: CGFloat) {
        if let middleConstraint = middleHeightConstraint, let bottomConstraint = bottomHeightConstraint {
            middleConstraint.constant = middle
            bottomConstraint.constant = bottom
            layoutIfNeeded()
        }
    }
}

struct TicketCellSpecs {
    var size: CGSize { return CGSize(width: width, height: topHeight + middleHeight + bottomHeight) }
    let topHeight: CGFloat
    let middleHeight: CGFloat
    let bottomHeight: CGFloat
    let width: CGFloat
}

extension TicketCellSpecs {
    static let moviesHomeSpecs: TicketCellSpecs = {
        return TicketCellSpecs(topHeight: 191, middleHeight: 35, bottomHeight: 112, width: 220)
    }()

    static let seriesHomeSpecs: TicketCellSpecs = {
        let width: CGFloat = 284
        let topHeight = ceil(width * 9 / 16)
        return TicketCellSpecs(topHeight: topHeight, middleHeight: 35, bottomHeight: 130, width: width)
    }()

    static let sportsHomeSpecs: TicketCellSpecs = {
        let width: CGFloat = 284
        let topHeight = ceil(width * 9 / 16)
        return TicketCellSpecs(topHeight: topHeight, middleHeight: 35, bottomHeight: 112, width: width)
    }()
        //= TicketCellSpecs(suggestedItemSize: nil, middleHeight: 35, bottomHeight: 112)
}
