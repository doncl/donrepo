//
//  ViewController.swift
//  DragDropSection
//
//  Created by Don Clore on 1/17/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  private var items: [String] = [
    "image0",
    "image1",
    "image2",
    "image4",
    "image5",
    "image6",
    "image7",
    "image8",
    "image9",
  ]

  override func viewDidLoad() {
    super.viewDidLoad()

    let layout = UICollectionViewFlowLayout()
    layout.minimumInteritemSpacing = .zero
    
    let collection = UICollectionView(frame: view.bounds, collectionViewLayout: layout)
    
    collection.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(collection)
    
    NSLayoutConstraint.activate([
      collection.topAnchor.constraint(equalTo: view.topAnchor),
      collection.leftAnchor.constraint(equalTo: view.leftAnchor),
      collection.rightAnchor.constraint(equalTo: view.rightAnchor),
      collection.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    ])
    
   
    collection.backgroundColor = .white
    collection.delegate = self
    collection.dataSource = self
    collection.dragDelegate = self
    collection.dropDelegate = self
    collection.dragInteractionEnabled = true
    collection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "CELL")
    collection.contentInset = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
  }
  
  func reorderItems(coordinator: UICollectionViewDropCoordinator, destinationIndexPath: IndexPath, collectionView: UICollectionView) {
    guard let item = coordinator.items.first,
      let sourceIndexPath = item.sourceIndexPath,
      let stringDragged = item.dragItem.localObject as? String else {
      return
    }

    self.items.remove(at: sourceIndexPath.item)
    self.items.insert(stringDragged, at: destinationIndexPath.item)

    collectionView.performBatchUpdates({
      collectionView.deleteItems(at: [sourceIndexPath])
      collectionView.insertItems(at: [destinationIndexPath])
    }, completion: nil)

    coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
  }
}

extension ViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return items.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath)
    let cellImage = UIImageView(image: UIImage(named: items[indexPath.item]))
    cellImage.translatesAutoresizingMaskIntoConstraints = false
    cellImage.contentMode = UIView.ContentMode.scaleAspectFill
    cell.contentView.clipsToBounds = true
    cell.contentView.addSubview(cellImage)
    NSLayoutConstraint.activate([
      cellImage.topAnchor.constraint(equalTo: cell.contentView.topAnchor, constant: 4),
      cellImage.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor, constant: 4),
      cellImage.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor, constant: -4),
      cellImage.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor, constant: -4),
    ])

    cell.backgroundColor = .white
    return cell
  }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let square = (collectionView.frame.width - 8) / 3
    return CGSize(width: square, height: square)
  }
}

extension ViewController: UICollectionViewDragDelegate {
  func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
    let item = self.items[indexPath.item]
    let itemProvider = NSItemProvider(object: item as NSString)
    let dragItem = UIDragItem(itemProvider: itemProvider)
    dragItem.localObject = item
    return [dragItem]
  }
}

extension ViewController: UICollectionViewDropDelegate {
  func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
    if collectionView.hasActiveDrag {
      return UICollectionViewDropProposal(operation: UIDropOperation.move,
                                          intent: UICollectionViewDropProposal.Intent.insertAtDestinationIndexPath)
    } else {
      return UICollectionViewDropProposal(operation: .forbidden)
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
    if coordinator.proposal.operation == .move {
      let destIndexPath: IndexPath
      if let dest = coordinator.destinationIndexPath {
        destIndexPath = dest
      } else {
        let section = collectionView.numberOfSections - 1
        let row = collectionView.numberOfItems(inSection: section) - 1
        guard row >= 0, section >= 0 else {
          return
        }
        destIndexPath = IndexPath(item: row, section: section)
      }
      reorderItems(coordinator: coordinator, destinationIndexPath: destIndexPath, collectionView: collectionView)
    }
  }
}

