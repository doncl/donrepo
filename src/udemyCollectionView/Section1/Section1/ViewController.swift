//
//  ViewController.swift
//  Section1
//
//  Created by Don Clore on 1/17/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
 
    let layout = UICollectionViewFlowLayout()
    let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
    
    collection.translatesAutoresizingMaskIntoConstraints = false
    collection.backgroundColor = UIColor.red
    view.addSubview(collection)
    
    NSLayoutConstraint.activate([
      collection.topAnchor.constraint(equalTo: view.topAnchor),
      collection.leftAnchor.constraint(equalTo: view.leftAnchor),
      collection.rightAnchor.constraint(equalTo: view.rightAnchor),
      collection.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    ])
    
    collection.dataSource = self
    collection.delegate = self
    collection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "CELL")
  }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: 100, height: 100)
  }
}

extension ViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 4
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath)
    cell.backgroundColor = UIColor.blue
    let label = UILabel()
    label.text = "what's goodie homies!?"
    label.translatesAutoresizingMaskIntoConstraints = false
    cell.contentView.addSubview(label)
    
    NSLayoutConstraint.activate([
      label.topAnchor.constraint(equalTo: cell.contentView.topAnchor),
      label.leftAnchor.constraint(equalTo: cell.contentView.leftAnchor),
      label.rightAnchor.constraint(equalTo: cell.contentView.rightAnchor),
      label.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor),
    ])

    
    return cell
  }
}

