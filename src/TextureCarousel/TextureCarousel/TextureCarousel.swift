//
//  TextureCarousel.swift
//  TextureCarousel
//
//  Created by Don Clore on 7/31/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit
import AsyncDisplayKit

@objc enum CarouselType : Int {
  case linear = 0
  case rotary = 1
  case invertedrotary = 2
  case cylinder = 3
  case invertedcycling = 4
  case wheel = 5
  case invertedwheel = 6
  case coverflow = 7
  case coverflow2 = 8
  case timemachine = 9
  case invertedtimemachine = 10
  case custom = 11
}

//@objc enum CarouselOption : Int {
//  case wrap = 0
//  case showbackfaces = 1
//  case offsetmultiplier = 2
//  case visibleitems = 3
//  case count = 4
//  case arc = 5
//  case angle = 6
//  case radius = 7
//  case tilt = 8
//  case spacing = 9
//  case fademin = 10
//  case fademax = 11
//  case faderange = 12
//  case fademinalpha = 13
//}

@objc protocol CarouselDelegate {
  func currentItemIndexDidChange(carousel: TextureCarousel)
  func carousel(carousel: TextureCarousel, didSelectItemAtIndex index : Int)
}

@objc protocol CarouselDataSource {
  func numberOfItems(inCarousel carousel : TextureCarousel) -> Int
  func carousel(carousel: TextureCarousel, nodeBlockForItemAtIndex index : IndexPath)
    -> () -> ASDisplayNode
}

@objc class TextureCarousel: ASDisplayNode {
  var dataSource : CarouselDataSource?
  var delegate : CarouselDelegate?
  
  var type : CarouselType = .rotary
  var perspective : CGFloat = -1.0 / 500.0
  var decelerationRate : CGFloat = 0.95
  var scrollSpeed : CGFloat = 1.0
  var bounceDistance : CGFloat = 1.0
  var scrollEnabled : Bool = true
  var pagingEnabled : Bool = false
  var wrapEnabled : Bool = false
  var bounces : Bool = false
  var scrollOffset : CGFloat = 0
  var offsetMultiplier : CGFloat = 1.0
  var contentOffset : CGSize = .zero
  var viewpointOffset : CGSize = .zero
  var numberOfItems : Int = 0
  var numberOfPlaceholders : Int = 0
  var currentItemIndex : Int = 0
  var currentItemNode : ASDisplayNode?
  var indexesForVisibleNodes : [IndexPath] = []
  var numberOfVisibleNodes : Int {
    return visibleItemNodes.count
  }
  var visibleItemNodes : [ASDisplayNode] = []
  var itemNodeWidth : CGFloat = 0
  let contentViewNode : ASDisplayNode = ASDisplayNode()
  var toggle : CGFloat = 0
  var autoScroll : CGFloat = 0
  var stopAtItemBoundary : Bool = true
  var scrollToItemBoundary : Bool = true
  var ignorePerpendicularSwipes : Bool = true
  var centerItemWhenSelected : Bool = true
  var isDragging : Bool = false
  var isDecelerating : Bool = false
  var isScrolling : Bool = false
  
  func setup() {
    assert(Thread.isMainThread)
    
    contentViewNode.frame = bounds
    contentViewNode.autoresizingMask =
      [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
    
    // add pan gesture recognizer
    let panGesture = UIPanGestureRecognizer(target: self,
       action: #selector(TextureCarousel.didPan(_:)))
    
    panGesture.delegate = self
    
    contentViewNode.view.addGestureRecognizer(panGesture)
    
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(TextureCarousel.didTap(_:)))
  }
  
  func didPan(_ sender: UIPanGestureRecognizer) {
    
  }
  
  func didTap(_ sender: UITapGestureRecognizer) {
    
  }
  
  func scroll(byOffset offset : CGFloat, duration: TimeInterval) {
    
  }
  
  func scroll(toOffset offset: CGFloat, duration: TimeInterval) {
    
  }
  
  func scroll(byNumberOfItems itemCount : Int, duration: TimeInterval) {
    
  }
  
  func scroll(toItemAtIndex index : Int, duration: TimeInterval) {
    
  }
  
  func scroll(toItemAtIndex index : Int, animated: Bool) {
    
  }
  
  func itemNodeAtIndex(index: Int) -> ASDisplayNode {
    return ASDisplayNode()
  }
  
  func index(ofItemNode node: ASDisplayNode) -> Int {
    return 0
  }
  
  func index(ofItemNodeOrSubNode node : ASDisplayNode) -> Int {
    return 0
  }
  
  func offset(forItemAtIndex index : Int) -> CGFloat {
    return 0
  }
  
  func itemNode(atPoint point : CGPoint) -> ASDisplayNode? {
    return nil
  }
  
  func remove(itemAtIndex index : Int, animated: Bool) {
    
  }

  func insert(itemAtIndex index: Int, animated: Bool) {
  
  }
  
  func reload(itemAtIndex index: Int, animated: Bool) {
    
  }
  
  func reloadData() {
    
  }

}

extension TextureCarousel : UIGestureRecognizerDelegate {
  
}


