//
//  ViewController.swift
//  TextureCarousel
//
//  Created by Don Clore on 7/31/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit
import AsyncDisplayKit

class ViewController: UIViewController {
  
  let carousel : TextureCarousel = TextureCarousel()

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }
}

extension ViewController : CarouselDelegate {
  func carousel(carousel: TextureCarousel, didSelectItemAtIndex index: Int) {
    
  }
  
  func currentItemIndexDidChange(carousel: TextureCarousel) {
    
  }
}

extension ViewController : CarouselDataSource {
  func numberOfItems(inCarousel carousel: TextureCarousel) -> Int {
    return 0
  }
  
  func carousel(carousel: TextureCarousel, nodeBlockForItemAtIndex index: IndexPath) ->
    () -> ASDisplayNode {

      return {
        return ASDisplayNode()
      }
  }
}



