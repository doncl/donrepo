//
//  main.m
//  RandomItems
//
//  Created by Don Clore on 3/7/15.
//  Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BNRItem.h"
#import "BNRContainer.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        //Create a mutable array object

        NSMutableArray *items = [[NSMutableArray alloc] init];

        for (int i = 0; i < 10; i++) {
            BNRItem *item = [BNRItem randomItem];
            [items addObject:item];
        }

        NSMutableArray * subItems = [[NSMutableArray alloc] init];
        for (int i = 0; i < 3; i++) {
            BNRItem * subItem = [BNRItem randomItem];
            [subItems addObject:subItem];
        }

        BNRContainer * subCtr = [[BNRContainer alloc] init];
        subCtr.name = @"Sub Container";
        subCtr.items = subItems;

        NSString * subDesc = subCtr.description;

        items[0] = subCtr;

        BNRContainer * ctr = [[BNRContainer alloc] init];
        ctr.name = @"My Container";
        ctr.items = items;

        NSString * desc = ctr.description;

        NSLog(@"%@", desc);

        // Destroy the mutable array object
        items = nil;
        subItems = nil;
        ctr = nil;
    }
    return 0;
}
