//
// Created by Don Clore on 3/8/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BNRItem.h"


@interface BNRContainer : BNRItem
{
    NSArray * _items;
    NSArray * _name;
}

- (void)setItems:(NSArray *) array;
- (NSArray *) items;

- (void) setName:(NSArray *) name;
- (NSString *) name;
@end