//
// Created by Don Clore on 3/8/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "BNRContainer.h"


@interface BNRContainer ()
- (int)sumArray;
@end

@implementation BNRContainer
{

}
- (void)setItems:(NSArray *)array
{
    _items = array;
}

- (NSArray *)items {
    return _items;
}

- (void)setName:(NSArray *)name
{
    _name = name;
}


- (NSString *)name
{
    return _name;
}


- (NSString *) description
{
    int sum = [self sumArray:self.items];

    NSString * itemsDesc = [self descriptionHelper:self.items];
    NSString * name = self.name;


    NSString * desc = [NSString stringWithFormat: @"Name: %@, Sum: %d, Item Detail: %@",
            self.name, sum, itemsDesc];

    return desc;
}

- (NSString *) descriptionHelper:(NSArray *)arr
{
    NSMutableString * stringBuilder = [[NSMutableString alloc] init];
    for (BNRItem *item in arr){
        if (item != nil) {
            if ([item isKindOfClass:[BNRContainer class]]) {
                BNRContainer * ctr = (BNRContainer *) item;
                [stringBuilder appendString:
                        [NSString stringWithFormat:@"SubContainer name %@\n", ctr.name]];
                NSString *recursedValue = [ctr descriptionHelper:ctr.items];
                NSString * withNewline = [NSString stringWithFormat:@"%@\n", recursedValue];
                [stringBuilder appendString:withNewline];

            } else {
                [stringBuilder appendString:
                        [NSString stringWithFormat:@"%@\n", item.description]];
            }
        }
    }
    return stringBuilder;
}

- (int) sumArray:(NSArray *)arr
{
    int sum = 0;
    for (BNRItem *item in arr) {
        if (item != nil) {
            if ([item isKindOfClass:[BNRContainer class] ]) {
                BNRContainer * ctr = (BNRContainer *) item;
                sum += [ctr sumArray:ctr.items];
            } else {
                sum += item.valueInDollars;
            }
        }
    }
    return sum;
}
@end