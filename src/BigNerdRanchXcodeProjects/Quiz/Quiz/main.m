//
//  main.m
//  Quiz
//
//  Created by Don Clore on 6/22/14.
//  Copyright (c) 2014 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BBPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BBPAppDelegate class]));
    }
}
