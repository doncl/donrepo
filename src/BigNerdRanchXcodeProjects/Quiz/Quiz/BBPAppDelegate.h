//
//  BBPAppDelegate.h
//  Quiz
//
//  Created by Don Clore on 6/22/14.
//  Copyright (c) 2014 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BBPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
