//
//  BNRDrawView.m
//  TouchTracker
//
//  Created by Don Clore on 3/17/15.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import "BNRDrawView.h"
#import "BNRLine.h"

static NSMutableArray * colors;

@interface BNRDrawView ()

@property (nonatomic, strong) NSMutableDictionary *linesInProgress;

@end

@implementation BNRDrawView

- (instancetype)initWithFrame:(CGRect)r
{
    self = [super initWithFrame:r];
    
    if (self) {
        self.linesInProgress = [[NSMutableDictionary alloc] init];
        self.finishedLines = [[NSMutableArray alloc] init];
        self.backgroundColor = [UIColor grayColor];
        self.multipleTouchEnabled = YES;
    }

    if (!colors) {
        colors = [[NSMutableArray alloc] init];
        [colors addObject:[UIColor blackColor]];
        [colors addObject:[UIColor darkGrayColor]];
        [colors addObject:[UIColor lightGrayColor]];
        [colors addObject:[UIColor whiteColor]];
        [colors addObject:[UIColor grayColor]];
        [colors addObject:[UIColor redColor]];
        [colors addObject:[UIColor greenColor]];
        [colors addObject:[UIColor blueColor]];
        [colors addObject:[UIColor cyanColor]];
        [colors addObject:[UIColor yellowColor]];
        [colors addObject:[UIColor magentaColor]];
        [colors addObject:[UIColor orangeColor]];
        [colors addObject:[UIColor purpleColor]];
        [colors addObject:[UIColor brownColor]];
    }
  
    return self;
}

#pragma mark - Drawing methods
- (void) strokeLine: (BNRLine *) line
{
    UIBezierPath *bp = [UIBezierPath bezierPath];
    bp.lineWidth = 10;
    bp.lineCapStyle = kCGLineCapRound;
    
    [bp moveToPoint:line.begin];
    [bp addLineToPoint:line.end];
    [bp stroke];
}

- (void) drawRect: (CGRect) rect
{
    // Draw finished lines in black.
    //[[UIColor blackColor] set];
    for (BNRLine *line in self.finishedLines) {
        double deltaY = fabs(line.begin.y - line.end.y);
        double deltaX = fabs(line.begin.x - line.end.x);
        if (deltaX == 0.0) {
            deltaX = 1.0;
        }

        NSUInteger m = (NSUInteger) (deltaY / deltaX);
        NSUInteger mod = m % colors.count;
        mod = colors.count - mod - 1;

        UIColor *color = [colors objectAtIndex:mod];
        [color set];
        [self strokeLine:line];
    }
    
    [[UIColor redColor] set];
    for (NSValue *key in self.linesInProgress) {
        [self strokeLine:self.linesInProgress[key]];
    }
    
}

#pragma mark - Turning Touches into Lines
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Let's put in a log statement to see the order of events.
    NSLog(@"%@", NSStringFromSelector(_cmd));
    
    for (UITouch *t in touches) {
        CGPoint location = [t locationInView:self];
        
        BNRLine *line = [[BNRLine alloc] init];
        line.begin = location;
        line.end = location;
        
        NSValue *key = [NSValue valueWithNonretainedObject:t];
        self.linesInProgress[key] = line;
    }
    [self setNeedsDisplay];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Let's put in a log statement to see the order of events.
    NSLog(@"%@", NSStringFromSelector(_cmd));
    
    for (UITouch *t in touches) {
        NSValue *key = [NSValue valueWithNonretainedObject:t];
        BNRLine *line = self.linesInProgress[key];
        
        line.end = [t locationInView:self];
    }
    
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Let's put in a log statement to see the order of events.
    NSLog(@"%@", NSStringFromSelector(_cmd));
    
    for (UITouch *t in touches) {
        NSValue *key = [NSValue valueWithNonretainedObject:t];
        BNRLine *line = self.linesInProgress[key];
        
        [self.finishedLines addObject:line];
        [self.linesInProgress removeObjectForKey:key];
    }
    
    [self setNeedsDisplay];
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Let's put in a log statement to see the order of events.
    NSLog(@"%@", NSStringFromSelector(_cmd));

    for (UITouch *t in touches) {
        NSValue *key = [NSValue valueWithNonretainedObject:t];
        [self.linesInProgress removeObjectForKey:key];
    }
    [self setNeedsDisplay];
}

@end
