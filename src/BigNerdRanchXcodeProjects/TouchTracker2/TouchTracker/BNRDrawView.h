//
//  BNRDrawView.h
//  TouchTracker
//
//  Created by Don Clore on 3/17/15.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNRDrawView : UIView

@property (nonatomic, strong) NSMutableArray *finishedLines;

@end
