//
//  BNRItemStore.m
//  Homepwner
//
//  Created by Don Clore on 3/13/15.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import "BNRItemStore.h"
#import "BNRItem.h"

@interface BNRItemStore ()

@property (nonatomic) NSMutableArray *privateItems0;
@property (nonatomic) NSMutableArray *privateItems1;
@end

@implementation BNRItemStore

+ (instancetype) sharedStore
{
    static BNRItemStore *sharedStore;

    // Do I need to create a sharedStore?
    if (!sharedStore) {
        sharedStore = [[self alloc] initPrivate];
    }
    return sharedStore;
}


// If a programmer calls [[BNRItemStore alloc] init], let her know the error of her ways.
- (instancetype) init
{
    [NSException raise:@"Singleton" format:@"Use +[BNRItemStore sharedStore]"];
    
    return nil;
}

// Here is the real (secret) initializer.
- (instancetype) initPrivate
{
    self = [super init];
    if (self) {
        _privateItems0 = [[NSMutableArray alloc] init];
        _privateItems1 = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (NSArray *) allItems0
{

    return [self.privateItems0 copy];
}

- (NSArray *) allItems1
{
    return [self.privateItems1 copy];
}

- (BNRItem *) createItem
{
    BNRItem *item = [BNRItem randomItem];

    if (item.valueInDollars <= 50) {
        [self.privateItems0 addObject:item];
    } else {
        [self.privateItems1 addObject:item];
    }

    return item;
}

@end
