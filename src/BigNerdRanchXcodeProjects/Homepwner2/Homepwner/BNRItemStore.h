//
//  BNRItemStore.h
//  Homepwner
//
//  Created by Don Clore on 3/13/15.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BNRItem;

@interface BNRItemStore : NSObject

@property (nonatomic, readonly, copy) NSArray *allItems0;
@property (nonatomic, readonly, copy) NSArray *allItems1;

// Notice that this is a class method and prefixed with a + instead of a -
+ (instancetype) sharedStore;

- (BNRItem *) createItem;
@end
