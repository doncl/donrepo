//
//  BNRItem.h
//  RandomItems
//
//  Created by Don Clore on 3/7/15.
//  Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNRItem : NSObject
{
    NSString *_itemName;
    NSString *_serialNumber;
    int _valueInDollars;
    NSDate *_dateCreated;
}

+ (instancetype)randomItem;

- (instancetype)initWithItemName:(NSString *) name;

// Designated initializer for BNRItem
- (instancetype)initWithItemName:(NSString *) name
                  valueInDollars:(int)value
                    serialNumber:(NSString *) sNumber;

- (void)setItemName:(NSString *) str;
- (NSString *) itemName;

- (void) setSerialNumber:(NSString *) str;
- (NSString *) serialNumber;

- (void) setValueInDollars:(int) v;
- (int) valueInDollars;

- (NSDate *) dateCreated;
@end
