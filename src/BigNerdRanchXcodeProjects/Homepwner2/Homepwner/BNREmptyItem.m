//
// Created by Don Clore on 3/14/15.
// Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import "BNREmptyItem.h"


@implementation BNREmptyItem
- (NSString *) description
{
    return @"No more items!";
}
@end