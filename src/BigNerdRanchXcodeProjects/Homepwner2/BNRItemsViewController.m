//
//  BNRItemsViewController.m
//  Homepwner
//
//  Created by Don Clore on 3/13/15.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BNRItemsViewController.h"
#import "BNRItemStore.h"
#import "BNRItem.h"
#import "BNREmptyItem.h"


@interface BNRItemsViewController ()
@property (strong, nonatomic) BNREmptyItem *lastItem;
@end

@implementation BNRItemsViewController

- (instancetype) init
{
    // Call the superclass's designated initializer.
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        for (int i = 0; i < 5; i++) {
            [[BNRItemStore sharedStore] createItem];
        }
        _lastItem = [[BNREmptyItem alloc] init];
    }
    return self;
}

- (instancetype) initWithStyle: (UITableViewStyle) style
{
    return [self init];
}

- (NSInteger)tableView: (UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [[[BNRItemStore sharedStore] allItems0] count];
    }

    return [[[BNRItemStore sharedStore] allItems1] count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get a new or recycled cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    
    // Set the text on the cell with the description of the item that is at the nth index of items,
    // where n = row this cell will appearing on the tableview.

    NSArray * items;

    if (indexPath.section == 0) {
        items = [[BNRItemStore sharedStore] allItems0];
    } else {
        NSMutableArray* group1 = [[NSMutableArray alloc] init];

        NSArray * itemsFromStore = [[BNRItemStore sharedStore] allItems1];
        [group1 addObjectsFromArray:itemsFromStore];
        [group1 addObject:_lastItem];
        items = group1;
    }

    BNRItem *item = items[indexPath.row];
    
    cell.textLabel.text = [item description];
    
    return cell;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass: [UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];

    UIImageView *backgroundView =
        [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"1362076-tardis-on-the-field.png"]];


    [self.tableView setBackgroundView:backgroundView];

    self.tableView.backgroundColor = [UIColor clearColor];
}



//+--------------------------------------
//  BRONZE CHALLENGE STUFF
//---------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString *) tableView:(UITableView *) tableView titleForHeaderInSection:(NSInteger) section
{
    return section ? @"Section 1" : @"Section 0";
}


- (CGFloat) tableView:(UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    if (indexPath.section == 0) {
        return 60.0;
    }


    NSInteger rowCount = [[[BNRItemStore sharedStore] allItems1] count] + 1;
    if (indexPath.row < (rowCount - 1)) {
        return 60.0;
    }

    return 44.0;
}

@end
