//
//  BNRDrawViewController.m
//  TouchTracker
//
//  Created by Don Clore on 3/17/15.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import "BNRDrawViewController.h"
#import "BNRDrawView.h"
#import "BNRLine.h"

@interface BNRDrawViewController ()

@end

@implementation BNRDrawViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadView
{
    self.view = [[BNRDrawView alloc] initWithFrame:CGRectZero];
}

#pragma  mark - Persistence methods
- (void)saveDrawnLines
{
    BNRDrawView * view = (BNRDrawView *)self.view;
    NSError *error;
    
    NSString *plistPath = [self getPropertyListPath];
    NSDictionary *pList = [self makeLinesToPlist];
    
    NSData *data = [NSPropertyListSerialization dataWithPropertyList:pList
                                                              format:kCFPropertyListXMLFormat_v1_0
                                                             options:0 error:&error];
    
    if (data == nil) {
        NSLog(@"You messed up, big guy");
        return;
    }
    
    BOOL writeStatus = [data writeToFile:plistPath options:NSDataWritingAtomic error: &error];
    if (!writeStatus) {
        NSLog(@"error writing to file: %@", error);
        return;
    }
}

- (void)loadDrawnLines
{
    BNRDrawView * view = (BNRDrawView *)self.view;
    NSError *error;
    NSString *plistPath = [self getPropertyListPath];
    
    NSData *data = [NSData dataWithContentsOfFile:plistPath options:0 error: &error];
    if (data == nil) {
        NSLog(@"Couldn't find any persistence file at %@", plistPath);
        return;
    }
    
    NSPropertyListFormat format;
    id plist = [NSPropertyListSerialization propertyListWithData:data
                                                         options:NSPropertyListImmutable
                                                          format:&format error:&error];
    
    if (plist == nil) {
        NSLog(@"could not deserialize %@: %@", plistPath, error);
        return;
    }
    if (format != kCFPropertyListXMLFormat_v1_0) {
        NSLog(@"Unexpected format returned %@", format);
        return;
    }
    
    NSMutableDictionary *top = (NSMutableDictionary *)plist;
    NSMutableArray * lines = (NSMutableArray *) top[@"lines"];
    for (NSArray *lineArray in lines) {
        BNRLine * line = [[BNRLine alloc] init];
        float beginX = [[lineArray objectAtIndex:0] floatValue];
        float beginY = [[lineArray objectAtIndex:1] floatValue];
        float endX = [[lineArray objectAtIndex:2] floatValue];
        float endY = [[lineArray objectAtIndex:3] floatValue];
        
        line.begin = CGPointMake(beginX, beginY);
        line.end = CGPointMake(endX, endY);
        
        [view.finishedLines addObject:line];
    }
}

#pragma mark - Helper methods
- (NSString *)getPropertyListPath
{
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)
                          objectAtIndex:0];
    
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"Data.plist"];
    return plistPath;
}

- (id) makeLinesToPlist
{
    BNRDrawView * view = (BNRDrawView *)self.view;
    NSMutableDictionary * top = [NSMutableDictionary dictionary];
    
    NSMutableArray *lines = [[NSMutableArray alloc] init];
    for (BNRLine *line in view.finishedLines) {
        NSArray *lineArray = @[@(line.begin.x), @(line.begin.y), @(line.end.x), @(line.end.y)];
        [lines addObject:lineArray];
    }
    [top setObject:lines forKey:@"lines"];
    return top;
}


@end
