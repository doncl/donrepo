//
// Created by Don Clore on 3/22/15.
// Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BNRImageViewController : UIViewController
@property (strong, atomic) UIImage *image;
@end