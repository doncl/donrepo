//
// Created by Don Clore on 3/27/15.
// Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BNRColorDescription : NSObject

@property (nonatomic) UIColor *color;
@property (nonatomic, copy) NSString *name;
@end