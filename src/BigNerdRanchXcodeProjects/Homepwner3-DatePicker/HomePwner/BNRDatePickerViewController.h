//
//  BNRDatePickerViewController.h
//  HomePwner
//
//  Created by Don Clore on 3/15/15.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BNRItem.h"

@interface BNRDatePickerViewController : UIViewController

@property (nonatomic, strong) BNRItem *item;

@end
