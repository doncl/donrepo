//
//  BNRDatePickerViewController.m
//  HomePwner
//
//  Created by Don Clore on 3/15/15.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import "BNRDatePickerViewController.h"
#import "BNRItem.h"

@interface BNRDatePickerViewController ()
@property (weak, nonatomic) IBOutlet UIDatePicker *dateControl;

@end

@implementation BNRDatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    BNRItem * item = self.item;
   
    [_dateControl setDate:item.dateCreated];
   
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Clear first responder.
    [self.view endEditing:YES];
    
    // "Save" changes to the item.
    BNRItem *item = self.item;
    
    NSDate * editedDate = [self.dateControl date];
    item.dateCreated = editedDate;
}

@end
