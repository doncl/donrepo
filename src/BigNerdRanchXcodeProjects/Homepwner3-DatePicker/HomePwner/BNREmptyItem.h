//
// Created by Don Clore on 3/14/15.
// Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BNRItem.h"

@interface BNREmptyItem : BNRItem
@end