//
//  BNRCustomPopoverBackgroundView.h
//  HomePwner
//
//  Created by Don Clore on 3/21/15.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import <UIKit/UIPopoverBackgroundView.h>

@interface BNRCustomPopoverBackgroundView : UIPopoverBackgroundView
{
    UIImageView * _borderImageView;
    UIImageView * _arrowView;
    CGFloat _arrowOffset;
    UIPopoverArrowDirection _arrowDirection;
}
@end
