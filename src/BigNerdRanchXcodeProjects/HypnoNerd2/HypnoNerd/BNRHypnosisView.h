//
//  BNRHypnosisView.h
//  Hypnosister
//
//  Created by Don Clore on 3/9/15.
//  Copyright (c) 2015 BeerBarrelPokerStudios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNRHypnosisView : UIView

@property (strong, nonatomic, readwrite) UIColor *circleColor;

@end
