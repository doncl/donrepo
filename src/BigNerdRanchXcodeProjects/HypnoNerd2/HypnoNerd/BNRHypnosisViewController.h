//
//  BNRHypnosisViewController.h
//  HypnoNerd
//
//  Created by Don Clore on 3/11/15.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BNRHypnosisView.h"

@interface BNRHypnosisViewController : UIViewController
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) BNRHypnosisView *hypnosisView;
@end
