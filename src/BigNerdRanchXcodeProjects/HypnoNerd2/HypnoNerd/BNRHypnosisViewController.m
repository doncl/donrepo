//
//  BNRHypnosisViewController.m
//  HypnoNerd
//
//  Created by Don Clore on 3/11/15.
//  Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import "BNRHypnosisViewController.h"
#import "BNRHypnosisView.h"

@implementation BNRHypnosisViewController

@synthesize segmentedControl;
@synthesize hypnosisView;


- (instancetype) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        // Set the tab bar item's title.
        self.tabBarItem.title = @"Hypnotize";
        
        // Create a UIImage from a file.
        // This will use Hypno@2x.png on retina display devices.
        UIImage *image = [UIImage imageNamed:@"Hypno.png"];
        
        // Put that image on the tab bar item.
        self.tabBarItem.image = image;
    }
    return self;
}

- (void) loadView
{
    // Create a view
    CGRect frame = [UIScreen mainScreen].bounds;
    self.hypnosisView = [[BNRHypnosisView alloc] initWithFrame:frame];
    
    // Set it as *the* view of this view controller
    self.view = self.hypnosisView;
}

- (void) viewDidLoad
{
    // Always call the super implementation of viewDidLoad.
    [super viewDidLoad];
    
    NSLog(@"BNRHypnosisViewController loaded its view.");
    
    NSArray *mySegments = [[NSArray alloc] initWithObjects: @"Red", @"Green", @"Blue", nil];
    
    self.segmentedControl = [[UISegmentedControl alloc] initWithItems:mySegments];
    
    self.segmentedControl.frame = self.view.frame;
    [self.segmentedControl setSelectedSegmentIndex:1];
    
    [self.segmentedControl addTarget:self action:@selector(whichColor:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:self.segmentedControl];
    
}

- (void) whichColor:(UISegmentedControl *)paramSender
{
    if ([paramSender isEqual:self.segmentedControl]) {
        
        // get index position for the selected control.
        NSInteger selectedIndex = [paramSender selectedSegmentIndex];
        
        // get the Text for the segmented control that was selected.
        
        NSString *myChoice = [paramSender titleForSegmentAtIndex:selectedIndex];
        
        NSLog(@"Segment at position %i with %@ text is selected", (int)selectedIndex, myChoice);
        
        UIColor * color;
        if ([myChoice isEqualToString:@"Red"]) {
            color = [UIColor redColor];
        } else if ([myChoice isEqualToString:@"Green"]) {
            color = [UIColor greenColor];
        } else if ([myChoice isEqualToString:@"Blue"]) {
            color = [UIColor blueColor];
        }
        
        self.hypnosisView.circleColor = color;
    }
}

@end
