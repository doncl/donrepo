//
// Created by Don Clore on 3/23/15.
// Copyright (c) 2015 Big Nerd Ranch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BNRWebViewController : UIViewController <UISplitViewControllerDelegate>

@property (nonatomic) NSURL *URL;

@end