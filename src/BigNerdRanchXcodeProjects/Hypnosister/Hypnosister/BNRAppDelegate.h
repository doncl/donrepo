//
//  BNRAppDelegate.h
//  Hypnosister
//
//  Created by Don Clore on 3/9/15.
//  Copyright (c) 2015 BeerBarrelPokerStudios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

