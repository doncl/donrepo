//
//  ViewController.swift
//  JWPlayerTest
//
//  Created by Don Clore on 8/21/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  var config : JWConfig?
  var player : JWPlayerController?
  
  var urlString = "https://maven-user-video-transcoded-videos.s3.amazonaws.com/p9oPggQEBkWKViLIe9dqEg/the-maven/dondev/008CFAC8-DD44-4D4F-9248-D6599E7EB041/008CFAC8-DD44-4D4F-9248-D6599E7EB041.m3u8"
  
//  var urlString = "https://maven-user-video-transcoded-videos.s3.amazonaws.com/p9oPggQEBkWKViLIe9dqEg/the-maven/dondev/008CFAC8-DD44-4D4F-9248-D6599E7EB041/ab67cdd8-d513-4b0d-97f9-2ff89f83b7be_1000.mp4"

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }


  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    let sources : [JWSource] = [
      JWSource(file: "https://videos.themaven.net/NabmZqP2AUiU76KWwL1Peg/nealegodfrey/ask/8a28b505-bc2b-4fb4-a5bc-24d46add4a15/59164217-25df-4b7e-83a1-afc1bc6dbc58_3000.mp4", label: "3000 kbps Mp4"),
      JWSource(file: "https://videos.themaven.net/NabmZqP2AUiU76KWwL1Peg/nealegodfrey/ask/8a28b505-bc2b-4fb4-a5bc-24d46add4a15/59164217-25df-4b7e-83a1-afc1bc6dbc58_1000.mp4", label: "1000 kbps Mp4"),
      JWSource(file: "https://videos.themaven.net/NabmZqP2AUiU76KWwL1Peg/nealegodfrey/ask/8a28b505-bc2b-4fb4-a5bc-24d46add4a15/59164217-25df-4b7e-83a1-afc1bc6dbc58_500.mp4", label: "500 kbps Mp4"),
      JWSource(file: "https://videos.themaven.net/NabmZqP2AUiU76KWwL1Peg/nealegodfrey/ask/8a28b505-bc2b-4fb4-a5bc-24d46add4a15/8a28b505-bc2b-4fb4-a5bc-24d46add4a15.m3u8", label: "MBR Hls", isDefault: true),
      JWSource(file: "https://videos.themaven.net/NabmZqP2AUiU76KWwL1Peg/nealegodfrey/ask/8a28b505-bc2b-4fb4-a5bc-24d46add4a15/59164217-25df-4b7e-83a1-afc1bc6dbc58_500.webm", label: "500 kbps webm"),
      JWSource(file: "https://videos.themaven.net/NabmZqP2AUiU76KWwL1Peg/nealegodfrey/ask/8a28b505-bc2b-4fb4-a5bc-24d46add4a15/59164217-25df-4b7e-83a1-afc1bc6dbc58_1000.webm", label: "1000 kbps webm"),
      JWSource(file: "https://videos.themaven.net/NabmZqP2AUiU76KWwL1Peg/nealegodfrey/ask/8a28b505-bc2b-4fb4-a5bc-24d46add4a15/59164217-25df-4b7e-83a1-afc1bc6dbc58_3000.webm", label: "3000 kbps webm"),
    ]
    
    config = JWConfig()
    if let config = config {
      config.title = "video test"
      config.image = "https://videothumbs.themaven.net/NabmZqP2AUiU76KWwL1Peg/nealegodfrey/ask/8a28b505-bc2b-4fb4-a5bc-24d46add4a15/8a28b505-bc2b-4fb4-a5bc-24d46add4a15thumb300000001.png"
      
      config.sources = sources
      
      config.autostart = true
      config.controls = true
      config.premiumSkin = JWPremiumSkinRoundster
      player = JWPlayerController(config: config, delegate: self)
      if let player = player {
        view.addSubview(player.view)
//        player.view.translatesAutoresizingMaskIntoConstraints = false
//        NSLayoutConstraint.activate([
//          player.view.topAnchor.constraint(equalTo: view.topAnchor),
//          player.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
//          player.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
//          player.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
//        ])
        player.view.frame = view.bounds
        player.view.autoresizingMask = [
          UIViewAutoresizing.flexibleBottomMargin,
          UIViewAutoresizing.flexibleHeight,
          UIViewAutoresizing.flexibleLeftMargin,
          UIViewAutoresizing.flexibleRightMargin,
          UIViewAutoresizing.flexibleTopMargin,
          UIViewAutoresizing.flexibleWidth]

        player.openSafariOnAdClick = true
        player.forceFullScreenOnLandscape = true
        player.forceLandscapeOnFullScreen = true
        //        player.enterFullScreen()
        player.play()
      }
    }
  }
}

extension ViewController : JWPlayerDelegate {
  func onReady() {
    print("ready")
  }
}

