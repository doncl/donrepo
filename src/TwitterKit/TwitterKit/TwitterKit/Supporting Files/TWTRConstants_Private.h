/*
 * Copyright (C) 2017 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#import <Foundation/Foundation.h>

#pragma mark - Twitter kit constants

FOUNDATION_EXPORT NSString *const TWTRVersion;
FOUNDATION_EXPORT NSString *const TWTRResourceBundleLocation;
FOUNDATION_EXPORT NSString *const TWTRBundleID;

#pragma mark - User messages

FOUNDATION_EXPORT NSString *const TWTRMissingKitInfoMsg;
FOUNDATION_EXPORT NSString *const TWTRMissingConsumerKeyMsg;
FOUNDATION_EXPORT NSString *const TWTRMissingConsumerSecretMsg;

#pragma mark - Twitter API

FOUNDATION_EXPORT NSString *const TWTRAPIRateLimitHeader;
FOUNDATION_EXPORT NSString *const TWTRAPIRateLimitRemainingHeader;
FOUNDATION_EXPORT NSString *const TWTRAPIRateLimitResetHeader;

#pragma mark - Kit Info

FOUNDATION_EXPORT NSString *const TWTRKitInfoConsumerKeyKey;
FOUNDATION_EXPORT NSString *const TWTRKitInfoConsumerSecretKey;

#pragma mark - URL Referrer

FOUNDATION_EXPORT NSString *const TWTRURLReferrer;
