//
//  main.m
//  Stringz
//
//  Created by Don Clore on 3/20/16.
//  Copyright (c) 2016 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {

        NSMutableString *str = [NSMutableString new];
        for (int i = 0; i < 10; i++) {
            [str appendString:@"Aaron is cool!\n"];
        }

        // Declare a pointer to an NSError object, but do not instantiate it.
        NSError *error;

        BOOL success = [str writeToFile:@"/too/darned/bad/cool.txt"
                             atomically:YES
                               encoding:NSUTF8StringEncoding
                                  error:&error];

        if (success) {
            NSLog(@"Done writing /tmp/cool.txt");
        } else {
            NSLog(@"writing /tmp/cool.txt failed: %@", error.localizedDescription);
        }
    }

    return 0;
}