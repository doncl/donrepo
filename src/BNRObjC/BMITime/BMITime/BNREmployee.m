//
//  BNREmployee.m
//  BMITime
//
//  Created by Don Clore on 3/19/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

#import "BNREmployee.h"
#import "BNRAsset.h"

@interface BNREmployee()
{
    NSMutableSet<BNRAsset *> *_assets;
}
@property (nonatomic) unsigned int officeAlarmCode;
@end

@implementation BNREmployee

// Accessors for assets properties
- (void)setAssets:(NSSet<BNRAsset *> *)assets
{
    _assets = [assets mutableCopy];
}

- (NSSet<BNRAsset*> *)assets
{
    return [_assets copy];
}

- (void)addAsset:(BNRAsset *)a
{
    // Is assets nil?
    if (!_assets) {
        // Create the set
        _assets = [NSMutableSet<BNRAsset *> new];
    }
    [_assets addObject:a];
    a.holder = self;
}

- (unsigned int)valueOfAssets
{
    // Sum up the resale value of the assets..
    unsigned int sum = 0;
    for (BNRAsset *a in _assets) {
        sum += [a resaleValue];
    }
    return sum;
}

- (double)yearsOfEmployment
{
    // Do I have a non-nil hire date?
    if (self.hireDate) {
        // NSTimeInterval is the same as double.
        NSDate *now = [NSDate date];
        NSTimeInterval secs = [now timeIntervalSinceDate:self.hireDate];
        return secs / 31557600.0;  // Seconds per year
    } else {
        return 0;
    }
}

- (float)bodyMassIndex
{
    float normalBMI = [super bodyMassIndex];
    return normalBMI * 0.9;
}


- (NSString *)description
{
    NSString *description = [NSString stringWithFormat:@"<Employee %u: $%u in assets>",
                    self.employeeID, self.valueOfAssets];

    return description;
}

- (void)dealloc
{
    NSLog(@"deallocating %@", self);
}


@end
