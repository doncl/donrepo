//
//  BNREmployee.h
//  BMITime
//
//  Created by Don Clore on 3/19/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

#import "BNRPerson.h"

@class BNRAsset;

@interface BNREmployee : BNRPerson
@property (nonatomic) unsigned int employeeID;
@property (nonatomic) NSDate *hireDate;
@property (nonatomic, copy) NSSet<BNRAsset *> *assets;

- (double)yearsOfEmployment;
- (void)addAsset:(BNRAsset *)a;
- (unsigned int)valueOfAssets;

@end
