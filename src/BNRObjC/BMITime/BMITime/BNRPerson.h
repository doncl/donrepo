//
//  BNRPerson.h
//  BMITime
//
//  Created by Don Clore on 3/19/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNRPerson : NSObject

@property (nonatomic) float heightInMeters;
@property (nonatomic) int weightInKilos;

// BNRPerson has a method that calculates the Body Mass Index.
- (float)bodyMassIndex;
@end
