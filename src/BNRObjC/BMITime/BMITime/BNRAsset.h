//
//  BNRAsset.h
//  BMITime
//
//  Created by Don Clore on 3/19/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BNREmployee;

@interface BNRAsset : NSObject
@property (nonatomic, copy) NSString *label;
@property (nonatomic, weak) BNREmployee *holder;
@property (nonatomic) unsigned int resaleValue;
@end
