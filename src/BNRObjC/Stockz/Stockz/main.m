//
//  main.m
//  Stockz
//
//  Created by Don Clore on 3/20/16.
//  Copyright (c) 2016 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {

        NSMutableArray *stocks = [NSMutableArray new];
        NSMutableDictionary *stock = [NSMutableDictionary new];

        stock[@"symbol"] = @"AAPL";
        stock[@"shares"] = [NSNumber numberWithInt:200];
        [stocks addObject:stock];

        stock = [NSMutableDictionary new];
        stock[@"symbol"] = @"GOOG";
        stock[@"shares"] = [NSNumber numberWithInt:160];
        [stocks addObject:stock];

        [stocks writeToFile:@"/tmp/stocks.plist" atomically:YES];

        NSArray *stockList = [NSArray arrayWithContentsOfFile:@"/tmp/stocks.plist"];

        for (NSDictionary *d in stockList) {
            NSLog(@"I have %@ shares of %@", d[@"shares"], d[@"symbol"]);
        }
    }

    return 0;
}