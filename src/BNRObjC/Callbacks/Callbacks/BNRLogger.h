//
//  BNRLogger.h
//  Callbacks
//
//  Created by Don Clore on 3/20/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNRLogger : NSObject<NSURLConnectionDelegate, NSURLConnectionDataDelegate>
{
    NSMutableData *_incomingData;
}
@property (nonatomic) NSDate *lastTime;

- (NSString *)lastTimeString;
- (void) updateLastTime:(NSTimer *)timer;
@end
