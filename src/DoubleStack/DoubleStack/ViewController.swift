//
//  ViewController.swift
//  DoubleStack
//
//  Created by Don Clore on 1/20/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class MyTable : UITableView {
  override var intrinsicContentSize: CGSize {
    return contentSize
  }
}

class ViewController: UIViewController {

  @IBOutlet var table: MyTable!
  @IBOutlet var stack: UIStackView!
  @IBOutlet var titleLabel: UILabel!
  
  @IBOutlet var titleLabel2: UILabel!
  @IBOutlet var table2: MyTable!

  let table1Labels : [String] = [
    "Popular", "Newest", "Latest Reply"
  ]
  
  let table2Labels : [String] = [
    "SplitStream", "MasterDetail", "SingleColumn"
  ]
  
  let rowHeight : CGFloat = 44.0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor.clear
    
    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
    let blurEffectView = UIVisualEffectView(effect: blurEffect)
    //always fill the view
    blurEffectView.frame = view.bounds
    blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    
    view.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
    view.sendSubview(toBack: blurEffectView)
    table.dataSource = self
    table.delegate = self
    table.rowHeight = rowHeight
    table.tableFooterView = UIView()
    
    table2.dataSource = self
    table2.delegate = self
    table2.rowHeight = rowHeight
    table2.tableFooterView = UIView()
    
    table.reloadData()
    table2.reloadData()
    table.layoutIfNeeded()
    table2.layoutIfNeeded()

    stack.layoutIfNeeded()
  }
  @IBAction func buttonTouched(_ sender: UIButton) {
    titleLabel2.isHidden = !titleLabel2.isHidden
    table2.isHidden = !table2.isHidden
  }
}



extension ViewController : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == table {
      return 3
    } else {
      return 3
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if tableView == table {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellId") else {
        fatalError("foo")
      }
      guard let label = cell.viewWithTag(44) as? UILabel else {
        fatalError("foo")
      }
      label.text = table1Labels[indexPath.row]
      return cell
    } else if tableView == table2 {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellId2") else {
        fatalError("bar")
      }
      guard let label = cell.viewWithTag(55) as? UILabel else {
        fatalError("bar")
      }
      label.text = table2Labels[indexPath.row]
      return cell
    }
    fatalError("dreck")
  }
  
  
}

extension ViewController : UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    if tableView == table {
      return 40.0
    }
    return 0
  }
}
