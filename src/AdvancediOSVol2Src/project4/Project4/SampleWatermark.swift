//
//  SampleWatermark.swift
//  Project4
//
//  Created by Paul Hudson on 27/06/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import PDFKit
import UIKit

class SampleWatermark: PDFPage {
    override func draw(with box: PDFDisplayBox, to context: CGContext) {
        // draw the existing page first
        super.draw(with: box, to: context)

        // create our string and attributes
        let string: NSString = "SAMPLE CHAPTER"
        let attributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.red, .font: UIFont.boldSystemFont(ofSize: 32)]
        let stringSize = string.size(withAttributes: attributes)

        // save the state before we start moving and rotating
        UIGraphicsPushContext(context)
        context.saveGState()

        // figure out how much space we have for drawing
        let pageBounds = bounds(for: box)

        // move and flip the context, making it render the right way up
        context.translateBy(x: (pageBounds.size.width - stringSize.width) / 2, y: pageBounds.size.height)
        context.scaleBy(x: 1.0, y: -1.0)

        // draw our string slightly down from the top
        string.draw(at: CGPoint(x: 0, y: 55), withAttributes: attributes)

        // put everything back as it was
        context.restoreGState()
        UIGraphicsPopContext()
    }
}
