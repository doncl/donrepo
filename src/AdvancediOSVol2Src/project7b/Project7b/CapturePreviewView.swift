//
//  CapturePreviewView.swift
//  Project7b
//
//  Created by Paul Hudson on 01/08/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import AVFoundation
import UIKit

class CapturePreviewView: UIView {
    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
}
