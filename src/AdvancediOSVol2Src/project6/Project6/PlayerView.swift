//
//  PlayerView.swift
//  Project6
//
//  Created by Paul Hudson on 31/07/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import UIKit

class PlayerView: UIView, UIPickerViewDataSource, UIPickerViewDelegate {
    weak var controller: PlayViewController?
    var picker = UIPickerView()
    var select = UIButton(type: .custom)
    var sortedSongs = [Song]()
    
    init(color: UIColor, songs: [Song], delegate: PlayViewController) {
        // call our super initializer with a dummy frame
        super.init(frame: .zero)

        // assign our basic properties
        controller = delegate
        select.backgroundColor = color
        sortedSongs = songs.sorted()
        backgroundColor = .white

        // prepare to use Auto Layout
        picker.translatesAutoresizingMaskIntoConstraints = false
        select.translatesAutoresizingMaskIntoConstraints = false
        addSubview(picker)
        addSubview(select)

        // make the picker occupy all the space up to where the select button lies
        picker.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        picker.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        picker.topAnchor.constraint(equalTo: topAnchor).isActive = true
        picker.bottomAnchor.constraint(equalTo: select.topAnchor).isActive = true

        // make the select button fill 50 points at the bottom of the view
        select.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        select.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        select.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        select.heightAnchor.constraint(equalToConstant: 50).isActive = true

        // give the select button some basic properties to make it look good
        select.setTitle("Select Song", for: .normal)
        select.setTitleColor(.white, for: .normal)
        select.showsTouchWhenHighlighted = true
        select.addTarget(self, action: #selector(selectTapped), for: .touchUpInside)

        picker.dataSource = self
        picker.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sortedSongs.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sortedSongs[row].attributes.name
    }

    @objc func selectTapped() {
        let selection = sortedSongs[picker.selectedRow(inComponent: 0)]
        controller?.selectTapped(player: select.backgroundColor!, answer: selection.attributes.name)
    }
}
