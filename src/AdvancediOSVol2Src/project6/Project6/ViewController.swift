//
//  ViewController.swift
//  Project6
//
//  Created by Paul Hudson on 27/07/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import StoreKit
import UIKit

class ViewController: UIViewController {
    let developerToken = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkgyS0E3V0ZBRDIifQ.eyJpc3MiOiJCNUMyNlhFNTlFIiwiaWF0IjoxNTAxNTM2NjY2LCJleHAiOjE1MDE1Nzk4NjZ9.era0-tKIt8gPZIEN607mxVrfOJRywkYAk7914Dc9pSIvPAOVqMNW4G_KRYlf44DijRbNNIyHyLOjdCv013h-Gw"
    let urlSession: URLSession = URLSession(configuration: .default)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let play = UIButton(type: .system)
        view.addSubview(play)

        play.translatesAutoresizingMaskIntoConstraints = false
        play.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        play.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        play.heightAnchor.constraint(equalToConstant: 80)

        play.setTitle("Start Game", for: .normal)
        play.addTarget(self, action: #selector(startGame), for: .touchUpInside)
    }

    @objc func startGame() {
        switch SKCloudServiceController.authorizationStatus() {
        case .notDetermined:
            SKCloudServiceController.requestAuthorization { [weak self] authorizationStatus in
                DispatchQueue.main.async {
                    self?.startGame()
                }
            }
        case .authorized:
            requestCapabilities()
        default:
            showNoGameMessage("We don't have permission to use your Apple Music library.")
        }
    }

    func showNoGameMessage(_ message: String) {
        let ac = UIAlertController(title: "No game for you", message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(ac, animated: true)
    }

    func requestCapabilities() {
        let controller = SKCloudServiceController()
        controller.requestCapabilities { capabilities, error in
            DispatchQueue.main.async {
                if let error = error {
                    print(error.localizedDescription)
                    return
                }

                if capabilities.contains(.musicCatalogPlayback) {
                    // we can play!
                    controller.requestStorefrontCountryCode { country, error in
                        if let country = country {
                            self.fetchSongs(countryCode: country)
                        } else {
                            self.showNoGameMessage("Unable to determine country code.")
                        }
                    }
                } else if capabilities.contains(.musicCatalogSubscriptionEligible) {
                    // we can at least subscribe to play
                    let subscribeController = SKCloudServiceSetupViewController()

                    let options: [SKCloudServiceSetupOptionsKey: Any] = [
                        .action: SKCloudServiceSetupAction.subscribe,
                        .messageIdentifier: SKCloudServiceSetupMessageIdentifier.playMusic
                    ]

                    subscribeController.load(options: options) { didSucceedLoading, error in
                        if didSucceedLoading {
                            self.present(subscribeController, animated: true)
                        } else {
                            self.showNoGameMessage(error?.localizedDescription ?? "Unknown error")
                        }
                    }
                } else {
                    // nada
                    self.showNoGameMessage("You aren't eligible to subscribe to Apple Music.")
                }
            }
        }
    }

    func fetchSongs(countryCode: String) {
        var urlRequest = URLRequest(url: URL(string: "https://api.music.apple.com/v1/catalog/\(countryCode)/charts?types=songs")!)
        urlRequest.addValue("Bearer \(developerToken)", forHTTPHeaderField: "Authorization")

        let task = urlSession.dataTask(with: urlRequest) { (data, response, error) in
            guard let data = data else { return }

            DispatchQueue.main.async {
                do {
                    let decoder = JSONDecoder()
                    let match = try decoder.decode(MusicResult.self, from: data)
                    if let songs = match.results.songs.first?.data {
                        let shuffledSongs = songs.shuffled()
                        let gameController = PlayViewController()
                        gameController.songs = shuffledSongs
                        self.present(gameController, animated: true)
                        return
                    }
                } catch {
                    // print a message, then fall through to show a user message
                    print(error.localizedDescription)
                }

                self.showNoGameMessage("Unable to fetch data from Apple Music")
            }
        }

        task.resume()
    }
}

