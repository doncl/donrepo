//
//  ShadesViewController.swift
//  Project8-DragDrop
//
//  Created by Paul Hudson on 24/06/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import UIKit

class ShadesViewController: UICollectionViewController {
    var blends = [(name: String, color: UIColor)]() {
        didSet {
            collectionView?.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Shades"
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return blends.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? ColorCell else { fatalError() }

        let blend = blends[indexPath.row]
        cell.swatch.backgroundColor = blend.color
        cell.textLabel.text = blend.name

        return cell
    }
}

