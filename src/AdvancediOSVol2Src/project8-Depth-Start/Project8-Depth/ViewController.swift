//
//  ViewController.swift
//  Project8-Depth
//
//  Created by Paul Hudson on 30/06/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let imageView = UIImageView()
    let aperture = UISlider()

    var displayMode = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Mode", style: .plain, target: self, action: #selector(changeDisplayMode))
        
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(imageView)
        
        imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        
        aperture.minimumValue = 1
        aperture.maximumValue = 22
        aperture.value = 8
        aperture.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(aperture)
        
        aperture.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        aperture.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        aperture.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 30).isActive = true
        aperture.addTarget(self, action: #selector(drawImage), for: .valueChanged)

        let recognizer = UITapGestureRecognizer(target: self, action: #selector(refocus))
        recognizer.delaysTouchesBegan = true
        imageView.addGestureRecognizer(recognizer)
        imageView.isUserInteractionEnabled = true

        loadImage()
    }

    func loadImage() {
        imageView.image = UIImage(named: "toy.heic")
        drawImage()
    }

    @objc func drawImage() {
        title = "Aperture: \(aperture.value)"
    }

    @objc func refocus(recognizer: UITapGestureRecognizer) {
        guard let bounds = recognizer.view?.bounds else { return }
        let touch = recognizer.location(in: recognizer.view)
    }

    @objc func changeDisplayMode() {
        displayMode += 1
        if displayMode == 3 { displayMode = 0 }
        drawImage()
    }
}
