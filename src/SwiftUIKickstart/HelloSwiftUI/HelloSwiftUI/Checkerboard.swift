//
//  Checkerboard.swift
//  HelloSwiftUI
//
//  Created by Don Clore on 10/4/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import SwiftUI

struct Checkerboard<A, B>: View where A: View, B: View{
  let firstView: A
  let secondView: B
  let stackColor: Color
  
  var body: some View {
    VStack {
      HStack {
        firstView
        secondView
      }
      HStack {
        secondView
        firstView
      }
    }.background(stackColor)
  }
  
  init(backgroundColor: Color = .white,
       @CheckerBoardBuilder builder: () -> (A, B)) {
    
    stackColor = backgroundColor
    (firstView, secondView) = builder()
  }
}

extension Checkerboard where B == Rectangle {
  
  init(backgroundColor: Color = .white,
       @CheckerBoardBuilder builder: () -> (A)) {
    stackColor = backgroundColor
    firstView = builder()
    secondView = Rectangle()
  }
}

struct Checkerboard_Previews: PreviewProvider {
  static var previews: some View {
    Checkerboard {
     Text("SwiftUI Rocks")
     Image("Cover")
    }
  }
}

@_functionBuilder
struct CheckerBoardBuilder {
  static func buildBlock<A: View>(_ firstView: A) {
    firstView
  }
  
  static func buildBlock<A: View, B: View>(_ firstView: A, _ secondView: B) -> (A, B) {
    (firstView, secondView)
  }
}


