//
//  ValueView.swift
//  Numbers
//
//  Created by Don Clore on 10/5/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import SwiftUI

struct ValueView: View {
  let value: Double
  
  var body: some View {
    Text(value.description)
  }
}

struct ValueView_Previews: PreviewProvider {
  static var previews: some View {
    ValueView(value: 10.0)
  }
}
