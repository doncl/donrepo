//
//  GreetingLabel.swift
//  HelloSwiftUIChap3
//
//  Created by Don Clore on 10/5/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import SwiftUI

struct GreetingLabel: View {
  let greeting: String
  
  var body: some View {
    Text(greeting)
      .font(.title)
      .foregroundColor(.secondary)
  }
}

struct GreetingLabel_Previews: PreviewProvider {
    static var previews: some View {
      GreetingLabel(greeting: "Hello")
    }
}
