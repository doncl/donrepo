//
//  GreetingLabel.swift
//  ManualGoodbye1
//
//  Created by Don Clore on 10/4/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import UIKit

class GreetingLabel: UILabel {
  convenience init(displaying text: String) {
    self.init()
    textColor = .secondaryLabel
    font = UIFont.preferredFont(forTextStyle: .title1)
    self.text = text 
  }
}
