//
//  GreetingButton.swift
//  ManualGoodbye1
//
//  Created by Don Clore on 10/4/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import UIKit

class GreetingButton: UIButton {
  convenience init(title: String, target: AnyObject, selector: Selector) {
    self.init(type: .roundedRect)
    setTitle(title, for: .normal)
    addTarget(target, action: selector, for: .touchUpInside)
  }
}
