//
//  ViewController.swift
//  SpawnAudio
//
//  Created by Don Clore on 6/17/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit
import MediaPlayer

class ViewController: UIViewController {
  var avAudioPlayer : AVAudioPlayer?
  
  var docDir : String {
    get {
      let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
      return paths[0]
    }
  }
 
//  let musicFileUrl : URL = URL(string: "https://maven-user-photos.s3.amazonaws.com/mp3/TT.mp3")!
  //let musicFileUrl : URL? = URL(string: "music://")
  let musicFileUrl : URL? = URL(string: "https://maven-user-photos.s3.amazonaws.com/mp3/TT.mp3")
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  @IBAction func buttonPushed(_ sender: UIButton) {

    if let musicFileUrl = musicFileUrl {
//      let player = MPMusicPlayerController.systemMusicPlayer()
//      let item = MPMediaItem()

      //let asset = AVAsset(url: musicFileUrl)
//       let playerItem:AVPlayerItem = AVPlayerItem(url: musicFileUrl)
//      let player = AVPlayer(playerItem: playerItem)
//      player.play()
//      DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
//        if let error = player.error {
//            print("error = \(error)")
//        }
//   
//      })
      
      if let data = try? Data(contentsOf: musicFileUrl) {
        let path = "\(docDir)/TT.mp3"
        let urlString = "file://\(path)"
//        if let fileUrl = URL(string: urlString) {
        let fileUrl = URL(fileURLWithPath: path)
          if let _ = try? data.write(to: fileUrl, options: Data.WritingOptions.atomicWrite) {
            //UIApplication.shared.open(fileUrl, options: [:], completionHandler: nil)
            //UIApplication.shared.open(musicFileUrl, options: [:], completionHandler: nil)
            
            let asset = AVAsset(url: fileUrl)
            self.avAudioPlayer = try? AVAudioPlayer(contentsOf: fileUrl)
              if let avAudioPlayer = self.avAudioPlayer {
                avAudioPlayer.play()
              }
          }
        
      }

    }
  }
}

