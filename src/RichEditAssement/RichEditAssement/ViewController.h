//
//  ViewController.h
//  RichEditAssement
//
//  Created by Don Clore on 9/25/15.
//  Copyright © 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JKRichTextView/JKRichTextView.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet JKRichTextView *jkRichText;


@end

