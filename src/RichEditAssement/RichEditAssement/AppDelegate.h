//
//  AppDelegate.h
//  RichEditAssement
//
//  Created by Don Clore on 9/25/15.
//  Copyright © 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

