//
//  ViewController.m
//  RichEditAssement
//
//  Created by Don Clore on 9/25/15.
//  Copyright © 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.jkRichText.shouldPassthoughUntouchableText = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
