//
//  Constants.swift
//  SurelyYouGist
//
//  Created by Mark Dalrymple on 2/8/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import Foundation

public let BbpDidReceiveURLNotification = "ApplicationDidReceiveURLNotification"
public let BbpOpenURLInfoKey = "ApplicationOpenURLInfoKey"

public let SharedDefaultsSuiteName = "group.com.beerbarrelpokerstudios.bbp.SurelyYouGist"

