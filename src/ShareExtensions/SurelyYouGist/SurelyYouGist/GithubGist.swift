//
//  Gist.swift
//  SurelyYouGist
//
//  Created by Michael Ward on 7/22/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

import Foundation

open class GithubGist: NSObject {
    @objc let remoteURL: URL
    @objc open var userDescription: String = ""
    @objc open var files: [GithubFile] = []
    
    @objc init(url remoteURL: URL) {
        self.remoteURL = remoteURL
    }
}
