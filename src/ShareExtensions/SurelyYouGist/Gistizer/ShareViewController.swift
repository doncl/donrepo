//
//  ShareViewController.swift
//  Gistizer
//
//  Created by Don Clore on 7/20/18.
//  Copyright © 2018 Big Nerd Ranch. All rights reserved.
//

import UIKit
import Social


class ShareViewController: SLComposeServiceViewController {
  let GithubUserNameDefaultsKey = "GithubUserNameDefaultsKey"
  let GithubAuthTokenDefaultsKey = "GithubAuthTokenDefaultsKey"
    let SharedDefaultsSuiteName = "group.com.beerbarrelpokerstudios.bbp.SurelyYouGist"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let token = GithubKeychain.token()
    print("Did we get a token? \(token == nil ? "no" : "yes")")
    
    let defs = UserDefaults(suiteName: SharedDefaultsSuiteName)
    let username = defs?.string(forKey: GithubUserNameDefaultsKey)
    print("Got username: \(username ?? "nil")")
  }

    override func isContentValid() -> Bool {
        // Do validation of contentText and/or NSExtensionContext attachments here
        return true
    }

    override func didSelectPost() {
      guard let context = self.extensionContext,
        let items = context.inputItems as? [NSExtensionItem],
        let item = items.first,
        let string = item.attributedContentText else  {
          self.extensionContext?.completeRequest(returningItems: [], completionHandler: nil)
          return
      }
      
      let plainString = string.string
      
      let githubClient = GithubClient()
      githubClient.postGist(plainString, description: "hello extension!", isPublic: true) { _ in
        self.extensionContext?.completeRequest(returningItems: [], completionHandler: nil)
      }
    }

    override func configurationItems() -> [Any]! {
        // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
        return []
    }

}
