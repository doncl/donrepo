//
//  CSS3DigitStringTests.swift
//  LoadRTFUnitTests
//
//  Created by Don Clore on 6/4/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import XCTest
@testable import LoadRTF

class CSS3DigitStringTests: XCTestCase {

  func testColorParsing() {
    testColorString("#000", colorToCompare: UIColor.black)
    testColorString("#FFF", colorToCompare: UIColor.white)
    testColorString("#F00", colorToCompare: UIColor.red)
    testColorString("#0F0", colorToCompare: UIColor.green)
    testColorString("#00F", colorToCompare: UIColor.blue)
    
    testColorString("#6EF", colorToCompare: #colorLiteral(red: 0.4551772475, green: 0.9422622919, blue: 1, alpha: 1))
    testColorString("#333", colorToCompare: #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1))
    testColorString("#FE6", colorToCompare: #colorLiteral(red: 1, green: 0.9333333333, blue: 0.4, alpha: 1))
  }
  
  private func testColorString(_ threeDigitStringWithPound: String, colorToCompare: UIColor) {
    guard let parsedColor = threeDigitStringWithPound.colorFromCSS3Digit else {
      XCTFail("Couldn't parse color")
      return
    }
    let f = parsedColor.colorEquals(otherColor: colorToCompare)
    XCTAssertTrue(f)
  }
}


fileprivate extension UIColor {
  func colorEquals(otherColor: UIColor) -> Bool {
    var r1: CGFloat = 0
    var b1: CGFloat = 0
    var g1: CGFloat = 0
    var a1: CGFloat = 0
      
    var r2: CGFloat = 0
    var g2: CGFloat = 0
    var b2: CGFloat = 0
    var a2: CGFloat = 0
    self.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
    otherColor.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)
    
    return abs(r1 - r2) < 1.0 && abs(g1 - g2) < 1.0 && abs(b1 - b2) < 1.0 && abs(a1 - a2) < 1.0
  }
}
