//
//  ANFStoryDecodableTests.swift
//  LoadRTFUnitTests
//
//  Created by Don Clore on 5/31/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import XCTest
@testable import LoadRTF

class ANFStoryDecodableTests: XCTestCase {
  let yogaJournalKey: String = "Yoga Journal"
  var data: Data?
  
  private func setResourceData() {
    let bundle = Bundle(for: type(of: self))
    guard let url = bundle.url(forResource: "yogajournal", withExtension: "anf") else {
      XCTFail("Missing resource")
      return
    }
    
    do {
      data = try Data(contentsOf: url)
    } catch let error {
      XCTFail("Error converting yogajournal to data = \(error.localizedDescription)")
    }
  }
  
  func testANFStoryDecode() {
    setResourceData()
    
    guard let data = data else {
      XCTFail("expected non-nil data")
      return
    }
    let start = Date()
    
    ANFStory.jsonDecode(data, success: { (story: ANFStory) in
      let time = Date().timeIntervalSince(start) * 1000.0
      print("time = \(time)")
      XCTAssertFalse(story.apiMetadata.isCandidateToBeFeatured)
      XCTAssertFalse(story.apiMetadata.isHidden)
      XCTAssertNotNil(story.apiMetadata.isPreview)
      XCTAssertNotNil(story.apiMetadata.maturityRating)
      
      // ComponentLayouts
      XCTAssertNotNil(story.articleJson.componentLayouts)
      let componentLayouts = story.articleJson.componentLayouts!
      XCTAssertEqual(12, componentLayouts.count)
      validateComponentLayouts(componentLayouts, for: yogaJournalKey)
      
      // ComponentStyles
      XCTAssertNotNil(story.articleJson.componentStyles)
      let componentStyles = story.articleJson.componentStyles!
      XCTAssertEqual(1, componentStyles.count)
      validateComponentStyles(componentStyles, for: yogaJournalKey)
      
      // ComponentTextStyles
      let componentTextStyles = story.articleJson.componentTextStyles
      validateComponentTextStyles(componentTextStyles, for: yogaJournalKey)
      
    }, failure: { (error: String) in
      XCTFail(error)
    })
  }
  
  private func validateComponentTextStyles(_ styles: [String: ANFStory.ComponentTextStyle], for key: String) {
    if key == yogaJournalKey {
      validateComponentTextStyle("attributionStyle", styles, fontName: "Georgia-Italic", fontSize: 14, fontStyle: "italic", textAlignment: "left", textColor: "#666")
      
      validateComponentTextStyle("bodyStyle", styles, fontName: "Georgia", fontSize: 18, lineHeight: 26, textAlignment: "left", textColor: "#333")
      
      validateComponentTextStyle("bylineStyle", styles, fontName: "HelveticaNeue", fontSize: 16, textAlignment: "left", textColor: "#000")
      
      validateComponentTextStyle("captionStyle", styles, fontName: "Georgia-Italic", fontSize: 16, fontStyle: "italic", textAlignment: "left", textColor: "#333")
      
      validateComponentTextStyle("heading2Style", styles, fontName: "HelveticaNeue-Bold", fontSize: 28, textAlignment: "left", textColor: "#000")
      
      validateComponentTextStyle("heading3Style", styles, fontName: "HelveticaNeue-Bold", fontSize: 20, textAlignment: "left", textColor: "#000")
      
      validateComponentTextStyle("heading4Style", styles, fontName: "HelveticaNeue-Bold", fontSize: 18, textAlignment: "left", textColor: "#000")
      
      validateComponentTextStyle("introStyle", styles, fontName: "HelveticaNeue-Medium", fontSize: 24, textAlignment: "left", textColor: "#000")
      
      validateComponentTextStyle("pullquoteStyle", styles, fontName: "HelveticaNeue-Medium", fontSize: 36, lineHeight: 36, textColor: "#000")
      
      validateComponentTextStyle("quoteStyle", styles, fontName: "HelveticaNeue-Medium", fontSize: 16, textAlignment: "left", textColor: "#777")
      
      validateComponentTextStyle("sectionTitleStyle", styles, fontName: "HelveticaNeue-Bold", fontSize: 24, textAlignment: "left", textColor: "#000")
      
      validateComponentTextStyle("titleStyle", styles, fontName: "HelveticaNeue-Bold", fontSize: 64, lineHeight: 74, textAlignment: "left", textColor: "#000")
      
    } else {
      // TODO: more types here, if we have multiple test files.
    }
  }
  
  private func validateComponentTextStyle(_ styleKey: String, _ styles: [String : ANFStory.ComponentTextStyle], backgroundColor: String? = nil,
                                          firstLineIndent: Int? = nil, fontFamily: String? = nil, fontName: String? = nil, fontSize: Int? = nil,
                                          fontStyle: String? = nil, fontWeight: String? = nil, fontWidth: String? = nil, hangingPunctuation: Bool? = nil,
                                          hyphenation: Bool? = nil, lineHeight: Int? = nil, paragraphSpacingAfter: Int? = nil,
                                          paragraphSpacingBefore: Int? = nil, strikethrough: Bool? = nil, textAlignment: String? = nil,
                                          textColor: String? = nil, tracking: Float? = nil, verticalAlignment: String? = nil) {
    
    guard let style = styles[styleKey] else {
      XCTFail("missing \(styleKey) style")
      return
    }
    
    if let backgroundColor = backgroundColor {
      XCTAssertNotNil(style.backgroundColor)
      XCTAssertEqual(backgroundColor, style.backgroundColor!)
    }
    if let firstLineIndent = firstLineIndent {
      XCTAssertNotNil(style.firstLineIndent)
      XCTAssertEqual(firstLineIndent, style.firstLineIndent!)
    }
    if let fontFamily = fontFamily {
      XCTAssertNotNil(style.fontFamily)
      XCTAssertEqual(fontFamily, style.fontFamily!)
    }
    if let fontName = fontName {
      XCTAssertNotNil(style.fontName)
      XCTAssertEqual(fontName, style.fontName!)
    }
    if let fontSize = fontSize {
      XCTAssertNotNil(style.fontSize)
      XCTAssertEqual(fontSize, style.fontSize!)
    }
    if let fontStyle = fontStyle {
      XCTAssertNotNil(style.fontStyle)
      XCTAssertEqual(fontStyle, style.fontStyle!)
    }
    if let fontWeight = fontWeight {
      XCTAssertNotNil(style.fontWeight)
      XCTAssertEqual(fontWeight, style.fontWeight!)
    }
    if let fontWidth = fontWidth {
      XCTAssertNotNil(style.fontWidth)
      XCTAssertEqual(fontWidth, style.fontWidth!)
    }
    if let hangingPunctuation = hangingPunctuation {
      XCTAssertNotNil(style.hangingPunctuation)
      XCTAssertEqual(hangingPunctuation, style.hangingPunctuation!)
    }
    if let hyphenation = hyphenation {
      XCTAssertNotNil(style.hyphenation)
      XCTAssertEqual(hyphenation, style.hyphenation!)
    }
    if let lineHeight = lineHeight {
      XCTAssertNotNil(style.lineHeight)
      XCTAssertEqual(lineHeight, style.lineHeight!)
    }
    if let paragraphSpacingAfter = paragraphSpacingAfter {
      XCTAssertNotNil(style.paragraphSpacingAfter)
      XCTAssertEqual(paragraphSpacingAfter, style.paragraphSpacingAfter!)
    }
    if let paragraphSpacingBefore = paragraphSpacingBefore {
      XCTAssertNotNil(style.paragraphSpacingBefore)
      XCTAssertEqual(paragraphSpacingBefore, style.paragraphSpacingBefore)
    }
    if let strikethrough = strikethrough {
      XCTAssertNotNil(style.strikethrough)
      XCTAssertEqual(strikethrough, style.strikethrough!)
    }
    if let textAlignment = textAlignment {
      XCTAssertNotNil(style.textAlignment)
      XCTAssertEqual(textAlignment, style.textAlignment!)
    }
    if let textColor = textColor {
      XCTAssertNotNil(style.textColor)
      XCTAssertEqual(textColor, style.textColor!)
    }
    if let tracking = tracking {
      XCTAssertNotNil(style.tracking)
      XCTAssertEqual(tracking, style.tracking!)
    }
    if let verticalAlignment = verticalAlignment {
      XCTAssertNotNil(style.verticalAlignment)
      XCTAssertEqual(verticalAlignment, style.verticalAlignment!)
    }
  }
  
  private func validateComponentStyles(_ styles: [String: ANFStory.ComponentStyle], for key: String) {
    if key == yogaJournalKey {
      validateComponentStyle("quoteComponentStyle", styles: styles, strokeColor: "#777", strokeStyle: "solid",
                             strokeWidth: 2, bottom: false, right: false, top: false, left: nil)
    } else {
      // TODO: more types here, if we have multiple test files.
    }
  }
  
  private func validateComponentStyle(_ styleKey: String, styles: [String: ANFStory.ComponentStyle],
                                      strokeColor: String? = nil, strokeStyle: String? = nil, strokeWidth: Int? = nil,
                                      bottom: Bool? = nil, right: Bool? = nil, top: Bool? = nil, left: Bool? = nil) {
    
    guard let style = styles[styleKey] else {
      XCTFail("missing \(styleKey) style")
      return
    }
    
    let border = style.border
    if let strokeColor = strokeColor {
      let all = getAll(border)
      XCTAssertEqual(all.color, strokeColor)
    }
    if let strokeStyle = strokeStyle {
      let all = getAll(border)
      XCTAssertEqual(all.style, strokeStyle)
    }
    if let strokeWidth = strokeWidth {
      let all = getAll(border)
      XCTAssertEqual(all.width, strokeWidth)
    }
  }
  
  private func getAll(_ border: ANFStory.ComponentStyle.Border) -> ANFStory.ComponentStyle.Border.StrokeStyle {
    XCTAssertNotNil(border.all)
    return border.all!
  }
  
  private func validateComponentLayouts(_ layouts: [String: ANFStory.ComponentLayout], for key: String) {
    if key == yogaJournalKey {
      
      validateComponentLayout("attributionLayout", layouts, columnSpan: 7, columnStart: 0, marginTop: 0, marginBottom: 0)
      
      validateComponentLayout("bodyLayout", layouts,  columnSpan: 7, columnStart: 0, marginTop: 15, marginBottom: 15)
      
      validateComponentLayout("bylineLayout", layouts, columnSpan: 7, columnStart: 0, marginTop: 15, marginBottom: 15)
      
      validateComponentLayout("captionLayout", layouts, columnSpan: 7, columnStart:  0, marginTop: 0, marginBottom: 0)
     
      validateComponentLayout("citationLayout", layouts, columnSpan: 7, columnStart: 0, marginTop: 5, marginBottom: 5)
      
      validateComponentLayout("headerImageLayout", layouts, columnSpan: 7, columnStart: 0, ignoreDocumentMargin: true,
                              marginTop: 15, marginBottom: 15, minimumHeight: "40vh")
      
      validateComponentLayout("introLayout", layouts, columnSpan: 7, columnStart: 0, marginTop: 15, marginBottom: 15)
      
      validateComponentLayout("photoLayout", layouts, marginTop: 15, marginBottom: 15)
      
      validateComponentLayout("pullquoteLayout", layouts, columnSpan: 7, columnStart: 0, marginTop: 15, marginBottom: 15)
      
      validateComponentLayout("quoteLayout", layouts, columnSpan: 7, columnStart: 0, marginTop: 15, marginBottom: 15)
      
      validateComponentLayout("sectionTitleLayout", layouts, columnSpan: 7, columnStart: 0, marginTop: 15, marginBottom: 15)
      
      validateComponentLayout("titleLayout", layouts, columnSpan: 7, columnStart: 0, marginTop: 30, marginBottom: 10)
    } else {
      // TODO: more types here, if we have multiple test files.
    }
  }
  
  private func validateComponentLayout(_ layoutKey: String, _ layouts: [String: ANFStory.ComponentLayout],
                                       columnSpan: Int? = nil, columnStart: Int? = nil, ignoreDocumentMargin: Bool? = nil,
                                       marginTop: Int? = nil, marginBottom: Int? = nil, minimumHeight: String? = nil) {
    
    guard let layout = layouts[layoutKey] else {
      XCTFail("missing \(layoutKey) Layout")
      return
    }
    
    if let columnSpan = columnSpan {
      XCTAssertNotNil(layout.columnSpan)
      XCTAssertEqual(layout.columnSpan!, columnSpan)
    }
    if let columnStart = columnStart {
      XCTAssertNotNil(layout.columnStart)
      XCTAssertEqual(layout.columnStart!, columnStart)
    }
    if let ignoreDocumentMargin = ignoreDocumentMargin {
      XCTAssertNotNil(layout.ignoreDocumentMargin)
      XCTAssertEqual(layout.ignoreDocumentMargin!, ignoreDocumentMargin)
    }
    if let marginTop = marginTop {
      XCTAssertNotNil(layout.margin)
      let margin = layout.margin!
      XCTAssertEqual(margin.top, marginTop)
    }
    if let marginBottom = marginBottom {
      XCTAssertNotNil(layout.margin)
      let margin = layout.margin!
      XCTAssertEqual(margin.bottom, marginBottom)
    }
    if let minimumHeight = minimumHeight {
      XCTAssertNotNil(layout.minimumHeight)
      XCTAssertEqual(minimumHeight, layout.minimumHeight)
    }
  }
}
