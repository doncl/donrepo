//
//  HTMLToAttributedStringTests.swift
//  LoadRTFUnitTests
//
//  Created by Don Clore on 6/5/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import XCTest
@testable import LoadRTF

class HTMLToAttributedStringTests: XCTestCase {
  
  let airBNBBody: String = """
  <p>Stay with friends, or at an AirBnB, or <a href=\"https://en.wikipedia.org/wiki/Casa_particular\">Casa Particular</a> \
  \u{2014}someone\u{2019}s house rented out to visitors. All hotels are government-owned. And eat at non-state-owned restaurants,\
   called <em>Paladeras</em>.\n</p>
"""
  
  let passionForLife: String = """
  <p>My passion for life undoubtedly comes from my Cuban roots. My mother fled her home country of \
  <a href=\"https://en.wikipedia.org/wiki/Cuba\">Cuba </a>at the age of nine, with her parents, three siblings, \
  grandmother, dog, and $35. They left behind family who still live in Cuba today. There\u{2019}s a spark and creative fire \
  that radiates in the Cuban culture. So it\u{2019}s unfortunate that Cuba is a controversial country to visit as a traveler. \
  The Cuban people have been, and continue to be, in my opinion, exploited and abused by their government\u{2014}through \
  injustices such as incarceration without a fair trial and lack of a free press. Because of this, there are mixed feelings \
  about when and if it is appropriate to visit this both beautiful and bleeding country.\n</p><p>In October of 2015, I was \
  approached by yoga photographer <a href=\"https://www.yogajournal.com/people/robert-sturman\">Robert Sturman</a> to go to Cuba \
  for what proved to be an epic and powerful photo shoot. I had to consider my highest ideal behind going. I didn\u{2019}t want \
  to go for selfish reasons, which included indulging my senses with dancing, eating, drinking, and relaxing on the beach; \
  snapping instagrammable pictures; or going to \u{201c}experience\u{201d} Cuba as a tourist. As a practicing yogi, that \
  wouldn\u{2019}t be ideal for me . Instead, I wanted my visit to be for a greater purpose\u{2014}to indirectly and directly \
  serve Cuban people, instead of supporting a government that tore families apart. It\u{2019}s important to consider that \
  serving is different than \u{201c}helping,\u{201d} because helping implies some kind of superiority on the part of the helper. \
  The last thing Cubans need is outsiders coming in to tell them what they need or how to do things.\n</p><p><em>See also\
  </em> <a href=\"https://www.yogajournal.com/livebeyoga/robert-sturmans-3-tips-amazing-yoga-photos\">\
  Robert Sturman's 3 Tips for Amazing Yoga Photos</a></p><p>Instead, I chose to practice \
  <a href=\"https://www.yogajournal.com/yoga-101/do-yoga-do-good\">Karma Yoga</a>, or the path of selfless service, and \
  <a href=\"https://www.yogajournal.com/lifestyle/bhakti-yoga-photo-essay-andy-richter\">Bhakti Yoga</a>\u{2013}\
  the path of devotion\u{2014}during my trip with Robert. This meant I brought supplies and resources for my relatives and for \
  the yoga community. I arrived with cash, my wedding dress, \
  <a href=\"https://en.wikipedia.org/wiki/Havaianas\">Havaianas flip flops</a>, toys, and more clothes. It was my way of serving \
  a community that had given me so much.\n</p><p><em>See also\
  </em> <a href=\"https://www.yogajournal.com/lifestyle/gooders\">Karma Yoga: How A Practice Brings People Closer Together</a></p>
"""
  
  let bodyTextStyle = ANFStory.ComponentTextStyle(backgroundColor: nil, dropCapStyle: nil, firstLineIndent: nil, fontFamily: nil,
                                              fontName: "Georgia", fontSize: 18, fontStyle: nil, fontWeight: nil, fontWidth: nil,
                                              hangingPunctuation: nil, hyphenation: nil, lineHeight: 26, linkStyle: nil,
                                              orderedListItems: nil, paragraphSpacingAfter: nil, paragraphSpacingBefore: nil,
                                              strikethrough: nil, stroke: nil, textAlignment: "left", textColor: "#333",
                                              textShadow: nil, tracking: nil, underline: nil, unorderedListItems: nil, verticalAlignment: nil)

  let byLineTextStyle = ANFStory.ComponentTextStyle(backgroundColor: nil, dropCapStyle: nil, firstLineIndent: nil, fontFamily: nil,
                                              fontName: "HelveticaNeue", fontSize: 16, fontStyle: nil, fontWeight: nil, fontWidth: nil,
                                              hangingPunctuation: nil, hyphenation: nil, lineHeight: nil, linkStyle: nil,
                                              orderedListItems: nil, paragraphSpacingAfter: nil, paragraphSpacingBefore: nil,
                                              strikethrough: nil, stroke: nil, textAlignment: "left", textColor: "#000",
                                              textShadow: nil, tracking: nil, underline: nil, unorderedListItems: nil, verticalAlignment: nil)


  func testRitasByline() {
    
    guard let attrString = ANFStory.attributedString(from: "<strong>Rina Jakubowicz</strong><br>May 31, 2019", textStyle: byLineTextStyle, scalingFactor: 1.0) else {
      XCTFail("nil attributed string returned")
      return
    }
    print(attrString)
  }
  
  func testAirBNBBody() {
    
    guard let attrString = ANFStory.attributedString(from: airBNBBody, textStyle: bodyTextStyle, scalingFactor: 1.0) else {
      XCTFail("nil attributed string returned")
      return
    }
    print(attrString)

  }
  
  func testPassionForLife() {
    guard let attrString = ANFStory.attributedString(from: passionForLife, textStyle: bodyTextStyle, scalingFactor: 1.0) else {
      XCTFail("nil attributed string returned")
      return
    }
    print(attrString)
  }
  
  func testEncodedAnchor() {
    guard let attrString = ANFStory.attributedString(from: "<strong>Rina Jakubowicz\u{2019}s byline</strong><br>May 31, 2019",
                                                    textStyle: byLineTextStyle , scalingFactor: 1.0) else {
                                                      
      XCTFail("nil attributed string returned")
      return
    }
    print(attrString)
  }
}
