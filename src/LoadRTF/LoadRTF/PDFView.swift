//
//  PDFView.swift
//  LoadRTF
//
//  Created by Don Clore on 5/31/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import UIKit
import CoreGraphics

class PDFView: UIView {

  var pdfURL: URL?
  
  override func draw(_ rect: CGRect) {
    let start = Date()
    guard let pdfURL = pdfURL, let ctx = UIGraphicsGetCurrentContext() else {
      return 
    }
    
    // PDF might be transparent, assume white paper
    UIColor.white.setFill()
    ctx.fill(rect)
    
    // flip coordinates
    
    ctx.scaleBy(x: 1, y: -1)
    ctx.translateBy(x: 0, y: -rect.height)
    
    // url is a file URL
    guard let pdf: CGPDFDocument = CGPDFDocument(pdfURL as CFURL) else {
      return
    }
    guard let page1 = pdf.page(at: 1) else {
      return
    }
    
    // Get the rectangle of the cropped inside
    let mediaRect: CGRect = page1.getBoxRect(CGPDFBox.cropBox)
    ctx.scaleBy(x: rect.width / mediaRect.width, y: rect.height / mediaRect.height)
    ctx.translateBy(x: -mediaRect.origin.x, y: -mediaRect.origin.y)
    
    // draw it
    ctx.drawPDFPage(page1)
    let end = Date().timeIntervalSince(start) * 1000.0
    
    print("spent \(end) ms. drawing pdf page")
  }
}

