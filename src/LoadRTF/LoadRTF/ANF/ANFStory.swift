//
//  ANFStory.swift
//  LoadRTF
//
//  Created by Don Clore on 5/31/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import Foundation
import UIKit

struct ANFStory: Codable, JSONCoding {
  
  //https://developer.apple.com/documentation/apple_news/textstyle
  struct Shadow: Codable, JSONCoding {
    typealias codableType = Shadow
    
    struct Offset: Codable, JSONCoding {
      typealias codableType = Offset
      
      let x: Float
      let y: Float
    }
    
    // Required
    let color: String
    let radius: Int
    
    // Optional
    var offset: Offset?
    var opacity: Float?
  }
  
  //https://developer.apple.com/documentation/apple_news/listitemstyle
  struct ListItemStyle: Codable & JSONCoding {
    typealias codableType = ListItemStyle
    
    var character: String?
    let type: String  // TODO:  Make an enum, maybe, talk with BenT.
    // Possible values: bullet, decimal, lower_roman, upper_roman, lower_alphabetical, upper_alphabetical, character, none
  }
  
  struct TextStrokeStyle: Codable & JSONCoding {
    typealias codableType = TextStrokeStyle
    
    let color: String
    let width: Int
  }
  
  struct TextStyle: Codable & JSONCoding {
    typealias codableType = TextStyle
    
    // default to transparent?
    var backgroundColor: String? = "transparent"
    var fontFamily: String?
    var fontName: String?
    var fontSize: Int?
    var fontStyle: String?
    var fontWeight: String?
    var fontWidth: String?
    var strikethrough: Bool?
    // N.B. ANF spec does not have 'textAlignment'
    var textAlignment: String?
    var underline: Bool?
    var verticalAlignment: String?
    var textColor: String?
    var textShadow: Shadow?
    var unorderedListItems: ListItemStyle?
    var tracking: Float?
    
    // not implemented: textTransform, conditional, unorderedListItems, tracking,
  }
  
  struct ComponentTextStyle: Codable & JSONCoding {
    typealias codableType = ComponentTextStyle
    
    struct DropCapStyle: Codable & JSONCoding {
      typealias codableType = DropCapStyle
      
      let numberOfLines: Int
      var backgroundColor: String? = "transparent"
      var fontName: String?
      var numberOfCharacters: Int? = 1
      var numberOfRaisedLines: Int?
      var padding: Int? = 0
      var textColor: String?
    }
    
    var backgroundColor: String?
    var dropCapStyle: DropCapStyle?
    var firstLineIndent: Int?
    var fontFamily: String?
    var fontName: String?
    var fontSize: Int?
    var fontStyle: String? 
    var fontWeight: String?
    var fontWidth: String?
    var hangingPunctuation: Bool? = false
    var hyphenation: Bool? = false
    var lineHeight: Int?
    var linkStyle: TextStyle?
    var orderedListItems: ListItemStyle?
    var paragraphSpacingAfter: Int?
    var paragraphSpacingBefore: Int?
    var strikethrough: Bool?
    var stroke: TextStrokeStyle?
    var textAlignment: String? // left, center, right, justified, none - TODO: Enum?
    var textColor: String? 
    var textShadow: Shadow?
    var tracking: Float?
    var underline: Bool?
    var unorderedListItems: ListItemStyle?
    var verticalAlignment: String? // TODO:  Enum? superscript, subscript, baseline
    // not implemented contional
    
    
    func getFont(textStyleKey: String, isRegular: Bool) -> UIFont? {
      var finalFontSize: CGFloat = 16
      var attrs: [UIFontDescriptor.AttributeName: Any] = [:]
      
      if let fontFamily = fontFamily {
        attrs[UIFontDescriptor.AttributeName.family] = fontFamily
      }
      if let fontName = fontName {
        attrs[UIFontDescriptor.AttributeName.name] = fontName
      }
      if let fontSize = fontSize {
        let scale: CGFloat
        if textStyleKey.lowercased() == "titlestyle" {
          scale = isRegular ? 1.0 : 0.61
        } else if isRegular {
          scale = 1.0
        } else {
          scale = 1.0
        }
        finalFontSize = CGFloat(fontSize) * scale
        attrs[UIFontDescriptor.AttributeName.size] = finalFontSize
      }
      let fd: UIFontDescriptor = UIFontDescriptor(fontAttributes: attrs)
      let weight = getFontWeight()
      let traits = [UIFontDescriptor.TraitKey.weight: weight]
      let fdWithWeight = fd.addingAttributes([UIFontDescriptor.AttributeName.traits : traits])
      return UIFont(descriptor: fdWithWeight, size: finalFontSize)
    }
    
    private func getFontWeight() -> UIFont.Weight {
      guard let fontWeightString = fontWeight else {
        return UIFont.Weight.regular
      }
      switch fontWeightString {
      case "thin":
        return UIFont.Weight.thin
      case  "extra-light", "ultra-light":
        return UIFont.Weight.ultraLight
      case "light":
        return UIFont.Weight.light
      case "regular", "normal", "book", "roman", "medium":
        return UIFont.Weight.regular
      case "semi-bold", "demi-bold":
        return UIFont.Weight.semibold
      case "bold":
        return UIFont.Weight.bold
      case "extra-bold", "ultra-bold", "black", "heavy":
        return UIFont.Weight.heavy
      case "bolder":
        return UIFont.Weight.bold
      default:
        return UIFont.Weight.regular
      }
    }
  }
  
  struct APIMetadata: Codable & JSONCoding {
        
      typealias codableType = APIMetadata
        
      var isCandidateToBeFeatured: Bool
      var isHidden: Bool
      var isPreview: Bool
      var maturityRating: String
  }
  
  // https://developer.apple.com/documentation/apple_news/articledocument
  struct ArticleJSON: Codable, JSONCoding {
    typealias codableType = ArticleJSON
    
    static let defaultFontSize: CGFloat = 16.0
    static let defaultFontName: String = "HelveticaNeue"
    
    struct Component: Codable, JSONCoding {
      enum ComponentType {
        case unknown
        case html
        case text
        case photo
      }
      typealias codableType = Component
      
      enum CodingKeys: String, CodingKey {
        case role
        case layout
        case text
        case textStyle
        case url = "URL"
      }

      let role: String
      var layout: String?
      var format: String?
      var text: String?
      var textStyle: String?
      var url: URL?
      
      var type: ComponentType {
        if let format = format, format == "html" {
          return ComponentType.html
        }
        if let text = text, !text.isEmpty {
          return ComponentType.text
        }
        if role == "photo" {
          return ComponentType.photo
        }
        return ComponentType.unknown
      }
    }
    
    struct Layout: Codable, JSONCoding {
      typealias codableType = Layout
      let columns: Int
      let gutter: Int
      let margin: Int
      let width: Int
    }
    
    struct Metadata: Codable, JSONCoding {
      typealias codableType = Metadata
      var authors: [String] = []
      var canonicalURL: URL?
      let dateCreated: Date
      var dateModified: Date?
      var datePublished: Date?
      var excerpt: String?
      var thumbnailURL: URL?
    }
    
    struct DocumentStyle: Codable, JSONCoding {
      typealias codableType = DocumentStyle
      
      let color: String
    }

    // MARK: Required things.
    let components: [Component]
    let componentTextStyles: [String : ComponentTextStyle]
    let identifier: String
    let language: String
    let layout: Layout
    let title: String
    let version: String

    // MARK: Optional things
    var componentLayouts: [String : ComponentLayout]?
    var componentStyles: [String : ComponentStyle]?
    var documentStyle: DocumentStyle?
    var metadata: Metadata?
    var subtitle: String?

    var textStyles: [String : TextStyle]?    
  }
  
  struct ComponentLayout: Codable, JSONCoding {
    typealias codableType = ComponentLayout
    
    struct Margin: Codable, JSONCoding {
      typealias codableType = Margin
      
      var bottom: Int
      var top: Int
    }
    
    var columnSpan: Int?
    var columnStart: Int?
    var ignoreDocumentMargin: Bool? 
    var margin: Margin?
    var minimumHeight: String?
  }
  
  struct ComponentStyle: Codable, JSONCoding {
      typealias codableType = ComponentStyle
      
      struct Border: Codable, JSONCoding {
        typealias codableType = Border
        
        struct StrokeStyle: Codable, JSONCoding {
          typealias codableType = StrokeStyle
          
          var color: String?
          var style: String?
          var width: Int?
        }
        
        var all: StrokeStyle?
        var bottom: Bool?
        var right: Bool?
        var top: Bool?
      }
      
    var border: Border
  }
  
    
  typealias codableType = ANFStory
    
  var apiMetadata: APIMetadata
  var articleJson: ArticleJSON
}


