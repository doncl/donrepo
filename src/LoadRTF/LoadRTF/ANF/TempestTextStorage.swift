//
//  TempestTextStorage.swift
//  LoadRTF
//
//  Created by Don Clore on 6/4/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import UIKit

class TempestTextStorage: NSTextStorage {
  var backingStore = NSMutableAttributedString()
 
  override var string: String {
    return backingStore.string
  }
}

// MARK:  TextKit stuff
extension TempestTextStorage {
  override func attributes(at location: Int, effectiveRange range: NSRangePointer?) -> [NSAttributedString.Key : Any] {
    return backingStore.attributes(at: location, effectiveRange: range)
  }
  
  override func replaceCharacters(in range: NSRange, with str: String) {
    beginEditing()
    
    backingStore.replaceCharacters(in: range, with: str)
    
    let changeInLength = str.count - range.length
    
    // Notify the parent class that stuff got modified.
    edited([.editedAttributes, .editedCharacters], range: range,
           changeInLength: changeInLength)
    
    endEditing()
  }

  override func setAttributes(_ attrs: [NSAttributedString.Key : Any]?, range: NSRange) {
    beginEditing()
    
    backingStore.setAttributes(attrs, range: range)
    edited([.editedAttributes], range: range, changeInLength: 0)
    
    endEditing()
  }
}
