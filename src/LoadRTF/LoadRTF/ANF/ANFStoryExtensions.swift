//
//  ANFStoryExtensions.swift
//  LoadRTF
//
//  Created by Don Clore on 6/4/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import Foundation
import UIKit
import SwiftSoup

extension ANFStory.ArticleJSON {
  // MARK: Main entry point.
  func attributedString(for component: ANFStory.ArticleJSON.Component, isRegular: Bool) -> NSMutableAttributedString? {
    guard let layoutKey = component.layout,
      let text = component.text,
      let componentLayouts = componentLayouts,
      let componentLayout = componentLayouts[layoutKey],
      let textStyleKey = component.textStyle,
      let textStyle = componentTextStyles[textStyleKey] else {
        return nil
    }

    let ret: NSMutableAttributedString = NSMutableAttributedString(string: "\n")
    switch component.type {
    case .unknown:
      return nil
    case .html, .text:
      guard let attrString = ANFStory.attributedString(from: text, textStyleKey: textStyleKey, textStyle: textStyle,
                                                       isRegular: isRegular) else {
                                                        return nil
      }
      ret.append(attrString)
    case .photo:
      return nil
    }
    
    ret.append(NSAttributedString(string:"\n"))
    return ret
  }
  
  static func getParagraphStyle(from textStyle: ANFStory.ComponentTextStyle) -> NSMutableParagraphStyle {
    let para: NSMutableParagraphStyle = NSMutableParagraphStyle()
    if let alignment = alignment(from: textStyle.textAlignment) {
      para.alignment = alignment
    }
    return para
  }
  
  private func getColor(from textStyle: ANFStory.ComponentTextStyle) -> UIColor? {
    guard let textColor = textStyle.textColor else {
      return nil
    }
    return textColor.colorFromCSS3Digit
  }
  
  private func getFont(for fontName: String, using scaledSize: CGFloat) -> UIFont? {
    let subs = fontName.split(separator: "-")
    
    guard subs.count > 1 else {
      return UIFont(name: fontName, size: scaledSize)
    }
    
    let secondSubstring = String(subs[1])
    let firstSubstring = String(subs[0])
    let fd: UIFontDescriptor = UIFontDescriptor(name: firstSubstring, size: scaledSize)
    switch secondSubstring.lowercased() {
    case "bold":
      guard let weightedFontDescriptor = fd.withSymbolicTraits(UIFontDescriptor.SymbolicTraits.traitBold) else {
        return UIFont(name: firstSubstring, size: scaledSize)
      }
      return UIFont(descriptor: weightedFontDescriptor, size: scaledSize)
    case "medium":
      return UIFont(name: firstSubstring, size: scaledSize)
    case "italic":
      guard let weightedFontDescriptor = fd.withSymbolicTraits(UIFontDescriptor.SymbolicTraits.traitItalic) else {
        return UIFont(name: firstSubstring, size: scaledSize)
      }
      return UIFont(descriptor: weightedFontDescriptor, size: scaledSize)
    default:
      return UIFont(name: firstSubstring, size: scaledSize)
    }
  }
  
  private static func alignment(from string: String?) -> NSTextAlignment? {
    guard let string = string else {
      return nil
    }
    switch string {
    case "left":
      return NSTextAlignment.left
    case "right":
      return NSTextAlignment.right
    case "justified":
      return NSTextAlignment.justified
    case "natural":
      return NSTextAlignment.natural
    default:
      return nil
    }
  }
}

extension ANFStory {
  static func attributedString(from html: String, textStyleKey: String, textStyle: ComponentTextStyle,
                               isRegular: Bool) -> NSMutableAttributedString? {
    do {
      let doc: SwiftSoup.Document = try SwiftSoup.parseBodyFragment(html)
      guard let body: SwiftSoup.Element = doc.body() else {
        return nil
      }
      
      let attrString = traverseTree(body, textStyleKey: textStyleKey, textStyle: textStyle, isRegular: isRegular)
      return attrString
    } catch Exception.Error(let type, let message) {
      print("error parsing, error type = \(type), message = \(message)")
      return nil
    } catch (let error) {
      print("error parsing message = \(error.localizedDescription)")
      return nil
    }
  }
  
  fileprivate static func traverseChildNodes(_ node: Node, textStyleKey: String, textStyle: ANFStory.ComponentTextStyle,
                                             ns: NSMutableAttributedString, isRegular: Bool) {
    let kids = node.getChildNodes()
    for kid in kids {
      let attrStringSubTree = traverseTree(kid, textStyleKey: textStyleKey, textStyle: textStyle, isRegular: isRegular)
      ns.append(attrStringSubTree)
    }
  }
  
  static private func traverseTree(_ node: SwiftSoup.Node, textStyleKey: String, textStyle: ComponentTextStyle,
                                   isRegular: Bool) -> NSMutableAttributedString {
    
    let ns = NSMutableAttributedString()
    let attrs: [NSAttributedString.Key: Any] = getAttrs(for: textStyle, textStyleKey: textStyleKey, isRegular: isRegular)
    
    if let textNode = node as? TextNode {
      let text = textNode.text()
      ns.append(NSAttributedString(string: text, attributes: attrs))
    } else if let element = node as? Element {
      let tag = element.tagName()
      //print(tag)
      switch tag {
      case "strong":
        handleStrongElement(element, textStyleKey: textStyleKey, textStyle: textStyle, ns: ns, isRegular: isRegular)
        return ns
      case "br":
        let newline = NSAttributedString(string: "\n")
        ns.append(newline)
        break
      case "a":
        ns.append(handleAnchor(element, textStyleKey: textStyleKey, textStyle: textStyle, isRegular: isRegular))
        return ns
      case "em":
        handleEmElement(element, textStyleKey: textStyleKey, textStyle: textStyle, ns: ns, isRegular: isRegular)
        return ns
      case "p":
        ns.append(handlePElement(element, textStyleKey: textStyleKey, textStyle:textStyle, ns: ns, isRegular: isRegular))
        return ns
      default:
        break
      }
    }
    traverseChildNodes(node, textStyleKey: textStyleKey, textStyle:textStyle, ns: ns, isRegular: isRegular)
    return ns
  }
  
  static private func getAttrs(for textStyle: ComponentTextStyle, textStyleKey: String, isRegular: Bool) -> [NSAttributedString.Key: Any] {
    
    var attrs: [NSAttributedString.Key: Any] = [:]
    let para: NSMutableParagraphStyle = ANFStory.ArticleJSON.getParagraphStyle(from: textStyle)
    
    if let font = textStyle.getFont(textStyleKey: textStyleKey, isRegular: isRegular) {
      attrs[NSAttributedString.Key.font] = font
      let fontHeight = font.pointSize
      para.minimumLineHeight = fontHeight
      para.maximumLineHeight = fontHeight
      para.lineSpacing = getLineSpacing(for: textStyle, fontHeight: fontHeight, textStyleKey: textStyleKey, isRegular: isRegular)
      if textStyleKey.lowercased() != "bylinestyle" {
        para.paragraphSpacing = isRegular ? CGFloat(26.0) : CGFloat(16.0)
      }      
      attrs[NSAttributedString.Key.paragraphStyle] = para
    }
    if let color = textStyle.backgroundColor, let backgroundColor = color.colorFromCSS3Digit {
      attrs[NSAttributedString.Key.backgroundColor] = backgroundColor
    }
    if let color = textStyle.textColor, let textColor = color.colorFromCSS3Digit {
      attrs[NSAttributedString.Key.foregroundColor] = textColor
    }
    return attrs
  }
  
  static private func getLineSpacing(for textStyle: ComponentTextStyle, fontHeight: CGFloat, textStyleKey: String, isRegular: Bool) -> CGFloat {
  
    switch textStyleKey.lowercased() {
    case "titlestyle":
      return fontHeight * 0.2
    case "introstyle":
      return fontHeight * 0.18
    default:
      break
    }
    
    if let textStyleLineHeight = textStyle.lineHeight {
      return (CGFloat(textStyleLineHeight) - fontHeight)
    }
    
    return fontHeight * 0.4
  }
  
  static private func handlePElement(_ pElement: Element, textStyleKey: String, textStyle: ComponentTextStyle,
                                     ns: NSMutableAttributedString, isRegular: Bool) -> NSMutableAttributedString {
    
    let surroundedByNewLines: NSMutableAttributedString = NSMutableAttributedString(string: "\n")
    traverseChildNodes(pElement, textStyleKey: textStyleKey, textStyle: textStyle, ns: ns, isRegular: isRegular)
    surroundedByNewLines.append(ns)
    surroundedByNewLines.append(NSAttributedString(string: "\n"))
    return surroundedByNewLines
  }
  
  static private func handleEmElement(_ emElement: Element, textStyleKey: String, textStyle: ComponentTextStyle, ns: NSMutableAttributedString,
                                      isRegular: Bool) {
    let finalItalicFont: UIFont
    if let font = textStyle.getFont(textStyleKey: textStyleKey, isRegular: isRegular),
      let size = textStyle.fontSize,
      let fd = font.fontDescriptor.withSymbolicTraits(UIFontDescriptor.SymbolicTraits.traitItalic) {
      let italicFont = UIFont(descriptor: fd, size: CGFloat(size))
      finalItalicFont = italicFont
    } else {
      finalItalicFont = UIFont.italicSystemFont(ofSize: ANFStory.ArticleJSON.defaultFontSize)
    }
    
    traverseChildNodes(emElement, textStyleKey: textStyleKey, textStyle: textStyle, ns:ns, isRegular: isRegular)
    ns.addAttribute(NSAttributedString.Key.font, value: finalItalicFont, range: NSRange(location: 0, length: ns.length))
  }
  
  static private func handleStrongElement(_ strongElement: Element, textStyleKey: String, textStyle: ComponentTextStyle,
                                          ns: NSMutableAttributedString, isRegular: Bool) {
    let finalBoldFont: UIFont
    if let font = textStyle.getFont(textStyleKey: textStyleKey, isRegular: isRegular),
      let size = textStyle.fontSize,
      let fd = font.fontDescriptor.withSymbolicTraits(UIFontDescriptor.SymbolicTraits.traitBold) {
      let boldFont = UIFont(descriptor: fd, size: CGFloat(size))
      finalBoldFont = boldFont
    } else {
        finalBoldFont = UIFont.boldSystemFont(ofSize: ANFStory.ArticleJSON.defaultFontSize)
    }
    
    traverseChildNodes(strongElement, textStyleKey: textStyleKey, textStyle: textStyle, ns: ns, isRegular: isRegular)
    
    ns.addAttribute(NSAttributedString.Key.font, value: finalBoldFont, range: NSRange(location: 0, length: ns.length))
  }
  
  static private func handleAnchor(_ anchor: Element, textStyleKey: String, textStyle: ComponentTextStyle, isRegular: Bool) -> NSAttributedString {
    let ns: NSAttributedString
    let font: UIFont
    if let styleFont = textStyle.getFont(textStyleKey: textStyleKey, isRegular: isRegular) {
      font = styleFont
    } else {
      if let defFont = UIFont(name: ANFStory.ArticleJSON.defaultFontName, size: ANFStory.ArticleJSON.defaultFontSize) {
        font = defFont
      } else {
        font = UIFont.systemFont(ofSize: ANFStory.ArticleJSON.defaultFontSize)
      }
    }
    
    do {
      var anchorName = try anchor.text()
      // So, anchor.text() will trim and do other processing, but if it's possible, try to get the text out of the first child.
      // this is the textnode that actually has the text - one of the yoga journal stories has a trailing space inside it that
      // must be preserved; it's part of the content.   TODO:  Figure out encoding issues.
      if let firstChild = anchor.getChildNodes().first, let textNode = firstChild as? TextNode {
          anchorName = textNode.getWholeText()
      }
      var attrs: [NSAttributedString.Key: Any] = [NSMutableAttributedString.Key.font: font]

      if let urlString = getAnchorUrlString(anchor), let url = URL(string: urlString) {
        attrs[NSAttributedString.Key.foregroundColor] = UIColor.blue
        // TODO: Add some kind of link - stick the urlString into the attribute.
      }
      
      ns = NSAttributedString(string: anchorName, attributes: attrs)
    } catch (let error) {
      print(error.localizedDescription)
      ns = NSAttributedString(string: "")
    }
    return ns
  }
  
  static private func getAnchorUrlString(_ anchor: Element) -> String? {
    do {
      return try anchor.attr("href")
    } catch (let error) {
      print(error.localizedDescription)
      return nil
    }
  }
}

extension String {
  var colorFromCSS3Digit: UIColor? {
    guard self.hasPrefix("#"), self.count == 4 else {
      return nil
    }
    let start = self.index(startIndex, offsetBy: 1)
    let css3DigitColorString = String(self[start...])
    let startIndex = css3DigitColorString.startIndex
    let first = css3DigitColorString[startIndex]
    let second = css3DigitColorString[css3DigitColorString.index(startIndex, offsetBy: 1)]
    let third = css3DigitColorString[css3DigitColorString.index(startIndex, offsetBy: 2)]
    let sixDigitString = "\(first)\(first)\(second)\(second)\(third)\(third)"
    let scanner = Scanner(string: sixDigitString)
    scanner.scanLocation = 0
    
    var rgbValue: UInt64 = 0
    
    scanner.scanHexInt64(&rgbValue)
    
    let r = (rgbValue & 0xff0000) >> 16
    let g = (rgbValue & 0xff00) >> 8
    let b = rgbValue & 0xff
    
    return UIColor(
      red: CGFloat(r) / 0xff,
      green: CGFloat(g) / 0xff,
      blue: CGFloat(b) / 0xff, alpha: 1
    )
  }
}
