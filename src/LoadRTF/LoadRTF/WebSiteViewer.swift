//
//  WebSiteViewer.swift
//  LoadRTF
//
//  Created by Don Clore on 5/31/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import UIKit
import WebKit

class WebSiteViewer: UIViewController {
  @IBOutlet var webView: WKWebView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    guard let url = URL(string: "https://www.yogajournal.com/lifestyle/cuba-taught-me-about-selflessness") else { return }
    let req = URLRequest(url: url)
    webView.load(req)
  }
}
