//
//  JSONCoding.swift
//  Duwamish
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 theMaven Network. All rights reserved.
//

import Foundation

public protocol JSONCoding {
    associatedtype codableType : Codable
    
    static func jsonEncodeToString(_ t : codableType, success: (String) -> (), failure: (String) -> ())
    static func jsonEncodeToData(_ t: codableType, success: (Data) -> (), failure: (String) -> ())
    static func jsonDecode(_ data: Data, success: (codableType) -> (), failure: (String) -> ())
}

public extension JSONCoding {
    static func jsonDecode(_ data: Data, success: (codableType) -> (), failure: (String) -> ()) {
        do {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .custom({ (decoder : Decoder) -> Date in
                let container = try decoder.singleValueContainer()
                let dateStr = try container.decode(String.self)
                guard let d = DateFormatter.tryAllCustomDateFormats(dateString: dateStr) else {
                    throw DecodingError.dataCorruptedError(in: container,
                                                           debugDescription: "\(dateStr) not parseable as date")
                }
                return d
            })
            let t = try decoder.decode(codableType.self, from: data)
            success(t)
            return
        } catch DecodingError.dataCorrupted(let context) {
            failure("Data corrupted, context = \(context)")
            return
        } catch DecodingError.keyNotFound(let codingKey, let context) {
            failure("\(codingKey) missing, context = \(context)")
            return
        } catch DecodingError.typeMismatch(let type, let context) {
            failure("type mismatch, type = \(type), context = \(context)")
            return
        } catch DecodingError.valueNotFound(let type, let context) {
            failure("type mismatch type = \(type), context = \(context)")
            return
        } catch {
            failure("unknown error")
            return
        }
    }
    
    static func jsonEncodeToString(_ t : codableType, success: (String) -> (),
                                   failure: (String) -> ()) {
        
        jsonEncodeToData(t, success: { (data : Data) in
            guard let jsonString = String(data: data, encoding: .utf8) else {
                failure("Couldn't generate json string from Data")
                return
            }
            success(jsonString)
        }, failure: { errString in
            failure(errString)
        })
    }
    
    static func jsonEncodeToData(_ t : codableType, success: (Data) -> (),
                                 failure: (String) -> ()) {
        
        let encoder = JSONEncoder()
        if #available(iOS 11.0, *) {
            encoder.outputFormatting = [.prettyPrinted, .sortedKeys]
        } else {
            // Fallback on earlier versions
            encoder.outputFormatting = [.prettyPrinted]
        }
        encoder.dateEncodingStrategy = .iso8601
        guard let data = try? encoder.encode(t) else {
            failure("Couldn't encode object \(t)")
            return
        }
        success(data)
    }
}

public extension DateFormatter {
    static let iso8601Full: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
    
    static let iso8601Shorter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
    
    static let iso8601Shortest: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
    
    // 0001-01-01T00:00:00
    static let emptyDate : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
    
    private static var formats : [DateFormatter] = [iso8601Full, iso8601Shorter, iso8601Shortest,
                                                    emptyDate]
    
    static func tryAllCustomDateFormats(dateString: String) -> Date? {
        for format : DateFormatter in formats {
            if let d = format.date(from: dateString) {
                return d
            }
        }
        return nil
    }
}

extension Array : JSONCoding where Element : Codable {
    public typealias codableType = Array
}

public extension JSONEncoder {
    func encodeJSONObject<T: Encodable>(_ value: T,
                                        options opt: JSONSerialization.ReadingOptions = []) throws -> Any {
        
        let data = try encode(value)
        return try JSONSerialization.jsonObject(with: data, options: opt)
    }
}

public extension JSONDecoder {
    func decode<T: Decodable>(_ type: T.Type, withJSONObject object: Any,
                              options opt: JSONSerialization.WritingOptions = []) throws -> T {
        let data = try JSONSerialization.data(withJSONObject: object, options: opt)
        return try decode(T.self, from: data)
    }
}
