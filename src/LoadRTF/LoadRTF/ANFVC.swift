//
//  ANFVC.swift
//  LoadRTF
//
//  Created by Don Clore on 6/3/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import UIKit
import SwiftSoup

class ANFVC: UIViewController {
  var story: ANFStory?
  var layoutManager: NSLayoutManager = NSLayoutManager()
  var textView: UITextView = UITextView()
  var storage: TempestTextStorage = TempestTextStorage()
  let compactTopInsetValue: CGFloat = 32.0
  let regularTopInsetValue: CGFloat = 32.0
  let compactHorzInsetValue: CGFloat = 24.0
  
  lazy var insets: UIEdgeInsets = {
    let topInset: CGFloat
    let horzInset: CGFloat
    
    if traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
      topInset = regularTopInsetValue
      horzInset = view.bounds.width * 0.15
    } else {
      topInset = compactTopInsetValue
      horzInset = compactHorzInsetValue
    }
    let insets: UIEdgeInsets = UIEdgeInsets(top: topInset, left: horzInset, bottom: 0, right: horzInset)
    return insets
  }()

  override func viewDidLoad() {
    super.viewDidLoad()
    
    let bundle = Bundle(for: type(of: self))
    guard let url = bundle.url(forResource: "yogajournal", withExtension: "anf") else {
      fatalError("Missing resource")
    }

    var storyData: Data?
    do {
      storyData = try Data(contentsOf: url)
    } catch let error {
      fatalError("Error converting yogajournal to data = \(error.localizedDescription)")
    }

    guard let dataForStory = storyData else {
      fatalError("something went wrong.")
    }
    
    ANFStory.jsonDecode(dataForStory, success: { parsedStory in
      self.story = parsedStory
    }, failure: { error in
      fatalError(error)
    })
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupTextKitStack(width: view.bounds.width)
    layoutContent()
    navigationController?.navigationBar.isTranslucent = true
    applyTransparentBackgroundToTheNavigationBar(0.0)
    DispatchQueue.main.async {
      self.textView.setContentOffset(.zero, animated: false)
    }

  }
}

// MARK: TextKit setup
extension ANFVC {
  private func setupTextKitStack(width: CGFloat) {
    // create brand new goodies.
    layoutManager = NSLayoutManager()
    storage = TempestTextStorage()

    // Figure out what size the textcontainer should be.   Make it the width of the rect passed
    // in, and no limit on the height.
    let containerSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
    
    // Create an NSTextContainer using that size.
    let container = NSTextContainer(size: containerSize)
    container.widthTracksTextView = true
    
    // Link the container to the LayoutManaager
    layoutManager.addTextContainer(container)
    
    // Now link the layoutManager to the textStorage
    storage.addLayoutManager(layoutManager)
    storage.delegate = self
    
    textView.removeFromSuperview()
    // Now create the UITextView, using the textContainer that's linked to our storage and
    // layout manager.   This allows us to get inserted into the processing chain of the
    // UITextView.
    textView = UITextView(frame: CGRect.zero, textContainer: container)
    var textContainerInsets = insets
    if let navbar = navigationController?.navigationBar {
      textContainerInsets.top += navbar.frame.height
    }
    textView.textContainerInset = textContainerInsets
    textView.translatesAutoresizingMaskIntoConstraints = false
    textView.contentInsetAdjustmentBehavior = .automatic
    view.addSubview(textView)

    NSLayoutConstraint.activate([
      //textView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      textView.topAnchor.constraint(equalTo: view.topAnchor),
      textView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
      textView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
      textView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
      textView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
    ])
    
    textView.delegate = self
  }
}

// MARK: NSTextStorageDelegate
extension ANFVC: NSTextStorageDelegate {
  func textStorage(_ textStorage: NSTextStorage, willProcessEditing editedMask: NSTextStorage.EditActions, range editedRange: NSRange, changeInLength delta: Int) {
  }
  

  func textStorage(_ textStorage: NSTextStorage, didProcessEditing editedMask: NSTextStorage.EditActions, range editedRange: NSRange, changeInLength delta: Int) {
  }
}

// MARK: Layout of the content
extension ANFVC {
  private func layoutContent() {
    guard let story = story else {
      return
    }
    
    let aggregatedAttrText = NSMutableAttributedString()
    for i in 0..<story.articleJson.components.count {
      let component = story.articleJson.components[i]
      let isRegular: Bool = traitCollection.horizontalSizeClass == .regular
      guard let attrText = story.articleJson.attributedString(for: component, isRegular: isRegular) else {
        continue
      }
      aggregatedAttrText.append(attrText)

    }
    textView.attributedText = aggregatedAttrText
    
    textView.setNeedsDisplay()
  }
}

extension ANFVC: UITextViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let yOffset = scrollView.contentOffset.y
    guard let navbar = navigationController?.navigationBar else {
      return
    }
    let navHeight = navbar.frame.height
    let offset = min(max(navHeight - yOffset, 0), navHeight)
    let opacity = offset / navHeight
    setNavBarAlpha(opacity)
  }
  
  private func setNavBarAlpha(_ alpha: CGFloat) {
    guard let navbar = navigationController?.navigationBar else {
        return
    }
    navbar.alpha = alpha
  }
  
  func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    let yOffset = scrollView.contentOffset.y
    if yOffset < 0 {
      DispatchQueue.main.async {
        scrollView.setContentOffset(.zero, animated: true)
      }
    }
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    let yOffset = scrollView.contentOffset.y

    if yOffset < 0 {
      scrollView.setContentOffset(.zero, animated: true)
    }

    //print("\(#function) - yOffset = \(yOffset), hidden = \(navbar.isHidden), translucent = \(navbar.isTranslucent)")
  }
  
  func applyTransparentBackgroundToTheNavigationBar(_ opacity: CGFloat) {
    var transparentBackground: UIImage
    
    // The background of a navigation bar switches from being translucent
    // to transparent when a background image is applied.  The intensity of
    // the background image's alpha channel is inversely related to the
    // transparency of the bar.  That is, a smaller alpha channel intensity
    // results in a more transparent bar and vis-versa.
    //
    // Below, a background image is dynamically generated with the desired
    // opacity.
    UIGraphicsBeginImageContextWithOptions(CGSize(width: 1, height: 1),
                                           false,
                                           navigationController!.navigationBar.layer.contentsScale)
    let context = UIGraphicsGetCurrentContext()!
    context.setFillColor(red: 1, green: 1, blue: 1, alpha: opacity)
    UIRectFill(CGRect(x: 0, y: 0, width: 1, height: 1))
    transparentBackground = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    
    // You should use the appearance proxy to customize the appearance of
    // UIKit elements.  However changes made to an element's appearance
    // proxy do not effect any existing instances of that element currently
    // in the view hierarchy.  Normally this is not an issue because you
    // will likely be performing your appearance customizations in
    // -application:didFinishLaunchingWithOptions:.  However, this example
    // allows you to toggle between appearances at runtime which necessitates
    // applying appearance customizations directly to the navigation bar.
    /* let navigationBarAppearance = UINavigationBar.appearance(whenContainedInInstancesOf: [UINavigationController.self]) */
    let navigationBarAppearance = self.navigationController!.navigationBar
    
    navigationBarAppearance.setBackgroundImage(transparentBackground, for: .default)
  }
}
