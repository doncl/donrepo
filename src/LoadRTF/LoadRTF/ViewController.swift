//
//  ViewController.swift
//  LoadRTF
//
//  Created by Don Clore on 5/30/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet var pdfView: PDFView!
  @IBOutlet var pdfButton: UIBarButtonItem!
  @IBOutlet var textView: UITextView!
    @IBOutlet var tmlButton: UIBarButtonItem!
    @IBOutlet var rtfButton: UIBarButtonItem!
    
    override func viewDidLoad() {
      super.viewDidLoad()

      tmlButton.target = self
      tmlButton.action = #selector(ViewController.tmlButtonTapped(_:))
      
      rtfButton.target = self
      rtfButton.action = #selector(ViewController.rtfButtonTapped(_:))
      
      pdfButton.target = self
      pdfButton.action = #selector(ViewController.pdfButtonTapped(_:))
    }
}

extension ViewController {
  @objc func rtfButtonTapped(_ sender: UIBarButtonItem) {
    guard let rtfPath = Bundle.main.url(forResource: "Skills", withExtension: "rtf") else {
      return
    }
    
    pdfView.isHidden = true
    do {
      let start = Date()
      let attributedStringWithRtf: NSAttributedString =
        try NSAttributedString(url: rtfPath,
                               options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.rtf],
                               documentAttributes: nil)
      
      self.textView.attributedText = attributedStringWithRtf
      let timeDiff = Date().timeIntervalSince(start) * 1000.0
      print(timeDiff)
    } catch let error {
      print("Got an error \(error)")
    }
  }
  
  @objc func tmlButtonTapped(_ sender: UIBarButtonItem) {
    print("tml")
    pdfView.isHidden = true
  }
  
  @objc func pdfButtonTapped(_ sender: UIBarButtonItem) {
    guard let pdfURL = Bundle.main.url(forResource: "Skills", withExtension: "pdf") else {
      return
    }
    
    pdfView.pdfURL = pdfURL
    textView.isHidden = true
    pdfView.isHidden = false
    pdfView.setNeedsDisplay()

  }
}

