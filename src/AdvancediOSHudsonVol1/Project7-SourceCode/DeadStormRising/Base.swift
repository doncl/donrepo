//
//  Base.swift
//  DeadStormRising
//
//  Created by TwoStraws on 28/06/2016.
//  Copyright © 2016 Paul Hudson. All rights reserved.
//

import SpriteKit
import UIKit

class Base: GameItem {
	var hasBuilt = false

	func reset() {
		hasBuilt = false
	}

	func setOwner(_ owner: Player) {
		self.owner = owner
		hasBuilt = true
		self.colorBlendFactor = 0.9

		if owner == .red {
			color = UIColor(red: 1, green: 0.4, blue: 0.1, alpha: 1)
		} else {
			color = UIColor(red: 0.1, green: 0.5, blue: 1, alpha: 1)
		}
	}

	func buildUnit() -> Unit? {
		// 1: ensure bases build only one thing per turn
		guard hasBuilt == false else { return nil }
		hasBuilt = true

		// 2: create the new unit
		let unit: Unit

		if owner == .red {
			unit = Unit(imageNamed: "tankRed")
		} else {
			unit = Unit(imageNamed: "tankBlue")
		}

		// 3: mark it as having moved and fired already
		unit.hasMoved = true
		unit.hasFired = true

		// 4 give it the same owner and position as this base
		unit.owner = owner
		unit.position = position

		// 5: give it the correct Z position
		unit.zPosition = zPositions.unit

		// 6: send it back to the caller
		return unit
	}
}
