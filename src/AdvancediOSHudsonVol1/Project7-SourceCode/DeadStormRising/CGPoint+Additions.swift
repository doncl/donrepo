//
//  CGPoint+Additions.swift
//  DeadStormRising
//
//  Created by TwoStraws on 29/06/2016.
//  Copyright © 2016 Paul Hudson. All rights reserved.
//

import UIKit

extension CGPoint {
	func manhattanDistance(to: CGPoint) -> CGFloat {
		return (abs(x - to.x) + abs(y - to.y))
	}
}
