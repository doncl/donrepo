//
//  Array+Additions.swift
//  DeadStormRising
//
//  Created by TwoStraws on 29/06/2016.
//  Copyright © 2016 Paul Hudson. All rights reserved.
//

import UIKit

extension Array where Element: GameItem {
	func itemsAt(position: CGPoint) -> [Element] {
		return filter {
			let diffX = abs($0.position.x - position.x)
			let diffY = abs($0.position.y - position.y)
			return diffX + diffY < 20
		}
	}
}
