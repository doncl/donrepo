//
//  ViewController.swift
//  Animation
//
//  Created by TwoStraws on 30/06/2016.
//  Copyright © 2016 Paul Hudson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	var animator: UIViewPropertyAnimator!

	override func viewDidLoad() {
		super.viewDidLoad()

		let redBox = UIView(frame: CGRect(x: -64, y: 0, width: 128, height: 128))
		redBox.translatesAutoresizingMaskIntoConstraints = false
		redBox.backgroundColor = UIColor.red
		redBox.center.y = view.center.y
		view.addSubview(redBox)

		let slider = UISlider()
		slider.translatesAutoresizingMaskIntoConstraints = false
		slider.addTarget(self, action: #selector(sliderChanged), for: .valueChanged)
		view.addSubview(slider)

		slider.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
		slider.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true

		let play = UIBarButtonItem(barButtonSystemItem: .play, target: self, action: #selector(playTapped))
		let flip = UIBarButtonItem(barButtonSystemItem: .rewind, target: self, action: #selector(reverseTapped))
		navigationItem.rightBarButtonItems = [play, flip]

		animator = UIViewPropertyAnimator(duration: 2, curve: .easeInOut) { [unowned self, redBox] in
			redBox.center.x = self.view.frame.width
			redBox.transform = CGAffineTransform(rotationAngle: CGFloat.pi).scaledBy(x: 0.001, y: 0.001)
		}

		animator.addCompletion { [unowned self] position in
			if position == .end {
				self.view.backgroundColor = UIColor.green
			} else {
				self.view.backgroundColor = UIColor.black
			}
		}
	}

	@objc func sliderChanged(_ sender: UISlider) {
		animator.fractionComplete = CGFloat(sender.value)
	}

	@objc func playTapped() {
		if animator.state == .active {
			animator.stopAnimation(false)
			animator.finishAnimation(at: .end)
		} else {
			animator.startAnimation()
		}
	}

	@objc func reverseTapped() {
		animator.isReversed = true
	}
}

