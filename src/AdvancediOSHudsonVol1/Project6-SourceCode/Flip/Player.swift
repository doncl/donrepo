//
//  Player.swift
//  Flip
//
//  Created by TwoStraws on 25/06/2016.
//  Copyright © 2016 Paul Hudson. All rights reserved.
//

import GameplayKit
import UIKit

class Player: NSObject, GKGameModelPlayer {
	static let allPlayers = [Player(stone: .black), Player(stone: .white)]
	var stoneColor: StoneColor
	var playerId: Int

	init(stone: StoneColor) {
		stoneColor = stone
		playerId = stone.rawValue
	}

	var opponent: Player {
		if stoneColor == .black {
			return Player.allPlayers[1]
		} else {
			return Player.allPlayers[0]
		}
	}
}
