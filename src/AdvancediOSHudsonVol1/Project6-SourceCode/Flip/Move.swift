//
//  Move.swift
//  Flip
//
//  Created by TwoStraws on 25/06/2016.
//  Copyright © 2016 Paul Hudson. All rights reserved.
//

import GameplayKit
import UIKit

class Move: NSObject, GKGameModelUpdate {
	var value = 0

	var row: Int
	var col: Int

	init(row: Int, col: Int) {
		self.row = row
		self.col = col
	}
}
