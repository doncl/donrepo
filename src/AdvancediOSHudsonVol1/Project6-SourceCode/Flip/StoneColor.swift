//
//  StoneColor.swift
//  Flip
//
//  Created by TwoStraws on 25/06/2016.
//  Copyright © 2016 Paul Hudson. All rights reserved.
//

import Foundation

enum StoneColor: Int {
	case empty, choice, white, black
}
