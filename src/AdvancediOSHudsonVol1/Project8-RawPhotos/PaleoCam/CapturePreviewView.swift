//
//  CapturePreviewView.swift
//  PaleoCam
//
//  Created by TwoStraws on 02/07/2016.
//  Copyright © 2016 Paul Hudson. All rights reserved.
//

import AVFoundation
import UIKit

class CapturePreviewView: UIView {
	override class var layerClass: AnyClass {
		return AVCaptureVideoPreviewLayer.self
	}
}
