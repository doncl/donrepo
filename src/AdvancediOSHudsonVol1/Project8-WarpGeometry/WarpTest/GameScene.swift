//
//  GameScene.swift
//  WarpTest
//
//  Created by TwoStraws on 01/07/2016.
//  Copyright © 2016 Paul Hudson. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
	var spaceship: SKSpriteNode!
	var warpType = 0

	var src = [vector_float2(0.0, 0.0), vector_float2(0.5, 0.0), vector_float2(1.0, 0.0), vector_float2(0.0, 0.5), vector_float2(0.5, 0.5), vector_float2(1.0, 0.5), vector_float2(0.0, 1.0), vector_float2(0.5, 1.0), vector_float2(1.0, 1.0)]

    override func didMove(to view: SKView) {
		spaceship = SKSpriteNode(imageNamed: "Spaceship")
		addChild(spaceship)

		let warp = SKWarpGeometryGrid(columns: 2, rows: 2, sourcePositions: src, destinationPositions: src)
		spaceship.warpGeometry = warp
    }
    
    
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		var dst = src

		switch warpType {
		case 0:
			// stretch the nose up
			dst[7] = vector_float2(0.5, 1.5)

		case 1:
			// stretch the wings down
			dst[0] = vector_float2(0, -0.5)
			dst[2] = vector_float2(1, -0.5)

		case 2:
			// squash the ship vertically
			dst[0] = vector_float2(0.0, 0.25)
			dst[1] = vector_float2(0.5, 0.25)
			dst[2] = vector_float2(1.0, 0.25)

			dst[6] = vector_float2(0.0, 0.75)
			dst[7] = vector_float2(0.5, 0.75)
			dst[8] = vector_float2(1.0, 0.75)

		case 3:
			// flip it left to right
			dst[0] = vector_float2(1.0, 0.0)
			dst[2] = vector_float2(0.0, 0.0)

			dst[3] = vector_float2(1.0, 0.5)
			dst[5] = vector_float2(0.0, 0.5)

			dst[6] = vector_float2(1.0, 1)
			dst[8] = vector_float2(0.0, 1)

		default:
			break
		}

		let newWarp = SKWarpGeometryGrid(columns: 2, rows: 2, sourcePositions: src, destinationPositions: dst)
		let oldWarp = spaceship.warpGeometry!

		if let action = SKAction.animate(withWarps: [newWarp, oldWarp], times: [0.5, 1]) {
			spaceship.run(action)
		}

		warpType += 1

		if warpType > 3 {
			warpType = 0
		}
	}
}
