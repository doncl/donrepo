//
//  ViewController.swift
//  HappyDays
//
//  Created by TwoStraws on 15/06/2016.
//  Copyright © 2016 Paul Hudson. All rights reserved.
//

import AVFoundation
import Photos
import Speech
import UIKit

class ViewController: UIViewController {
	@IBOutlet var helpLabel: UILabel!

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}

	@IBAction func requestPermissions(_ sender: AnyObject) {
		requestPhotosPermissions()
	}

	func requestPhotosPermissions() {
		PHPhotoLibrary.requestAuthorization { [unowned self] authStatus in
			DispatchQueue.main.async {
				if authStatus == .authorized {
					self.requestRecordPermissions()
				} else {
					self.helpLabel.text = "Photos permission was declined; please enable it in settings then tap Continue again."
				}
			}
		}
	}

	func requestRecordPermissions() {
		AVAudioSession.sharedInstance().requestRecordPermission() { [unowned self] allowed in
			DispatchQueue.main.async {
				if allowed {
					self.requestTranscribePermissions()
				} else {
					self.helpLabel.text = "Recording permission was declined; please enable it in settings then tap Continue again."
				}
			}
		}
	}

	func requestTranscribePermissions() {
		SFSpeechRecognizer.requestAuthorization { [unowned self] authStatus in
			DispatchQueue.main.async {
				if authStatus == .authorized {
					self.authorizationComplete()
				} else {
					self.helpLabel.text = "Transcription permission was declined; please enable it in settings then tap Continue again."
				}
			}
		}
	}

	func authorizationComplete() {
		dismiss(animated: true)
	}
}

