//
//  MemoryCell.swift
//  HappyDays
//
//  Created by TwoStraws on 15/06/2016.
//  Copyright © 2016 Paul Hudson. All rights reserved.
//

import UIKit

class MemoryCell: UICollectionViewCell {
	@IBOutlet var imageView: UIImageView!
}
