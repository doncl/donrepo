//
//  PaddedLabel.swift
//  KhromaLike
//
//  Created by Don Clore on 4/23/16.
//  Copyright © 2016 RayWenderlich. All rights reserved.
//

import UIKit

class PaddedLabel: UILabel  {

    var verticalPadding : CGFloat = 0.0

    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
        // Check the vertical size class
        if traitCollection.verticalSizeClass == .Compact {
            // No padding when vertically compact
            verticalPadding = 0.0
        } else {
            // Padding when we have regular size class
            verticalPadding = 20.0
        }
        invalidateIntrinsicContentSize()
    }

    override func intrinsicContentSize() -> CGSize {
        var intrinsicSize = super.intrinsicContentSize()

        // Add the padding
        intrinsicSize.height += verticalPadding
        return intrinsicSize
    }
}
