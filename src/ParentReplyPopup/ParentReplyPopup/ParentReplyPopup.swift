//
//  ParentReplyPopup.swift
//  ParentReplyPopup
//
//  Created by Don Clore on 5/21/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit
import SnapKit

enum PointerDirection {
  case up
  case down
}

class ParentReplyPopup: UIViewController {
  var loaded : Bool = false
  var presenting : Bool = true
  static let pointerHeight : CGFloat = 16.0
  static let pointerBaseWidth : CGFloat = 24.0
  static let avatarOffset : CGFloat = 16.0
  static let avatarWidth : CGFloat = 48.0
  static let horzFudge : CGFloat = 15.0
  var origin : CGPoint
  private var width : CGFloat
  private var height : CGFloat
  
  let popupView : UIView = UIView()
  let avatar : UIImageView = UIImageView()
  let pointer : CAShapeLayer = CAShapeLayer()
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  init(origin : CGPoint, size: CGSize, avatarImage: UIImage? = nil) {
    self.origin = origin
    self.width = size.width
    self.height = size.height
    super.init(nibName: nil, bundle: nil)
    if let image = avatarImage {
      avatar.image = image
    }
  }

  override func viewDidLoad() {
    if loaded {
      return
    }
    loaded = true
    super.viewDidLoad()

    view.addSubview(popupView)
    view.backgroundColor = .clear
    popupView.backgroundColor = .white
    popupView.layer.shadowOffset = CGSize(width: 2.0, height: 3.0)
    popupView.layer.shadowRadius = 5.0
    popupView.layer.shadowOpacity = 0.5
    
    popupView.addSubview(avatar)
    popupView.clipsToBounds = false
    popupView.layer.masksToBounds = false
  
    if let _ = avatar.image {
      avatar.layer.cornerRadius = ParentReplyPopup.avatarWidth / 2.0
      avatar.clipsToBounds = true
      popupView.bringSubview(toFront: avatar)
    }
    view.isUserInteractionEnabled = true
    let tap = UITapGestureRecognizer(target: self,
                                     action: #selector(ParentReplyPopup.tap(_:)))
    
    view.addGestureRecognizer(tap)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    let tuple = getPopupFrameAndPointerDirection(size: view.frame.size)
    let popupFrame = tuple.0
    let direction = tuple.1
    
    let pointerPath = makePointerPath(direction: direction, popupFrame: popupFrame)
    pointer.path = pointerPath.cgPath
    pointer.fillColor = UIColor.white.cgColor
    pointer.lineWidth = 0.5
    pointer.masksToBounds = false
    popupView.layer.addSublayer(pointer)
    
    remakeConstraints(popupFrame)
    
    let bounds = UIScreen.main.bounds
    let width = view.frame.width + ParentReplyPopup.avatarOffset

    if bounds.width < width && width > 0 {
      let ratio = (bounds.width / width) * 0.9
      let xform = CGAffineTransform(scaleX: ratio, y: ratio)
      popupView.transform = xform
    } else {
      popupView.transform = .identity
    }
  }
  
  private func getPopupFrameAndPointerDirection(size: CGSize) -> (CGRect, PointerDirection) {
    let y : CGFloat
    let direction : PointerDirection
    if origin.y > size.height / 2 {
      y = origin.y - (height + ParentReplyPopup.pointerHeight)
      direction = .down
    } else {
      y = origin.y + ParentReplyPopup.pointerHeight
      direction = .up
    }
    var rc : CGRect = CGRect(x: 30.0, y: y, width: width, height: height)
    let rightmost = rc.origin.x + rc.width
    let center = rc.center
    if origin.x > rightmost {
      let offset = origin.x - center.x
      rc = rc.offsetBy(dx: offset, dy: 0)
    } else if origin.x < rc.origin.x {
      let offset = center.x - origin.x
      rc = rc.offsetBy(dx: -offset, dy: 0)
    }
    let bounds = UIScreen.main.bounds
    let popupWidth = rc.width + ParentReplyPopup.avatarOffset
    if bounds.width <= popupWidth {
      rc = CGRect(x: 0, y: rc.origin.y, width: rc.width,
                  height: rc.height)
    }
    return (rc, direction)
  }
  
  fileprivate func remakeConstraints(_ popupFrame: CGRect) {
    popupView.snp.remakeConstraints { make in
      make.leading.equalTo(view).offset(popupFrame.origin.x)
      make.top.equalTo(view).offset(popupFrame.origin.y)
      make.width.equalTo(popupFrame.width)
      make.height.equalTo(popupFrame.height)
    }
    
    if let _ = avatar.image {
      avatar.snp.remakeConstraints { make in
        make.width.equalTo(ParentReplyPopup.avatarWidth)
        make.height.equalTo(ParentReplyPopup.avatarWidth)
        make.left.equalTo(popupView.snp.left).offset(ParentReplyPopup.avatarOffset)
        make.top.equalTo(popupView.snp.top).offset(-ParentReplyPopup.avatarOffset)
      }
    }
  }
  
  private func makePointerPath(direction: PointerDirection, popupFrame : CGRect) -> UIBezierPath {
    let path = UIBezierPath()
    path.lineJoinStyle = CGLineJoin.bevel
    
    // previous code is supposed to assure that the popupFrame is not outside the origin.
    assert(popupFrame.origin.x < origin.x && popupFrame.origin.x + popupFrame.width > origin.x)

    let adjustedX = origin.x - popupFrame.origin.x
    
    if direction == .down {
      let adjustedApex = CGPoint(x: adjustedX, y: popupFrame.height + ParentReplyPopup.pointerHeight - 1)
      path.move(to: adjustedApex)
      // down is up.
      let leftBase = CGPoint(x: adjustedApex.x - (ParentReplyPopup.pointerBaseWidth / 2),
                             y: adjustedApex.y - ParentReplyPopup.pointerHeight)
      path.addLine(to: leftBase)
      let rightBase = CGPoint(x: adjustedApex.x + (ParentReplyPopup.pointerBaseWidth / 2),
                              y: adjustedApex.y - ParentReplyPopup.pointerHeight)
      
      path.addLine(to: rightBase)
      path.close()
    } else {
      let adjustedApex = CGPoint(x: adjustedX,
                                 y: -ParentReplyPopup.pointerHeight + 1)
      path.move(to: adjustedApex)
      
      let leftBase = CGPoint(x: adjustedApex.x - (ParentReplyPopup.pointerBaseWidth / 2),
                             y: adjustedApex.y + ParentReplyPopup.pointerHeight)
      path.addLine(to: leftBase)
      
      let rightBase = CGPoint(x: adjustedApex.x + (ParentReplyPopup.pointerBaseWidth / 2),
                              y: adjustedApex.y + ParentReplyPopup.pointerHeight)
      path.addLine(to: rightBase)
      path.close()
    }
    
    return path
  }
  

  @objc func tap(_ sender: UITapGestureRecognizer) {
    dismiss(animated: true, completion: nil)
  }
}

//MARK: - rotation
extension ParentReplyPopup {
  override func willTransition(to newCollection: UITraitCollection,
                               with coordinator: UIViewControllerTransitionCoordinator) {

    dismiss(animated: true, completion: nil)
  }
  
  
  override func viewWillTransition(to size: CGSize,
                                   with coordinator: UIViewControllerTransitionCoordinator) {
    
    dismiss(animated: true, completion: nil)
  }
}

//MARK: UIViewControllerAnimatedTransitioning
extension ParentReplyPopup : UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?)
    -> TimeInterval {
    return 0.400
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
         let containerView = transitionContext.containerView
    

    let transitionView : UIView
    if presenting {
      transitionView = transitionContext.view(forKey: .to)!
    } else {
      transitionView = transitionContext.view(forKey: .from)!
    }
   
    let xScaleFactor : CGFloat = 0.01
    let yScaleFactor : CGFloat = 0.01

    let scaleTransform = CGAffineTransform(scaleX: xScaleFactor, y: yScaleFactor)

    if presenting {
      transitionView.transform = scaleTransform
      transitionView.clipsToBounds = true
    } else {
      transitionView.transform = .identity
    }

    containerView.addSubview(transitionView)
    containerView.bringSubview(toFront: transitionView)
    
    UIView.animate(withDuration: 0.300, delay:0.0, options: [.curveEaseInOut], animations: {
        transitionView.transform = self.presenting ?
          CGAffineTransform.identity : scaleTransform
      },
      completion:{_ in
        transitionContext.completeTransition(true)
      }
    )
  }
}



extension CGRect {
  var center : CGPoint {
    let x = origin.x + width / 2
    let y = origin.y + height / 2
    return CGPoint(x: x, y: y)
  }
}
