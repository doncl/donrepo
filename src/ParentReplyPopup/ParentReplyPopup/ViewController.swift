//
//  ViewController.swift
//  ParentReplyPopup
//
//  Created by Don Clore on 5/21/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  var origin : CGPoint = .zero
  var popup : ParentReplyPopup?
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func upperButtonTouched(_ sender: UIButton) {
    buttonTouchHandler(sender)
  }
  
  fileprivate func buttonTouchHandler(_ sender: UIButton) {
    view.isUserInteractionEnabled = true
    let buttonCenter = CGPoint(x: sender.frame.midX, y: sender.frame.midY)
    
    origin = buttonCenter
    popup = ParentReplyPopup(origin: buttonCenter, 
                             size: CGSize(width: 320, height: 144),
                             avatarImage: UIImage(named: "HowIFeelAboutThis"))
    
    popup?.modalPresentationStyle = .custom
    popup?.transitioningDelegate = self
    present(popup!, animated: true, completion: nil)
  }
  
  @IBAction func buttonTouched(_ sender: UIButton) {
    buttonTouchHandler(sender)
  }
  
}
  
extension ViewController : UIViewControllerTransitioningDelegate {
  func presentationController(forPresented presented: UIViewController,
                              presenting: UIViewController?,
                              source: UIViewController) -> UIPresentationController? {
    
    let presentationController =
      ParentReplyPopupPresentationController(presentedViewController: presented,
                                             presenting: presenting)
    presentationController.parentFrame = view.frame
    presentationController.origin = origin
    return presentationController
  }
  
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    if let transition = dismissed as? ParentReplyPopup {
      transition.presenting = false
      return transition
    }
    return nil
  }
  
  func animationController(forPresented presented: UIViewController,
                           presenting: UIViewController,
                           source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    
    if let transition = presented as? ParentReplyPopup {
      transition.presenting = true
      return transition
    }
    return nil
  }
}

