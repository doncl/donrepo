//
//  Notification.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct Notification : Codable {
  var id : String?
  var userId : String?
  var subscriberId : String?
  var channelKey : String?
  var roomKey : String?
  var conversationId : String?
  var isDirect : Bool?
  var messageAuthor : UserSummary?
  var messageId : String?
  var notificationType : NotificationType?
  var subscriptionKey : String?
  var sentOn : Date?
  var title : String?
  var body : String?
  var url : String?
  var digestBody : String?
  var delayNotification : Bool?
  var consumedOn : Date?
  var inDigestSentOn : Date?
  var inDigest : Bool?
  var consumed : Bool?
}
