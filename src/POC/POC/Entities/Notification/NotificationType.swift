//
//  NotificationType.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

enum NotificationType : String, Codable {
  case precreated = "PreCreated"
  case newmessage = "NewMessage"
  case deletedmessage = "DeletedMessage"
  case editedmessage = "EditedMessage"
  case newconversation = "NewConversation"
  case usermentioned = "UserMentioned"
  case mutated = "Mutated"
  case notificationconsumed = "NotificationConsumed"
  case subscribetoconversation = "SubscribeToConversation"
  case updatedmessagereactionsummary = "UpdatedMessageReactionSummary"
  case userevent = "UserEvent"
  case digestsummary = "DigestSummary"
}
