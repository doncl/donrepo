//
//  AccessBehaviorSummary.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct AccessBehaviorSummary : Codable {
  var name : String?
  var displayName : String?
  var description : String?
  var active : Bool?
}

