//
//  Reaction.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct Reaction : Codable {
  var reactionType : ReactionType
  var dateCreated : Date
}
