//
//  VideoMetaData.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct VideoMetaData : Codable {
  var duration : Int?
  var aspectRatio : Double?
  var mezzanineFileSize : Int?
  var mezzanineBucket : String?
}
