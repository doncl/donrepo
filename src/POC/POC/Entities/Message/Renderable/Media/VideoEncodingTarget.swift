//
//  VideoEncodingTarget.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

enum VideoEncodingTargetState : String, Codable {
  case new = "New"
  case begun = "Begun"
  case error = "Error"
  case complete = "Complete"
}

enum EncodingType : String, Codable {
  case mp4 = "Mp4"
  case hls = "Hls"
  case webm = "Webm"
}

enum TranscodeJobType : String, Codable {
  case mp4500 = "Mp4500"
  case hls = "Hls"
  case mp41000 = "Mp41000"
  case mp43000 = "Mp43000"
  case webm500 = "WebM500"
  case webm1000 = "WebM1000"
  case webm3000 = "WebM3000"
}

struct VideoEncodingTarget : Codable {
  var urlSuffix : String?
  var encodingType : EncodingType?
  var jobType : TranscodeJobType?
  var encodingState : VideoEncodingTargetState?
  
  var newAt : Date?
  var begunAt : Date?
  var newToBegunSeconds : Double?
  var errorAt : Date?
  var completeAt : Date?
  var begunToCompleteSeconds : Double?
  
  var bitRates : [Int]?
  var size : Int?
  var outputs : [VideoTranscodeOutput]?
}
