//
//  MediaSummary.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

enum MediaType : String, Codable {
  case photo = "Photo"
  case video = "Video"
  case document = "Document"
  case audio = "Audio"
}

enum ImageType : String, Codable {
  case jpeg = "Jpeg"
  case png = "Png"
  case svg = "Svg"
}

enum HostType: String, Codable {
  case s3 = "s3"
  case oauth = "oAuth"
}

struct MediaSummary : Codable {
  var id : String?
  var hostType : HostType?
  var caption : String?
  var title : String?
  var description : String?
  var url : String?
  var mediaType : MediaType?
  var uploadedBy : UserSummary?
  var imageWidth : Int?
  var imageHeight : Int?
  var imageType : ImageType?
  
  var encodingTargets : [VideoEncodingTarget]?
  
  var videoMetaData : VideoMetaData?
  var videoThumbnailUrlSuffix : String?
  var videoThumbnailUrl : String?
}
