//
//  Renderable.swift
//  POC
//
//  Created by Don Clore on 12/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct Renderable : Codable {
  var markdown : String?
  var urlInfo : UrlInfo?
  var mediaCollection : [MediaSummary]?
}
