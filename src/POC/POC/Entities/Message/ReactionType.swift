//
//  ReactionType.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

enum ReactionType : String, Codable {
  case none = "None"
  case like = "Like"
  case laugh = "Laugh"
  case shock = "Shock"
}
