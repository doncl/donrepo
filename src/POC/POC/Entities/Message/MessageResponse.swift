//
//  MessageResponse.swift
//  POC
//
//  Created by Don Clore on 12/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct MessageResponse : Codable, JSONCoding {
  typealias codableType =  MessageResponse
  
  var id : String?
  var channelKey : String?
  var roomKey : String?
  var conversationId : String?
  var author : UserSummary?
  var body : String?
  var isFirstMessage : Bool?
  var datePosted : Date?
  var renderables : [Renderable?]?
  var isDeleted : Bool?
  var isFlaggedSpam : Bool?
  var isFlaggedAbuse : Bool?
  var isMarkedReviewed : Bool?
  var totalAbuseFlags : Int?
  var totalSpamFlags : Int?
  var totalReviewed : Int?
  var reactionSummaries : [ReactionSummary]?
}

