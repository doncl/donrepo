//
//  UserPolicy.swift
//  POC
//
//  Created by Don Clore on 12/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

enum ActionAuthorizationState : String, Codable {
  case yes = "Yes"
  case no = "No"
  case register = "Register"
  case verifyEmail = "VerifyEmail"
  case subscribe = "Subscribe"
  case publisher = "Publisher"
  case username = "Username"
}

struct UserPolicy : Codable {
  var canPostAction : ActionAuthorizationState?
  var canDirectMessage : ActionAuthorizationState?
  var canReply : ActionAuthorizationState?
  var canPreview : ActionAuthorizationState?
  var canView : ActionAuthorizationState?
  var othersCanViewMyPosts : ActionAuthorizationState?
  var canModerate : ActionAuthorizationState?
  var canUploadAudio : ActionAuthorizationState?
  var canUploadDocuments : ActionAuthorizationState?
  var canUploadVideos : ActionAuthorizationState?
  var canUploadPhotos: ActionAuthorizationState?
  var canPublishLongForm : ActionAuthorizationState?
  var canEditAnyPost : ActionAuthorizationState?
  var canEditMyPosts : ActionAuthorizationState?
  var canDeleteAnyPost : ActionAuthorizationState?
  var canDeleteMyPosts : ActionAuthorizationState?
  var canViewDeletedMessage : ActionAuthorizationState?
  var canUndeleteAnyMessage : ActionAuthorizationState?
  var canManageModerators: ActionAuthorizationState?
  var canViewModerationEvents : ActionAuthorizationState?
  var canManagePublishers : ActionAuthorizationState?
  var canManageChannelVisibility : ActionAuthorizationState?
  var canManageUserPolicies : ActionAuthorizationState?
  var canManageChannel : ActionAuthorizationState?
  var canViewChannelData : ActionAuthorizationState?
  var canManageGroups : ActionAuthorizationState?
  var canManageRoom : ActionAuthorizationState?
  var canModifyLockedConversation : ActionAuthorizationState?
  var canViewModerationStatus : ActionAuthorizationState?
  var canViewFullName : ActionAuthorizationState?
  var canEditAvatar : ActionAuthorizationState?
  var canEditFullName : ActionAuthorizationState?
  var canUpdateUserProfile : ActionAuthorizationState?
  var canFlagMessage : ActionAuthorizationState?
  var canMention : ActionAuthorizationState?
  var canMentionChannel : ActionAuthorizationState?
  var canMentionRoom : ActionAuthorizationState?
  var canMentionConversation : ActionAuthorizationState?
  var canAddReaction : ActionAuthorizationState?
  var canEditUserFlags : ActionAuthorizationState?
  var canViewChannelUsers : ActionAuthorizationState?
  var canDownloadChannelUsers : ActionAuthorizationState?
  var canSetStoryPublishDate : ActionAuthorizationState?
  var canViewAllPostdatedConversations : ActionAuthorizationState?
  var canManageAccessBehaviors : ActionAuthorizationState?
  var canMarkUserVerified : ActionAuthorizationState?
  var canSetIsFacebookInstantArticle : ActionAuthorizationState?
}
