//
//  UserProfileResponse.swift
//  POC
//
//  Created by Don Clore on 11/26/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation


struct UserProfileResponse : Codable, JSONCoding {
  typealias codableType = UserProfileResponse
  
  var id : String?
  var username : String?
  var avatar : MediaSummary?
  var firstName : String?
  var lastName : String?
  var email : String?
  var displayName : String?
  var showFullName : Bool?
  var bio : String?
  var contactEmail : String?
  var contactPhoneNumber : String?
  var contactAddress : String?
  var isVerified : Bool?
  var emailVerified : Bool?
  var dateJoinedChannel : Date?
  var isSuper : Bool?
  var readOnlyBan : BanStatus?
  var shadowBan : BanStatus?
  var fullBan : BanStatus?
  var isActive : Bool?
  var viewerProfilePolicy : UserProfilePolicy?
  var channels : [ChannelUserPolicyResponse]?
  var rooms : [RoomUserPolicyResponse]?
  var availableChannels : [String]?
  var flags : [String]?
  var viewerIsSubscribed : Bool?
  var numberUsersFollowing : Int? 
}

struct BanStatus : Codable {
  var isBanned : Bool?
  var bannedUntil : Date?
}

struct ChannelUserPolicyResponse : Codable {
  var channelKey : String?
  var isPublisher : Bool?
  var isModerator : Bool?
  var isSubscriber : Bool?
  var hideAvatar : Bool?
  var readOnlyBan : BanStatus?
  var shadowBan : BanStatus?
  var fullBan : BanStatus?
  var viewerProfilePolicy : UserProfilePolicy?
  var explicitGroups : [String]?
  var availableGroups : [String]?
}

struct RoomUserPolicyResponse : Codable {
  var channelKey : String?
  var roomKey : String?
  var isModerator : Bool?
  var readOnlyBan : BanStatus?
  var shadowBan : BanStatus?
  var viewerProfilePolicy : UserProfilePolicy?
}

struct UserProfilePolicy : Codable {
  var canModerate : ActionAuthorizationState?
  var canManageGroups : ActionAuthorizationState?
  var canManageModerators : ActionAuthorizationState?
  var canViewModerationEvents : ActionAuthorizationState?
  var canManagePublishers : ActionAuthorizationState?
  var canViewModerationStatus : ActionAuthorizationState?
  var canViewFullName : ActionAuthorizationState?
  var canEditAvatar : ActionAuthorizationState?
  var canEditFullName : ActionAuthorizationState?
  var canDirectMessage : ActionAuthorizationState?
  var canEditUserFlags : ActionAuthorizationState?
  var canMarkUserVerified : ActionAuthorizationState?
}
