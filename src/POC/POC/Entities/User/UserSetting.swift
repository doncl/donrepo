//
//  UserSetting.swift
//  POC
//
//  Created by Don Clore on 12/3/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

enum SettingType : String, Codable {
  case string = "String"
  case liststring = "ListString"
  case bool = "Bool"
  case number = "Number"
}

struct UserSetting : Codable {
  var key : String?
  var channelKey : String?
  var roomKey : String?
  var extraKey : String?
  var settingType : SettingType?
  var valueString : String?
  var valueListString : [String]?
  var valueBool : Bool?
  var valueNumber : Double?
}

struct UserSettings : Codable {
  var id : String?
  var userId : String?
  var settings : [UserSetting]?
}
