//
//  FacebookUser.swift
//  POC
//
//  Created by Don Clore on 12/3/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct FacebookUser : Codable {
  enum CodingKeys: String, CodingKey {
    case id
    case email
    case firstName = "first_name"
    case lastName = "last_name"
  }

  var id : String?
  var firstName : String?
  var lastName : String?
  var email : String?
}
