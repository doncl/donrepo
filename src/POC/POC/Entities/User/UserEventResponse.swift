//
//  UserEventResponse.swift
//  POC
//
//  Created by Don Clore on 12/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct UserEventResponse : Codable {
  var date : Date?
  var type : UserEventType?
  var channelKey : String?
  var roomKey : String?
  var conversationId : String?
  var messageId : String?
  var user : UserSummary?
}
