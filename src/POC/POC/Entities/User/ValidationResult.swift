//
//  ValidationResult.swift
//  POC
//
//  Created by Don Clore on 12/3/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

enum ValidationFailureReason : String, Codable {
  case usernamenotavailable = "UsernameNotAvailable"
  case emailnotavailable = "EmailNotAvailable"
  case emailinvalidformat = "EmailInvalidFormat"
  case usernametooshort = "UsernameTooShort"
  case usernametoolong = "UsernameTooLong"
  case usernameprohibitedcharacter = "UsernameProhibitedCharacter"
  case passwordempty = "PasswordEmpty"
  case passwordtooshort = "PasswordTooShort"
  case passwordnotsecure = "PasswordNotSecure"
}


// This replaces the UserNameCreationResponse object in the ObjectMapper entities, which is
// frankly a better name than this, but I've opted to match *exactly* what Elwha code names
// everything, because I've just spent many, many hours trying to match up our Swift entities
// to C# entities in Elwha when debugging; it's a lot easier if they're named the same.
struct ValidationResult {
  var isValid : Bool?
  var reason : ValidationFailureReason?
}


