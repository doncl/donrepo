//
//  UserNotification.swift
//  POC
//
//  Created by Don Clore on 12/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct UserNotification : Codable {
  var id : String?
  var channelKey : String?
  var roomKey : String?
  var conversationId : String?
  var messageId : String?
  var messageAuthor : UserSummary?
  var isDirect : Bool?
  var title : String?
  var body : String?
  var url : String?
  var notificationType : String?
  var subscriptionKey : String?
  var sentOn : Date?
  var consumed : Bool?
}
