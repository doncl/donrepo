//
//  UserModEvents.swift
//  POC
//
//  Created by Don Clore on 12/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct QueryResponse<T: Codable> : Codable , JSONCoding {
  typealias codableType = QueryResponse<T>

  var total : Int?
  var documents : [T]?
}

struct ModerationEvent : Codable {
  var userId : String?
  var username : String?
  var channelKey : String?
  var roomKey : String?
  var conversationId : String?
  var messageId : String?
  var added : Bool?
  var destinationConversationId : String?
  var validUntil : Date?
  var moderatorUserName : String?
  var moderationUserId : String?
  var summary : String?
  var dateCreated : Date?
}
