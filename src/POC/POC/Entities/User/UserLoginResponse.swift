//
//  UserLoginResponse.swift
//  POC
//
//  Created by Don Clore on 12/3/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct UserLoginResponse : Codable {
  var id : String?
  var loginToken : String?
  var email : String?
  var username : String?
  var avatar : MediaSummary?
  var firstName : String?
  var lastName : String?
  var displayName : String?
  var emailVerified : Bool?
}
