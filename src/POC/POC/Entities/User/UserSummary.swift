//
//  UserSummary.swift
//  POC
//
//  Created by Don Clore on 12/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation
struct UserSummary : Codable {
  var id : String?
  var username : String?
  var displayName : String?
  var analyzedDisplayName : String?
  var firstName : String?
  var lastName : String?
  var groups : [String]?
  var isChannelGroup : Bool?
  var score : Double?
}


