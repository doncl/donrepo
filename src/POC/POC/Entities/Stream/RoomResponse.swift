//
//  RoomResponse.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct RoomResponse : Codable {
  enum CodingKeys : String, CodingKey {
    case roomKey = "key"
    case channelKey
    case displayName
    case headerMarkdown
    case opengraphImage
    case description
    case title
    case defaultAccessBehavior
    case accessBehaviors
    case isSubscribed
  }
  
  var roomKey : String?
  var channelKey : String?
  var displayName : String?
  var headerMarkdown : String?
  var opengraphImage : String?
  var description : String?
  var title : String?
  var defaultAccessBehavior : String?
  var accessBehaviors : [AccessBehaviorSummary]?
  var isSubscribed : Bool?
}
