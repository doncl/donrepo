//
//  FoundationResponse2.swift
//  POC
//
//  Created by Don Clore on 11/25/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct FoundationResponse2<T : Codable, U : Codable> : Codable, JSONCoding {
  typealias codableType = FoundationResponse2<T, U>
  var channelInfo : ChannelInfoResponse?
  var serverTime : Date?
  var body : T?
  var body2 : U?
  var userPolicy : UserPolicy?
  var flags : [String]?
}

struct UserSet : Codable {
  var include : Bool?
  var groups : [String]?
  var users : [UserIdentifier]?
}

struct ConfigEntry : Codable {
  var id : String?
  var value : String?
  var values : [String]?
}


