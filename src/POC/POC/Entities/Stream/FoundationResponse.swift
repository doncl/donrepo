//
//  FoundationResponse.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct FoundationResponse<T: Codable> : Codable , JSONCoding {
  typealias codableType = FoundationResponse<T>
  
  var channelInfo : ChannelInfoResponse?
  var serverTime : Date?
  var body : T?
  var userPolicy : UserPolicy?
  var flags : [String]?
}
