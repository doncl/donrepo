//
//  StreamResponse.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct StreamResponse : Codable {
  var userPolicy : UserPolicy?
  var total : Int?
  var documents : [ConversationResponse]?
  // TODO, maybe...do aggregations?
}
