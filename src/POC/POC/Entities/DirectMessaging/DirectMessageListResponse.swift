//
//  DirectMessageResponse.swift
//  POC
//
//  Created by Don Clore on 12/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct DirectMessageListResponse : Codable, JSONCoding {
  typealias codableType = DirectMessageListResponse
  var conversations : [ConversationResponse]?
  var total : Int?
  var users : [String : UserSummary]?
}
