//
//  DirectMessageResponse.swift
//  POC
//
//  Created by Don Clore on 12/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct DirectMessageListResponse : Codable {
  var conversations : [ConversationResponse]?
}
