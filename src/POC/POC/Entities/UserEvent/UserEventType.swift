//
//  UserEventType.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

enum UserEventType : String,  Codable {
  case view = "View"
  case navigation = "Navigation"
  case userRegistered = "UserRegistered"
  case rescoreUser = "RescoreUser"
  case preview = "Preview"
  case enteredText = "EnteredText"
  case clearedText = "ClearedText"
  case sentMessage = "SentMessage"
  case readMore = "ReadMore"
  case readMessage = "ReadMessage"
  case play = "Play"
  case playPercent = "PlayPercent"
  case playSeconds = "PlaySeconds"
  case joinChannel = "JoinChannel"
  case newQuickpost = "NewQuickPost"
  case newStory = "NewStory"
  case newVideoStory = "NewVideoStory"
  case loadMore = "LoadMore"
  case copyLinkStart = "CopyLinkStart"
  case facebookShareStart = "FacebookShareStart"
  case twitterShareStart = "TwitterShareStart"
  case pinterestShareStart = "PinterestShareStart"
  case copyLinkComplete = "CopyLinkComplete"
  case facebookShareComplete = "FacebookShareComplete"
  case twitterShareComplete = "TwitterShareComplete"
  case pinterestShareComplete = "PinterestShareComplete"
  case leaveChannel = "LeaveChannel"
  case subscribe = "Subscribe"
  case changeSetting = "ChangeSetting"
  case uploadMedia = "UploadMedia"
  case adCall = "AdCall"
  case adCallSuccess = "AdCallSuccess"
  case adCallFailure = "AdCallFailure"
}
