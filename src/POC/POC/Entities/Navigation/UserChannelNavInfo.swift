//
//  UserChannelNavInfo.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct UserChannelNavInfo : Codable {
  var channelKey : String?
  var displayName : String?
  var description : String?
  var active : Bool?
  var notificationCount : Int?
  var joined : Bool?
}
