//
//  ChannelInfoResponse.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

enum FeaturedStoryLayoutMode : String, Codable {
  // 1 large story on left, a column with an ad and a small story on right
  case oneplusone = "OnePlusOne"
  case one = "One" // One large story
  case oneplustwo = "OnePlusTwo"  // 1 large story on left, a column with two stories on right
}

struct ChannelInfoResponse : Codable {
  var id : String?
  var key : String?
  var displayName : String?
  var headerWidth : Int?
  var headerHeight : Int?
  var mobileHeaderHeight : Int?
  var headerUrl : String?
  var mobileHeaderImage : String?
  var headerBackgroundImage : String?
  var headerBackgroundColor : String?
  var menuBackgroundColor : String?
  var menuForegroundColor : String?
  
  // SEO properties
  var opengraphImage : String?
  var description : String?
  var title : String?
  var facebookAppId : String?
  var twitterSite : String?
  var instagramHandle : String?
  var pinterestUser : String?
  var siteNameOverride : String?
  var channelShortCode : String?
  var facebookPageId : String?
  
  // Channel Home Layout properties
  var featuredStoryLayoutMode : FeaturedStoryLayoutMode?
  var frontPageWriters : UserSet?
  var rooms : [UserRoomNavInfo]?
  var menu : [MenuItem]?
  var topStories : [ConversationResponse]?
  var joinedUsersCount : Int?
  var elwhaConfigs : [ConfigEntry]?
  var gaTrackingId : String?
  var facebookPixelId : String?
  
  // Ad properties
  var appnexusPlacementId : String?
  var iabTierOne : String?
  var iabTierTwo : String?
  var mavenSensitivityIndex : Int?
  var mavenSensitivityCategory : String?
  
  var maxNumberOfItemsInCarousel : Int?
  var isSubscribed : Bool?
  var channelHomeFlags : [String]?
}
