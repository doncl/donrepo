//
//  UserRoomNavInfo.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct UserRoomNavInfo : Codable {
  var channelKey : String?
  var roomKey : String?
  var key : String?
  var displayName : String?
  var active : Bool = true
  var weight : Int?
  var hasUnread : Bool?
  var notificationCount : Int?
  var joined : Bool?
  var canSee : Bool?
  var isExclusive : Bool?
}
