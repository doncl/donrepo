//
//  UserRoomNavigationRoomInfo.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct UserRoomNavigationRoomInfo : Codable {
  var channelDisplayName : String?
  var channelKey : String?
  var myRooms : [UserRoomNavInfo]?
  var suggestedRooms : [UserRoomNavInfo]?
  var conversations : [ConversationNavigationInfo]?
}
