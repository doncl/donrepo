//
//  ClientAppVersion.swift
//  POC
//
//  Created by Don Clore on 11/27/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

enum ClientAppType : String, Codable {
  case ios = "MavenIosRelease"
  case android = "MavenAndroidRelease"
}

struct ClientAppVersion : Codable {
  var majorVersion : Int?
  var minorVersion : Int?
  var hotfixVersion : Int?
  var buildNumber : Int?
}

struct ClientAppVersionData : Codable, JSONCoding {
  typealias codableType = ClientAppVersionData
  
  var appType : ClientAppType?
  var appStoreUri : String?
  var clientAppVersionSupplied : ClientAppVersion?
  var versionSupplied : String?
  var currentVersion : ClientAppVersion?
  var isNewerVersionRecommended : Bool?
  var isVersionCurrent : Bool?
  var isVersionSupported : Bool?
  var notCurrentRemindingAgainInMinutes : Int?
  var typeSupplied : ClientAppType?
}
