//
//  ConversationStory.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct ConversationStory : Codable {
  var photo : String?
  var photoRenderable : Renderable?
  var title : String?
  var deck : String?
  var video : String?
  var videoRenderable : Renderable?
  var publishDate : Date?
  var news : Bool?
  var videoPending : Bool?
  var isFacebookInstantArticle : Bool?
  var isPhotoImportant : Bool?
}
