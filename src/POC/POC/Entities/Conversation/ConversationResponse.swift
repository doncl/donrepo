//
//  ConversationResponse.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct ConversationResponse : Codable, JSONCoding {
  typealias codableType = ConversationResponse
  
  var title : String?
  var titleSlug : String?
  var description : String?
  var firstMessage : MessageResponse?
  var lastMessage : MessageResponse?
  var numberOfReplies : Int?
  var isLocked : Bool?
  var isDirect : Bool?
  var isPinned : Bool?
  var isSubscribed : Bool?
  var channelKey : String?
  var roomKey : String?
  
  var userPolicy : UserPolicy?
  var authorUserId : String?
  var firstMessageReactionSummaries : [ReactionSummary]?
  var reactionSummaries : [ReactionSummary]?
  var extensions : [ConversationExtensionResponse]?
  var accessBehavior : AccessBehaviorSummary?
  var viewers : [String : UserEventResponse]?
  var conversationResponseStatus : ConversationResponseStatus?
  var hasStory : Bool?
}
