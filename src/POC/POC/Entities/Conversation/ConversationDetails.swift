//
//  ConversationDetails.swift
//  POC
//
//  Created by Don Clore on 11/22/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation


struct ConversationDetails : Codable, JSONCoding {
  typealias codableType = ConversationDetails
  var conversation : ConversationResponse?
  var messages : [MessageResponse]?
}




