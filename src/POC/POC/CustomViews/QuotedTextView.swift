//
//  QuotedTextView.swift
//  POC
//
//  Created by Don Clore on 5/11/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

protocol QuotedTextViewDelegate {
  func cancelTouched()
}

class QuotedTextView: UIView {
  
  static let quoteFontSize : CGFloat = 14.0
  static let vertExtra : CGFloat = 20.0
  static let cornerRadius : CGFloat = 10.0
  
  var delegate : QuotedTextViewDelegate?
  let cancelButtonHorzFudge : CGFloat = -3.0
  let vertPad : CGFloat = 10
  let labelInset : CGSize = CGSize(width: 15.0, height: 5.0)
  let horzPad : CGFloat = 15
  var userPrefix : String
  var text : String
  fileprivate let label : UIPaddedLabel = UIPaddedLabel()
  let cancelButton : CancelButton = CancelButton(frame: .zero)
  let cancelButtonSize : CGSize = CGSize(width: 22, height: 22)
  
  
  init(userPrefix : String, text: String, origin: CGPoint, width: CGFloat) {
    self.userPrefix = userPrefix
    self.text = text
    let height = QuotedTextView.calcHeight()
    let rc = CGRect(x: origin.x, y: origin.y - height - vertPad,
                    width: width, height: height + vertPad)
    
    super.init(frame: rc)
    
    addSubview(label)
    
    label.layer.cornerRadius = QuotedTextView.cornerRadius
    backgroundColor = .clear
    label.textColor = .black
    label.layer.borderColor = UIColor.black.cgColor
    label.layer.borderWidth = 0.3
    label.backgroundColor = .white
    label.textAlignment = .left
    label.numberOfLines = 2
    label.clipsToBounds = true
    label.layer.masksToBounds = true
    label.isUserInteractionEnabled = false
    label.lineBreakMode = .byTruncatingTail
    let font = UIFont.systemFont(ofSize: QuotedTextView.quoteFontSize)
    label.font = font
    label.text = "\"\(userPrefix)\": \(text)"
    
    clipsToBounds = false
    addSubview(cancelButton)
    setLabelAndCancelButtonFrames()
    let tap = UITapGestureRecognizer(target: self,
                                     action: #selector(QuotedTextView.cancelTouched(_:)))
    cancelButton.isUserInteractionEnabled = true
    cancelButton.addGestureRecognizer(tap)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
    
  @objc func cancelTouched(_ sender: UITapGestureRecognizer) {
    delegate?.cancelTouched()
  }
  
  fileprivate class func calcHeight() -> CGFloat {
    let f = UIFont.systemFont(ofSize: quoteFontSize)
    
    let fakeString = NSString(string: "fake String")
    let height = fakeString.size(withAttributes: [NSAttributedStringKey.font : f]).height
    return height * CGFloat(2.0) + vertExtra
  }
  
  fileprivate func setLabelAndCancelButtonFrames() {
    label.frame = bounds.insetBy(dx: labelInset.width + horzPad, dy: labelInset.height)
    let x = label.frame.origin.x + label.frame.width - cancelButtonSize.width / 2 + cancelButtonHorzFudge
    let y = label.frame.origin.y - cancelButtonSize.height / 2
    cancelButton.frame = CGRect(x: x,
                                y: y,
                                width: cancelButtonSize.width,
                                height: cancelButtonSize.height)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    setLabelAndCancelButtonFrames()
  }
}

fileprivate class UIPaddedLabel : UILabel {
  override func drawText(in rect: CGRect) {
    let insets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    setNeedsLayout()
    let drawRect = UIEdgeInsetsInsetRect(rect, insets)
    super.drawText(in: drawRect)
  }
}
