//
//  Net.swift
//  POC
//
//  Created by Don Clore on 11/25/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct Net {
  static func getNetworkData(uri : String, success: @escaping (Data) -> (),
    failure : @escaping (String) -> ()){
    
    let session = URLSession(configuration: .default)
    let headers : [String : String] = [
      "Accept" : "application/json",
      "LoginToken" : "od6qsyKBf7aneAUTfH8XucNQV37EAmhzIjcdbccU1U0=",
      ]
    
    guard let url : URL = URL(string: uri) else {
      return
    }
    let request = NSMutableURLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                      timeoutInterval: 3000)
    
    request.httpMethod = "GET"
    for header in headers {
      request.addValue(header.1, forHTTPHeaderField: header.0)
    }
    
    let task = session.dataTask(with: request as URLRequest) {
      (data, response, error) -> () in
      
      if let error = error {
        failure(error.localizedDescription)
        return
      }
      if let httpResp = response as? HTTPURLResponse {
        if httpResp.statusCode != 200 {
          failure("more bummer, code = \(httpResp.statusCode)")
          return
        }
      }
      guard let data = data else {
        failure("even more bummer, data is nil")
        return
      }
      success(data)
    }
    task.resume()
  }
}
