//
//  ReplyQuoteVC.swift
//  POC
//
//  Created by Don Clore on 5/11/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class ReplyQuoteVC: UIViewController {
  @IBOutlet var table: UITableView!
  
  var quoteView: QuotedTextView?
  
  override func viewDidLoad() {
    super.viewDidLoad()

    table.isUserInteractionEnabled = true
    table.allowsSelection = true
    table.allowsMultipleSelection = false
    table.delegate = self
    table.dataSource = self
    table.reloadData()
  }
  
  fileprivate func text(for indexPath : IndexPath) -> String {

    let s = "Row \(indexPath.row) - and some more text so that we have enough to copy and " +
    " some more text, and keep on going, and so forth"
    
    return s
  }
}

extension ReplyQuoteVC : UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100.0
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    print("tableView rect = \(table.frame)")
    let txt = text(for: indexPath)
    let tableR = table.frame
    let y = tableR.height + tableR.origin.y
    
    if quoteView != nil {
      quoteView!.removeFromSuperview()
      quoteView = nil
    }
    
    quoteView = QuotedTextView(userPrefix: "@some User",
                               text: txt,
                               origin: CGPoint(x: tableR.origin.x, y: y),
                               width: tableR.width)
    
    quoteView!.delegate = self
    view.addSubview(quoteView!)
  }
  
  
}

extension ReplyQuoteVC : QuotedTextViewDelegate {
  func cancelTouched() {
    if let quote = quoteView {
      quote.removeFromSuperview()
    }
    quoteView = nil
  }
  

}

extension ReplyQuoteVC : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 200
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CellId")!
    if let textView = cell.viewWithTag(42) as? UITextView {
      textView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 10)
      textView.textContainer.maximumNumberOfLines = 0
      
      textView.text = text(for: indexPath)
      textView.isUserInteractionEnabled = false

      //textView.backgroundColor = .lightGray
      textView.backgroundColor = .white
    }
    
    cell.selectionStyle = .blue
    
    return cell
  }
  
  
}
