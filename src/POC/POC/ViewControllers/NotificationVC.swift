//
//  NotificationVC.swift
//  POC
//
//  Created by Don Clore on 12/4/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {

  @IBOutlet var table: UITableView!
  
  var notifs : [UserNotification]?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    table.dataSource = self
    table.delegate = self
    table.estimatedRowHeight = 300.0
    table.rowHeight = UITableViewAutomaticDimension
  }
 
  @IBAction func button1Touched(_ sender: UIButton) {
    Net.getNetworkData(uri: "https://www.themaven.net/api/notifications",
      success: { data in

        QueryResponse<UserNotification>.jsonDecode(data, success: { queryResponse in
          guard let notifs = queryResponse.documents else {
            fatalError("gack")
          }

          self.notifs = notifs
          DispatchQueue.main.async {
            self.table.reloadData()
          }
        } , failure: { error in
          fatalError("foo")
        })
      
    }, failure: { error in
      fatalError("gawp")
    })
  }
}

extension NotificationVC : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let notifs = notifs else {
      return 0
    }
    return notifs.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CellId")!
    
    guard let notifs = notifs else {
      fatalError("oops")
    }
    
    if let textView = cell.viewWithTag(42) as? UITextView {
      textView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 10)
      textView.textContainer.maximumNumberOfLines = 0
      let notif = notifs[indexPath.row]
      if let notifString = jsonEncode(t: notif) {
        textView.text = notifString
        textView.backgroundColor = .white
        textView.isScrollEnabled = false
      }
    }
    
    return cell
  }
  
}

extension NotificationVC : UITableViewDelegate {
  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return 400.0
  }
}
