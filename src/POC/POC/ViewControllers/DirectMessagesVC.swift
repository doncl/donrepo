//
//  DirectMessagesVC.swift
//  POC
//
//  Created by Don Clore on 12/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class DirectMessagesVC: UIViewController {
  var response : DirectMessageListResponse?

  @IBOutlet var table: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    table.delegate = self
    table.dataSource = self
  }
  

  @IBAction func button1Pressed(_ sender: UIButton) {
    
    Net.getNetworkData(uri: "https://www.themaven.net/api/navigation/testing/_dms?size=200",
      success: { data in
   
      DirectMessageListResponse.jsonDecode(data, success: { response in

        self.response = response
        
        DispatchQueue.main.async {
          self.table.reloadData()
        }
        
      }, failure: { error in
        fatalError("uggh")
      })
    }, failure: { error in
        fatalError("uggh")
    })
  }
}

extension DirectMessagesVC : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let response = response, let conversations = response.conversations else {
      return 0
    }
    return conversations.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CellId")!

    guard let response = response, let conversations = response.conversations else {
      fatalError("uggh")
    }
    
    let conversation = conversations[indexPath.row]
    if let json = jsonEncode(t: conversation) {
      if let textView = cell.viewWithTag(42) as? UITextView {
        textView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 10)
        textView.textContainer.maximumNumberOfLines = 0
        textView.backgroundColor = .white
        textView.isScrollEnabled = true
        textView.text = json
      }
    }
    return cell
  }
}

extension DirectMessagesVC : UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 500.0
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

}
