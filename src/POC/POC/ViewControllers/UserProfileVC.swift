//
//  UserProfileVC.swift
//  POC
//
//  Created by Don Clore on 11/25/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class UserProfileVC: UIViewController {
  var userData : FoundationResponse<UserProfileResponse>?

  @IBOutlet var textView: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()

  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
  }
  
  @IBAction func button1Pressed(_ sender: UIButton) {
    let uri = "https://www.themaven.net/api/user/p9oPggQEBkWKViLIe9dqEg/profile/channel/the-maven/_foundation"
    
    Net.getNetworkData(uri: uri, success: { data in
      FoundationResponse<UserProfileResponse>.jsonDecode(data, success: { response in
        self.userData = response
        
        FoundationResponse<UserProfileResponse>.jsonEncode(response, success: { jsonString in
          DispatchQueue.main.async {
            self.textView.text = jsonString
            self.textView.isScrollEnabled = true
          }
   
        }, failure: { error in
          
        })
        
      }, failure: { error in
        
      })
    }, failure: { err in
      fatalError("oops")
    })
  }
}
