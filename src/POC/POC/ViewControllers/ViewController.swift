//
//  ViewController.swift
//  POC
//
//  Created by Don Clore on 11/20/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController, JSONCoding {
  typealias codableType = ConversationDetails
  
  @IBOutlet weak var table: UITableView!
  
  var conversation : ConversationDetails?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    table.delegate = self
    table.dataSource = self
    table.estimatedRowHeight = 300.0
    table.rowHeight = UITableViewAutomaticDimension
    table.reloadData()
  }

  @IBAction func button1Touched(_ sender: UIButton) {
   
    let uri = "https://www.themaven.net/api/conversation/y8_2Fc8nd0urh957Jh4mcw/_details?sortOrder=descending&from=0&size=200"
    
    Net.getNetworkData(uri: uri, success: { data in
      ConversationDetails.jsonDecode(data, success: { conversation in
        self.conversation = conversation
        DispatchQueue.main.async {
          self.table.reloadData()
        }
      }, failure: { error in
        fatalError(error)
      })
    }, failure: { error in


      fatalError(error)
    })
  }
}

extension ViewController {
}

extension UIViewController {
  func jsonEncode<T : Codable>(t : T) -> String? {
    let encoder = JSONEncoder()
    encoder.outputFormatting = [.prettyPrinted, .sortedKeys]
    encoder.dateEncodingStrategy = .iso8601
    guard let data = try? encoder.encode(t) else {
      return nil
    }
    guard let jsonString = String(data: data, encoding: .utf8) else {
      return nil
    }
    return jsonString
  }
}

extension ViewController : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let conversation = conversation else {
      return 0
    }
    if section == 0 {
      return 1
    }
    guard let messages = conversation.messages else {
      return 0
    }
    return messages.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CellId")!
    
    guard let conversation = conversation,
      let conversationResponse = conversation.conversation else {
      fatalError("rethink this")
    }
    
    if let textView = cell.viewWithTag(42) as? UITextView {
      textView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 10)
      textView.textContainer.maximumNumberOfLines = 0
      
      if indexPath.section == 0 {
        ConversationResponse.jsonEncode(conversationResponse, success: { json in
          textView.text = json
          textView.backgroundColor = .lightGray
          textView.isScrollEnabled = true
        }, failure: { error in
          
        })
      } else if indexPath.section == 1 {
        guard let messages = conversation.messages else {
          return cell
        }
        let message = messages[indexPath.row]
        MessageResponse.jsonEncode(message, success: { jsonString in
          textView.text = jsonString
          textView.backgroundColor = .white
          textView.isScrollEnabled = false
        }, failure: { error in
          
        })
      }
    }

    return cell
  }
}

extension ViewController : UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.section == 0 {
      return 400.0
    }
    return UITableViewAutomaticDimension
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
}

extension ViewController {
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
  }
}





