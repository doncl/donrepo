//
//  StreamVC.swift
//  POC
//
//  Created by Don Clore on 11/25/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class StreamVC: UIViewController {
  var foundation : FoundationResponse2<StreamResponse, RoomResponse>?
  
  @IBOutlet var table: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    table.delegate = self
    table.dataSource = self
    table.estimatedRowHeight = 300.0
    table.rowHeight = UITableViewAutomaticDimension
    table.reloadData()
  }


override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
}

  @IBAction func button1Pressed(_ sender: UIButton) {
    print("button1 pressed")
    
    let uri = "https://www.themaven.net/api/stream/the-maven/team/_foundation?from=0&size=200"
    
    Net.getNetworkData(uri: uri,
      success: { (data : Data) in
      FoundationResponse2<StreamResponse, RoomResponse>.jsonDecode(data,
        success: {response in
        self.foundation = response
        DispatchQueue.main.async {
          self.table.reloadData()
        }
      }, failure: { error in
         print("error = \(error)")
      })
    }, failure: { error in
      print("error = \(error)")
    })
  }
}

extension StreamVC : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let foundation = foundation,
      let stream = foundation.body,
      let docs = stream.documents,
      let _ = foundation.body2 else {
      return 0
    }
    if section == 0 {
      return 1
    }
    return docs.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CellId")!
    
    guard let foundation = foundation,
      let stream = foundation.body,
      let docs = stream.documents,
      let room = foundation.body2 else {
      fatalError("oops")
    }
    
    if let textView = cell.viewWithTag(42) as? UITextView {
      textView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 10)
      textView.textContainer.maximumNumberOfLines = 0
    
      if indexPath.section == 0 {
        if let roomString = jsonEncode(t: room) {
          textView.text = roomString
          textView.backgroundColor = .lightGray
          textView.isScrollEnabled = true
        }
      } else if indexPath.section == 1 {
        let conversation = docs[indexPath.row]
      
        if let jsonString = jsonEncode(t: conversation) {
          textView.text = jsonString
          textView.backgroundColor = .white
          textView.isScrollEnabled = true
      }
    }
  }
  
    return cell
  }
}

extension StreamVC : UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.section == 0 {
      return 400.0
    }
    return 500.0
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
}
