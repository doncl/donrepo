//
//  ClientAppVersionVC.swift
//  POC
//
//  Created by Don Clore on 11/27/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ClientAppVersionVC: UIViewController {

  @IBOutlet var textView: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    textView.isScrollEnabled = true
  }


  @IBAction func button1Pressed(_ sender: UIButton) {
    
    Net.getNetworkData(uri:
      "https://www.themaven.net/api/clientappcheck?appType=MavenIosRelease&version=1.0.9.579",
       success: { data in
        
        ClientAppVersionData.jsonDecode(data, success: { versionData in
        
            ClientAppVersionData.jsonEncode(versionData, success: { jsonString in
          
              DispatchQueue.main.async {
                self.textView.text = jsonString
              }
          
            }, failure: { error in
          fatalError(error)
        })
        
      }, failure: { error in
        fatalError(error)
      })
    }, failure: { error in
      fatalError(error)
    })
  }
  
}
