//
//  UserModEventsVC.swift
//  POC
//
//  Created by Don Clore on 12/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class UserModEventsVC: UIViewController {
  var scrubber = Scrubber()
  
  var response : QueryResponse<ModerationEvent>?

  @IBOutlet var table: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    table.delegate = self
    table.dataSource = self
    table.rowHeight = UITableViewAutomaticDimension
    table.estimatedRowHeight = 400.0
    table.showsVerticalScrollIndicator = false
    table.showsHorizontalScrollIndicator = false


  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    scrubber.anchorTo(grandParentView: view, parentView: table)
  }
  

  @IBAction func button1Pressed(_ sender: UIButton) {
    print("button1 pressed")
    
    
    Net.getNetworkData(uri:
      "https://www.themaven.net/api/moderation/user/p9oPggQEBkWKViLIe9dqEg?channelKey=testing&from=0&size=200",
       success: { data in
                        
         QueryResponse<ModerationEvent>.jsonDecode(data, success: { response in
  
            self.response = response
          
            DispatchQueue.main.async {
              self.table.reloadData()
            }
                            
         }, failure: { error in
           fatalError(error)
         })
    }, failure: { error in
      fatalError(error)
    })
  }
}

extension UserModEventsVC : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let response = response, let docs = response.documents else {
      return 0
    }
    return docs.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CellId")!
    
    guard let response = response, let docs = response.documents else {
      fatalError("wha??")
    }
    
    if let textView = cell.viewWithTag(42) as? UITextView {
      textView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 10)
      textView.textContainer.maximumNumberOfLines = 0
      
      let event = docs[indexPath.row]
      
      if let userEvent = jsonEncode(t: event) {
          textView.text = userEvent
          textView.backgroundColor = .lightGray
          textView.isScrollEnabled = false
        }
     }
    
    return cell
  }
}

extension UserModEventsVC : UITableViewDelegate {
  func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
    if scrollView.contentSize.height > scrollView.bounds.height {
      scrubber.shown = true
    }
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    scrubber.shown = false
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    guard scrollView.contentSize.height > scrollView.bounds.height,
      scrollView.contentSize.height > 0 else {
      return
    }
    let proportionOfScroll = (scrollView.contentOffset.y / scrollView.contentSize.height) *
      scrollView.bounds.height
    scrubber.setThumbYIfNotBeingTouched(proportionOfScroll)
  }
}




