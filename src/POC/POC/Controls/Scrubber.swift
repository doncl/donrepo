//
//  Scrubber.swift
//  ScrubberExample
//
//  Created by Don Clore on 3/25/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class Scrubber: UIControl {
  var shown : Bool = false {
    didSet {
      if shown {
        self.show(true)
      } else {
        if inSlide == false {
          self.show(false)
        }
      }
    }
  }
  private let shadowColor : CGColor = UIColor.black.cgColor
  private let shadowRadius : CGFloat = 1.7
  private let shadowOffset : CGSize = CGSize(width: 0.5, height: 2.5)
  private let shadowOpacity : Float = 0.4
  private let vertPad : CGFloat = 20.0
  private let width : CGFloat = 29.0
  private let touchSlop : CGFloat = 10.0
  private let trackX : CGFloat = 14.0
  private var trackY : CGFloat = 20.0
  private let trackWidth : CGFloat = 2.0
  private let trackRadii : CGSize = CGSize(width: 20.0, height: 20.0)
  private let thumbRadii : CGSize = CGSize(width: 30.0, height: 30.0)
  
  private let thumbBorderColor : CGColor = #colorLiteral(red: 0.8784313725, green: 0.8784313725, blue: 0.8784313725, alpha: 1).cgColor
  private let thumbBorderWidth : CGFloat = 0.5
  
  private let topChevron : CAShapeLayer = CAShapeLayer()
  private let bottomChevron : CAShapeLayer = CAShapeLayer()
  
  private let chevronYOffset : CGFloat = 18.0
  private let chevronYDelta : CGFloat = 4.0
  private let chevronXDelta : CGFloat = 4.0
  
  private let ridges : CAShapeLayer = CAShapeLayer()
  private let ridgeWidth : CGFloat = 14.0
  private let ridgeColor : CGColor = #colorLiteral(red: 0.8235294118, green: 0.8235294118, blue: 0.8235294118, alpha: 1).cgColor
  private let ridgeThickness : CGFloat = 1.0
  private let ridgeCenterYOffset : CGFloat = 3.0
  private let ridgeX : CGFloat = 5.0
  
  private let scrubberTrack : CAShapeLayer = CAShapeLayer()
  private let scrubberTrackColor : CGColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1).cgColor
  
  private let thumb : CAShapeLayer = CAShapeLayer()
  private let thumbBorder : CAShapeLayer = CAShapeLayer()
  private let thumbX : CGFloat = -13.0
  private let thumbHeight : CGFloat = 60.0
  private var thumbYOrg : CGFloat = 0.0
  
  var startIndex : Int = 1
  var endIndex : Int = 100
  var maxY : CGFloat = 0
  
  var trackRect : CGRect = .zero
  
  var thumbY : CGFloat = 0.0 {
    didSet (oldValue) {
      if thumbY > maxY {
        thumbY = maxY
      } else if thumbY < 0 {
        thumbY = 0
      }
    }
  }
  
  private var thumbRect : CGRect {
    let r : CGRect = CGRect(x: trackRect.origin.x + thumbX , y: trackRect.origin.y + thumbY,
                            width: width, height: thumbHeight)
    return r
  }
  
  private var initialThumbTouchY : CGFloat = 0.0
  private var inSlide : Bool = false
  
  init() {
    super.init(frame: .zero)
    let heightCalc : NSString = "Fake string used to calculateHeight"
    
    trackY = heightCalc.size(withAttributes:
      [NSAttributedStringKey.font : UIFont.preferredFont(forTextStyle: .body)]).height
    
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.clear
    layer.backgroundColor = UIColor.clear.cgColor
    
    layer.addSublayer(scrubberTrack)
    scrubberTrack.fillColor = scrubberTrackColor
    
    isUserInteractionEnabled = true
    isMultipleTouchEnabled = false
    
    thumb.shadowColor = shadowColor
    thumb.shadowOffset = shadowOffset
    thumb.shadowRadius = shadowRadius
    thumb.shadowOpacity = shadowOpacity
    layer.opacity = 0.0
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func show(_ visible: Bool) {
    let targetOpacity : Float = visible ? 1.0 : 0.0
    UIView.animate(withDuration: 0.4, animations: {
      self.layer.opacity = targetOpacity
    })
  }
  
  
  override func layoutSubviews() {
    super.layoutSubviews()

    let trackHeight = bounds.height - (trackY * 2)
    trackRect = CGRect(x: trackX, y: trackY, width: trackWidth,
                       height: trackHeight)
    
    
    let trackPath = UIBezierPath(roundedRect: trackRect,
                                 byRoundingCorners: UIRectCorner.allCorners,
                                 cornerRadii: trackRadii)
    
    maxY = trackHeight - thumbHeight
    scrubberTrack.path = trackPath.cgPath

    thumbYOrg = trackRect.origin.y
  }
  
  func anchorTo(grandParentView: UIView, parentView : UIView) {
    grandParentView.addSubview(self)
    translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      leftAnchor.constraint(equalTo: grandParentView.rightAnchor, constant: -width),
      bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant: -vertPad),
      topAnchor.constraint(equalTo: parentView.topAnchor, constant: vertPad),
      heightAnchor.constraint(equalTo: parentView.heightAnchor, constant: -(2 * vertPad)),
      widthAnchor.constraint(equalToConstant: width),
    ])
  }
  
  
  fileprivate func drawThumbLayer() {
    if thumb.superlayer == nil {
      scrubberTrack.addSublayer(thumb)
      thumb.masksToBounds = false
    }
    
    let r = thumbRect
    
    let thumbPath = UIBezierPath(roundedRect: r, byRoundingCorners: UIRectCorner.allCorners,
                                 cornerRadii: thumbRadii)
    
    thumb.zPosition = 0.0
    thumb.strokeColor = thumbBorderColor
    thumb.lineWidth = thumbBorderWidth
    thumb.fillColor = UIColor.white.cgColor
    thumb.allowsEdgeAntialiasing = true
    thumb.path = thumbPath.cgPath
    
    let center = CGPoint(x: r.origin.x + (r.width / 2),
                         y: r.origin.y + (r.height / 2))
    
    ridges.path = makeThumbRidgePath(center: center)
    ridges.strokeColor = ridgeColor
    
    if ridges.superlayer == nil {
      thumb.addSublayer(ridges)
    }
    
    
    // TOP CHEVRON
    topChevron.strokeColor = ridgeColor
    topChevron.path = makeTopChevronPath(center: center)
    
    if topChevron.superlayer == nil {
      thumb.addSublayer(topChevron)
    }
    
    // BOTTOM CHEVRON
    bottomChevron.strokeColor = ridgeColor
    bottomChevron.path = makeBottomChevronPath(center: center)
    
    if bottomChevron.superlayer == nil {
      thumb.addSublayer(bottomChevron)
    }
  }
  
  fileprivate func makeThumbRidgePath(center: CGPoint) -> CGPath {
    let ridgePath = UIBezierPath()
    ridgePath.lineWidth = 1.0
    ridgePath.lineCapStyle = CGLineCap.round
    
    let halfWidth = ridgeWidth / 2
    
    // Draw top ridge
    var start = CGPoint(x: center.x - halfWidth, y: center.y - ridgeCenterYOffset)
    var end = CGPoint(x: start.x + ridgeWidth, y: start.y)
    ridgePath.move(to: start)
    ridgePath.addLine(to: end)
    
    // Draw middle ridge
    start = CGPoint(x: start.x, y: start.y + ridgeCenterYOffset)
    end = CGPoint(x: end.x, y: start.y)
    ridgePath.move(to: start)
    ridgePath.addLine(to: end)
    
    // Draw bottom ridge
    start = CGPoint(x: start.x, y: start.y + ridgeCenterYOffset)
    end = CGPoint(x: end.x, y: start.y)
    ridgePath.move(to: start)
    ridgePath.addLine(to: end)
    
    return ridgePath.cgPath
  }
  
  fileprivate func makeTopChevronPath(center: CGPoint) -> CGPath {
    let apex = CGPoint(x: center.x, y: center.y - chevronYOffset)
    let path = UIBezierPath()
    path.lineWidth = 1.0
    path.lineCapStyle = CGLineCap.square
    path.lineJoinStyle = CGLineJoin.bevel

    path.move(to: apex)
    let leftLegEnd = CGPoint(x: apex.x - chevronXDelta, y: apex.y + chevronYDelta)
    path.addLine(to: leftLegEnd)
    
    path.move(to: apex)
    let rightLegEnd = CGPoint(x: apex.x + chevronXDelta, y: apex.y + chevronYDelta)
    path.addLine(to: rightLegEnd)
    
    return path.cgPath
  }
  
  fileprivate func makeBottomChevronPath(center: CGPoint) -> CGPath {
    let apex = CGPoint(x: center.x, y: center.y + chevronYOffset)
    let path = UIBezierPath()
    path.lineWidth = 1.0
    path.lineCapStyle = CGLineCap.square
    path.lineJoinStyle = CGLineJoin.bevel
    
    path.move(to: apex)
    let leftLegEnd = CGPoint(x: apex.x - chevronXDelta, y: apex.y - chevronYDelta)
    path.addLine(to: leftLegEnd)
    
    path.move(to: apex)
    let rightLegEnd = CGPoint(x: apex.x + chevronXDelta, y: apex.y - chevronYDelta)
    path.addLine(to: rightLegEnd)
    
    return path.cgPath

  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard let location = locationOfFirstTouch(inTouches: touches) else {
      return
    }
    if hitTest(location) {
      thumbYOrg = thumbY
      initialThumbTouchY = location.y
      inSlide = true
    }
  }
  
  private func hitTest(_ point : CGPoint) -> Bool {
    let r = thumbRect
    let twoXSlop = touchSlop * 2.0
    let slopRect : CGRect = CGRect(x: r.origin.x - touchSlop, y: r.origin.y - touchSlop,
                                   width: r.width + twoXSlop, height: r.height + twoXSlop)
    return slopRect.contains(point)
  }
  
  fileprivate func calculateNewThumbY(_ location: CGPoint) {
    let deltaY = location.y - initialThumbTouchY
    thumbY = thumbYOrg + deltaY
    setNeedsDisplay()
  }
  
  override func draw(_ rect: CGRect) {
    super.draw(rect)
    
    drawThumbLayer()
  }
  
  override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard inSlide else {
      return
    }
    guard let location = locationOfFirstTouch(inTouches: touches) else {
      return
    }
    calculateNewThumbY(location)
  }
  
  override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    defer {
      initialThumbTouchY = 0.0
      inSlide = false
      thumbYOrg = 0.0
      if shown == false {
        show(false)
      }
    }
    
    guard inSlide else {
      return
    }
    
    guard let location = locationOfFirstTouch(inTouches: touches) else {
      return
    }
    
    calculateNewThumbY(location)
  }
  
  private func locationOfFirstTouch(inTouches touches : Set<UITouch>) -> CGPoint? {
    
    guard let touch = touches.first else {
      return nil
    }
    return touch.location(in: touch.view)
  }
}

//MARK - Behavior
extension Scrubber {
  func setThumbYIfNotBeingTouched(_ yOffset : CGFloat) {
    guard false == inSlide else {
      return
    }
    thumbY = yOffset
    setNeedsDisplay()
  }
}



































