//
//  ViewController.swift
//  HelloWorld
//
//  Created by Don Clore on 6/29/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

typealias buttonAction = (UIBarButtonItem) -> ()

class ViewController: UIViewController {
  let bitsPerComponent : Int = 8
  let argbCount : Int = 4
  
  var imageView : UIImageView = UIImageView()

  override func loadView() {
    view = UIView()
    view.backgroundColor = .white
    navigationItem.rightBarButtonItems = []
    
    navigationItem.rightBarButtonItems = [
      barButtomItem(title: "State", selector: #selector(ViewController.loadSample(_:))),
      barButtomItem(title: "A1", selector: #selector(ViewController.loadSample(_:))),
      barButtomItem(title: "A2", selector: #selector(ViewController.loadSample(_:))),
      barButtomItem(title: "A3", selector: #selector(ViewController.loadSample(_:))),
      barButtomItem(title: "Quartz", selector: #selector(ViewController.loadSample(_:))),
      barButtomItem(title: "ColorWheel", selector: #selector(ViewController.loadSample(_:))),
    ]
    
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = .scaleAspectFit
    view.addSubview(imageView)
    
    imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
    imageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    imageView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
    imageView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
  }


  fileprivate func barButtomItem(title: String, selector : Selector ) -> UIBarButtonItem {
    return UIBarButtonItem(title: title, style: .plain, target: self, action: selector)
  }

  func loadSample(_ sender: UIBarButtonItem) {
    guard let items = navigationItem.rightBarButtonItems,
      let item = items.index(of: sender) else {
      return
    }

    switch item {
    case 0:
      imageView.image = buildWithState(size: CGSize(width: 400.0, height: 400.0))
    case 1:
      imageView.image = buildAlphabet1(size: CGSize(width: 400.0, height: 400.0))
    case 2:
      imageView.image = buildAlphabet2(size: CGSize(width: 400.0, height: 400.0))
    case 3:
      imageView.image = buildAlphabet3(size: CGSize(width: 400.0, height: 400.0))
    case 4:
      imageView.image = buildQuartzContext(size: CGSize(width: 400.0, height: 400.0))
    case 5:
      imageView.image = ColorWheel(400.0, true)
    default:
      break
    }
  }
  
  
  fileprivate func buildWithState(size: CGSize) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    guard let context = UIGraphicsGetCurrentContext() else {
      return nil
    }

    // Set initial stroke/fill colors
    UIColor.green.setFill()
    UIColor.purple.setStroke()
    
    // Draw the bunny.
    guard let bunnyPath = BuildBunnyPath() else {
      return nil
    }
    FitPathToRect(bunnyPath, CGRect(x: 0, y: 0, width: 100, height: 100))
    MovePathToPoint(bunnyPath, CGPoint(x: 100, y: 50))
    
    bunnyPath.fill()
    bunnyPath.stroke()
    
    // Save the state.
    context.saveGState()
    
    // Change the fill/state colors
    UIColor.orange.setFill()
    UIColor.blue.setStroke()
    
    // Move then draw again
    let xform = CGAffineTransform(translationX: 50.0, y: 0.0)
    bunnyPath.apply(xform)
    bunnyPath.fill()
    bunnyPath.stroke()
    
    // Restore the previous state.
    context.restoreGState()
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    
    return image
  }
  
  fileprivate func buildAlphabet1(size: CGSize) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    let center = CGPoint(x: size.width / 2, y: size.height / 2)
    let r = size.width * 0.35
    let font = UIFont.boldSystemFont(ofSize: 16.0)

    
    let alphabet : String = "ABCDEFGHIJKLMNOPQRSTUVWXZ"
    for i in 0..<alphabet.characters.count {
      let start = alphabet.index(alphabet.startIndex, offsetBy: i)
      let end = alphabet.index(alphabet.startIndex, offsetBy : i + 1)
      let range = start..<end
      let letter = alphabet.substring(with: range)
      let letterSize = letter.size(attributes: [NSFontAttributeName : font])
      
      let theta = CGFloat.pi - CGFloat(i) * (CGFloat(2.0 ) * CGFloat.pi / CGFloat (26.0))
      let x = center.x + r * sin(theta) - letterSize.width / 2
      let y = center.y + r * cos(theta) - letterSize.height / 2
      
      letter.draw(at: CGPoint(x: x, y: y), withAttributes: [NSFontAttributeName : font])
    }
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    
    return image
  }
  
  fileprivate func buildAlphabet2(size: CGSize) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    guard let context = UIGraphicsGetCurrentContext() else {
      return nil
    }
    
    let center = CGPoint(x: size.width / 2, y: size.height / 2)
    let r = size.width * 0.35
    let font = UIFont.boldSystemFont(ofSize: 16.0)
    
    let alphabet : String = "ABCDEFGHIJKLMNOPQRSTUVWXZ"
    
    // Start by adjusting the context origin
    // This affects all subsequent operations
    
    context.translateBy(x: center.x, y: center.y)
    
    // Iterate through the alphabet
    for i in 0..<alphabet.characters.count {
      
      // Retrieve the letter and measure its display size.
      let start = alphabet.index(alphabet.startIndex, offsetBy: i)
      let end = alphabet.index(alphabet.startIndex, offsetBy : i + 1)
      let range = start..<end
      let letter = alphabet.substring(with: range)
      let letterSize = letter.size(attributes: [NSFontAttributeName : font])
      
      // Calculate the current angular offset
      let theta = CGFloat(i) * CGFloat(2.0) * CGFloat.pi / CGFloat(26.0)
      
      // Encapsulate each stage of the drawing
      context.saveGState()
      
      // Rotate the context
      context.rotate(by: theta)
      
      // Translate up to the edge of the radius and move left by half of the letter width.
      // The height translation is negative as this drawing sequence uses the UIKit coordinate
      // system. Transformations that go up move to lower y values.
      context.translateBy(x: -letterSize.width, y: -r)
      
      // Draw the letter and pop the transform state
      letter.draw(at: .zero, withAttributes: [NSFontAttributeName : font])
      context.restoreGState()
    }
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    return image
  }
  
  fileprivate func buildAlphabet3(size: CGSize) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    guard let context = UIGraphicsGetCurrentContext() else {
      return nil
    }
    
    let center = CGPoint(x: size.width / 2, y: size.height / 2)
    let r = size.width * 0.35
    let font = UIFont.boldSystemFont(ofSize: 16.0)
    
    let alphabet : String = "ABCDEFGHIJKLMNOPQRSTUVWXZ"
    
    var fullSize : CGFloat = 0.0
    
    // Iterate through the alphabet
    // Calculate the full extent
    for i in 0..<alphabet.characters.count {
      
      // Retrieve the letter and measure its display size.
      let start = alphabet.index(alphabet.startIndex, offsetBy: i)
      let end = alphabet.index(alphabet.startIndex, offsetBy : i + 1)
      let range = start..<end
      let letter = alphabet.substring(with: range)
      let letterSize = letter.size(attributes: [NSFontAttributeName : font])
      fullSize += letterSize.width
    }
    
    // Start by adjusting the context origin
    // Translation moves points buy a change in x and y
    context.translateBy(x: center.x, y: center.y)
    
    // Initialize the consumed space.
    var consumedSize : CGFloat = 0.0
    
    // Iterate through the alphabet, consuming that width.
    for i in 0..<alphabet.characters.count {
      
      // Retrieve the letter and measure its display size.
      let start = alphabet.index(alphabet.startIndex, offsetBy: i)
      let end = alphabet.index(alphabet.startIndex, offsetBy : i + 1)
      let range = start..<end
      let letter = alphabet.substring(with: range)
      
      // Measure that letter
      let letterSize = letter.size(attributes: [NSFontAttributeName : font])
      
      // Move the pointer forward, calculating the new percentage of travel along the path.
      consumedSize += letterSize.width / CGFloat(2.0)
      let percent = consumedSize / fullSize
      let theta = percent * CGFloat(2.0) * CGFloat.pi
      consumedSize += letterSize.width / CGFloat(2.0)
      
      // Prepare to draw the letter by saving the state
      context.saveGState()
      
      // Rotate the context by the calculated angle
      context.rotate(by: theta)
      
      // Move to the next letter position
      context.translateBy(x: -letterSize.width / 2.0, y: -r)
      
      // Draw the letter
      letter.draw(at: .zero, withAttributes: [NSFontAttributeName : font])
      
      // Reset the context back to the way it was.
      context.restoreGState()
    }
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    
    return image
  }
  
  fileprivate func buildQuartzContext(size: CGSize) -> UIImage? {
    let height = Int(size.height)
    let width = Int(size.width)
    
    let rect = CGRect(origin: .zero, size: size).insetBy(dx: 30.0, dy: 80.0)
    
    // Create a color space
    let colorSpace = CGColorSpaceCreateDeviceRGB()
    
    // Create the bitmap context
    guard let context = CGContext(data: nil, width: width, height: height,
      bitsPerComponent: bitsPerComponent, bytesPerRow: width * argbCount, space: colorSpace,
      bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue) else {
        return nil
    }
    
    UIGraphicsPushContext(context)
    
    // UIKit drawing in Quartz context
    let path = UIBezierPath(ovalIn: rect)
    path.lineWidth = 4.0
    UIColor.gray.setStroke()
    path.stroke()
    
    UIGraphicsPopContext()
    
    // Convert to image
    guard let imageRef = context.makeImage() else {
      return nil
    }
    
    let image = UIImage(cgImage: imageRef)
    
    return image
  }
}

