//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#ifndef HELLOWORLD_BRIDGING_HEADER_H
#define HELLOWORLD_BRIDGING_HEADER_H
@import UIKit;
@import QuartzCore;

#import "Utility.h"
#endif
