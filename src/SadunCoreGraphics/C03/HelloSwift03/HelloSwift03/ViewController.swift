//
//  ViewController.swift
//  HelloSwift03
//
//  Created by Don Clore on 6/30/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  let greenColor : UIColor =  #colorLiteral(red: 0.4901960784, green: 0.6352941176, blue: 0.2470588235, alpha: 1)
  let purpleColor : UIColor = #colorLiteral(red: 0.3882352941, green: 0.2431372549, blue: 0.6352941176, alpha: 1)
  
  var imageView : UIImageView = UIImageView()
  var button : UIButton = UIButton(type: .roundedRect)
  
  override func loadView() {
    view = UIView()
    view.backgroundColor = .white
    navigationItem.rightBarButtonItems = []
    
    navigationItem.rightBarButtonItems = [
      barButtomItem(title: "Pattern", selector: #selector(ViewController.loadSample(_:))),
      barButtomItem(title: "Caps", selector: #selector(ViewController.loadSample(_:))),
      barButtomItem(title: "No Caps", selector: #selector(ViewController.loadSample(_:))),
      barButtomItem(title: "Watermark", selector: #selector(ViewController.loadSample(_:))),
    ]
    
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = .scaleAspectFit
    view.addSubview(imageView)
    
    NSLayoutConstraint.activate([
      imageView.topAnchor.constraint(equalTo: view.topAnchor),
      imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      imageView.widthAnchor.constraint(equalTo: view.widthAnchor),
      imageView.heightAnchor.constraint(equalTo: view.heightAnchor),
    ])
    
    button.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(button)
    button.setTitle("  Sample Button  ", for: .normal)
    button.setTitleColor(.white, for: .normal)
    NSLayoutConstraint.activate([
      button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      button.centerYAnchor.constraint(equalTo: view.centerYAnchor),
    ])
    button.isHidden = true
  }

  func loadSample(_ sender: UIBarButtonItem) {
    guard let items = navigationItem.rightBarButtonItems,
      let index = items.index(of: sender) else {
      return
    }
  
    switch index {
    case 0:
      if let color = view.backgroundColor, color == .white, let pattern = buildPattern() {
        view.backgroundColor = UIColor(patternImage: pattern)
      }
    case 1:
      button.isHidden = false
      if let backImage = buildButtonImage(useCapInsets: true) {
        button.setBackgroundImage(backImage, for: .normal)
      }

    case 2:
      button.isHidden = false
      if let backImage = buildButtonImage(useCapInsets: false) {
        button.setBackgroundImage(backImage, for: .normal)
      }
      break
    case 3:
      if let _ = imageView.image {
        imageView.image = nil
      } else if let watermark = buildWatermarking(targetSize: CGSize(width: 400, height: 300)) {
        imageView.image = watermark
      }
      break
      
    default:
      break
    }
  }
  
  fileprivate func buildPattern() -> UIImage? {
    // Create a small tile
    let targetSize = CGSize(width: 80, height: 80)
    let targetRect = SizeMakeRect(targetSize)
    
    // Start a new image
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    // Fill background with pink
    #colorLiteral(red: 0.9803921569, green: 0.8549019608, blue: 0.8666666667, alpha: 1).set()
    UIRectFill(targetRect)
    
    // Draw a couple of dogcattle in gray
    UIColor.gray.set()
    
    // First, bigger with interior detail in the top-left
    let weeRect = CGRect(x: 0, y: 0, width: 40, height: 40)
    let moof : UIBezierPath = BuildMoofPath()
    FitPathToRect(moof, weeRect)
    RotatePath(moof, CGFloat.pi / 4.0)
    moof.fill()
    
    // Then smaller, flipped around, and offset down and right.
    RotatePath(moof, CGFloat.pi)
    OffsetPath(moof, CGSize(width: 40, height: 40))
    ScalePath(moof, 0.5, 0.5)
    moof.fill()
    
    // Retrieve and return the pattern image
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    
    return image
  }
  
  fileprivate func buildButtonImage(useCapInsets : Bool) -> UIImage? {
    let targetSize = CGSize(width: 40, height: 40)
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    // Create the outer rounded rectangle
    let targetRect = SizeMakeRect(targetSize)
    let path = UIBezierPath(roundedRect: targetRect, cornerRadius: 12.0)
    
    // Fill and stroke it
    purpleColor.setFill()
    path.fill()
    path.stroke(inside: 2.0)
    
    // Create the inner rounded rectangle
    let innerPath = UIBezierPath(roundedRect: targetRect.insetBy(dx: 4.0, dy: 4.0),
                                 cornerRadius: 8.0)
    
    // Stroke it
    innerPath.stroke(inside: 1.0)
    
    // Retrieve the initial image
    guard let baseImage = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    if false == useCapInsets {
      return baseImage
    }
    
    return baseImage.resizableImage(withCapInsets:
      UIEdgeInsets(top: 12.0, left: 12, bottom: 12, right: 12))
  }
  
  fileprivate func buildWatermarking(targetSize: CGSize) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    guard let context = UIGraphicsGetCurrentContext() else {
      return nil
    }
    
    // Draw the original image onto the context
    let targetRect = SizeMakeRect(targetSize)
    guard let sourceImage = UIImage(named: "pronghorn.jpg") else {
      return nil
    }
    
    let imgRect = RectByFillingRect(SizeMakeRect(sourceImage.size), targetRect)
    sourceImage.draw(in: imgRect)
    
    // Create a string
    let watermark : String = "watermark"
    guard let font = UIFont(name: "HelveticaNeue", size: 48.0) else {
      return nil
    }
    let size = watermark.size(attributes: [NSFontAttributeName : font])
    let stringRect = RectCenteredInRect(SizeMakeRect(size), targetRect)
    
    // Rotate the context
    let center = RectGetCenter(targetRect)
    context.translateBy(x: center.x, y: center.y)
    context.rotate(by: CGFloat.pi / 4.0)
    context.translateBy(x: -center.x, y: -center.y)
    
    // Draw the string, using a blend mode...
    context.setBlendMode(.difference)
    watermark.draw(in: stringRect, withAttributes: [
      NSFontAttributeName : font, NSForegroundColorAttributeName : UIColor.white
    ])
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    
    return image
  }
  
  
  fileprivate func barButtomItem(title: String, selector : Selector ) -> UIBarButtonItem {
    return UIBarButtonItem(title: title, style: .plain, target: self, action: selector)
  }
  
}

