//
//  ViewController.swift
//  HelloSwift04
//
//  Created by Don Clore on 6/30/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  let greenColor : UIColor = #colorLiteral(red: 0.4901960784, green: 0.6352941176, blue: 0.2470588235, alpha: 1)
  let purpleColor : UIColor = #colorLiteral(red: 0.3882352941, green: 0.2431372549, blue: 0.6352941176, alpha: 1)
  
  var imageView : UIImageView = UIImageView()
  
  override func loadView() {
    view = UIView()
    view.backgroundColor = .white
    navigationItem.rightBarButtonItems = []
    
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = .scaleAspectFit
    view.addSubview(imageView)
    
    NSLayoutConstraint.activate([
      imageView.topAnchor.constraint(equalTo: view.topAnchor),
      imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      imageView.widthAnchor.constraint(equalTo: view.widthAnchor),
      imageView.heightAnchor.constraint(equalTo: view.heightAnchor),
    ])
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    if let image = build(targetSize: CGSize(width: 300.0, height: 300.0)) {
      imageView.image = image
    }
  }

  fileprivate func build(targetSize: CGSize) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    let fullRect = SizeMakeRect(targetSize).insetBy(dx: 30, dy: 30)
    
    // Establish a new path
    let bezierPath = UIBezierPath()
    
    // Create an ellipse as the face outline and append it to the path
    let inset = fullRect.insetBy(dx: 32.0, dy: 32.0)
    let faceOutline = UIBezierPath(ovalIn: inset)
    bezierPath.append(faceOutline)
    
    // Move in again, for the eyes and mouth
    let insetAgain = inset.insetBy(dx: 64.0, dy: 64.0)
    
    // Calculate a radius
    let minX = insetAgain.minX
    let maxY = insetAgain.maxY
    let referencePoint = CGPoint(x: minX, y: maxY)
    let center = RectGetCenter(inset)
    let radius = PointDistanceFromPoint(referencePoint, center)
    
    // Add a smile from 40 degrees around to 140 degrees
    let smile = UIBezierPath(arcCenter: center, radius: radius,
      startAngle: RadiansFromDegrees(140), endAngle: RadiansFromDegrees(40), clockwise: false)
    
    bezierPath.append(smile)
    
    // Build Eye 1
    let eye1X = insetAgain.minX
    let eye1Y = insetAgain.minY
    let p1 = CGPoint(x: eye1X, y: eye1Y)
    let eyeRect1 = RectAroundCenter(p1, CGSize(width: 20, height: 20))
    let eye1 = UIBezierPath(rect: eyeRect1)
    bezierPath.append(eye1)
    
    // And Eye 2
    let eye2X = insetAgain.maxX
    let eye2Y = insetAgain.minY
    let p2 = CGPoint(x: eye2X, y: eye2Y)
    let eyeRect2 = RectAroundCenter(p2, CGSize(width: 20.0, height: 20.0))
    let eye2 = UIBezierPath(rect: eyeRect2)
    bezierPath.append(eye2)
    
    // Draw the complete path
    bezierPath.lineWidth = 4.0
    bezierPath.stroke()
    
    // Listing 4-2.  See Utility.m
    if let p = BuildStarPath() {
      FitPathToRect(p, inset)
      p.stroke()
    }

    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    
    return image
  }
}



































