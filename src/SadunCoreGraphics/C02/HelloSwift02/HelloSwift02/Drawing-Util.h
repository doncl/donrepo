/*
 
 Erica Sadun, http://ericasadun.com
 
 */

#import <Foundation/Foundation.h>
@import UIKit;
@import QuartzCore;

void DrawStringCenteredInRect(NSString *string, UIFont *font, UIColor *color, CGRect rect);
