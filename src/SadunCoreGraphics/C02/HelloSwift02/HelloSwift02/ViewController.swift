//
//  ViewController.swift
//  HelloSwift02
//
//  Created by Don Clore on 6/29/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  let imageView : UIImageView = UIImageView()
  let greenColor : UIColor = UIColor(red: 125.0 / 255.0, green: 162.0 / 255.0, blue: 63.0 / 255.0, alpha: 1.0)
  
  let purpleColor : UIColor = UIColor(red: 99.0 / 255.0, green: 62.0 / 255.0, blue: 162.0 / 255.0, alpha: 1.0)
  
  override func loadView() {
    view = UIView()
    view.backgroundColor = .white
    navigationItem.rightBarButtonItems = []
    
    navigationItem.rightBarButtonItems = [
      barButtomItem(title: "CGRect.divided", selector: #selector(ViewController.loadSample(_:))),
    ]
    
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = .scaleAspectFit
    view.addSubview(imageView)
    
    NSLayoutConstraint.activate([
      imageView.topAnchor.constraint(equalTo: view.topAnchor),
      imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      imageView.widthAnchor.constraint(equalTo: view.widthAnchor),
      imageView.heightAnchor.constraint(equalTo: view.heightAnchor),
    ])
  }
  
  func loadSample(_ sender: UIBarButtonItem) {
    guard let items = navigationItem.rightBarButtonItems,
      let index = items.index(of: sender) else {
      return
    }
    
    switch index {
    case 0:
      imageView.image = build(CGSize(width: 400.0, height: 400.0))
    default:
      break
    }
  }
  
  fileprivate func barButtomItem(title: String, selector : Selector ) -> UIBarButtonItem {
    return UIBarButtonItem(title: title, style: .plain, target: self, action: selector)
  }
  
  fileprivate func build(_ size: CGSize) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    var rect = SizeMakeRect(size)
    
    // Slice a section off the left and color it orange
    var sliceAndRemainder = rect.divided(atDistance: 80.0, from: .minXEdge)
    var slice = sliceAndRemainder.0
    var remainder = sliceAndRemainder.1
    
    UIColor.orange.set()
    var path = UIBezierPath(rect: slice)
    path.fill()
    
    // Slice the other portion in half horizontally
    rect = remainder
    sliceAndRemainder = rect.divided(atDistance: remainder.size.height / 2.0, from: .minYEdge)
    slice = sliceAndRemainder.0
    remainder = sliceAndRemainder.1
    
    // Tint the sliced portion purple
    UIColor.purple.set()
    path = UIBezierPath(rect: slice)
    path.fill()
    
    // From Listing 2-6
    //  Drawing a centered string is in Drawing-Util.m
    DrawStringCenteredInRect("Purple", UIFont.boldSystemFont(ofSize: 16.0), .black, slice)
    
    // Slice a 20 point segment from the bottom left.
    // Draw it in gray.
    
    rect = remainder
    sliceAndRemainder = rect.divided(atDistance: 20.0, from: .minXEdge)
    slice = sliceAndRemainder.0
    remainder = sliceAndRemainder.1
    UIColor.gray.set()
    path = UIBezierPath(rect: slice)
    path.fill()
    
    // Add another 20-point segment from the bottom right.
    // Draw it in gray.
    rect = remainder
    sliceAndRemainder = rect.divided(atDistance: 20.0, from: .maxXEdge)
    slice = sliceAndRemainder.0
    remainder = sliceAndRemainder.1
    
    // use same color on the right
    path = UIBezierPath(rect: slice)
    path.fill()
    
    // Fill the rest in brown 
    UIColor.brown.set()
    path = UIBezierPath(rect: remainder)
    path.fill()
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    
    return image
  }
}

