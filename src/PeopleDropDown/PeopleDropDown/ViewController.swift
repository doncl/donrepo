//
//  ViewController.swift
//  PeopleDropDown
//
//  Created by Don Clore on 12/11/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


  @IBAction func peopleButtonTouched(_ sender: UIButton) {
    
    let tip = PeopleDropDown(["paul", "ringo", "john", "george"])
    tip.show(animated: true, forView: sender, withinSuperview: view)
    
  }
}

class PeopleDropDown : EasyTipView {
  
  init(_ people: [String]) {
    var prefs = EasyTipView.globalPreferences
    prefs.drawing.backgroundColor = UIColor.lightGray
    prefs.drawing.foregroundColor = UIColor.black
    
    
    super.init(text: people.joined(separator: "\r\n"), preferences : prefs, delegate: nil)
  }
  
  required public init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}

