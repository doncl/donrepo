//
// Created by Don Clore on 9/28/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PinchyBaseButton.h"

@interface PinchyGoButton : PinchyBaseButton
@end