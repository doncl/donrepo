//
// Created by Don Clore on 9/28/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "PinchyGoButton.h"


@implementation PinchyGoButton
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

    // Draw circle
    CGContextFillEllipseInRect(self.ref, rect);

    CGContextRef context = UIGraphicsGetCurrentContext();
    //CGContextSetRGBFillColor(context, 255.0 / 255.0, 0 / 255.0, 0 / 255.0, 1.0);
    //CGContextFillEllipseInRect(context, rect);

    CGRect innerCircle = CGRectMake(rect.origin.x + (rect.size.width * .2),
            rect.origin.y + (rect.size.height * .2) - 1.0, rect.size.width * .6,
            rect.size.height * .6);


    CGContextSetRGBFillColor(context, 231.0 / 255.0, 231.0 / 255.0, 231.0 / 255.0, 0.6);
    CGContextFillEllipseInRect(context, innerCircle);
    //CGContextSetRGBStrokeColor(context, 50 / 255.0, 52 / 255.0, 55 / 255.0, 1.0);
    CGContextSetLineWidth(context, 1.0f);
    CGContextSetRGBStrokeColor(context, 0, 0, 0, 1.0);
    CGContextStrokeEllipseInRect(context, innerCircle);

    //CGContextSetLineWidth(context, 0.5f);
    CGRect oneLessCircle = CGRectMake(innerCircle.origin.x + 1.0f, innerCircle.origin.y + 1.0f,
                            innerCircle.size.width - 2.0f, innerCircle.size.height - 2.0f);

    CGContextSetRGBStrokeColor(context, 50 / 255.0, 52 / 255.0, 55 / 255.0, 1.0);
    CGContextStrokeEllipseInRect(context, oneLessCircle);
}
@end