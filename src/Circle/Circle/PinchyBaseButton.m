//
// Created by Don Clore on 9/28/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "PinchyBaseButton.h"


@implementation PinchyBaseButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.opaque = NO;
    }

    return self;
}

- (UIColor *)lineColor
{
    return [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.6];
}

- (CGContextRef)ref
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.0f);
    CGContextSetRGBStrokeColor(context, 231.0 / 255.0, 231.0 / 255.0, 231.0 / 255.0, 0.6);
    CGContextSetRGBFillColor(context, 50 / 255.0, 52 / 255.0, 55 / 255.0, 0.6);

    return context;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
}

- (void)drawLine:(CGPoint)lineStart lineEnd:(CGPoint)lineEnd
{
    CGContextSetStrokeColorWithColor(self.ref, self.lineColor.CGColor);
    CGContextMoveToPoint(self.ref, lineStart.x, lineStart.y);
    CGContextAddLineToPoint(self.ref, lineEnd.x, lineEnd.y);

    // now draw it.
    CGContextStrokePath(self.ref);
}


@end