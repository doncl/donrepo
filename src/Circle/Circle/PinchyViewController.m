//
//  PinchyViewController.m
//  Circle
//
//  Created by Don Clore on 9/26/15.
//  Copyright © 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "PinchyViewController.h"
#import "PinchyResizeButton.h"
#import "PinchyGoButton.h"

@interface PinchyViewController ()
@end

@implementation PinchyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor grayColor];
    self.imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"image.jpg"]];
    self.imageView.frame = self.view.frame;
    self.imageView.userInteractionEnabled = YES;
    self.imageView.hidden = NO;
    [self.view addSubview:self.imageView];

    UIPinchGestureRecognizer *pinch =
        [[UIPinchGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(pinchy:)];

    [self.view addGestureRecognizer:pinch];

    CGRect circleRect = CGRectMake(15, self.imageView.frame.size.height - 45, 30, 30);

    self.circleView = [[PinchyResizeButton alloc] initWithFrame:circleRect];
    [self.imageView addSubview:self.circleView];

    CGRect goRect = CGRectMake(self.imageView.frame.size.width - 45,
            self.imageView.frame.size.height - 45, 30, 30);

    self.goView = [[PinchyGoButton alloc] initWithFrame:goRect];
    [self.imageView addSubview:self.goView];
}

- (void)pinchy:(UIPinchGestureRecognizer *)gr
{
    if (UIGestureRecognizerStateBegan == gr.state ||
            UIGestureRecognizerStateChanged == gr.state) {

        // Use the x or y scale, they should be the same for typical zooming (non-skewing)
        float currentScale = [[gr.view.layer valueForKeyPath:@"transform.scale.x"] floatValue];

        // Variables to adjust the max/min values of zoom
        float minScale = 0.6;
        float maxScale = 1.0;
        float zoomSpeed = .5;

        float deltaScale = gr.scale;

        // You need to translate the zoom to 0 (origin) so that you
        // can multiply a speed factor and then translate back to "zoomSpace" around 1
        deltaScale = ((deltaScale - 1) * zoomSpeed) + 1;

        //  A deltaScale is ~0.99 for decreasing or ~1.01 for increasing
        //  A deltaScale of 1.0 will maintain the zoom size
        deltaScale = MIN(deltaScale, maxScale / currentScale);
        deltaScale = MAX(deltaScale, minScale / currentScale);

        CGAffineTransform zoomTransform = CGAffineTransformScale(gr.view.transform, deltaScale, deltaScale);
        gr.view.transform = zoomTransform;

        if (deltaScale > 0.01) {
            CGFloat inverse = 1.0 / deltaScale;
            self.circleView.transform = CGAffineTransformScale(self.circleView.transform,
                    inverse, inverse);

            self.goView.transform = CGAffineTransformScale(self.goView.transform, inverse,
                    inverse);
        }
        // Reset to 1 for scale delta's
        //  Note: not 0, or we won't see a size: 0 * width = 0
        gr.scale = 1;
    }
}
@end
