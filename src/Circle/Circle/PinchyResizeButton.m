//
// Created by Don Clore on 9/26/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "PinchyResizeButton.h"


@implementation PinchyResizeButton

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

    // Draw circle.
    CGContextFillEllipseInRect(self.ref, rect);

    // 1st chevron.
    CGPoint lineStart = CGPointMake(rect.origin.x + 15, rect.origin.y + 8);
    CGPoint lineEnd = CGPointMake(lineStart.x + 6, lineStart.y);
    [self drawLine:lineStart lineEnd:lineEnd];

    lineStart = lineEnd;
    lineEnd = CGPointMake(lineStart.x, lineStart.y + 7);
    [self drawLine:lineStart lineEnd:lineEnd];

    // 2nd chevron.
    lineStart = CGPointMake(rect.origin.x + 8, rect.origin.y + 16);
    lineEnd = CGPointMake(lineStart.x, lineEnd.y + 7);
    [self drawLine:lineStart lineEnd:lineEnd];

    lineStart = lineEnd;
    lineEnd = CGPointMake(lineStart.x + 6, lineStart.y);
    [self drawLine:lineStart lineEnd:lineEnd];
}
@end