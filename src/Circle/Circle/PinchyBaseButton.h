//
// Created by Don Clore on 9/28/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PinchyBaseButton : UIView
@property (readonly) CGContextRef ref;
@property (readonly) UIColor *lineColor;

- (void)drawLine:(CGPoint)lineStart lineEnd:(CGPoint)lineEnd;
@end