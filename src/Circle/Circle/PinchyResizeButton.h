//
// Created by Don Clore on 9/26/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PinchyBaseButton.h"


@interface PinchyResizeButton : PinchyBaseButton
@end