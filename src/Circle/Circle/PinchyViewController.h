//
//  PinchyViewController.h
//  Circle
//
//  Created by Don Clore on 9/26/15.
//  Copyright © 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PinchyResizeButton;
@class PinchyGoButton;

@interface PinchyViewController : UIViewController
@property (nonatomic, strong) UIImageView *imageView;
@property (strong, nonatomic) PinchyResizeButton *circleView;
@property (strong, nonatomic) PinchyGoButton *goView;
@end

