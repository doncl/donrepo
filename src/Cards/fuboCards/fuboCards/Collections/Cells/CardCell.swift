//
//  CardCell.swift
//  fuboCards
//
//  Created by Don Clore on 8/24/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class CardCell: UICollectionViewCell {
  let face : UIImageView = UIImageView()
  let back : UIImageView = UIImageView()
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    addSubview(back)
    addSubview(face)
    
    face.image = #imageLiteral(resourceName: "ace_of_spades2")
    
    face.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      face.topAnchor.constraint(equalTo: topAnchor),
      face.leadingAnchor.constraint(equalTo: leadingAnchor),
      face.trailingAnchor.constraint(equalTo: trailingAnchor),
      face.bottomAnchor.constraint(equalTo: bottomAnchor),
    ])
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
