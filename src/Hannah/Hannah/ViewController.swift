//
//  ViewController.swift
//  Hannah
//
//  Created by Don Clore on 1/27/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  init() {
    super.init(nibName:  nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = .white
    
    let slack = SlackyViewController()
    addChildViewController(slack)
    slack.didMove(toParentViewController: self)

    view.addSubview(slack.view)
    
    slack.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    slack.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    slack.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    slack.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
  }
}

