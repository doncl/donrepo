//
//  SlackyViewController.swift
//  Hannah
//
//  Created by Don Clore on 1/27/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit
import SlackTextViewController
import AsyncDisplayKit

class SlackyViewController: SLKTextViewController {
  var tableNode : ASTableNode = ASTableNode()
  
  override var tableView: ASTableView {
    get {
      return super.scrollView as! ASTableView
    }
  }
  
  init() {
    super.init(scrollView: tableNode.view)
    tableNode.dataSource = self
    tableNode.reloadData()
  }
  
  required init(coder decoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
  }

}

extension SlackyViewController : ASTableDataSource {
  func numberOfSections(in tableNode: ASTableNode) -> Int {
    return 1
  }

  
  func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
    return 4
  }
  
  func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath)
    -> ASCellNodeBlock {
    
    return {
      return Cell(indexPath: indexPath, transform: self.tableView.transform)
    }
  }
}

class Cell : ASCellNode {
  struct Constants {
    static let atMentionAttrName : String = "atMentionAttribute"
    static let bypassLinkAttributeName : String = "NSLinkAttributeName"
  }
  var node : ASTextNode
  var cachedTransform : CGAffineTransform
  var row : Int
  
  init(indexPath: IndexPath, transform: CGAffineTransform) {
    // save, but don't touch off main thread.
    cachedTransform = transform
    row = indexPath.row
    node = ASTextNode()
    
    node.isUserInteractionEnabled = true
    
    node.linkAttributeNames = [NSLinkAttributeName, Constants.atMentionAttrName,
                                   Constants.bypassLinkAttributeName]

  
    super.init()
  
    addSubnode(node)
  }
  
  override func didLoad() {
    // Gotta do on main thread
    view.transform = cachedTransform
    let text = "CellNode number \(row)"
    
    node.attributedText = NSAttributedString(string: text)
  }
  
  override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
    let stack = ASStackLayoutSpec.vertical()
    stack.spacing = 0.0
    stack.justifyContent = .start
    
    stack.children = [node]
    
    return stack
  }
}
