//
//  CustomAppearanceViewController.swift
//  NavBar
//
//  Created by Don Clore on 8/21/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit
import SnapKit
import QuartzCore

class CustomAppearanceViewController: UIViewController {
  var cities : [String] = []
  var table : UITableView!
  var backgroundSwitcher : UISegmentedControl!

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  init(frame: CGRect) {
    super.init(nibName: nil, bundle: nil)
    view.frame = frame
    view.backgroundColor = UIColor.white
    table = UITableView(frame: frame, style: .plain)
    backgroundSwitcher = UISegmentedControl(items: ["Image", "Transparent", "Color"])
    backgroundSwitcher.addTarget(self,
      action: #selector(CustomAppearanceViewController.configureNewNavBarBackground(_:)),
      for: .valueChanged)
    table.dataSource = self
    table.delegate = self
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()

    let citiesJSONURL = Bundle.main.url(forResource: "Cities", withExtension: "json")!
    let citiesData = try! Data(contentsOf: citiesJSONURL)
    cities = try! JSONSerialization.jsonObject(with: citiesData, options: []) as! [String]
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    view.addSubview(table)
    view.addSubview(backgroundSwitcher)

    let nav = navigationController!
    nav.setToolbarHidden(false, animated: animated)

    let backgroundSwitcherItem = UIBarButtonItem(customView: backgroundSwitcher)
    let barButtonItems : [UIBarButtonItem] = [
      UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
      backgroundSwitcherItem,
      UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    ]
    setToolbarItems(barButtonItems, animated: animated)

    table.snp.makeConstraints{ (make) -> Void in
      //make.top.equalTo(nav.navigationBar.snp_bottom)
      make.top.equalTo(view)
      make.leading.equalTo(view)
      make.trailing.equalTo(view)
      make.bottom.equalTo(nav.toolbar.snp.top)
    }

    applyImageBackgroundToTheNavigationBar()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    if let nav = navigationController {
      nav.setToolbarHidden(true, animated: animated)
    }
  }
}

extension CustomAppearanceViewController {
  override func viewWillTransition(to size: CGSize, with
  coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)

    coordinator.animate(alongsideTransition: { (context: UIViewControllerTransitionCoordinatorContext) in
      self.configureNewNavBarBackground(self.backgroundSwitcher)

    }, completion: nil)
  }
}

// MARK: NavBar background
extension CustomAppearanceViewController {
  func applyImageBackgroundToTheNavigationBar() {
    // These background images contain a small pattern which is displayed
    // in the lower right corner of the navigation bar.

    var background = UIImage(named: "NavigationBarDefault")!
    var backgroundImageLandscape = UIImage(named: "NavigationBarLandscapePhone")!

    // Both of the above images are smaller than the navigation bar's
    // size.  To enable the images to resize gracefully while keeping their
    // content pinned to the bottom right corner of the bar, the images are
    // converted into resizable images width edge insets extending from the
    // bottom up to the second row of pixels from the top, and from the
    // right over to the second column of pixels from the left.  This results
    // in the topmost and leftmost pixels being stretched when the images
    // are resized.  Not coincidentally, the pixels in these rows/columns
    // are empty.

    background = background.resizableImage(
      withCapInsets: UIEdgeInsets(top: 0.0, left: 0.0, bottom: background.size.height - 1,
        right: background.size.width - 1))

    backgroundImageLandscape = backgroundImageLandscape.resizableImage(
      withCapInsets: UIEdgeInsets(top: 0.0, left: 0.0, bottom: backgroundImageLandscape.size.height - 1,
        right: backgroundImageLandscape.size.width - 1))

    // You should use the appearance proxy to customize the appearance of
    // UIKit elements.  However changes made to an element's appearance
    // proxy do not effect any existing instances of that element currently
    // in the view hierarchy.  Normally this is not an issue because you
    // will likely be performing your appearance customizations in
    // -application:didFinishLaunchingWithOptions:.  However, this example
    // allows you to toggle between appearances at runtime which necessitates
    // applying appearance customizations directly to the navigation bar.
    /* id navigationBarAppearance = [UINavigationBar appearanceWhenContainedIn:[NavigationController class], nil]; */

    let nav = navigationController!
    let navigationBarAppearance = nav.navigationBar
    navigationBarAppearance.setBackgroundImage(background, for: .default)
    navigationBarAppearance.setBackgroundImage(backgroundImageLandscape,
      for: .compact)
  }

  //| ----------------------------------------------------------------------------
  //! Configures the navigation bar to use a transparent background (see-through
  //! but without any blur).
  //
  func applyTransparentBackgroundToTheNavigationBar(_ opacity: CGFloat) {
    var transparentBackground : UIImage

    // The background of a navigation bar switches from being translucent
    // to transparent when a background image is applied.  The intensity of
    // the background image's alpha channel is inversely related to the
    // transparency of the bar.  That is, a smaller alpha channel intensity
    // results in a more transparent bar and vis-versa.
    //
    // Below, a background image is dynamically generated with the desired
    // opacity.

    let nav = navigationController!
    let navBar = nav.navigationBar

    UIGraphicsBeginImageContextWithOptions(CGSize(width: 1, height: 1), false, navBar.layer.contentsScale)
    let context = UIGraphicsGetCurrentContext()
    context?.setFillColor(red: 1, green: 1, blue: 1, alpha: opacity)
    UIRectFill(CGRect(x: 0, y: 0, width: 1, height: 1))
    transparentBackground = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()

    let navigationBarAppearance = nav.navigationBar
    navigationBarAppearance.setBackgroundImage(transparentBackground, for: .default)
  }

  //| ----------------------------------------------------------------------------
  //! Configures the navigation bar to use a custom color as its background.
  //! The navigation bar remains translucent.
  //

  func applyBarTintToTheNavigationBar() {
    // Be aware when selecting a barTintColor for a translucent bar that
    // the tint color will be blended with the content passing under
    // the translucent bar.  See QA1808 for more information.
    // <https://developer.apple.com/library/ios/qa/qa1808/_index.html>

    let barTint = UIColor(red: 176.0 / 255.0, green: 226.0 / 255.0, blue: 172.0 / 255.0,
      alpha: 1.0)

    let darkenedBarTintColor = UIColor(red: (176.0 / 255.0) - 0.05,
      green: (226.0 / 255.0) - 0.02, blue: (172.0 / 255.0) - 0.05, alpha: 1.0)

    let nav = navigationController!
    let navigationBarAppearance = nav.navigationBar
    navigationBarAppearance.barTintColor = darkenedBarTintColor

    nav.toolbar.barTintColor = barTint
    nav.toolbar.isTranslucent = false
  }

}

// MARK: Background Switcher
extension CustomAppearanceViewController {
  func configureNewNavBarBackground(_ sender : UISegmentedControl) {
    // Reset everything
    let nav = navigationController!
    let navBar = nav.navigationBar
    let toolBar = nav.toolbar

    navBar.setBackgroundImage(nil, for: .default)
    navBar.setBackgroundImage(nil, for: .compact)

    navBar.barTintColor = nil
    toolBar?.isTranslucent = true

    switch sender.selectedSegmentIndex {
      case 0: // Transparent
        applyImageBackgroundToTheNavigationBar()
      case 1: // Transparent
        applyTransparentBackgroundToTheNavigationBar(0.87)
      case 2: // Color
        applyBarTintToTheNavigationBar()

      default:
        break
    }
  }
}



extension CustomAppearanceViewController : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cities.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
      -> UITableViewCell {

    var cell = table.dequeueReusableCell(withIdentifier: "Cell")
    if cell == nil {
      cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
    }
    cell!.textLabel!.text = cities[indexPath.row]

    return cell!
  }
}

extension CustomAppearanceViewController : UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if navigationItem.prompt == cities[indexPath.row] {
      navigationItem.prompt = nil
    } else {
      navigationItem.prompt = cities[indexPath.row]
    }
  }

}
