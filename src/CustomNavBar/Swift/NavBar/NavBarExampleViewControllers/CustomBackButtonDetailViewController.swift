//
//  CustomBackButtonDetailViewController.swift
//  NavBar
//
//  Created by Don Clore on 8/21/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit
import SnapKit

class CustomBackButtonDetailViewController: UIViewController {
  var city : String?
  fileprivate var cityLabel : UILabel!

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  init(frame: CGRect) {
    super.init(nibName: nil, bundle: nil)
    view.frame = frame
    view.backgroundColor = UIColor.white
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    cityLabel = UILabel()

    view.addSubview(cityLabel)

    cityLabel.snp.makeConstraints{ (make) -> Void in
      make.height.equalTo(21)
      make.centerY.equalTo(view)
      make.trailing.equalTo(view).offset(-20)
      make.leading.equalTo(view).offset(20)
    }

    cityLabel.font = UIFont.systemFont(ofSize: 17.0)
    cityLabel.textColor = UIColor.black
    cityLabel.textAlignment = .center
    cityLabel.baselineAdjustment = .alignBaselines
    cityLabel.numberOfLines = 1
    cityLabel.lineBreakMode = .byTruncatingTail
    cityLabel.contentMode = .left
    cityLabel.semanticContentAttribute = .unspecified
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    navigationItem.title = "Custom Back Button"
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


  /*
  // MARK: - Navigation

  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
  }
  */

}
