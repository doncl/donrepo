//
//  ExtendedNavBarView.swift
//  NavBar
//
//  Created by Don Clore on 8/21/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit
import QuartzCore

class ExtendedNavBarView: UIView {
  override func willMove(toWindow newWindow: UIWindow?) {
    // User the layer shadow to draw a one pixel hairline under this view.
    layer.shadowOffset = CGSize(width: 0.0, height: 1.0 / UIScreen.main.scale)
    layer.shadowRadius = 0

    // UINavigationBar's hairline is adaptive, its properties change with the content it
    //overlies.  You may need to experiment with the values to best match your content.
    layer.shadowColor = UIColor.black.cgColor
    layer.shadowOpacity = 0.25
  }
}
