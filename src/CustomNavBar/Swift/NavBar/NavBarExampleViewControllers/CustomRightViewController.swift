//
//  CustomRightViewController.swift
//  NavBar
//
//  Created by Don Clore on 8/21/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit
import SnapKit

class CustomRightViewController: UIViewController {

  var segmentedControl : UISegmentedControl!
  var customRightView : UIImageView!

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  init(frame: CGRect) {
    super.init(nibName: nil, bundle: nil)
    view.frame = frame
    view.backgroundColor = UIColor.white
  }

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    // Custom right view thingy
    customRightView = UIImageView(image:
    UIImage(named: "CustomRightView"))
    customRightView.contentMode = .topRight
    customRightView.semanticContentAttribute = .unspecified

    view.addSubview(customRightView)

    let nav = navigationController!

    customRightView.snp.makeConstraints { (make) -> Void in

      make.top.equalTo(nav.navigationBar.snp.bottom)
      make.leading.equalTo(view)
      make.trailing.equalTo(view)
      make.bottom.equalTo(view)
    }

    // Segmented Control
    let items : [String] = ["Button", "Image", "View"]
    segmentedControl = UISegmentedControl(items: items)
    view.addSubview(segmentedControl)
    segmentedControl.snp.makeConstraints { (make) -> Void in
      make.centerY.equalTo(view.snp.centerY)
      make.trailing.equalTo(view.snp.trailing).offset(-20)
      make.leading.equalTo(view.snp.leading).offset(20)
    }

    segmentedControl.addTarget(self,
      action: #selector(CustomRightViewController.segmentedControlSelected(_ :)),
      for: .valueChanged)
  }

  func segmentedControlSelected(_ sender: UISegmentedControl) {
    print("segmented control selected")
    let index = sender.selectedSegmentIndex
    switch index {
      case 0:
         doButtonThang()
      case 1:
         doImageThang()
      case 2:
         doViewThang()
      default:
        break
    }
  }

  fileprivate func doButtonThang() {
    let button = UIBarButtonItem(title: "AddTitle", style: .plain, target: self,
            action: #selector(CustomRightViewController.action(_:)))

    navigationItem.rightBarButtonItem = button
  }

  fileprivate func doImageThang() {
    let button = UIBarButtonItem(image: UIImage(named: "Email"), style: .plain, target: self,
            action: #selector(CustomRightViewController.action(_:)))

    navigationItem.rightBarButtonItem = button
  }

  fileprivate func doViewThang() {
    let segCtrl = UISegmentedControl(
            items: [UIImage(named: "UpArrow")!, UIImage(named: "DownArrow")!])

    segCtrl.addTarget(self, action: #selector(CustomRightViewController.action(_:)),
            for: .valueChanged)

    let button = UIBarButtonItem(customView: segCtrl)

    navigationItem.rightBarButtonItem = button
  }

  func action(_ sender: AnyObject) {

  }

}


