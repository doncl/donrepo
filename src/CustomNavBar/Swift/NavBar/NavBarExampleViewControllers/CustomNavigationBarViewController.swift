//
//  CustomNavigationBarViewController.swift
//  NavBar
//
//  Created by Don Clore on 8/22/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit
import SnapKit

class CustomNavigationBarViewController: UIViewController {

  var cities : [String] = []
  var table : UITableView!

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  init(frame: CGRect) {
    super.init(nibName: nil, bundle: nil)
    view.frame = frame
    view.backgroundColor = UIColor.white
    table = UITableView(frame: frame, style: .plain)
    view.addSubview(table)
    table.snp.makeConstraints{ (make) -> Void in
      make.edges.equalTo(view)
    }
    table.delegate = self
    table.dataSource = self
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    let citiesJSONURL = Bundle.main.url(forResource: "Cities", withExtension: "json")!
    let citiesData = try! Data(contentsOf: citiesJSONURL)
    cities = try! JSONSerialization.jsonObject(with: citiesData, options: []) as! [String]
    navigationItem.title = "Custom Navigation Bar"
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)


    let returnToMenuButton = UIButton(type: .system)
    returnToMenuButton.setTitle("Return to Menu", for: UIControlState())
    returnToMenuButton.titleLabel?.font = returnToMenuButton.titleLabel?.font.withSize(12.0)
    returnToMenuButton.addTarget(self,
      action: #selector(CustomNavigationBarViewController.returnToMenuAction(_:)),
      for: .touchUpInside)

    let nav = navigationController!
    if let customNavBar = nav.navigationBar as? CustomNavigationBar {
      customNavBar.customButton = returnToMenuButton
    }
  }

  func returnToMenuAction(_ sender: UIButton) {
    if let nav = navigationController {
      nav.dismiss(animated: true, completion: nil)
    }
  }
}

extension CustomNavigationBarViewController : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cities.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
      -> UITableViewCell {

    var cell = tableView.dequeueReusableCell(withIdentifier: "CellId")
    if cell == nil {
      cell = UITableViewCell(style: .default, reuseIdentifier: "CellId")
    }
    cell!.textLabel!.text = cities[indexPath.row]

    return cell!
  }
}

extension CustomNavigationBarViewController : UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let city = cities[indexPath.row]
    let vc = CustomNavBarDetailViewController(frame: view.frame, city: city)
    if let nav = navigationController {
      nav.show(vc, sender: self)
    }
  }
}
