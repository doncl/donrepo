//
//  CustomNavigationBar.swift
//  NavBar
//
//  Created by Don Clore on 8/22/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit

class CustomNavigationBar: UINavigationBar {

  var customButtonBacking : UIButton?
  var customButton: UIButton? {
    set (newButton) {
      customButtonBacking?.removeFromSuperview()
      customButtonBacking = newButton
      addSubview(newButton!)

      // Force our -sizeThatFits: method to be called again and flag the
      // navigation bar as needing layout.
      invalidateIntrinsicContentSize()
      setNeedsLayout()
    }
    get {
      return customButtonBacking
    }
  }

  //| ----------------------------------------------------------------------------
  //  A navigation bar subclass should override -sizeThatFits: to pad the fitting
  //  height for the navigation bar, creating space for the extra elements that
  //  will be added.  UINavigationController calls this method to retrieve the
  //  size of the navigation bar, which it then uses when computing the bar's
  //  frame.
  //
  override func sizeThatFits(_ size: CGSize) -> CGSize {
    var navigationBarSize = super.sizeThatFits(size)

    // Pad the base navigation bar height by the fitting height of our button.
    if let customButton = customButton {
      let buttonSize = customButton.sizeThatFits(CGSize(width: size.width, height: 0))
      navigationBarSize.height += buttonSize.height
    }

    return navigationBarSize
  }

  override func layoutSubviews() {
    // UINavigationBar positions its elements along the bottom edge of the
    // bar's bounds.  This allows our subclass to position our custom elements
    // at the top of the navigation bar, in the extra space we created by
    // padding the height returned from -sizeThatFits:

    super.layoutSubviews()

    // NOTE: You do not need to account for the status bar height in your
    //       layout.  The navigation bar is positioned just below the
    //       status bar by the navigation controller.

    // Retrieve the button's fitting height and position the button along the
    // top edge of the navigation bar.  The button is sized to the full
    // width of the navigation bar as it will automatically center its contents.

    if let customButton = customButton {
      let buttonSize = customButton.sizeThatFits(CGSize(width: bounds.size.width, height: 0))
      customButton.frame = CGRect(x: 0, y: 0, width: bounds.size.width, height: buttonSize.height)
    }
  }




}
