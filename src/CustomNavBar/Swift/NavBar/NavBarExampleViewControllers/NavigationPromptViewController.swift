//
//  NavigationPromptViewController.swift
//  NavBar
//
//  Created by Don Clore on 8/21/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit

class NavigationPromptViewController: UIViewController {


  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  init(frame: CGRect) {
    super.init(nibName: nil, bundle: nil)
    view.frame = frame
    view.backgroundColor = UIColor.white

  }

  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    navigationItem.prompt = "Navigation prompts appear at the top"
  }
}
