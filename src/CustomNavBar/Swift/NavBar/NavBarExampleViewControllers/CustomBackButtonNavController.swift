//
//  CustomBackButtonNavController.swift
//  NavBar
//
//  Created by Don Clore on 8/21/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit

// Custom UINavigationController subclass used for targeting appearance proxy changes in the
// Custom Back Button example.
class CustomBackButtonNavController: UINavigationController {

}
