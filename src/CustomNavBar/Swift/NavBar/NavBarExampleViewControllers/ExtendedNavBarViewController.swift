//
//  ExtendedNavBarViewController.swift
//  NavBar
//
//  Created by Don Clore on 8/21/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit

class ExtendedNavBarViewController: UIViewController {
  var cities : [String] = []
  var table : UITableView!
  var extendedNavBarView : ExtendedNavBarView!


  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  init(frame: CGRect) {
    super.init(nibName: nil, bundle: nil)
    view.frame = frame
    view.backgroundColor = UIColor.white
    table = UITableView(frame: frame, style: .plain)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    let citiesJSONURL = Bundle.main.url(forResource: "Cities", withExtension: "json")!
    let citiesData = try! Data(contentsOf: citiesJSONURL)
    cities = try! JSONSerialization.jsonObject(with: citiesData, options: []) as! [String]
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    let nav = navigationController!

    extendedNavBarView = ExtendedNavBarView(frame: CGRect(x: 0, y: 0,
      width: view.frame.size.width, height: 50.0))

    view.addSubview(extendedNavBarView)

    extendedNavBarView.snp.makeConstraints{ (make) -> Void in
      make.top.equalTo(nav.navigationBar.snp.bottom)
      make.leading.equalTo(view)
      make.trailing.equalTo(view)
      make.height.equalTo(50.0)
    }

    let label = UILabel()
    label.text = "This label appears as part of the navigation bar."
    label.font = UIFont.systemFont(ofSize: 12.0)
    label.textColor = UIColor.black
    label.baselineAdjustment = .alignBaselines
    label.lineBreakMode = .byTruncatingTail
    label.numberOfLines = 0

    extendedNavBarView.addSubview(label)
    label.snp.makeConstraints{ (make) -> Void in
      make.width.equalTo(183.0)
      make.height.equalTo(29.0)
      make.centerX.equalTo(extendedNavBarView)
      make.centerY.equalTo(extendedNavBarView)
    }

    // For the extended navigation bar effect to work, a few changes
    // must be made to the actual navigation bar.  Some of these changes could
    // be applied in the storyboard but are made in code for clarity.

    // Translucency of the navigation bar is disabled so that it matches with
    // the non-translucent background of the extension view.


    nav.navigationBar.isTranslucent = false

    // The navigation bar's shadowImage is set to a transparent image.  In
    // conjunction with providing a custom background image, this removes
    // the grey hairline at the bottom of the navigation bar.  The
    // ExtendedNavBarView will draw its own hairline.
    nav.navigationBar.shadowImage = UIImage(named: "TransparentPixel")

    // "Pixel" is a solid white 1x1 image.
    nav.navigationBar.setBackgroundImage(UIImage(named: "Pixel"), for: .default)

    view.addSubview(table)

    table.snp.makeConstraints{ (make) -> Void in
      make.top.equalTo(extendedNavBarView.snp.bottom)
      make.leading.equalTo(view)
      make.trailing.equalTo(view)
      make.bottom.equalTo(view)
    }
    table.delegate = self
    table.dataSource = self
  }
}

extension ExtendedNavBarViewController : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cities.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
      -> UITableViewCell {

    let cellId = "CityCell"
    var cell = table.dequeueReusableCell(withIdentifier: cellId)
    if cell == nil {
      cell = UITableViewCell(style: .default, reuseIdentifier: cellId)
    }

    cell!.textLabel!.text = cities[indexPath.row]

    return cell!
  }
}

extension ExtendedNavBarViewController : UITableViewDelegate {

}
