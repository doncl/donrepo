//
//  CustomBackButtonViewController.swift
//  NavBar
//
//  Created by Don Clore on 8/21/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit
import SnapKit

class CustomBackButtonViewController: UIViewController {
  var table : UITableView!
  var cities : [String] = []

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  init(frame: CGRect) {
    super.init(nibName: nil, bundle: nil)
    view.frame = frame
    view.backgroundColor = UIColor.white
    table = UITableView(frame: frame, style: .plain)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  func die(_ sender: UIBarButtonItem) {
    if let nav = navigationController {
      nav.dismiss(animated: true, completion: nil)
    }
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    view.addSubview(table)
    table.snp.makeConstraints{ (make) -> Void in
      make.edges.equalTo(view)
    }

    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain,
      target: self, action: #selector(CustomBackButtonViewController.die(_:)))

    table.delegate = self
    table.dataSource = self

    navigationItem.title = "Custom Back Button"
    let citiesJSONURL = Bundle.main.url(forResource: "Cities", withExtension: "json")!
    let citiesData = try! Data(contentsOf: citiesJSONURL)
    cities = try! JSONSerialization.jsonObject(with: citiesData, options: []) as! [String]

    // Note that images configured as the back bar button's background do
    // not have the current tintColor applied to them, they are displayed
    // as it.
    var backButtonBackgroundImage = UIImage(named: "Menu")!

    // The background should be pinned to the left and not stretch.
    backButtonBackgroundImage = backButtonBackgroundImage.resizableImage(
      withCapInsets: UIEdgeInsetsMake(0, backButtonBackgroundImage.size.width - 1, 0, 0))


    let appearance = UIBarButtonItem.appearance(
      whenContainedInInstancesOf: [CustomBackButtonNavController.self])

    appearance.setBackButtonBackgroundImage(backButtonBackgroundImage, for:UIControlState(),
      barMetrics:.default)

    // Provide an empty backBarButton to hide the 'Back' text present by
    // default in the back button.
    //
    // NOTE: You do not need to provide a target or action.  These are set
    //       by the navigation bar.
    // NOTE: Setting the title of this bar button item to ' ' (space) works
    //       around a bug in iOS 7.0.x where the background image would be
    //       horizontally compressed if the back button title is empty.
    let backBarButton = UIBarButtonItem(title:" ", style:.plain, target:nil, action:nil)
    self.navigationItem.backBarButtonItem = backBarButton;

    // NOTE: There is a bug in iOS 7.0.x where the background of the back bar
    //       button item will not appear until the back button has been tapped
    //       once.
  }

}

extension CustomBackButtonViewController : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cities.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
      -> UITableViewCell {

    var cell = tableView.dequeueReusableCell(withIdentifier: "CellId")
    if cell == nil {
      cell = UITableViewCell(style: .default, reuseIdentifier: "CellId")
    }
    cell!.textLabel!.text = cities[indexPath.row]
    return cell!
  }

}

extension CustomBackButtonViewController : UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    let city = cities[indexPath.row]
    let vc = CustomBackButtonDetailViewController(frame: view.frame)
    vc.city = city

    if let nav = navigationController {
      nav.show(vc, sender: self)
    }
  }
}
