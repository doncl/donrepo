//
//  CustomTitleViewController.swift
//  NavBar
//
//  Created by Don Clore on 8/21/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit
import SnapKit

class CustomTitleViewController: UIViewController {

  var customTitleView : UIImageView!

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  init(frame: CGRect) {
    super.init(nibName: nil, bundle: nil)
    view.frame = frame
    view.backgroundColor = UIColor.white

  }

  override func viewDidLoad() {
    super.viewDidLoad()


  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    let nav = navigationController!

    customTitleView = UIImageView(image: UIImage(named: "CustomTitleView"))
    customTitleView.contentMode = .topRight
    customTitleView.semanticContentAttribute = .unspecified

    view.addSubview(customTitleView)

    customTitleView.snp.makeConstraints{ (make) -> Void in
      make.top.equalTo(nav.navigationBar.snp.bottom)
      make.leading.equalTo(view.snp.leading)
      make.trailing.equalTo(view.snp.trailing)
      make.bottom.equalTo(view.snp.bottom)
    }

    let segCtrl = UISegmentedControl(items: ["Image", "Text", "Video"])

    // Segmented control as the custom title control
    segCtrl.selectedSegmentIndex = 0
    segCtrl.autoresizingMask = .flexibleWidth
    segCtrl.frame = CGRect(x: 0, y: 0, width: 400.0, height: 30.0)
    segCtrl.addTarget(self, action: #selector(CustomTitleViewController.action(_:)),
      for: .valueChanged)

    navigationItem.titleView = segCtrl

  }

  func action(_ sender: UISegmentedControl) {

  }


}
