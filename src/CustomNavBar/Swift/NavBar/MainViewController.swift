//
//  MainViewController.swift
//  NavBar
//
//  Created by Don Clore on 8/21/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit
import SnapKit

struct CellContents {
  var title : String
  var body: String

}

enum CustomNavBarExampleType {
  case customRightView
  case customTitleView
  case navigationPrompt
  case extendedNavBar
  case customAppearance
  case customBackButton
  case customNavBar
}

struct ExampleMenuItem {
  var cellContents : CellContents
  var exampleType : CustomNavBarExampleType
}

class MainViewController: UIViewController {
  let table : UITableView!

  static let rightViewId = "customRightView"
  static let titleViewId = "customTitleView"
  static let navPromptId = "navigationPrompt"
  static let extendedNavId = "extendedNavigationBar"
  static let customAppearanceId = "customAppearance"
  static let customBackButton = "customBackButton"
  static let customNavBar = "customNavBar"

  let tableContents : [ExampleMenuItem] = [
    ExampleMenuItem( cellContents: CellContents(title: "Custom Right View",
      body: "A navigation bar with a custom control on the right."),
       exampleType: .customRightView),

    ExampleMenuItem( cellContents: CellContents(title: "Custom Title View",
      body: "A navigation bar with a custom control in the center"),
      exampleType: .customTitleView),

    ExampleMenuItem(cellContents: CellContents(title: "Navigation Prompt",
      body: "A navigation bar with text added above the title"),
      exampleType:.navigationPrompt),

    ExampleMenuItem(cellContents: CellContents(title: "Extended Navigation Bar",
      body: "A navigation bar with a custom view underneath"),
      exampleType: .extendedNavBar),

    ExampleMenuItem(cellContents: CellContents(title: "Custom Appearance",
      body: "A navigation var with a custom bar tint color and background"),
      exampleType: .customAppearance),

    ExampleMenuItem(cellContents: CellContents(title: "Custom Back Button",
      body: "A navigation controller with a custom back button"),
      exampleType: .customBackButton),

    ExampleMenuItem(cellContents: CellContents(title: "Custom Navigation Bar",
      body: "A navigation controller with a custom navigation bar"),
      exampleType: .customNavBar)
  ]

  init(frame: CGRect) {
    table = UITableView(frame: frame, style: .plain)
    super.init(nibName: nil, bundle: nil)
    view.frame = frame
    view.backgroundColor = UIColor.white

    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Style", style: .plain,
      target: self, action: #selector(MainViewController.styleAction(_:)))

    navigationItem.title = "NavBar"


    view.addSubview(table)
    table.snp.makeConstraints{ (make) -> Void in
      make.edges.equalTo(self.view)
    }
    table.delegate = self
    table.dataSource = self
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func styleAction(_ sender: UIBarButtonItem) {
    print("Style clicked")

    let s = "Choose a UIBarStyle:"

    let ac = UIAlertController(title: nil, message: s,
      preferredStyle: .actionSheet)

    let def = UIAlertAction(title: "Default", style: .default, handler: {
      _ in
      self.navigationController?.navigationBar.barStyle = .default
      self.navigationController?.navigationBar.tintColor = nil
      self.navigationController?.navigationBar.isTranslucent = true
      self.dismiss(animated: true, completion: {
        self.setNeedsStatusBarAppearanceUpdate()
      })
    })

    let blackOpaque = UIAlertAction(title: "Black Opaque", style: .default, handler: {
      _ in
      self.navigationController?.navigationBar.barStyle = .blackOpaque
      self.navigationController?.navigationBar.isTranslucent = false
      self.navigationController?.navigationBar.tintColor = UIColor.white
      self.dismiss(animated: true, completion:  {
        self.setNeedsStatusBarAppearanceUpdate()
      })
    })

    let blackTranslucent = UIAlertAction(title: "Black Translucent", style: .default,
      handler: {
        _ in

        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.dismiss(animated: true, completion: {
          self.setNeedsStatusBarAppearanceUpdate()
        })
      })

    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
      _ in
      self.dismiss(animated: true, completion: nil)
    })

    ac.addAction(def)
    ac.addAction(blackOpaque)
    ac.addAction(blackTranslucent)
    ac.addAction(cancel)

    present(ac, animated: true, completion: nil)
  }

}

extension MainViewController : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 7
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
      -> UITableViewCell {

    let cellId = "s\(indexPath.section) r\(indexPath.row)"
    var cell = table.dequeueReusableCell(withIdentifier: cellId)
    if cell == nil {
      cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellId)
      cell!.selectionStyle = .blue
      cell!.accessoryType = .disclosureIndicator
      cell!.editingAccessoryView = .none
      cell!.focusStyle = .default
      cell!.indentationLevel = 1
      cell!.shouldIndentWhileEditing = true

      let cellContents = tableContents[indexPath.row].cellContents
      cell!.detailTextLabel!.text = cellContents.body
      cell!.textLabel!.text = cellContents.title
    }

    return cell!
  }

}

extension MainViewController : UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let exampleType = tableContents[indexPath.row].exampleType

    var vc : UIViewController?

    switch exampleType {
      case .customRightView:
        vc = CustomRightViewController(frame: view.frame)

      case .customTitleView:
        vc = CustomTitleViewController(frame: view.frame)

      case .navigationPrompt:
        vc = NavigationPromptViewController(frame: view.frame)

      case .extendedNavBar:
        vc = ExtendedNavBarViewController(frame: view.frame)

      case .customAppearance:
        vc = CustomAppearanceViewController(frame: view.frame)

      case .customBackButton:
        let customBackVC = CustomBackButtonViewController(frame: view.frame)
        let customNav = CustomBackButtonNavController(rootViewController: customBackVC)
        present(customNav, animated: true, completion: nil)

      case .customNavBar:
         let customNavBarVC = CustomNavigationBarViewController(frame: view.frame)
         let nav = UINavigationController(navigationBarClass: CustomNavigationBar.self,
           toolbarClass: nil)
         nav.viewControllers = [customNavBarVC]
         present(nav, animated: true, completion: nil)
    }

    if let vc = vc, let nav = navigationController {
      nav.show(vc, sender: self)
    }
  }
}

