//
//  TimeBasedBanView.swift
//  Cal
//
//  Created by Don Clore on 10/30/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit
import JTAppleCalendar

//MARK: View initialization and layout.
extension TimeBasedBan {
  struct Constants {
    static let leadingPad : CGFloat = 15.0
    static let switchHeight : CGFloat = 31.0
    static let trailingPad : CGFloat = 15.0
    static let horzSpace : CGFloat = 25.0
    static let topPad : CGFloat = 5.0
    static let timeStackSpacing : CGFloat = 5.0
    static let bottomPad : CGFloat = 15.0
    static let heightPlaceholder : CGFloat = 31.0
    static let calendarTopPad : CGFloat = 15.0
    static let monthLabelHeight : CGFloat =  20.0
    static let daysOfWeekLabelHeight : CGFloat = 20.0
    //static let buttonHeight : CGFloat = 20.0
    //static let buttonWidth : CGFloat = 20.0
    static let buttonXPad : CGFloat = 5.0
  }
  
  func setupViews() {
    isUserInteractionEnabled = true
    
    // Ban Name label
    addSubview(banName)
    
    banName.snp.makeConstraints { (make) -> Void in
      make.leading.equalTo(self).offset(Constants.leadingPad)
      make.top.equalTo(self).offset(Constants.topPad)
    }
    
    banName.font = UIFont.preferredFont(forTextStyle: .caption2)
    banName.textColor = textColor
    banName.textAlignment = .left
    
    // Ban boolean switch
    addSubview(banSwitch)
    
    banSwitch.snp.makeConstraints { (make) -> Void in
      make.leading.equalTo(banName.snp.trailing).offset(Constants.leadingPad)
      make.centerY.equalTo(banName)
      make.height.equalTo(Constants.switchHeight)
    }
    
    banSwitch.isOn = false
    banSwitch.isEnabled = true
    
    banSwitch.addTarget(self, action: #selector(TimeBasedBan.typeSwitchToggled(_:)),
                        for: .valueChanged)
    
    // 'TimeStack' stack view
    addSubview(timeStack)
    timeStack.axis = .horizontal
    timeStack.alignment = .fill
    timeStack.distribution = .fillProportionally
    timeStack.spacing = Constants.timeStackSpacing
    
    timeStack.snp.makeConstraints { (make) -> Void in
      make.leading.equalTo(banSwitch.snp.trailing).offset(Constants.horzSpace)
      make.centerY.equalTo(banSwitch)
      make.height.equalTo(banSwitch)
    }
    
    // 'TimeBased' label
    
    timeBasedLabel.text = "Time-based"
    timeBasedLabel.textAlignment = .left
    timeBasedLabel.textColor = textColor
    timeBasedLabel.font = UIFont.preferredFont(forTextStyle: .caption2)
    
    timeStack.addArrangedSubview(timeBasedLabel)
    
    // 'Time-based' switch
    timeBasedSwitch.isOn = false
    timeBasedSwitch.addTarget(self, action: #selector(TimeBasedBan.timeBasedSwitchToggled(_:)),
                          for: .valueChanged)
    
    timeStack.addArrangedSubview(timeBasedSwitch)
    timeStack.isHidden = true
    
    addSubview(selectedDateLabel)
    selectedDateLabel.snp.makeConstraints{ (make) -> Void in
      make.leading.equalTo(timeStack)
      make.centerY.equalTo(banSwitch)
    }
    
    // SELECTED DATE LABEL and CLOSE BUTTON
    selectedDateLabel.text = "2016-01-01"
    selectedDateLabel.textAlignment = .left
    selectedDateLabel.textColor = textColor
    selectedDateLabel.font = UIFont.preferredFont(forTextStyle: .caption1)
    
    let recog = UITapGestureRecognizer()
    recog.addTarget(self, action: #selector(TimeBasedBan.selectedDateLabelTouched(_:)))
    selectedDateLabel.isUserInteractionEnabled = true
    selectedDateLabel.addGestureRecognizer(recog)
    
    addSubview(cancelTimeBasedButton)
    cancelTimeBasedButton.snp.makeConstraints{ (make) -> Void in
      make.leading.equalTo(selectedDateLabel.snp.trailing).offset(Constants.buttonXPad)
      //make.height.equalTo(Constants.buttonHeight)
      //make.width.equalTo(Constants.buttonWidth)
      make.height.equalTo(banSwitch)
      make.width.equalTo(banSwitch.snp.height)
      make.centerY.equalTo(selectedDateLabel)
    }

    cancelTimeBasedButton.setImage(#imageLiteral(resourceName: "Cancel"), for: .normal)
    cancelTimeBasedButton.tintColor = Colors.grey74
    
    cancelTimeBasedButton.addTarget(self,
      action: #selector(TimeBasedBan.cancelTimeBasedBanButtonTouched(_:)), for: .touchUpInside)
       
    // Hide these two
    selectedDateLabel.isHidden = true
    cancelTimeBasedButton.isHidden = true
    
    addSubview(monthLabel)
    monthLabel.textAlignment = .center
    monthLabel.font = UIFont.preferredFont(forTextStyle: .caption1)
    monthLabel.textColor = textColor
    
    monthLabel.snp.makeConstraints { (make) -> Void in
      make.centerX.equalTo(self)
      make.top.equalTo(timeStack.snp.bottom).offset(Constants.calendarTopPad)
      make.height.equalTo(Constants.monthLabelHeight)
    }
    
    monthLabel.isHidden = true
    
    let days : [String] = ["S", "M", "T", "W", "Th", "F", "S"]
    daysOfWeek.axis = .horizontal
    daysOfWeek.distribution = .fillEqually
    daysOfWeek.alignment = .firstBaseline
    
    for day in days {
      let dayLabel : UILabel = UILabel()
      dayLabel.text = day
      dayLabel.textColor = textColor
      dayLabel.font = UIFont.preferredFont(forTextStyle: .caption2)
      dayLabel.textAlignment = .center
      daysOfWeek.addArrangedSubview(dayLabel)
    }
    
    addSubview(daysOfWeek)
    daysOfWeek.snp.makeConstraints{(make) -> Void in
      make.top.equalTo(monthLabel.snp.bottom)
      make.leading.equalTo(self).offset(Constants.leadingPad)
      make.trailing.equalTo(self).offset(-Constants.trailingPad)
      make.height.equalTo(Constants.monthLabelHeight)
    }
    
    daysOfWeek.isHidden = true
    
    // Calendar
    addSubview(calendar)
    calendar.snp.makeConstraints {(make) -> Void in
      make.top.equalTo(daysOfWeek.snp.bottom)
      make.leading.equalTo(self).offset(Constants.leadingPad)
      make.trailing.equalTo(self).offset(-Constants.trailingPad)
    }
    calendar.registerCellViewClass(type: CellView.self)
    
    calendarHeight = calendar.heightAnchor.constraint(equalToConstant: 0.0)
    calendarHeight?.isActive = true
    
    calendar.visibleDates { (visibleDates: DateSegmentInfo) in
      self.setupViewsOfCalendar(from: visibleDates)
    }
    
    calendar.dataSource = self
    calendar.delegate = self
    
    totalHeight = heightAnchor.constraint(equalToConstant: Constants.heightPlaceholder)
    totalHeight?.isActive = true
  }
  
  func showTimeLabel(_ show: Bool) {
    selectedDateLabel.isHidden = !show
    cancelTimeBasedButton.isHidden = !show
    timeStack.isHidden = show
  }
}

