//
//  TimeBasedBan.swift
//  Cal
//
//  Created by Don Clore on 10/27/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit
import SnapKit
import JTAppleCalendar

class TimeBasedBan: UIView {
  var selectedDate : Date?
    
  var selectedDateLabel : UILabel = UILabel()
  var cancelTimeBasedButton : UIButton = UIButton()
  
  let calendarOpenedHeight : CGFloat = 128.0
  var textColor : UIColor = UIColor.black
  
  var banName : UILabel = UILabel()
  var banSwitch : UISwitch = UISwitch()
  
  var timeStack : UIStackView = UIStackView()
  var timeBasedLabel: UILabel = UILabel()
  var timeBasedSwitch: UISwitch = UISwitch()
    
  var daysOfWeek : UIStackView = UIStackView()

  var monthLabel : UILabel = UILabel()
  var calendar : JTAppleCalendarView = JTAppleCalendarView()
  
  var calendarHeight : NSLayoutConstraint?
  var totalHeight : NSLayoutConstraint?
  
  var calendarToggled : Bool {
    get {
      return calendarHeight!.constant > 0.0
    }
    set {
      let calHeight : CGFloat = newValue ? calendarOpenedHeight : 0.0
      let monthLabelHeight = newValue ?
        Constants.monthLabelHeight + Constants.calendarTopPad : 0.0
      
      let daysHeaderHeight = newValue ? Constants.daysOfWeekLabelHeight : 0.0

      totalHeight?.constant = calHeight + Constants.topPad +
        banSwitch.frame.size.height + Constants.topPad + Constants.bottomPad +
        monthLabelHeight + daysHeaderHeight
      calendarHeight?.constant = calHeight
      calendar.isHidden = !newValue
      monthLabel.isHidden = !newValue
      daysOfWeek.isHidden = !newValue
        
      setNeedsLayout()
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  convenience init(frame: CGRect, name : String) {
    
    self.init(frame:frame)
    
    banName.text = name
    setupViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
}

//MARK: Behaviors
extension TimeBasedBan {
  func typeSwitchToggled(_ sender: UISwitch) {
      timeStack.isHidden = !sender.isOn
      if false == sender.isOn {
        calendarToggled = false
        timeBasedSwitch.isOn = false
      }
      
      // Turn this off no matter what.
      showTimeLabel(false)
  }
  
  func timeBasedSwitchToggled(_ sender: UISwitch) {
    let hidden = !sender.isOn
   
    calendarToggled = !hidden
    // If the user is toggline the time-based switch these two pieces of UI need to be hidden
    cancelTimeBasedButton.isHidden = true
    selectedDateLabel.isHidden = true
    selectedDate = nil
  }
  
  func cancelTimeBasedBanButtonTouched(_ sender: UIButton) {
    cancelTimeBasedButton.isHidden = true
    selectedDateLabel.isHidden = true
    selectedDate = nil
    timeBasedSwitch.isOn = false
    timeStack.isHidden = false
  }
  
  func selectedDateLabelTouched(_ sender: UITapGestureRecognizer) {
    calendarToggled = true
    cancelTimeBasedButton.isHidden = true
    selectedDateLabel.isHidden = true
    selectedDate = nil
    timeBasedSwitch.isOn = true
    timeStack.isHidden = false
  }
}

//MARK: JTAppleCalendarViewDataSource
extension TimeBasedBan : JTAppleCalendarViewDataSource {
  
  func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
    let firstDate = Date()
    let numberOfRows = 6
    let aCalendar = Calendar.current
    var offsetComponents = DateComponents()
    offsetComponents.timeZone = TimeZone.current
    offsetComponents.year = 2
    
    let secondDate = aCalendar.date(byAdding: offsetComponents, to: firstDate)!
    
    let parms = ConfigurationParameters(startDate: firstDate, endDate: secondDate,
                                        numberOfRows: numberOfRows, calendar: aCalendar,
                                        generateInDates: .forFirstMonthOnly, generateOutDates: .tillEndOfGrid,
                                        firstDayOfWeek: .sunday)
    
  
    
    return parms
  }
  
  
  func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
    guard let startDate = visibleDates.monthDates.first else {
      return
    }
    
    let monthNames = DateFormatter().monthSymbols!
    let utc = TimeZone(identifier: "UTC")!
    let components = Calendar.current.dateComponents(in: utc, from: startDate)
    let month = components.month!
    let year = components.year!
    let monthName = monthNames[month - 1]
    monthLabel.text = monthName + " " + String(year)
  }
}

//MARK: JTAppleCalendarViewDelegate
extension TimeBasedBan : JTAppleCalendarViewDelegate {
  func calendar(_ calendar: JTAppleCalendarView, canSelectDate date: Date,
                cell: JTAppleDayCellView, cellState: CellState) -> Bool {
    return true
  }
  
  func calendar(_ calendar: JTAppleCalendarView, canDeselectDate date: Date,
                cell: JTAppleDayCellView, cellState: CellState) -> Bool {
    return true
  }
  
  func calendar(_ calendar: JTAppleCalendarView,
                didSelectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
    selectedDate = date
    
    if let myCell = cell as? CellView {
      myCell.setupCellBeforeDisplay(cellState, date: date, selected: true)
      
      UIView.animate(withDuration: 0.04, animations: {
        self.calendarToggled = false
        self.selectedDateLabel.text = self.formatDate(date)
        self.showTimeLabel(true)
        
      }, completion: { _ in
        DispatchQueue.main.async {
          // Deselect the cell.
          myCell.setupCellBeforeDisplay(cellState, date: date, selected: false)
        }
      })
     
    }
  }
  
  fileprivate func formatDate(_ date : Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "YYYY/MM/dd"
    
    // I know this is hard to make sense of, but this is what it takes to give the user the
    // intuitive thing.
    formatter.timeZone = TimeZone(identifier: "UTC")
    return formatter.string(from: date)
  }
  
  func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date,
                cell: JTAppleDayCellView?, cellState: CellState) {
    
    if let myCell = cell as? CellView {
      myCell.setupCellBeforeDisplay(cellState, date: date, selected: false)
    }
  }
  
  func calendar(_ calendar: JTAppleCalendarView,
                didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
    
    // By definition for this calendar, if you're scrolling you have no date selected.
    selectedDate = nil
    self.setupViewsOfCalendar(from: visibleDates)
  }
  
  func calendar(_ calendar: JTAppleCalendarView, willDisplayCell cell: JTAppleDayCellView,
                date: Date, cellState: CellState) {
    
    if let myCell = cell as? CellView {
      let selected = isSelected(date)
      myCell.setupCellBeforeDisplay(cellState, date: date, selected: selected)
    }
  }
  
  fileprivate func isSelected(_ date: Date) -> Bool {
    var selected = false
    if let selectedDate = selectedDate {
      if selectedDate == date {
        selected = true
      }
    }
    return selected
  }
  
  func calendar(_ calendar: JTAppleCalendarView, willResetCell cell: JTAppleDayCellView) {
    print("reset Cell")
  }
  
  func calendar(_ calendar: JTAppleCalendarView,
                willDisplaySectionHeader header: JTAppleHeaderView, range: (start: Date, end: Date),
                identifier: String) {
  }
}

























