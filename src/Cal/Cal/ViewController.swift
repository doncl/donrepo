//
//  ViewController.swift
//  Cal
//
//  Created by Don Clore on 10/26/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  @IBOutlet var timeBasedBan: TimeBasedBan!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    timeBasedBan.banName.text = "Total Ban"
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    timeBasedBan.setupViews()
  }
}

