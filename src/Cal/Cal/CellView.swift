//
//  CellView.swift
//  Cal
//
//  Created by Don Clore on 10/26/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit
import JTAppleCalendar
import SnapKit

class CellView: JTAppleDayCellView {
  var dayLabel : UILabel = UILabel()

  var normalDayColor = UIColor.black
  var weekendDayColor = UIColor.gray
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    addSubview(dayLabel)
    dayLabel.snp.makeConstraints { (make) -> Void in
      make.centerX.equalTo(self)
      make.centerY.equalTo(self)
    }
  }
   
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupCellBeforeDisplay(_ cellState: CellState, date: Date, selected: Bool = false) {
    // Setup Cell text
    dayLabel.text =  cellState.text
    
    if !selected {
      // Setup text color
      dayLabel.backgroundColor = UIColor.white
      configureTextColor(cellState)
    } else {
      dayLabel.backgroundColor = UIColor.lightGray
      dayLabel.textColor = UIColor.white
    }
  }
  
  func configureTextColor(_ cellState: CellState) {
    if cellState.dateBelongsTo == .thisMonth {
      dayLabel.textColor = normalDayColor
    } else {
      dayLabel.textColor = weekendDayColor
    }
  }
}
