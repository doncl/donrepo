//
//  Colors.swift
//  Duwamish
//
//  Created by Sonu on 8/1/16.
//  Copyright © 2016 AmplifyMedia. All rights reserved.
//

import Foundation
import UIKit

public struct Colors{
  
  //////////////////////////////////////////////////GREYS//////////////////////////////////////////////////
  
  public static let grey74 : UIColor! = UIColor(white: 74.0/255.0, alpha: 1.0)
  
  public static let grey121 : UIColor! = UIColor(white: 121.0/255.0, alpha: 1.0)
  
  public static let grey151 : UIColor! = UIColor(white: 151.0/255.0, alpha: 1.0)
  
  public static let grey191 : UIColor! = UIColor(white: 191.0 / 255.0, alpha: 1.0)
  
  public static let grey210 : UIColor! = UIColor(white: 210.0 / 255.0, alpha: 1.0)
  
  public static let grey238 : UIColor! = UIColor(white: 238.0/255.0, alpha: 1.0)
  
  public static let grey247 : UIColor! = UIColor(white: 247.0/255.0, alpha: 1.0)
    
  //////////////////////////////////////////////////GREYS//////////////////////////////////////////////////

  public static let coolGrey : UIColor! = UIColor(netHex:0xB7BBC0)
   
  public static let pinkColor = UIColor(netHex:0xF0274B)
  
  static let violet = UIColor(netHex:0x9400D3)
  
  static let indigo = UIColor(netHex:0x4B0082)
  
  static let lilyOrange = UIColor(netHex:0xff533d)
  
  static let velvetGreen = UIColor(netHex:0x28D2A5)
  
  static let blue = UIColor(netHex:0x0C31E9)
  
  static let salmon = UIColor(netHex:0xFA8072)
  
  /////////////////////////////////////////////////RANDOM//////////////////////////////////////////////////

  // Placeholder for whatever Yoshiko wants it to be.
  public static let atMentionColor : UIColor! = UIColor.blue
  
  public static var tintColor : UIColor! {
    get{
      return UIColor.darkGray
    }
  }
}

