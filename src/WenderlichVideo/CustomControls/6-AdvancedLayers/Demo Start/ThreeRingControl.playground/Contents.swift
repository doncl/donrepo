import PlaygroundSupport
import UIKit


let ringLayer = RingLayer()
ringLayer.value = 1.7
ringLayer.ringWidth = 60
ringLayer.ringColor = Color.pink

let ring2 = RingLayer()
ring2.value = 1.1
ring2.ringWidth = 60
ring2.ringColor = Color.blue

let ring3 = RingLayer()
ring3.value = 1.2
ring3.ringWidth = 60
ring3.ringColor = Color.green

PlaygroundPage.set(layers: ringLayer, ring2, ring3)
