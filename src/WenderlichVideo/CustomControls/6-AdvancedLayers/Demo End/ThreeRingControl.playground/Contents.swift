import PlaygroundSupport
import UIKit


let ringLayer = RingLayer()
ringLayer.value = 1.7
ringLayer.ringWidth = 60
ringLayer.ringColor = Color.pink

PlaygroundPage.set(layers: ringLayer)
