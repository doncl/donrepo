//
//  CollectionHeaderView.swift
//  Character Collector
//
//  Created by Don Clore on 12/31/17.
//  Copyright © 2017 Razeware, LLC. All rights reserved.
//

import UIKit

class CollectionHeaderView: UICollectionReusableView {
  @IBOutlet var backgroundImage : UIImageView!
  @IBOutlet var foregroundImage : UIImageView!
}
