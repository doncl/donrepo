//
//  RoundedCharacterCell.swift
//  Character Collector
//
//  Created by Don Clore on 12/31/17.
//  Copyright © 2017 Razeware, LLC. All rights reserved.
//

import UIKit

class RoundedCharacterCell: UICollectionViewCell {
  @IBOutlet var characterImage : UIImageView!
  @IBOutlet var characterTitle : UILabel!
  
  
  var character : Characters? {
    didSet {
      if let theCharacter = character {
        characterImage.image = UIImage(named: theCharacter.name)
        characterTitle.text = theCharacter.title
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    layer.cornerRadius = frame.size.width * 0.125
    layer.borderWidth = 6.0
    layer.borderColor = UIColor(red: 0.5, green: 0.47, blue: 0.25, alpha : 1.0).cgColor
  }

  override func prepareForReuse() {
    super.prepareForReuse()
    characterImage.image = nil
    characterTitle.text = ""
  }
}
