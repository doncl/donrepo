//
//  CharacterFlowLayout.swift
//  Character Collector
//
//  Created by Don Clore on 12/31/17.
//  Copyright © 2017 Razeware, LLC. All rights reserved.
//

import UIKit

class CharacterFlowLayout: UICollectionViewFlowLayout {
  let standardItemAlpha : CGFloat = 0.5
  let standardItemScale : CGFloat = 0.5
  
  override func prepare() {
    super.prepare()
  }

  override func layoutAttributesForElements(in rect: CGRect)
    -> [UICollectionViewLayoutAttributes]? {

    guard let attributes = super.layoutAttributesForElements(in: rect) else {
      return nil
    }
    var attributesCopy = [UICollectionViewLayoutAttributes]()
    
    for itemAttributes in attributes {
      let itemAttributesCopy = itemAttributes.copy() as! UICollectionViewLayoutAttributes
      changeLayoutAttributes(itemAttributesCopy)
      attributesCopy.append(itemAttributesCopy)
    }
      
    return attributesCopy
  }
  
  override open func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
    return true
  }
  
  func changeLayoutAttributes(_ attributes: UICollectionViewLayoutAttributes) {
    let collectionCenter = collectionView!.frame.size.height / 2
    let offset = collectionView!.contentOffset.y
    let normalizedCenter = attributes.center.y - offset
    
    let maxDistance = itemSize.height + minimumLineSpacing
    let distance = min(abs(collectionCenter - normalizedCenter), maxDistance)
  
    let ratio = (maxDistance - distance) / maxDistance
    
    let alpha = ratio * (1 - standardItemAlpha) + standardItemAlpha
    let scale = ratio * (1 - standardItemScale) + standardItemScale
    
    attributes.alpha = alpha
    attributes.transform3D = CATransform3DScale(CATransform3DIdentity, scale, scale, 1)
  }
}
