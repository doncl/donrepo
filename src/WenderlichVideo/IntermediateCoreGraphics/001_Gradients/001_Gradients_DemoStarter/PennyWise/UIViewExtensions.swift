//
//  UIViewExtensions.swift
//  PennyWise
//
//  Created by Don Clore on 6/27/17.
//  Copyright © 2017 Caroline. All rights reserved.
//

import UIKit

extension UIView {
  
  func drawGradient(startColor : UIColor, endColor : UIColor, startPoint : CGPoint,
    endPoint : CGPoint) {
    
    let colors : [CGColor] = [startColor.cgColor, endColor.cgColor]
    let colorSpace = CGColorSpaceCreateDeviceRGB()
    let colorLocations : [CGFloat] = [0.0, 1.0]
    let gradient = CGGradient(colorsSpace: colorSpace, colors: colors as CFArray,
      locations: colorLocations)!
    
    let context = UIGraphicsGetCurrentContext()!
    
    context.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: [])
    
  }
}
