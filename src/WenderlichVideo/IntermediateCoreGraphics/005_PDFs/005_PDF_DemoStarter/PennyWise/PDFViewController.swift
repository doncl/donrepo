/*
* Copyright (c) 2016 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import UIKit

class PDFViewController: UIViewController {
  fileprivate var pageNumber:Int = 1

  
  @IBOutlet var webView: UIWebView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Create the pdf
    createPDF()

    // Load the pdf file in a UIWebView
    if let url = documentPath() {
      webView.loadRequest(URLRequest(url: url))
    }
  }
  
  // Document Directory URL
  fileprivate func documentPath() -> URL? {
    let fileManager = FileManager.default
    let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0] as URL
    return documentsUrl.appendingPathComponent("PennyWise.pdf")
  }
  
  fileprivate func createPDF() {
    // Create PDF
    // set the default page size to 8.5 by 11 inches
    // (612 by 792 points).
    
    guard let pdfPath = documentPath()?.path else {
      return
    }
    
    UIGraphicsBeginPDFContextToFile(pdfPath, .zero, nil)
    UIGraphicsBeginPDFPage()
    renderBackground()
    renderCategories()
    
    printPageNumber()
    
    UIGraphicsBeginPDFPage()
    
    renderBackground()
    renderPieChart()
    printPageNumber()
    
    UIGraphicsEndPDFContext()
  }
  
  fileprivate func renderBackground() {
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    let rect = context.boundingBoxOfClipPath
    
    lightViewColor.setFill()
    UIRectFill(rect)
  }
  
  fileprivate func renderCategories() {
    guard let context = UIGraphicsGetCurrentContext(),
      let storyboard = storyboard,
      let budgetController =
        storyboard.instantiateViewController(withIdentifier: "BudgetTableViewController")
          as? BudgetTableViewController else {
            
      return
    }
    
    let width = budgetController.tableView.contentSize.width
    let height = budgetController.tableView.rowHeight * CGFloat(categories.count)
    
    let frame = CGRect(origin: .zero, size: CGSize(width: width, height: height))
    budgetController.tableView.frame = frame
    
    context.saveGState()
 
  
    let rect = context.boundingBoxOfClipPath
    context.translateBy(x: (rect.width - width ) / 2, y: (rect.height - height) / 2)
 
    
    budgetController.tableView.layer.render(in: context)
    context.restoreGState()
  
    let attributes = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 36),
                      NSForegroundColorAttributeName : UIColor.white]
    
    let title = "Pennywise Budget"
    title.draw(at: CGPoint(x: 32, y: 40), withAttributes: attributes)
  }

  func renderPieChart() {
    
    guard let context = UIGraphicsGetCurrentContext() else { return }
    let attributes = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 26),
                      NSForegroundColorAttributeName: UIColor.white]
    let title = "Pie Chart of Expenses"
    title.draw(at: CGPoint(x: 32, y: 40), withAttributes: attributes)
    
    let graphView = GraphView(frame:UIScreen.main.bounds)
    
    if graphView.frame.width < graphView.frame.height {
      graphView.frame.size.width = UIScreen.main.bounds.height
      graphView.frame.size.height = UIScreen.main.bounds.width
    }
    
    graphView.backgroundColor = UIColor.clear
    
    
    context.saveGState()
    context.translateBy(x: (612 - graphView.frame.width) / 2,
                        y: (792 - graphView.frame.height) / 3)
    
    graphView.layer.render(in: context)
    
    context.restoreGState()
    
  }
  
  func printPageNumber() {
    guard let context = UIGraphicsGetCurrentContext() else { return }
    
    let rect = context.boundingBoxOfClipPath
    
    let attributes = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 20),
                      NSForegroundColorAttributeName: UIColor.white]
    let pageNumberString = "\(pageNumber)"
    pageNumberString.draw(at: CGPoint(x: rect.midX, y: rect.height - 40), withAttributes: attributes)
    pageNumber += 1
    
  }

}


