/*
* Copyright (c) 2016 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
import UIKit

@IBDesignable

class GraphView: UIView {
  let chartColors = [
    UIColor(red: 1.0, green: 107/255, blue: 107/255, alpha: 1.0),
    UIColor(red: 155/255, green: 224/255, blue: 172/255, alpha: 1.0),
    UIColor(red: 136/255, green: 161/255, blue: 212/255, alpha: 1.0),
    UIColor(red: 1.0, green: 172/255, blue: 99/255, alpha: 1.0),
    UIColor(red: 135/255, green: 218/255, blue: 230/255, alpha: 1.0),
    UIColor(red: 250/255, green: 250/255, blue: 147/255, alpha: 1.0)]

  override func draw(_ rect:CGRect) {
    
    
    
    
    let context = UIGraphicsGetCurrentContext()
    
    let totalSpent = categories.reduce(0,
      {$0 + $1.spent})
    guard totalSpent > 0 else { return }
    
    let diameter = min(bounds.width, bounds.height)
    let margin:CGFloat = 20
    
    let circle = UIBezierPath(ovalIn:
      CGRect(x:0, y:0,
        width:diameter,
        height:diameter
        ).insetBy(dx: margin, dy: margin))
    
    let transform = CGAffineTransform(translationX: bounds.width/2 - diameter/2 ,y: 0)
    
    circle.apply(transform)
    
    context?.saveGState()
    
    context?.scaleBy(x: 1, y: 0.6)
    context?.translateBy(x: 0, y: 100)
    
    
    let shadowColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.4)
    context?.setShadow(offset: CGSize(width: 10, height: 10),
              blur: 1,
              color: shadowColor.cgColor)
    
    context?.beginTransparencyLayer(auxiliaryInfo: nil)
    
    let workingCategories = categories.filter({ $0.spent > 0 })
    
    for (index, _) in workingCategories.enumerated() {
    
      let percent = CGFloat(workingCategories[index].spent/totalSpent)
      let angle = percent * 2 * π
      
      print(workingCategories[index].name, workingCategories[index].spent, round(percent * 100))
      
      let slice = UIBezierPath()
      
      let radius = diameter / 2 - margin
      let centerPoint = center
      
      slice.move(to: centerPoint)
      slice.addLine(to: CGPoint(x:centerPoint.x + radius, y:centerPoint.y))
      slice.addArc(withCenter: centerPoint, radius:radius, startAngle: 0, endAngle: angle, clockwise: true)
      slice.close()
      
      chartColors[index].setFill()
      slice.fill()
      
      drawText(workingCategories[index].name, centerPoint: centerPoint, radius:radius, angle:angle)

      
      context?.translateBy(x: centerPoint.x, y: centerPoint.y)
      context?.rotate(by: angle)
      context?.translateBy(x: -centerPoint.x, y: -centerPoint.y)
      
    }
    circle.stroke()
    
    context?.endTransparencyLayer()
    context?.restoreGState()
    
    
  }
  
  func drawText(_ name: String, centerPoint:CGPoint, radius:CGFloat, angle:CGFloat) {
  
    let context = UIGraphicsGetCurrentContext()

    context?.saveGState()

    context?.translateBy(x: centerPoint.x, y: centerPoint.y)
    context?.rotate(by: angle/2)
    context?.translateBy(x: -centerPoint.x, y: -centerPoint.y)

    
    let font = UIFont(name: "HelveticaNeue-Bold", size: 18)!
    let attributes = [NSFontAttributeName: font,
      NSForegroundColorAttributeName: UIColor.white]
    

    let transform = context?.ctm
    let radians = atan2((transform?.b)!, (transform?.a)!)
    if abs(radians) > π / 2 && abs(radians) < 3 / 2 * π {

      let textBounds = name.size(attributes: attributes)

      context?.saveGState()
      context?.rotate(by: π)
      
      name.draw(at: CGPoint(x:-centerPoint.x - radius - textBounds.width - 10, y:-centerPoint.y), withAttributes:attributes)
      
      context?.restoreGState()


    } else {
      name.draw(at: CGPoint(x:centerPoint.x + radius + 10, y:centerPoint.y), withAttributes:attributes)
    }
    
    context?.restoreGState()

  }
  
  func drawKey(_ categories: [Category], rect: CGRect) {
    let context = UIGraphicsGetCurrentContext()
    
    let font = UIFont(name: "HelveticaNeue-Bold", size: 14)!
    let attributes = [NSFontAttributeName: font,
      NSForegroundColorAttributeName:
                                     UIColor.white]
    let boxSize: CGFloat = 15
    let margin: CGFloat = 12
    
    var row: CGFloat = 0
    
    var largestWidth: CGFloat = 0
    for category in categories {
        let textBounds = category.name.size(attributes: attributes)
      if textBounds.width > largestWidth {
            largestWidth = textBounds.width
      }
    }
    
    context?.saveGState()
    
    let offsetX = rect.width - largestWidth - margin - boxSize - margin
    
    let totalHeight = CGFloat(categories.count) * (margin + boxSize)
    let offsetY = rect.height/2 - totalHeight/2
    
    
    context?.translateBy(x: offsetX, y: offsetY)
    
    
    for (index, category) in categories.enumerated() {
      
      UIColor.black.setStroke()
      
      let box = UIBezierPath(rect: CGRect(x: 0, y: row,
        width: boxSize, height: boxSize))
      chartColors[index].setFill()
      box.fill()
      box.lineWidth = 1
      box.stroke()
      
      let point = CGPoint(x: boxSize + margin, y: row)
      category.name.draw(at: point, withAttributes: attributes)
      
      
      
      row += boxSize + margin
      
    }
    
    context?.restoreGState()
    
    
  }
  
  func drawImage() -> UIImage {
    
    let rect = CGRect(x: 0, y: 0, width: 20, height: 20)
    UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0)
    
    darkViewColor.setFill()
    UIRectFill(rect)
    
let context = UIGraphicsGetCurrentContext()
    context?.setAlpha(0.5)
    lightViewColor.setFill()
    UIBezierPath(rect: rect.insetBy(dx: 0, dy: 9)).fill()
    
    
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image!
    
  }
  
  
}

