//
//  RoundedView.swift
//  RWBooks
//
//  Created by Don Clore on 5/21/17.
//  Copyright © 2017 Ray Wenderlich. All rights reserved.
//

import UIKit
import QuartzCore

@IBDesignable
class RoundedView: UIView {


  override func layoutSubviews() {
    super.layoutSubviews()
    
    let shapeLayer = CAShapeLayer()
    
    shapeLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.topLeft, .topRight],
      cornerRadii: CGSize(width: 30.0, height: 30.0)).cgPath
    
    layer.mask = shapeLayer
  }

}
