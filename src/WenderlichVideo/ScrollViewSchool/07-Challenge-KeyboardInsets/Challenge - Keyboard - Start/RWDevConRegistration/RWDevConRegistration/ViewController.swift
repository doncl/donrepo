//
//  ViewController.swift
//  RWDevConRegistration
//
//  Created by Brian on 2/16/17.
//  Copyright © 2017 Razeware. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var keyboardVisible: Bool = false
  
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet var nameTextField: UITextField!
  @IBOutlet var emailTextField: UITextField!
  
  @objc func adjustInsetForKeyboard(_ note: NSNotification) {
    guard let userinfo = note.userInfo else {
        return
    }
    guard let keyboardFrame = (userinfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
        return
    }
    let show = note.name == UIResponder.keyboardWillShowNotification ? true : false
    if show == keyboardVisible {
        return
    }
    keyboardVisible = show
    let adjustmentHeight = (keyboardFrame.height + 20) * (show ? 1 : -1)
    scrollView.contentInset.bottom += adjustmentHeight
    scrollView.scrollIndicatorInsets.bottom += adjustmentHeight
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    nameTextField.delegate = self
    emailTextField.delegate = self
    
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(adjustInsetForKeyboard(_:)),
      name: UIResponder.keyboardWillHideNotification,
      object: nil
    )
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(adjustInsetForKeyboard(_:)),
      name: UIResponder.keyboardWillShowNotification,
      object: nil)

    nameTextField.delegate = self
    emailTextField.delegate = self

  }
}

extension ViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}

