//
//  ViewController.swift
//  CoreLocationDemo
//
//  Created by Brian on 4/22/16.
//  Copyright © 2016 Razeware LLC. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
  
  var locationManager: CLLocationManager?
  var startLocation: CLLocation?

  override func viewDidLoad() {
    super.viewDidLoad()
    
    locationManager = CLLocationManager()
    locationManager?.delegate = self
    locationManager?.desiredAccuracy = kCLLocationAccuracyBest
    locationManager?.requestWhenInUseAuthorization()
    
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

}

extension ViewController: CLLocationManagerDelegate {
  
  func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    if startLocation == nil {
      startLocation = locations.first
    } else {
      guard let latest = locations.first else { return }
      let distanceInMeters = startLocation?.distanceFromLocation(latest)
      print("distance in meters: \(distanceInMeters)")
    }
  
  }
  
  func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
    if status == .AuthorizedWhenInUse || status == .AuthorizedAlways {
      locationManager?.startUpdatingLocation()
    }
  }
  
}

