//
//  OddPlace.swift
//  InterestingPlaces
//
//  Created by Brian on 5/10/16.
//  Copyright © 2016 Razeware LLC. All rights reserved.
//

import Foundation
import CoreLocation

class OddPlace {
  
  let location: CLLocation
  let name: String
  let imageName: String
  
  init(latitude: Double, longitude: Double, name: String,
       imageName: String) {
    location = CLLocation(latitude: latitude,
                          longitude: longitude)
    self.name = name
    self.imageName = imageName
  }

  
  
}

