//
//  ViewController.swift
//  InterestingPlaces
//
//  Created by Brian on 4/7/16.
//  Copyright © 2016 Razeware LLC. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {

  @IBOutlet weak var placeName: UILabel!
  @IBOutlet weak var locationDistance: UILabel!
  @IBOutlet weak var placeImage: UIImageView!
  var places:[OddPlace] = []
  var selectedPlace:OddPlace? = nil
  var locationManager: CLLocationManager?
  var startLocation: CLLocation?

  
  override func viewDidLoad() {
    super.viewDidLoad()
    loadPlaces()
    selectedPlace = places.first
    updateUI()
    
    locationManager = CLLocationManager()
    locationManager?.desiredAccuracy = kCLLocationAccuracyBest
    locationManager?.delegate = self
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  @IBAction func startLocationService(sender: AnyObject) {
    locationManager?.requestWhenInUseAuthorization()
  }
  
  @IBAction func selectPlace(sender: AnyObject) {
    guard let button = sender as? UIButton else { return }
    selectedPlace = places[button.tag]
    updateUI()
  }
  
  func updateUI() {
    placeName.text = selectedPlace?.name
    guard let imageName = selectedPlace?.imageName,
      let image = UIImage(named:imageName)  else { return }
    placeImage.image = image
    // Request location here
  }
  
  func loadPlaces() {
    guard let plistUrl = NSBundle.mainBundle().URLForResource("Places", withExtension: "plist"), let plistData = NSData(contentsOfURL: plistUrl) else { return }
    var format = NSPropertyListFormat.XMLFormat_v1_0
    var placedEntries: NSArray? = nil
    
    do {
      placedEntries = try NSPropertyListSerialization.propertyListWithData(plistData, options:.Immutable, format: &format) as? NSArray
    } catch {
      print("error reading plist")
    }
    if placedEntries != nil {
      for property in placedEntries! {
        if let name = property["Name"] as? String,
          let latitude = property["Latitude"] as? NSNumber,
          let longitude = property["Longitude"] as? NSNumber,
          let image = property["Image"] as? String {
          
          // To learn how to read data from plist file, check out our Saving Data
          // in iOS Video Tutorial series.
          
          let place = OddPlace(latitude: latitude.doubleValue, longitude:
            longitude.doubleValue, name: name, imageName: image)
          places.append(place)
        }
      }
    }
  }
}

extension ViewController: CLLocationManagerDelegate {
  
  func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
    if status == .AuthorizedAlways || status == .AuthorizedWhenInUse {
      locationManager?.startUpdatingLocation()
    }
  }
  
  func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    if startLocation == nil {
      startLocation = locations.first
    } else {
      guard let selectedPlace = selectedPlace else { return }
      if let metersFromLocation = startLocation?.distanceFromLocation(selectedPlace.location) {
        let miles = Int((metersFromLocation / 1000) * 0.62137)
        locationDistance.text = "You are \(miles) miles away"
      }
    }
  }
}

