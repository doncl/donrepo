//
//  ViewController.swift
//  InterestingPlaces
//
//  Created by Brian on 4/7/16.
//  Copyright © 2016 Razeware LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var placeName: UILabel!
  @IBOutlet weak var locationDistance: UILabel!
  @IBOutlet weak var placeImage: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    loadPlaces()
    updateUI()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  @IBAction func startLocationService(sender: AnyObject) {
  }
  
  @IBAction func selectPlace(sender: AnyObject) {
    guard let button = sender as? UIButton else { return }
    updateUI()
  }
  
  func updateUI() {
  }
  
  func loadPlaces() {
    guard let plistUrl = NSBundle.mainBundle().URLForResource("Places", withExtension: "plist"), let plistData = NSData(contentsOfURL: plistUrl) else { return }
    var format = NSPropertyListFormat.XMLFormat_v1_0
    var placedEntries: NSArray? = nil
    
    do {
      placedEntries = try NSPropertyListSerialization.propertyListWithData(plistData, options:.Immutable, format: &format) as? NSArray
    } catch {
      print("error reading plist")
    }
    if placedEntries != nil {
      for property in placedEntries! {
        if let name = property["Name"] as? String,
          let latitude = property["Latitude"] as? NSNumber,
          let longitude = property["Longitude"] as? NSNumber,
          let image = property["Image"] as? String {
          
          // To learn how to read data from plist file, check out our Saving Data
          // in iOS Video Tutorial series.
          
          // Code goes here

          
        }
      }
    }
  }
}
