//
//  Icons.swift
//  PennyWise
//
//  Created by Don Clore on 6/26/17.
//  Copyright © 2017 TheMaven Network. All rights reserved.
//
//  Generated by PaintCode
//  http://www.paintcodeapp.com
//



import UIKit

public class Icons : NSObject {

    //// Drawing Methods

    public dynamic class func drawAudoIcon(frame: CGRect = CGRect(x: 0, y: 0, width: 400, height: 400), isSelected: Bool = false) {
        //// General Declarations
        // This non-generic function dramatically improves compilation times of complex expressions.
        func fastFloor(_ x: CGFloat) -> CGFloat { return floor(x) }

        //// Color Declarations
        let color = UIColor(red: 0.651, green: 0.588, blue: 0.588, alpha: 1.000)
        let color2 = UIColor(red: 0.889, green: 0.990, blue: 0.000, alpha: 1.000)
        let selectedColor = UIColor(red: 0.838, green: 0.262, blue: 0.262, alpha: 1.000)
        let unselectedColor = UIColor(red: 0.579, green: 0.884, blue: 0.267, alpha: 0.983)

        //// Variable Declarations
        let selectColor = isSelected ? selectedColor : unselectedColor


        //// Subframes
        let group: CGRect = CGRect(x: frame.minX + fastFloor(frame.width * 0.04616 + 0.04) + 0.46, y: frame.minY + fastFloor(frame.height * 0.14247 - 0.49) + 0.99, width: fastFloor(frame.width * 0.94750 - 0.5) - fastFloor(frame.width * 0.04616 + 0.04) + 0.54, height: fastFloor(frame.height * 0.81000 + 0.5) - fastFloor(frame.height * 0.14247 - 0.49) - 0.99)


        //// Group
        //// Wheel1 Drawing
        let wheel1Path = UIBezierPath(ovalIn: CGRect(x: group.minX + fastFloor(group.width * 0.16513 - 0.04) + 0.54, y: group.minY + fastFloor(group.height * 0.76405 + 0.49) + 0.01, width: fastFloor(group.width * 0.34542 - 0.04) - fastFloor(group.width * 0.16513 - 0.04), height: fastFloor(group.height * 1.00000 + 0.49) - fastFloor(group.height * 0.76405 + 0.49)))
        color.setFill()
        wheel1Path.fill()
        selectColor.setStroke()
        wheel1Path.lineWidth = 2
        wheel1Path.stroke()


        //// Wheel2 Drawing
        let wheel2Path = UIBezierPath(ovalIn: CGRect(x: group.minX + fastFloor(group.width * 0.66994 - 0.04) + 0.54, y: group.minY + fastFloor(group.height * 0.76405 + 0.49) + 0.01, width: fastFloor(group.width * 0.85022 - 0.04) - fastFloor(group.width * 0.66994 - 0.04), height: fastFloor(group.height * 1.00000 + 0.49) - fastFloor(group.height * 0.76405 + 0.49)))
        color.setFill()
        wheel2Path.fill()
        selectColor.setStroke()
        wheel2Path.lineWidth = 2
        wheel2Path.stroke()


        //// Car Body Drawing
        let carBodyPath = UIBezierPath()
        carBodyPath.move(to: CGPoint(x: group.minX + 0.00010 * group.width, y: group.minY + 0.68893 * group.height))
        carBodyPath.addCurve(to: CGPoint(x: group.minX + 0.09302 * group.width, y: group.minY + 0.50002 * group.height), controlPoint1: CGPoint(x: group.minX + 0.04035 * group.width, y: group.minY + 0.62201 * group.height), controlPoint2: CGPoint(x: group.minX + 0.00520 * group.width, y: group.minY + 0.60824 * group.height))
        carBodyPath.addCurve(to: CGPoint(x: group.minX + 0.23447 * group.width, y: group.minY + 0.40220 * group.height), controlPoint1: CGPoint(x: group.minX + 0.12827 * group.width, y: group.minY + 0.45658 * group.height), controlPoint2: CGPoint(x: group.minX + 0.20043 * group.width, y: group.minY + 0.45518 * group.height))
        carBodyPath.addCurve(to: CGPoint(x: group.minX + 0.50629 * group.width, y: group.minY + 0.00004 * group.height), controlPoint1: CGPoint(x: group.minX + 0.34238 * group.width, y: group.minY + 0.23425 * group.height), controlPoint2: CGPoint(x: group.minX + 0.29955 * group.width, y: group.minY + -0.00356 * group.height))
        carBodyPath.addCurve(to: CGPoint(x: group.minX + 0.76701 * group.width, y: group.minY + 0.40220 * group.height), controlPoint1: CGPoint(x: group.minX + 0.71431 * group.width, y: group.minY + 0.00366 * group.height), controlPoint2: CGPoint(x: group.minX + 0.73144 * group.width, y: group.minY + 0.35215 * group.height))
        carBodyPath.addCurve(to: CGPoint(x: group.minX + 0.90570 * group.width, y: group.minY + 0.50002 * group.height), controlPoint1: CGPoint(x: group.minX + 0.79256 * group.width, y: group.minY + 0.43815 * group.height), controlPoint2: CGPoint(x: group.minX + 0.85818 * group.width, y: group.minY + 0.44305 * group.height))
        carBodyPath.addCurve(to: CGPoint(x: group.minX + 1.00000 * group.width, y: group.minY + 0.68842 * group.height), controlPoint1: CGPoint(x: group.minX + 0.96529 * group.width, y: group.minY + 0.57146 * group.height), controlPoint2: CGPoint(x: group.minX + 1.00000 * group.width, y: group.minY + 0.68842 * group.height))
        carBodyPath.addCurve(to: CGPoint(x: group.minX + 0.00010 * group.width, y: group.minY + 0.68893 * group.height), controlPoint1: CGPoint(x: group.minX + 1.00000 * group.width, y: group.minY + 0.68842 * group.height), controlPoint2: CGPoint(x: group.minX + -0.01147 * group.width, y: group.minY + 0.70816 * group.height))
        carBodyPath.close()
        color2.setFill()
        carBodyPath.fill()
        selectColor.setStroke()
        carBodyPath.lineWidth = 2
        carBodyPath.stroke()


        //// Car Window Drawing
        let carWindowPath = UIBezierPath()
        carWindowPath.move(to: CGPoint(x: group.minX + 0.50074 * group.width, y: group.minY + 0.09424 * group.height))
        carWindowPath.addLine(to: CGPoint(x: group.minX + 0.50074 * group.width, y: group.minY + 0.38046 * group.height))
        carWindowPath.addLine(to: CGPoint(x: group.minX + 0.69490 * group.width, y: group.minY + 0.38046 * group.height))
        carWindowPath.addCurve(to: CGPoint(x: group.minX + 0.65329 * group.width, y: group.minY + 0.22467 * group.height), controlPoint1: CGPoint(x: group.minX + 0.69490 * group.width, y: group.minY + 0.38046 * group.height), controlPoint2: CGPoint(x: group.minX + 0.67273 * group.width, y: group.minY + 0.25527 * group.height))
        carWindowPath.addCurve(to: CGPoint(x: group.minX + 0.58395 * group.width, y: group.minY + 0.14496 * group.height), controlPoint1: CGPoint(x: group.minX + 0.62741 * group.width, y: group.minY + 0.18391 * group.height), controlPoint2: CGPoint(x: group.minX + 0.58395 * group.width, y: group.minY + 0.14496 * group.height))
        carWindowPath.addCurve(to: CGPoint(x: group.minX + 0.50074 * group.width, y: group.minY + 0.09424 * group.height), controlPoint1: CGPoint(x: group.minX + 0.58395 * group.width, y: group.minY + 0.14496 * group.height), controlPoint2: CGPoint(x: group.minX + 0.49934 * group.width, y: group.minY + 0.09424 * group.height))
        UIColor.white.setFill()
        carWindowPath.fill()
        selectColor.setStroke()
        carWindowPath.lineWidth = 1
        carWindowPath.stroke()
    }

    public dynamic class func drawMiscellaneous(frame targetFrame: CGRect = CGRect(x: 0, y: 0, width: 400, height: 400), resizing: ResizingBehavior = .aspectFit, isSelected: Bool = false) {
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()!
        
        //// Resize to Target Frame
        context.saveGState()
        let resizedFrame: CGRect = resizing.apply(rect: CGRect(x: 0, y: 0, width: 400, height: 400), target: targetFrame)
        context.translateBy(x: resizedFrame.minX, y: resizedFrame.minY)
        context.scaleBy(x: resizedFrame.width / 400, y: resizedFrame.height / 400)


        //// Color Declarations
        let selectedColor = UIColor(red: 0.838, green: 0.262, blue: 0.262, alpha: 1.000)
        let black = UIColor(red: 0.000, green: 0.000, blue: 0.000, alpha: 1.000)

        //// Variable Declarations
        let questionSelectColor = isSelected ? selectedColor : black

        //// QuestionMarkBody Drawing
        let questionMarkBodyPath = UIBezierPath()
        questionMarkBodyPath.move(to: CGPoint(x: 134.5, y: 96.5))
        questionMarkBodyPath.addCurve(to: CGPoint(x: 152.5, y: 71.5), controlPoint1: CGPoint(x: 155.5, y: 60.5), controlPoint2: CGPoint(x: 140.25, y: 84.25))
        questionMarkBodyPath.addCurve(to: CGPoint(x: 182.5, y: 58.5), controlPoint1: CGPoint(x: 164.75, y: 58.75), controlPoint2: CGPoint(x: 163.5, y: 64.5))
        questionMarkBodyPath.addCurve(to: CGPoint(x: 217.5, y: 58.5), controlPoint1: CGPoint(x: 201.5, y: 52.5), controlPoint2: CGPoint(x: 199, y: 55.25))
        questionMarkBodyPath.addCurve(to: CGPoint(x: 251.5, y: 71.5), controlPoint1: CGPoint(x: 236, y: 61.75), controlPoint2: CGPoint(x: 239.25, y: 62))
        questionMarkBodyPath.addCurve(to: CGPoint(x: 266.5, y: 96.5), controlPoint1: CGPoint(x: 263.75, y: 81), controlPoint2: CGPoint(x: 262.75, y: 83.25))
        questionMarkBodyPath.addCurve(to: CGPoint(x: 266.5, y: 124.5), controlPoint1: CGPoint(x: 270.25, y: 109.75), controlPoint2: CGPoint(x: 270.25, y: 110.5))
        questionMarkBodyPath.addCurve(to: CGPoint(x: 251.5, y: 152.5), controlPoint1: CGPoint(x: 262.75, y: 138.5), controlPoint2: CGPoint(x: 261, y: 141.25))
        questionMarkBodyPath.addCurve(to: CGPoint(x: 228.5, y: 169.5), controlPoint1: CGPoint(x: 242, y: 163.75), controlPoint2: CGPoint(x: 241.25, y: 161.5))
        questionMarkBodyPath.addCurve(to: CGPoint(x: 200.5, y: 184.5), controlPoint1: CGPoint(x: 215.75, y: 177.5), controlPoint2: CGPoint(x: 200.5, y: 184.5))
        questionMarkBodyPath.addLine(to: CGPoint(x: 200.5, y: 278.5))
        questionSelectColor.setStroke()
        questionMarkBodyPath.lineWidth = 11
        questionMarkBodyPath.stroke()


        //// QuestionMarkPoint Drawing
        let questionMarkPointPath = UIBezierPath(ovalIn: CGRect(x: 190.5, y: 301.5, width: 23, height: 23))
        questionSelectColor.setStroke()
        questionMarkPointPath.lineWidth = 11
        questionMarkPointPath.stroke()
        
        context.restoreGState()

    }




    @objc(IconsResizingBehavior)
    public enum ResizingBehavior: Int {
        case aspectFit /// The content is proportionally resized to fit into the target rectangle.
        case aspectFill /// The content is proportionally resized to completely fill the target rectangle.
        case stretch /// The content is stretched to match the entire target rectangle.
        case center /// The content is centered in the target rectangle, but it is NOT resized.

        public func apply(rect: CGRect, target: CGRect) -> CGRect {
            if rect == target || target == CGRect.zero {
                return rect
            }

            var scales = CGSize.zero
            scales.width = abs(target.width / rect.width)
            scales.height = abs(target.height / rect.height)

            switch self {
                case .aspectFit:
                    scales.width = min(scales.width, scales.height)
                    scales.height = scales.width
                case .aspectFill:
                    scales.width = max(scales.width, scales.height)
                    scales.height = scales.width
                case .stretch:
                    break
                case .center:
                    scales.width = 1
                    scales.height = 1
            }

            var result = rect.standardized
            result.size.width *= scales.width
            result.size.height *= scales.height
            result.origin.x = target.minX + (target.width - result.width) / 2
            result.origin.y = target.minY + (target.height - result.height) / 2
            return result
        }
    }
}
