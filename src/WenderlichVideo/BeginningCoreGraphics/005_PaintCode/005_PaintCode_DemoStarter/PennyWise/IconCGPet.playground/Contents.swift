//: Playground - noun: a place where people can play

import UIKit


let size = CGSize(width: 400, height: 400)
let rect = CGRect(origin: .zero, size: size)

let lineWidth:CGFloat = 5.0

UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
let context = UIGraphicsGetCurrentContext()!


context.setLineWidth(lineWidth)
context.setFillColor(UIColor.red.cgColor)
context.setStrokeColor(UIColor.black.cgColor)

let insetRect = rect.insetBy(dx: lineWidth / 2, dy: lineWidth / 2)

context.fillEllipse(in: insetRect)
context.strokeEllipse(in: insetRect)

context.strokeEllipse(in: CGRect(x: 130, y: 174, width: 140, height: 154))
context.strokeEllipse(in: CGRect(x: 71, y: 104, width: 71, height: 89))
context.strokeEllipse(in: CGRect(x: 161, y: 66, width: 71, height: 89))
context.strokeEllipse(in: CGRect(x: 254, y: 104, width: 71, height: 89))

let image = UIGraphicsGetImageFromCurrentImageContext()
UIGraphicsEndImageContext()
image

