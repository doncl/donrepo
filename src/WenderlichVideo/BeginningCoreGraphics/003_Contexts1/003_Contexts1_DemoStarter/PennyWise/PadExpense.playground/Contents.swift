//: Playground - noun: a place where people can play

import UIKit


let size = CGSize(width: 400, height: 400)
let rect = CGRect(origin: .zero, size: size)

let backgroundColor : UIColor = .white
let drawingColor : UIColor = .black

let lineWidth : CGFloat = 5.0

UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
let edge = UIBezierPath(roundedRect: rect.insetBy(dx: lineWidth * 4, dy: lineWidth * 4), cornerRadius: 50)

edge.lineWidth = lineWidth
backgroundColor.setFill()
UIRectFill(rect)

drawingColor.setStroke()
edge.stroke()

let lowerWidth : CGFloat = 130
let lowerX : CGFloat = rect.midX - (lowerWidth / 2)

let lowerHeight : CGFloat = 140
let lowerPad = UIBezierPath(ovalIn: CGRect(x: lowerX, y: 180, width: lowerWidth, height: lowerHeight))
lowerPad.lineWidth = lineWidth
lowerPad.stroke()

let leftx : CGFloat = 85.0
let lefty :CGFloat  = 120
let leftWidth = lowerWidth / 2 - 5
let leftHeight = lowerHeight / 2 + 5

let leftPad = UIBezierPath(ovalIn: CGRect(x: leftx, y: lefty, width: leftWidth, height: leftHeight))
leftPad.lineWidth = lineWidth
leftPad.stroke()

let rightx : CGFloat = 400.0 - 85.0 - leftWidth
let righty = lefty
let rightWidth = leftWidth
let rightHeight = leftHeight
let rightRect = CGRect(x: rightx, y: righty, width: rightWidth, height: rightHeight)
let rightPad = UIBezierPath(ovalIn: rightRect)
rightPad.lineWidth = lineWidth
rightPad.stroke()

let leftTrailing : CGFloat = leftx + leftWidth
let leftToRightCx : CGFloat = rightx - leftTrailing

let topX : CGFloat = leftTrailing + (leftToRightCx / 2.0) - (leftWidth / 2.0)
let topY : CGFloat = lefty - (leftHeight * 0.50)
let topWidth = leftWidth
let topHeight = leftHeight
let topRect = CGRect(x: topX, y: topY, width: topWidth, height: topHeight)
let topPad = UIBezierPath(ovalIn: topRect)
topPad.lineWidth = lineWidth
topPad.stroke()



let image = UIGraphicsGetImageFromCurrentImageContext()
UIGraphicsEndImageContext()
image
