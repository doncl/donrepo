//
//  CustomSplitViewController.swift
//  Project8-iOSDesign
//
//  Created by Paul Hudson on 19/06/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import UIKit

class CustomSplitViewController: UISplitViewController {
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func preferredScreenEdgesDeferringSystemGestures() -> UIRectEdge {
        return .top
    }
}
