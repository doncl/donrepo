//
//  ViewController.swift
//  Project2b
//
//  Created by Paul Hudson on 14/06/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import ARKit
import CoreLocation
import GameplayKit
import SpriteKit
import UIKit

class ViewController: UIViewController, ARSKViewDelegate, CLLocationManagerDelegate {
    @IBOutlet var sceneView: ARSKView!

    let locationManager = CLLocationManager()
    var userLocation = CLLocation()
    var userHeading = 0.0
    var headingCount = 0

    var pages = [UUID: String]()
    var sightsJSON: JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and node count
        sceneView.showsFPS = true
        sceneView.showsNodeCount = true
        
        // Load the SKScene from 'Scene.sks'
        if let scene = SKScene(fileNamed: "Scene") {
            sceneView.presentScene(scene)
        }

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        userLocation = location

        DispatchQueue.global().async {
            self.fetchSights()
        }
    }

    func fetchSights() {
        let urlString = "https://en.wikipedia.org/w/api.php?ggscoord=\(userLocation.coordinate.latitude)%7C\(userLocation.coordinate.longitude)&action=query&prop=coordinates%7Cpageimages%7Cpageterms&colimit=50&piprop=thumbnail&pithumbsize=500&pilimit=50&wbptterms=description&generator=geosearch&ggsradius=10000&ggslimit=50&format=json"
        guard let url = URL(string: urlString) else { return }

        if let data = try? Data(contentsOf: url) {
            sightsJSON = JSON(data)
            locationManager.startUpdatingHeading()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        DispatchQueue.main.async {
            self.headingCount += 1
            if self.headingCount != 2 { return }

            self.userHeading = newHeading.magneticHeading
            self.locationManager.stopUpdatingHeading()
            self.createSights()
        }
    }

    func createSights() {
        for page in sightsJSON["query"]["pages"].dictionaryValue.values {
            let locationLat = page["coordinates"][0]["lat"].doubleValue
            let locationLon = page["coordinates"][0]["lon"].doubleValue
            let location = CLLocation(latitude: locationLat, longitude: locationLon)

            let distance = Float(userLocation.distance(from: location))

            let azimuthFromUser = direction(from: userLocation, to: location)
            let angle = azimuthFromUser - userHeading

            let angleRadians = deg2rad(angle)
            let rotationHorizontal = SCNMatrix4ToMat4(SCNMatrix4MakeRotation(Float(angleRadians), 1, 0, 0))
            let rotationVertical = SCNMatrix4ToMat4(SCNMatrix4MakeRotation(-0.2 + Float(distance / 600), 0, 1, 0))
            let rotation = simd_mul(rotationHorizontal, rotationVertical)

            guard let sceneView = self.view as? ARSKView else { return }
            guard let frame = sceneView.session.currentFrame else { return }
            let rotation2 = simd_mul(frame.camera.transform, rotation)

            var translation = matrix_identity_float4x4
            translation.columns.3.z = -(distance / 50)

            let transform = simd_mul(rotation2, translation)
            let anchor = ARAnchor(transform: transform)
            sceneView.session.add(anchor: anchor)

            pages[anchor.identifier] = page["title"].string ?? "Unknown"
        }
    }

    func deg2rad(_ degrees: Double) -> Double {
        return degrees * Double.pi / 180
    }

    func rad2deg(_ radians: Double) -> Double {
        return radians * 180 / Double.pi
    }

    func direction(from p1: CLLocation, to p2: CLLocation) -> Double {
        let lon_delta = p2.coordinate.longitude - p1.coordinate.longitude;
        let y = sin(lon_delta) * cos(p2.coordinate.longitude)
        let x = cos(p1.coordinate.latitude) * sin(p2.coordinate.latitude) - sin(p1.coordinate.latitude) * cos(p2.coordinate.latitude) * cos(lon_delta)
        let radians = atan2(y, x)
        return rad2deg(radians)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARSessionConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    

    // MARK: - ARSKViewDelegate
    
    func view(_ view: ARSKView, nodeFor anchor: ARAnchor) -> SKNode? {
        // Create and configure a node for the anchor added to the view's session.
        let labelNode = SKLabelNode(text: pages[anchor.identifier])
        labelNode.horizontalAlignmentMode = .center
        labelNode.verticalAlignmentMode = .center

        let size = labelNode.frame.size.applying(CGAffineTransform(scaleX: 1.1, y: 1.4))
        let backgroundNode = SKShapeNode(rectOf: size, cornerRadius: 10)
        backgroundNode.fillColor = UIColor(hue: CGFloat(GKRandomSource.sharedRandom().nextUniform()), saturation: 0.5, brightness: 0.4, alpha: 0.9)
        backgroundNode.strokeColor = backgroundNode.fillColor.withAlphaComponent(1)
        backgroundNode.lineWidth = 2
        backgroundNode.addChild(labelNode)
        return backgroundNode
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
