//
//  ImageCacheTest.swift
//  dd_ios_takehomeTests
//
//  Created by Don Clore on 2/11/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import XCTest
@testable import dd_ios_takehome

class ImageCacheTest: XCTestCase {
  let docDir :String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
  var copiedUrls : [URL] = []
  
  override func setUp() {
    super.setUp()
  
    do {
      var url : URL = try copyBundleFileToDocumentsDir(name: "California-Pizza-Kitchen",
        withExtension: "png")
      copiedUrls.append(url)
      
      url = try copyBundleFileToDocumentsDir(name: "Fang", withExtension: "png")
      copiedUrls.append(url)

      url = try copyBundleFileToDocumentsDir(name: "Ikes-Love--Sandwiches-NEW", withExtension: "png")
      copiedUrls.append(url)

      url = try copyBundleFileToDocumentsDir(name: "jack-logo-viraj-update", withExtension: "png")
      copiedUrls.append(url)

      url = try copyBundleFileToDocumentsDir(name: "KingOfThaiNoodleHouse", withExtension: "png")
      copiedUrls.append(url)

      url = try copyBundleFileToDocumentsDir(name: "LahoreKarahi", withExtension: "png")
      copiedUrls.append(url)

      url = try copyBundleFileToDocumentsDir(name: "OSHA-Thai-Restaurant-NEW", withExtension: "png")
      copiedUrls.append(url)

      url = try copyBundleFileToDocumentsDir(name: "Thai-Spice-Restaurant", withExtension: "png")
      copiedUrls.append(url)

      url = try copyBundleFileToDocumentsDir(name: "The-Halal-Guys-RESIZED_2", withExtension: "png")
      copiedUrls.append(url)
      
    } catch  {
      XCTFail("error setting up tests")
    }
  }
    
  override func tearDown() {
    super.tearDown()
    let fm = FileManager.default
    do {
      for url in copiedUrls {
        let path = url.path
        if fm.fileExists(atPath: path) {
          try fm.removeItem(at: url)
        }
      }
    } catch {
      XCTFail("error cleaning up.")
    }
  }
    
  func testCaching() {
    for url in copiedUrls {
      ExploreVC.downloadImage(url: url, completion: { image, error, hitWire in
        if let _ = error {
          XCTFail("Failed to find image")
        }
        guard let _ = image else {
          XCTFail("Attempt to get image \(url.path) returned nil")
          return
        }
        XCTAssertTrue(hitWire, "Didn't have to hit wire")
      })
    }
    
    for url in copiedUrls {
      ExploreVC.downloadImage(url: url, completion: { image, error, hitWire in
        if let _ = error {
          XCTFail("Failed to find image")
        }
        guard let _ = image else {
          XCTFail("Attempt to get image \(url.path) returned nil")
          return
        }
        XCTAssertFalse(hitWire, "Incorrectly had to hit wire")
      })
    }
  }
  
  fileprivate func copyBundleFileToDocumentsDir(name : String, withExtension ext : String) throws -> URL {
    let bundle = Bundle(for: ImageCacheTest.self)
    
    guard let url = bundle.url(forResource: name, withExtension: ext) else {
      XCTFail("Couldn't find url for \(name).\(ext)")
      throw TestError()
    }
    let fm = FileManager.default

    let pathString = docDir + "\(name).\(ext)"
    guard let pathURL = URL(string: "file://\(pathString)") else {
      XCTFail("Error forming pathURL for \(pathString)")
      throw TestError()
    }
    
    if false == fm.fileExists(atPath: pathString) {
      try fm.copyItem(at: url, to: pathURL)
    }
    return pathURL
  }
}
