//
//  JSONCodingTests.swift
//  dd_ios_takehomeTests
//
//  Created by Don Clore on 2/11/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import XCTest
@testable import dd_ios_takehome

struct TestError : Error {
  
}

class JSONCodingTests: XCTestCase {
  var serverResp : Data!
  var equalStores : Data!
  var unequalStores : Data!
  var storesWithNilValues : Data!
  var storesWithMissingValues : Data!
  
  var expectedStoresList : [Store] = [
    
    Store(id: 3835, name: "California Pizza Kitchen (San Francisco)",
      description: "California-Style Cuisine", deliveryFee: 0,
      coverImgURL: URL(string:"https://cdn.doordash.com/media/restaurant/cover/California-Pizza-Kitchen.png"),
      asapTime: 27),
    
    Store(id: 77069, name: "The Halal Guys", description: "", deliveryFee: 0,
      coverImgURL: URL(string:"https://cdn.doordash.com/media/restaurant/cover/The-Halal-Guys-RESIZED_2.png"),
      asapTime: 27),
    
    Store(id: 4546, name: "Grubstake",
      description: "American (Traditional), Diners, Burgers, Breakfast & Brunch, Desserts, Barbecue, Vegetarian, Chicken, Fast Food",
      deliveryFee: 0,
      coverImgURL: URL(string: "https://cdn.doordash.com/media/restaurant/cover/grubstake.jpg"),
      asapTime: 35),
    
    Store(id: 96481, name: "Ike's Love and Sandwiches (Polk)",
      description:  "Sandwiches, Vegetarian, Gluten-Free",
      deliveryFee: 0,
      coverImgURL: URL(string:"https://cdn.doordash.com/media/restaurant/cover/Ikes-Love--Sandwiches-NEW.png"),
      asapTime: 36),
    
    Store(id: 33068, name: "King Of Thai Noodle House (O'Farrell St)", description: "Thai",
      deliveryFee: 0,
      coverImgURL: URL(string:"https://cdn.doordash.com/media/restaurant/cover/KingOfThaiNoodleHouse.png"),
      asapTime: 29),
    
    Store(id: 4661, name: "Osha (Geary Street)", description: "Authentic Thai Food",
      deliveryFee: 0,
      coverImgURL: URL(string: "https://cdn.doordash.com/media/restaurant/cover/OSHA-Thai-Restaurant-NEW.png"),
      asapTime: 29),
    
    Store(id: 32378, name: "Lahore Karahi", description: "Indian, Pakistani", deliveryFee: 0,
      coverImgURL: URL(string: "https://cdn.doordash.com/media/restaurant/cover/LahoreKarahi.png"),
      asapTime: 31),
    
    Store(id: 4621, name: "Thai Spice Restaurant (San Francisco)", description: "Thai, Catering",
      deliveryFee: 0,
      coverImgURL: URL(string: "https://cdn.doordash.com/media/restaurant/cover/Thai-Spice-Restaurant.png"),
      asapTime: 33),
    
    Store(id: 52832, name: "Jack in the Box (400 Geary St.)",
      description: "Fast Food, Burgers, Late Night", deliveryFee: 0,
      coverImgURL: URL(string: "https://cdn.doordash.com/media/restaurant/cover/jack-logo-viraj-update.png"),
      asapTime: 28),
    
    Store(id: 33059, name: "Fang", description: "Chinese", deliveryFee: 0,
      coverImgURL: URL(string: "https://cdn.doordash.com/media/restaurant/cover/Fang.png"),
      asapTime: 26),
  ]
  
  override func setUp() {
    super.setUp()
    do {
      serverResp = try loadJSON(name: "serverResp", withExtension: "json")
      equalStores = try loadJSON(name: "equalStores", withExtension: "json")
      unequalStores = try loadJSON(name: "unequalStores", withExtension: "json")
      storesWithNilValues = try loadJSON(name: "storesWithNilValues", withExtension: "json")
      storesWithMissingValues = try loadJSON(name: "storesWithMissingValues", withExtension: "json")
    } catch {
      XCTFail("Failed to load JSON")
    }
  }
    
  override func tearDown() {
    super.tearDown()
  }
    
  func testGeneralCoding() {
    [Store].jsonDecode(serverResp, success: { stores in
      let actual = Set<Store>(stores)
      let expected = Set<Store>(expectedStoresList)
      XCTAssertEqual(actual, expected)
      
    }, failure: {err in
      XCTFail("failed to deserialize stores array err = \(err)")
    })
  }
  
  func testEquality() {
    [Store].jsonDecode(equalStores, success: { twoStores in
      XCTAssert(twoStores.count == 2)
      let store1 = twoStores[0]
      let store2 = twoStores[1]
      
      XCTAssertEqual(store1, store2)
      
      var storeNilId = store1
      storeNilId.id = nil
      
      // Testing to see if we can still recognize store when id is nil
      XCTAssertEqual(storeNilId, store2)
      
    }, failure: { err in
      XCTFail("failed to deserialize stores array err = \(err)")
    })
  }
  
  func testInequality() {
    [Store].jsonDecode(unequalStores, success: { twoStores in
      XCTAssert(twoStores.count == 2)
      let store1 = twoStores[0]
      let store2 = twoStores[1]
      
      XCTAssertNotEqual(store1, store2)
    }, failure: { err in
      XCTFail("failed to deserialize stores array err = \(err)")
    })
  }
  
  func testForCorrectDeserializationInThePresenceOfNullValues() {
    nilOrMissing(data: storesWithNilValues)
  }
  
  func testForCorrectDeserializationInTheAbsenceOfSomeValues() {
    nilOrMissing(data: storesWithMissingValues)
  }
  
  fileprivate func nilOrMissing(data : Data) {
    [Store].jsonDecode(data, success: { twoStores in
      XCTAssert(twoStores.count == 2)
      let store1 = twoStores[0]
      let store2 = twoStores[1]
      
      XCTAssertNotEqual(store1, store2)
      
      XCTAssertNil(store1.id)
      XCTAssertNil(store2.name)
      
    }, failure: { err in
      XCTFail("failed to deserialize stores array err = \(err)")
    })
  }
  
    
  fileprivate func loadJSON(name : String, withExtension ext : String) throws  -> Data {
    let bundle = Bundle(for: JSONCodingTests.self)
    
    guard let url = bundle.url(forResource: name, withExtension: ext) else {
      XCTFail("Couldn't find url for \(name).\(ext)")
      throw TestError()
    }
    
    var data : Data?
    do {
      data = try Data(contentsOf: url, options: [])
    } catch {
      XCTFail("Couldn't load JSON test resource")
      throw TestError()
    }
    return data!
  }
}

extension Store : Hashable {
  public var hashValue: Int {
    if let id = id {
      return id
    }
    if let name = name, let description = description {
      return name.hashValue + description.hashValue
    }
    fatalError("This Store is un-hashable - something wrong with test data")
  }
  
  public static func ==(lhs: Store, rhs: Store) -> Bool {
    if let lhId = lhs.id, let rhId = rhs.id {
      return lhId == rhId
    }
    if lhs.name != rhs.name {
      return false
    }
    if lhs.description != rhs.description {
      return false
    }
    
    // heuristic - identical name AND description, but missing an id, call it equal.
    // This is for the purposes of this test file, guys.  If we needed Hashable in the actual
    // app, this code would have to go. 
    return true
  }
  
  
}
