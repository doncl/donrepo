//
//  AppDelegate.swift
//  dd_ios_takehome
//
//  Created by Don Clore on 2/9/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var locationManager: CLLocationManager!

  func application(_ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
    locationManager = CLLocationManager()
    locationManager.requestWhenInUseAuthorization()
    return true
  }
}

