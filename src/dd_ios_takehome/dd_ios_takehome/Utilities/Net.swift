//
//  Net.swift
//  dd_ios_takehome
//
//  Created by Don Clore on 02/10/18
//  Copyright © 2017 Don Clore. All rights reserved.
//

import Foundation

struct Net {
  // I was a bit surprised that the homework assignment suggested using AFNetworking.
  // One of the things Mikey Ward (instructor at Big Nerd Ranch Advanced iOS
  // Developer Bootcamp) told all of us was that developers are too quick to reach for
  // a library to do something that's handled quite adequately by Apple's classes.
  // The single biggest thing that slows down load time is the use of too many dylibs.
  // Apple recommends no more than 6 external dylib dependencies.   I don't see how
  // AFNetworking or AlamoFire offer enough over plain old NSURLSession to be worth that
  // cost.  It really isn't hard, or tricky, NSURLSession was designed to be easy to use. 
  static func getNetworkData(uri : String, success: @escaping (Data) -> (),
    failure : @escaping (String) -> ()){
    
    let session = URLSession(configuration: .default)
    let headers : [String : String] = [
      "Accept" : "application/json",
    ]
    
    guard let url : URL = URL(string: uri) else {
      return
    }
    let request = NSMutableURLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
      timeoutInterval: 3000)
    
    request.httpMethod = "GET"
    for header in headers {
      request.addValue(header.1, forHTTPHeaderField: header.0)
    }
    
    let task = session.dataTask(with: request as URLRequest) {
      (data, response, error) -> () in
      
      if let error = error {
        failure(error.localizedDescription)
        return
      }
      if let httpResp = response as? HTTPURLResponse {
        if httpResp.statusCode != 200 {
          failure("Error making request \(uri) code = \(httpResp.statusCode)")
          return
        }
      }
      guard let data = data else {
        failure("Data is nil coming back from request \(uri)")
        return
      }
      success(data)
    }
    task.resume()
  }
}

