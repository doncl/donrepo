//
//  JSONCoding.swift
//  dd_ios_takehome
//
//  Created by Don Clore on 2/10/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation


/// Just add some helper stuff to Codable, so that classes can be dead-simple.  They just
/// declare conformance to Codable and JSONCoding, and define their codableType as being
/// themselves, and then they get full-featured jsonDecode and jsonEncode methods.
protocol JSONCoding {
  associatedtype codableType : Codable
  
  static func jsonEncode(_ t : codableType, success: (String) -> (), failure: (String) -> ())
  static func jsonDecode(_ data: Data, success: (codableType) -> (), failure: (String) -> ())
}

// A little POP (Protocol Oriented Programming) makes using Codable pretty straightforward.
// Yes, I know the community has pushed Swift into an exception throwing model rather than
// NSError explicit handling.  I don't have a lot of religion about this, but I chose to
// map the exceptions to string errors here.
//
// In my defense, I reference Raymond Chen's blog post (one of the best programmers who has
// ever lived; no matter what you might think of Microsoft; Raymond Chen is pretty amazing, and
// can divide by zero, and whenever he says something, it's worth at least thinking about it.)
// https://blogs.msdn.microsoft.com/oldnewthing/20050114-00/?p=36693
extension JSONCoding {
  static func jsonDecode(_ data: Data, success: (codableType) -> (), failure: (String) -> ()) {
    do {
      let decoder = JSONDecoder()
      decoder.dateDecodingStrategy = .custom({ (decoder : Decoder) -> Date in
        let container = try decoder.singleValueContainer()
        let dateStr = try container.decode(String.self)
        guard let d = DateFormatter.tryAllCustomDateFormats(dateString: dateStr) else {
          throw DecodingError.dataCorruptedError(in: container,
                                                 debugDescription: "\(dateStr) not parseable as date")
        }
        return d
      })
      let t = try decoder.decode(codableType.self, from: data)
      success(t)
      return
    } catch DecodingError.dataCorrupted(let context) {
      failure("Data corrupted, context = \(context)")
      return
    } catch DecodingError.keyNotFound(let codingKey, let context) {
      failure("\(codingKey) missing, context = \(context)")
      return
    } catch DecodingError.typeMismatch(let type, let context) {
      failure("type mismatch, type = \(type), context = \(context)")
      return
    } catch DecodingError.valueNotFound(let type, let context) {
      failure("type mismatch type = \(type), context = \(context)")
      return
    } catch {
      failure("unknown error")
      return
    }
  }
  
  static func jsonEncode(_ t : codableType, success: (String) -> (), failure: (String) -> ()) {
    let encoder = JSONEncoder()
    encoder.outputFormatting = [.prettyPrinted, .sortedKeys]
    encoder.dateEncodingStrategy = .iso8601
    guard let data = try? encoder.encode(t) else {
      failure("Couldn't encode object \(t)")
      return
    }
    guard let jsonString = String(data: data, encoding: .utf8) else {
      failure("Couldn't generate json string from Data")
      return
    }
    success(jsonString)
  }
}

/// These are not necessary for this project - this is just my practice for Codable stuff;
/// cram lots 'o date formats in here, and try them all before giving up.  
extension DateFormatter {
  static let iso8601Full: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
  }()
  
  static let iso8601Shorter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
  }()
  
  static let iso8601Shortest: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
  }()
  
  // 0001-01-01T00:00:00
  static let emptyDate : DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
  }()
  
  private static var formats : [DateFormatter] = [iso8601Full, iso8601Shorter, iso8601Shortest,
                                                  emptyDate]
  
  static func tryAllCustomDateFormats(dateString: String) -> Date? {
    for format in formats {
      if let d = format.date(from: dateString) {
        return d
      }
    }
    return nil
  }
}

extension Array : JSONCoding {
  typealias codableType = Array
}
