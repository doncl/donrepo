//
//  Store.swift
//  dd_ios_takehome
//
//  Created by Don Clore on 2/10/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

struct Store : Codable, JSONCoding {
  typealias codableType = Store
  
  enum CodingKeys : String, CodingKey {
    case id
    case name
    case description
    case deliveryFee = "delivery_fee"
    case coverImgURL = "cover_img_url"
    case asapTime = "asap_time"
  }
  
  // With Codable, I'm sort of in the camp of 'trust no one'.  I don't trust the server enough
  // to make anything non-optional, because it gets painful at deserialization time if you do;
  // I'd rather eat the cost of unwrapping optionals repeatedly in code (90% of code is error
  // handling, right?).   
  var id : Int?
  var name : String?
  var description : String?
  var deliveryFee : Decimal?
  var coverImgURL : URL?
  var asapTime : Int?
}
