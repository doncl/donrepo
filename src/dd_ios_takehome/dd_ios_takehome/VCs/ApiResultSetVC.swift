//
//  ApiResultSetVC.swift
//  dd_ios_takehome
//
//  Created by Don Clore on 2/10/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit
import CoreLocation

enum ImageError : Error {
  case datacorrupt
}

class ExploreVC: UIViewController {
  let pseudoNav : UIView = UIView()
  let noResults : NoResultsCell = NoResultsCell()
  let formatter = NumberFormatter()
  let spinner : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
  let cellHeight : CGFloat = 100.0
  let cellId : String = "StoreCellId"
  let horzPad : CGFloat = 15.0
  let pseudoNavBottomBorderY : CGFloat = 70.0
  let pseudoNavHeight : CGFloat = 55.0
  let navAddressSide : CGFloat = 25.0
  let borderWidth : CGFloat = 1.0
  let borderColor : UIColor = #colorLiteral(red: 0.7215686275, green: 0.7215686275, blue: 0.7215686275, alpha: 1)
  var showNoResults : Bool = false
  
  var stores : [Store] = []
  static let imageCache = NSCache<NSString, UIImage>()

  let pseudoNavBottom : CALayer = CALayer()
  let location : CLLocation
  let table : UITableView = UITableView(frame: CGRect.zero, style: .plain)
  let doorDash : UILabel = UILabel()
  let navAddressImage : UIImageView = UIImageView(image: #imageLiteral(resourceName: "nav-address"))
  
  init(location : CLLocation) {
    self.location = location
    super.init(nibName: nil, bundle: nil)
    view.backgroundColor = .white
    table.delegate = self
    table.dataSource = self
    table.separatorStyle = .none
    table.register(StoreCell.self, forCellReuseIdentifier: cellId)
    createView()

    formatter.locale = Locale.current
    formatter.numberStyle = .currency
    
    spinner.center = view.center
    spinner.startAnimating()
    DoorDashApiManager.getStoreObjects(lat: location.coordinate.latitude,
      long: location.coordinate.longitude, success: { stores in
        
      self.stores = stores
      self.showNoResults = self.stores.count == 0
      self.table.reloadData()
      self.spinner.stopAnimating()
    }, failure: { _ in
      self.showNoResults = true
      self.spinner.stopAnimating()
      self.simpleError("Network error")
    })
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("IB is a wonderful thing, but not today...")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

//MARK: Layout
extension ExploreVC {
  fileprivate func createView() {
    pseudoNav.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(pseudoNav)
    
    doorDash.translatesAutoresizingMaskIntoConstraints = false
    pseudoNav.addSubview(doorDash)
    doorDash.text = "DoorDash"
    doorDash.textColor = UIColor.red
    doorDash.textAlignment = .center
    doorDash.font = UIFont.boldSystemFont(ofSize: 22.0)
    doorDash.sizeToFit()
    
    navAddressImage.translatesAutoresizingMaskIntoConstraints = false
    pseudoNav.addSubview(navAddressImage)
    
    table.translatesAutoresizingMaskIntoConstraints = false
    table.backgroundColor = .white
    view.addSubview(table)
    
    NSLayoutConstraint.activate ([
      pseudoNav.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      pseudoNav.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      pseudoNav.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      pseudoNav.heightAnchor.constraint(equalToConstant: pseudoNavHeight),
      doorDash.centerXAnchor.constraint(equalTo: pseudoNav.centerXAnchor),
      doorDash.centerYAnchor.constraint(equalTo: pseudoNav.centerYAnchor),
      navAddressImage.widthAnchor.constraint(equalToConstant: navAddressSide),
      navAddressImage.heightAnchor.constraint(equalToConstant: navAddressSide),
      navAddressImage.centerYAnchor.constraint(equalTo: doorDash.centerYAnchor),
      navAddressImage.leadingAnchor.constraint(equalTo: pseudoNav.leadingAnchor,
        constant: horzPad),
      
      table.topAnchor.constraint(equalTo: pseudoNav.bottomAnchor),
      table.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      table.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      table.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
    
    let ddTap = UITapGestureRecognizer(target: self,
      action: #selector(ExploreVC.doorDashTapped(_:)))
    doorDash.isUserInteractionEnabled = true
    doorDash.addGestureRecognizer(ddTap)
    
    let naiTap = UITapGestureRecognizer(target: self,
      action: #selector(ExploreVC.navAddressImageTapped(_:)))
    navAddressImage.isUserInteractionEnabled = true
    navAddressImage.addGestureRecognizer(naiTap)
    
    view.addSubview(spinner)
    view.bringSubview(toFront: spinner)
  }
  
  @objc func doorDashTapped(_ sender: UITapGestureRecognizer) {
    dismiss(animated: true, completion: nil)
  }
  
  @objc func navAddressImageTapped(_ sender: UITapGestureRecognizer) {
    dismiss(animated: true, completion: nil)
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    let x : CGFloat = table.frame.origin.x
    let y = pseudoNav.frame.origin.y + pseudoNav.frame.height - borderWidth
    let width = table.frame.width
    let height = borderWidth
    pseudoNavBottom.frame = CGRect(x: x, y: y, width: width, height: height)
    pseudoNavBottom.backgroundColor = borderColor.cgColor
    pseudoNavBottom.masksToBounds = false
    if nil == pseudoNavBottom.superlayer {
      view.layer.addSublayer(pseudoNavBottom)
    }
  }
}

extension ExploreVC : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return showNoResults ? 1 : stores.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if showNoResults {
      return noResults
    }
    let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! StoreCell
    let store = stores[indexPath.item]
    if let url = store.coverImgURL {
      ExploreVC.downloadImage(url: url, completion: { image, error in
        if let image = image {
          cell.storeImage.image = image
        }
      })
    }
 
    if let name = store.name {
      cell.storeName.text = name
    }
    if let description = store.description {
      cell.storeDescription.text = description
    }
    if let deliveryFee = store.deliveryFee  {
      if deliveryFee <= 0 {
        cell.deliveryCost.text = "Free Delivery"
      } else {
        let adjustedDeliveryFee = deliveryFee / 100
        let ns = adjustedDeliveryFee as NSNumber
        if let currency = formatter.string(from: ns) {
          cell.deliveryCost.text = "\(currency) delivery"
        } else {
          // If this can't be made into a number, all bets are off.
          cell.deliveryCost.text = ""
        }
      }
    } else {
      cell.deliveryCost.text = "Free Delivery"
    }
    if let deliveryEta = store.asapTime {
      cell.deliveryETA.text = "\(deliveryEta) min"
    }
    
    return cell
  }
}

extension ExploreVC : UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return cellHeight
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    dismiss(animated: true, completion: nil)
  }
}

//MARK: crude image caching.
extension ExploreVC {
  static func downloadImage(url: URL,
    completion: @escaping (_ image: UIImage?, _ error: Error? ) -> Void) {
    
    if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
      completion(cachedImage, nil)
    } else {
      do {
        let data = try Data(contentsOf: url)
        guard let image = UIImage(data: data) else {
          completion(nil, ImageError.datacorrupt)
          return
        }
        imageCache.setObject(image, forKey: url.absoluteString as NSString)
        completion(image, nil)
      } catch let error {
        completion(nil, error)
      }
    }
  }
}
