//
//  NoResultsCell.swift
//  dd_ios_takehome
//
//  Created by Don Clore on 2/10/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit


/// Wasn't spec'ed in the comp - seemed like we needed something for this - this is pretty
/// much the irreducible minimum.  
class NoResultsCell: UITableViewCell {
  init() {
    super.init(style: .default, reuseIdentifier: "No ID Needed")
    if let label = textLabel {
       label.text = "No Results Found"
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
