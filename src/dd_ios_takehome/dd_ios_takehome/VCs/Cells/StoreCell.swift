//
//  StoreCell.swift
//  dd_ios_takehome
//
//  Created by Don Clore on 2/10/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit


/// An Auto-layout based table cell.  I could have done a custom cell using a nib, which I'm
/// comfortable doing, or even just a prototype cell (if this View Controller had been done
/// with Interface Builder), using tags for all the subviews, so you could set values on
/// things.  In that scenario, you'd just do the constraints in IB.  This way is more
/// explicit and imperative and all that - like I said, I'm not averse to any reasonable
/// approach.
class StoreCell: UITableViewCell {
  let bottomBorder : CALayer = CALayer()
  let borderWidth : CGFloat = 0.5
  let boldFontName = "Avenir-Bold"
  let fontName = "Avenir"
  let horzPad : CGFloat = 15.0
  let imageSize : CGSize = CGSize(width: 140.0, height: 70.0)
  let storeImage : UIImageView = UIImageView()
  let storeName : UILabel = UILabel()
  let storeDescription : UILabel = UILabel()
  let deliveryCost : UILabel = UILabel()
  let deliveryETA : UILabel = UILabel()
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    bottomBorder.backgroundColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1).cgColor
    createCellView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // Draw the bottom border as shown in the comp, using Core Animation.
  override func layoutSubviews() {
    super.layoutSubviews()
    let x = contentView.frame.origin.x + horzPad
    let y = frame.height - borderWidth
    let height = borderWidth
    let width = frame.width - x
    let r = CGRect(x: x, y: y, width: width, height: height)
    bottomBorder.frame = r
    bottomBorder.masksToBounds = false
    if nil == bottomBorder.superlayer {
      layer.addSublayer(bottomBorder)
    }
  }
}

extension StoreCell {
  
  /// Set up view hierarchy, and auto-layout constraints for all the subviews.  At my current
  /// position, we use SnapKit for all the AutoLayout stuff, but as of iOS9, it's gotten so
  /// easy to layout stuff in code, it hardly seems necessary to take on a library for that.
  /// My guess is....with SnapKit, you end up writing about 70% of the code compared to the
  /// bare-bones approach I'm using here, and you end up taking on a pretty significant
  /// dependency, one I've ported to Swift 3, and Swift 4, and Swift 4.1, because the SnapKit
  /// guys never update their code as quickly as I want to update my phone to the newest
  /// security patches.   Open Source code dependencies can be a double-edged sword, both
  /// because they have a maintenance cost (especially Swift libraries - Obj C less so), and
  /// more importantly because if they're a dylib, you're paying for it at load time.  Apple
  /// was very explicit at WWDC16 - if you want fast, responsive load times, use fewer dylibs.
  fileprivate func createCellView() {
    // It is nice in SnapKit to not have to set TAMIC to false on everything - it does it for
    // you.  
    storeImage.translatesAutoresizingMaskIntoConstraints = false
    storeName.translatesAutoresizingMaskIntoConstraints = false
    storeDescription.translatesAutoresizingMaskIntoConstraints = false
    deliveryCost.translatesAutoresizingMaskIntoConstraints = false
    deliveryETA.translatesAutoresizingMaskIntoConstraints = false
    contentView.addSubview(storeImage)
    contentView.addSubview(storeName)
    contentView.addSubview(storeDescription)
    contentView.addSubview(deliveryCost)
    contentView.addSubview(deliveryETA)
    
    storeImage.contentMode = .scaleAspectFit
    
    storeName.font = UIFont.boldSystemFont(ofSize: 19.0)
    storeName.textColor = .black
    storeName.numberOfLines = 1
    storeName.lineBreakMode = .byTruncatingTail
    
    storeDescription.font = UIFont.systemFont(ofSize: 15.0)
    storeDescription.textColor = #colorLiteral(red: 0.4666666667, green: 0.4666666667, blue: 0.4666666667, alpha: 1)
    storeDescription.numberOfLines = 1
    storeDescription.lineBreakMode = .byTruncatingTail
    // the deliveryCost label needs a little help to layout properly
    deliveryCost.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)
    deliveryCost.font = UIFont.systemFont(ofSize: 15.0)
    deliveryCost.textColor = #colorLiteral(red: 0.3176470588, green: 0.3176470588, blue: 0.3176470588, alpha: 1)
    deliveryCost.numberOfLines = 1
    deliveryCost.lineBreakMode = .byTruncatingTail
    
    deliveryETA.font = UIFont.systemFont(ofSize: 15.0)
    deliveryETA.textColor = #colorLiteral(red: 0.3176470588, green: 0.3176470588, blue: 0.3176470588, alpha: 1)
    
    NSLayoutConstraint.activate([
      storeImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: horzPad),
      storeImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
      storeImage.widthAnchor.constraint(equalToConstant: imageSize.width),
      storeImage.heightAnchor.constraint(equalToConstant: imageSize.height),
      
      storeName.topAnchor.constraint(equalTo: storeImage.topAnchor),
      storeName.leadingAnchor.constraint(equalTo: storeImage.trailingAnchor, constant: horzPad),
      storeName.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -horzPad),
      
      storeDescription.leadingAnchor.constraint(equalTo: storeName.leadingAnchor),
      storeDescription.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
      storeDescription.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -horzPad),
      
      deliveryCost.leadingAnchor.constraint(equalTo: storeDescription.leadingAnchor),
      deliveryCost.bottomAnchor.constraint(equalTo: storeImage.bottomAnchor),
      deliveryCost.trailingAnchor.constraint(lessThanOrEqualTo: deliveryETA.leadingAnchor, constant: -horzPad),
      
      deliveryETA.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,
        constant: -horzPad),
      
      deliveryETA.topAnchor.constraint(equalTo: deliveryCost.topAnchor),
    ])
  }
}
