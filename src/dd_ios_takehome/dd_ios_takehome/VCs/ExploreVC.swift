//
//  ApiResultSetVC.swift
//  dd_ios_takehome
//
//  Created by Don Clore on 2/10/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit
import CoreLocation

enum ImageError : Error {
  case datacorrupt
}

/// Fairly basic ViewController with a faux navbar and UITableView.   The assignment didn't
/// specify the transition style - I wasn't sure if the navbar-like thing in the comp should
/// be a real UINavBar and use a full-on UInavigationController, but it would have required
/// a custom backbutton - after some head-scratching, I decided you were wanting a modal
/// transition.  I considered doing a custom transition animation, but decided it would just
/// look like I'm full of myself :).
///
/// Oh, Interface Builder.   So....I work with a developer who is *completely* over IB, and
/// she probably would quit rather than be forced to do a storyboard again, after our last
/// company.  I have some ambivalence about IB, but not like that; I'm glad to use it when
/// doing prototyping, or do wrestle some problem out quickly, or when someone who is paying
/// me says to use it :).   I'm really OK either way; my biggest reluctance is that pull
/// requests / code reviews are really a bear using IB - so much of the logic is declarative.
/// Diffs are really hard to judge - I pretty much have to load up the project myself, and
/// eyeball the storyboard in question, and figure out what is going on.  Imperative code is
/// actually much easier to do a diligent code review for.
///
/// So....the first VC in this project was done with IB, just to show I don't shy away from it.
/// Subsequent VC's were done by rolling everything directly in code, to show that I'm quite
/// comfortable laying out UI and doing AutoLayout (or other layout paradigms, like
/// AsyncDisplayKit) 'by hand'.
///
/// I will say, that as of iOS 9, with Storyboard references, it is easy to 'modularize'
/// your layouts and have multiple storyboards, to multiple developers on a team can work on
/// them without stepping on each other too much, which mitigates one of my biggest gripes
/// about Storyboards; that, by their nature, they encouraged devs to do huge monolothic
/// intractable Storyboards.   It's always been possible to break Storyboards into multiple
/// pieces, but with Storyboard references, it became easy and sensible.   Still, there's a
/// liberating aspect to doing it all in code.
///
/// There's no Dynamic Text in any of this project - it wasn't asked for, and it makes layout
/// a lot more challenging.   I'm a huge proponent of this, however, and all
/// Accessibility technologies.   FWIW.
class ExploreVC: UIViewController {
  let pseudoNav : UIView = UIView()
  let noResults : NoResultsCell = NoResultsCell()
  let formatter = NumberFormatter()
  let spinner : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
  let cellHeight : CGFloat = 100.0
  let cellId : String = "StoreCellId"
  let horzPad : CGFloat = 15.0
  let pseudoNavHeight : CGFloat = 55.0
  let navAddressSide : CGFloat = 25.0
  let borderWidth : CGFloat = 1.0
  let borderColor : UIColor = #colorLiteral(red: 0.7215686275, green: 0.7215686275, blue: 0.7215686275, alpha: 1)
  var showNoResults : Bool = false
  
  var stores : [Store] = []
  static let imageCache = NSCache<NSString, UIImage>()

  let pseudoNavBottom : CALayer = CALayer()
  let location : CLLocation
  let table : UITableView = UITableView(frame: CGRect.zero, style: .plain)
  let doorDash : UILabel = UILabel()
  let navAddressImage : UIImageView = UIImageView(image: #imageLiteral(resourceName: "nav-address"))
  
  /// init
  ///
  /// - Parameter location: The geo location this ExploreVC is going to display results for.
  ///   One small advantage of not using IB is being able to have a custom initializer like
  ///   this, with a non-optional parameter, the code can be simpler without doing any
  ///   pernicious force unwrapping. 
  init(location : CLLocation) {
    self.location = location
    super.init(nibName: nil, bundle: nil)
    view.backgroundColor = .white
    table.delegate = self
    table.dataSource = self
    table.separatorStyle = .none
    table.rowHeight = cellHeight
    table.estimatedRowHeight = cellHeight
    table.register(StoreCell.self, forCellReuseIdentifier: cellId)
    createView()

    formatter.locale = Locale.current
    formatter.numberStyle = .currency
    
    spinner.center = view.center
    spinner.startAnimating()
    DoorDashApiManager.getStoreObjects(lat: location.coordinate.latitude,
      long: location.coordinate.longitude, success: { stores in
        
      self.stores = stores
      self.showNoResults = self.stores.count == 0
      self.table.reloadData()
      self.spinner.stopAnimating()
    }, failure: { _ in
      self.showNoResults = true
      self.spinner.stopAnimating()
      self.simpleError("Network error")
    })
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("IB is a wonderful thing, but not today...")
  }
}

//MARK: Layout
extension ExploreVC {
  fileprivate func createView() {
    pseudoNav.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(pseudoNav)
    
    doorDash.translatesAutoresizingMaskIntoConstraints = false
    pseudoNav.addSubview(doorDash)
    doorDash.text = "DoorDash"
    doorDash.textColor = UIColor.red
    doorDash.textAlignment = .center
    doorDash.font = UIFont.boldSystemFont(ofSize: 22.0)
    doorDash.sizeToFit()
    
    navAddressImage.translatesAutoresizingMaskIntoConstraints = false
    pseudoNav.addSubview(navAddressImage)
    
    table.translatesAutoresizingMaskIntoConstraints = false
    table.backgroundColor = .white
    view.addSubview(table)
    
    /// Sure, let's do a bunch 'o AutoLayout.
    NSLayoutConstraint.activate ([
      pseudoNav.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      pseudoNav.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      pseudoNav.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      pseudoNav.heightAnchor.constraint(equalToConstant: pseudoNavHeight),
      doorDash.centerXAnchor.constraint(equalTo: pseudoNav.centerXAnchor),
      doorDash.centerYAnchor.constraint(equalTo: pseudoNav.centerYAnchor),
      navAddressImage.widthAnchor.constraint(equalToConstant: navAddressSide),
      navAddressImage.heightAnchor.constraint(equalToConstant: navAddressSide),
      navAddressImage.centerYAnchor.constraint(equalTo: doorDash.centerYAnchor),
      navAddressImage.leadingAnchor.constraint(equalTo: pseudoNav.leadingAnchor,
        constant: horzPad),
      
      table.topAnchor.constraint(equalTo: pseudoNav.bottomAnchor),
      table.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      table.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      table.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
    
    // Obviously, this app is, by design and intent, incomplete.  So...when users are on the
    // Explore VC, give them a way to get back to the mappy guy.  If they tap on the graphic
    // or title in the 'nav' area, it dismisses, just as tapping on a tablecell does.
    let ddTap = UITapGestureRecognizer(target: self,
      action: #selector(ExploreVC.doorDashTapped(_:)))
    doorDash.isUserInteractionEnabled = true
    doorDash.addGestureRecognizer(ddTap)
    
    let naiTap = UITapGestureRecognizer(target: self,
      action: #selector(ExploreVC.navAddressImageTapped(_:)))
    navAddressImage.isUserInteractionEnabled = true
    navAddressImage.addGestureRecognizer(naiTap)
    
    view.addSubview(spinner)
    view.bringSubview(toFront: spinner)
  }
  
  @objc func doorDashTapped(_ sender: UITapGestureRecognizer) {
    dismiss(animated: true, completion: nil)
  }
  
  @objc func navAddressImageTapped(_ sender: UITapGestureRecognizer) {
    dismiss(animated: true, completion: nil)
  }
  
  /// Painting a grey line at the bottom of the pseudo-nav layer.  I usually use
  /// Core Animation for this kind of thing - I don't know that it's that much cheaper than
  /// using a UIView object, but for stuff that doesn't need to do UIResponder, that's just
  /// inert visual stuff, it seems like a safe bet to just use CALayer.  Also, this gets
  /// added into the visual layout without adding yet more equations for the Cassowary
  /// solver to grapple with.  Not that this particular layout has any kind of Auto-Layout
  /// perf problem - it's simple enough that it's kinda not an issue.  Still, this is generally
  /// the way I do this kind of thing, or if I need to do some kind of custom drawing, do a
  /// custom UIView class, and use a CAShapeLayer if possible, or draw on the context directly
  /// if that's not feasible for some reason.
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    let x : CGFloat = table.frame.origin.x
    let y = pseudoNav.frame.origin.y + pseudoNav.frame.height - borderWidth
    let width = table.frame.width
    let height = borderWidth
    pseudoNavBottom.frame = CGRect(x: x, y: y, width: width, height: height)
    pseudoNavBottom.backgroundColor = borderColor.cgColor
    pseudoNavBottom.masksToBounds = false
    if nil == pseudoNavBottom.superlayer {
      view.layer.addSublayer(pseudoNavBottom)
    }
  }
}

//MARK: UITableViewDataSource
extension ExploreVC : UITableViewDataSource {
  
  /// How many tablecells?   The only wrinkle here is if we had a network error, or there's no
  /// stores available for the lat/long pair chosen, we want to show a single cell that says
  /// 'no results available'.
  ///
  /// - Parameters:
  ///   - tableView: a table
  ///   - section: a section
  /// - Returns: number of cells.
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return showNoResults ? 1 : stores.count
  }
  
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if showNoResults {
      return noResults
    }
    let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! StoreCell
    let store = stores[indexPath.item]
    if let url = store.coverImgURL {
      ExploreVC.downloadImage(url: url, completion: { image, error, _ in
        if let image = image {
          cell.storeImage.image = image
        }
      })
    }
 
    if let name = store.name {
      cell.storeName.text = name
    }
    if let description = store.description {
      cell.storeDescription.text = description
    }
    if let deliveryFee = store.deliveryFee  {
      if deliveryFee <= 0 {
        cell.deliveryCost.text = "Free Delivery"
      } else {
        let adjustedDeliveryFee = deliveryFee / 100
        let ns = adjustedDeliveryFee as NSNumber
        if let currency = formatter.string(from: ns) {
          cell.deliveryCost.text = "\(currency) delivery"
        } else {
          // If this can't be made into a number, all bets are off.
          cell.deliveryCost.text = ""
        }
      }
    } else {
      cell.deliveryCost.text = "Free Delivery"
    }
    if let deliveryEta = store.asapTime {
      cell.deliveryETA.text = "\(deliveryEta) min"
    }
    
    return cell
  }
}

//MARK: UITableViewDelegate
extension ExploreVC : UITableViewDelegate {
  /// Not doing heightFprRowAtIndexPath - this design had a fixed height cell, and I just
  /// set it on the table in the initializer.
  /// I do know how to do self-sizing table cells (use auto-layout,
  ///return UITableViewAutomaticDimension for the cell height, and give a reasonable guess for
  /// estimated row height.  But this project didn't require it.
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    // Just go back to the map.
    dismiss(animated: true, completion: nil)
  }
}

//MARK: crude image caching.
extension ExploreVC {
  /// I've used several image caching libraries, and I'm not against doing so, but it seemed
  /// like overkill for this homework assignment.  NSCache works fine - it forgets everything
  /// when the app dies, but that's not entirely a bad thing.  Tastes great, less filling.
  static func downloadImage(url: URL,
    completion: @escaping (_ image: UIImage?, _ error: Error?, _ hitWire : Bool ) -> Void) {
    
    if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
      completion(cachedImage, nil, false)
    } else {
      do {
        let data = try Data(contentsOf: url)
        guard let image = UIImage(data: data) else {
          completion(nil, ImageError.datacorrupt, true)
          return
        }
        imageCache.setObject(image, forKey: url.absoluteString as NSString)
        completion(image, nil, true)
      } catch let error {
        completion(nil, error, true)
      }
    }
  }
}
