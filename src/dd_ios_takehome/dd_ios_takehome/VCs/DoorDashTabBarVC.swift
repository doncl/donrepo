//
//  DoorDashTabBarVC.swift
//  dd_ios_takehome
//
//  Created by Don Clore on 2/10/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit
import CoreLocation


/// A simple tab bar controller with two view controllers, one of them disabled.
/// My guess is that you guys just wanted to see if candidates are comfortable using the
/// the UITabBarController - or maybe just because it's in the real production app.
class DoorDashTabBarVC: UITabBarController {
  let location : CLLocation
  let apiResultSetVC : ExploreVC
  let favoritesVC : FavoritesVC
  
  let exploreTag : Int = 0
  let favoritesTag : Int = 1
  
  init(location: CLLocation) {
    self.location = location
    apiResultSetVC = ExploreVC(location: location)
    favoritesVC = FavoritesVC()
    super.init(nibName: nil, bundle: nil)
    setupTabBar()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    tabBar.tintColor = UIColor.red
    if let barItems = tabBar.items, barItems.count > 1 {
      let tabBarItem = barItems[1]
      tabBarItem.isEnabled = false
    }
  }
  
  private func setupTabBar() {
    let vcs = [apiResultSetVC, favoritesVC]
    // These are template images in the asset catalog, and I set the tint color so it gets
    // picked up.
    apiResultSetVC.tabBarItem = UITabBarItem(title: "Explore", image: #imageLiteral(resourceName: "tab-explore"), tag: exploreTag)
    favoritesVC.tabBarItem = UITabBarItem(title: "Favorites", image: #imageLiteral(resourceName: "tab-star"), tag: favoritesTag)
    
    viewControllers = vcs
  }
}
