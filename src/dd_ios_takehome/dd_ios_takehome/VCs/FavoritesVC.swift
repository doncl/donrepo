//
//  FaoritesVC.swift
//  dd_ios_takehome
//
//  Created by Don Clore on 2/10/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

// Doesn't do anything, just to populate the viewControllers property of
// the UITabBarController
class FavoritesVC: UIViewController {
  init() {
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
