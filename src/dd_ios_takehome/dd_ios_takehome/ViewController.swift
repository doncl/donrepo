//
//  ViewController.swift
//  dd_ios_takehome
//
//  Created by Don Clore on 2/9/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit
import MapKit

/// Principal VC of the app.  I'm sure there are many ways to accomplish what you guys had in
/// your comp, but I chose to use a UITextField with user input disabled for the 'Add new
/// Location' field, rather than a UILabel - made it easy to just set alignment to bottom of
/// field, and then add a little spacer constraint between the textfield and the map.  It was
/// unclear from the comp what you want to do for iPhone X; I opted to make the size of the
/// textfield at 36, but constrain to the safe area - it ends up looking like the comp, and
/// looks OK when the notch is present, also.  
class ViewController: UIViewController {
  @IBOutlet var map: MKMapView!
  @IBOutlet var selectedLocation: UILabel!
  @IBOutlet var confirmAddressButton: UIButton!
  
  let userLocationPin : MKPointAnnotation = MKPointAnnotation()
  let userSelectionPin : MKPointAnnotation = MKPointAnnotation()
  var userSelectionLocation : CLLocation?
  var mapCentered : Bool = false // only do this once.
  
  // Hmmmm, not quite sure what you guys are doing to stick this little border guy on the map;
  // this seems quite close, and I suspect this is not worth obsessing about.
  let borderWidth : CGFloat = 0.5
  private let mapTop : CALayer = CALayer()
  private let mapBottom : CALayer = CALayer()
  
  override func viewDidLoad() {
    super.viewDidLoad()

    mapTop.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
    mapTop.masksToBounds = false
    mapBottom.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
    mapBottom.masksToBounds = false
    map.layer.masksToBounds = false
    map.showsUserLocation = true
    map.delegate = self
    
    let longPress = UILongPressGestureRecognizer(target: self,
      action: #selector(ViewController.longPress(_:)))
    
    map.addGestureRecognizer(longPress)
  }

  @IBAction func confirmAddressButtonTouched(_ sender: UIButton) {
    assert(userSelectionLocation != nil)
    
    /// Given the wording in the comp, I considered using an AlertController here to let the
    /// user 'confirm' that this address is really what they want, but I figured it was a
    /// jump ball either way as to whether that's what you guys really want candidates to do.
    /// It kind of seems excessive, UX-wise, but....i could go either way, I guess.  No doubt
    /// you guys do usability tests on all this kind of stuff, maybe?  
    
    guard let location = userSelectionLocation else {
      confirmAddressButton.isEnabled = false
      return
    }
    
    let vc = DoorDashTabBarVC(location: location)
    present(vc, animated: true, completion: nil)
  }
  
  @objc func longPress(_ sender: UILongPressGestureRecognizer) {
    if sender.state != .began {
      return
    }
    
    let touchPoint = sender.location(in: map)
    let touchMapCoord = map.convert(touchPoint, toCoordinateFrom: map)
    
    map.removeAnnotation(userSelectionPin)
    userSelectionPin.coordinate = touchMapCoord
    map.addAnnotation(userSelectionPin)
    
    let geoCoder = CLGeocoder()
    userSelectionLocation = CLLocation(latitude: touchMapCoord.latitude,
      longitude: touchMapCoord.longitude)
    
    geoCoder.reverseGeocodeLocation(userSelectionLocation!,
      completionHandler: { placemarks, error in
        
      if let error = error {
        self.confirmAddressButton.isEnabled = false
        self.simpleError(error.localizedDescription)
        return
      }
      guard let placemarks = placemarks, placemarks.count > 0 else {
        self.simpleError("No placemarks returned.")
        return
      }
      let placemark = placemarks[0]
      guard let locationName = placemark.name else {
        return
      }
      self.selectedLocation.text = locationName
      self.confirmAddressButton.isEnabled = true
    })
  }
  
  /// Good time to do Core Animation stuff.  Since Auto-Layout is non-existent on layers,
  /// best to capture it here, 'cause system has decided what size frames are going to be, so
  /// you can use those values to drive the size of CALayer sublayers.
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    
    // The comp shows a little bit of border on top and bottom of map; so I'm adding a coupla
    // sublayers here.
    let x = map.layer.bounds.origin.x
    var y = map.layer.bounds.origin.y
    let width = map.layer.bounds.width
    let height = borderWidth
    mapTop.frame = CGRect(x: x, y: y, width: width, height: height)
    if nil == mapTop.superlayer {
      map.layer.addSublayer(mapTop)
    }
    y = map.layer.bounds.origin.y + map.layer.bounds.height
    mapBottom.frame = CGRect(x: x, y: y, width: width, height: height)
    if nil == mapBottom.superlayer {
      map.layer.addSublayer(mapBottom)
    }
  }
}

//MARK: MKMapViewDelegate
/// I had never done anything with Core Location or MKMapView before this homework assignment,
/// and I was really jazzed to do so.  It was a lot 'o fun, although....if you guys really
/// expect devs to do this project in 2-4 hours, maybe I'm not good enough for you :).
extension ViewController : MKMapViewDelegate {
  func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
    guard let userLocation = updateUserPosition() else {
      return
    }
    centerMapOnUserLocation(userLocation)
  }
  
  fileprivate func updateUserPosition() -> CLLocation? {
    guard let location = map.userLocation.location else {
      return nil
    }
    map.removeAnnotation(userLocationPin)
    userLocationPin.coordinate = location.coordinate
    map.addAnnotation(userLocationPin)
    return location
  }
  
  fileprivate func centerMapOnUserLocation(_ userLocation: CLLocation) {
    if mapCentered {
      return
    }
    mapCentered = true
    let region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 2000, 2000)
    map.setRegion(region, animated: true)
  }
}

extension UIViewController {
  func simpleError(_ err : String) {
    let ac = UIAlertController(title: "Error", message: err, preferredStyle: .alert)
    let ok = UIAlertAction(title: "OK", style: .default, handler: { _ in
      ac.dismiss(animated: true, completion: nil)
    })
    ac.addAction(ok)
    present(ac, animated: true, completion: nil)
  }
}

