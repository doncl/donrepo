//
//  DoorDashApiManager.swift
//  dd_ios_takehome
//
//  Created by Don Clore on 2/10/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

/// If we added more endpoing requests to the API, they'd go here.  Right now there's only one,
/// no authentication, simple.
struct DoorDashApiManager {
  static func getStoreObjects(lat: Double, long: Double, success: @escaping ([Store]) -> (),
    failure: @escaping (String) -> ()) {
    
    let uri = "https://api.doordash.com/v1/store_search/?lat=\(lat)&lng=\(long)"
    Net.getNetworkData(uri: uri, success: { data in
      [Store].jsonDecode(data, success: { stores in
        DispatchQueue.main.async {
          success(stores)
        }
      }, failure: { error in
        DispatchQueue.main.async {
          failure(error)
        }
      })
    }, failure: { error in
      DispatchQueue.main.async {
        failure(error)
      }
    })
  }
}
