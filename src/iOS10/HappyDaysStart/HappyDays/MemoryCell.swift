//
//  MemoryCell.swift
//  HappyDays
//
//  Created by Don Clore on 6/26/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit

class MemoryCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    
}
