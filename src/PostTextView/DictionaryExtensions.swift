//
//  DictionaryExtensions.swift
//  PostTextView
//
//  Created by Don Clore on 3/14/17.
//  Copyright © 2017 Beer Barrel Poker Studios. All rights reserved.
//

import Foundation

extension Dictionary {
  mutating func merge(dict: [Key: Value]){
    for (k, v) in dict {
      updateValue(v, forKey: k)
    }
  }
}
