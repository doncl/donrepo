//
//  NSAttributedStringExtensions.swift
//  Duwamish
//
//  Created by Don Clore on 8/30/16.
//  Copyright © 2016 AmplifyMedia. All rights reserved.
//

import Foundation
import UIKit

public struct AttributeRange {
  public var attributes : [String : Any]
  public var range : NSRange
}

//MARK: NSAttributedString

public extension NSAttributedString {
  // This is strictly a debug-print thing.  The idea is to be able to see what attributes
  // are set on an NSAttributedString.
  public func logAttributes() {
    let attrRanges = getAttributesWithRanges()
    
    for attrRange in attrRanges {
      
      let attrs = attrRange.attributes
      for kvp in attrs {
        print("attr: \(kvp.0), value: \(kvp.1)")
      }
    }
  }
  
  func getAttributesWithRanges() -> [AttributeRange] {
    var returnedAttrs : [AttributeRange] = []
    enumerateAttributes(in: NSMakeRange(0, length), options: [], using: {
      (attrs: [String : Any], range: NSRange, stop : UnsafeMutablePointer<ObjCBool>) -> Void in
      
      if attrs.count == 0 {
        return
      }
      
      let attributeRange = AttributeRange(attributes: attrs, range: range)
      returnedAttrs.append(attributeRange)
    })
    return returnedAttrs
  }
  
  func attributePresent(_ attribute: String) -> AttributeRange? {
    let attrRanges = getAttributesWithRanges()
    return attrRanges.first(where: {$0.attributes.contains(where: {$0.key == attribute})})
  }
  
}

public enum TrimOptions {
  case start
  case end
}

//MARK: NSMutableAttributedString
public extension NSMutableAttributedString {
  
  //http://stackoverflow.com/questions/38737920/swift-nsattributedstring-trim
  public func trimCharactersInSet(charSet: CharacterSet,
                                  options: [TrimOptions] = [.start, .end]) {
    
    if options.contains(.start) {
      var range = (string as NSString).rangeOfCharacter(from: charSet)
      
      // Trim leading characters from character set.
      while range.length != 0 && range.location == 0 {
        replaceCharacters(in: range, with: "")
        range = (string as NSString).rangeOfCharacter(from: charSet)
      }
    }
    
    if options.contains(.end) {
      // Trim trailing characters from character set.
      var range = (string as NSString).rangeOfCharacter(from: charSet, options: .backwards)
      while range.length != 0 && NSMaxRange(range) == length {
        replaceCharacters(in: range, with: "")
        range = (string as NSString).rangeOfCharacter(from: charSet, options: .backwards)
      }
    }
  }
  
 
  static public func fromInt(_ int : Int) -> NSMutableAttributedString {
    let attrStr = NSMutableAttributedString(string: String(int))
    
    let style = NSMutableParagraphStyle()
    style.alignment = .center
    
    attrStr.addAttribute(NSParagraphStyleAttributeName, value: style,
                         range: NSMakeRange(0, attrStr.length))
    
    attrStr.addAttribute(NSFontAttributeName, value: UIFont.preferredFont(forTextStyle: .footnote),
                         range: NSMakeRange(0, attrStr.length))
    
    return attrStr
  }
  
  public func setFont(_ font: UIFont) {
    addAttribute(NSFontAttributeName, value: font, range: NSMakeRange(0, length))
  }
  
  public func setTextColor(_ color: UIColor) {
    addAttribute(NSForegroundColorAttributeName, value: color, range: NSMakeRange(0, length))
  }
}
