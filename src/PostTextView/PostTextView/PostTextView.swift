//
//  PostTextView.swift
//  PostTextView
//
//  Created by Don Clore on 3/14/17.
//  Copyright © 2017 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit
import SnapKit
import QuartzCore

class PostTextViewContainer: UIViewController {
  struct Constants {
    static let toolbarHeight : CGFloat = 50.0
    static let toolbarSpacerWidth : CGFloat = 15.0
  }
  
  private var textView : UITextView!  // deferred creation
  var highlightedTextStorage : HighlightedTextStorage = HighlightedTextStorage()
  private var layoutManager : NSLayoutManager = NSLayoutManager()
  
  var boldBtn : UIButton = UIButton(type: .custom)
  var italicBtn : UIButton = UIButton(type: .custom)
  var strikeBtn : UIButton = UIButton(type: .custom)
  
  init() {
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func viewDidLoad() {
    let viewRect = view.bounds
    highlightedTextStorage.substitutionDelegate = self
    
    let containerSize = CGSize(width: viewRect.width, height: CGFloat.greatestFiniteMagnitude)
    
    let container = NSTextContainer(size: containerSize)
    container.widthTracksTextView = true
    layoutManager.addTextContainer(container)
    highlightedTextStorage.addLayoutManager(layoutManager)
    
    textView = UITextView(frame: viewRect, textContainer: container)
    textView.delegate = self
    view.addSubview(textView)
    textView.font = UIFont.preferredFont(forTextStyle: .body)
    
    textView.snp.makeConstraints { (make) -> Void in
      make.edges.equalTo(view)
    }
    
    let grey151 = #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1)
    
    let toolbarRect = CGRect(x: 0, y: 0, width: view.frame.width, height: Constants.toolbarHeight)
    let toolbar = UIToolbar(frame: toolbarRect)
    toolbar.barStyle = .default
    let cameraItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_photo_camera"), style: .plain, target: self,
      action: #selector(PostTextViewContainer.cameraClicked))
    
    let spacer = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
    spacer.width = Constants.toolbarSpacerWidth
    
    boldBtn.addTarget(self, action: #selector(PostTextViewContainer.boldClicked(_:)),
      for: .touchUpInside)
    
    boldBtn.setBackgroundImage(#imageLiteral(resourceName: "Bold").withRenderingMode(.alwaysTemplate), for: .normal)
    boldBtn.tintColor = grey151
    boldBtn.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
    let boldItem = UIBarButtonItem(customView: boldBtn)

    italicBtn.addTarget(self, action: #selector(PostTextViewContainer.italicClicked(_sender:)),
                        for: .touchUpInside)
    italicBtn.setBackgroundImage(#imageLiteral(resourceName: "Italic").withRenderingMode(.alwaysTemplate), for: .normal)
    italicBtn.tintColor = grey151
    italicBtn.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
    
    let italicItem = UIBarButtonItem(customView: italicBtn)
    

    strikeBtn.addTarget(self, action: #selector(PostTextViewContainer.strikeClicked(_:)),
      for: .touchUpInside)
    strikeBtn.setBackgroundImage(#imageLiteral(resourceName: "Strike").withRenderingMode(.alwaysTemplate), for: .normal)
    strikeBtn.tintColor = grey151
    strikeBtn.frame = CGRect(x: 0, y:0, width: 20, height: 20)
    
    let strikeItem = UIBarButtonItem(customView: strikeBtn)
    

    toolbar.items = [cameraItem, spacer, boldItem, spacer, italicItem, spacer, strikeItem]
    toolbar.sizeToFit()
    textView.inputAccessoryView = toolbar
    toolbar.clipsToBounds = true
    toolbar.backgroundColor = .white
    toolbar.tintColor = #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1)
  }
}

//MARK: Behaviors
extension PostTextViewContainer  {
  func cameraClicked() {
  }
    
  func strikeClicked(_ sender: UIBarButtonItem) {
    highlightedTextStorage.strike = !highlightedTextStorage.strike
    if highlightedTextStorage.strike {
      strikeBtn.setBackgroundImage(#imageLiteral(resourceName: "Strike").withRenderingMode(.alwaysOriginal), for: .normal)
    } else {
      strikeBtn.setBackgroundImage(#imageLiteral(resourceName: "Strike").withRenderingMode(.alwaysTemplate), for: .normal)
    }
  }
  
  func boldClicked(_ sender : UIBarButtonItem) {
    highlightedTextStorage.bold = !highlightedTextStorage.bold
    if highlightedTextStorage.bold {
      boldBtn.setBackgroundImage(#imageLiteral(resourceName: "Bold").withRenderingMode(.alwaysOriginal), for: .normal)
    } else {
      boldBtn.setBackgroundImage(#imageLiteral(resourceName: "Bold").withRenderingMode(.alwaysTemplate), for: .normal)
    }
  }
  
  func italicClicked(_sender : UIBarButtonItem) {
    highlightedTextStorage.italic = !highlightedTextStorage.italic
    if highlightedTextStorage.italic {
      italicBtn.setBackgroundImage(#imageLiteral(resourceName: "Italic").withRenderingMode(.alwaysOriginal), for: .normal)
    } else {
      italicBtn.setBackgroundImage(#imageLiteral(resourceName: "Italic").withRenderingMode(.alwaysTemplate), for: .normal)
    }
  }
}

extension PostTextViewContainer : UITextViewDelegate {
  
  
}

extension PostTextViewContainer : HighlightedTextStorageDelegate {
  func didApplySubstitution(attributes: [String : AnyObject], overRange range: NSRange) {
    clearAllButtons()
  }
    
  func clearAllButtons() {
    strikeBtn.setBackgroundImage(#imageLiteral(resourceName: "Strike").withRenderingMode(.alwaysTemplate), for: .normal)
    boldBtn.setBackgroundImage(#imageLiteral(resourceName: "Bold").withRenderingMode(.alwaysTemplate), for: .normal)
    italicBtn.setBackgroundImage(#imageLiteral(resourceName: "Italic").withRenderingMode(.alwaysTemplate), for: .normal)
  }
}
