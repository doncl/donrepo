//
//  ViewController.swift
//  PostTextView
//
//  Created by Don Clore on 3/14/17.
//  Copyright © 2017 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
  var textViewContainer : PostTextViewContainer = PostTextViewContainer()

  override func viewDidLoad() {
    super.viewDidLoad()
    
    addChildViewController(textViewContainer)
    textViewContainer.didMove(toParentViewController: self)
    view.addSubview(textViewContainer.view)
    
    textViewContainer.view.snp.makeConstraints { (make) -> Void in
      make.top.equalTo(topLayoutGuide.snp.bottom)
      make.leading.equalTo(view)
      make.trailing.equalTo(view)
      make.bottom.equalTo(view)
    }
  }
}

