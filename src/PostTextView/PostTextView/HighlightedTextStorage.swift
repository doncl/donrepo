//
//  HighlightedTextStorage.swift
//  PostTextView
//
//  Created by Don Clore on 3/14/17.
//  Copyright © 2017 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit

protocol HighlightedTextStorageDelegate {
  func didApplySubstitution(attributes : [String : AnyObject],
    overRange range: NSRange)
}

class HighlightedTextStorage: NSTextStorage {
  private let backingStore = NSMutableAttributedString()
  
  var substitutionDelegate : HighlightedTextStorageDelegate?
  
  var bold : Bool = false
  var italic : Bool = false
  var strike : Bool = false
  
  var boldAttributes : [String : AnyObject] = [:]
  var italicAttributes : [String : AnyObject] = [:]
  var boldAndItalicAttributes : [String : AnyObject] = [:]
  var strikeThroughAttributes : [String : AnyObject] = [:]
  
  
  var strikeThroughPlusNormalFont : [String : AnyObject] = [:]
  var boldAttrsNoStrikeThrough : [String : AnyObject] = [:]
  var italicAttrsNoStrikeThrough : [String : AnyObject] = [:]
  var boldAndItalicNoStrikeThrough : [String : AnyObject] = [:]
  
  override var string: String {
    get {
      return backingStore.string
    }
  }
  private var setToNormalOnNextAppend = false
  private var replacements : [String: ((Int, Int), [String : AnyObject])] = [:]
  
  private let normalAttrs : [String : AnyObject] = [
    NSFontAttributeName : UIFont.preferredFont(forTextStyle: .body)
  ]
  
  override init() {
    super.init()
    replacements = createHighlightPatterns()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func createHighlightPatterns() -> [String: ((Int, Int), [String : AnyObject])] {
    boldAttributes = createAttributesForFontStyle(style: .body, bold : true)
    
    italicAttributes = createAttributesForFontStyle(style: .body, italic: true)
    
    boldAndItalicAttributes = createAttributesForFontStyle(style: .body, bold: true,
      italic: true)
        
    strikeThroughAttributes = [NSStrikethroughStyleAttributeName : 1 as AnyObject]
    
    strikeThroughPlusNormalFont = [
      NSFontAttributeName : UIFont.preferredFont(forTextStyle: .body),
      NSStrikethroughStyleAttributeName : 1 as AnyObject,
    ]
    
    boldAttrsNoStrikeThrough = boldAttributes
    boldAttrsNoStrikeThrough[NSStrikethroughStyleAttributeName] = 0 as AnyObject
    
    italicAttrsNoStrikeThrough = italicAttributes
    italicAttrsNoStrikeThrough[NSStrikethroughStyleAttributeName] = 0 as AnyObject
      
    boldAndItalicNoStrikeThrough = boldAndItalicAttributes
    boldAndItalicNoStrikeThrough[NSStrikethroughStyleAttributeName] = 0 as AnyObject
    
    // construct a dictionary of replacements based on regexes
    return [
      "(?<!\\*)(\\*{2}+\\w+(\\s\\w+)*\\*{2}+)(?!\\*)" : ((2, 2), boldAttrsNoStrikeThrough),
      "(?<!\\*)(\\*{1}+\\w+(\\s\\w+)*\\*{1}+)(?!\\*)" : ((1, 1), italicAttrsNoStrikeThrough),
      "(?<!\\*)(\\*{3}+\\w+(\\s\\w+)*\\*{3}+)(?!\\*)" : ((3, 3), boldAndItalicNoStrikeThrough),
      "(~{2,}+\\w+(\\s\\w+)*~{2,}+)" : ((2, 2), strikeThroughPlusNormalFont),
    ]
  }
  
  private func createAttributesForFontStyle(style: UIFontTextStyle,
    bold : Bool = false, italic : Bool = false) -> [String : AnyObject] {
    
    let fontDescriptor = UIFontDescriptor .preferredFontDescriptor(withTextStyle: style)
    var descriptorWithTrait : UIFontDescriptor?
    if bold && italic {
      descriptorWithTrait = fontDescriptor.withSymbolicTraits([.traitItalic, .traitBold])
    } else if bold {
      descriptorWithTrait = fontDescriptor.withSymbolicTraits(.traitBold)
    } else {
      descriptorWithTrait = fontDescriptor.withSymbolicTraits(.traitItalic)
    }
    let font = UIFont(descriptor: descriptorWithTrait!, size: 0.0)
    return [NSFontAttributeName : font]
  }
  
  override func attributes(at location: Int, effectiveRange range: NSRangePointer?)
    -> [String : Any] {
      return backingStore.attributes(at: location, effectiveRange: range)
  }
  
  override func replaceCharacters(in range: NSRange, with str: String) {
    print("replaceCharacters in range: \(NSStringFromRange(range)), " +
      "with str: \(str)")
    
    beginEditing()
    
    backingStore.replaceCharacters(in: range, with: str)
    
    let changeInLength = str.characters.count - range.length
    
    edited([.editedAttributes, .editedCharacters], range: range,
           changeInLength: changeInLength)
    
    endEditing()
  }
  
  override func setAttributes(_ attrs: [String : Any]?, range: NSRange) {
    print("setAttributes(\(attrs), range: \(NSStringFromRange(range)))")
    
    beginEditing()
    
    var mutableAttrs = attrs
    
    if setToNormalOnNextAppend && range.length == 1 && range.location == backingStore.length - 1 {
      setToNormalOnNextAppend = false
    
      mutableAttrs = normalAttrs
    }
    backingStore.setAttributes(mutableAttrs, range: range)
    edited([.editedAttributes], range: range, changeInLength: 0)
    
    endEditing()
  }
  
  override func processEditing() {
    if bold {
       addAttributes(boldAttributes, range: editedRange)
    }
    if italic {
      addAttributes(italicAttributes, range: editedRange)
    }
    // This is very simplistic, and redundant, but it works, and makes for simple code, for
    // now (when we add lots of other attributes, that might change).
    // If both bold AND italic buttons are down, we need to set the
    if bold && italic {
      addAttributes(boldAndItalicAttributes, range: editedRange)
    }
    if strike {
      addAttributes(strikeThroughAttributes, range: editedRange)
    } else {
      removeAttribute(NSStrikethroughStyleAttributeName, range: editedRange)
    }
    
    // Strikethrough is a different attribute, so this will not cancel the strikethrough.
    if !bold && !italic {
      addAttributes(normalAttrs, range: editedRange)
    }
    
    performReplacementsForRange(changedRange: editedRange)
    super.processEditing()
  }
  
  private func performReplacementsForRange(changedRange: NSRange) {
    let nsString = backingStore.string as NSString
    let startOfRange = nsString.lineRange(for: NSMakeRange(changedRange.location, 0))
    let endOfRange = nsString.lineRange(for: NSMakeRange(NSMaxRange(changedRange), 0))
    
    var extendedRange = NSUnionRange(changedRange, startOfRange)
    extendedRange = NSUnionRange(extendedRange, endOfRange)
    
    applyStylesToRange(extendedRange)
  }
  
  private func applyStylesToRange(_ searchRange: NSRange) {
    var mutableSearchRanage = searchRange
    var attrRange : NSRange = searchRange
    for (pattern, tuple) in replacements {
      let regex = try! NSRegularExpression(pattern: pattern, options: [])
      
      let matches = regex.matches(in: backingStore.string, options: [], range: mutableSearchRanage)
      for match in matches {
      
        let matchRange = match.range
        // TRIM OFF THE MARKDOWN SPECIAL CHARACTERS ON THE ENDS OF THE SUBSTRING
        beginEditing()
        
        // Get the one at the beginning.
        let endMarginLengths = tuple.0
        if endMarginLengths.1 > 0 {
          let endDeletionRange = NSMakeRange(
            matchRange.location + matchRange.length - endMarginLengths.1, endMarginLengths.1)
          
          deleteCharacters(in: endDeletionRange)
          mutableSearchRanage = NSMakeRange(mutableSearchRanage.location,
            mutableSearchRanage.length - endMarginLengths.1)
        }
        
        // Get the one at the end.
        if endMarginLengths.0 > 0 {
          let beginDeletionRange = NSMakeRange(matchRange.location, endMarginLengths.0)
          deleteCharacters(in: beginDeletionRange)
          mutableSearchRanage = NSMakeRange(mutableSearchRanage.location,
            mutableSearchRanage.length - endMarginLengths.0)
        }
        endEditing()
        
        let attributes = tuple.1
        attrRange = NSMakeRange(matchRange.location, matchRange.length -
          (endMarginLengths.0 + endMarginLengths.1))
        
        addAttributes(attributes, range: attrRange)
        substitutionDelegate?.didApplySubstitution(attributes: attributes, overRange: attrRange)
        
        strike = false
        bold = false
        italic = false
        
        // reset the style to the original
        let afterMatch = NSMaxRange(attrRange)
        if afterMatch < self.length {
          let normalRange = NSMakeRange(afterMatch, 1)
          setToNormal(normalRange)
        } else {
          setToNormalOnNextAppend = true
        }
      }
    }
  }
  
  private func setToNormal(_ range: NSRange) {
    let normalAttrs = [NSFontAttributeName : UIFont.preferredFont(forTextStyle: .body)]
    addAttributes(normalAttrs, range: range)
    removeAttribute(NSStrikethroughStyleAttributeName, range: range)
  }
  
  
  public func outputMarkdown() -> String {
    var outputString = NSMutableString(string: backingStore.string)
    let attrRanges = getAttributesWithRanges().reversed()
    
    for attrRange in attrRanges {
      let attrs = attrRange.attributes
      let range = attrRange.range
      
      
      changeRangeToMarkdown(string: &outputString, attrs: attrs as [String : AnyObject], range: range)
    }
    
    return outputString as String
  }
  
  fileprivate func changeRangeToMarkdown(string : inout NSMutableString,
                                         attrs: [String : AnyObject], range: NSRange) {
    
    let replacementString = NSMutableString(string: string.substring(with: range))
    var bold = false
    var italic = false
    var strike = false
    
    if let font = attrs[NSFontAttributeName] as? UIFont {
      let desc = font.fontDescriptor
      if desc.symbolicTraits.contains(.traitBold) {
        bold = true
      }
      if desc.symbolicTraits.contains(.traitItalic) {
        italic = true
      }
    }
    
    if let strikeNum = attrs[NSStrikethroughStyleAttributeName] as? NSNumber {
      strike = strikeNum.boolValue
    }
    
    var prefix : String = ""
    var suffix : String = ""
    
    if italic && bold {
      prefix = "***"
      suffix = "***"
    } else if bold {
      prefix = "**"
      suffix = "**"
    } else if italic {
      prefix = "*"
      suffix = "*"
    }
    
    if strike {
      prefix = "~~" + prefix
      suffix = suffix + "~~"
    }
    
    replacementString.insert(prefix, at: 0)
    replacementString.append(suffix)
    
    string.replaceCharacters(in: range, with: replacementString as String)
  }
}
