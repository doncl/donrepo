//
//  ViewController.swift
//  seg
//
//  Created by Don Clore on 12/29/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func segmentToggled(_ sender: UISegmentedControl) {
    if sender.selectedSegmentIndex == 0 {
      view.backgroundColor = .white
    } else {
      view.backgroundColor = .green
    }
  }
  
}

