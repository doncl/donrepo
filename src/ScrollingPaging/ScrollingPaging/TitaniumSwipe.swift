//
//  TitaniumSwipe.swift
//  ScrollingPaging
//
//  Created by Don Clore on 2/3/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import UIKit

class TitaniumSwipe: UIViewController {
    var vcs: [UIViewController] = []
    var titles: [String] = [
        "ALL", "PREMIER LEAGUE", "LA LIGA", "BUNDESLIGA", "LIGA MX","LIGUE1","PEMERA DIVISION OF ARGENTINA",
        "B METROPOLITAN DE ARGENTINA", "COPA DEL REY", "SEGUNDA DIVISION DE ESPANA"]

    let headerHeight: CGFloat = 60
    var flowLayout: UICollectionViewFlowLayout
    let header: UICollectionView
    let pager: UIPageViewController = UIPageViewController(transitionStyle: .scroll,
                                                           navigationOrientation: UIPageViewController.NavigationOrientation.horizontal,
                                                           options: nil)

    var currentIndex: Int = 0

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        header = UICollectionView(frame: .zero, collectionViewLayout: self.flowLayout)
        super.init(nibName: nil, bundle: nil)
    }

    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        titles.forEach({
            let vc = TitleViewController($0)
            vcs.append(vc)
        })
        pager.dataSource = self

        if let firstVC = vcs.first {
            pager.setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }

        view.addSubview(header)
        header.translatesAutoresizingMaskIntoConstraints = false
        header.showsVerticalScrollIndicator = false
        header.showsHorizontalScrollIndicator = false
        header.backgroundColor = .black

        addChild(pager)
        pager.delegate = self
        pager.didMove(toParent: self)
        view.addSubview(pager.view)

        pager.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            header.topAnchor.constraint(equalTo: view.topAnchor),
            header.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            header.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            header.heightAnchor.constraint(equalToConstant: headerHeight),
            pager.view.topAnchor.constraint(equalTo: header.bottomAnchor),
            pager.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pager.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            pager.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])

        header.register(HeaderCell.self, forCellWithReuseIdentifier: HeaderCell.cellId)
        header.allowsSelection = true
        header.allowsMultipleSelection = false
        flowLayout.estimatedItemSize = CGSize(width: 150, height: headerHeight)
        header.dataSource = self
        header.delegate = self
        header.reloadData()
        header.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.left)
    }
}

extension TitaniumSwipe: UIPageViewControllerDelegate {
    public func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {

    }

    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
    }
}

extension TitaniumSwipe: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = vcs.firstIndex(of: viewController) else {
            return nil
        }
        let previousIndex = index - 1
        guard previousIndex > -1 else {
            return nil
        }
        guard vcs.count > previousIndex else {
            return nil
        }
        currentIndex = previousIndex
        header.selectItem(at: IndexPath(item: currentIndex, section: 0), animated: true, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
        header.setNeedsLayout()
        return vcs[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = vcs.firstIndex(of: viewController) else {
            return nil
        }

        let nextIndex = index + 1
        guard vcs.count != nextIndex else {
            return vcs[0]
        }
        guard vcs.count > nextIndex else {
            return nil
        }

        currentIndex = nextIndex
        print("Current = \(titles[currentIndex])")
        header.selectItem(at: IndexPath(item: currentIndex, section: 0), animated: true, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
        header.setNeedsLayout()
        return vcs[nextIndex]
    }

}

extension TitaniumSwipe: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vcs.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HeaderCell.cellId, for: indexPath) as? HeaderCell else {
            fatalError("Should never happen")
        }

        cell.label.text = titles[indexPath.item]

        return cell
    }
}

extension TitaniumSwipe: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? HeaderCell else {
            return
        }
        cell.isSelected = true
        cell.setNeedsLayout()
        let vc = vcs[indexPath.item]
        let direction: UIPageViewController.NavigationDirection = indexPath.item >= currentIndex ? .forward : .reverse
        currentIndex = indexPath.item
        pager.setViewControllers([vc], direction: direction, animated: true, completion: nil)

        collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
    }

    public func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? HeaderCell else {
            return
        }
        cell.isSelected = false
    }

    func setCellSelection(_ collectionView: UICollectionView, indexPath: IndexPath, selected: Bool) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? HeaderCell else {
            return
        }
        cell.isSelected = selected
        cell.setNeedsLayout()
    }
}


fileprivate class HeaderCell: UICollectionViewCell {
    static let cellId: String = "HeaderCell"
    static let selectedColor: UIColor = .white
    static let normalColor: UIColor = UIColor(white: 119 / 255, alpha: 1.0)

    let underline: CALayer = CALayer()
    let underlineHeight: CGFloat = 2.0

    let label: UILabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        secondPhaseInitializer()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        secondPhaseInitializer()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        if isSelected {
            label.textColor = HeaderCell.selectedColor
        } else {
            label.textColor = HeaderCell.normalColor
        }
    }

    override var isSelected: Bool {
        get {
            return super.isSelected
        }
        set {
            label.textColor = newValue ? HeaderCell.selectedColor : HeaderCell.normalColor
            underline.isHidden = !newValue
            super.isSelected = newValue
            setNeedsLayout()
            setNeedsDisplay()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let x: CGFloat = 0
        let y: CGFloat = bounds.height - underlineHeight

        underline.frame = CGRect(x: x, y: y, width: bounds.width, height: underlineHeight)
//        if isSelected {
//            underline.isHidden = false
//        } else {
//            underline.isHidden = true
//        }
    }

    fileprivate func secondPhaseInitializer() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = HeaderCell.normalColor

        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor),
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),

            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        ])

        underline.backgroundColor = UIColor.red.cgColor
        contentView.layer.addSublayer(underline)
        underline.isHidden = true
        underline.masksToBounds = true
    }
}

private class TitleViewController: UIViewController {

    let color: UIColor = UIColor(white: 73 / 255, alpha: 1.0)
    let label: UILabel = UILabel()

    init(_ title: String) {
        super.init(nibName: nil, bundle: nil)
        self.title = title
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: .largeTitle)
        label.textAlignment = .center
        label.textColor = .white
        label.text = title
        NSLayoutConstraint.activate([
            label.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
        view.backgroundColor = color
    }
}
