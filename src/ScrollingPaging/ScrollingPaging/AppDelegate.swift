//
//  AppDelegate.swift
//  ScrollingPaging
//
//  Created by Don Clore on 2/3/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let vc = TitaniumSwipe()
        let bounds = UIScreen.main.bounds
        window = UIWindow(frame: bounds)
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
        return true
    }
}

