//
//  GameTodayUITests.swift
//  GoCubs
//
//  Created by Ellen Shapiro on 2/21/16.
//  Copyright © 2016 Vokal. All rights reserved.
//

import XCTest

class GameTodayUITests: XCTestCase {
  
  override func setUp() {
    super.setUp()
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    continueAfterFailure = false
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    XCUIApplication().launch()
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    XCUIDevice.sharedDevice().orientation = .Portrait
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testNavigatingToAndFromGameToday() {
    let app = XCUIApplication()
    app.navigationBars[LocalizedString.listTitle]
      .buttons[AccessibilityLabel.gameTodayButton]
      .tap()
    let closeButton = app.buttons[AccessibilityLabel.closeButton]
    closeButton.tap()
    XCTAssertFalse(closeButton.exists)
  }
}
