//
//  Cubs2015TableViewController.swift
//  GoCubs
//
//  Created by Ellen Shapiro on 9/29/15.
//  Copyright © 2015 RWDevCon. All rights reserved.
//

import UIKit

class Cubs2015TableViewController: UITableViewController {
  
  var dataSource: CubsGameDataSource!
  
  @IBOutlet var gameTodayButton: UIBarButtonItem!
  
  //MARK: - View Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //Setup the data source
    dataSource = CubsGameDataSource(tableView: tableView)
    setupAccessibilityAndLocalization()
  }
  
  private func setupAccessibilityAndLocalization() {
    title = LocalizedString.listTitle
    gameTodayButton.accessibilityLabel = AccessibilityLabel.gameTodayButton
  }
  
  override func viewWillAppear(animated: Bool) {
    //Clear selection before VWA if the splitviewcontroller is collapsed.
    if let split = splitViewController {
      clearsSelectionOnViewWillAppear = split.collapsed
    }
    
    super.viewWillAppear(animated)
  }
  
  // MARK: - Segues
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let navigationController = segue.destinationViewController as? UINavigationController

    if let gameDetailVC = navigationController?.topViewController as? GameDetailViewController,
        cell = sender as? CubsGameCell {
            gameDetailVC.game = dataSource.gameForCell(cell, inTableView: tableView)
            gameDetailVC.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem()
            gameDetailVC.navigationItem.leftItemsSupplementBackButton = true
    }
  }
}
