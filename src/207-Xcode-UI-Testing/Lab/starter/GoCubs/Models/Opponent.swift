//
//  Opponent.swift
//  GoCubs
//
//  Created by Ellen Shapiro on 10/11/15.
//  Copyright © 2015 RWDevCon. All rights reserved.
//

import Foundation
import UIKit

struct Opponent {
  let at = "at "
  let name: String
  let isHomeTeam: Bool
  let team: MLBTeam
  
  init(teamName: String) {
    if teamName.hasPrefix(at) {
      isHomeTeam = true
      name = teamName.substringFromIndex(at.endIndex)
    } else {
      isHomeTeam = false
      name = teamName
    }
    
    guard let enumTeam = MLBTeam(rawValue: name) else {
      fatalError("Couldn't get team colors for team named \(teamName)")
    }
    
    self.team = enumTeam
  }
}
