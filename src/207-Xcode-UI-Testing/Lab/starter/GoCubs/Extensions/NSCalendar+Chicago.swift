//
//  NSCalendar+Chicago.swift
//  GoCubs
//
//  Created by Ellen Shapiro on 12/27/15.
//  Copyright © 2015 RWDevCon. All rights reserved.
//

import Foundation

extension NSCalendar {

  //Since NSCalendar inherits from NSObject, you have to mark this
  //as @nonobjc or you'll get an error about how a declaration can't be
  //both final and dynamic.
  @nonobjc static let chicagoCalendar: NSCalendar = {
    let calendar = NSCalendar.currentCalendar()
    
    //We need to make sure everything is converted to Chicago time.
    guard let chicagoTime = NSTimeZone(name: "America/Chicago") else {
      assertionFailure("Can't create time zone!")
      return calendar
    }
    
    calendar.timeZone = chicagoTime
    
    return calendar
  }()
    
  static func dateInChicagoForMonth(month: Int, day: Int, year: Int?) -> NSDate {
    let components = NSDateComponents()
    components.day = day
    components.month = month
        
    if let year = year {
      components.year = year
    }
        
    guard let date = self.chicagoCalendar.dateFromComponents(components) else {
      fatalError("Could not create date for \(month)/\(day)/\(year)")
    }
        
    return date
  }
}
