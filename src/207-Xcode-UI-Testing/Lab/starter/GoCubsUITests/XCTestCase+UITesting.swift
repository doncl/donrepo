//
//  XCTestCase+UITesting.swift
//  GoCubs
//
//  Created by Ellen Shapiro on 2/21/16.
//  Copyright © 2016 Vokal. All rights reserved.
//

import XCTest

extension XCTestCase {
  
  func launchAppForUITesting() {
    continueAfterFailure = false
    XCUIApplication().launch()
    XCUIDevice.sharedDevice().orientation = .Portrait
  }
}
