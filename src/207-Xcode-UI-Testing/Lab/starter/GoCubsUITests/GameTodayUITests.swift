//
//  GameTodayUITests.swift
//  GoCubs
//
//  Created by Ellen Shapiro on 2/21/16.
//  Copyright © 2016 Vokal. All rights reserved.
//

import XCTest

class GameTodayUITests: XCTestCase {
  
  //MARK: - Test Lifecycle
  
  override func setUp() {
    super.setUp()
    launchAppForUITesting()
  }
  
  //MARK: - Helper Methods
  
  private func goToGameToday() {
    let app = XCUIApplication()
    app.navigationBars[LocalizedString.listTitle]
      .buttons[AccessibilityLabel.gameTodayButton]
      .tap()
    let closeButton = app.buttons[AccessibilityLabel.closeButton]
    XCTAssertTrue(closeButton.exists)
  }
  
  private func checkDataAsExpectedForMonth(month: Int, day: Int, year: Int,
    expectedYesOrNo: String,
    expectedParking: String) {
      
      let chicagoDate = NSCalendar.dateInChicagoForMonth(month, day: day, year: year)
      let formattedChicagoDate = NSDateFormatter.longDateFormatter.stringFromDate(chicagoDate)
      
      let app = XCUIApplication()
      XCTAssertTrue(app.staticTexts[formattedChicagoDate].exists)
      XCTAssertTrue(app.staticTexts[expectedYesOrNo].exists)
      XCTAssertTrue(app.staticTexts[expectedParking].exists)
  }
  
  //MARK: - Actual Tests

  func testNavigatingToAndFromGameToday() {
    goToGameToday()
    let closeButton = XCUIApplication().buttons[AccessibilityLabel.closeButton]
    closeButton.tap()
    XCTAssertFalse(closeButton.exists)
  }
  
  func testExpectedInformationShowsUpForToday() {
    goToGameToday()
    // NOTE: If you're trying this not at RWDevCon, you should change this
    //       to the current date (Chicago time) or it will fail since it will 
    //       look for the formatted date string for whatever the current date 
    //       is in Chicago. 
    //       If you're not sure what that date is, build and run
    //       the app, and tap the calendar icon - when that screen loads, you'll 
    //       see the date Xcode should be looking for in this initial state.    
    checkDataAsExpectedForMonth(3, day: 12, year: 2016,
      expectedYesOrNo: LocalizedString.gameTodayNegative,
      expectedParking: LocalizedString.parkingOK)
  }
}
