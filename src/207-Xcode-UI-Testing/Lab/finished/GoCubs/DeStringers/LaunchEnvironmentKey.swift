//
//  LaunchEnvironmentKey.swift
//  GoCubs
//
//  Created by Ellen Shapiro on 2/23/16.
//  Copyright © 2016 Vokal. All rights reserved.
//

import Foundation

enum LaunchEnvironmentKey: String {
  case IsUITesting = "IS_UI_TESTING",
  MonthToTest = "CUBS_MONTH",
  DayToTest = "CUBS_DAY",
  YearToTest = "CUBS_YEAR"
  
  func isInLaunchArguments() -> Bool {
    return NSProcessInfo.processInfo().arguments.contains(rawValue)
  }
  
  func processInfoValue() -> String? {
    return NSProcessInfo.processInfo().environment[rawValue]
  }
}
