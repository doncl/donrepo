//
//  GameTodayUITests.swift
//  GoCubs
//
//  Created by Ellen Shapiro on 2/21/16.
//  Copyright © 2016 Vokal. All rights reserved.
//

import XCTest

class GameTodayUITests: XCTestCase {
  
  //MARK: - Test Lifecycle
  
  override func setUp() {
    super.setUp()
    launchAppForUITesting()
  }
  
  //MARK: - Helper Methods
  
  private func goToGameToday() {
    let app = XCUIApplication()
    app.navigationBars[LocalizedString.listTitle]
      .buttons[AccessibilityLabel.gameTodayButton]
      .tap()
    let closeButton = app.buttons[AccessibilityLabel.closeButton]
    XCTAssertTrue(closeButton.exists)
  }
  
  private func checkDataAsExpectedForMonth(month: Int, day: Int, year: Int,
    expectedYesOrNo: String,
    expectedParking: String) {
      
      let chicagoDate = NSCalendar.dateInChicagoForMonth(month, day: day, year: year)
      let formattedChicagoDate = NSDateFormatter.longDateFormatter.stringFromDate(chicagoDate)
      
      let app = XCUIApplication()
      XCTAssertTrue(app.staticTexts[formattedChicagoDate].exists)
      XCTAssertTrue(app.staticTexts[expectedYesOrNo].exists)
      XCTAssertTrue(app.staticTexts[expectedParking].exists)
  }
  
  private func launchAppWithMonth(month: Int, day: Int, year: Int) {
    launchAppForUITesting(withAdditionalInfo: [
      LaunchEnvironmentKey.MonthToTest.rawValue: String(month),
      LaunchEnvironmentKey.DayToTest.rawValue: String(day),
      LaunchEnvironmentKey.YearToTest.rawValue: String(year)])
  }
  
  //MARK: - Actual Tests

  func testNavigatingToAndFromGameToday() {
    goToGameToday()
    let closeButton = XCUIApplication().buttons[AccessibilityLabel.closeButton]
    closeButton.tap()
    XCTAssertFalse(closeButton.exists)
  }
  
  func testDisplayingEventAtWrigley() {
    let month = 4
    let day = 5
    let year = 2015
    launchAppWithMonth(month, day: day, year: year)
    goToGameToday()
    checkDataAsExpectedForMonth(month, day: day, year: year,
      expectedYesOrNo: LocalizedString.gameTodayPositive,
      expectedParking: LocalizedString.parkingTerrible)
  }
  
  func testDisplayingEventNotAtWrigley() {
    let month = 6
    let day = 30
    let year = 2016
    self.launchAppWithMonth(month, day: day, year: year)
    goToGameToday()
    checkDataAsExpectedForMonth(month, day: day, year: year,
      expectedYesOrNo: LocalizedString.gameTodayNegative,
      expectedParking: LocalizedString.parkingOK)
  }
  
  func testDisplayingNoEvent() {
    let month = 12
    let day = 25
    let year = 2015
    launchAppWithMonth(month, day: day, year: year)
    goToGameToday()
    self.checkDataAsExpectedForMonth(month, day: day, year: year,
      expectedYesOrNo: LocalizedString.gameTodayNegative,
      expectedParking: LocalizedString.parkingOK)
  }
}
