//
//  GoCubsUITests.swift
//  GoCubsUITests
//
//  Created by Ellen Shapiro on 2/21/16.
//  Copyright © 2016 Vokal. All rights reserved.
//

import XCTest

class GoCubsUITests: XCTestCase {
  
  //MARK: - Test Lifecycle
  
  override func setUp() {
    super.setUp()
    launchAppForUITesting()
  }
  
  //MARK: Helper functions
  
  private func selectGameWithDateString(dateString: String,
    versusString: String) {
      
      //Grab a reference to the application object
      let app = XCUIApplication()
      
      //Select the given opponent on the given date
      app.tables
        .cells
        .containingType(.StaticText, identifier:dateString)
        .staticTexts[versusString]
        .tap()
  }
  
  private func checkResultOfGameWithExpectedWinningTeam(expectedWinningTeamName: String,
    expectedLosingTeamName: String,
    expectedWinningTeamScore: String,
    expectedLosingTeamScore: String,
    expectedFlag: String) {
      
      //Grab a reference to the application object
      let app = XCUIApplication()
      
      //Check that the passed-in values are as expected
      XCTAssertEqual(app.staticTexts[AccessibilityIdentifier.WinningTeamName.rawValue].label, expectedWinningTeamName)
      XCTAssertEqual(app.staticTexts[AccessibilityIdentifier.LosingTeamName.rawValue].label, expectedLosingTeamName)
      XCTAssertEqual(app.staticTexts[AccessibilityIdentifier.WinningTeamScore.rawValue].label, expectedWinningTeamScore)
      XCTAssertEqual(app.staticTexts[AccessibilityIdentifier.LosingTeamScore.rawValue].label, expectedLosingTeamScore)
      
      XCTAssertTrue(app.staticTexts[expectedFlag].exists)
  }
  
  
  //MARK: Actual Tests
  
  func testKnownWin() {
    //October 10th: Cubs beat the Cardinals 6-3
    selectGameWithDateString("Oct 10",
      versusString: "Cubs vs. Cardinals")
    
    checkResultOfGameWithExpectedWinningTeam("CUBS",
      expectedLosingTeamName: "CARDINALS",
      expectedWinningTeamScore: "6",
      expectedLosingTeamScore: "3",
      expectedFlag: AccessibilityLabel.cubsWin)
  }
  
  func testKnownLoss() {
    //August 29th: Cubs lost to the Dodgers, 5-2
    selectGameWithDateString("Aug 29",
      versusString:  "Cubs vs. Dodgers")
    
    checkResultOfGameWithExpectedWinningTeam("DODGERS",
      expectedLosingTeamName: "CUBS",
      expectedWinningTeamScore: "5",
      expectedLosingTeamScore: "2",
      expectedFlag: AccessibilityLabel.cubsLose)
  }
  
  func testKnownPostponement() {
    //September 10th: Cubs and Phillies were rained out
    selectGameWithDateString("Sep 10",
      versusString: "Cubs vs. Phillies")
    
    //Note: The Cubs "win" when a game is postponed
    checkResultOfGameWithExpectedWinningTeam("CUBS",
      expectedLosingTeamName: "PHILLIES",
      expectedWinningTeamScore: "-",
      expectedLosingTeamScore: "-",
      expectedFlag: AccessibilityLabel.postponed)
  }
}
