//
//  XCTestCase+UITesting.swift
//  GoCubs
//
//  Created by Ellen Shapiro on 2/21/16.
//  Copyright © 2016 Vokal. All rights reserved.
//

import XCTest

extension XCTestCase {
  
  func launchAppForUITesting(withAdditionalInfo additionalInfo: [String : String] = [:]) {
    
    //Kill the test suite when something fails. 
    continueAfterFailure = false
    
    //Grab a reference to the application proxy object.
    let app = XCUIApplication()
    
    //Passes in the UI testing variable so the app can check if it's there
    app.launchArguments = [ LaunchEnvironmentKey.IsUITesting.rawValue ]
    
    app.launchEnvironment = additionalInfo
    
    //Actually launch the app
    app.launch()
    
    //Make sure the device is in portrait orientation
    XCUIDevice.sharedDevice().orientation = .Portrait
  }
}
