//
//  GoCubsUITests.swift
//  GoCubsUITests
//
//  Created by Ellen Shapiro on 2/21/16.
//  Copyright © 2016 Vokal. All rights reserved.
//

import XCTest

class GoCubsUITests: XCTestCase {
  
  override func setUp() {
    super.setUp()
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    continueAfterFailure = false
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    XCUIApplication().launch()
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    XCUIDevice.sharedDevice().orientation = .Portrait
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testKnownWin() {
    //October 10th: Cubs beat the Cardinals 6-3
    let app = XCUIApplication()
    app.tables
      .cells
      .containingType(.StaticText, identifier:"Oct 10")
      .staticTexts["Cubs vs. Cardinals"]
      .tap()
    
    XCTAssertEqual(app.staticTexts[AccessibilityIdentifier.WinningTeamName.rawValue].label, "CUBS")
    XCTAssertEqual(app.staticTexts[AccessibilityIdentifier.LosingTeamName.rawValue].label, "CARDINALS")
    XCTAssertEqual(app.staticTexts[AccessibilityIdentifier.WinningTeamScore.rawValue].label, "6")
    XCTAssertEqual(app.staticTexts[AccessibilityIdentifier.LosingTeamScore.rawValue].label, "3")
  }
  
  
  func testKnownLoss() {
    //August 29th: Cubs lost to the Dodgers, 5-2
    let app = XCUIApplication()
    app.tables
      .cells
      .containingType(.StaticText, identifier:"Aug 29")
      .staticTexts["Cubs vs. Dodgers"]
      .tap()
        
    XCTAssertEqual(app.staticTexts[AccessibilityIdentifier.WinningTeamName.rawValue].label, "DODGERS")
    XCTAssertEqual(app.staticTexts[AccessibilityIdentifier.LosingTeamName.rawValue].label, "CUBS")
    XCTAssertEqual(app.staticTexts[AccessibilityIdentifier.WinningTeamScore.rawValue].label, "5")
    XCTAssertEqual(app.staticTexts[AccessibilityIdentifier.LosingTeamScore.rawValue].label, "2")
  }
}
