//
//  GameDetailViewController.swift
//  GoCubs
//
//  Created by Ellen Shapiro on 9/29/15.
//  Copyright © 2015 RWDevCon. All rights reserved.
//

import UIKit

class GameDetailViewController: UIViewController {
  
  //MARK: - Properties
  
  @IBOutlet weak var flagView: UIView!
  @IBOutlet weak var resultLabel: UILabel!
  @IBOutlet weak var winningTeamNameLabel: UILabel!
  @IBOutlet weak var winningTeamScoreLabel: UILabel!
  @IBOutlet weak var losingTeamNameLabel: UILabel!
  @IBOutlet weak var losingTeamScoreLabel: UILabel!
  @IBOutlet weak var cubsRecordLabel: UILabel!
  @IBOutlet weak var winningPitcherNameLabel: UILabel!
  @IBOutlet weak var losingPitcherNameLabel: UILabel!
  
  
  var game: CubsGame? {
    didSet {
      configureForGame()
    }
  }
  
  //MARK: - View Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    configureForGame()
    setupAccessibilityAndLocalization()
  }
  
  private func setupAccessibilityAndLocalization() {
    //TODO
  }
  
  //MARK: - Setup
  
  func configureForGame() {
    guard let cubsGame = game else {
      //There's nothing to configure yet.
      return
    }
    
    //If the view hasn't been loaded by the VC yet, fire it up.
    loadViewIfNeeded()
    
    title = NSDateFormatter.shortMonthDayDateFormatter.stringFromDate(cubsGame.date)
    
    let components = NSCalendar.currentCalendar().components([.Day, .Month], fromDate: cubsGame.date)
    var isPostseason = false
    
    if (components.month > 10 || //Is november OR
      (components.month == 10 && components.day >= 7)) { //Is October 7th or later
        isPostseason = true
    }
    
    switch cubsGame.result.type {
    case .Win:
      winningTeamNameLabel.text = LocalizedString.cubs.uppercaseString
      winningTeamNameLabel.backgroundColor = MLBTeam.Cubs.primaryColor()
      winningTeamScoreLabel.text = "\(cubsGame.result.cubsRuns)"
      losingTeamNameLabel.text = cubsGame.opponent.name.uppercaseString
      losingTeamNameLabel.backgroundColor = cubsGame.opponent.team.primaryColor()
      losingTeamScoreLabel.text = "\(cubsGame.result.opponentRuns)"
    case .Loss:
      losingTeamNameLabel.text = LocalizedString.cubs.uppercaseString
      losingTeamNameLabel.backgroundColor = MLBTeam.Cubs.primaryColor()
      losingTeamScoreLabel.text = "\(cubsGame.result.cubsRuns)"
      winningTeamNameLabel.text = cubsGame.opponent.name.uppercaseString
      winningTeamNameLabel.backgroundColor = cubsGame.opponent.team.primaryColor()
      winningTeamScoreLabel.text = "\(cubsGame.result.opponentRuns)"
    case .Postponed:
      winningTeamNameLabel.text = LocalizedString.cubs.uppercaseString
      winningTeamNameLabel.backgroundColor = MLBTeam.Cubs.primaryColor()
      winningTeamScoreLabel.text = LocalizedString.noResult
      losingTeamNameLabel.text = cubsGame.opponent.name.uppercaseString
      losingTeamNameLabel.backgroundColor = cubsGame.opponent.team.primaryColor()
      losingTeamScoreLabel.text = LocalizedString.noResult
    }
    
    cubsRecordLabel.text = cubsGame.resultString(isPostseason)
    configureFlagForResultType(cubsGame.result.type)
    configurePitchersForGame(cubsGame)
  }
  
  func configurePitchersForGame(game: CubsGame) {
    switch game.result.type {
    case .Postponed:
      winningPitcherNameLabel.text = LocalizedString.noResult
      losingPitcherNameLabel.text = LocalizedString.noResult
    default:
      winningPitcherNameLabel.text = game.winningPitcher.name
      losingPitcherNameLabel.text = game.losingPitcher.name
    }
  }
  
  func configureFlagForResultType(resultType: ResultType) {
    flagView.backgroundColor = resultType.flagBackground
    resultLabel.text = resultType.flagString
    resultLabel.textColor = resultType.flagTextColor
  }
}
