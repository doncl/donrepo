//
//  ViewController.swift
//  DrawingUtilities
//
//  Created by Don Clore on 6/30/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  @IBOutlet var imageView: UIImageView!
  
  override func viewDidLoad() {
    navigationItem.rightBarButtonItems = [
      barItem(title: "gradients", selector: #selector(ViewController.pushGradientsVC(_:))),
      barItem(title: "text", selector: #selector(ViewController.text(_:))),
      barItem(title: "watermark", selector: #selector(ViewController.watermark(_:))),
      barItem(title: "subImage", selector: #selector(ViewController.subImage(_:))),
      barItem(title: "tthumb", selector: #selector(ViewController.thumb(_:))),
      barItem(title: "roundtrip", selector: #selector(ViewController.roundTrip(_:))),
      barItem(title: "polygon", selector: #selector(ViewController.polygon(_:))),
      barItem(title: "star", selector: #selector(ViewController.star(_:))),
      barItem(title: "inflect", selector: #selector(ViewController.inflectedPath(_:))),
    ]
  }
  
  @objc fileprivate func pushGradientsVC(_ sender: Any) {
    guard let nav = navigationController else {
      return
    }
    
    let gradientVC = GradientsVC(frame: view.bounds)
    nav.pushViewController(gradientVC, animated: true)
  }
  
  fileprivate func barItem(title: String, selector : Selector) -> UIBarButtonItem {
    return UIBarButtonItem(title: title, style: .plain, target: self, action: selector)
  }
  
  @objc fileprivate func text(_ sender : Any) {
    guard let image = UIBezierPath.textImage(from: "Whole Lotta Love",
        withFontFace: "HelveticaNeue", pointSize: 32.0,
        renderSize: CGSize(width: 300.0, height: 50.0)) else {
      
      return
    }
    imageView.image = image
    imageView.contentMode = .scaleAspectFit
  }
  
  @objc fileprivate func watermark(_ sender: Any) {
    if let colorImg = UIImage(named: "Bao.jpg") {
      watermarkTheImage(colorImg: colorImg)
    }
  }
  
  @objc fileprivate func subImage(_ sender: Any) {
    if let colorImg = UIImage(named: "Bao.jpg") {
      extractSubImage(CGRect(x: 1000, y: 300, width: 1300, height: 1600), img: colorImg)
    }
  }
  
  @objc fileprivate func thumb(_ sender: Any) {
    if let colorImg = UIImage(named: "Bao.jpg") {
      doThumb(image: colorImg)
    }
  }
  
  @objc fileprivate func roundTrip(_ sender: Any) {
    if let colorImg = UIImage(named: "Bao.jpg") {
      roundTripTheImageToDataAndBack(image: colorImg)
    }
  }
  
  @objc fileprivate func inflectedPath(_ sender: Any) {
    
    guard let image = pushGetImage(with: { (r : CGRect) in
      guard let b = UIBezierPath.inflectedShape(numberOfInflections: 24,
        percentInflection: 1.0) else {
        return
      }
      b.fit(to: r)
      b.fill(withColor: .blue)
      b.stroke(withWidth: 4.0, andColor: .black)
    }, andSize: view.bounds.size) else {
      return
    }
   
    imageView.image = image
    imageView.contentMode = .scaleAspectFit
  }
  
  @objc fileprivate func star(_ sender: Any) {
    
    guard let image = pushGetImage(with: { (r : CGRect) in
      guard let b = UIBezierPath.starShape(numberOfInflections: 12,
        percentInflection: 8.0) else {
        return
      }
      
      b.fit(to: r)
      b.fill(withColor: .blue)
      b.stroke(withWidth: 4.0, andColor: .black)
    }, andSize: view.bounds.size) else {
      return
    }
    
    imageView.image = image
    imageView.contentMode = .scaleAspectFit
  }
  
  @objc fileprivate func polygon(_ sender: Any) {
    
    guard let image = pushGetImage(with: { (r : CGRect) in
      guard let b = UIBezierPath.polygon(numberOfSides: 12) else {
        return
      }
      
      // Let's roundtrip, to exercise the to->elements and from->elements
      // conversions
      let eles = b.elements
      for ele in eles {
        ele.showTheCode()
      }
      let b2 = UIBezierPath.path(from: eles)
      r.insetBy(dx: 20.0, dy: 20)
      b2.fit(to: r)
      b2.fill(withColor: .blue)
      b2.stroke(withWidth: 1.0, andColor: .black)
      
      //b2.showPathProgression(maxPercent: 0.6)
      
      let len = b2.pathLength
      print("polygon is \(len) points long")
      
      let pointSlope = b2.pointAndSlope(atPercent : 0.8, calculateSlope : true)
      guard let slopePoint = pointSlope.1 else {
        fatalError("Problem with slope calculation!!")
      }
      print("point at 40 percent is \(pointSlope.0), slope is \(slopePoint)")
      
      //b2.drawOuterGlow(fillColor: .red, withRadius: 15.0)
      //b2.drawInnerGlow(fillColor: .red, withRadius: 15.0)
      
    
    }, andSize: view.bounds.size) else {
      return
    }
    
    imageView.image = image
    imageView.contentMode = .scaleAspectFit
  }

  
  fileprivate func roundTripTheImageToDataAndBack(image: UIImage) {
    if let nsdata = image.bytesFromRGB() {
      if let returnedImage = UIImage.fromBytesRGB(data: nsdata,
        targetSize: image.size) {
        
        imageView.contentMode = .scaleAspectFit
        imageView.image = returnedImage
      }
    }
  }
  
  fileprivate func doThumb(image: UIImage) {
    if let thumb = image.thumb(targetSize: CGSize(width: 30, height: 40), useFitting: true) {
      imageView.contentMode = .center
      imageView.image = thumb
    }
  }

  fileprivate func watermarkTheImage(colorImg : UIImage) {
    imageView.contentMode = .scaleAspectFit
    let imgSize = colorImg.size
    let imgRect = CGRect.from(size: imgSize)
    let imgVueRect = imageView.bounds
    let targetScale = imgRect.aspectScaleFit(destSize: imgVueRect.size)
    let targetSize = imgRect.size.scale(byFactor: targetScale)
    
    if let font = UIFont(name: "HelveticaNeue", size: 48.0) {
      
      if let w = colorImg.watermarked(withText: "BaoMark ",
                                      
                                      targetSize: targetSize, font: font) {
        
        imageView.image = w
      }
    }
  }
  
  fileprivate func extractSubImage(_ rect: CGRect, img : UIImage) {
    imageView.contentMode = .scaleAspectFit
    if let subImage = img.extract(rect: rect) {
      imageView.image = subImage
    }
  }
  
}

