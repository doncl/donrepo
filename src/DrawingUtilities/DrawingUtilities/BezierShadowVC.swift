//
//  BezierShadowVC.swift
//  DrawingUtilities
//
//  Created by Don Clore on 1/30/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class BezierShadowVC: UIViewController {
  var imageView : UIImageView = UIImageView()

  
  override func loadView() {
    view = UIView()
    view.backgroundColor = .white
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = .scaleAspectFit
//    view.addSubview(imageView)
//
//    NSLayoutConstraint.activate([
//      imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
//      imageView.topAnchor.constraint(equalTo: view.topAnchor),
//      imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
//      imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
//      ])
    
    navigationItem.rightBarButtonItems = []
    navigationItem.rightBarButtonItems = [
      barItem(title: "DoShadowThing", selector: #selector(BezierShadowVC.doShadow(_:))),
    ]
  }

  
  fileprivate func barItem(title: String, selector : Selector) -> UIBarButtonItem {
    return UIBarButtonItem(title: title, style: .plain, target: self, action: selector)
  }
}

extension BezierShadowVC {
  @objc func doShadow(_ sender: UIBarButtonItem) {
    let targetSize = CGSize(width: 200, height: 350)
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    let rect = CGRect.from(size: targetSize)
    let path = UIBezierPath(rect: rect.insetBy(dx: 5.0, dy: 5.0))
    path.drawShadow(color: UIColor.black, size: .zero, blur: 7.0)
  
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
//    imageView.clipsToBounds = false
//    imageView.image = image
    
    let shadow = CALayer()
    shadow.frame = CGRect(x: 100, y: 100, width: targetSize.width, height:targetSize.height)
    shadow.contents = image.cgImage
    shadow.masksToBounds = false
    view.layer.addSublayer(shadow)
    shadow.zPosition = 10.0
//    view.layer.contents = image.cgImage
//    view.layer.masksToBounds = false
  
  }
}


