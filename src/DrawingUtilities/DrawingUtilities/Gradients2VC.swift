//
//  Gradients2VC.swift
//  DrawingUtilities
//
//  Created by Don Clore on 7/16/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class Gradients2VC: UIViewController {
  var imageView : UIImageView = UIImageView()
  
  let purple : UIColor = #colorLiteral(red: 0.3882352941, green: 0.2431372549, blue: 0.6352941176, alpha: 1)
  let green : UIColor = #colorLiteral(red: 0.4901960784, green: 0.6352941176, blue: 0.2470588235, alpha: 1)
  let darkGreen : UIColor = #colorLiteral(red: 0.1568627451, green: 0.2156862745, blue: 0.1254901961, alpha: 1)
  
  var flipExample : Int = 0
  var otherExample : Int = 0
  
  override func loadView() {
    view = UIView()
    view.backgroundColor = .white
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = .scaleAspectFit
    view.addSubview(imageView)
    
    NSLayoutConstraint.activate([
      imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      imageView.topAnchor.constraint(equalTo: view.topAnchor),
      imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
      ])
    
    navigationItem.rightBarButtonItems = []
    
    navigationItem.rightBarButtonItems = [
      barItem(title: "Flip(2)", selector: #selector(Gradients2VC.buildFlipExample(_:))),
      barItem(title: "Stroke", selector: #selector(Gradients2VC.buildStrokeExample(_:))),
      barItem(title: "FX(7)", selector: #selector(Gradients2VC.buildFXExample(_:))),
      barItem(title: "BezShad", selector: #selector(Gradients2VC.launchBezierShadowVC(_:))),
    ]
  }
  
  @objc fileprivate func buildFlipExample(_ sender: UIBarButtonItem) {
    let targetSize = CGSize(width: 400, height: 400)
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    let targetRect = CGRect(origin: .zero, size: targetSize)
    let outerRect = targetRect.insetBy(dx: 80, dy: 80)
    let innerRect = outerRect.insetBy(dx: 40, dy: 40)
    
    let outerPath = UIBezierPath(ovalIn: outerRect)
    let innerPath = UIBezierPath(ovalIn: innerRect)
    
    let c1 : UIColor = UIColor(white: 0.66, alpha: 1.0)
    let c2 : UIColor = UIColor(white: 0.33, alpha: 1.0)
    
    guard let gradient = Gradient.from(color1: c1, to: c2) else {
      return
    }
    
    pushDraw(block: {
      outerPath.addClip()
      gradient.drawTopToBottom(rect: outerRect)
    })
    
    pushDraw(block: {
      innerPath.addClip()
      gradient.drawBottomToTop(rect: innerRect)
    })
    
    let shadowColor : UIColor = UIColor(white: 0.0, alpha: 0.5)
    let shadowSize : CGSize = CGSize(width: 0.0, height: 2.0)
    innerPath.drawInnerShadow(color: shadowColor, size: shadowSize, blur: 2.0)
    let setSize : CGSize = CGSize(width: -4.0, height: 4)
    innerPath.setShadow(color: shadowColor, size: setSize, blur: 4.0)
    
    let skyColor : UIColor = UIColor(red: 0.0, green: 0.75, blue: 1.0, alpha: 1.0)
    let darkSkyColor : UIColor = skyColor.scaleBrightness(amount: 0.5)
    
    switch flipExample {
    case 0:
      break
    case 1:
      let insetRect = innerRect.insetBy(dx: 2.0, dy: 2.0)
      let bluePath = UIBezierPath(ovalIn: insetRect)
      
      // Produce an ease-in-out gradient, as in Listing 6-5
      guard let blueGradient =
        Gradient.easeInOut(between: skyColor, and: darkSkyColor) else {
          break
      }
      
      // Draw the radial gradient
      let center = insetRect.center
      let topRight = insetRect.topRight
      let width = center.distance(from: topRight)
      
      pushDraw(block: {
        bluePath.addClip()
        guard let context = UIGraphicsGetCurrentContext() else {
          return
        }
        context.drawRadialGradient(blueGradient.gradient, startCenter: center,
          startRadius: 0, endCenter: center, endRadius: width, options: [])
      })
    default:
      break
    }
    
    flipExample = (flipExample + 1) % 2
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
    imageView.image = image
  }
  
  @objc fileprivate func buildStrokeExample(_ sender: UIBarButtonItem) {
    let targetSize = CGSize(width: 400, height: 400)
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    let targetRect = CGRect(origin: .zero, size: targetSize)
    let inset = targetRect.insetBy(dx: 80, dy: 80)
    
    guard let bunny = BuildBunnyPath() else {
      return
    }
    bunny.fit(to: inset)
    
    guard let gradient = Gradient.from(color1: purple, to: green) else {
      return
    }
    
    bunny.clip(toStrokeWidth: 8)
    gradient.drawTopToBottom(rect: inset)
    
    // To see the original path
    bunny.addDashes()
    let bunnyColor : UIColor = UIColor(white: 0, alpha: 1.0)
    bunny.stroke(withWidth: 1.0, andColor: bunnyColor)
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
    
    imageView.image = image
  }
  
  @objc fileprivate func buildFXExample(_ sender: UIBarButtonItem) {
    let targetSize = CGSize(width: 400, height: 200)
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
   
    let targetRect = CGRect(origin: .zero, size: targetSize)
    let inset = targetRect.inset(byPercent: 0.15)
    
    switch otherExample {
    case 0:
      UIBezierPath.fill(rect: targetRect, withColor: UIColor.white)
      UIBezierPath.drawStrokedShadowedText(string: "Quartz 2D",
        fontFace: "Avenir-BlackOblique", baseColor: green, dest: inset)
      
    case 1:
      UIBezierPath.fill(rect: targetRect, withColor: green)
      guard let path = BuildBunnyPath() else {
        break
      }
      path.fit(to: inset)
      path.drawIndented(primary: green, rect: inset)
      
    case 2:
      let path = UIBezierPath(roundedRect: inset, cornerRadius: 32)
      path.addClip()
      path.fill(withColor: .black)
      guard let gradient = Gradient.rainbow() else {
        break
      }
      let embossColor : UIColor = UIColor(white: 0.0, alpha: 0.5)
      path.emboss(color: embossColor, radius: 2, blur: 2)
      guard let agate = UIImage(named: "agate_small.jpg") else {
        break
      }
      path.draw(overTexture: agate, usingGradient: gradient, withAlpha: 0.5)
      
    case 3:
      let targetColor = darkGreen
      let path = UIBezierPath(roundedRect: inset, cornerRadius: 32)
      path.addClip()
      path.fillWithNoise(fillColor: targetColor)
      guard let gradient = Gradient.linearGloss(color: targetColor) else {
        break
      }
      gradient.drawTopToBottom(rect: inset)
    
    case 4:
      let targetColor = darkGreen
      let path = UIBezierPath(roundedRect: inset, cornerRadius: 32)
      path.fill(withColor: targetColor)
      path.addClip()
      guard let gradient = Gradient.linearGloss(color: targetColor) else {
        break
      }
      gradient.drawTopToBottom(rect: inset)
      
      guard let text = UIBezierPath.from(string: "Button",
        withFontFace: "HelveticaNeue") else {
          
        break
      }
      let insetPercent = inset.inset(byPercent: 0.4)
      text.fit(to: insetPercent)
      text.fill(withColor: .white)
      
    case 5:
      let targetColor = darkGreen
      let path = UIBezierPath(roundedRect: inset, cornerRadius: 32)
      path.fill(withColor: targetColor)
      let glow : UIColor = UIColor(white: 0.4, alpha: 1.0)
      path.drawBottomGlow(color: glow, percent: 0.4)
      
    case 6:
      let targetColor = darkGreen
      let path = UIBezierPath(roundedRect: inset, cornerRadius: 32)
      path.fill(withColor: targetColor)
      
      guard let text = UIBezierPath.from(string: "Button",
        withFontFace: "HelveticaNeue") else {
        break
      }
      
      let percentInset = inset.inset(byPercent: 0.4)
      text.fit(to: percentInset)
      text.fill(withColor: .white)
      
      let glow : UIColor = UIColor(white: 0.4, alpha: 1.0)
      path.drawBottomGlow(color: glow, percent: 0.4)
      path.drawIconTopLight(p: 0.45)
      
    default:
      break
    }
    
    otherExample = (otherExample + 1) % 7
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
    imageView.image = image
  }
  
  @objc fileprivate func launchBezierShadowVC(_ sender: UIBarButtonItem) {
    guard let nav = navigationController else {
      return
    }
    let vc = BezierShadowVC(nibName: nil, bundle: nil)
    nav.pushViewController(vc, animated: true)
  }
    
    
  fileprivate func barItem(title: String, selector : Selector) -> UIBarButtonItem {
    return UIBarButtonItem(title: title, style: .plain, target: self, action: selector)
  }
  
}
