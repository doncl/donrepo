//
//  CGRectExtensions.swift
//  DrawingUtilities
//
//  Created by Don Clore on 6/30/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.
//
import UIKit

extension CGRect {
  //MARK: general Geometry
  var center : CGPoint {
    return CGPoint(x: midX, y: midY)
  }
  
  //MARK: Rectangle construction
  static func from(size: CGSize) -> CGRect {
    return CGRect(origin: .zero, size: size)
  }
  
  static func pointsMakeRect(p1 : CGPoint, p2: CGPoint) -> CGRect {
    let rect = CGRect(x: p1.x, y: p1.y, width: p2.x - p1.x, height: p2.y - p1.y)
    return rect.standardized
  }
  
  static func originMakeRect(origin : CGPoint) -> CGRect {
    return CGRect(origin: origin, size: .zero)
  }
  
  static func rectAroundCenter(center : CGPoint, size: CGSize) -> CGRect {
    let halfWidth = size.width / 2.0
    let halfHeight = size.height / 2.0
    
    return CGRect(x: center.x - halfWidth, y: center.y - halfHeight, width: size.width,
      height: size.height)
  }
  
  func inset(byPercent percent : CGFloat) -> CGRect {
    let wInset = size.width * (percent / 2.0)
    let yInset = size.height * (percent / 2.0)
    return insetBy(dx: wInset, dy: yInset)
  }
  
  func centeredInRect(mainRect: CGRect) -> CGRect {
    let dx = mainRect.midX - midX
    let dy = mainRect.midY - midY
    return offsetBy(dx: dx, dy: dy)
  }
  
  //MARK: Cardinal points
  var topLeft : CGPoint {
    return CGPoint(x: minX, y: minY)
  }
  
  var topRight : CGPoint {
    return CGPoint(x: maxX, y: minY)
  }
  
  var bottomLeft : CGPoint {
    return CGPoint(x: minX, y: maxY)
  }
  
  var bottomRight : CGPoint {
    return CGPoint(x: maxX, y: maxY)  //MARK: Linear
  }
  
  var midTop : CGPoint {
    return CGPoint(x: midX, y: minY)
  }
  
  var midBottom : CGPoint {
    return CGPoint(x: midX, y: maxY)
  }
  
  var midLeft : CGPoint {
    return CGPoint(x: minX, y: midY)
  }
  
  var midRight : CGPoint {
    return CGPoint(x: maxX, y: midY)
  }
  
  //MARK: Point location
  func pointAtPercents(xPercent : CGFloat, yPercent : CGFloat) -> CGPoint {
    let dx = xPercent * size.width
    let dy = yPercent * size.height
    return CGPoint(x: origin.x + dx, y: origin.y + dy)
  }
  
  //MARK: Aspect and fitting
  func getScale(to destRect: CGRect) -> CGSize {
    let scaleW = size.width == 0 ? 0 : destRect.size.width / width
    let scaleH = size.height == 0 ? 0 : destRect.size.height / height
    
    return CGSize(width: scaleW, height: scaleH)
  }
  
  // What do we have to scale by to make it fill (Sadun's definition of 'fill')?
  func aspectScaleFill(destSize : CGSize) -> CGFloat {
    let dimsScale = getDimensionsScale(destSize: destSize)
    
    return max(dimsScale.0, dimsScale.1)
  }
  
  // What do we have to scale by to make it fit (with letterboxing or pillarboxing)?
  func aspectScaleFit(destSize: CGSize) -> CGFloat {
    let dimsScale = getDimensionsScale(destSize: destSize)
    
    return min(dimsScale.0, dimsScale.1)
  }
  
  fileprivate func getDimensionsScale(destSize : CGSize) -> (CGFloat, CGFloat) {
    let scaleW = size.width == 0 ? 0 : destSize.width / size.width
    let scaleH = size.height == 0 ? 0 : destSize.height / size.height

    return (scaleW, scaleH)
  }
  
  func fit(into destRect : CGRect) -> CGRect {
    let aspect = aspectScaleFit(destSize: destRect.size)
    let targetSize = size.scale(byFactor: aspect)
    return CGRect.rectAroundCenter(center: destRect.center, size: targetSize)
  }
  
  // This is 'fill' in the Erica Sadun sense, not the Core Graphics sense.
  func fill(into destRect: CGRect) -> CGRect {
    let aspect = aspectScaleFill(destSize: destRect.size)
    let targetSize = size.scale(byFactor: aspect)
    return CGRect.rectAroundCenter(center: destRect.center, size: targetSize)
  }
}
