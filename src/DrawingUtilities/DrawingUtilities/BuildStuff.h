//
//  BuildStuff.h
//  DrawingUtilities
//
//  Created by Don Clore on 7/9/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

#ifndef BuildStuff_h
#define BuildStuff_h

@import UIKit;
@import QuartzCore;

UIBezierPath *BuildBunnyPath();
UIBezierPath *BuildMoofPath();
UIBezierPath *BuildStarPath();


#endif /* BuildStuff_h */
