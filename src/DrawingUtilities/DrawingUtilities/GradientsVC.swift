//
//  GradientsVC.swift
//  DrawingUtilities
//
//  Created by Don Clore on 7/9/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit
import QuartzCore
import CoreGraphics

class GradientsVC: UIViewController {
  var layerExample : Int = 0
  var baseGradientExample : Int = 0
  var easeGradientExample : Int = 0
  
  let purple : UIColor = #colorLiteral(red: 0.3882352941, green: 0.2431372549, blue: 0.6352941176, alpha: 1)
  let green : UIColor = #colorLiteral(red: 0.4901960784, green: 0.6352941176, blue: 0.2470588235, alpha: 1)
  let imageView : UIImageView = UIImageView()
  
  init(frame: CGRect) {
    super.init(nibName: nil, bundle: nil)
    view.frame = frame
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func loadView() {
    view = UIView()
    view.backgroundColor = .white
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = .scaleAspectFit
    view.addSubview(imageView)
    
    NSLayoutConstraint.activate([
      imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      imageView.topAnchor.constraint(equalTo: view.topAnchor),
      imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    ])

    navigationItem.rightBarButtonItems = []
    
    navigationItem.rightBarButtonItems = [
      barItem(title: "Gradients2", selector: #selector(GradientsVC.pushGradients2VC(_:))),
      barItem(title: "Layer(2)", selector: #selector(GradientsVC.buildLayerExample(_:))),
      barItem(title: "Grad(9)", selector: #selector(GradientsVC.buildBaseGradientExample(_:))),
      barItem(title: "Ease(4)", selector: #selector(GradientsVC.buildEaseGradientExample(_:))),
      barItem(title: "Edge", selector: #selector(GradientsVC.buildEdgeGradientExample(_:))),
    ]
  }
  
  @objc fileprivate func pushGradients2VC(_ sender: UIBarButtonItem) {
    guard let nav = navigationController else {
      return
    }
    let g2 = Gradients2VC(nibName:  nil, bundle: nil)
    nav.pushViewController(g2, animated: true)
  }
  
  @objc fileprivate func buildLayerExample(_ sender : UIBarButtonItem) {
    let targetSize = CGSize(width: 400.0, height: 400.0)
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    let targetRect = CGRect(origin: .zero, size: targetSize)
    let inset = targetRect.insetBy(dx: 80.0, dy: 80.0)
    
    guard let path = BuildStarPath() else {
      return
    }
    path.fit(to: inset)
    
    path.moveCenter(to: inset.center)
    let shadowColor = UIColor(white: 0.0, alpha: 0.5)
    let size = CGSize(width: -4.0, height: 4.0)
    let blur : CGFloat = 4.0
    path.setShadow(color: shadowColor, size: size, blur: blur)
    
    if layerExample == 0 {
      path.fill(withColor: purple)
      path.offset(by: CGSize(width: 50.0, height: 0.0))
      path.fill(withColor: green)
      
    } else if layerExample == 1 {
      pushDraw(block: {
        path.fill(withColor: self.purple)
        path.offset(by: CGSize(width: 50.0, height: 0.0))
        path.fill(withColor: self.green)
      })
    }
    
    layerExample = (layerExample + 1) % 2
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
    imageView.image = image
  }
  
 
  @objc fileprivate func buildBaseGradientExample(_ sender: UIBarButtonItem) {
    let targetSize = CGSize(width: 400.0, height: 400.0)
    
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    let targetRect = CGRect(origin: .zero, size: targetSize)
    let inset = targetRect.insetBy(dx: 80.0, dy: 80.0)
    let insetter = inset.insetBy(dx: 100.0, dy: 100.0)
    let p0 : CGPoint = insetter.origin
    let p1 : CGPoint = insetter.bottomRight
    
    switch baseGradientExample {
    case 0:
      guard let gradient = Gradient.rainbow() else {
        return
      }
      gradient.drawLeftToRight(rect: inset)
      
    case 1:
      let c1 = UIColor(white: 1.0, alpha: 1.0)
      let c2 = UIColor(white: 0.0, alpha: 1.0)
      guard let gradient = Gradient.from(color1: c1, to: c2) else {
        break
      }
      gradient.drawLeftToRight(rect: inset)
    
    case 2:
      let c1 = UIColor(white: 1.0, alpha: 1.0)
      let c2 = UIColor(white: 0.0, alpha: 1.0)
      guard let gradient = Gradient.from(color1: c1, to: c2) else {
        break
      }
      let center = inset.center
      let midRight = inset.midRight
      
      gradient.drawRadial(from: center, to: midRight)
      
    case 3:
      guard let gradient = Gradient.from(color1: green, to: purple) else {
        break
      }
      gradient.draw(from: p0, to: p1)
      
    case 4:
      guard let gradient = Gradient.from(color1: green, to: purple) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: .drawsBeforeStartLocation)
      
    case 5:
      guard let gradient = Gradient.from(color1: green, to: purple) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: .drawsAfterEndLocation)

    case 6:
      guard let gradient = Gradient.from(color1: green, to: purple) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: [.drawsBeforeStartLocation, .drawsAfterEndLocation])
      
    case 7:
      guard let gradient = Gradient.from(color1: green, to: purple) else {
        break
      }
      let radii : CGPoint = CGPoint(x: 50.0, y: 100.0)
      gradient.drawRadial(from: p0, to: p1, radii: radii, style: [])
      
    case 8:
      guard let gradient = Gradient.from(color1: green, to: purple) else {
        break
      }
      let radii = CGPoint(x: 50.0, y: 100.0)
      gradient.drawRadial(from: p0, to: p1, radii: radii,
        style: [.drawsBeforeStartLocation, .drawsAfterEndLocation])
    default:
      break
    }
    
    baseGradientExample = (baseGradientExample + 1) % 9
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
    
    imageView.image = image
  }
  
  @objc fileprivate func buildEaseGradientExample(_ sender: UIBarButtonItem) {
    let targetSize = CGSize(width: 400.0, height: 400.0)
    
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    let targetRect = CGRect(origin: .zero, size: targetSize)
    let inset = targetRect.insetBy(dx: 80.0, dy: 80.0)
    let p = UIBezierPath(roundedRect: inset, cornerRadius: 32.0)
    p.fill(withColor: green)
    p.addClip()
    
    let p0 = inset.pointAtPercents(xPercent: 0.7, yPercent: 0.5)
    let p1 = inset.pointAtPercents(xPercent: 1.0, yPercent: 0.5)
    
    let c1 : UIColor = UIColor(white: 0.0, alpha: 0.0)
    let c2 : UIColor = UIColor(white: 0.0, alpha: 1.0)
    
    
    switch easeGradientExample {
    case 0:
      guard let gradient = Gradient.from(color1: c1, to: c2) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: [])
      
    case 1:
      guard let gradient = Gradient.easeIn(between: c1, and: c2) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: [])
     
    case 2:
      guard let gradient = Gradient.easeInOut(between: c1, and: c2) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: [])
      
    case 3:
      guard let gradient = Gradient.easeOut(between: c1, and: c2) else {
        break
      }
      gradient.draw(from: p0, to: p1, style: [])
      
    default:
      break
    }
    
    easeGradientExample = (easeGradientExample + 1) % 4
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
    imageView.image = image
  }
  
  @objc fileprivate func buildEdgeGradientExample(_ sender: UIBarButtonItem) {
    let targetSize = CGSize(width: 400.0, height: 400.0)
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    let targetRect = CGRect(origin: .zero, size: targetSize)
    let inset = targetRect.insetBy(dx: 80.0, dy: 80.0)
    
    let p = UIBezierPath(ovalIn: inset)
    p.fill(withColor: green)
    p.addClip()
    
    let block : InterpolationBlock = {( percent : CGFloat) in
      let skippingPercent : CGFloat = 0.75
      if percent < skippingPercent {
        return 0
      }
      let scaled = (percent - skippingPercent) * (CGFloat(1.0) / (CGFloat(1.0) - skippingPercent))
      return sin(scaled * CGFloat.pi)
    }
    
    let c1 : UIColor = UIColor(white: 0.0, alpha: 0.0)
    let c2 : UIColor = UIColor(white: 0.0, alpha: 1.0)
    
    guard let gradient = Gradient.using(interpolationBlock: block, between: c1, and: c2) else {
      return
    }
    let center = inset.center
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    context.drawRadialGradient(gradient.gradient, startCenter: center, startRadius: 0,
      endCenter: center, endRadius: inset.width / 2.0, options: [])
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
    imageView.image = image
  }
  
  fileprivate func barItem(title: String, selector : Selector) -> UIBarButtonItem {
    return UIBarButtonItem(title: title, style: .plain, target: self, action: selector)
  }
}












































