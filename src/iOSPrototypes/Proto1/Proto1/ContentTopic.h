//
// Created by Don Clore on 4/22/15.
// Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ContentTopic : NSObject
@property (weak, nonatomic) NSString * name;
@property (weak, nonatomic) NSArray *children;
@end