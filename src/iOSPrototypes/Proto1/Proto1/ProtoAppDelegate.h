//
//  ProtoAppDelegate.h
//  Proto1
//
//  Created by Don Clore on 4/22/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProtoAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

