//
//  main.m
//  Proto1
//
//  Created by Don Clore on 4/22/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProtoAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ProtoAppDelegate class]));
    }
}
