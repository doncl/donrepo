//
//  UITraitCollection+IsBig.m
//  ObjCLingo
//
//  Created by Don Clore on 8/5/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import "UITraitCollection+IsBig.h"

@implementation UITraitCollection (IsBig)
- (BOOL)isBig {
  return self.horizontalSizeClass == UIUserInterfaceSizeClassRegular &&
    self.verticalSizeClass == UIUserInterfaceSizeClassRegular;
}
@end
