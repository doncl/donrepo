//
//  LetterCell.h
//  ObjCLingo
//
//  Created by Don Clore on 8/4/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LetterCell : UICollectionViewCell
@property (readwrite, strong, nonatomic) UILabel *label;
- (void)setSelected:(BOOL)blueCircle amberBackground:(BOOL)amberBackground;
@end
