//
//  TargetWordCell.m
//  ObjCLingo
//
//  Created by Don Clore on 8/5/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import "TargetWordCell.h"

@implementation TargetWordCell

+ (UIFont *)preferredFont {
  return [UIFont boldSystemFontOfSize:14.0];
}

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.checkMark = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"done"]];
    if (!self.checkMark) {
      return nil;
    }
    
    self.checkMark.hidden = YES;
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:self.checkMark];
    
    self.label = [UILabel new];
    [self addSubview:self.label];
    self.label.font = [TargetWordCell preferredFont];
    self.label.textColor = [UIColor whiteColor];
    self.label.textAlignment = NSTextAlignmentLeft;
    
    self.checkMark.translatesAutoresizingMaskIntoConstraints = NO;
    self.label.translatesAutoresizingMaskIntoConstraints = NO;
    
    CGFloat horzPad = 5.0;
    CGFloat vertPad = 3.0;
    CGSize checkSize = CGSizeMake(10, 10);
    
    NSArray<NSLayoutConstraint *> *constraints = @[
      [self.checkMark.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:horzPad],
      [self.checkMark.centerYAnchor constraintEqualToAnchor:self.label.centerYAnchor],
      [self.checkMark.widthAnchor constraintEqualToConstant:checkSize.width],
      [self.checkMark.heightAnchor constraintEqualToConstant:checkSize.height],
      
      [self.label.leadingAnchor constraintEqualToAnchor:self.checkMark.trailingAnchor constant:horzPad],
      [self.label.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:vertPad],
    ];
    
    [NSLayoutConstraint activateConstraints:constraints];
  }
  return self;
}
@end
