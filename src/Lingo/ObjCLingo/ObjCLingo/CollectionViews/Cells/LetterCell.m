//
//  LetterCell.m
//  ObjCLingo
//
//  Created by Don Clore on 8/4/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import "LetterCell.h"


@interface LetterCell()
@property (nonatomic, readwrite, strong) CAShapeLayer * _Nullable lightBorder;
@property (nonatomic, readwrite, strong) CAShapeLayer * _Nullable darkBorder;
@property (nonatomic, readwrite, strong) CAShapeLayer * _Nullable inProcessSelectionLayer;
@property (nonatomic, readwrite, strong) UIColor * _Nullable greyBackground;
@property (nonatomic, readwrite, strong) UIColor * _Nullable selectedColor;
@property (nonatomic, readwrite, strong) UIColor * _Nullable lightBorderColor;
@property (nonatomic, readwrite, strong) UIColor * _Nullable darkBorderColor;
@end

@implementation LetterCell
- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.label = [UILabel new];
    self.lightBorder = [CAShapeLayer new];
    self.darkBorder = [CAShapeLayer new];
    self.inProcessSelectionLayer = [CAShapeLayer new];
    
    self.selectedColor = [UIColor colorNamed:@"Selected"];
    
    CGFloat lightVal = 238.0 / 255.0;
    CGFloat darkVal = 208.0 / 255.0;
    
    self.lightBorderColor = [UIColor colorWithRed:lightVal green:lightVal blue:lightVal alpha:1.0];
    self.darkBorderColor = [UIColor colorWithRed:darkVal green:darkVal blue:darkVal alpha:1.0];
    
    self.greyBackground = [UIColor colorNamed:@"LetterBackground"];
    self.backgroundColor = self.greyBackground;
    
    [self.contentView addSubview:self.label];
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    self.label.textColor = [UIColor blackColor];
    
    [self.layer addSublayer:self.darkBorder];
    [self.layer addSublayer:self.lightBorder];
    
    [self.contentView.layer insertSublayer:self.inProcessSelectionLayer below: self.label.layer];
    self.inProcessSelectionLayer.hidden = YES;
    self.inProcessSelectionLayer.fillColor = [UIColor colorNamed:@"InProcessSelection"].CGColor;
  }
  return self;
}

// Core Animation layers have no concept of auto-layout, so we just position the frames
// during layoutSubviews

- (void)layoutSubviews {
  [super layoutSubviews];
  
  self.label.frame = self.contentView.bounds;
  
  CGFloat bottom = self.contentView.bounds.size.height;
  CGFloat width = self.contentView.bounds.size.width;
  
  UIBezierPath *path = [UIBezierPath new];
  [path moveToPoint:CGPointMake(0, 0)];
  [path addLineToPoint:CGPointMake(0, bottom)];
  [path addLineToPoint:CGPointMake(width, bottom)];
  
  [self setupPath:path shapeLayer: self.darkBorder color:self.darkBorderColor];
  
  path = [UIBezierPath new];
  [path moveToPoint:CGPointMake(1.0, 0)];
  [path addLineToPoint:CGPointMake(1.0, bottom - 1.0)];
  [path addLineToPoint:CGPointMake(width, bottom - 1.0)];
  
  [self setupPath:path shapeLayer:self.lightBorder color:self.lightBorderColor];
}

- (void)setupPath:(UIBezierPath *)path shapeLayer: (CAShapeLayer *)shapeLayer color: (UIColor *)color {
  shapeLayer.path = path.CGPath;
  shapeLayer.strokeColor = color.CGColor;
  shapeLayer.lineWidth = 1.0;
  shapeLayer.lineJoin = kCALineJoinMiter;
  shapeLayer.lineCap = kCALineCapButt;
  shapeLayer.fillColor = [UIColor clearColor].CGColor;
  shapeLayer.backgroundColor = color.CGColor;
}

- (void)setSelected:(BOOL)blueCircle amberBackground:(BOOL)amberBackground {
  NSAssert(blueCircle && !amberBackground || !blueCircle && amberBackground ||
           !blueCircle && !amberBackground, @"Broken assumptions");

  if (!blueCircle) {
    self.inProcessSelectionLayer.hidden = YES;
    self.label.textColor = [UIColor blackColor];
  } else {
    CGFloat fivePercent = self.contentView.bounds.size.width * 0.05;
    CGRect pathRect = CGRectInset(self.contentView.bounds, fivePercent, fivePercent);
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:pathRect];
    self.inProcessSelectionLayer.path = path.CGPath;
    self.inProcessSelectionLayer.hidden = NO;
    self.label.textColor = [UIColor whiteColor];
  }
  
  if (!amberBackground) {
    self.backgroundColor = self.greyBackground;
    self.label.textColor = [UIColor blackColor];
  } else {
    self.backgroundColor = self.selectedColor;
    self.label.textColor = [UIColor whiteColor];
  }
  
  [self setNeedsDisplay];
}
@end
