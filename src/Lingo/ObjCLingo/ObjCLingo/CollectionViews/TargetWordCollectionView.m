//
//  TargetWordCollectionView.m
//  ObjCLingo
//
//  Created by Don Clore on 8/1/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//


#import "TargetWordCollectionView.h"
#import "TargetWordCell.h"
#import "TargetCollectionHeader.h"

NSString *targetCellId = @"targetCellId";
NSString *headerId = @"targetCollectionHeaderId";

@interface TargetWordCollectionView()
@property (nonatomic, strong) NSArray<NSString *> *targetWords;
@property (nonatomic, readwrite) UICollectionViewFlowLayout * _Nullable flow;

@end

@implementation TargetWordCollectionView

@synthesize wordGrid = _wordGrid;

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    self.targetWords = [NSArray<NSString *> new];
    NSAssert([self.collectionViewLayout isKindOfClass:[UICollectionViewFlowLayout class]],
             @"Broken assumptions");
    
    self.flow = (UICollectionViewFlowLayout *)self.collectionViewLayout;
    self.flow.headerReferenceSize = CGSizeMake(0, 87);
    self.flow.sectionInset = UIEdgeInsetsMake(10, 0, 0, 0);
    
    self.backgroundColor = [UIColor purpleColor];
    self.dataSource = self;
    
    self.contentInset = UIEdgeInsetsMake(5, 10, 5, 10);
    
    [self registerClass:[TargetWordCell class] forCellWithReuseIdentifier:targetCellId];
    
        [self registerClass:[TargetCollectionHeader class]
  forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
        withReuseIdentifier:headerId];
  }
  
  return self;
}

- (WordGrid *)wordGrid {
  return _wordGrid;
}

- (void)setWordGrid:(WordGrid *)newWordGrid {
  _wordGrid = newWordGrid;
  if (!newWordGrid) {
    return;
  }
  NSDictionary<NSString *, NSString *> *wordLocationDictionary = newWordGrid.wordLocations;
  if (!wordLocationDictionary) {
    return;
  }
  self.targetWords = wordLocationDictionary.allValues;
  [self updateCellSize];
  [self reloadSections:[[NSIndexSet alloc] initWithIndex:0]];
  
}

- (void)updateCellSize {
  NSAssert(self.flow, @"Broken assumptions");
  if (!self.targetWords.count) {
    return;
  }
  CGFloat largestWidth = 0;
  CGFloat largestHeight = 0;
  NSDictionary *attrs = @{NSFontAttributeName : [TargetWordCell preferredFont]};

  for (NSString *targetWord in self.targetWords) {
    CGSize size = [targetWord sizeWithAttributes:attrs];
    largestWidth = MAX(size.width, largestWidth);
    largestHeight = MAX(size.height, largestHeight);
  }
  UIEdgeInsets inset = self.contentInset;
  CGFloat width = self.frame.size.width - (inset.left + inset.right);
  CGFloat maxTwoColWidth = width / 2 - self.flow.minimumLineSpacing;
  
  CGSize itemSize;
  if (largestWidth > maxTwoColWidth) {
    itemSize = CGSizeMake(largestWidth, largestHeight);
  } else {
    // Two Columns
    itemSize = CGSizeMake(maxTwoColWidth, largestHeight);
  }
  self.flow.itemSize = itemSize;
  [self.flow invalidateLayout];
}

- (void)weHaveAWinner:(NSString *)winner {
  NSUInteger index = [self.targetWords indexOfObject:winner];
  if (NSNotFound == index) {
    NSLog(@"Couldn't find target word %@", winner);
    return;
  }
  
  NSIndexPath *indexPath = [[NSIndexPath alloc] initWithIndex:index];
  [self selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionTop];
  UICollectionViewCell *cell = [self cellForItemAtIndexPath:indexPath];
  if (cell && [cell isKindOfClass:[TargetWordCell class]]) {
    TargetWordCell *tgtCell = (TargetWordCell *)cell;
    tgtCell.checkMark.hidden = NO;
    [tgtCell setNeedsDisplay];
  }
}

- (void)removeCheckmarkFromAllCells {
  for (UICollectionViewCell *cell in self.visibleCells) {
    if ([cell isKindOfClass:[TargetWordCell class]]) {
      TargetWordCell *tgtWordCell = (TargetWordCell *)cell;
      tgtWordCell.checkMark.hidden = YES;
    }
  }
}

#pragma mark - UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
  
  return self.targetWords.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  
  NSAssert(self.targetWords.count > indexPath.item, @"Broken assumptions");
  TargetWordCell *tgtWordCell = [collectionView dequeueReusableCellWithReuseIdentifier:targetCellId
                                                                         forIndexPath:indexPath];
  tgtWordCell.label.text = [self.targetWords[indexPath.item] uppercaseString];
  return tgtWordCell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
  
  
  TargetCollectionHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind: kind
                                                                      withReuseIdentifier:headerId forIndexPath:indexPath];
  
  if (self.wordGrid) {
    if (self.wordGrid.word) {
      header.wordValLabel.text = [self.wordGrid.word uppercaseString];
    }
    if (self.wordGrid.sourceLanguage) {
      header.sourceLangValLabel.text = [self.wordGrid.sourceLanguage uppercaseString];
    }
    if (self.wordGrid.targetLanguage) {
      header.targetLangValLabel.text = [self.wordGrid.targetLanguage uppercaseString];
    }
  }
  [header setNeedsLayout];
  return header;
}
@end
