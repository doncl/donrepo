//
//  WordGridCollectionView.m
//  ObjCLingo
//
//  Created by Don Clore on 8/1/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import "WordGridCollectionView.h"
#import "LetterCell.h"

const CGFloat borderWidth = 0.5;
NSString * _Nonnull letterCellId = @"letterCellId";

@interface WordGridCollectionView ()
@property (nonatomic, readwrite) NSUInteger numLetters;
@property (nonatomic, readwrite) NSUInteger numRows;
@property (nonatomic, readwrite) NSUInteger numCols;
@property (nonatomic, readwrite) UICollectionViewFlowLayout * _Nullable layout;

@property (nonatomic, readwrite) NSMutableArray<CellCoords *> * _Nullable selectedCells;
@property (nonatomic, readwrite) CellCoords * _Nullable touchDownCell;
@property (nonatomic, readwrite) NSArray<TargetWordInfo *> * _Nullable tgtWordInfos;
@end

@implementation WordGridCollectionView

@synthesize wordGrid = _wordGrid;

- (instancetype)init {
  self = [super init];
  if (self) {
    [self commonInit];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self commonInit];
  }
  return self;
}

- (void)commonInit {
  self.numLetters = 0;
  self.numRows = 0;
  self.numCols = 0;
  self.selectedCells = [NSMutableArray<CellCoords *> new];
  self.layout = (UICollectionViewFlowLayout *)self.collectionViewLayout;
  self.tgtWordInfos = nil;
  self.touchDownCell = nil;
  
  [self registerClass:[LetterCell class] forCellWithReuseIdentifier:letterCellId];
  self.layout = [self setFlowLayoutValues];
  
  self.dataSource = self;
  
  UIColor *gridBorderColor = [UIColor colorNamed:@"WordGridBorder"];
  if (gridBorderColor) {
    self.layer.borderColor = gridBorderColor.CGColor;
    self.layer.borderWidth = borderWidth;
  }
  
  self.userInteractionEnabled = YES;
  self.multipleTouchEnabled = NO;
}

- (void)layoutSubviews {
  [super layoutSubviews];
}

- (void)updateCellSize {
  if (!self.numLetters || !self.numRows || !self.numCols) {
    return;
  }
  
  CGFloat width = self.frame.size.width;
  CGFloat height = self.frame.size.height;
  
  CGSize cellSize = CGSizeMake(width / self.numCols, height / self.numRows);
  if (cellSize.width > cellSize.height) {
    cellSize.width = cellSize.height;
  } else if (cellSize.height > cellSize.width) {
    cellSize.height = cellSize.width;
  }
  
  self.layout.itemSize = cellSize;

  [self.layout invalidateLayout];
}

- (void)transitioning {
  if (!self.touchDownCell) {
    return;
  }
  // We're in the middle of a selection.
  [self selectCells:NO finalSelect:NO];
  [self.selectedCells removeAllObjects];
  self.touchDownCell = nil;
}

- (WordGrid *)wordGrid {
  return _wordGrid;
}

- (void)setWordGrid:(WordGrid *)newWordGrid {
  _wordGrid = newWordGrid;
  
  if (!newWordGrid) {
    return;
  }
  
  if (!newWordGrid.characterGrid || !newWordGrid.characterGrid.count) {
    return;
  }
  
  NSArray<TargetWordInfo *> *targetWordInfos = [newWordGrid getTargetWordInfos];
  if (!targetWordInfos) {
    return;
  }
  
  NSUInteger count = 0;
  NSArray<NSArray<NSString *> *> *characterRows = newWordGrid.characterGrid;
  for (NSArray *row in characterRows) {
    count += row.count;
  }
  self.numLetters = count;
  
  self.numRows = characterRows.count;
  self.numCols = characterRows[0].count;
  
  if (self.numLetters == 0 || self.numRows == 0 || self.numCols == 0) {
    return;
  }
  
  [self updateCellSize];
  self.tgtWordInfos = targetWordInfos;
}

- (UICollectionViewFlowLayout *)setFlowLayoutValues {
  NSAssert([self.collectionViewLayout isKindOfClass:[UICollectionViewFlowLayout class]], @"broken Assumptions");
  
  UICollectionViewFlowLayout *flow = (UICollectionViewFlowLayout *)self.collectionViewLayout;
  flow.minimumLineSpacing = 0;
  flow.minimumInteritemSpacing = 0;
  flow.sectionInset = UIEdgeInsetsZero;
  flow.scrollDirection = UICollectionViewScrollDirectionVertical;
  return flow;
}
- (UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
  NSAssert(self.numLetters > indexPath.item, @"Broken assumptions");
  NSArray<NSArray<NSString *> *> *characterGrid = self.wordGrid.characterGrid;
  NSAssert(characterGrid && self.numCols > 0 && self.numRows > 0, @"Broken assumptions");
  
  LetterCell *cell = (LetterCell *)[collectionView dequeueReusableCellWithReuseIdentifier:letterCellId forIndexPath:indexPath];
  NSUInteger rowNum = indexPath.item / self.numRows;
  NSUInteger colNum = indexPath.item % self.numCols;
  NSString *letter = characterGrid[rowNum][colNum];
  cell.label.text = [letter uppercaseString];
  return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return self.numLetters;
}

- (void)selectCells:(BOOL)inProcessSelect finalSelect:(BOOL)finalSelect {
  for (CellCoords *cell in self.selectedCells) {
    NSUInteger item = (cell.row * self.numCols) + cell.col;
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:0];
    LetterCell *letterCell = (LetterCell *)[self cellForItemAtIndexPath:indexPath];
    if (letterCell) {
      [letterCell setSelected:inProcessSelect amberBackground:finalSelect];
    }
  }
}

- (void)clearAll {
  for (UICollectionView * cell in self.visibleCells) {
    LetterCell *letterCell = (LetterCell *)cell;
    if (letterCell) {
      [letterCell setSelected:NO amberBackground:NO];
    }
  }
}

- (void)clearInProcessSelection {
  [self selectCells:NO finalSelect:NO];
  [self.selectedCells removeAllObjects];
}

#pragma mark - Gesture stuff
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
  if (self.touchDownCell) {
    return;
  }
  UITouch *touch = touches.anyObject;
  if (!touch) {
    return;
  }
  CGPoint location = [touch locationInView:self];
  CellCoords *colRow = [self hitTest:location];
  if (!colRow) {
    return;
  }
  self.touchDownCell = colRow;
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
  if (!self.touchDownCell) {
    return;
  }
  UITouch *touch = touches.anyObject;
  if (!touch) {
    return;
  }
  CGPoint location = [touch locationInView:self];

  if (false == CGRectContainsPoint(self.bounds, location)) {
    return;
  }
  [self hitTestAndProcess:location touchesEnded:NO];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
  UITouch *touch = touches.anyObject;
  NSAssert(touch, @"Assumptions broken");
  CGPoint location = [touch locationInView:self];
  
  if (CGRectContainsPoint(self.bounds, location)) {
    [self hitTestAndProcess:location touchesEnded:YES];
  } else {
    [self selectCells:NO finalSelect:NO];
  }
  
  NSLog(@"TouchesEnded - selectedCells = %@", self.selectedCells);
  self.touchDownCell = nil;
  [self.selectedCells removeAllObjects];
}

- (CellCoords *)hitTest:(CGPoint)point {
  if (![self gridNotEmpty]) {
    return nil;
  }
  NSUInteger col = (point.x / self.bounds.size.width) * self.numCols;
  NSUInteger row = (point.y / self.bounds.size.height) * self.numRows;
  
  CellCoords *cell = [[CellCoords alloc] initWithCol:col row:row];
  for (TargetWordInfo *tgtWordInfo in self.tgtWordInfos) {
    if (tgtWordInfo.found) {
      for (CellCoords *cellCoords in tgtWordInfo.coords) {
        if (cellCoords.row == cell.row && cellCoords.col == cell.col) {
          return nil;
        }
      }
    }
  }
  return cell;
}

- (void)hitTestAndProcess:(CGPoint)point touchesEnded:(BOOL)touchesEnded {
  if (!self.touchDownCell) {
    return;
  }
  CellCoords *cellToProcess = [self hitTest:point];
  if (!cellToProcess) {
    return;
  }
  
  [self clearInProcessSelection];
  
  NSArray<CellCoords *> *vector = [CellCoords getVectorWithSrc:self.touchDownCell dest:cellToProcess];
  
  // Oh, this is pretty ugly in imperative Obj-C.   It's no more efficient in Swift, of course.
  for (TargetWordInfo *tgtWordInfo in self.tgtWordInfos) {
    if (tgtWordInfo.found) {
      for (CellCoords *cellCoord in tgtWordInfo.coords) {
        for (CellCoords *vectorCoords in vector) {
          if (cellCoord.row == vectorCoords.row && cellCoord.col == vectorCoords.col) {
            NSLog(@"Disallowing intersecting selections");
            [self clearInProcessSelection];
            return;
          }
        }
      }
    }
  }
  self.selectedCells = [vector mutableCopy];
  
  if (touchesEnded) {
    TargetWordInfo *winner = [self selectionInTarget];
    if (winner) {
      NSLog(@"We have a winner at %@", self.selectedCells);
      winner.found = YES;
      [self selectCells:NO finalSelect:YES];
      CGPoint midPointOfSelection = [self getLocationOfMiddleLetterOfSelection:self.selectedCells];
      BOOL foundAll = YES;
      for (TargetWordInfo *tgtWordInfo in self.tgtWordInfos) {
        if (!tgtWordInfo.found) {
          foundAll = NO;
          break;
        }
      }
      [self.wordGridDelegate foundWinner:winner.word midPoint:&midPointOfSelection foundAll:foundAll];
    } else {
      [self clearInProcessSelection];
    }
  }  else {
    [self selectCells:YES finalSelect:NO];
  }
}

- (TargetWordInfo *)selectionInTarget {
  for (TargetWordInfo *tgtWordInfo in self.tgtWordInfos) {
    if (tgtWordInfo.found) {
      continue;
    }
    if ([self arraysOfCoordsEqual:tgtWordInfo.coords a1:self.selectedCells]) {
      return tgtWordInfo;
    }
  }
  return nil;
}

- (BOOL)arraysOfCoordsEqual:(NSArray<CellCoords *> *)a0 a1:(NSArray<CellCoords *> *)a1 {
  NSSet *set0 = [[NSSet alloc] initWithArray:a0];
  NSSet *set1 = [[NSSet alloc] initWithArray:a1];

  return [set0 isEqualToSet:set1];
}

- (CGPoint)getLocationOfMiddleLetterOfSelection:(NSArray<CellCoords *> *)cells {
  NSAssert(cells.count, @"Broken assumptions");
  
  CellCoords *mid = cells[cells.count / 2];
  CGSize itemSize = self.layout.itemSize;
  CGFloat x = (mid.col * itemSize.width) + itemSize.width / 2;
  CGFloat y = (mid.row * itemSize.height) + itemSize.height / 2;
  return CGPointMake(x, y);
}

- (BOOL)gridNotEmpty {
  return self.wordGrid && self.numRows && self.numCols;
}
@end


