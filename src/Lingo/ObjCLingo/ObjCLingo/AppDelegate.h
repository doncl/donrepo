//
//  AppDelegate.h
//  ObjCLingo
//
//  Created by Don Clore on 8/1/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

