//
//  BaseModalPopup.swift
//  BaseModalPopup
//
//  Created by Don Clore on 5/21/18.
//  Copyright © 2018 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit

enum PointerDirection {
  case up
  case down
}

/// A base class that manages a rectangular shadows popover object with a pointer.
public class BaseModalPopup: UIViewController {
  var loaded : Bool = false
  var presenting : Bool = true
  static let pointerHeight : CGFloat = 16.0
  static let pointerBaseWidth : CGFloat = 24.0
  static let horzFudge : CGFloat = 15.0
  var origin : CGPoint
  weak var referenceView : UIView?
  private var width : CGFloat
  private var height : CGFloat
  
  let popupView : UIView = UIView()
  let pointer : CAShapeLayer = CAShapeLayer()
  
  public required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  init(origin : CGPoint, referenceView : UIView, size: CGSize) {
    
    self.origin = origin
    self.width = size.width
    self.height = size.height
    self.referenceView = referenceView
    super.init(nibName: nil, bundle: nil)
  }
  
  public override func viewDidLoad() {
    if loaded {
      return
    }
    loaded = true
    super.viewDidLoad()
    
    if let ref = referenceView {
      origin = view.convert(origin, from: ref)
    }
    
    view.addSubview(popupView)
    view.backgroundColor = .clear
    popupView.backgroundColor = .white
    popupView.layer.shadowOffset = CGSize(width: 2.0, height: 3.0)
    popupView.layer.shadowRadius = 5.0
    popupView.layer.shadowOpacity = 0.5
    
    popupView.clipsToBounds = false
    popupView.layer.masksToBounds = false
    
    view.isUserInteractionEnabled = true
    let tap = UITapGestureRecognizer(target: self,
                                     action: #selector(BaseModalPopup.tap))
    
    view.addGestureRecognizer(tap)
  }
  
  public override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    let tuple = getPopupFrameAndPointerDirection(size: view.frame.size)
    let popupFrame = tuple.0
    let direction = tuple.1
    
    let pointerPath = makePointerPath(direction: direction, popupFrame: popupFrame)
    pointer.path = pointerPath.cgPath
    pointer.fillColor = UIColor.white.cgColor
    pointer.lineWidth = 0.5
    pointer.masksToBounds = false
    popupView.layer.addSublayer(pointer)
    
    
    layout(popupFrame)
    
    let bounds = UIScreen.main.bounds
    let width = view.frame.width
    
    if bounds.width < width && width > 0 {
      let ratio = (bounds.width / width) * 0.9
      let xform = CGAffineTransform(scaleX: ratio, y: ratio)
      popupView.transform = xform
    } else {
      popupView.transform = .identity
    }
  }
  
  private func getPopupFrameAndPointerDirection(size: CGSize) -> (CGRect, PointerDirection) {
    let y : CGFloat
    let direction : PointerDirection
    if origin.y > size.height / 2 {
      y = origin.y - (height + BaseModalPopup.pointerHeight)
      direction = .down
    } else {
      y = origin.y + BaseModalPopup.pointerHeight
      direction = .up
    }
    var x = min(30.0, origin.x - BaseModalPopup.pointerBaseWidth / 2)
    x = max(x, 0)
    var rc : CGRect = CGRect(x: x, y: y, width: width, height: height)
    let rightmost = rc.origin.x + rc.width
    if origin.x > rightmost - (BaseModalPopup.pointerBaseWidth / 2) {
      let offset = origin.x - (rightmost - BaseModalPopup.pointerBaseWidth)
      rc = rc.offsetBy(dx: offset, dy: 0)
    } else if origin.x - (BaseModalPopup.pointerBaseWidth / 2) < rc.origin.x {
      let offset = rc.origin.x - (origin.x + (BaseModalPopup.pointerBaseWidth / 2))
      rc = rc.offsetBy(dx: -offset, dy: 0)
    }
    let bounds = UIScreen.main.bounds
    let popupWidth = rc.width
    if bounds.width <= popupWidth {
      rc = CGRect(x: 0, y: rc.origin.y, width: rc.width,
                  height: rc.height)
    }
    return (rc, direction)
  }
  
  fileprivate func layout(_ popupFrame: CGRect) {
    popupView.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      popupView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: popupFrame.origin.x),
      popupView.topAnchor.constraint(equalTo: view.topAnchor, constant: popupFrame.origin.y),
      popupView.widthAnchor.constraint(equalToConstant: popupFrame.width),
      popupView.heightAnchor.constraint(equalToConstant: popupFrame.height),
      ])
  }
  
  private func makePointerPath(direction: PointerDirection, popupFrame : CGRect) -> UIBezierPath {
    let path = UIBezierPath()
    path.lineJoinStyle = CGLineJoin.bevel
    
    // previous code is supposed to assure that the popupFrame is not outside the origin.
    assert(popupFrame.origin.x <= origin.x && popupFrame.origin.x + popupFrame.width > origin.x)
    
    let adjustedX = origin.x - popupFrame.origin.x
    
    if direction == .down {
      let adjustedApex = CGPoint(x: adjustedX, y: popupFrame.height + BaseModalPopup.pointerHeight - 1)
      path.move(to: adjustedApex)
      // down is up.
      let leftBase = CGPoint(x: adjustedApex.x - (BaseModalPopup.pointerBaseWidth / 2),
                             y: adjustedApex.y - BaseModalPopup.pointerHeight)
      path.addLine(to: leftBase)
      let rightBase = CGPoint(x: adjustedApex.x + (BaseModalPopup.pointerBaseWidth / 2),
                              y: adjustedApex.y - BaseModalPopup.pointerHeight)
      
      path.addLine(to: rightBase)
      path.close()
    } else {
      let adjustedApex = CGPoint(x: adjustedX,
                                 y: -BaseModalPopup.pointerHeight + 1)
      path.move(to: adjustedApex)
      
      let leftBase = CGPoint(x: adjustedApex.x - (BaseModalPopup.pointerBaseWidth / 2),
                             y: adjustedApex.y + BaseModalPopup.pointerHeight)
      path.addLine(to: leftBase)
      
      let rightBase = CGPoint(x: adjustedApex.x + (BaseModalPopup.pointerBaseWidth / 2),
                              y: adjustedApex.y + BaseModalPopup.pointerHeight)
      path.addLine(to: rightBase)
      path.close()
    }
    
    return path
  }
  
  @objc func tap() {
    dismiss(animated: true, completion: {
      self.onDismissed()
    })
  }
  
  // override in subclasses.
  func onDismissed() {
  }
}

//MARK: - rotation
extension BaseModalPopup {
  public override func willTransition(to newCollection: UITraitCollection,
                               with coordinator: UIViewControllerTransitionCoordinator) {
    
    dismiss(animated: true, completion: {
      self.onDismissed()
    })
  }
  
  public override func viewWillTransition(to size: CGSize,
                                   with coordinator: UIViewControllerTransitionCoordinator) {
    
    dismiss(animated: true, completion: {
      self.onDismissed()
    })
  }
}
