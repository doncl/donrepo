//
//  JSONCoding.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 06/27/2018
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

/// Just some boilerplate POP code to manage Codable date handling, and map error throwing to
/// failure returns.
protocol JSONCoding {
  associatedtype codableType : Codable
  
  static func jsonEncode(_ t : codableType, success: (String) -> (), failure: (String) -> ())
  static func jsonDecode(_ data: Data, success: (codableType) -> (), failure: (String) -> ())
  static func jsonDecode(_ string: String, success: (codableType) -> (), failure: (String) -> ())
}

// MARK: - Protocol extension gives this functionality to anyone who declares themself as
// implementing JSONCoding.  The only requirement is to add a 'typealias codableType = xxx'
// statement inside struct or class definition.
extension JSONCoding {
  static func jsonDecode(_ string: String, success: (codableType) -> (), failure: (String) -> ()) {
    guard let data = string.data(using: .utf8) else {
      failure("Couldn't encode \"\(string)\" to data")
      return
    }
    
    jsonDecode(data, success: success, failure: failure)
  }
  
  static func jsonDecode(_ data: Data, success: (codableType) -> (), failure: (String) -> ()) {
    do {
      let decoder = JSONDecoder()
      decoder.dateDecodingStrategy = .custom({ (decoder : Decoder) -> Date in
        let container = try decoder.singleValueContainer()
        let dateStr = try container.decode(String.self)
        guard let d = DateFormatter.tryAllCustomDateFormats(dateString: dateStr) else {
          throw DecodingError.dataCorruptedError(in: container,
            debugDescription: "\(dateStr) not parseable as date")
        }
        return d
      })
      let t = try decoder.decode(codableType.self, from: data)
      success(t)
      return
    } catch DecodingError.dataCorrupted(let context) {
      failure("Data corrupted, context = \(context)")
      return
    } catch DecodingError.keyNotFound(let codingKey, let context) {
      failure("\(codingKey) missing, context = \(context)")
      return
    } catch DecodingError.typeMismatch(let type, let context) {
      failure("type mismatch, type = \(type), context = \(context)")
      return
    } catch DecodingError.valueNotFound(let type, let context) {
      failure("type mismatch type = \(type), context = \(context)")
      return
    } catch {
      failure("unknown error")
      return
    }
  }

  static func jsonEncode(_ t : codableType, success: (String) -> (), failure: (String) -> ()) {
    let encoder = JSONEncoder()
    encoder.outputFormatting = [.sortedKeys]
    encoder.dateEncodingStrategy = .iso8601
    guard let data = try? encoder.encode(t) else {
      failure("Couldn't encode object \(t)")
      return
    }
    guard let jsonString = String(data: data, encoding: .utf8) else {
      failure("Couldn't generate json string from Data")
      return
    }
    success(jsonString)
  }
}


// MARK: - Dates are not really needed for this project; but it was easier to leave this in than
// strip out and re-test. 
extension DateFormatter {
  static let iso8601Full: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
  }()
  
  static let iso8601Shorter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
  }()
  
  static let iso8601Shortest: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
  }()
  
  // 0001-01-01T00:00:00
  static let emptyDate : DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
  }()
  
  private static var formats : [DateFormatter] = [iso8601Full, iso8601Shorter, iso8601Shortest,
    emptyDate]
  
  static func tryAllCustomDateFormats(dateString: String) -> Date? {
    for format in formats {
      if let d = format.date(from: dateString) {
        return d
      }
    }
    return nil
  }
}
