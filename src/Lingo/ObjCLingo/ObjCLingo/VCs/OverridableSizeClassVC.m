//
//  OverridableSizeClassVC.m
//  ObjCLingo
//
//  Created by Don Clore on 8/1/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import "OverridableSizeClassVC.h"
#import "WordGridCollectionView.h"
#import "TargetWordCollectionView.h"
#import "ObjCLingo-Swift.h"

@interface OverridableSizeClassVC ()
@property (strong, nonatomic) IBOutlet WordGridCollectionView *wordGridCollectionView;
@property (strong, nonatomic) IBOutlet TargetWordCollectionView *targetWordCollectionView;
@property (strong, nonatomic) IBOutlet UIImageView *titleImage;
@property (weak, nonatomic) WinnerPopup * _Nullable popup;
@end

@implementation OverridableSizeClassVC

- (void)viewDidLoad {
  [super viewDidLoad];
  self.wordGridCollectionView.wordGridDelegate =  self;
//  NotificationCenter.default
//  .addObserver(self, selector: #selector(OverridableSizeClassVC.dataChanged(_:)),
//               name: Model.modelRefreshedFromNet, object: nil)
  [[NSNotificationCenter defaultCenter]
   addObserver:self
   selector:@selector(dataChanged:)
   name:Model.modelRefreshedFromNet object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
  [self getDataAndReload];
  [super viewWillAppear:animated];
}

- (void)redrawGridsAndTitle {
  [self.wordGridCollectionView updateCellSize];
  [self.targetWordCollectionView updateCellSize];
  
  [self drawTheTitle];
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  
  [self redrawGridsAndTitle];
}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
  
  [self redrawGridsAndTitle];
}

- (void)drawTheTitle {
  CGSize targetSize = self.titleImage.bounds.size;
  
  UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0);
  
  CGRect targetRect = self.titleImage.bounds;
  CGRect inset = CGRectInset(targetRect, targetRect.size.width * 0.15, targetRect.size.height * 0.15);
  
  UIColor *blue = [UIColor colorWithRed:0.4003806114 green:0.8025868535 blue:0.9822079539 alpha:1];
  [UIBezierPath drawStrokedShadowedTextWithString:@"ObjC homework"
                                         fontFace:@"Avenir-BlackOblique"
                                        baseColor:blue dest:inset];
  
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  if (image) {
    self.titleImage.image = image;
    self.titleImage.contentMode = UIViewContentModeScaleAspectFit;
  }
  UIGraphicsEndImageContext();
}

- (void)transitioning {
  if (self.popup) {
    [self.popup dismissViewControllerAnimated:true completion:nil];
    self.popup = nil;
    [self onDismissedWithFoundAll:YES];
  }
  
  [self.wordGridCollectionView transitioning];
}

- (void)onDismissedWithFoundAll:(BOOL)foundAll {
  self.popup = nil;
  if (foundAll) {
    [self clearCollections];
    [self loadCollectionsWithNext];
  }
}

- (void)clearCollections {
  [self.wordGridCollectionView clearAll];
  [self.targetWordCollectionView removeCheckmarkFromAllCells];
  self.wordGridCollectionView.wordGrid = nil;
  self.targetWordCollectionView.wordGrid = nil;
  [self.wordGridCollectionView reloadData];
  [self.targetWordCollectionView reloadData];
}

- (void)loadCollectionsWithNext {
  WordGrid * next = Model.sharedInstance.NextGrid;
  self.wordGridCollectionView.wordGrid = next;
  self.targetWordCollectionView.wordGrid = next;
  [self.wordGridCollectionView reloadData];
  [self.targetWordCollectionView reloadData];
}

- (void)foundWinner:(NSString *)winner midPoint:(CGPoint  * _Nullable)midPoint foundAll:(BOOL)foundAll {
  [self.targetWordCollectionView weHaveAWinner:winner];
  if (NO == foundAll) {
    // We only display the popup when all words are found.
    return;
  }
  if (midPoint == nil) {
    return;
  }
  
  CGPoint convertedMid = [self.view convertPoint:*midPoint toView:self.view];
  NSString *prefix = @"Winner! \n";

  UIFont *prefixFont = [UIFont boldSystemFontOfSize:16.0];
  NSDictionary *attrs = @{
     NSFontAttributeName : prefixFont,
     NSForegroundColorAttributeName : [UIColor purpleColor],
     NSBackgroundColorAttributeName : [UIColor whiteColor],
  };
  
  NSMutableAttributedString *message = [[NSMutableAttributedString alloc] initWithString:prefix attributes:attrs];
  
  NSString *suffix = @"(Touch to play new game";
  NSDictionary *attributesForSuffix = @{
    NSFontAttributeName : [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote],
    NSForegroundColorAttributeName : [UIColor purpleColor],
    NSBackgroundColorAttributeName : [UIColor whiteColor],
  };
  NSAttributedString *touchToPlay = [[NSAttributedString alloc] initWithString:suffix attributes:attributesForSuffix];
  [message appendAttributedString:touchToPlay];

  WinnerPopup * pop = [[WinnerPopup alloc] initWithMessage:message
                                                  foundAll:foundAll
                                                    origin:convertedMid
                                             referenceView:self.wordGridCollectionView
                                                      size:CGSizeMake(250.0, 75.0)];
  
  pop.modalPresentationStyle = UIModalPresentationCustom;
  pop.transitioningDelegate = self;
  pop.delegate = self;
  [self presentViewController:pop animated:YES completion:nil];
  self.popup = pop;
}


- (void)dataChanged:(NSNotification *)note {
  dispatch_async(dispatch_get_main_queue(), ^{
    [self getDataAndReload];
  });
}

- (void)getDataAndReload {
  WordGrid * wordGrid = Model.sharedInstance.NextGrid;
  self.wordGridCollectionView.wordGrid = wordGrid;
  self.targetWordCollectionView.wordGrid = wordGrid;
  
  [self.wordGridCollectionView reloadData];
  [self.targetWordCollectionView reloadData];
}

- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
  if ([dismissed isKindOfClass:[WinnerPopup class]]) {
    ModalPopupAnimator *animator = [ModalPopupAnimator new];
    animator.presenting = NO;
    return animator;
  }
  return nil;
}

- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                            presentingController:(UIViewController *)presenting
                                                                                sourceController:(UIViewController *)source {

  if ([presented isKindOfClass:[WinnerPopup class]]) {
    ModalPopupAnimator *animator = [ModalPopupAnimator new];
    animator.presenting = YES;
    return animator;
  }
  return nil;

}

@end
