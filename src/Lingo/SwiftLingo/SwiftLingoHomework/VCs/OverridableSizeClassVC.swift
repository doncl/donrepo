//
//  OverridableSizeClassVC.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 6/27/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

/// This is the main VC that controls the two collection views and the imageview title.
/// Doing the stroked-shadowed text for the title is probably kind of hokey, but I get a kick
/// out of it - I spent an entire week in Hawaii translating Erica Sadun's UIKit Drawing book
/// code from Obj-C to Swift, and this seemed as good a time as any to use it; it's sort of
/// in keeping with the aesthetic spirit of the titling on the website game, although it's
/// not the same, by any means.  
@IBDesignable class OverridableSizeClassVC: UIViewController {

  @IBOutlet var wordGridCollectionView: WordGridCollectionView!
  @IBOutlet var targetWordCollectionView: TargetWordCollectionView!
  
  @IBOutlet weak var titleImage: UIImageView!
  
  var letterCount : Int = 0
  var popup : WinnerPopup?
  
  let blue : UIColor = #colorLiteral(red: 0.4003806114, green: 0.8025868535, blue: 0.9822079539, alpha: 1)
    
  override func viewDidLoad() {
    super.viewDidLoad()
    wordGridCollectionView.wordGridDelegate = self
    NotificationCenter.default
      .addObserver(self, selector: #selector(OverridableSizeClassVC.dataChanged(_:)),
                   name: Model.modelRefreshedFromNet, object: nil)
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
    
  @objc func dataChanged(_ note: Notification) {
    DispatchQueue.main.async {
      self.getDataAndReload()
    }
  }
  
  private func getDataAndReload() {
    let wordGrid = Model.sharedInstance.nextGrid
    wordGridCollectionView.wordGrid = wordGrid
    targetWordCollectionView.wordGrid = wordGrid
    
    wordGridCollectionView.reloadData()
    targetWordCollectionView.reloadData()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    getDataAndReload()
    super.viewWillAppear(animated)
  }
  
  // MARK: Dynamic sizing and drawing the silly title.
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    wordGridCollectionView.updateCellSize()
    targetWordCollectionView.updateCellSize()
    
    drawTheTitle()
  }
    
  override func viewDidLayoutSubviews() {
    titleImage.image = nil
    super.viewDidLayoutSubviews()
    wordGridCollectionView.updateCellSize()
    targetWordCollectionView.updateCellSize()
    
    drawTheTitle()
  }
  
  private func drawTheTitle() {
    let targetSize = titleImage.bounds.size
    
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    let targetRect = titleImage.bounds
    let inset = targetRect.inset(byPercent: 0.15)
  
    UIBezierPath.drawStrokedShadowedText(string: "SwiftLingo",
                                         fontFace: "Avenir-BlackOblique",
                                         baseColor: blue, dest: inset)
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return
    }
    titleImage.image = image
    titleImage.contentMode = .scaleAspectFit    
  }
}

// MARK: WordCollectionViewDelegate
extension OverridableSizeClassVC : WordGridCollectionViewDelegate {
  
  
  /// Handle UX when a word is found.
  ///
  /// - Parameters:
  ///   - winner: The word that was found.
  ///   - midPoint: The approximate midpoint of the word that was found.
  ///   - foundAll: true if all words are found, i.e. game over.
  func foundWinner(winner: String, midPoint: CGPoint?, foundAll: Bool) {
    
    // Call the targetWordCollectionView to have it put a checkmark on the found word.
    targetWordCollectionView.weHaveAWinner(winner: winner)
    if false == foundAll {
      // We only display the popup when all words are found.
      return
    }
    guard let mid = midPoint else {
      return
    }
    
    // Now display a popup giving the user some validation, and initiating the UX to get it
    // to recycle to a new set of game data.
    let convertedMid = view.convert(mid, to: view)
    let prefix = "Winner! \n"
    
    let attrs : [NSAttributedStringKey : Any] =
      [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 16.0),
      NSAttributedStringKey.foregroundColor : UIColor.purple,
      NSAttributedStringKey.backgroundColor : UIColor.white]
    
    let message = NSMutableAttributedString(string: prefix, attributes: attrs)
    
    let suffix = "(Touch to play new game)"
    let attributesForSuffix : [NSAttributedStringKey : Any] = [
       NSAttributedStringKey.font : UIFont.preferredFont(forTextStyle: .footnote),
       NSAttributedStringKey.foregroundColor : UIColor.purple,
       NSAttributedStringKey.backgroundColor : UIColor.white,
    ]
    let touchToPlay = NSAttributedString(string: suffix, attributes: attributesForSuffix)
    
    message.append(touchToPlay)
    
    let pop = WinnerPopup(message: message, foundAll: foundAll, origin: convertedMid,
                            referenceView: wordGridCollectionView,
                            size: CGSize(width: 250.0, height: 75.0))
    
    pop.modalPresentationStyle = .custom
    pop.transitioningDelegate = self
    pop.delegate = self
    self.present(pop, animated: true, completion: nil)
    popup = pop
  }  
}

// MARK: transitions - cancel selection
extension OverridableSizeClassVC {
  func transitioning() {
    if let pop = popup {
      pop.dismiss(animated: true, completion: nil)
      popup = nil
      onDismissed(foundAll: true)
    }
    wordGridCollectionView.transitioning()
  }
}

// MARK: WinnerPopupDelegate
extension OverridableSizeClassVC : WinnerPopupDelegate {
  
  
  /// Releases the popup reference, clears ther grid and target word collections, gets more
  /// data, and loads things up for the next game.
  ///
  /// - Parameter foundAll: We found all of them, time for next game?   The only reason this
  ///   exists is for the contingency of showing a popup every time a word is found; originally
  ///   I did that, but the 16 year old dismissed it as bad UX, so now it only displays popup
  ///   when all words are found.
  func onDismissed(foundAll: Bool) {
    popup = nil
    if foundAll {
      clearCollections()
      loadCollectionsWithNext()
    }
  }
  
  fileprivate func clearCollections() {
    wordGridCollectionView.clearAll()
    targetWordCollectionView.removeCheckmarkFromAllCells()
    wordGridCollectionView.wordGrid = nil
    targetWordCollectionView.wordGrid = nil
    wordGridCollectionView.reloadData()
    targetWordCollectionView.reloadData()
  }
  
  fileprivate func loadCollectionsWithNext() {
    let next = Model.sharedInstance.nextGrid
    wordGridCollectionView.wordGrid = next
    targetWordCollectionView.wordGrid = next
    wordGridCollectionView.reloadData()
    targetWordCollectionView.reloadData()
  }
  
}

// MARK: UIViewControllerTransitioningDelegate
extension OverridableSizeClassVC : UIViewControllerTransitioningDelegate {
  
  // Sets returns an instance of ModalPopupAnimator to handle the shrinking of
  // the popup.
  func animationController(forDismissed dismissed: UIViewController)
    -> UIViewControllerAnimatedTransitioning? {
      
      if let _ = dismissed as? WinnerPopup {
        let animator = ModalPopupAnimator()
        animator.presenting = false
        return animator
      }
      return nil
  }
  
  // Similarly, returns an instance of ModalPopupAnimator to handle the growing of the popup.
  func animationController(forPresented presented: UIViewController,
                           presenting: UIViewController,
                           source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    
    if let _ = presented as? WinnerPopup {
      let animator = ModalPopupAnimator()
      animator.presenting = true
      return animator
    }
    return nil
  }
}



