//
//  WinnerPopup.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 7/3/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

protocol WinnerPopupDelegate : class {
  func onDismissed(foundAll: Bool)
}

/// I just wanted some sort of acknowledgement that all the words in a particular grid were
/// found, as otherwise, you'd never see the last checkmark on the word in the
/// TargetWordCollectionView  - it would just move on too quickly to the next grid.
//
//  I had written a popup class on the job, my boss gave me permission to open-source it; it's
//  in my github, and this was based off that. 
class WinnerPopup: BaseModalPopup {
  let pad : CGFloat = 8.0
  var winningMessage: UILabel = UILabel()
  
  var foundAll : Bool
  
  weak var delegate: WinnerPopupDelegate?
  
  init(message: NSAttributedString, foundAll: Bool, origin: CGPoint, referenceView: UIView,
       size: CGSize) {
  
    self.foundAll = foundAll
    super.init(origin: origin, referenceView: referenceView, size: size)
    
    winningMessage.attributedText = message
    winningMessage.numberOfLines = 0
    winningMessage.textAlignment = .center
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    popupView.addSubview(winningMessage)
    
    winningMessage.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      winningMessage.centerXAnchor.constraint(equalTo: popupView.centerXAnchor),
      winningMessage.centerYAnchor.constraint(equalTo: popupView.centerYAnchor),
      winningMessage.heightAnchor.constraint(equalTo: popupView.heightAnchor, constant: -pad * 2),
      winningMessage.widthAnchor.constraint(equalTo: popupView.widthAnchor, constant: -pad * 2),
    ])
  }
  
  override func onDismissed() {
    delegate?.onDismissed(foundAll: foundAll)
  }
}
