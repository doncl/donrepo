//
//  AppDelegate.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 6/27/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
    /// N.B. It is required that unit tests pass a 'testing' argument.
    
    if false == ProcessInfo.processInfo.arguments.contains("testing") {
      // Just to kick things off.  Swift compiler is very clever about lazy initialization of
      // things, and we want to load the challenges from cache, or failing that, from the
      // bundle, and then kick off a network attempt as soon as is feasible.
      let _ = Model.sharedInstance
    }
    
    return true
  }
}

