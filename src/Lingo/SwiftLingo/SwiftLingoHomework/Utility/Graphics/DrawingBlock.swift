//
//  DrawingBlock.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 7/2/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

//
//  DrawingBlock.swift
//  DrawingUtilities
//
//  Created by Don Clore on 7/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.

import UIKit
import QuartzCore

typealias DrawingBlock = (CGRect) -> ()

typealias DrawingStateBlock = () -> ()

func pushGetImage(with block : DrawingBlock?, andSize size: CGSize) -> UIImage? {
  UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
  defer {
    UIGraphicsEndImageContext()
  }
  
  if let block = block {
    let r : CGRect = CGRect.from(size: size)
    block(r)
  }
  
  guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
    return nil
  }
  
  return image
}

func pushDraw(block : DrawingStateBlock?) {
  guard let block = block else {
    return
  }
  
  guard let context = UIGraphicsGetCurrentContext() else {
    return
  }
  
  context.saveGState()
  block()
  context.restoreGState()
}

// Improve performanmce by pre-clipping context before beginning layer drawing.
func pushLayerDraw(block: DrawingStateBlock?) {
  guard let block = block, let context = UIGraphicsGetCurrentContext() else {
    return
  }
  
  context.beginTransparencyLayer(auxiliaryInfo: nil)
  block()
  context.endTransparencyLayer()
}




