//
//  Storable.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 6/27/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

// With appropriate thanks to
// https://medium.com/@sdrzn/swift-4-codable-lets-make-things-even-easier-c793b6cf29e1
// Modified a bit to use POP instead of generics.

import Foundation

enum StorableDirectory {
  case documents
  case caches
}

/// Some simple code to let us store Codable objects in the documents or caches directories.
protocol Storable : Codable {
  associatedtype StorableType : Codable
  
  func store(to directory: StorableDirectory, as fileName: String, success: () -> (),
                  failure: (String) -> ())
  
  static func retrieve(_ fileName: String, from directory: StorableDirectory) -> StorableType?
  
  /// Remove specified file from specified directory
  static func remove(_ fileName: String, from directory: StorableDirectory,
                     failure: (String) -> ())
}

extension Storable {

  /// Store an encodable struct to the specified directory on disk
  ///
  /// - Parameters:
  ///   - directory: where to store the struct
  ///   - fileName: what to name the file where the struct data will be stored
  func store(to directory: StorableDirectory, as fileName: String, success: () -> (),
                    failure: (String) -> ()) {
    let url = Self.getURL(for: directory).appendingPathComponent(fileName, isDirectory: false)
  
    let encoder = JSONEncoder()
    do {
      let data = try encoder.encode(self)
      if FileManager.default.fileExists(atPath: url.path) {
        try FileManager.default.removeItem(at: url)
      }
      FileManager.default.createFile(atPath: url.path, contents: data, attributes: nil)
      success()
    } catch {
      failure(error.localizedDescription)
    }
}

  /// Retrieve and convert a struct from a file on disk
  ///
  /// - Parameters:
  ///   - fileName: name of the file where struct data is stored
  ///   - directory: directory where struct data is stored
  ///   - type: struct type (i.e. Message.self)
  /// - Returns: decoded struct model(s) of data
  static func retrieve(_ fileName: String, from directory: StorableDirectory) -> StorableType? {
    let url = getURL(for: directory).appendingPathComponent(fileName, isDirectory: false)
  
    if !FileManager.default.fileExists(atPath: url.path) {
      print("File at path \(url.path) does not exist!")
      return nil
    }
  
    if let data = FileManager.default.contents(atPath: url.path) {
      let decoder = JSONDecoder()
      do {
    
        let model = try decoder.decode(StorableType.self, from: data)
        return model
      } catch {
        print(error.localizedDescription)
        return nil
      }
    } else {
      print("No data at \(url.path)!")
      return nil
    }
  }

  /// Remove all files at specified directory
  static func clear(_ directory: StorableDirectory, failure: (String) -> ()) {
    let url = getURL(for: directory)
    do {
      let contents = try FileManager.default.contentsOfDirectory(at: url,
                                                                 includingPropertiesForKeys: nil,
                                                                 options: [])
      for fileUrl in contents {
        try FileManager.default.removeItem(at: fileUrl)
      }
    } catch {
      failure(error.localizedDescription)
    }
  }
  
  /// Remove specified file from specified directory
  static func remove(_ fileName: String, from directory: StorableDirectory,
                     failure: (String) -> ()) {
    
    let url = getURL(for: directory).appendingPathComponent(fileName, isDirectory: false)
    if FileManager.default.fileExists(atPath: url.path) {
      do {
        try FileManager.default.removeItem(at: url)
      } catch {
        failure(error.localizedDescription)
      }
    }
  }

  /// Returns BOOL indicating whether file exists at specified directory with specified file name
  static func fileExists(_ fileName: String, in directory: StorableDirectory) -> Bool {
    let url = getURL(for: directory).appendingPathComponent(fileName, isDirectory: false)
    return FileManager.default.fileExists(atPath: url.path)
  }
  
  /// Returns URL constructed from specified directory
  static fileprivate func getURL(for directory: StorableDirectory) -> URL {
    var searchPathDirectory: FileManager.SearchPathDirectory
    
    switch directory {
    case .documents:
      searchPathDirectory = .documentDirectory
    case .caches:
      searchPathDirectory = .cachesDirectory
    }
    
    return FileManager.default.urls(for: searchPathDirectory, in: .userDomainMask).first!
  }
}
