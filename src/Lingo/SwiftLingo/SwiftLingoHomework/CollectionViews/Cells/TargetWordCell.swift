//
//  TargetWordCell.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 7/1/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

/// A word with a selectable checkmark.
class TargetWordCell: UICollectionViewCell {
  struct Constants {
    static let horzPad : CGFloat = 5.0
    static let vertPad : CGFloat = 3.0
    static let checkSize = CGSize(width: 10, height: 10)
  }
  var label = UILabel()
  var checkMark = UIImageView(image: UIImage(named: "done")!)
  
  
  class var preferredFont : UIFont {
    return UIFont.boldSystemFont(ofSize: 14.0)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    checkMark.isHidden = true
    backgroundColor = .clear
    addSubview(checkMark)
    addSubview(label)
    label.font = TargetWordCell.preferredFont
    label.textColor = .white
    label.textAlignment = .left
    
    // Very simple layout - easiest to just do in code, as opposed to custom nib. 
    checkMark.translatesAutoresizingMaskIntoConstraints = false
    label.translatesAutoresizingMaskIntoConstraints = false

    NSLayoutConstraint.activate([
      checkMark.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.horzPad),
      checkMark.centerYAnchor.constraint(equalTo: label.centerYAnchor),
      checkMark.widthAnchor.constraint(equalToConstant: Constants.checkSize.width),
      checkMark.heightAnchor.constraint(equalToConstant: Constants.checkSize.height),
      
      label.leadingAnchor.constraint(equalTo: checkMark.trailingAnchor, constant: Constants.horzPad),
      label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Constants.vertPad),
    ])
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
