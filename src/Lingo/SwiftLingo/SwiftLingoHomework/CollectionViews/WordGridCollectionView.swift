//
//  WordGridCollectionView.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 7/1/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

protocol WordGridCollectionViewDelegate : class {
  func foundWinner(winner : String, midPoint: CGPoint?, foundAll: Bool)
}

/// Displays the draggable-selectable word grid.   It's a UICollectionView with some special
/// code layered on top to handle the UITouch stuff.
@IBDesignable class WordGridCollectionView: UICollectionView {
  let borderWidth : CGFloat = 0.5
  
  // MARK: Constants
  let letterCellId : String = "letterCellId"
  
  // MARK: Mutable data.
  var numLetters : Int = 0
  var numRows : Int = 0
  var numCols : Int = 0
  weak var layout : UICollectionViewFlowLayout?
  
  private var selectedCells : [CellCoords] = []
  private var touchDownCell : CellCoords?
  private var tgtWordInfos : [TargetWordInfo] = []
  
  weak var wordGridDelegate : WordGridCollectionViewDelegate?
  
  var wordGrid : WordGrid? {
    didSet {
      // The whole issue of side-effects on property setters is a contentious one.  This sort
      // of pattern seems to be ubiquitous in the Swift/iOS world, though.  Lots of other ways
      // to do it.  The entire Reactive / MVVM paradigm seems to embrace it; change some data,
      // and a bunch of stuff happens automagically.  I'm agnostic, and conform to whatever
      // philosophy my shop subscribes to.
      guard let characterRows = wordGrid?.characterGrid,
        characterRows.count > 0,
        let targetWordInfos = wordGrid?.getTargetWordInfos() else {
        return
      }

      numLetters = characterRows.reduce(0) { $0 + $1.count }
      numRows = characterRows.count
      numCols = characterRows[0].count
      
      guard numLetters > 0, numRows > 0, numCols > 0 else {
        return
      }
      updateCellSize()
      tgtWordInfos = targetWordInfos
    }
  }
  var currentCellContentTransform = CGAffineTransform.identity
  var currentCellCALayerTransform = CATransform3DIdentity

  // MARK: Initializers
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
       
    register(LetterCell.self, forCellWithReuseIdentifier: letterCellId)
    layout = setFlowLayoutValues()
    
    dataSource = self
    
    if let gridBorderColor = UIColor(named: "WordGridBorder") {
      layer.borderColor = gridBorderColor.cgColor
      layer.borderWidth = borderWidth
    }
    
    isUserInteractionEnabled = true
    isMultipleTouchEnabled = false
  }
  
  @discardableResult
  private func setFlowLayoutValues() -> UICollectionViewFlowLayout {
    guard let flow = collectionViewLayout as? UICollectionViewFlowLayout else {
      fatalError("Assumptions are broken.  This is supposed to be a flow layout")
    }
    flow.minimumLineSpacing = 0
    flow.minimumInteritemSpacing = 0
    flow.sectionInset = .zero
    return flow
  }
  
  // MARK: Methods.
  
  /// It's the external container's responsibility to let this grid know when things change.
  func updateCellSize() {
    guard numLetters > 0, numRows > 0, numCols > 0 else {
      return
    }
    let width = frame.width
    let height = frame.height
    
    var cellSize = CGSize(width: width / CGFloat(numCols), height: height / CGFloat(numRows))
    
    // This was a bear to figure out.  The flow layout is pretty touchy about this stuff, for
    // pretty obvious reasons when you think about it.  This ONLY manifested on the iPhone 8
    // portrait layout - the constraint for giving the collectionView a 1:1 aspect ratio turns
    // out to only get it within a half point or so.  So when this calculation is done, under
    // some circumstances, the cellSize members not being quite equal cause it to lay out
    // improperly in the grid.  After much hair-pulling, the solution is just to equalize the
    // cell size members to the smaller size - they have to be dead-on square.
    if cellSize.width > cellSize.height {
      cellSize.width = cellSize.height
    } else if cellSize.height > cellSize.width {
      cellSize.height = cellSize.width
    }
    
    layout?.itemSize = cellSize
    layout?.invalidateLayout()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
  }
  
  /// Called by parent whenever a transition (rotation or whatever) happens so we can avoid
  /// doing anything embarassing.
  func transitioning() {
    if let _ = touchDownCell {
      // we're in the middle of a selection
      selectCells(false, finalSelect: false)
      selectedCells.removeAll()
      touchDownCell = nil
    }
  }
}

// MARK: UICollectionViewDataSource
extension WordGridCollectionView : UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int)
    -> Int {
      
    return numLetters
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath)
    -> UICollectionViewCell {
      return getLetterCell(collectionView, indexPath: indexPath)
  }
  
  private func getLetterCell(_ collectionView : UICollectionView, indexPath: IndexPath)
    -> LetterCell {
      
      assert(numLetters > indexPath.item)
      guard let characters = wordGrid?.characterGrid, numCols > 0, numCols > 0 else {
        fatalError("Code assumptions are broken.  This method should never be called if there's no data")
      }
      
      guard let cell = collectionView
        .dequeueReusableCell(withReuseIdentifier: letterCellId, for: indexPath) as? LetterCell else {
          fatalError("Unable to dequeue cell")
      }
      
      let rowNum = indexPath.item / numRows
      let colNum = indexPath.item % numCols
      let letter = characters[rowNum][colNum]
      cell.label.text = letter.uppercased()
      return cell
  }
}

// MARK: Gesture stuff
extension WordGridCollectionView {
  
  // MARK: TouchesBegan/Moved/Ended trinity.
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard touchDownCell == nil else {
      return
    }
    
    let touch = touches.first!
    let location = touch.location(in: self)
    
    guard let colRow = hitTest(location) else {
      return
    }
    touchDownCell = colRow
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard let _ = touchDownCell else {
      return
    }
    let touch = touches.first!
    let location = touch.location(in: self)
    
    if false == bounds.contains(location) {
      return
    }

    hitTestAndProcess(location, touchesEnded: false)
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    let touch = touches.first!
    let location = touch.location(in: self)
    
    if bounds.contains(location) {
      hitTestAndProcess(location, touchesEnded: true)
    } else {
      selectCells(false, finalSelect: false)
    }
    
    print("TouchesEnded - selectedCells = \(selectedCells)")
    touchDownCell = nil
    selectedCells.removeAll()
  }
  
  // MARK: Utility methods for the Touch trinity
  
  
  /// Selects and deselects cells, both for the blue 'inprocess' state, and for the orange
  /// 'final' state.
  ///
  /// - Parameters:
  ///   - inProcessSelect: bool - adds or removes the blue color to selected cells collection.
  ///   - finalSelect: bool - adds or removes the orange color to selected cells collection.
  private func selectCells(_ inProcessSelect: Bool, finalSelect : Bool) {
    for cell in selectedCells {
      let item = (cell.row * numCols) + cell.col
      let indexPath = IndexPath(item: item, section: 0)
      if let letterCell = cellForItem(at: indexPath) as? LetterCell {
        letterCell.setSelected(blueCircle: inProcessSelect, amberBackground: finalSelect)
      }
    }
  }
  
  /// Called by parent VC when it needs to start a new game.
  func clearAll() {
    for cell in visibleCells {
      if let letterCell = cell as? LetterCell {
        letterCell.setSelected(blueCircle: false, amberBackground: false)
      }
    }
    selectedCells.removeAll()
  }
  
  /// Clear the selection utterly.
  private func clearInProcessSelection() {
    selectCells(false, finalSelect: false)
    selectedCells.removeAll()
  }

  private func hitTestAndProcess(_ point : CGPoint, touchesEnded: Bool) {
    guard let touchDownCell = touchDownCell,
      let cellToProcess = hitTest(point) else {
        clearInProcessSelection()
      return
    }
    
    clearInProcessSelection()
   
    guard let vector = CellCoords.getVector(src: touchDownCell, dest: cellToProcess) else {
      return
    }
    
    // OK, this is way too functional/closure-y/lamda-y for my poor head, but it's still easier
    // than writing the imperative code.  The idea is to check to see if the user is trying to
    // select across something that intersects with a word already found (for games that have
    // more than one word), and diallowing it, because...it's a grey area, but teeming with
    // UX wrinkles.
    if let _ = tgtWordInfos.first(where: {$0.coords.filter({vector.contains($0)}).isEmpty == false && $0.found}) {
      print("Disallowing intersecting selections")
      clearInProcessSelection()
      return
    }
    
    selectedCells = vector
    
    if touchesEnded {
      if let winner = tgtWordInfos.first(where: {$0.coords == selectedCells && $0.found == false}) {
        
        print("We have a winner at \(selectedCells)")
        
        winner.found = true
        selectCells(false, finalSelect: true)
        let midPointOfSelection = getLocationOfMiddleLetterOfSelection(cells: selectedCells)
        var foundAll : Bool = true
        if let _ = tgtWordInfos.first(where: {$0.found == false}) {
          foundAll = false
        }
        wordGridDelegate?.foundWinner(winner: winner.word, midPoint: midPointOfSelection,
                                      foundAll: foundAll)
      } else {
        clearInProcessSelection()
      }
    } else {
      selectCells(true, finalSelect: false)
    }
  }
  
  /// By convention, the popup guy points at the middle-ish letter of the selected word.
  ///
  /// - Parameter cells: array of cell coords to crunch on.
  /// - Returns: location of middle-ish cell, or nil if not possible.
  private func getLocationOfMiddleLetterOfSelection(cells: [CellCoords]) -> CGPoint? {
    guard !cells.isEmpty, let layout = layout else {
      return nil
    }
    let mid = cells[cells.count / 2]
    let itemSize = layout.itemSize
    let x = (CGFloat(mid.col) * itemSize.width) + itemSize.width / 2
    let y = (CGFloat(mid.row) * itemSize.height) + itemSize.height / 2
    return CGPoint(x: x, y: y)
  }
  
  /// Given a touch, return the grid coordinates.
  ///
  /// - Parameter point: location of touch.
  /// - Returns: Grid col/row of cell the touch was over, or nil. 
  private func hitTest(_ point : CGPoint) -> CellCoords? {
    guard gridNotEmpty() else {
      return nil
    }
    
    let col = Int((point.x / bounds.width) * CGFloat(numCols))
    let row = Int((point.y / bounds.height) * CGFloat(numRows))
    
    let cell = CellCoords(col: col, row: row)
    
    // Disallow cells that are part of an already-found word.
    if let _ = tgtWordInfos.first(where: {$0.coords.contains(cell) && $0.found}) {
      return nil
    }
    return cell
  }
  
  private func gridNotEmpty() -> Bool {
    return wordGrid != nil && numRows > 0 && numCols > 0
  }
}

