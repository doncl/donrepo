//
//  TargetCollectionHeader.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 7/2/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

private struct HeaderFontSizes {
  var wordFontSize : CGFloat
  var wordValFontSize : CGFloat
  var langFontSize : CGFloat
}

/// I elected to do the header in code, because IB kept crashing when I did the header
/// declaratively.   It seemed worthwhile to show that I know how to do layouts in code.

/// It also seemed worthwhile to change the font sizes, based on size class.  I know you can set
/// font overrides declaratively in IB, and I probably would have done so if it hadn't kept
/// crashing, but it seemed a good opportunity to show I have some knowledge of UITraitEnvironment
/// protocol, and can listen for changes and detect properly.
///
/// I could have gone the full distance and implemented full-on Adaptive Text, using
/// UIFont.preferredFontforXXX, and listening for changes, and so forth (I'm pretty sure custom
/// UIView's don't get this for free, although sonme of the UIKit child classes do, IIRC).
/// I elected not to because I judged it outside the scope of what was being asked for here,
/// but, just in case, I was just reading:
///  https://codelle.com/blog/2016/10/overriding-the-ios-dynamic-type-font-family/
/// In practice, I've found our designers don't have bandwidth to go the extra mile to really
/// see how this would affect complex layouts - they give us Zeplin comps that have hardcoded
/// font sizes coded right in the comp; I've spoken with them about giving us symbolic names
/// (.body, .subheadline, etc.), and they haven't a lot of appetite for this.
///
/// In fact, I'm totally willing to color outside the lines in this matter, although it helps
/// me to have a designer to collaborate with to decide the semantics of each piece of text,
/// and what symbol it ought to map to - then I'm glad to use whatever custom font family they
/// want for everything, using the technique shown above, or some similar scheme.
class TargetCollectionHeader: UICollectionReusableView {
  private let compactFontSizes =
    HeaderFontSizes(wordFontSize: 16, wordValFontSize: 14, langFontSize: 12)
  
  private let regularFontSizes =
    HeaderFontSizes(wordFontSize: 24, wordValFontSize: 20, langFontSize: 18)
  
  var horzInset : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 5.0)
  let vertInset : UIEdgeInsets = UIEdgeInsets(top: 3.0, left: 0.0, bottom: 3.0, right: 0.0)
  
  var wordLabel : UILabel = UILabel()
  var wordValLabel : UILabel = UILabel()
  
  var sourceLangLabel : UILabel = UILabel()
  var sourceLangValLabel : UILabel = UILabel()
  var targetLangLabel : UILabel = UILabel()
  var targetLangValLabel : UILabel = UILabel()
  
  let horzPad : CGFloat = 5.0
  let regularVertPad : CGFloat = 5.0
  let compactVertPad : CGFloat = 10.0
  
  private var labelFontMap : [UILabel : (UIFont, UIFont)]
  
  override init(frame: CGRect) {
    labelFontMap = [
      wordLabel : (UIFont.boldSystemFont(ofSize: regularFontSizes.wordFontSize),
                   UIFont.boldSystemFont(ofSize: compactFontSizes.wordFontSize)),
      
      wordValLabel : (UIFont.systemFont(ofSize: regularFontSizes.wordValFontSize),
                      UIFont.systemFont(ofSize: compactFontSizes.wordValFontSize)),
      
      sourceLangLabel : (UIFont.systemFont(ofSize: regularFontSizes.langFontSize),
                         UIFont.systemFont(ofSize: compactFontSizes.langFontSize)),
      
      sourceLangValLabel : (UIFont.systemFont(ofSize: regularFontSizes.langFontSize),
                            UIFont.systemFont(ofSize: compactFontSizes.langFontSize)),
      
      targetLangLabel : (UIFont.systemFont(ofSize: regularFontSizes.langFontSize),
                        UIFont.systemFont(ofSize: compactFontSizes.langFontSize)),
      
      targetLangValLabel : (UIFont.systemFont(ofSize: regularFontSizes.langFontSize),
                          UIFont.systemFont(ofSize: compactFontSizes.langFontSize)),
    ]
    super.init(frame: frame)
  
    setCategoryLabel(wordLabel, text: "Word: ")
    setValueLabel(wordValLabel)
   
    setCategoryLabel(sourceLangLabel, text: "Source: ")
    setValueLabel(sourceLangValLabel)
    
    setCategoryLabel(targetLangLabel, text: "Target: ")
    setValueLabel(targetLangValLabel)
   
    addSubview(wordLabel)
    addSubview(wordValLabel)
    addSubview(sourceLangLabel)
    addSubview(sourceLangValLabel)
    addSubview(targetLangLabel)
    addSubview(targetLangValLabel)
    
    backgroundColor = .white
  }
  
  private func setCategoryLabel(_ label : UILabel, text: String) {
    setFont(forLabel: label)
    label.textColor = .black
    label.textAlignment = .left
    label.text = text
  }
  
  private func setFont(forLabel label: UILabel) {
    guard let foundFonts = labelFontMap[label] else {
      return
    }
    label.font = traitCollection.isBig ? foundFonts.0 : foundFonts.1
  }
  
  private func setAllLabelFonts() {
    for kvp in labelFontMap {
      kvp.key.font = traitCollection.isBig ? kvp.value.0 : kvp.value.1
      kvp.key.setNeedsLayout()
      kvp.key.setNeedsDisplay()
    }
  }
  
  private func setValueLabel(_ label : UILabel) {
    setFont(forLabel: label)
    label.textColor = .black
    label.textAlignment = .left
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    let startY = traitCollection.isBig ? regularVertPad : compactVertPad
    
    var size = wordLabel.intrinsicContentSize
    wordLabel.frame = CGRect(x: horzPad, y: startY, width: size.width, height: size.height)
    
    size = wordValLabel.intrinsicContentSize
    wordValLabel.frame = CGRect(x: bounds.width - (size.width + horzPad), y: startY,
                                width: size.width, height: size.height)
    
    size = sourceLangLabel.intrinsicContentSize
    sourceLangLabel.frame = CGRect(x: horzPad, y: wordLabel.frame.maxY + regularVertPad,
                                      width: size.width, height: size.height)
    
    size = sourceLangValLabel.intrinsicContentSize
    sourceLangValLabel.frame = CGRect(x: bounds.width - (size.width + horzPad),
      y: wordLabel.frame.maxY + regularVertPad, width: size.width, height: size.height)
    
    size = targetLangLabel.intrinsicContentSize
    targetLangLabel.frame = CGRect(x: horzPad, y: sourceLangLabel.frame.maxY + regularVertPad,
                                   width: size.width, height: size.height)
    
    size = targetLangValLabel.intrinsicContentSize
    targetLangValLabel.frame = CGRect(x: bounds.width - (size.width + horzPad),
                                      y: sourceLangLabel.frame.maxY + regularVertPad, width: size.width,
                                      height: size.height)
  }
  
  // MARK: - Adaptive UI font override.
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    if let previousTraitCollection = previousTraitCollection {
      if previousTraitCollection.isBig == traitCollection.isBig {
        return
      }
    }
  
    setAllLabelFonts()
  }
}

private extension UITraitCollection {
  var isBig : Bool {
    return horizontalSizeClass == .regular && verticalSizeClass == .regular
  }
}
