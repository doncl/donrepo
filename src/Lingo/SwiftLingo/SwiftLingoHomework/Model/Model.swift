//
//  Model.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 6/28/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

/// We have a very simple data model, however, for robustness' sake we are storing it in two
/// ways locally, and also obtaining it from the net.   The original find_challenges.txt file
/// is stored as a resource in the main bundle.  We also keep a copy in the .caches directory;
/// whenever a model is obtained either from the network, or from the bundle (when the net is
/// down), we store it in the cache.  Whenever we get a changed model from the server, then
/// we store it in the cache, and fire a modelRefreshedFromNet Notification that the Controller
/// layer can listen for, and refresh its views accordingly.  The goal here was to be robust
/// without triggering unnecessary view repaints.
class Model {
  let resourceName = "find_challenges"
  let fileExt = "txt"
  let onlineVersion = "https://s3.amazonaws.com/duolingo-data/s3/js2/find_challenges.txt"
  static let cachedDataName = "cachedGrids.model"
  static let modelRefreshedFromNet = Notification.Name("model_refreshed_from_net_key")

  // Singleton pattern.
  // From https://krakendev.io/blog/the-right-way-to-write-a-singleton
  // I have no religion about this; this way seems as good as any.
  static var sharedInstance : Model = Model()
  
  private var gridIndex : Int = 0
  
  private init() {
    secondPhaseInitializer()
  }
  
  var data : WordGridCollection = WordGridCollection()
 
  
  /// This property is the principal way consumers of this class interact with it. 
  public var nextGrid : WordGrid {
    defer {
      gridIndex += 1
      if gridIndex == data.grids.count {
        gridIndex = 0
      }
    }
    return data.grids[gridIndex]
  }
  
  // Two-phase initializer - this weakens encapsulation of the singleton, but makes it easy to
  // unit test; seemed like a worthwhile tradeoff.
  public func secondPhaseInitializer() {
    
    // If we can restore from the cache, just do that.
    if false == restoreModelFromCache() {
      // otherwise, build it from the bundle resource.
      buildModelFromBundle()
    }
    
    // Now check with the network to see if we have a fresh copy.
    buildModelFromInterWebs()
  }
  
  /// Builds up the model from the caches directory, if the cached file is present.
  ///
  /// - Returns: true if the file is found, else false.
  private func restoreModelFromCache() -> Bool {
    guard let cachedGrids =
      WordGridCollection.retrieve(Model.cachedDataName, from: .caches) else {
        return false
    }
    data = cachedGrids
    return true
  }
  
  /// Stores grids to data model if it has changed, regardless of the source of the grids.
  /// If the model has changed, fires off a notification.
  ///
  /// - Parameter wordGrids: array of wordgrids.
  fileprivate func storeGrids(wordGrids : [WordGrid]) {
    if self.data.grids != wordGrids {
      self.data.grids = wordGrids
      self.gridIndex = 0 // safest to do this, in case model has drastically changed.
      self.data.store(to: .caches, as: Model.cachedDataName, success: {
      }, failure: { err in
        print("Error storing grids to cache = \(err)")
        // continue on, not fatal.
      })
      NotificationCenter.default.post(name: Model.modelRefreshedFromNet, object: nil)
    }
  }
  
  
  /// Hits the network to build the model.   The model file is not a JSON doc - it's just a
  /// text file, with a bunch of lines of JSON - so it splits it on newlines, and deserializes
  /// it to an array of WordGrid structs.
  private func buildModelFromInterWebs() {
    Net.sharedInstance.getNetworkData(uri: onlineVersion, success: { stringValue in
      var lines : [String] = []
      if false == stringValue.isEmpty {
        lines = stringValue.components(separatedBy: "\n")
      }
      guard let wordGrids = Model.build(from: lines) else {
        return // No worries, just use cache or bundle.
      }
      
      if false == Thread.isMainThread {
        DispatchQueue.main.async {
          self.storeGrids(wordGrids: wordGrids)
        }
      } else {
        // Do it synchronously.  This is mainly the unit test case, where Net.sharedInstance is
        // a mock.  We need synchronous behavior to test whether the notification was fired or
        // not.
        self.storeGrids(wordGrids: wordGrids)
      }
    }, failure: { err in
      print("Failed getting model from network - error = \(err), falling back to local version")
    })
  }
  
  /// Builds model straight from bundle resources.
  private func buildModelFromBundle() {
    if let lines = getBundleLines(), let grids = Model.build(from: lines) {
      data.grids = grids
    }
    data.store(to: .caches, as: Model.cachedDataName, success: {
      
    }, failure: { err in
      print("Failed to store grids to cache - err = \(err)")
      //Consider: should this be a fatalError?
    })
  }
  
  /// Class func that builds a WordGrid array from a string array.
  ///
  /// - Parameter lines: array of strings that purports to be a collection of serialized
  ///   WordGrid structs.
  /// - Returns: On success, an array of WordGrids, on failure, nil.
  class func build(from lines:[String]) -> [WordGrid]? {
    var grids : [WordGrid]? = []
    
    for line in lines {
      WordGrid.jsonDecode(line, success: { wordGrid in
        grids?.append(wordGrid)
      }, failure: { err in
        print("Error deccoding \(err) decoding line \(line)")
        grids = nil
      })
      if nil == grids {
        break
      }
    }
    return grids
  }
    
  /// Gets the lines of text straight from the bundle resource.
  ///
  /// - Returns: Array of lines of text, or nil on failure.
  public func getBundleLines() -> [String]? {
    guard let url = Bundle.main.url(forResource: resourceName, withExtension: fileExt) else {
      return nil
    }
    
    do {
      let content = try String(contentsOf: url)
      let lines = content.components(separatedBy: "\n")
      return lines.filter({$0.isEmpty == false})
    } catch let err {
      print("Failed to load \(resourceName) from bundle, err = \(err)")
      return nil
    }
  }
}
