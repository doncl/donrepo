# SwiftLingo Homework

## Overview

I'd like to take a moment here to point to some other open source code that I have written, that may be of interest.  I have a github account with some custom iOS controls, but probably the best place to look is my [Portfolio app](https://gitlab.com/doncl/portfolio) on Gitlab; it's a superset of everything, for the most part.  Also, as regards data structures and algorithms, [Sid](https://github.com/doncl/SeeSharp/tree/master/Sid) is a C#/Mono/Linux-based RESTful WebService layer that shows the use of a Trie data structure to map incoming RESTful requests to endpoints.  It serves the same purpose that Jersey does in the JEE world, or Flask on Python. Sid has been in production for almost four years, and has served us well; a lot of facilities that are available on .NET/Windows (like Microsoft's WebApi) have not traditionally been available on Linux/Mono, although that is happily changing.  Still, when you peruse [Maven's website](https://www.themaven.net) you are using this web service layer that I am the sole author of. 

As regards this homework, I hope I didn't unnecessarily throw in a lot of features or frills that were unnecessary or above and beyond your specification; I did a fair bit of thinking about how to balance my desire to display some of the tools in my iOS toolbox that I've acquired on my 3.5 year journey into this marvellous domain, with a desire to avoid going egregiously overboard in an off-putting way.  I considered, and rejected, doing a particle-emitter psuedo-fireworks display when the user completes all the grids, before rolling over to the first grid again, but I decided that....that was probably too much.  And obviously there were time constraints too, although you guys have been awesome about this aspect of the process.

Honestly, one reason I'm looking and interviewing is that my company is de-prioritizing the native apps, and I'm being shuffled back onto back-end work. This is important and necessary for my tiny company, for business reasons, and I bear the company no ill-will for this strategic course change, but is not in alignment with my career goals.  

### iOS 
I adore iOS.  Not so much the shiny gadgets (although I love them), or Apple's core culture or company itself (although I admire the company's software track record greatly), but I what I really love the most is the iOS software stack, and its ancestry stretching back to the 80's, Xerox PARC, Smalltalk 80, Objective-C, all that.  I love the fact that it's native code, in a tech world where managed code and interpreted languages are ubiquitous.  I think LLVM and LLDB are awesome, and I anticipate many happy years of exploring all the nooks and crannies of advanced debugging in this enrivonment.  While I'm a very senior engineer (23 years now), I consider myself a journeyman in iOS development, and I am more excited about this technical journey and coding than I have been in a very long time.  I truly wish I'd jumped into iOS in 2007, when Apple first introduced the App Store and iPhone OS.  
 
So, let me talk about the  structure of the homework assignment, and the rationales for the various decisions I made building this thing. 

## Homework app structure
### Model
I decided, given the requirement to download the word grid data from the network, that it would be nice if the thing would work if you guys test it in a room without wifi, or you deliberately turn the net off, or whatever.  So I grabbed the file from S3, stuck it in the bundle, and I initially build the model from that.  But...obviously the file can change; you guys might decide to add something to the data set.  So I use the caches directory to build a temporary on-disk version the file that the system can flush if it needs to.  I copy the bundle data to the cache, then hit the network, and if that succeeds, I compare the fresh data from the network to the cache, and if it's different I update the cache.   

The goal was to produce a simple, but robust system.  I wrote a very minimal URLSession-based downloader, and used Codable for deserializing the JSON.  I use protocol extensions for building sharable utility code for this kind of thing. 

### Overall View Hierarchy and size class transition management

#### View Hierarchy
So....as soon as I got into any sort of View-related stuff, I started needing to color between the lines a bit.  The spec referred to a web page, which gave pretty good guidance, but couldn't really be followed literally, as it is a web-based context, and it is a slightly different game than what I was asked to produce.  But it had three fundamental components that seemed appropriate to adapt to iOS: 

- Word grid
- Target word pane
- Stylized title

##### Word Grid
So, this was the most work, I guess.  This is the grid of words that's selectable by touch/drag, and displays a blue circle when selection is in-process, and then an amber rectangle on letters over successfully identified words.  The state machine of touchdown and so forth was probably the trickiest bit here, although it wasn't a huge deal.  It helped to have my 16 year old pound on it, and help me find bugs.  It's a UICollectionView which uses the Flow layout, no scrolling, fixed grid that changes dimensions with each game.  The data set implies that the grid is always square; rightly or wrongly, I decided it was permissable to take this as an assumption and constraint.  I have not tested it with assymetric data - it's likely it will still work with some assymetric data sets, and render rectangular cells with ovals, but....I judged, correctly or not, that this was not a rabbit hole you wished me to explore; and I guess it's my bad if I misjudged.

I used Core Animation to draw the borders of the cells, with CAShapeLayers so I could get butted line ends and mitered joins. CA was also used for the selection shapes.

I brought in some open source code I have for a modal animated popup to demarcate the UX upon winning a 'game', just to visually distinguish each grid from the next.  

###### Blue in-process selection mechanics
I opted to copy the behaviors of the web page, modulo the instructions that were given as regards prohibiting right-to-left, down-to-up, and diagonal right-to-left, down-to-up vectors.  Otherwise, I believe it behaves like the web page does. 

##### Target Word Pane
This was another very simple UICollectionView, single or multi-column, depending on the device and orientation, and it has a custom header view that shows the source word, and source and target languages.  At first I tried to do the header in Interface Builder, but it kept crashing, over and over, and after many attempts to clear Derived Data, and everything I could think of, I just rolled one in code.  I could have also used a Xib file, and I could have used Auto Layout for this one, but it was such a simple layout....I opted at first to use some UIStackViews - 3 horizontal ones, with a single vertical one.  But....I kept getting a bunch of AutoLayout warnings (despite the fact that I wasn't creating a single constraint).  I'm pretty sure this was just me using the system in a way that wasn't anticipated, and the warnings could safely be ignored.  However, I've learned from painful experience to never, ever ignore those warnings; and I love a good hard crash as much as anybody, but...not so great from a product standpoint.  So I just overrode layoutSubviews(), and manually put the frames where I wanted, and that was that.   AutoLayout was used pretty much everywhere else, I reckon, and I took great pains to try to do the right thing in all size classes / orientations, and I'm pretty sure all the issues are resolved; more on this below. 

##### Stylized Title
The web page game had a funky, somewhat retro, non-flat graphical title, with raised and shadowed edges and so forth, very much not Jony-Ive-flat :).  I didn't attempt to duplicate exactly what they had, but rather I had a bit of code that I ran into a year ago when on vacation in Hawaii. It's I suppose rather pathetic and says something about me (whether good or bad is up to you) that while my wife and daughter were snorkeling and recreating in Maui last summer, I was busy reading and studying Erica Sadun's entrancing book on UIKit Drawing, and translating her ObjC code into Swift.  For me, this is a really effective way to really absorb the meaning of some code; sometimes it's a mechanical translation process, but other times, I have to stop and think about what the code is saying, and how I'm going to use it, and if it needs to be more 'Swifty' or whatever.   

So...this project includes a small subset of the code I translated from Erica's book; it's got some UIBezierPath extensions that take a font name and a string, and draw path with shadowing and bevelling, and while quite different than the actual style of titling in the web page, it seems congruent in spirit to me.   In any case, it was just kind of a fun thing to do, a simple label seemed kind of bland; I hope you guys don't mind this.  The credit, if any due, is all Erica's, for sure, but it's one of the arrows in my quiver. 

#### Size class transition management. 
I felt like, on iPad, it needed a different layout than in portrait than landscape, and that's a bit at tension with the tools Apple gives us to do Adaptive Layout. By default (and I know I'm not telling you anything), iPads are regular vetical and regular horizontal size classes, and there's really no distinction between portrait and landscape, it's just a size transition, not a traitcollection transition.  The Maven app had slightly different issues in this area; in that case, I chose to write all the transition handling in code, and write a custom container for view controllers, and kind of do my own thing that met my boss's spec for doing a master-view layout, which was not quite what the SplitViewController provides.

However, in this case, I just wanted a portrait and a landscape layout, and when scaled up to tablet, I still thought it needed a similar portrait layout to the phone.  I found a [Stack Overlow link](https://stackoverflow.com/questions/26633172/sizing-class-for-ipad-portrait-and-landscape-modes/28268200#28268200) that was really helpful, as regards overriding the UITraitCollection when it's in portrait on iPad so I can still have a compact horizontal with regular vertical orientation.  It did kind of make IB's previewing features a little less useful, but I think it was warranted for this layout. Comments on the article suggest that Apple uses this very technique for Safari; it seems quite reasonable to me. 

I did some manual in-code font override stuff on the TargetWord collection; as that header view was not done in IB, I could not use the graphical tools for that. 

Otherwise, everything was done in IB, with the exception of a bit of layout-anchor-in-code stuff here and there where it was convenient for pinning a child to a parent.

##### Layout in Interface Builder vs. code.
I'm a little out of practice using IB for layout; when we started this company, my coding partner, Aarti, was really fed up with wrestling with it from the Scout Media days. She had a developer friend in San Francisco who spoke enthusiastically of SnapKit, which is a fluent DSL for AutoLayout, a descendant of Masonry, I believe.  So I went along with this, and we did the entire [Maven app](https://itunes.apple.com/us/app/maven-news-community/id1254283617?mt=8) (which I hope you have downloaded and looked at) in code; the only storyboard in that app is the Launch Screen :). We actually had a very positive experience with SnapKit, except for the pain of porting it from Swift 2 to 3, and from 3 to 4, when we needed to move ahead, and the maintainers were lagging a bit.  We also use [AsyncDisplayKit](https://texturegroup.org) for layout of the media-heavy scrolling areas, which suffered from poor performance and frame drops when using AutoLayout initially.  I have come to believe this was a naive implementation on my part, and we probably could have continued to use UIKit for this stuff, but we had an initially very positive experience with ASDK (the scrolling rendering performance was initially quite shockingly pleasing), and so we went with it.  So...it's incompatible with both IB and AutoLayout, and has its own layout model sort of loosely based on CSS FlexBox.  So, say I think it's fair to I've had experience doing iOS layouts in a lot of different ways. In retrospect, I would not have used ASDK; it breaks some fundamental UIKit assumptions quite deliberately (doing layout calculations on background threads) which can cause tension with other 3rd party dependencies like (in our case) Realm, which has its own set of threading constraints.  I've solved all those 'impedance mismatch' problems now, but only after many long hours, days, and even weeks of debugging and managing technical debt.

I think I could have gotten satisfactory performance by just overriding layoutSubviews() for the layout hotspots, avoiding overburdening the linear equation solver, and using prefetching on Tableview and CollectionView.  I think, now that TableView and CollectionView feature prefetching, and since a lot of shops are going other hybrid development routes (like React Native or Flutter), that ASDK has kind of fallen out of favor a bit.  It's quite an impressive framework, but also sports a rather restrictive open source license, and well....I'm ready to move past it.

So...in my long winded way, I'm just trying to say that I haven't done a lot of IB work in awhile, but for this homework assignment, because it's my perception that this is mainstream, and the spec mentioned rotation and Adaptive UI, that it was probably wise to use IB as much as possible, to show that I don't have any issues using it.  The only really salient exception to that was the header on TargetWordCollectionView, I believe.

## Summary
That's about it. I tried to show my conversancy with 

- Adaptive Layout principles, including size classes, trait overrides, font overrides
- Animated Custom View Controller transitions (on the popup) 
- AutoLayout - used everywhere except for a table header, and the TargetWord cells
- Codable
- Core Animation (drawing cell and collectionview borders)
- Interface Builder (despite some exceptions I've detailed above, almost all the adaptive layout was IB)
- POP (Protocol Oriented Programming) used for Codable and Storable shared utility code
- UICollectionView fundamentals
- Unit Testing
- UIKit Drawing (the title)
- UIResponder chain (UITouch) handling (the dragging and selection business)
- URLSession

in this little exercise, and it was a lot of fun, actually.  This is the work I love to do.   This kind of thing plays a lot more to my strengths than writing code in front of you guys, whether on whiteboard or laptop, and I hope it therefore plays out in my favor in the decision process.  I hope it goes without saying that I honor your need and right to use whatever process you feel is warranted in determining my fitness for this position; I want you to know that I will come out looking better if judged on my results, and not my process, but it's entirely your perogative, and I completely respect that. 

best,
Don




