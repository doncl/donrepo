//
//  BBPRichEditTextStorage.swift
//  BBPRichEditExample
//
//  Created by Don Clore on 5/15/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit

class BBPRichEditTextStorage: NSTextStorage {
    var backingStore = NSMutableAttributedString()
    
    override var string: String {
        return backingStore.string
    }

    override init() {
        super.init()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func attributesAtIndex(location: Int, effectiveRange range: NSRangePointer) ->
        [String : AnyObject] {
        
        return backingStore.attributesAtIndex(location, effectiveRange: range)
    }
    
    override func replaceCharactersInRange(range: NSRange, withString str: String) {
        beginEditing()
        
        backingStore.replaceCharactersInRange(range, withString: str)
        edited([.EditedCharacters, .EditedAttributes], range: range,
               changeInLength: (str as NSString).length - range.length)
        
        
        endEditing()
    }
    
    override func setAttributes(attrs: [String : AnyObject]?, range: NSRange) {
        beginEditing()
        
        backingStore.setAttributes(attrs, range: range)
        edited(.EditedAttributes, range: range, changeInLength: 0)
        
        endEditing()
    }
    
    func makeBold(range: NSRange, bold: Bool) {
        applyOrRemoveTrait(range, apply: bold, trait: .TraitBold)
    }

    func makeItalic(range: NSRange, italic: Bool) {
        applyOrRemoveTrait(range, apply: italic, trait: .TraitItalic)
    }

    private func applyOrRemoveTrait(range: NSRange, apply: Bool, trait: UIFontDescriptorSymbolicTraits) {
        var attrs :  [String : AnyObject]
        if apply {
            attrs = createAttributesForFontStyle(UIFontTextStyleBody, withTrait: trait)
        } else {
            attrs = [NSFontAttributeName: UIFont.preferredFontForTextStyle(UIFontTextStyleBody)]
        }

        addAttributes(attrs, range: range)
    }


    func createAttributesForFontStyle(style: String,
        withTrait trait: UIFontDescriptorSymbolicTraits) -> [String: AnyObject] {
        
        let fontDescriptor = UIFontDescriptor.preferredFontDescriptorWithTextStyle(UIFontTextStyleBody)
        let descriptorWithTrait = fontDescriptor.fontDescriptorWithSymbolicTraits(trait)
        let font = UIFont(descriptor: descriptorWithTrait, size: 0)
        return [NSFontAttributeName: font]
    }
    
    func markup() -> NSString {
        let documentAttrs = [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType]
        let data = try! backingStore.dataFromRange(NSMakeRange(0, backingStore.length),
                                              documentAttributes: documentAttrs)
        return NSString(data: data, encoding: NSUTF8StringEncoding)!
    }

}
