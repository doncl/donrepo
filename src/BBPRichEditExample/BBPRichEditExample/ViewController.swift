//
//  ViewController.swift
//  BBPRichEditExample
//
//  Created by Don Clore on 5/15/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var richEditView: BBPRichEdit!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        richEditView.initThings()
        richEditView.jamHtmlIntoEditControl("<span style=\"font-family: HelveticaNeue; font-size: 17\"><STRONG>This is bold</STRONG> and this is not." +
                "This is a <a href=\"http://sketchytech.blogspot.com/2016/02/swift-from-html-to-nsattributedtext-and.html\">link</a><p>" +
                    "Here's some <i>italics</i></span>")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showMarkupSegue" {
            let markupVC = segue.destinationViewController as! MarkupViewController
            markupVC.markupText = richEditView.storage.markup() as String
        }
    }
    

}

