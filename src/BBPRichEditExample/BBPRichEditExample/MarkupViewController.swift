//
//  MarkupViewController.swift
//  BBPRichEditExample
//
//  Created by Don Clore on 5/15/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit

class MarkupViewController: UIViewController {
    @IBOutlet var markupView: UITextView!

    var markupText : String!

    override func viewDidLoad() {
        super.viewDidLoad()
        if let markupText = markupText {
            markupView.text = markupText
        }
    }
    
    @IBAction func donePressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
