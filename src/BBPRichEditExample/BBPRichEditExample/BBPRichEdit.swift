//
//  BBPRichEdit.swift
//  BBPRichEditExample
//
//  Created by Don Clore on 5/15/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit

class BBPRichEdit: UIView, UITextViewDelegate {
    var textView : UITextView!
    var storage : BBPRichEditTextStorage!

    func initThings() {
        print("Init things called")
        createTextView()
    }
    
    func createTextView() {
        storage = BBPRichEditTextStorage()
        
        let textViewRect = bounds
        
        let layoutManager = NSLayoutManager()
        
        let containerSize = CGSize(width: textViewRect.width, height: CGFloat.max)
        let container = NSTextContainer(size: containerSize)
        container.widthTracksTextView = true
        layoutManager.addTextContainer(container)
        storage.addLayoutManager(layoutManager)
        
        textView = UITextView(frame: textViewRect, textContainer: container)
        textView.delegate = self
        addSubview(textView)
        textView.scrollEnabled = true
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 100.0, height: 44.0))
        toolbar.tintColor = UIColor.darkGrayColor()

      
        let boldButton = UIBarButtonItem(title: "B", style: .Plain, target: self,
                                         action: #selector(NSObject.toggleBoldface(_:)))

        let italicButton = UIBarButtonItem(title: "I", style: .Plain, target: self,
                                          action: #selector(NSObject.toggleItalics(_:)))

        let items = [boldButton, italicButton]
        
        toolbar.items = items
        textView.inputAccessoryView = toolbar
        //textView.allowsEditingTextAttributes = true
    }
    
    override func toggleBoldface(sender: AnyObject?) {
        textView.toggleBoldface(sender)
    }

    override func toggleItalics(sender: AnyObject?) {
        textView.toggleItalics(sender)
    }

    func jamHtmlIntoEditControl(html: String) {
        let data = html.dataUsingEncoding(NSUTF8StringEncoding)!
        do {
            let bodyFontDescriptor = UIFontDescriptor.preferredFontDescriptorWithTextStyle(UIFontTextStyleBody)
            let bodyFontSize = bodyFontDescriptor.fontAttributes()[UIFontDescriptorSizeAttribute] as! NSNumber
            let attrs = [NSFontAttributeName: UIFont.preferredFontForTextStyle(UIFontTextStyleBody)]
            let attrStr = try NSAttributedString(data: data,
                     options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                     documentAttributes: nil)
        
            
            textView.attributedText = attrStr
        } catch {
            print("Error creating attributed string")
        }
    }

}
