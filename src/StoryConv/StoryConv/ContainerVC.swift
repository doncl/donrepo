//
//  ViewController.swift
//  StoryConv
//
//  Created by Don Clore on 12/26/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ContainerVC: UIViewController {
  var gradient : CAGradientLayer  = CAGradientLayer()
  @IBOutlet var segmentedControl: UISegmentedControl!
  
  private lazy var catVC : CatVC = {
    return getVC(storyboardId: "catVCId", type: CatVC.self)
  }()
  
  private lazy var kittenVC : KittenVC = {
    return getVC(storyboardId: "kittenVCId", type: KittenVC.self)
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    segmentedControl.selectedSegmentIndex = 0
    add(asChildViewController: kittenVC)
    add(asChildViewController: catVC)
  }
  
  fileprivate func bottomXForm() -> CATransform3D {
    return CATransform3DMakeTranslation(view.frame.width, 0, 0)
  }
  

  @IBAction func selectionDidChange(_ sender: UISegmentedControl) {
    updateView()
  }
}

extension ContainerVC {
  fileprivate func updateView() {
    if segmentedControl.selectedSegmentIndex == 0 {
      shuffleViews(bottom: catVC.view, top: kittenVC.view)
    } else {
      shuffleViews(bottom: kittenVC.view, top: catVC.view)
    }
  }
  
  fileprivate func shuffleViews(bottom: UIView, top: UIView) {
    segmentedControl.isEnabled = false
    top.layer.shouldRasterize = true
    top.layer.rasterizationScale = UIScreen.main.scale
    bottom.layer.shouldRasterize = true
    bottom.layer.rasterizationScale = UIScreen.main.scale
    let identity = CATransform3DIdentity
    let newXform = bottomXForm()
    
    UIView.animate(withDuration: 1.0, delay: 0.0, options: [.curveEaseInOut], animations: {
      bottom.layer.transform = identity
      top.layer.transform = newXform
    }, completion: { _ in
      self.view.insertSubview(bottom, aboveSubview: top)
      bottom.layer.transform = identity
      top.layer.transform = identity
      self.segmentedControl.isEnabled = true
    })
  }
  
//  fileprivate func shuffleViews(bottom: UIView, top: UIView) {
//    inProgress = true
//    top.layer.shouldRasterize = true
//    top.layer.rasterizationScale = UIScreen.main.scale
//    bottom.layer.shouldRasterize = true
//    bottom.layer.rasterizationScale = UIScreen.main.scale
//    let offToSide = topToRight()
//    let rotateAndScale = topRotateAndScale(starting: offToSide)
//    let offToSideAndBack = straightenOutInBack(starting: rotateAndScale)
//    let directlyInBack = CATransform3DTranslate(CATransform3DIdentity, 0, 0.0, -100.0)
//    let bottomRotate = bottomRotationXForm()
//    view.sendSubview(toBack: bottom)
//    bottom.layer.transform = CATransform3DScale(CATransform3DMakeTranslation(0, 0, -100.0),
//      0.8, 0.8, 0.8)
//
//    UIView.animateKeyframes(withDuration: 1.0, delay: 0.0, options: [.calculationModeCubicPaced], animations: {
//      UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.2, animations: {
//        top.layer.transform = offToSide
//        bottom.layer.transform = bottomRotate
//      })
//
//      UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.2, animations: {
//        top.layer.transform = rotateAndScale
//      })
//
//      UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.1, animations: {
//        top.layer.transform = offToSideAndBack
//      })
//
//      UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.1, animations: {
//        top.layer.transform = directlyInBack
//      })
//
//      UIView.addKeyframe(withRelativeStartTime: 0.8, relativeDuration: 0.2, animations: {
//        bottom.layer.transform = CATransform3DIdentity
//      })
//    }, completion: { _ in
//      self.view.sendSubview(toBack: top)
//      top.layer.transform = CATransform3DIdentity
//      bottom.layer.transform = CATransform3DIdentity
//      self.inProgress = false
//    })
//  }
//
//  fileprivate func bottomRotationXForm() -> CATransform3D {
//    var identity = CATransform3DIdentity
//    identity.m34 = -1.0/1000
//    let angle = CGFloat(0.3) * .pi * CGFloat(0.5)
//    let rotationTransform = CATransform3DRotate(identity, angle, 0.0, 1.0, 0.0)
//    let scaleRotate = CATransform3DScale(rotationTransform, 0.9, 0.9, 0.4)
//    let finalXForm = CATransform3DTranslate(scaleRotate, -(view.frame.width * 0.2), 0, 0)
//    return finalXForm
//  }
//
//  fileprivate func topToRight() -> CATransform3D {
//    return CATransform3DMakeTranslation(view.frame.width, 0, 0)
//  }
//
//  fileprivate func topRotateAndScale(starting : CATransform3D) -> CATransform3D {
//    let angle = CGFloat(0.6) * .pi * CGFloat(-0.5)
//    let rotate = CATransform3DRotate(starting, angle, 0.0, 1.0, 0.0)
//    let rotateAndScale = CATransform3DScale(rotate, 0.6, 0.6, 0.4)
//    return rotateAndScale
//  }
//
//  fileprivate func straightenOutInBack(starting: CATransform3D) -> CATransform3D {
//    let pushedBack = CATransform3DTranslate(starting, 0, 0, -100)
//    let angle = CGFloat(0.6) * .pi * CGFloat(0.5)
//    let rotate = CATransform3DRotate(pushedBack, angle, 0.0, 1.0, 0.0)
//    return rotate
//  }
}

extension ContainerVC : CAAnimationDelegate {
  func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
//    if false == inProgress {
//      gradient.removeFromSuperlayer()
//      return
//    }
//    self.animateLayer(gradient: self.gradient,
//      fromColors: [UIColor.black.cgColor, UIColor.clear.cgColor],
//      toColors: [UIColor.clear.cgColor, UIColor.clear.cgColor], delegate: nil)
    
    if let animId = anim.value(forKey: "animationId") as? String {
      if animId == "topAnimation" {
        print("found top animation")
      } else if animId == "bottomAnimation" {
        print("found bottom")
      }
    }
    kittenVC.view.layer.transform = CATransform3DIdentity
    catVC.view.layer.transform = CATransform3DIdentity
  }
}

extension ContainerVC {
  fileprivate func getVC<T : UIViewController>(storyboardId id : String, type : T.Type) -> T {
    let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
    guard let vc = storyBoard.instantiateViewController(withIdentifier: id) as? T
      else {
        
        fatalError("You're kidding, right?")
    }
    add(asChildViewController: vc)
    return vc
  }
  
  fileprivate func add(asChildViewController viewController: UIViewController) {
    addChildViewController(viewController)
    view.addSubview(viewController.view)
    viewController.view.frame = view.bounds
    viewController.didMove(toParentViewController: self)
    
    let child = viewController.view!

    child.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      child.topAnchor.constraint(equalTo: view.topAnchor),
      child.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      child.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      child.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    ])
  }
  
  fileprivate func remove(asChildViewController viewController: UIViewController) {
    viewController.willMove(toParentViewController: nil)
    viewController.view.removeFromSuperview()
    viewController.removeFromParentViewController()
  }
}

