//
//  BirthdayVC.swift
//  Birthday
//
//  Created by Don Clore on 11/20/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class BirthdayVC: UIViewController {
    @IBOutlet var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let gif = Bundle.main.url(forResource: "birthday", withExtension: "gif") else {
            fatalError("can't find gif")
        }

        imageView.image = UIImage.animatedImage(withAnimatedGIFURL: gif)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }


}
