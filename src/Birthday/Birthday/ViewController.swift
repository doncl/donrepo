//
//  ViewController.swift
//  Birthday
//
//  Created by Don Clore on 11/20/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

enum BirthdayState {
    case itsyourbirthday
    case daybeforeyourbirthday
    case everythingelse
}

class ViewController: UIViewController {
    let now: Date = Date()

    private let dayCountInMonths: [Int] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,]

    @IBOutlet var todaysDateLabel: UILabel!
    @IBOutlet var todaysCardinalityLabel: UILabel!
    @IBOutlet var birthdayDatePicker: UIDatePicker!
    @IBOutlet var numDaysTillBirhdayLabel: UILabel!

    @discardableResult
    fileprivate func updateLabelsWithDateInfo() -> BirthdayState {
        let numberOfDaysSoFar = calculateAbsoluteDate(now)
        todaysCardinalityLabel.text = "It is \(numberOfDaysSoFar) days since the year's start."
        let daysTillBirthday = calculateDaysUntilBirthday(daysSoFar: numberOfDaysSoFar)
        numDaysTillBirhdayLabel.text = "It is \(daysTillBirthday) days until your birthday."

        switch daysTillBirthday {
        case 0:
            return .itsyourbirthday
        case 1:
            return .daybeforeyourbirthday
        default:
            return .everythingelse
        }
    }

    fileprivate func setTodaysDateLabel() {
        let df = DateFormatter()
        df.dateStyle = .medium
        df.timeStyle = .none
        let dateString = df.string(from: now)
        todaysDateLabel.text = "Today: \(dateString)"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setTodaysDateLabel()
        updateLabelsWithDateInfo()
    }

    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        let state = updateLabelsWithDateInfo()
        guard state != .everythingelse else {
            return
        }
        if state == .itsyourbirthday {
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let birthdayVC = sb.instantiateViewController(withIdentifier: "birthdayVCId")
            present(birthdayVC, animated: true, completion: nil)
        } else if state == .daybeforeyourbirthday {
            let ac = UIAlertController(title: "YAYYY!", message: "Tomorrow's your birthday!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.dismiss(animated: true, completion: nil)
            })
            ac.addAction(ok)
            present(ac, animated: true, completion: nil)
        }
    }
}

extension ViewController {
    func calculateDaysUntilBirthday(daysSoFar: Int) -> Int {
        let dateInPicker = birthdayDatePicker.date
        let absoluteDaysForBirthday = calculateAbsoluteDate(dateInPicker)
        if absoluteDaysForBirthday < daysSoFar {
            return 365 - daysSoFar + absoluteDaysForBirthday
        } else {
            return absoluteDaysForBirthday - daysSoFar
        }
    }

    func calculateAbsoluteDate(_ date: Date) -> Int {
        let month = Calendar.current.component(.month, from: date)
        let days = Calendar.current.component(.day, from: date)

        if month == 1 {
            return days
        }
        let slice = dayCountInMonths[0...month - 2]
        let numberOfDaysForMonth = slice.reduce(0, +)

        return numberOfDaysForMonth + days
    }
}

