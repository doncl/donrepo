- (void)setupButtonRoundedCorners
{
    CGSize cornerRadius = CGSizeMake(4.0, 4.0);
    self.cancelPhotoBtn.layer.cornerRadius = cornerRadius.height;
    CAShapeLayer *topCornerMask = [self roundedCorner:self.photoLibraryBtn.bounds onCorner:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:cornerRadius];
    CAShapeLayer *bottomCornerMask = [self roundedCorner:self.takePhotoOrVideoBtn.bounds onCorner:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:cornerRadius];
    self.photoLibraryBtn.layer.mask = topCornerMask;
    self.takePhotoOrVideoBtn.layer.mask = bottomCornerMask;
}

- (CAShapeLayer *) roundedCorner:(CGRect)bounds onCorner:(UIRectCorner)rectCorner cornerRadii:(CGSize)cornerRadii
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:bounds byRoundingCorners:rectCorner cornerRadii:cornerRadii];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = bounds;
    maskLayer.path = maskPath.CGPath;
    return maskLayer;
}
