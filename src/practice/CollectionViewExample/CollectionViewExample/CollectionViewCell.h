//
//  CollectionViewCell.h
//  CollectionViewExample
//
//  Created by Don Clore on 7/27/15.
//  Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
