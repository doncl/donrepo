//
//  CollectionViewCell.m
//  CollectionViewExample
//
//  Created by Don Clore on 7/27/15.
//  Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "CollectionViewCell.h"
#import <UIKit/UIKit.h>

@implementation CollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"NibCell" owner:self options:nil];

        if ([arrayOfViews count] < 1) {
            return nil;
        }

        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }

        self = [arrayOfViews objectAtIndex:0];
    }

    return self;
}

@end
