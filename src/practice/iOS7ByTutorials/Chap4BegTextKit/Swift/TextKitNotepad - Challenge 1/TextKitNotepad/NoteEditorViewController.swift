//
//  DetailViewController.swift
//  TextKitNotepad
//
//  Created by Colin Eberhardt on 24/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class NoteEditorViewController: UIViewController, UITextViewDelegate {
  
  @IBOutlet weak var editButton: UIBarButtonItem!
  var note: Note!
  
  fileprivate var timeView: TimeIndicatorView!
  
  fileprivate var textStorage: SyntaxHighlightTextStorage!
  fileprivate var textView: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    createTextView()
    
    NotificationCenter.default
      .addObserver(self, selector: #selector(NoteEditorViewController.preferredContentSizeChanged(_:)),
          name: NSNotification.Name.UIContentSizeCategoryDidChange, object: nil)

    timeView = TimeIndicatorView(date: note.timestamp)
    view.addSubview(timeView)
    
    NotificationCenter.default.addObserver(self, selector: #selector(NoteEditorViewController.keyboardShown(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(NoteEditorViewController.keyboardHidden(_:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
  }
  
  func keyboardShown(_ notification: Notification) {
    let info  = notification.userInfo!
    let value: AnyObject = info[UIKeyboardFrameEndUserInfoKey]! as AnyObject
    let keyboardFrame = value.cgRectValue
    
    var textViewFrame = view.bounds
    textViewFrame.size.height -= (keyboardFrame?.height)!
    textView.frame = textViewFrame
  }
  
  func keyboardHidden(_ notification: Notification) {
    textView.frame = view.bounds
  }
  
  fileprivate func createTextView() {
    // 1. Create the text storage that backs the editor
    let attrs = [NSFontAttributeName : UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
    let attrString = NSAttributedString(string: note.contents, attributes: attrs)
    textStorage = SyntaxHighlightTextStorage()
    textStorage.append(attrString)
    
    let newTextViewRect = view.bounds
    
    // 2. Create the layout manager
    let layoutManager = NSLayoutManager()
    
    // 3. Create a text container
    let containerSize = CGSize(width: newTextViewRect.size.width,
      height: CGFloat.greatestFiniteMagnitude)
    let container = NSTextContainer(size: containerSize)
    container.widthTracksTextView = true
    layoutManager.addTextContainer(container)
    textStorage.addLayoutManager(layoutManager)
    
    // 4. Create a UITextView
    textView = UITextView(frame: newTextViewRect, textContainer: container)
    textView.delegate = self
    view.addSubview(textView)
    
    // ensure that the text view is not editable initially
    textView.isEditable = false;
    textView.dataDetectorTypes = .link
  }
  
  func preferredContentSizeChanged(_ notification: Notification) {
    textView.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)
    updateTimeIndicatorFrame()
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    note.contents = textView.text!
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    updateTimeIndicatorFrame()
    textView.frame = view.bounds
  }
  
  fileprivate func updateTimeIndicatorFrame() {
    timeView.updateSize()
    timeView.frame = timeView.frame.offsetBy(dx: self.view.frame.size.width - timeView.frame.size.width,
      dy: textView.frame.origin.y)
    
    let exclusionPath = timeView.curvePathWithOrigin(timeView.center)
    textView.textContainer.exclusionPaths = [exclusionPath]
  }
  
  @IBAction func editButtonTapped(_ sender: AnyObject) {
    if textView.isEditable {
      editButton.title = "Edit"
      textView.isEditable = false
      textView.resignFirstResponder()
    } else {
      editButton.title = "Done"
      textView.isEditable = true
      textView.becomeFirstResponder()
    }
  }
}

