//
//  MasterViewController.swift
//  TextKitNotepad
//
//  Created by Colin Eberhardt on 24/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class NotesListViewController: UITableViewController {

  fileprivate var notes: [Note] {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    return appDelegate.notes
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    
    NotificationCenter.default
      .addObserver(self, selector: #selector(NotesListViewController.preferredContentSizeChanged(_:)),
        name: NSNotification.Name.UIContentSizeCategoryDidChange, object: nil)
  }
  
  func preferredContentSizeChanged(_ notification: Notification) {
    tableView.reloadData()
  }

  override func viewDidAppear(_ animated: Bool) {
    // Whenever this view controller appears, reload the table. This allows it to reflect any changes
    // made whilst editing notes.
    tableView.reloadData()
    
    super.viewDidAppear(animated)
  }


  // MARK: - Segues

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    let editorVC = segue.destination as! NoteEditorViewController
    
    if segue.identifier == "CellSelected" {
      // if the cell selected segue was fired, edit the selected note
      let path = tableView.indexPathForSelectedRow!
      editorVC.note = notes[path.row]
    }
    
    if segue.identifier == "AddNewNote" {
      // if the add new note segue was fired, create a new note, and edit it
      editorVC.note = Note(text: " ")
      // also, add this note to the collection
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
      appDelegate.notes.append(editorVC.note)
    }
  }

  // MARK: - Table View

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return notes.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell

    let note = notes[indexPath.row]
    
    let font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
    
    let textColor = UIColor(red: 0.175, green: 0.458, blue: 0.831, alpha: 1.0)
    let attrs = [NSForegroundColorAttributeName : textColor,
      NSTextEffectAttributeName : NSTextEffectLetterpressStyle] as [String : Any]
    
    let attrString = NSAttributedString(string: note.title, attributes: attrs)
    
    cell.textLabel?.attributedText = attrString
    
    return cell
  }
  
  override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return 20.0
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    struct Static {
      static var label: UILabel!
    }
    
    if Static.label == nil {
      let label = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat(FLT_MAX), height: CGFloat(FLT_MAX)))
      label.text = "test"
      Static.label = label
    }
    
    Static.label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
    Static.label.sizeToFit()
    return ceil(Static.label.frame.size.height * 1.7)
  }
}

