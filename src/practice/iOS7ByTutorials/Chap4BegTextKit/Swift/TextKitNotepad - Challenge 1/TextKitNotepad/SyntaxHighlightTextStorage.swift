//
//  SyntaxHighlightTextStorage.swift
//  TextKitNotepad
//
//  Created by Colin Eberhardt on 24/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class SyntaxHighlightTextStorage: NSTextStorage {
  fileprivate let backingStore = NSMutableAttributedString()
  
  fileprivate let replacements = SyntaxHighlightTextStorage.createHighlightPatterns()
  
  override var string : String {
    get {
      return backingStore.string
    }
  }
  

  override func attributes(at location: Int, effectiveRange range: NSRangePointer?)
    -> [String : Any] {
      
    return backingStore.attributes(at: location, effectiveRange: range)
  }
  
  override func replaceCharacters(in range: NSRange, with str: String) {
    print("replaceCharactersInRange(\(NSStringFromRange(range))," + " withString: \(str))")
    
    beginEditing()
    backingStore.replaceCharacters(in: range, with: str)
    
    edited([.editedAttributes, .editedCharacters],
      range: range, changeInLength: str.characters.count - range.length)
    
    endEditing()
  }

  override func setAttributes(_ attrs: [String : Any]?, range: NSRange) {
    print("setAttributes(\(attrs)," + " range: \(NSStringFromRange(range)))")
    
    beginEditing()
    
    backingStore.setAttributes(attrs, range: range)
    
    edited(NSTextStorageEditActions.editedAttributes, range: range, changeInLength: 0)
    
    endEditing()
  }
  
  override func processEditing() {
    performReplacementsForRange(editedRange)
    super.processEditing()
  }
  
  
  fileprivate func performReplacementsForRange(_ changedRange: NSRange) {
    let nsString = backingStore.string as NSString
    let startOfRange = nsString.lineRange(for: NSMakeRange(changedRange.location, 0))
    let endOfRange = nsString.lineRange(for: NSMakeRange(NSMaxRange(changedRange), 0))
    
    var extendedRange = NSUnionRange(changedRange, startOfRange)
    extendedRange = NSUnionRange(extendedRange, endOfRange)
    
    applyStylesToRange(extendedRange)
  }
  
  fileprivate func applyStylesToRange(_ searchRange: NSRange) {

    let normalAttrs = [NSFontAttributeName : UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
    
    // iterate over each replacement
    for (pattern, attributes) in replacements {
      let regex = try! NSRegularExpression(pattern: pattern, options: [])
      
      regex.enumerateMatches(in: backingStore.string, options: [], range: searchRange) {
        (match, flags, stop) in
        
        // apply the style
        if let matchRange = match?.range {
          self.addAttributes(attributes, range: matchRange)
        
          // reset the style to the original
          if NSMaxRange(matchRange) + 1 < self.length {
            self.addAttributes(normalAttrs, range: NSMakeRange(NSMaxRange(matchRange)+1, 1))
        }
      }
      }
    }
    
    highlightHyperlinksInRange(searchRange)
  }
  
  fileprivate func highlightHyperlinksInRange(_ searchrange: NSRange) {
    // I found this on the web ;-)
    let regex = try! NSRegularExpression(pattern: "((http|ftp|https):\\/\\/[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?)", options: [.caseInsensitive])
    
    regex.enumerateMatches(in: backingStore.string, options: [], range: searchrange) {
      (match, flags, stop) in
      
      // create a url using the matched string
      if let matchRange = match?.rangeAt(1) {
        let nsString = self.backingStore.string as NSString
        let url = NSURL(string: nsString.substring(with: matchRange))!
      
        // apply a blue underline style - and add an NSLinkAttributeName attribute
        let attributes: [String:AnyObject] = [NSForegroundColorAttributeName : UIColor.blue,
          NSUnderlineStyleAttributeName : 1 as AnyObject, NSLinkAttributeName : url]
      self.addAttributes(attributes, range: matchRange)
    }
    }
  }
  
  fileprivate class func createHighlightPatterns() -> [String:[String:AnyObject]] {

    let scriptFontDescriptor = UIFontDescriptor(fontAttributes: [UIFontDescriptorFamilyAttribute: "Zapfino"])

    // 1. base our script font on the preferred body font size
    let bodyFontDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.body)
    let bodyFontSize = bodyFontDescriptor.fontAttributes[UIFontDescriptorSizeAttribute]! as! CGFloat
    let scriptFont = UIFont(descriptor: scriptFontDescriptor, size: bodyFontSize)
    
    // 2. create the attributes
    let boldAttributes = createAttributesForFontStyle(UIFontTextStyle.body.rawValue, trait: .traitBold)
    let italicAttributes = createAttributesForFontStyle(UIFontTextStyle.body.rawValue, trait: .traitItalic)
    let scriptAttributes: [String:AnyObject] = [NSFontAttributeName : scriptFont]
    let strikeThroughAttributes: [String:AnyObject] = [NSStrikethroughStyleAttributeName : 1 as AnyObject]
    let redTextAttributes: [String:AnyObject] = [NSForegroundColorAttributeName : UIColor.red]
    
    // construct a dictionary of replacements based on regexes
    return [
      "(\\*\\w+(\\s\\w+)*\\*)\\s" : boldAttributes,
      "(_\\w+(\\s\\w+)*_)\\s" : italicAttributes,
      "([0-9]+\\.)\\s" : boldAttributes,
      "(-\\w+(\\s\\w+)*-)\\s" : strikeThroughAttributes,
      "(~\\w+(\\s\\w+)*~)\\s" : scriptAttributes,
      "\\s([A-Z]{2,})\\s" : redTextAttributes
    ]
  }
  
  fileprivate class func createAttributesForFontStyle(_ style: String, trait: UIFontDescriptorSymbolicTraits)
    -> [String:AnyObject] {
   let fontDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle(rawValue: style))
    let descriptorWithTrait = fontDescriptor.withSymbolicTraits(trait)
    let font = UIFont(descriptor: descriptorWithTrait!, size: 0.0)
    return [NSFontAttributeName : font]
  }
  
}
