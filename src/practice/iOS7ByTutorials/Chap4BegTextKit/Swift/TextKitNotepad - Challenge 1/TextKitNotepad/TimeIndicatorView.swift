//
//  TimeIndicatorView.swift
//  TextKitNotepad
//
//  Created by Colin Eberhardt on 24/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class TimeIndicatorView: UIView {
  
  fileprivate let date: Date
  fileprivate let label: UILabel
  
  init(date: Date) {
    self.date = date
    
    // Initialization code
    label = UILabel()
    label.textAlignment = .right
    
    // format and style the date
    let format = DateFormatter()
    format.dateFormat = "dd\rMMMM\ryyyy"
    let formattedDate = format.string(from: date)
    label.text = formattedDate.uppercased()
    label.textAlignment = .center
    label.textColor = UIColor.white
    label.numberOfLines = 0
    
    super.init(frame: CGRect.zero)
    
    backgroundColor = UIColor.clear
    clipsToBounds = false
    
    addSubview(label)
  }

  required init(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
  }
  
  func updateSize() {
    // size the label based on the font
    label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
    label.frame = CGRect(x: 0, y: 0, width: CGFloat(FLT_MAX), height: CGFloat(FLT_MAX))
    label.sizeToFit()
    
    // set the frame to be large enough to accomodate circle that surrounds the text
    let radius = radiusToSurroundFrame(label.frame)
    frame = CGRect(x: 0, y: 0, width: radius*2, height: radius*2);
    
    // center the label within this circle
    label.center = center;
    
    // offset the center of this view to ... erm ... can I just draw you a picture?
    // You know the story - the designer provides a mock-up with some static data, leaving
    // you to work out the complex calculations required to accomodate the variability of real-world
    // data. C'est la vie!
    let padding: CGFloat = 5.0
    self.center = CGPoint(x: self.center.x + label.frame.origin.x - padding,
      y: self.center.y - label.frame.origin.y + padding);
    
  }
  
  // calculates the radius of the circle that surrounds the label
  fileprivate func radiusToSurroundFrame(_ frame: CGRect) -> CGFloat {
    return max(frame.size.width, frame.size.height) * 0.5 + 20.0
  }
  
  func curvePathWithOrigin(_ origin: CGPoint) -> UIBezierPath {
    return UIBezierPath(arcCenter: origin, radius: radiusToSurroundFrame(label.frame), startAngle: -180.0, endAngle: 180.0, clockwise: true)
  }
  
  override func draw(_ rect: CGRect) {
    //super.drawRect(rect)
    let ctx = UIGraphicsGetCurrentContext()
    ctx?.setShouldAntialias(true)
    let path = curvePathWithOrigin(label.center)
    
    UIColor(red: 0.329, green: 0.584, blue: 0.898, alpha: 1.0).setFill()
    path.fill()
  }
}

