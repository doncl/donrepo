//
//  TimeIndicatorView.swift
//  TextKitNotepad
//
//  Created by Colin Eberhardt on 24/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class TimeIndicatorView: UIView {
  
  private let date: NSDate
  private let label: UILabel
  
  init(date: NSDate) {
    self.date = date
    
    // Initialization code
    label = UILabel()
    label.textAlignment = .Right
    
    // format and style the date
    let format = NSDateFormatter()
    format.dateFormat = "dd\rMMMM\ryyyy"
    let formattedDate = format.stringFromDate(date)
    label.text = formattedDate.uppercaseString
    label.textAlignment = .Center
    label.textColor = UIColor.whiteColor()
    label.numberOfLines = 0
    
    super.init(frame: CGRectZero)
    
    backgroundColor = UIColor.clearColor()
    clipsToBounds = false
    
    addSubview(label)
  }

  required init(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
  }
  
  func updateSize() {
    // size the label based on the font
    label.font = UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)
    label.frame = CGRectMake(0, 0, CGFloat(FLT_MAX), CGFloat(FLT_MAX))
    label.sizeToFit()
    
    // set the frame to be large enough to accomodate circle that surrounds the text
    let radius = radiusToSurroundFrame(label.frame)
    frame = CGRectMake(0, 0, radius*2, radius*2);
    
    // center the label within this circle
    label.center = center;
    
    // offset the center of this view to ... erm ... can I just draw you a picture?
    // You know the story - the designer provides a mock-up with some static data, leaving
    // you to work out the complex calculations required to accomodate the variability of real-world
    // data. C'est la vie!
    let padding: CGFloat = 5.0
    self.center = CGPointMake(self.center.x + label.frame.origin.x - padding,
      self.center.y - label.frame.origin.y + padding);
    
  }
  
  // calculates the radius of the circle that surrounds the label
  private func radiusToSurroundFrame(frame: CGRect) -> CGFloat {
    return max(frame.size.width, frame.size.height) * 0.5 + 20.0
  }
  
  func curvePathWithOrigin(origin: CGPoint) -> UIBezierPath {
    return UIBezierPath(arcCenter: origin, radius: radiusToSurroundFrame(label.frame), startAngle: -180.0, endAngle: 180.0, clockwise: true)
  }
  
  override func drawRect(rect: CGRect) {
    //super.drawRect(rect)
    let ctx = UIGraphicsGetCurrentContext()
    CGContextSetShouldAntialias(ctx, true)
    let path = curvePathWithOrigin(label.center)
    
    UIColor(red: 0.329, green: 0.584, blue: 0.898, alpha: 1.0).setFill()
    path.fill()
  }
}

