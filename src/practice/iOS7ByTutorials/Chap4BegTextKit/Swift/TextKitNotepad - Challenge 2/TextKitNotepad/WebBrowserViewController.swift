//
//  WebBrowserViewController.swift
//  TextKitNotepad
//
//  Created by Colin Eberhardt on 26/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class WebBrowserViewController: UIViewController {
  
  var url: NSURL!
  
  @IBOutlet weak var webView: UIWebView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let request = NSURLRequest(URL: url)
    webView.loadRequest(request)
  }
}
  
