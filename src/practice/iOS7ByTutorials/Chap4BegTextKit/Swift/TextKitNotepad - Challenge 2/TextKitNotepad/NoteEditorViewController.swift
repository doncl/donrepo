//
//  DetailViewController.swift
//  TextKitNotepad
//
//  Created by Colin Eberhardt on 24/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class NoteEditorViewController: UIViewController, UITextViewDelegate {
  
  @IBOutlet weak var editButton: UIBarButtonItem!
  var note: Note!
  
  private var timeView: TimeIndicatorView!
  private var tappedUrl: NSURL!
  
  private var textStorage: SyntaxHighlightTextStorage!
  private var textView: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    createTextView()
    
    NSNotificationCenter.defaultCenter()
      .addObserver(self, selector: "preferredContentSizeChanged:",
          name: UIContentSizeCategoryDidChangeNotification, object: nil)

    timeView = TimeIndicatorView(date: note.timestamp)
    view.addSubview(timeView)
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardShown:", name: UIKeyboardDidShowNotification, object: nil)
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardHidden:", name: UIKeyboardDidHideNotification, object: nil)
  }
  
  func keyboardShown(notification: NSNotification) {
    let info  = notification.userInfo!
    let value: AnyObject = info[UIKeyboardFrameEndUserInfoKey]!
    let keyboardFrame = value.CGRectValue()
    
    var textViewFrame = view.bounds
    textViewFrame.size.height -= keyboardFrame.height
    textView.frame = textViewFrame
  }
  
  func keyboardHidden(notification: NSNotification) {
    textView.frame = view.bounds
  }
  
  private func createTextView() {
    // 1. Create the text storage that backs the editor
    let attrs = [NSFontAttributeName : UIFont.preferredFontForTextStyle(UIFontTextStyleBody)]
    let attrString = NSAttributedString(string: note.contents, attributes: attrs)
    textStorage = SyntaxHighlightTextStorage()
    textStorage.appendAttributedString(attrString)
    
    let newTextViewRect = view.bounds
    
    // 2. Create the layout manager
    let layoutManager = NSLayoutManager()
    
    // 3. Create a text container
    let containerSize = CGSizeMake(newTextViewRect.size.width,
      CGFloat.max)
    let container = NSTextContainer(size: containerSize)
    container.widthTracksTextView = true
    layoutManager.addTextContainer(container)
    textStorage.addLayoutManager(layoutManager)
    
    // 4. Create a UITextView
    textView = UITextView(frame: newTextViewRect, textContainer: container)
    textView.delegate = self
    view.addSubview(textView)
    
    // ensure that the text view is not editable initially
    textView.editable = false;
    textView.dataDetectorTypes = .Link
  }
  
  func preferredContentSizeChanged(notification: NSNotification) {
    textView.font = UIFont.preferredFontForTextStyle(UIFontTextStyleBody)
    updateTimeIndicatorFrame()
  }
  
  func textViewDidEndEditing(textView: UITextView) {
    note.contents = textView.text!
  }
  
  func textView(textView: UITextView, shouldInteractWithURL URL: NSURL, inRange characterRange: NSRange) -> Bool {
    tappedUrl = URL
    performSegueWithIdentifier("showBrowser", sender:self)
    return false
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    updateTimeIndicatorFrame()
    textView.frame = view.bounds
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    super.prepareForSegue(segue, sender: sender)
    
    if segue.identifier == "showBrowser" {
      let browserVC = segue.destinationViewController as WebBrowserViewController
      browserVC.url = tappedUrl
    }
  }
  
  private func updateTimeIndicatorFrame() {
    timeView.updateSize()
    timeView.frame = CGRectOffset(timeView.frame,
      self.view.frame.size.width - timeView.frame.size.width,
      textView.frame.origin.y)
    
    let exclusionPath = timeView.curvePathWithOrigin(timeView.center)
    textView.textContainer.exclusionPaths = [exclusionPath]
  }
  
  @IBAction func editButtonTapped(sender: AnyObject) {
    if textView.editable {
      editButton.title = "Edit"
      textView.editable = false
      textView.resignFirstResponder()
    } else {
      editButton.title = "Done"
      textView.editable = true
      textView.becomeFirstResponder()
    }
  }
}

