//
//  AppDelegate.swift
//  TextKitNotepad
//
//  Created by Colin Eberhardt on 24/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  
  lazy var notes: [Note] = {
      [Note(text: "Shopping List\r\r1. Cheese\r2. Biscuits\r3. Sausages\r4. IMPORTANT Cash for going out!\r5. -potatoes-\r6. A copy of iOS6 by tutorials\r7. A new iPhone\r8. A present for mum"),
      Note(text: "Meeting notes\rA long and drawn out meeting, it lasted hours and hours and hours!"),
      Note(text: "Perfection ... \n\nPerfection is achieved not when there is nothing left to add, but when there is nothing left to take away - Antoine de Saint-Exupery"),
      Note(text: "Notes on iOS7\nThis is a big change in the UI design, it's going to take a *lot* of getting used to!"),
      Note(text: "Meeting notes\rA dfferent meeting, just as long and boring"),
      Note(text: "A collection of thoughts\rWhy do birds sing? Why is the sky blue? Why is it so hard to create good test data?")]
  }()


  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    
    // style the navigation bar
    let navColor = UIColor(red: 0.175, green: 0.458, blue: 0.831, alpha: 1.0)
    UINavigationBar.appearance().barTintColor = navColor
    UINavigationBar.appearance().tintColor = UIColor.whiteColor()
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
   
    // make the status bar white
    UIApplication.sharedApplication().statusBarStyle = .LightContent
    
    return true
  }
}

