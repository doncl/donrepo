//
//  MasterViewController.swift
//  TextKitNotepad
//
//  Created by Colin Eberhardt on 24/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class NotesListViewController: UITableViewController {

  private var notes: [Note] {
    let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    return appDelegate.notes
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    
    NSNotificationCenter.defaultCenter()
      .addObserver(self, selector: "preferredContentSizeChanged:",
        name: UIContentSizeCategoryDidChangeNotification, object: nil)
  }
  
  func preferredContentSizeChanged(notification: NSNotification) {
    tableView.reloadData()
  }

  override func viewDidAppear(animated: Bool) {
    // Whenever this view controller appears, reload the table. This allows it to reflect any changes
    // made whilst editing notes.
    tableView.reloadData()
    
    super.viewDidAppear(animated)
  }


  // MARK: - Segues

  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

    let editorVC = segue.destinationViewController as NoteEditorViewController
    
    if segue.identifier == "CellSelected" {
      // if the cell selected segue was fired, edit the selected note
      let path = tableView.indexPathForSelectedRow()!
      editorVC.note = notes[path.row]
    }
    
    if segue.identifier == "AddNewNote" {
      // if the add new note segue was fired, create a new note, and edit it
      editorVC.note = Note(text: " ")
      // also, add this note to the collection
      let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
      appDelegate.notes.append(editorVC.note)
    }
  }

  // MARK: - Table View

  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return notes.count
  }

  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell

    let note = notes[indexPath.row]
    
    let font = UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)
    
    let textColor = UIColor(red: 0.175, green: 0.458, blue: 0.831, alpha: 1.0)
    let attrs = [NSForegroundColorAttributeName : textColor,
      NSTextEffectAttributeName : NSTextEffectLetterpressStyle]
    
    let attrString = NSAttributedString(string: note.title, attributes: attrs)
    
    cell.textLabel.attributedText = attrString
    
    return cell
  }
  
  override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 20.0
  }
  
  override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    struct Static {
      static var label: UILabel!
    }
    
    if Static.label == nil {
      let label = UILabel(frame: CGRectMake(0, 0, CGFloat(FLT_MAX), CGFloat(FLT_MAX)))
      label.text = "test"
      Static.label = label
    }
    
    Static.label.font = UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)
    Static.label.sizeToFit()
    return ceil(Static.label.frame.size.height * 1.7)
  }
}

