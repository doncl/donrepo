//
//  SyntaxHighlightTextStorage.swift
//  TextKitNotepad
//
//  Created by Colin Eberhardt on 24/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class SyntaxHighlightTextStorage: NSTextStorage {
  private let backingStore = NSMutableAttributedString()
  
  private let replacements = SyntaxHighlightTextStorage.createHighlightPatterns()
  
  func string() -> String {
    return backingStore.string
  }

  override func attributesAtIndex(location: Int, effectiveRange range: NSRangePointer) -> [NSObject : AnyObject] {
    return backingStore.attributesAtIndex(location, effectiveRange: range)
  }
  
  override func replaceCharactersInRange(range: NSRange, withString str: String) {
    println("replaceCharactersInRange(\(NSStringFromRange(range))," + " withString: \(str))")
    
    beginEditing()
    backingStore.replaceCharactersInRange(range, withString: str)
    edited(NSTextStorageEditActions.EditedAttributes |
      NSTextStorageEditActions.EditedCharacters,
      range: range, changeInLength: countElements(str) - range.length)
    endEditing()
  }

  override func setAttributes(attrs: [NSObject:AnyObject]?, range: NSRange) {
    println("setAttributes(\(attrs)," + " range: \(NSStringFromRange(range)))")
    
    beginEditing()
    backingStore.setAttributes(attrs, range: range)
    edited(NSTextStorageEditActions.EditedAttributes,
      range: range, changeInLength: 0)
    endEditing()
  }
  
  override func processEditing() {
    performReplacementsForRange(editedRange)
    super.processEditing()
  }
  
  
  private func performReplacementsForRange(changedRange: NSRange) {
    let nsString = backingStore.string as NSString
    let startOfRange = nsString.lineRangeForRange(NSMakeRange(changedRange.location, 0))
    let endOfRange = nsString.lineRangeForRange(NSMakeRange(NSMaxRange(changedRange), 0))
    
    var extendedRange = NSUnionRange(changedRange, startOfRange)
    extendedRange = NSUnionRange(extendedRange, endOfRange)
    
    applyStylesToRange(extendedRange)
  }
  
  private func applyStylesToRange(searchRange: NSRange) {

    let normalAttrs = [NSFontAttributeName : UIFont.preferredFontForTextStyle(UIFontTextStyleBody)]
    
    // iterate over each replacement
    for (pattern, attributes) in replacements {
      let regex = NSRegularExpression(pattern: pattern, options: nil, error: nil)
      
      regex?.enumerateMatchesInString(backingStore.string, options: nil, range: searchRange) {
        (match, flags, stop) in
        
        // apply the style
        let matchRange = match.range
        self.addAttributes(attributes, range: matchRange)
        
        // reset the style to the original
        if NSMaxRange(matchRange) + 1 < self.length {
          self.addAttributes(normalAttrs, range: NSMakeRange(NSMaxRange(matchRange)+1, 1))
        }
      }
    }
  }

  private class func createHighlightPatterns() -> [String:[String:AnyObject]] {

    let scriptFontDescriptor = UIFontDescriptor(fontAttributes: [UIFontDescriptorFamilyAttribute: "Zapfino"])

    // 1. base our script font on the preferred body font size
    let bodyFontDescriptor = UIFontDescriptor.preferredFontDescriptorWithTextStyle(UIFontTextStyleBody)
    let bodyFontSize = bodyFontDescriptor.fontAttributes()[UIFontDescriptorSizeAttribute]! as CGFloat
    let scriptFont = UIFont(descriptor: scriptFontDescriptor, size: bodyFontSize)
    
    // 2. create the attributes
    let boldAttributes = createAttributesForFontStyle(UIFontTextStyleBody, trait: .TraitBold)
    let italicAttributes = createAttributesForFontStyle(UIFontTextStyleBody, trait: .TraitItalic)
    let scriptAttributes: [String:AnyObject] = [NSFontAttributeName : scriptFont]
    let strikeThroughAttributes: [String:AnyObject] = [NSStrikethroughStyleAttributeName : 1]
    let redTextAttributes: [String:AnyObject] = [NSForegroundColorAttributeName : UIColor.redColor()]
    
    // construct a dictionary of replacements based on regexes
    return [
      "(\\*\\w+(\\s\\w+)*\\*)\\s" : boldAttributes,
      "(_\\w+(\\s\\w+)*_)\\s" : italicAttributes,
      "([0-9]+\\.)\\s" : boldAttributes,
      "(-\\w+(\\s\\w+)*-)\\s" : strikeThroughAttributes,
      "(~\\w+(\\s\\w+)*~)\\s" : scriptAttributes,
      "\\s([A-Z]{2,})\\s" : redTextAttributes
    ]
  }
  
  private class func createAttributesForFontStyle(style: String, trait: UIFontDescriptorSymbolicTraits)
    -> [String:AnyObject] {
   let fontDescriptor = UIFontDescriptor.preferredFontDescriptorWithTextStyle(style)
    let descriptorWithTrait = fontDescriptor.fontDescriptorWithSymbolicTraits(trait)
    let font = UIFont(descriptor: descriptorWithTrait, size: 0.0)
    return [NSFontAttributeName : font]
  }
}