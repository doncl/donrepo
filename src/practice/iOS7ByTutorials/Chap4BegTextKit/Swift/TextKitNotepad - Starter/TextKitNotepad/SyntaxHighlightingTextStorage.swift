//
//  SyntaxHighlightingTextStorage.swift
//  TextKitNotepad
//
//  Created by Don Clore on 3/12/17.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

import UIKit

class SyntaxHighlightingTextStorage: NSTextStorage {
  private let backingStore = NSMutableAttributedString()
  
  override var string: String {
    get {
      return backingStore.string
    }
  }
  
  private let replacements = SyntaxHighlightingTextStorage.createHighlightPatterns()
  
  private class func createHighlightPatterns() -> [String: [String : AnyObject]] {
    let scriptFontDescriptor =
      UIFontDescriptor(fontAttributes: [UIFontDescriptorFamilyAttribute : "Zapfino"])
    
    //1. base our script font on the preferred body font size.
    let bodyfontDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: .body)
    let bodyFontSize = bodyfontDescriptor.fontAttributes[UIFontDescriptorSizeAttribute]!
      as! CGFloat
    let scriptFont = UIFont(descriptor: scriptFontDescriptor, size: bodyFontSize)
    
    //2. Create the attributes
    let boldAttributes = createAttributesForFontStyle(style: .body, trait: .traitBold)
    
    let italicAttributes = createAttributesForFontStyle(style: .body, trait: .traitItalic)
   
    let scriptAttributes : [String : AnyObject] = [NSFontAttributeName : scriptFont]
    
    let strikeThroughAttributes : [String : AnyObject] =
      [NSStrikethroughStyleAttributeName : 1 as AnyObject]
    
    let redTextAttributes: [String: AnyObject] = [NSForegroundColorAttributeName : UIColor.red]
    
    // construct a dictionary of replacements based on regexes
    return [
      "(\\*\\w+(\\s\\w+)*\\*)\\s" : boldAttributes,
      "(_\\w+(\\s\\w+)*_)\\s" : italicAttributes,
      "([0-9]+\\.)\\s" : boldAttributes,
      "(-\\w+(\\s\\w+)*-)\\s" : strikeThroughAttributes,
      "(~\\w+(\\s\\w+)*~)\\s" : scriptAttributes,
      "\\s([A-Z]{2,})\\s" : redTextAttributes
    ]
  }
  
  private class func createAttributesForFontStyle(style: UIFontTextStyle,
    trait: UIFontDescriptorSymbolicTraits) -> [String : AnyObject] {
  
    let fontDescriptor = UIFontDescriptor .preferredFontDescriptor(withTextStyle: style)
    let descriptorWithTrait = fontDescriptor.withSymbolicTraits(trait)
    let font = UIFont(descriptor: descriptorWithTrait!, size: 0.0)
    return [NSFontAttributeName : font]
  }
  
  override func attributes(at location: Int, effectiveRange range: NSRangePointer?)
    -> [String : Any] {
      
    return backingStore.attributes(at: location, effectiveRange: range)
  }
  
  override func replaceCharacters(in range: NSRange, with str: String) {
    print("replaceCharacters in range: \(NSStringFromRange(range)), " +
      "with str: \(str)")
    
    beginEditing()
    
    backingStore.replaceCharacters(in: range, with: str)
    
    edited([.editedAttributes, .editedCharacters], range: range,
      changeInLength: str.characters.count - range.length)
    
    endEditing()
  }
  
  override func setAttributes(_ attrs: [String : Any]?, range: NSRange) {
    print("setAttributes(\(attrs), range: \(NSStringFromRange(range)))")
    
    beginEditing()
    
    backingStore.setAttributes(attrs, range: range)
    
    edited([.editedAttributes], range: range, changeInLength: 0)
    
    endEditing()
  }
  
  override func processEditing() {
    performReplacementsForRange(changedRange: editedRange)
    super.processEditing()
  }
  
  private func performReplacementsForRange(changedRange: NSRange) {
    let nsString = backingStore.string as NSString
    let startOfRange = nsString.lineRange(for: NSMakeRange(changedRange.location, 0))
    let endOfRange = nsString.lineRange(for: NSMakeRange(NSMaxRange(changedRange), 0))
    
    var extendedRange = NSUnionRange(changedRange, startOfRange)
    extendedRange = NSUnionRange(extendedRange, endOfRange)
    
    applyStylesToRange(extendedRange)
  }
  
  private func applyStylesToRange(_ searchRange: NSRange) {
    
    let normalAttrs = [NSFontAttributeName : UIFont.preferredFont(forTextStyle: .body)]

    for (pattern, attributes) in replacements {
      let regex = try! NSRegularExpression(pattern: pattern, options: [])
      regex.enumerateMatches(in: backingStore.string, options: [], range: searchRange) {
        (match, flags, stop) in
        
        // apply the style
        if let matchRange = match?.range {
          self.addAttributes(attributes, range: matchRange)
          
          // reset the style to the original
          if NSMaxRange(matchRange) + 1 < self.length {
            self.addAttributes(normalAttrs, range: NSMakeRange(NSMaxRange(matchRange) + 1, 1))
          }
        }
      }
    }
  }
}
