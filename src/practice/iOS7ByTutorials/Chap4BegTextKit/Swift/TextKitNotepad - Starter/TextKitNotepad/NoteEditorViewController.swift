//
//  DetailViewController.swift
//  TextKitNotepad
//
//  Created by Colin Eberhardt on 24/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class NoteEditorViewController: UIViewController, UITextViewDelegate {

  private var timeView : TimeIndicatorView!
  
  var note: Note!
  private var textStorage : SyntaxHighlightingTextStorage!
  private var textView : UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    createTextView()
    timeView = TimeIndicatorView(date: note.timestamp)
    view.addSubview(timeView)
    
    NotificationCenter.default.addObserver(self,
      selector: #selector(NoteEditorViewController.prefferedContentSizeChanged(_:)),
      name: .UIContentSizeCategoryDidChange, object: nil)
    
    NotificationCenter.default.addObserver(self,
      selector: #selector(NoteEditorViewController.keyboardShown(_:)), name: .UIKeyboardDidShow,
      object: nil)
    
    NotificationCenter.default.addObserver(self,
      selector: #selector(NoteEditorViewController.keyboardHidden(_:)), name: .UIKeyboardDidHide,
      object: nil)
  }

  func textViewDidEndEditing(_ textView: UITextView) {
    note.contents = textView.text!
  }

  func prefferedContentSizeChanged(_ note : Notification) {
    textView.font = UIFont.preferredFont(forTextStyle: .body)
    updateTimeIndicatorFrame()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    updateTimeIndicatorFrame()
    textView.frame = view.bounds
  }
  
  private func updateTimeIndicatorFrame() {
    timeView.updateSize()
    timeView.frame = timeView.frame.offsetBy(dx: view.frame.width - timeView.frame.width,
      dy: textView.frame.origin.y)
    
    let exclusionPath = timeView.curvePathWithOrigin(timeView.center)
    textView.textContainer.exclusionPaths = [exclusionPath]
  }
  
  private func createTextView() {
    //1. Create the tex storage that backs the editor.
    let attrs = [NSFontAttributeName : UIFont.preferredFont(forTextStyle: . body)]
    let attrString = NSAttributedString(string: note.contents, attributes: attrs)
    
    textStorage = SyntaxHighlightingTextStorage()
    textStorage.append(attrString)
    
    let newTextViewRect = view.bounds
    
    //2. Create the layout manager
    let layoutManager = NSLayoutManager()
    
    //3. Create a text container
    let containerSize = CGSize(width: newTextViewRect.width,
      height: CGFloat.greatestFiniteMagnitude)
    
    let container = NSTextContainer(size: containerSize)
    container.widthTracksTextView = true
    layoutManager.addTextContainer(container)
    textStorage.addLayoutManager(layoutManager)
    
    //4. Create a UITextView
    textView = UITextView(frame: newTextViewRect, textContainer: container)
    textView.delegate = self
    view.addSubview(textView)
  }
  
  func keyboardShown(_ notification: Notification) {
    let info = notification.userInfo!
    let value: AnyObject = info[UIKeyboardFrameEndUserInfoKey]! as AnyObject
    let keyboardFrame = value.cgRectValue!
    var textViewFrame = view.bounds
    textViewFrame.size.height -= keyboardFrame.height
    textView.frame = textViewFrame
  }
  
  func keyboardHidden(_ notification : Notification) {
    textView.frame = view.bounds
  }
}

