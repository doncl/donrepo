//
//  Note.swift
//  TextKitNotepad
//
//  Created by Colin Eberhardt on 24/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import Foundation

class Note {
  var contents: String
  let timestamp: Date
  
  var title: String {
    return self.contents.components(separatedBy: CharacterSet.newlines)[0]
  }
  
  init(text: String) {
    self.contents = text
    self.timestamp = Date()
  }
}
