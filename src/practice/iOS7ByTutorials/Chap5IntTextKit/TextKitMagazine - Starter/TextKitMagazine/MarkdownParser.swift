//
//  MarkdownParser.swift
//  TextKitMagazine
//
//  Created by Don Clore on 3/13/17.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

import UIKit

class MarkdownParser {
  private let bodyTextAttributes : [String: UIFont]
  private let headingOneAttributes : [String : UIFont]
  private let headingTwoAttributes : [String : UIFont]
  private let headingThreeAttributes : [String : UIFont]
  
  init() {
    //1.  Create the font descriptors.
    let baskerville = UIFontDescriptor(
      fontAttributes: [UIFontDescriptorFamilyAttribute : "Baskerville"])
    
    let baskervilleBold = UIFontDescriptor(
      fontAttributes: [UIFontDescriptorFamilyAttribute : "Baskerville",
                       UIFontDescriptorFaceAttribute : "Bold"])
    
    //2. Determine the current size preference
    let bodyFont = UIFontDescriptor.preferredFontDescriptor(withTextStyle: .body)
    
    let bodyFontSize : AnyObject = bodyFont.fontAttributes[UIFontDescriptorSizeAttribute]!
      as AnyObject
    
    let fontSizeVal = CGFloat(bodyFontSize.floatValue)
    
    //3. Create the attributes for the various styles
    func attributesWith(descriptor: UIFontDescriptor, size: CGFloat)
      -> [String : UIFont] {
      
        let font = UIFont(descriptor: descriptor, size: size)
        return [NSFontAttributeName : font]
    }
    
    bodyTextAttributes = attributesWith(descriptor: baskerville, size: fontSizeVal)
    headingOneAttributes = attributesWith(descriptor: baskervilleBold, size: fontSizeVal * 2.0)
    headingTwoAttributes = attributesWith(descriptor: baskervilleBold, size: fontSizeVal * 1.8)
    headingThreeAttributes = attributesWith(descriptor: baskervilleBold, size: fontSizeVal * 1.4)
  }
  
  func parseMarkdownFile(_ path: String) -> NSAttributedString {
    //1. break the file into lines and iterate over each line
    let text = try! NSString(contentsOfFile: path, encoding: String.Encoding.utf8.rawValue)
    let lines = text.components(separatedBy: CharacterSet.newlines) as [String]
    
    //2. define a mapping for the markdown
    let mapping = [
      "###" : headingThreeAttributes,
      "##" : headingTwoAttributes,
      "#" : headingOneAttributes,
    ]
    
    //3. a function that formats a single line
    let formatLine = {
      (line : NSString) -> NSAttributedString in
      
      let lineStartExpression = try! NSRegularExpression(pattern: "^(#{0,3})(.*)", options: [])
      let matches = lineStartExpression.matches(in: line as String, options: [],
        range: NSMakeRange(0, line.length))
      
      if matches.count > 0 {
        let match = matches[0] as NSTextCheckingResult
        let prefix = line.substring(with: match.rangeAt(1))
        let remainder = line.substring(with: match.rangeAt(2))
        if let attributes = mapping[prefix] {
          return NSAttributedString(string: remainder + "\n\n", attributes: attributes)
        }
      }
      return NSAttributedString(string: (line as String) + "\n\n", attributes: self.bodyTextAttributes)
    }
    
    //4. transform the text
    let parsedOutput =  lines.filter {$0 != ""}.reduce(NSMutableAttributedString(),
      { (attributedString, nextLine) in
      
       attributedString.append(formatLine(nextLine as NSString))
       return attributedString
      
      })
    
    //1. locate images
    let regex = try! NSRegularExpression(pattern: "\\!\\[.*\\]\\((.*)\\)", options: [])
    
    let matches = regex.matches(in: parsedOutput.string, options: [],
      range: NSMakeRange(0, parsedOutput.length))
    
    //2. iterate over matches in reverse
    for result in matches.reversed() as [NSTextCheckingResult] {
      let matchRange = result.range
      let captureRange = result.rangeAt(1)
      
      //3. Create an attachment for each image
      let textAttachment = NSTextAttachment()
      textAttachment.image =
        UIImage(named: (parsedOutput.string as NSString).substring(with: captureRange))
      
      //4. Replace the image markup with the attachment
      let replacementString = NSAttributedString(attachment: textAttachment)
      
      parsedOutput.replaceCharacters(in: matchRange, with: replacementString)
    }
    return parsedOutput
  }
}

