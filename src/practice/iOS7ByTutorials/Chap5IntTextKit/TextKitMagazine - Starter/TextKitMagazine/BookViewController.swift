//
//  DetailViewController.swift
//  TextKitMagazine
//
//  Created by Colin Eberhardt on 01/12/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class BookViewController: UIViewController, UIBarPositioningDelegate {

  @IBOutlet weak var bookView: BookView!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = UIColor(white: 0.87, alpha: 1.0)
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    bookView.bookMarkup = appDelegate.bookMarkup
  }
  
  func position(for bar: UIBarPositioning) -> UIBarPosition {
    return .topAttached
  }

  func navigateToCharacterLocation(_ location : Int) {
    bookView.navigateToCharacterLocation(location)
  }  
}

