//
//  BookView.swift
//  TextKitMagazine
//
//  Created by Don Clore on 3/13/17.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

import UIKit

class BookView: UIScrollView, UIScrollViewDelegate {
  var bookMarkup : NSAttributedString!
  
  private let layoutManager = NSLayoutManager()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    delegate = self
  }

  func buildFrames() {
    // create the text storage
    let textStorage = NSTextStorage(attributedString: bookMarkup)
    
    // add the layout manager
    textStorage.addLayoutManager(layoutManager)
    
    
    // build the frames
    var range = NSMakeRange(0, 0)
    var containerIndex = 0
    
    while NSMaxRange(range) < layoutManager.numberOfGlyphs {
      
      //1
      let textViewRect = frameForViewAtIndex(containerIndex)
      
      //2
      let containerSize = CGSize(width: textViewRect.width, height: textViewRect.height - 16)
      let textContainer = NSTextContainer(size: containerSize)
      layoutManager.addTextContainer(textContainer)
      
//      //3 
//      let textView = UITextView(frame: textViewRect, textContainer: textContainer)
//      
//      addSubview(textView)
      
      containerIndex += 1
      
      //4
      range = layoutManager.glyphRange(for: textContainer)
    }

    // 5
    contentSize = CGSize(width: (bounds.width / 2.0) * CGFloat(containerIndex),
      height: bounds.height)
    
    isPagingEnabled = true
    
    buildViewsForCurrentOffset()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    if layoutManager.textContainers.count == 0 {
      buildFrames()
    }
  }
  
  private func frameForViewAtIndex(_ index: Int) -> CGRect {
    var textViewRect = CGRect(x: 0, y: 0, width: bounds.width / 2.0, height: bounds.height)
    textViewRect = textViewRect.insetBy(dx: 10, dy: 20)
    textViewRect = textViewRect.offsetBy(dx: (bounds.width / 2.0) * CGFloat(index), dy: 0)
    
    return textViewRect
  }
  
  private func textViewForContainer(textContainer: NSTextContainer) -> UITextView? {
    let textViews = subviews.filter{ ($0 as? UITextView) != nil } as! [UITextView]
    let matches = textViews.filter { $0.textContainer === textContainer }
    return matches.first
  }
  
  private func shouldRenderView(viewFrame: CGRect) -> Bool {
    if viewFrame.origin.x + viewFrame.width < (contentOffset.x - bounds.width) {
      return false
    }
    if viewFrame.origin.x >  (contentOffset.x + bounds.width * 2) {
      return false
    }
    return true
  }
  
  private func buildViewsForCurrentOffset() {
    //1
    for index in 0..<layoutManager.textContainers.count {
      //2 
      let textContainer = layoutManager.textContainers[index] as NSTextContainer
      let textView = textViewForContainer(textContainer: textContainer)
      
      //3
      let textViewRect = frameForViewAtIndex(index)
      
      if shouldRenderView(viewFrame: textViewRect) {
        // 4
        if textView == nil {
          print("Adding view at index \(index)")
          let newTextView = UITextView(frame: textViewRect, textContainer: textContainer)
          addSubview(newTextView)
        }
      } else {
        // 5
        if let textView = textView {
          print("Deleting the text view at index \(index)")
          textView.removeFromSuperview()
        }
      }
    }
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    buildViewsForCurrentOffset()
  }
  
  func navigateToCharacterLocation(_ location: Int) {
    var offset: CGFloat = 0.0
    
    let containers = layoutManager.textContainers as [NSTextContainer]
    
    for container in containers {
      let glyphRange = layoutManager.glyphRange(for: container)
      let charRange = layoutManager.characterRange(forGlyphRange: glyphRange,
        actualGlyphRange: nil)
      
      if location >= charRange.location && location < NSMaxRange(charRange) {
        contentOffset = CGPoint(x: offset, y: 0)
        buildViewsForCurrentOffset()
        return
      }
      offset += bounds.width / 2.0
    }
  }
}
