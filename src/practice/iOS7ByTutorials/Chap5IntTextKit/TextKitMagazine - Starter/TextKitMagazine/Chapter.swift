//
//  Chapter.swift
//  TextKitMagazine
//
//  Created by Don Clore on 3/13/17.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

import UIKit

struct Chapter {
  let title : String
  let location : Int
}
