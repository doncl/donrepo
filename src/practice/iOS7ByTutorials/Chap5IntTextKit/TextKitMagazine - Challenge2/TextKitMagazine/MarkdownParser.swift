//
//  MarkdownParser.swift
//  TextKitMagazine
//
//  Created by Colin Eberhardt on 01/12/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class MarkdownParser {
  
  private let bodyTextAttributes: [String:UIFont]
  private let headingOneAttributes: [String:UIFont]
  private let headingTwoAttributes: [String:UIFont]
  private let headingThreeAttributes: [String:UIFont]
  
  init() {
    // 1. Create the font descriptors
    let baskerville = UIFontDescriptor(fontAttributes:
      [UIFontDescriptorFamilyAttribute : "Baskerville"])
    let baskervilleBold = UIFontDescriptor(fontAttributes:
      [UIFontDescriptorFamilyAttribute : "Baskerville",
        UIFontDescriptorFaceAttribute : "Bold"])
    
    // 2. Determine the current text size preference
    let bodyFont = UIFontDescriptor.preferredFontDescriptorWithTextStyle(UIFontTextStyleBody)
    let bodyFontSize: AnyObject = bodyFont.fontAttributes()[UIFontDescriptorSizeAttribute]!
    let bodyFontSizeValue = CGFloat(bodyFontSize.floatValue)

    // 3. create the attributes for the various styles
    func attributesWithDescriptor(descriptor: UIFontDescriptor, size: CGFloat) -> [String:UIFont] {
      let font = UIFont(descriptor: descriptor, size: size)
      return [NSFontAttributeName : font]
    }
    
    bodyTextAttributes = attributesWithDescriptor(baskerville, bodyFontSizeValue)
    headingOneAttributes = attributesWithDescriptor(baskervilleBold, bodyFontSizeValue * 2.0)
    headingTwoAttributes = attributesWithDescriptor(baskervilleBold, bodyFontSizeValue * 1.8)
    headingThreeAttributes = attributesWithDescriptor(baskervilleBold, bodyFontSizeValue * 1.4)
  }
  
  func parseMarkdownFile(path: String) -> NSAttributedString {
    // 1. break the file into lines and iterate over each line
    let text = NSString(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)
    let lines = text?.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet()) as [String]
    
    // 2. define a mapping for teh markdown
    let mapping = [
      "###" : headingThreeAttributes,
      "##" : headingTwoAttributes,
      "#" : headingOneAttributes
    ]
    
    // 3. a function that formats a single line
    let formatLine = {
      (line: NSString) -> NSAttributedString in
      let lineStartExpression = NSRegularExpression(pattern: "^(#{0,3})(.*)", options: nil, error: nil)!
      let matches = lineStartExpression.matchesInString(line, options: nil, range: NSMakeRange(0, line.length))
      
      if matches.count > 0 {
        let match = matches[0] as NSTextCheckingResult
        let prefix = line.substringWithRange(match.rangeAtIndex(1))
        let remainder = line.substringWithRange(match.rangeAtIndex(2))
        if let attributes = mapping[prefix] {
          return NSAttributedString(string: remainder + "\n\n", attributes: attributes)
        }
      }
      return NSAttributedString(string: line + "\n\n", attributes: self.bodyTextAttributes)
    }
    
    // 4. transform the text
    let parsedOutput = lines
      .filter { $0 != ""}
      .reduce(NSMutableAttributedString(), combine: { (attributedString, nextLine)  in
        attributedString.appendAttributedString(formatLine(nextLine))
        return attributedString
      })
    
    // 1. Locate images
    let regex = NSRegularExpression(pattern: "\\!\\[.*\\]\\((.*)\\)", options: nil, error: nil)!
    
    let matches = regex.matchesInString(parsedOutput.string, options: nil, range: NSMakeRange(0, parsedOutput.length))
    
    // 2. Iterate over matches in reverse
    for result in matches.reverse() as [NSTextCheckingResult] {
      let matchRange = result.range
      let captureRange = result.rangeAtIndex(1)
      
      // 3. Create an attachment for each image
      let textAttachment = NSTextAttachment()
      textAttachment.image = UIImage(named: (parsedOutput.string as NSString).substringWithRange(captureRange))
      
      // 4. Replace the image markup with the attachment
      let replacementString = NSAttributedString(attachment: textAttachment)
      parsedOutput.replaceCharactersInRange(matchRange, withAttributedString: replacementString)
    }
    
    return parsedOutput
  }

}