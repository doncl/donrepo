//
//  DetailViewController.swift
//  TextKitMagazine
//
//  Created by Colin Eberhardt on 01/12/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class BookViewController: UIViewController, UIBarPositioningDelegate, BookViewDelegate, UIPopoverControllerDelegate {
  

  private var popover: UIPopoverController!
  
  @IBOutlet weak var bookView: BookView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    println("\(NSDate().timeIntervalSinceReferenceDate) viewDidLoad")

    view.backgroundColor = UIColor(white: 0.87, alpha: 1.0)
    
    edgesForExtendedLayout = .None
    
    let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    bookView.bookMarkup = appDelegate.bookMarkup
    
    bookView.bookViewDelegate = self
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "preferredContentSizeChanged:", name: UIContentSizeCategoryDidChangeNotification, object: nil)
  }
  
  func preferredContentSizeChanged(notification: NSNotification) {
    // tell the app delegate to re-load the markup. This will cause
    // it to create new text attributes based on the current font preferences
    let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    appDelegate.parseMarkdown()
    
    // provide the updated markup to the book view and re-draw
    bookView.bookMarkup = appDelegate.bookMarkup
    bookView.buildFrames()
  }
  
  func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
    return .TopAttached
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    println("\(NSDate().timeIntervalSinceReferenceDate) viewDidAppear")
  }
  
  func navigateToCharacterLocation(location: Int) {
    bookView.navigateToCharacterLocation(location)
  }
  
  func bookView(bookView: BookView, didHighlightWord word: String, inRect rect: CGRect) {
    let dictionaryVC = UIReferenceLibraryViewController(term: word)
    popover = UIPopoverController(contentViewController: dictionaryVC)
    popover.delegate = self
    popover.presentPopoverFromRect(rect, inView: bookView, permittedArrowDirections: .Any, animated: true)
  }
  
  func popoverControllerDidDismissPopover(popoverController: UIPopoverController) {
    bookView.removeHighlightedWord()
  }
}

