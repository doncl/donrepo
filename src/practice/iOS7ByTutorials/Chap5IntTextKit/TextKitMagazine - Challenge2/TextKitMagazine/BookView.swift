//
//  BookView.swift
//  TextKitMagazine
//
//  Created by Colin Eberhardt on 01/12/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class BookView: UIScrollView, UIScrollViewDelegate {
  
  var bookMarkup: NSAttributedString!
  
  var bookViewDelegate: BookViewDelegate?
  
  private var layoutManager: NSLayoutManager!
  
  private var currentCharacterLocation: Int = 0
  
  private var wordCharacterRange: NSRange!
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    if layoutManager == nil {
      buildFrames()
    }
  }
  

  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    delegate = self
    
    let recognizer = UITapGestureRecognizer(target: self, action: "handleTap:")
    addGestureRecognizer(recognizer)
  }
  
  func handleTap(recognizer: UITapGestureRecognizer) {
    let textStorage = layoutManager.textStorage!
    
    // 1
    let tappedLocation = recognizer.locationInView(self)
    let tappedViews =
      (subviews as [UIView])
        .filter { CGRectContainsPoint($0.frame, tappedLocation) }
    
    if tappedViews.count < 0 {
      return
    }
    let tappedTextView = tappedViews[0] as UITextView
    
    // 2
    var subViewLocation = recognizer.locationInView(tappedTextView)
    subViewLocation.y -= 8
    
    // 3
    let glyphIndex = layoutManager.glyphIndexForPoint(subViewLocation, inTextContainer: tappedTextView.textContainer)
    let charIndex = layoutManager.characterIndexForGlyphAtIndex(glyphIndex)

    // 4
    if !NSCharacterSet.letterCharacterSet()
      .characterIsMember((textStorage.string as NSString).characterAtIndex(charIndex)) {
      return
    }
    
    // 5
    wordCharacterRange = wordThatContainsCharacter(charIndex, string: textStorage.string)
    
    // 6
    textStorage.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: wordCharacterRange)
    
    // 1
    let rect = layoutManager.lineFragmentRectForGlyphAtIndex(glyphIndex, effectiveRange: nil)

    // 2
    let wordGlyphRange = layoutManager.glyphRangeForCharacterRange(wordCharacterRange, actualCharacterRange: nil)
    let startLocation = layoutManager.locationForGlyphAtIndex(wordGlyphRange.location)
    let endLocation = layoutManager.locationForGlyphAtIndex(NSMaxRange(wordGlyphRange))

    // 3
    var wordRect = CGRectMake(startLocation.x, rect.origin.y,
      endLocation.x - startLocation.x, rect.size.height)

    // 4
    wordRect = CGRectOffset(wordRect, tappedTextView.frame.origin.x,
      tappedTextView.frame.origin.y)

    // 5
    wordRect = CGRectOffset(wordRect, 0.0, 8.0)

    let word = (textStorage.string as NSString).substringWithRange(wordCharacterRange)
    bookViewDelegate?.bookView(self, didHighlightWord: word, inRect: wordRect)
  }
  
  func removeHighlightedWord() {
    layoutManager.textStorage?.removeAttribute(NSForegroundColorAttributeName, range: wordCharacterRange)
  }
  
  func wordThatContainsCharacter(charIndex: Int, string: NSString) -> NSRange {
    
    let isLetter = {
      (location: Int) -> Bool in
      return NSCharacterSet.letterCharacterSet().characterIsMember(string.characterAtIndex(location))
    }
    
    var startLocation = charIndex
    while startLocation > 0 && isLetter(startLocation - 1) {
      startLocation--
    }
    
    var endLocation = charIndex
    while endLocation < string.length && isLetter(endLocation + 1) {
      endLocation++
    }
    
    return NSMakeRange(startLocation, endLocation - startLocation + 1)
  }

  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    buildViewsForCurrentOffset()
  }
  
  func buildFrames() {
    
    // create the text storage
    let textStorage = NSTextStorage(attributedString: bookMarkup)
    
    // add to the layout manager
    layoutManager = NSLayoutManager()
    textStorage.addLayoutManager(layoutManager)
    
    // build the frames
    var range = NSMakeRange(0, 0)
    var containerIndex = 0
    
    while(NSMaxRange(range) < layoutManager.numberOfGlyphs) {
      // 1
      let textViewRect = frameForViewAtIndex(containerIndex)
      
      //2
      let containerSize = CGSizeMake(textViewRect.size.width,
        textViewRect.size.height - 16)
      let textContainer = NSTextContainer(size: containerSize)
      layoutManager.addTextContainer(textContainer)
      
      // 3
      containerIndex++
      
      // 4
      range = layoutManager.glyphRangeForTextContainer(textContainer)
    }
    
    navigateToCharacterLocation(currentCharacterLocation)
    
    // 5
    contentSize = CGSizeMake(
      (bounds.size.width / 2) * CGFloat(containerIndex),
      bounds.size.height)
    pagingEnabled = true
    
    buildViewsForCurrentOffset()
  }
  
  func navigateToCharacterLocation(location: Int) {
    var offset: CGFloat = 0.0
    let containers = layoutManager.textContainers as [NSTextContainer]
    for container in containers {
      let glyphRange = layoutManager.glyphRangeForTextContainer(container)
      let charRange = layoutManager.characterRangeForGlyphRange(glyphRange, actualGlyphRange: nil)
      if location >= charRange.location && location < NSMaxRange(charRange) {
        self.contentOffset = CGPointMake(offset, 0)
        buildViewsForCurrentOffset()
        return
      }
      offset += bounds.size.width / 2.0;
    }
  }
  
  private func shouldRenderView(viewFrame: CGRect) -> Bool {
    if (viewFrame.origin.x + viewFrame.size.width < (contentOffset.x - bounds.size.width)) {
      return false
    }
    if (viewFrame.origin.x > (contentOffset.x + bounds.size.width * 2)) {
      return false
    }
    return true
  }
  
  private func textViewForContainer(textContainer: NSTextContainer) -> UITextView? {
    let textViews = subviews.filter { ($0 as? UITextView) != nil } as [UITextView]
    let matches = textViews.filter { $0.textContainer === textContainer }
    return matches.first
  }
  
  private func buildViewsForCurrentOffset() {
    // 1
    for index in 0..<layoutManager.textContainers.count {
      // 2
      let textContainer = layoutManager.textContainers[index] as NSTextContainer
      let textView = textViewForContainer(textContainer)
      
      // 3
      let textViewRect = frameForViewAtIndex(index)
      
      if shouldRenderView(textViewRect) {
        // 4
        if textView == nil {
          println("Adding view at index \(index)")
          let newTextView = UITextView(frame: textViewRect, textContainer: textContainer)
          addSubview(newTextView)
        }
      } else {
        // 5
        if let textView = textView {
          println("Deleting the view at index \(index)")
          textView.removeFromSuperview()
        }
      }
    }
    
    // record the character index for the first container
    
    // 1. iterate over the containers
    for (index, textContainer) in enumerate(layoutManager.textContainers as [NSTextContainer]) {
      // 2. determine the required frame
      let textViewRect = frameForViewAtIndex(index)
      // for the first container that is rendered on screen ...
      if textViewRect.origin.x > contentOffset.x {
        // determine the index of the first character
        let glyphRange = layoutManager.glyphRangeForTextContainer(textContainer)
        let charRange = layoutManager.characterRangeForGlyphRange(glyphRange, actualGlyphRange: nil)
        currentCharacterLocation = charRange.location
        break
      }
    }
  }
  
  private func frameForViewAtIndex(index: Int) -> CGRect {
    var textViewRect = CGRectMake(0, 0, bounds.size.width / 2, bounds.size.height)
    textViewRect = CGRectInset(textViewRect, 10, 20)
    textViewRect = CGRectOffset(textViewRect, (bounds.size.width / 2) * CGFloat(index), 0)
    return textViewRect
  }
  
}