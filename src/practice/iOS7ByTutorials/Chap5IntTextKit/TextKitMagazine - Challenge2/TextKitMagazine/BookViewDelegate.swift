//
//  BookViewDelegate.swift
//  TextKitMagazine
//
//  Created by Colin Eberhardt on 02/12/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

protocol BookViewDelegate {
  func bookView(bookView: BookView, didHighlightWord: String, inRect: CGRect)
}