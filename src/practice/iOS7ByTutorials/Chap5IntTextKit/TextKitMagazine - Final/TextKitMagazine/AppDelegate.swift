//
//  AppDelegate.swift
//  TextKitMagazine
//
//  Created by Colin Eberhardt on 01/12/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  
  var chapters: [Chapter]!
  
  lazy var bookMarkup: NSAttributedString = {
    let parser = MarkdownParser()
    let path = Bundle.main.path(forResource: "alices_adventures", ofType: "md")!
    return parser.parseMarkdownFile(path)
  }()

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
    // style the navigation bar
    let navColor = UIColor(white: 0.2, alpha: 1.0)
    UINavigationBar.appearance().barTintColor = navColor
    UINavigationBar.appearance().tintColor = UIColor.white
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
    
    chapters = locateChapters(bookMarkup.string as NSString)
    
    return true
  }
  
  fileprivate func locateChapters(_ markdown: NSString) -> [Chapter] {
    var chapters = [Chapter]()
    markdown.enumerateSubstrings(in: NSMakeRange(0, markdown.length), options: NSString.EnumerationOptions()) {
      (substring, substringRange, enclosingRange, stop) -> () in
      if (substring?.hasPrefix("CHAPTER"))! {
        let chapter = Chapter(title: substring!, location: substringRange.location)
        chapters.append(chapter)
      }
    }
    return chapters
  }
  
}

