//
//  BookView.swift
//  TextKitMagazine
//
//  Created by Colin Eberhardt on 01/12/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class BookView: UIScrollView, UIScrollViewDelegate {
  
  var bookMarkup: NSAttributedString!
  
  var bookViewDelegate: BookViewDelegate?
  
  fileprivate let layoutManager = NSLayoutManager()
  
  fileprivate var wordCharacterRange: NSRange!
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    if layoutManager.textContainers.count == 0 {
      buildFrames()
    }
  }
  

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    delegate = self
    
    let recognizer = UITapGestureRecognizer(target: self, action: #selector(BookView.handleTap(_:)))
    addGestureRecognizer(recognizer)
  }
  
  func handleTap(_ recognizer: UITapGestureRecognizer) {
    let textStorage = layoutManager.textStorage!
    
//    // 1
//    let tappedLocation = recognizer.location(in: self)
//    let tappedViews =
//      (subviews as [UIView])
//        .filter { $0.frame.contains(tappedLocation) }
//    
//    if tappedViews.count < 0 {
//      return
//    }
//    let tappedTextView = tappedViews[0] as! UITextView
//    
//    // 2
//    var subViewLocation = recognizer.location(in: tappedTextView)
//    subViewLocation.y -= 8
//    
//    // 3
//    let glyphIndex = layoutManager.glyphIndex(for: subViewLocation, in: tappedTextView.textContainer)
//    let charIndex = layoutManager.characterIndexForGlyph(at: glyphIndex)
//
//    // 4
//    if !CharacterSet.letters
//      .characterIsMember((textStorage.string as NSString).characterAtIndex(charIndex)) {
//      return
//    }
//    
//    // 5
//    wordCharacterRange = wordThatContainsCharacter(charIndex, string: textStorage.string as NSString)
//    
//    // 6
//    textStorage.addAttribute(NSForegroundColorAttributeName, value: UIColor.red, range: wordCharacterRange)
//    
//    // 1
//    let rect = layoutManager.lineFragmentRect(forGlyphAt: glyphIndex, effectiveRange: nil)
//
//    // 2
//    let wordGlyphRange = layoutManager.glyphRange(forCharacterRange: wordCharacterRange, actualCharacterRange: nil)
//    let startLocation = layoutManager.location(forGlyphAt: wordGlyphRange.location)
//    let endLocation = layoutManager.location(forGlyphAt: NSMaxRange(wordGlyphRange))
//
//    // 3
//    var wordRect = CGRect(x: startLocation.x, y: rect.origin.y,
//      width: endLocation.x - startLocation.x, height: rect.size.height)
//
//    // 4
//    wordRect = wordRect.offsetBy(dx: tappedTextView.frame.origin.x,
//      dy: tappedTextView.frame.origin.y)
//
//    // 5
//    wordRect = wordRect.offsetBy(dx: 0.0, dy: 8.0)
//
//    let word = (textStorage.string as NSString).substring(with: wordCharacterRange)
//    bookViewDelegate?.bookView(self, didHighlightWord: word, inRect: wordRect)
  }
  
  func removeHighlightedWord() {
    layoutManager.textStorage?.removeAttribute(NSForegroundColorAttributeName, range: wordCharacterRange)
  }
  
  func wordThatContainsCharacter(_ charIndex: Int, string: NSString) -> NSRange {
    
    let isLetter = {
      (location: Int) -> Bool in
      return CharacterSet.letters.contains(UnicodeScalar(string.character(at: location))!)
    }
    
    var startLocation = charIndex
    while startLocation > 0 && isLetter(startLocation - 1) {
      startLocation -= 1
    }
    
    var endLocation = charIndex
    while endLocation < string.length && isLetter(endLocation + 1) {
      endLocation += 1
    }
    
    return NSMakeRange(startLocation, endLocation - startLocation + 1)
  }

  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    buildViewsForCurrentOffset()
  }
  
  func buildFrames() {
    
    // create the text storage
    let textStorage = NSTextStorage(attributedString: bookMarkup)
    
    // add to the layout manager
    textStorage.addLayoutManager(layoutManager)
    
    // build the frames
    var range = NSMakeRange(0, 0)
    var containerIndex = 0
    
    while(NSMaxRange(range) < layoutManager.numberOfGlyphs) {
      // 1
      let textViewRect = frameForViewAtIndex(containerIndex)
      
      //2
      let containerSize = CGSize(width: textViewRect.size.width,
        height: textViewRect.size.height - 16)
      let textContainer = NSTextContainer(size: containerSize)
      layoutManager.addTextContainer(textContainer)
      
      // 3
      containerIndex += 1
      
      // 4
      range = layoutManager.glyphRange(for: textContainer)
    }
    
    // 5
    contentSize = CGSize(
      width: (bounds.size.width / 2) * CGFloat(containerIndex),
      height: bounds.size.height)
    isPagingEnabled = true
    
    buildViewsForCurrentOffset()
  }
  
  func navigateToCharacterLocation(_ location: Int) {
    var offset: CGFloat = 0.0
    let containers = layoutManager.textContainers as [NSTextContainer]
    for container in containers {
      let glyphRange = layoutManager.glyphRange(for: container)
      let charRange = layoutManager.characterRange(forGlyphRange: glyphRange, actualGlyphRange: nil)
      if location >= charRange.location && location < NSMaxRange(charRange) {
        self.contentOffset = CGPoint(x: offset, y: 0)
        buildViewsForCurrentOffset()
        return
      }
      offset += bounds.size.width / 2.0;
    }
  }
  
  fileprivate func shouldRenderView(_ viewFrame: CGRect) -> Bool {
    if (viewFrame.origin.x + viewFrame.size.width < (contentOffset.x - bounds.size.width)) {
      return false
    }
    if (viewFrame.origin.x > (contentOffset.x + bounds.size.width * 2)) {
      return false
    }
    return true
  }
  
  fileprivate func textViewForContainer(_ textContainer: NSTextContainer) -> UITextView? {
    let textViews = subviews.filter { ($0 as? UITextView) != nil } as! [UITextView]
    let matches = textViews.filter { $0.textContainer === textContainer }
    return matches.first
  }
  
  fileprivate func buildViewsForCurrentOffset() {
    // 1
    for index in 0..<layoutManager.textContainers.count {
      // 2
      let textContainer = layoutManager.textContainers[index] as NSTextContainer
      let textView = textViewForContainer(textContainer)
      
      // 3
      let textViewRect = frameForViewAtIndex(index)
      
      if shouldRenderView(textViewRect) {
        // 4
        if textView == nil {
          print("Adding view at index \(index)")
          let newTextView = UITextView(frame: textViewRect, textContainer: textContainer)
          addSubview(newTextView)
        }
      } else {
        // 5
        if let textView = textView {
          print("Deleting the view at index \(index)")
          textView.removeFromSuperview()
        }
      }
    }
  }
  
  fileprivate func frameForViewAtIndex(_ index: Int) -> CGRect {
    var textViewRect = CGRect(x: 0, y: 0, width: bounds.size.width / 2, height: bounds.size.height)
    textViewRect = textViewRect.insetBy(dx: 10, dy: 20)
    textViewRect = textViewRect.offsetBy(dx: (bounds.size.width / 2) * CGFloat(index), dy: 0)
    return textViewRect
  }
  
}
