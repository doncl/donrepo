//
//  DetailViewController.swift
//  TextKitMagazine
//
//  Created by Colin Eberhardt on 01/12/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class BookViewController: UIViewController, UIBarPositioningDelegate, BookViewDelegate, UIPopoverControllerDelegate {
  

  fileprivate var popover: UIPopoverController!
  
  @IBOutlet weak var bookView: BookView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    print("\(Date().timeIntervalSinceReferenceDate) viewDidLoad")

    view.backgroundColor = UIColor(white: 0.87, alpha: 1.0)
    
    edgesForExtendedLayout = UIRectEdge()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    bookView.bookMarkup = appDelegate.bookMarkup
    
    bookView.bookViewDelegate = self
  }
  
  func position(for bar: UIBarPositioning) -> UIBarPosition {
    return .topAttached
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    print("\(Date().timeIntervalSinceReferenceDate) viewDidAppear")
  }
  
  func navigateToCharacterLocation(_ location: Int) {
    bookView.navigateToCharacterLocation(location)
  }
  
  func bookView(_ bookView: BookView, didHighlightWord word: String, inRect rect: CGRect) {
    let dictionaryVC = UIReferenceLibraryViewController(term: word)
    popover = UIPopoverController(contentViewController: dictionaryVC)
    popover.delegate = self
    popover.present(from: rect, in: bookView, permittedArrowDirections: .any, animated: true)
  }
  
  func popoverControllerDidDismissPopover(_ popoverController: UIPopoverController) {
    bookView.removeHighlightedWord()
  }
}

