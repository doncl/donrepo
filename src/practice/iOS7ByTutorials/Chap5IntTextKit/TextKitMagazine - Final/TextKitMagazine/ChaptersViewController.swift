//
//  MasterViewController.swift
//  TextKitMagazine
//
//  Created by Colin Eberhardt on 01/12/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class ChaptersViewController: UITableViewController {

  var bookViewController: BookViewController! = nil
  
  var chapters: [Chapter] {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    return appDelegate.chapters
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    self.preferredContentSize = CGSize(width: 320.0, height: 600.0)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
  }

  // MARK: - Table View

  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return chapters.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
    let chapter = chapters[indexPath.row]
    cell.textLabel?.text = chapter.title
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let chapter = chapters[indexPath.row]
    let appDelegate  = UIApplication.shared.delegate as! AppDelegate
    let bookViewController = appDelegate.window!.rootViewController as! BookViewController
    bookViewController.navigateToCharacterLocation(chapter.location)
    
    dismiss(animated: true, completion: nil)
  }

}

