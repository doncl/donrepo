//
//  MarkdownParser.swift
//  TextKitMagazine
//
//  Created by Colin Eberhardt on 01/12/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class MarkdownParser {
  
  fileprivate let bodyTextAttributes: [String:UIFont]
  fileprivate let headingOneAttributes: [String:UIFont]
  fileprivate let headingTwoAttributes: [String:UIFont]
  fileprivate let headingThreeAttributes: [String:UIFont]
  
  init() {
    // 1. Create the font descriptors
    let baskerville = UIFontDescriptor(fontAttributes:
      [UIFontDescriptorFamilyAttribute : "Baskerville"])
    let baskervilleBold = UIFontDescriptor(fontAttributes:
      [UIFontDescriptorFamilyAttribute : "Baskerville",
        UIFontDescriptorFaceAttribute : "Bold"])
    
    // 2. Determine the current text size preference
    let bodyFont = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.body)
    let bodyFontSize: AnyObject = bodyFont.fontAttributes[UIFontDescriptorSizeAttribute]! as AnyObject
    let bodyFontSizeValue = CGFloat(bodyFontSize.floatValue)

    // 3. create the attributes for the various styles
    func attributesWithDescriptor(_ descriptor: UIFontDescriptor, size: CGFloat) -> [String:UIFont] {
      let font = UIFont(descriptor: descriptor, size: size)
      return [NSFontAttributeName : font]
    }
    
    bodyTextAttributes = attributesWithDescriptor(baskerville, size: bodyFontSizeValue)
    headingOneAttributes = attributesWithDescriptor(baskervilleBold, size: bodyFontSizeValue * 2.0)
    headingTwoAttributes = attributesWithDescriptor(baskervilleBold, size: bodyFontSizeValue * 1.8)
    headingThreeAttributes = attributesWithDescriptor(baskervilleBold, size: bodyFontSizeValue * 1.4)
  }
  
  func parseMarkdownFile(_ path: String) -> NSAttributedString {
    // 1. break the file into lines and iterate over each line
    let text = try! NSString(contentsOfFile: path, encoding: String.Encoding.utf8.rawValue)
    let lines = text.components(separatedBy: CharacterSet.newlines) as [String]
    
    // 2. define a mapping for teh markdown
    let mapping = [
      "###" : headingThreeAttributes,
      "##" : headingTwoAttributes,
      "#" : headingOneAttributes
    ]
    
    // 3. a function that formats a single line
    let formatLine = {
      (line: NSString) -> NSAttributedString in
      let lineStartExpression = try! NSRegularExpression(pattern: "^(#{0,3})(.*)", options: [])
      let matches = lineStartExpression.matches(in: line as String, options: [], range: NSMakeRange(0, line.length))
      
      if matches.count > 0 {
        let match = matches[0] as NSTextCheckingResult
        let prefix = line.substring(with: match.rangeAt(1))
        let remainder = line.substring(with: match.rangeAt(2))
        if let attributes = mapping[prefix] {
          return NSAttributedString(string: remainder + "\n\n", attributes: attributes)
        }
      }
      return NSAttributedString(string: (line as String) + "\n\n", attributes: self.bodyTextAttributes)
    }
    
    // 4. transform the text
    let parsedOutput = lines
      .filter { $0 != ""}
      .reduce(NSMutableAttributedString(), { (attributedString, nextLine)  in
        attributedString.append(formatLine(nextLine as NSString))
        return attributedString
      })
    
    // 1. Locate images
    let regex = try! NSRegularExpression(pattern: "\\!\\[.*\\]\\((.*)\\)", options: [])
    
    let matches = regex.matches(in: parsedOutput.string, options: [], range: NSMakeRange(0, parsedOutput.length))
    
    // 2. Iterate over matches in reverse
    for result in matches.reversed() as [NSTextCheckingResult] {
      let matchRange = result.range
      let captureRange = result.rangeAt(1)
      
      // 3. Create an attachment for each image
      let textAttachment = NSTextAttachment()
      textAttachment.image = UIImage(named: (parsedOutput.string as NSString).substring(with: captureRange))
      
      // 4. Replace the image markup with the attachment
      let replacementString = NSAttributedString(attachment: textAttachment)
      parsedOutput.replaceCharacters(in: matchRange, with: replacementString)
    }
    
    return parsedOutput
  }

}
