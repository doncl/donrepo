//
//  AppDelegate.swift
//  TextKitMagazine
//
//  Created by Colin Eberhardt on 01/12/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  
  var chapters: [Chapter]!
  
  var bookMarkup: NSAttributedString!
  
  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    
    // style the navigation bar
    let navColor = UIColor(white: 0.2, alpha: 1.0)
    UINavigationBar.appearance().barTintColor = navColor
    UINavigationBar.appearance().tintColor = UIColor.whiteColor()
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]

    parseMarkdown()
    
    chapters = locateChapters(bookMarkup.string)
    
    return true
  }
  
  func parseMarkdown() {
    let parser = MarkdownParser()
    let path = NSBundle.mainBundle().pathForResource("alices_adventures", ofType: "md")!
    bookMarkup = parser.parseMarkdownFile(path)
  }
  
  private func locateChapters(markdown: NSString) -> [Chapter] {
    var chapters = [Chapter]()
    markdown.enumerateSubstringsInRange(NSMakeRange(0, markdown.length), options: .ByLines) {
      (substring, substringRange, enclosingRange, stop) -> () in
      if substring.hasPrefix("CHAPTER") {
        let chapter = Chapter(title: substring, location: substringRange.location)
        chapters.append(chapter)
      }
    }
    return chapters
  }
  
}

