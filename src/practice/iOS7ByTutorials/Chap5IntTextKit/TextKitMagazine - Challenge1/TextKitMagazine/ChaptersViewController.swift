//
//  MasterViewController.swift
//  TextKitMagazine
//
//  Created by Colin Eberhardt on 01/12/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class ChaptersViewController: UITableViewController {

  var bookViewController: BookViewController! = nil
  
  var chapters: [Chapter] {
    let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    return appDelegate.chapters
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    self.preferredContentSize = CGSize(width: 320.0, height: 600.0)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
  }

  // MARK: - Table View

  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }

  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return chapters.count
  }

  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
    let chapter = chapters[indexPath.row]
    cell.textLabel.text = chapter.title
    return cell
  }
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let chapter = chapters[indexPath.row]
    let appDelegate  = UIApplication.sharedApplication().delegate as AppDelegate
    let bookViewController = appDelegate.window!.rootViewController as BookViewController
    bookViewController.navigateToCharacterLocation(chapter.location)
    
    dismissViewControllerAnimated(true, completion: nil)
  }

}

