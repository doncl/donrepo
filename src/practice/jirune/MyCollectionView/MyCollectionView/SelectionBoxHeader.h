//
//  SelectionBoxHeader.h
//  MyCollectionView
//
//  Created by Nehruji V on 21/11/12.
//  Copyright (c) 2012 Nehruji V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectionBoxHeader : UICollectionReusableView

@property (nonatomic, strong) UIButton *checkBox;

+ (NSString *)kind;

@end
