//
//  CustomItem.h
//  MyCollectionView
//
//  Created by Nehruji V on 21/11/12.
//  Copyright (c) 2012 Nehruji V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomItem : UICollectionViewCell

@property (nonatomic, strong) UILabel *dataLabel;

@end
