//
//  MyCollectionViewController.m
//  MyCollectionView
//
//  Created by Nehruji V on 21/11/12.
//  Copyright (c) 2012 Nehruji V. All rights reserved.
//

#import "MyCollectionViewController.h"
#import "CustomItem.h"
#import "SelectionBoxHeader.h"

@interface MyCollectionViewController ()

@property (nonatomic, strong) NSMutableArray *dataModel;
@property (nonatomic, strong) UIView *topShadowView;

@end

@implementation MyCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _dataModel = [[NSMutableArray alloc] initWithCapacity:100];
    _topShadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 500)];
    _topShadowView.backgroundColor = [UIColor blueColor];

    
    for (int i=0; i<100; i++) {
        
        NSMutableArray *tempArray = [[NSMutableArray alloc] initWithCapacity:50];
        for (int j=0; j<50; j++) {
            [tempArray addObject:[NSNumber numberWithInt:j]];
        }
        [_dataModel addObject:tempArray];
    }
    
    [self.collectionView registerClass:[CustomItem class] forCellWithReuseIdentifier:@"CustomCell"];
    [self.collectionView registerClass:[SelectionBoxHeader class] forSupplementaryViewOfKind:[SelectionBoxHeader kind] withReuseIdentifier:@"CustomHeaderForSelectionBox"];

    self.collectionView.backgroundColor = [UIColor whiteColor];
 
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.dataModel count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 50;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CustomItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CustomCell" forIndexPath:indexPath];
    cell.dataLabel.text = [NSString stringWithFormat:@"%@",[[self.dataModel objectAtIndex:indexPath.section] objectAtIndex:indexPath.item]];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{    
    SelectionBoxHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"CustomHeaderForSelectionBox" forIndexPath:indexPath];
    return header;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
