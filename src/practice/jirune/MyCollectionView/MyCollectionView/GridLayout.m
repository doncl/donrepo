//
//  GridLayout.m
//  MyCollectionView
//
//  Created by Nehruji V on 21/11/12.
//  Copyright (c) 2012 Nehruji V. All rights reserved.
//

#import "GridLayout.h"
#import "SelectionBoxHeader.h"

#define ITEM_WIDTH 100
#define ITEM_HEIGHT 50

@interface GridLayout()

@property (nonatomic, assign) CGSize contentSize;
@property (nonatomic, strong) NSArray *cellCounts;
@property (nonatomic, strong) NSArray *sectionRects;
@property (nonatomic, assign) NSInteger sectionCount;

@end

@implementation GridLayout

-(void)prepareLayout
{
    [super prepareLayout];
    
    self.sectionCount = [self.collectionView numberOfSections];
    
    NSMutableArray *counts = [NSMutableArray arrayWithCapacity:self.sectionCount];
    NSMutableArray *rects = [NSMutableArray arrayWithCapacity:self.sectionCount];
    
    for (int section = 0; section < self.sectionCount; section++)
    {
        [counts addObject:@([self.collectionView numberOfItemsInSection:section])];
        [rects addObject:[NSValue valueWithCGRect:CGRectMake(0, section*50, 50*100, 50)]];
    }
    self.cellCounts = [NSArray arrayWithArray:counts];
    self.sectionRects = [NSArray arrayWithArray:rects];
    
    self.contentSize = CGSizeMake(50+[self cellCountForSection:0]*100, self.sectionCount*50);
}

-(CGSize)collectionViewContentSize
{
    return self.contentSize;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return NO;
}

- (int)cellCountForSection:(NSInteger)section
{
    NSNumber *count = self.cellCounts[section];
    return [count intValue];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)path
{
    UICollectionViewLayoutAttributes* attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:path];
    attributes.size = CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT);
    CGRect sectionRect = [self.sectionRects[path.section] CGRectValue];
    attributes.center = CGPointMake(50+50+path.item*100, CGRectGetMidY(sectionRect));
    
    return attributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"added supplementary view now");
    if (![kind isEqualToString:[SelectionBoxHeader kind]])
        return nil;
    
    UICollectionViewLayoutAttributes* attributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:kind withIndexPath:indexPath];
    
    attributes.size = CGSizeMake(50, 50);
    attributes.center = CGPointMake(25, 25+50*indexPath.section);
    
    return attributes;
}

-(NSArray*)layoutAttributesForElementsInRect:(CGRect)rect
{
    int page = 0;
    NSMutableArray* attributes = [NSMutableArray array];
    for (NSValue *sectionRect in self.sectionRects)
    {
        if (CGRectIntersectsRect(rect, sectionRect.CGRectValue))
        {
            int cellCount = [self cellCountForSection:page];
            for (int i = 0; i < cellCount; i++) {
                NSIndexPath* indexPath = [NSIndexPath indexPathForItem:i inSection:page];
                [attributes addObject:[self layoutAttributesForItemAtIndexPath:indexPath]];
            }
            
            [attributes addObject:[self layoutAttributesForSupplementaryViewOfKind:[SelectionBoxHeader kind] atIndexPath:[NSIndexPath indexPathForItem:0 inSection:page]]];

        }
        page++;
    }

    return attributes;
}
@end
