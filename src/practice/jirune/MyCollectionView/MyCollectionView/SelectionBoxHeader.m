//
//  SelectionBoxHeader.m
//  MyCollectionView
//
//  Created by Nehruji V on 21/11/12.
//  Copyright (c) 2012 Nehruji V. All rights reserved.
//

#import "SelectionBoxHeader.h"

NSString *kSelectionBoxHeaderKind = @"HeaderWithSelectionBox";

@implementation SelectionBoxHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}

+ (NSString *)kind
{
    return (NSString *)kSelectionBoxHeaderKind;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
