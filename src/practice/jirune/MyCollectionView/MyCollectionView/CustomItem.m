//
//  CustomItem.m
//  MyCollectionView
//
//  Created by Nehruji V on 21/11/12.
//  Copyright (c) 2012 Nehruji V. All rights reserved.
//

#import "CustomItem.h"
#import <QuartzCore/QuartzCore.h>

@implementation CustomItem

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.dataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        self.dataLabel.backgroundColor = [UIColor lightGrayColor];
        self.dataLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.dataLabel];
        
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0f, 49.5f, self.dataLabel.frame.size.width, 0.5f);
        bottomBorder.backgroundColor = [UIColor grayColor].CGColor;
        [self.dataLabel.layer addSublayer:bottomBorder];
    }
    return self;
}

//- (id)initWithCoder:(NSCoder *)aDecoder
//{
//    self = [super initWithCoder:aDecoder];
//    if (self) {
//        // Initialization code
//        NSLog(@"custome cell init");
//        self.dataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//        self.dataLabel.backgroundColor = [UIColor yellowColor];
//        NSLog(@"date Label : %@",self.dataLabel);
//        [self.contentView addSubview:self.dataLabel];
//    }
//    return self;
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
