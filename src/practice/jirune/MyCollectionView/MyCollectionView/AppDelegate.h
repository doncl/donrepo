//
//  AppDelegate.h
//  MyCollectionView
//
//  Created by Nehruji V on 21/11/12.
//  Copyright (c) 2012 Nehruji V. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MyCollectionViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MyCollectionViewController *viewController;

@end
