/*
* Copyright (c) 2014 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import Foundation
import UIKit

open class SavedImageService {
  
  public init() {
    
  }
  
  open func savedImageFileNames() -> [String] {
    var error: NSError?
    var imageNames = [String]()
    if let path = imagesDirectoryURL().path {
        imageNames = FileManager.default.contentsOfDirectoryAtPath(path, error: &error) as! [String]
    }
    
    if error != nil {
      NSLog("Error loading images: %@", error!)
    }
    
    return imageNames
  }
  
  open func imageNamed(_ imageName: String) -> UIImage? {
    var imageURL = imagesDirectoryURL()
    imageURL = imageURL.appendingPathComponent(imageName)
    
    var image: UIImage?
    if let path = imageURL.path {
        image = UIImage(contentsOfFile: path)
    }
    
    return image
  }
  
  open func saveImage(_ image: UIImage, name: String) -> URL? {
    return saveImage(image, name: name, imageUrl: imagesDirectoryURL())
  }
  
  open func saveImageToUpload(_ image: UIImage, name: String) -> URL? {
    return saveImage(image, name: name, imageUrl: imagesToUploadDirectoryURL())
  }
  
  open func saveImage(_ image: UIImage, name: String, imageUrl: URL) -> URL? {
    var imageDirectoryURL = imageUrl
    imageDirectoryURL = imageDirectoryURL.appendingPathComponent(name)
    imageDirectoryURL = imageDirectoryURL.appendingPathExtension("jpg")
    let imageData = UIImageJPEGRepresentation(image, 1.0)
    let saved = imageData.writeToFile(imageDirectoryURL.path!, atomically: true)
    return imageDirectoryURL
  }
  
  fileprivate func imagesDirectoryURL() -> URL {
    return urlForDirectoryWithName("Images")
  }
  
  fileprivate func imagesToUploadDirectoryURL() -> URL {
    return urlForDirectoryWithName("Uploads")
  }
  
  fileprivate func urlForDirectoryWithName(_ name: String) -> URL! {
    if let containerURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.com.raywenderlich.swift.imgvue1") {
      var contairURLWithName = containerURL.appendingPathComponent(name)
      if !FileManager.default.fileExists(atPath: contairURLWithName.path) {
        FileManager.default.createDirectoryAtPath(containerURL.path, withIntermediateDirectories: false, attributes: nil, error: nil)
      }
      
      return containerURL
    } else {
      fatalError("Unable to obtain container URL for app group, verify your app group settings.")
    }
  }
}
