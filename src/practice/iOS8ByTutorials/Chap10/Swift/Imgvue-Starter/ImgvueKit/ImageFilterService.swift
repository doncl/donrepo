/*
* Copyright (c) 2014 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import Foundation
import UIKit

open class ImageFilterService {
  
  public init() {
    
  }
  
  open func availableFilters() -> Dictionary<String, String> {
    return ["Sepia": "CISepiaTone",
      "Invert": "CIColorInvert",
      "Vintage": "CIPhotoEffectTransfer",
      "Black & White": "CIPhotoEffectMono"]
  }
  
  open func applyFilter(_ filterName: String, toImage image: UIImage) -> UIImage {
    let filter = CIFilter(name: filterName)
    filter?.setDefaults()
    
    let orientation = orientationFromImageOrientation(image.imageOrientation)
    
    let ciImage = CIImage(cgImage: image.cgImage!)
    let inputImage = ciImage.applyingOrientation(orientation)
    
    filter?.setValue(inputImage, forKey: kCIInputImageKey)
    
    let outputImage = filter?.outputImage
    
    let context = CIContext(options: nil)
    
    let cgImageRef = context.createCGImage(outputImage!, from: outputImage!.extent)
    
    if let filteredImage = UIImage(CGImage: cgImageRef!) {
      return filteredImage
    } else {
      return image
    }
  }
  
  fileprivate func orientationFromImageOrientation(_ imageOrientation: UIImageOrientation) -> CInt {
    var orientation: CInt = 0
    
    switch (imageOrientation) {
    case .up:               orientation = 1
    case .down:             orientation = 3
    case .left:             orientation = 8
    case .right:            orientation = 6
    case .upMirrored:   	orientation = 2
    case .downMirrored:     orientation = 4
    case .leftMirrored:     orientation = 5
    case .rightMirrored:    orientation = 7
    }
    
    return orientation
  }
}
