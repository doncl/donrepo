/*
* Copyright (c) 2014 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import Foundation
import UIKit

open class ImgurService: NSObject, URLSessionDataDelegate, URLSessionTaskDelegate {
  
  fileprivate let imgurClientId = "YOUR_CLIENT_ID"
  fileprivate let imgurAPIBaseUrlString = "https://api.imgur.com/3/"
  
  open var session: Foundation.URLSession!
  fileprivate var progressCallbacks: Dictionary<URLSessionTask, (Float) -> ()> = Dictionary()
  fileprivate var completionCallbacks: Dictionary<URLSessionTask, (ImgurImage?, NSError?) -> ()> = Dictionary()
  
  open class var sharedService: ImgurService {
    struct Singleton {
      static let instance = ImgurService()
    }
    return Singleton.instance
  }
  
  fileprivate override init() {
    let sessionConfig = URLSessionConfiguration.default
    sessionConfig.httpAdditionalHeaders = ["Authorization": "Client-ID \(imgurClientId)",
      "Content-Type": "application/json"]

    super.init()
    
    session = Foundation.URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
  }
  
  open func hotViralGalleryThumbnailImagesPage(_ page: Int, completion: @escaping ([ImgurImage]?, NSError?) -> ()) {
    let urlString = "gallery/hot/viral/\(page)"
    if let url = imgurAPIUrlWithPathComponents(urlString) {
      let request = URLRequest(url: url)
      let task = session.dataTask(with: request, completionHandler: { data, response, error in
        if error == nil {
          var images: [ImgurImage] = []
          let jsonResponse = JSON(data: data, options: .AllowFragments, error: nil)
          let imagesArray = jsonResponse["data"].array ?? []
          for imageJSON in imagesArray {
            if !imageJSON["is_album"].bool! { // no album support
              let image = ImgurImage(fromJson: imageJSON)
              images.append(image)
            }
          }
          
          completion(images, nil)
        } else {
          completion(nil, error as NSError?)
        }
      }) 
      
      task.resume()
    }
  }
  
  open func uploadImage(_ image: UIImage, title: NSString, completion: (ImgurImage?, NSError?) -> (), progressCallback: (Float) -> ()) {
    uploadImage(image, title: title, session: session, completion: completion, progressCallback: progressCallback)
  }
  
  open func uploadImage(_ image: UIImage, title: NSString, session: Foundation.URLSession?, completion: (ImgurImage?, NSError?) -> (), progressCallback: (Float) -> ()) {
    if let url = imgurAPIUrlWithPathComponents("image") {
      let request = NSMutableURLRequest(url: url)
      request.httpMethod = "POST"
      let postBodyDict = ["title": title]
      
      let postBodyData = JSONSerialization.dataWithJSONObject(postBodyDict, options: nil, error: nil)
      request.HTTPBody = postBodyData
      
      let savedImageService = SavedImageService()
      let uuidString = UUID().uuidString
      
      let imageToUploadUrl = savedImageService.saveImageToUpload(image, name: uuidString)
      
      var sessionToUse: Foundation.URLSession
      
      if let session = session {
        sessionToUse = session
      } else {
        sessionToUse = self.session
      }
      if let uploadUrl = imageToUploadUrl {
        let task = sessionToUse.uploadTask(with: request, fromFile: uploadUrl)
        completionCallbacks[task] = completion
        progressCallbacks[task] = progressCallback
        
        task.resume()
      } else {
        let error = NSError(domain: "com.raywenderlich.imgvue.imgurservice", code: 1, userInfo: nil)
        completion(nil, error)
      }
    }
  }
  
  fileprivate func imgurAPIUrlWithPathComponents(_ components: String) -> URL? {
    let baseUrl = URL(string: imgurAPIBaseUrlString)
    let APIUrl = URL(string: components, relativeTo: baseUrl)
    
    return APIUrl
  }
  
  // MARK - NSURLSessionTaskDelegate
  
  open func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
    if let progressCallback = progressCallbacks[task] {
      let progress = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
      DispatchQueue.main.async {
        progressCallback(progress)
      }
      if totalBytesSent == totalBytesExpectedToSend {
        progressCallbacks.removeValue(forKey: task)
      }
    }
  }
  
  open func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
    if let progressCallback = progressCallbacks[task] {
      progressCallbacks.removeValue(forKey: task)
    }
    
    if let completionCallback = completionCallbacks[task] {
      completionCallbacks.removeValue(forKey: task)
      
      
      if error != nil {
        completionCallback(nil, error as NSError?)
      }
    }
  }
  
  open func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
    if let completionBlock = completionCallbacks[dataTask] {
      var jsonError: NSError?
      
      if jsonError == nil {
        let jsonResponse = JSON(data: data, options: .allowFragments, error: nil)
        let imgurImage = ImgurImage(fromJson: jsonResponse["data"])
        DispatchQueue.main.async {
          completionBlock(imgurImage, nil)
        }
      } else {
        completionBlock(nil, jsonError)
      }
    }
  }
}
