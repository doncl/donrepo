//
//  PhotoGridTableViewCell.h
//  LayoutPhotoGrid
//
//  Created by Don Clore on 11/11/15.
//  Copyright © 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoGridTableViewCell : UITableViewCell<UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *posterName;
@property (weak, nonatomic) IBOutlet UIButton *userProfileButton;
@property (weak, nonatomic) IBOutlet UILabel *posterTitle;
@property (weak, nonatomic) IBOutlet UILabel *postedDate;
@property (weak, nonatomic) IBOutlet UILabel *postsValue;
@property (weak, nonatomic) IBOutlet UILabel *posts;
@property (weak, nonatomic) IBOutlet UIView *authorAvatar;
@property (weak, nonatomic) IBOutlet UIImageView *authorIcon;
@property (weak, nonatomic) IBOutlet UIView *textContainer;

@property (weak, nonatomic) IBOutlet UITextView *postText;
@property (strong, nonatomic) NSArray<UIImage *> *images;
@property (strong, nonatomic) UICollectionView *photoGrid;

- (void)doPhotoGridConstraints;
@end
