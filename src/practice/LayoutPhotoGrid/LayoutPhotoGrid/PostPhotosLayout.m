//
// Created by Don Clore on 11/11/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "PostPhotosLayout.h"

#define CELL_SIDE_PADDING 18
#define CELL_INTER_PADDING 9
#define CELL_VERT_PADDING 18

@implementation PostPhotosLayout

+ (CGFloat)heightForImageCount:(NSUInteger)imageCount viewWidth:(CGFloat)viewWidth
{
    // This is all just voodoo; trial-and-error until the results look good, and look like the
    // comps.  
    CGFloat baseLine = (viewWidth - (CELL_SIDE_PADDING * 2) - CELL_INTER_PADDING);
    switch (imageCount) {
        case 1:
            return baseLine * 0.7;
            break;

        case 2:
            return baseLine / 2;
            break;

        case 3:
            return baseLine * 1.3;
            break;

        case 4:
            return baseLine;
            break;

        case 5:
            return baseLine;
            break;

        default:
            return 0.0;
            break;
    }
}


- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray<UICollectionViewLayoutAttributes *> *attrsArray =
        [NSMutableArray<UICollectionViewLayoutAttributes *> new];

    for (NSUInteger i = 0; i < self.numPhotos; i++) {
        NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:0];
        UICollectionViewLayoutAttributes *attrs = [self getAttrsForPath:path];
        //if (CGRectIntersectsRect(attrs.frame, rect)) {
            [attrsArray addObject:attrs];
        //} else {
          //  NSLog(@"foo");
        //}
    }
    return attrsArray;
}


- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self getAttrsForPath:indexPath];
}

- (UICollectionViewLayoutAttributes *)getAttrsForPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attrs =
        [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];

    attrs.frame = [self frameForItemAtIndexPath:indexPath];

    return attrs;
}

- (CGRect)frameForItemAtIndexPath:(NSIndexPath *)path
{
    if (!self.numPhotos) {
        return CGRectZero;
    }
    if (self.numPhotos == 1) {
        return [self frameForSinglePhoto];
    }
    if (self.numPhotos == 2) {
        return [self frameForTwoPhotos:path];
    }
    if (self.numPhotos == 3) {
        return [self frameForThreePhotos:path];
    }
    if (self.numPhotos == 4) {
        return [self frameForFourPhotos:path];
    }
    if (self.numPhotos == 5) {
        return [self frameForFivePhotos:path];
    }

    return CGRectZero;
}

- (CGRect)frameForFivePhotos:(NSIndexPath *)path
{
    CGFloat x = CELL_SIDE_PADDING;
    CGFloat xBorders = CELL_SIDE_PADDING * 2;
    CGFloat yBorders = CELL_VERT_PADDING * 2;
    CGSize size = [self getSize];
    CGFloat y = CELL_VERT_PADDING;
    CGFloat width = (size.width - (xBorders + CELL_INTER_PADDING)) / 2;
    CGFloat height;

    // Just going to go around clockwise; it's easier.
    if (!path.row || path.row == 4) {
        height = (size.height - yBorders - CELL_INTER_PADDING) / 2;
        if (path.row == 4) {
            y += (height + CELL_INTER_PADDING);
        }

        return CGRectMake(x, y, width, height);
    }
    // 2nd Column, rows, 1, 2, 3
    x += (width + CELL_INTER_PADDING);

    height = (size.height - yBorders - (CELL_INTER_PADDING * 2)) / 3;
    CGFloat yIncrement = height + CELL_INTER_PADDING;
    if (path.row == 2) {
        y += yIncrement;
    } else if (path.row == 3) {
        y += (yIncrement * 2);
    }

    // If it's row 1 (the second row), we're already in the right place.

    return CGRectMake(x, y, width, height);
}

- (CGRect)frameForFourPhotos:(NSIndexPath *)path {
    CGFloat x = CELL_SIDE_PADDING;
    CGFloat xBorders = (CELL_SIDE_PADDING * 2);
    CGSize size = [self getSize];
    CGFloat y = CELL_VERT_PADDING;
    CGFloat width = (size.width - (xBorders + CELL_INTER_PADDING)) / 2;
    CGFloat height;

    CGFloat heightPercent;
    if (!path.row || path.row== 3) {
        heightPercent = 0.6;
    } else {
        heightPercent = 0.4;
    }

    if (path.row == 1 || path.row == 3) {
        x += (width + CELL_INTER_PADDING);
    }
    height = ((size.height - CELL_INTER_PADDING) * heightPercent) - CELL_VERT_PADDING;

    if (path.row == 3) {
        y = ((size.height - CELL_INTER_PADDING) * 0.4) + CELL_INTER_PADDING;
    }
    if (path.row  == 2) {
        y = ((size.height - CELL_INTER_PADDING) * 0.6) + CELL_INTER_PADDING;
    }
    return CGRectMake(x, y, width, height);
}

- (CGRect)frameForThreePhotos:(NSIndexPath *)path
{
    CGFloat x = CELL_SIDE_PADDING;
    CGFloat xBorders = (CELL_SIDE_PADDING * 2);  // xBorders for lone top image.
    CGSize size = [self getSize];
    CGFloat y = CELL_VERT_PADDING;

    // The top photo takes up 60% the vertical space, but only after subtracting the
    // border between it and the two bottom ones.

    CGFloat topImageHeight = ((size.height - CELL_INTER_PADDING) * 0.6) - CELL_VERT_PADDING;

    if (!path.row) {  // lone top image.
        CGFloat width = size.width - xBorders;
        return CGRectMake(x, y, width, topImageHeight);
    }

    CGFloat bottomPhotosHeight = ((size.height - CELL_INTER_PADDING) * 0.4) - CELL_VERT_PADDING;

    // The bottom photos have a pad on each side, and one in between.
    CGFloat width = (size.width - (xBorders + CELL_INTER_PADDING)) / 2;
    y = topImageHeight + CELL_INTER_PADDING + CELL_VERT_PADDING;
    if (path.row == 2) {
        x += (width + CELL_INTER_PADDING);
    }
    return CGRectMake(x, y, width, bottomPhotosHeight);
}

- (CGRect)frameForTwoPhotos:(NSIndexPath *)path
{
    CGFloat horzPadding = (CELL_SIDE_PADDING * 2) + CELL_INTER_PADDING;
    CGFloat x = CELL_SIDE_PADDING;
    CGSize size = [self getSize];
    CGFloat cxImage = (size.width - horzPadding) / 2;
    if (path.row) {
        x += (cxImage + CELL_INTER_PADDING);
    }
    return CGRectMake(x, CELL_VERT_PADDING, cxImage, size.height - (CELL_VERT_PADDING * 2));
}

- (CGRect)frameForSinglePhoto
{
    CGSize size = [self getSize];
    return CGRectMake(CELL_SIDE_PADDING, CELL_VERT_PADDING,
        size.width - (CELL_SIDE_PADDING * 2), size.height - (CELL_VERT_PADDING * 2) );
}

- (CGSize)getSize
{
    return self.collectionView.frame.size;
}

@end