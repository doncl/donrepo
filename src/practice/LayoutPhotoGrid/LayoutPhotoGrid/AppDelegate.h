//
//  AppDelegate.h
//  LayoutPhotoGrid
//
//  Created by Don Clore on 11/11/15.
//  Copyright © 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end

