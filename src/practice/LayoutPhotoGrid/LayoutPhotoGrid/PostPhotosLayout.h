//
// Created by Don Clore on 11/11/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PostPhotosLayout : UICollectionViewLayout
@property (nonatomic, assign) NSUInteger numPhotos;
+ (CGFloat)heightForImageCount:(NSUInteger)imageCount viewWidth:(CGFloat)viewWidth;
@end