//
//  ViewController.m
//  LayoutPhotoGrid
//
//  Created by Don Clore on 11/11/15.
//  Copyright © 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "ViewController.h"
#import "PhotoGridTableViewCell.h"
#import "PostPhotosLayout.h"

const int cellHeight = 350;
#define CELL_SIDE_PADDING 18


@interface ViewController ()
@property (nonatomic, strong) NSMutableArray<UIImage *> *images;
@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.images = [NSMutableArray<UIImage *> new];
    [self.images addObject:[UIImage imageNamed:@"countryRoad"]];
    [self.images addObject:[UIImage imageNamed:@"greenGrass"]];
    [self.images addObject:[UIImage imageNamed:@"islandTown"]];
    [self.images addObject:[UIImage imageNamed:@"tardis"]];
    [self.images addObject:[UIImage imageNamed:@"yellowSunShine"]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"frame is %@ points wide, %@ points high", @(self.view.frame.size.width),
          @(self.view.frame.size.height));
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PhotoGridTableViewCell *cell =

    [[PhotoGridTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                  reuseIdentifier:@"photoGridCell"];

    cell.photoGrid.hidden = NO;
    cell.photoGrid.delegate = cell;
    cell.photoGrid.dataSource = cell;
    cell.images = [self.images subarrayWithRange:NSMakeRange(0, indexPath.row + 1)];
    cell.hidden = NO;

    PostPhotosLayout *layout;
    if (!cell.photoGrid) {
        layout = [PostPhotosLayout new];
        layout.numPhotos = cell.images.count;

        CGFloat y = cell.textContainer.frame.origin.y + cell.textContainer.frame.size.height;
        CGFloat height = [self tableView:tableView heightForRowAtIndexPath:indexPath];
        CGRect r = CGRectMake(0, y, cell.frame.size.width,
            MAX(height, 0));

        cell.photoGrid = [[UICollectionView alloc] initWithFrame:r
                                            collectionViewLayout:layout];
        cell.photoGrid.backgroundColor = [UIColor whiteColor];
        cell.photoGrid.delegate = cell;
        cell.photoGrid.dataSource = cell;
        [cell.photoGrid registerClass:[UICollectionViewCell class]
           forCellWithReuseIdentifier:@"masonryCell"];

        // for debugging.
        //cell.photoGrid.backgroundColor = [UIColor blueColor];

        [cell addSubview:cell.photoGrid];
        [cell doPhotoGridConstraints];

    } else {
        layout = (PostPhotosLayout *)cell.photoGrid.collectionViewLayout;
        layout.numPhotos = cell.images.count;
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 184 + [PostPhotosLayout heightForImageCount:indexPath.row + 1
                                             viewWidth:self.view.frame.size.width];
}

@end
