//
//  PhotoGridTableViewCell.m
//  LayoutPhotoGrid
//
//  Created by Don Clore on 11/11/15.
//  Copyright © 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "PhotoGridTableViewCell.h"

@interface PhotoGridTableViewCell()
@property (strong, nonatomic) NSLayoutConstraint *photoGridTop;
@property (strong, nonatomic) NSLayoutConstraint *photoGridBottom;
@property (strong, nonatomic) NSLayoutConstraint *photoGridLeft;
@property (strong, nonatomic) NSLayoutConstraint *photoGridRight;
@end

@implementation PhotoGridTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *xibArray = [[NSBundle mainBundle] loadNibNamed:@"PhotoGridTableCell" owner:self options:nil];
        self = [xibArray objectAtIndex:0];
    }

    return self;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (!self.images) {
        return 0;
    }
    return self.images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [self.photoGrid dequeueReusableCellWithReuseIdentifier:@"masonryCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor yellowColor];
    cell.hidden = NO;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:self.images[indexPath.row]];
    imageView.contentMode = UIViewContentModeScaleToFill;
    imageView.frame = CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
    [cell addSubview:imageView];
    [self doCellConstraints:cell imageView:imageView];

    return cell;
}

-(void)doCellConstraints:(UICollectionViewCell *)cell imageView:(UIImageView *)imageView
{
    NSLayoutConstraint *top, *left, *right, *bottom;
    top = [NSLayoutConstraint constraintWithItem:imageView
                                       attribute:NSLayoutAttributeTop
                                       relatedBy:NSLayoutRelationEqual
                                          toItem:cell
                                       attribute:NSLayoutAttributeTop
                                      multiplier:1
                                        constant:0];
    left = [NSLayoutConstraint constraintWithItem:imageView
                                        attribute:NSLayoutAttributeLeft
                                        relatedBy:NSLayoutRelationEqual
                                           toItem:cell
                                        attribute:NSLayoutAttributeLeft
                                       multiplier:1
                                         constant:0];
    right = [NSLayoutConstraint constraintWithItem:imageView
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:cell
                                         attribute:NSLayoutAttributeRight
                                        multiplier:1
                                          constant:0];
    bottom = [NSLayoutConstraint constraintWithItem:imageView
                                          attribute:NSLayoutAttributeBottom
                                          relatedBy:NSLayoutRelationEqual
                                             toItem:cell
                                          attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                           constant:0];
    NSArray<NSLayoutConstraint *> *constraints = @[top, left, right, bottom];
    imageView.translatesAutoresizingMaskIntoConstraints  = NO;

    [cell addConstraints:constraints];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)doPhotoGridConstraints
{
    if (!self.photoGrid) {
        NSLog(@"WARNING - trying to set constraints on non-existent collectionview");
    }

    NSMutableArray<NSLayoutConstraint *> *constraints =
        [NSMutableArray<NSLayoutConstraint *> new];

    if (!self.photoGridTop) {
        self.photoGridTop = [NSLayoutConstraint constraintWithItem:self.photoGrid
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.textContainer
                                                      attribute:NSLayoutAttributeBottom
                                                     multiplier:1
                                                       constant:0];
        [constraints addObject:self.photoGridTop];
    }
    if (!self.photoGridBottom) {
        self.photoGridBottom = [NSLayoutConstraint constraintWithItem:self.photoGrid
                                                            attribute:NSLayoutAttributeBottom
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:self
                                                            attribute:NSLayoutAttributeBottom
                                                           multiplier:1
                                                             constant:0];
        [constraints addObject:self.photoGridBottom];
    }
    if (!self.photoGridLeft) {
        self.photoGridLeft = [NSLayoutConstraint constraintWithItem:self.photoGrid
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1
                                                           constant:0];
        [constraints addObject:self.photoGridLeft];
    }
    if (!self.photoGridRight) {
        self.photoGridRight = [NSLayoutConstraint constraintWithItem:self.photoGrid
                                                           attribute:NSLayoutAttributeRight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:self
                                                           attribute:NSLayoutAttributeRight
                                                          multiplier:1
                                                            constant:0];
        [constraints addObject:self.photoGridRight];
    }
    if (constraints.count) {
        [self addConstraints:constraints];
    }
    self.photoGrid.translatesAutoresizingMaskIntoConstraints = NO;
}

@end
