//
//  FlickrPhotoViewController.h
//  FlickrSearch
//
//  Created by Don Clore on 7/28/15.
//  Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FlickrPhoto;

@interface FlickrPhotoViewController : UIViewController
@property (nonatomic, strong) FlickrPhoto *flickrPhoto;
@end
