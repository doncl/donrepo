//
//  FlickrPhotoHeaderView.m
//  FlickrSearch
//
//  Created by Don Clore on 7/28/15.
//  Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "FlickrPhotoHeaderView.h"

@interface FlickrPhotoHeaderView ()
@property (weak) IBOutlet UIImageView *backgroundImageView;
@property (weak) IBOutlet UILabel *searchLabel;
@end

@implementation FlickrPhotoHeaderView
- (void)setSearchText:(NSString *)text
{
    self.searchLabel.text = text;
    UIImage *shareButtonImage = [[UIImage imageNamed:@"header_bg.png"]
            resizableImageWithCapInsets:UIEdgeInsetsMake(68, 68, 68, 68)];

    self.backgroundImageView.image = shareButtonImage;
    self.backgroundImageView.center = self.center;
}
@end
