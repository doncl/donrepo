//
//  FlickrPhotoHeaderView.h
//  FlickrSearch
//
//  Created by Don Clore on 7/28/15.
//  Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrPhotoHeaderView : UICollectionReusableView
- (void)setSearchText:(NSString *)text;
@end
