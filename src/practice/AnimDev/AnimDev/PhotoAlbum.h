//
// Created by Don Clore on 10/6/15.
// Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoLibraryManager.h"


@class Photo;
@class PhotoAlbumInfo;
@class ALAssetsGroup;
@class PHAssetCollection;
@class PHCachingImageManager;
@class PHAsset;

@interface PhotoAlbum : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSMutableArray<Photo *> *photos;

// Caching
@property (nonatomic, strong) NSMutableArray<NSIndexPath *> *cachedIndices;
@property (assign, nonatomic) CGFloat lastCacheFrameCenter;

- (instancetype)initForPhotoLibrary:(PHAssetCollection *)assetCollection
                               name:(NSString *)albumName;
- (void)loadPhotos:(void (^)(PhotoAlbum *))completion;

// Caching
- (void)resetCache;
- (void) updateCache:(CGRect)bounds
sortedVisibleIndices:(NSArray<NSIndexPath *> *)visibleIndices
         cacheingMgr:(PHCachingImageManager *)cachingMgr
           imageSize:(CGSize)imageSize;

- (void)cachedPhotoAtIndex:(NSIndexPath *)indexPath
                cachingMgr:(PHCachingImageManager *)cachingMgr
                 imageSize:(CGSize)imageSize
                completion:(void (^)(UIImage * result, NSDictionary * info))completion;

- (NSArray<PHAsset *> *)assetsAtIndexPaths:(NSArray *)indexPaths;
@end