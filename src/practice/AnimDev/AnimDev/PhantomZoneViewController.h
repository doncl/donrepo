//
//  PhantomZoneViewController.h
//  AnimDev
//
//  Created by Don Clore on 11/1/15.
//  Copyright © 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

// OK, the animation was modelled after the 1980 movie, when Jorel sentences the criminals to
// the phantom zone.7
@interface PhantomZoneViewController : UIViewController


@end

