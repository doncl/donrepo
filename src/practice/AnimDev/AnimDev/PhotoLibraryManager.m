//
// Created by Don Clore on 10/6/15.
// Copyright (c) 2015 Scout. All rights reserved.
//
#import <AssetsLibrary/AssetsLibrary.h>
#import "PhotoLibraryManager.h"
#import "PhotoAlbum.h"
#import "Photo.h"
#import "PhotoAlbumInfo.h"
@import Photos;

@interface PhotoLibraryManager()
@property (nonatomic, strong) NSMutableDictionary<NSString *, PHAssetCollection *> *collDict;

// Only used in ScoutPhotoLibraryUsesPhotoLibrary case - i.e., on iOS 8 and later.
@property (nonatomic, strong) PHCachingImageManager *cachingMgr;
@property (nonatomic, strong) dispatch_queue_t cacheQueue;
@end

@implementation PhotoLibraryManager
@synthesize photoAlbumInfos;

- (instancetype)init {
    self = [super init];
    if (self) {
        self.cachingMgr = [[PHCachingImageManager alloc] init];
        self.cacheQueue = dispatch_queue_create("cache_queue", DISPATCH_QUEUE_SERIAL);
    }
    return self;
}

- (void)loadAlbumInfos:(CGFloat)cellEdge
{
    self.photoAlbumInfos = [[NSMutableDictionary<NSString *, PhotoAlbumInfo *> alloc] init];
    self.collDict = [[NSMutableDictionary<NSString *, PHAssetCollection *> alloc] init];
    [self handleAllPhotosSpecialCase:cellEdge];
    PHFetchResult *newUserAlbums =
            [PHCollectionList fetchTopLevelUserCollectionsWithOptions:nil];
    [self handleAlbumFetchResult:newUserAlbums cellEdge:cellEdge];
}

- (void)handleAllPhotosSpecialCase:(CGFloat)cellEdge
{
    NSString *albumName = @"All Photos";
    PHFetchOptions * options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate"
                                                              ascending:true]];
    PHFetchResult *fetchRes = [PHAsset fetchAssetsWithOptions:options];
    if (!fetchRes || !fetchRes.count) {
        NSLog(@"Couldn't get photos");
        return;
    }

    PhotoAlbumInfo *info = [[PhotoAlbumInfo alloc] init];
    info.imageCount = fetchRes.count;

    PHAsset *firstPhotoAsset = fetchRes[0];
    Photo *firstPhoto = [[Photo alloc] initFromPHAsset:firstPhotoAsset];
    [firstPhoto returnThumbnail:CGSizeMake(cellEdge, cellEdge) completion:^(UIImage *image){
        info.firstPhoto = image;
    }];

    self.photoAlbumInfos[albumName] = info;
    self.collDict[albumName] = nil;
}

- (void)handleAlbumFetchResult:(PHFetchResult *)fetchResult cellEdge:(CGFloat)cellEdge
{
    for (PHAssetCollection *userAlbum in fetchResult) {
        //NSLog(@"Album is %@", [userAlbum localizedTitle]);
        PhotoAlbumInfo *info = [PhotoAlbumInfo new];
        UIImage *firstThumbnail;
        info.imageCount = [self photoLibraryGetCountAndFirstImage:userAlbum
                                                       firstImage:&firstThumbnail
                                                        cellEdge:cellEdge];
        info.firstPhoto = firstThumbnail;
        self.photoAlbumInfos[userAlbum.localizedTitle] = info;
        self.collDict[userAlbum.localizedTitle] = userAlbum;
    }
}

- (NSUInteger)photoLibraryGetCountAndFirstImage:(PHAssetCollection *)coll
                                     firstImage:(UIImage **)ppImage
                                       cellEdge:(CGFloat)cellEdge
{
    PHFetchOptions * options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate"
                                                              ascending:true]];

    __block UIImage *firstThumbnail = nil;
    PHFetchResult *fetchRes = [PHAsset fetchAssetsInAssetCollection:coll options:options];
    if (!fetchRes || !fetchRes.count) {
        *ppImage = nil;
        return 0;
    }
    PHAsset *firstAsset = fetchRes[0];
    PHImageRequestOptions * reqOptions = [[PHImageRequestOptions alloc] init];
    reqOptions.networkAccessAllowed = NO;
    reqOptions.synchronous = YES;
    [[PHImageManager defaultManager]
            requestImageForAsset:firstAsset
                      targetSize:CGSizeMake(cellEdge, cellEdge)
                     contentMode:PHImageContentModeAspectFill
                         options:reqOptions
                   resultHandler:^(UIImage *result, NSDictionary *info){
                        firstThumbnail = result;
                    }];

    *ppImage = firstThumbnail;
    return coll.estimatedAssetCount;
}


+ (void)checkImagePermissionsWithBlock:(UIViewController *)vc completion:(void (^)())completion
{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (status == PHAuthorizationStatusAuthorized) {
                if (completion) {
                    completion();
                }
            } else {
                UIAlertController * alert =
                [UIAlertController alertControllerWithTitle:@"No Photo Permissions"
                                                    message:@"Please grant photo "
                                                            @"permissions in Settings"
                                             preferredStyle:UIAlertControllerStyleAlert];

                [alert addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                                          style:UIAlertActionStyleCancel
                                                        handler:^(UIAlertAction *action) {
                    // Do nothing.
                }]];
                [alert addAction:[UIAlertAction actionWithTitle:@"Settings"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action) {
                    [[UIApplication sharedApplication]
                        openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }]];
                [vc presentViewController:alert animated:YES completion:nil];
            }
        });
    }];
};

- (BOOL)currentAlbumIsCameraRollOrAllPhotos
{
    if (!self.currentAlbum || !self.currentAlbum.name) {
        return NO;
    }
    return [self.currentAlbum.name isEqualToString:@"All Photos"];
}


- (PhotoAlbum *)loadCurrentAlbum:(void (^)(PhotoAlbum *))completion
{
    if (self.currentAlbum) {
        if (self.currentAlbum.photos) {
            self.currentAlbum.photos = nil;
        }
        self.currentAlbum = nil;
    }

    NSString *albumName = [self.currentAlbumName isEqualToString:@"Camera Roll"] ?
        @"All Photos" : self.currentAlbumName;

    PHAssetCollection *currentAlbumAsset =  self.collDict[albumName];
    self.currentAlbum =
            [[PhotoAlbum alloc] initForPhotoLibrary:currentAlbumAsset
                                                    name:albumName];
    [self.currentAlbum loadPhotos:completion];

    return self.currentAlbum;
}

- (void)getLatestImageFromCameraRoll:(void (^)(Photo *finalPhoto))completion
                             onError:(void (^)(NSError *error))onError
{
    PHFetchOptions * options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate"
                                                              ascending:false]];
    PHFetchResult *fetchRes = [PHAsset fetchAssetsWithOptions:options];
    if (!fetchRes || !fetchRes.count) {
        NSLog(@"Couldn't find camera shot");
        return;
    }
    PHAsset *latest = fetchRes[0];
    Photo *latestPhoto = [[Photo alloc] initFromPHAsset:latest];
    if (completion) {
        completion(latestPhoto);
    }
}

#pragma mark - Caching
- (void)resetCache
{
    [self.cachingMgr stopCachingImagesForAllAssets];
    if (self.currentAlbum) {
        [self.currentAlbum resetCache];
    }
}

- (void) updateCache:(CGRect)bounds
sortedVisibleIndices:(NSArray<NSIndexPath *> *)sortedVisibleIndices
           imageSize:(CGSize)imageSize
{
    if (self.currentAlbum && self.cachingMgr) {
        [self.currentAlbum updateCache:bounds
                  sortedVisibleIndices:sortedVisibleIndices
                           cacheingMgr:self.cachingMgr
                             imageSize:imageSize];
    }
}

- (void)cachedPhotoAtIndex:(NSIndexPath *)indexPath
                 imageSize:(CGSize)imageSize
                completion:(void (^)(UIImage *__nullable result,
        NSDictionary *__nullable info))completion
{
    if (!self.currentAlbum) {
        return;
    }

    if (self.cachingMgr) {
        [self.currentAlbum cachedPhotoAtIndex:indexPath
                                   cachingMgr:self.cachingMgr
                                    imageSize:imageSize
                                   completion:completion];
    }
}

- (void)updateCacheAsync:(CGRect)bounds
    sortedVisibleIndices:(NSArray<NSIndexPath *> *)sortedVisibleIndices
               imageSize:(CGSize)imageSize
{
    dispatch_async(self.cacheQueue,^{
        [self updateCache:bounds sortedVisibleIndices:sortedVisibleIndices imageSize:imageSize];
    });
}

+ (PHImageRequestOptions *)getCachedRequestOptions
{
    PHImageRequestOptions * options = [[PHImageRequestOptions alloc] init];
    options.networkAccessAllowed = NO;
//    options.resizeMode = PHImageRequestOptionsResizeModeFast;
//    options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    return options;
}
@end