//
//  ViewController.m
//  AnimDev
//
//  Created by Don Clore on 11/1/15.
//  Copyright © 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "ViewController.h"
#import "ScoutPhotoLibraryManager.h"
#import "ScoutPhotoAlbum.h"
#import "ScoutPhoto.h"
@import QuartzCore;

#define CELLEDGE 102
#define PHOTOWIDTH 225
#define PHOTOHEIGHT 300

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *header;
@property (weak, nonatomic) IBOutlet UITextView *bodyText;
@property (strong, nonatomic) UIToolbar *toolBar;
@property (strong, nonatomic) ScoutPhotoLibraryManager *manager;
@property (strong, nonatomic) NSMutableArray<UIImage *> *images;
@property (strong, nonatomic) NSMutableArray<ScoutPhoto *> *photos;
@property (strong, nonatomic) NSMutableArray<UIImageView *> *imageViews;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [ScoutPhotoLibraryManager checkImagePermissionsWithBlock:self completion:^{
        self.images = [NSMutableArray<UIImage *> new];
        self.imageViews = [NSMutableArray<UIImageView *> new];
        self.photos = [NSMutableArray<ScoutPhoto *> new];
        self.manager = [ScoutPhotoLibraryManager new];
        [self.manager loadAlbumInfos:CELLEDGE];
        [self.manager setCurrentAlbumName:@"All Photos"];
        [self.manager loadCurrentAlbum:^(ScoutPhotoAlbum *album){
            for (NSUInteger i = 0; i < 5; i++ ) {
                [self.photos addObject:album.photos[i]];
            }

            [self setupCameraAccessory];
        }];
    }];


}
- (IBAction)PostButtonClicked:(id)sender
{
    [self.bodyText resignFirstResponder];
    [self createThumbnailsAtBottomOfTextView];
    [self doViewAnimation:0];
}

- (void)doViewAnimation:(NSUInteger)idx
{
    if (idx >= self.imageViews.count) {
        return;
    }
    UIImageView *img = self.imageViews[idx];
    //CGAffineTransform t = CGAffineTransformMakeScale(1.0 / 0.2, 1.0 / 0.2);

    CGPoint newCenter = CGPointMake(self.bodyText.center.x,
            self.bodyText.center.y - self.header.bounds.size.height / 2);
    [UIView animateWithDuration:1.0
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             img.center = newCenter;
                             //img.transform = t;
                             img.bounds = CGRectMake(0.0, 0.0, PHOTOWIDTH, PHOTOHEIGHT);

        } completion:^(BOOL finished) {
                [self fadeOutAndShrink:idx];
            }];
}

- (void) fadeOutAndShrink:(NSUInteger)idx
{
    UIImageView *img = self.imageViews[idx];
    CGFloat yDelta = img.frame.origin.y;

    [UIView animateKeyframesWithDuration:2.0 delay:0.0 options:0 animations:^{

        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.25 animations:^{
            img.layer.transform = [self getTransformForPercent:0.25 originalY:yDelta];
            img.alpha = 0.7;
        }];

        [UIView addKeyframeWithRelativeStartTime:0.25 relativeDuration:0.25 animations:^{
            img.layer.transform = [self getTransformForPercent:0.50 originalY:yDelta];
            img.alpha = 0.5;
        }];

        [UIView addKeyframeWithRelativeStartTime:.50 relativeDuration:0.25 animations:^{
            img.layer.transform = [self getTransformForPercent:0.85 originalY:yDelta];
            img.alpha = 0.1;
        }];

        [UIView addKeyframeWithRelativeStartTime:0.75 relativeDuration:0.25 animations:^{
            img.layer.transform = [self getTransformForPercent:0.99999 originalY:yDelta];
            img.alpha = 0.05;
        }];

    } completion:^(BOOL finished) {
        img.alpha = 0.0;
        [self doViewAnimation:idx + 1];
    }];
}

- (CATransform3D)getTransformForPercent:(CGFloat)percent
                              originalY:(CGFloat)originalY
{
    CATransform3D t = CATransform3DIdentity;
    t.m34 = -1/(1000 + (1000 * percent));
    CATransform3D scale = CATransform3DScale(t, 1.0 - percent, 1.0 - percent, 1.0 - percent);
    CATransform3D trans = CATransform3DMakeTranslation(0.0, - (originalY * percent) * 2.5,
            - (originalY * percent) * 2.5);
    CGFloat angle = percent * (M_PI *4);
    CATransform3D rotate = CATransform3DRotate(t, angle, 1, 1, 0);
    return CATransform3DConcat(CATransform3DConcat(rotate, scale), trans);
}

- (void)createThumbnailsAtBottomOfTextView
{
    CGFloat x = 40;
    CGFloat y = self.bodyText.frame.size.height - 60;
    for (NSUInteger i = 0; i < 5; i++) {
        UIImageView *imgView = [[UIImageView alloc] initWithImage:self.images[i]];
        imgView.frame = CGRectMake(x, y, 45.0, 45.0);
        [self.imageViews addObject:imgView];
        [self.bodyText addSubview:imgView];
        x += 60;
    }
}

- (void)setupCameraAccessory
{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.bodyText.bounds.size.width, 60)];
    toolbar.barTintColor = [UIColor whiteColor];
    self.toolBar = toolbar;
    self.toolBar.barStyle = UIBarStyleDefault;
    UIImage *cameraImage = [UIImage imageNamed:@"upload_photo"];
    UIBarButtonItem *cameraButton =
        [[UIBarButtonItem alloc] initWithImage:cameraImage
                                         style:UIBarButtonItemStylePlain
                                        target:self
                                        action:@selector(cameraClicked:)];

    cameraButton.tintColor = [UIColor grayColor];
    [self.toolBar setItems:[NSArray arrayWithObjects:cameraButton, nil]];

    __block NSUInteger i;

    for (i = 0; i < 5; i++) {
        NSIndexPath *idx = [NSIndexPath indexPathForRow:i inSection:0];
        [self.manager cachedPhotoAtIndex:idx
                               imageSize:CGSizeMake(PHOTOWIDTH, PHOTOHEIGHT)
                              completion:^(UIImage *photo, NSDictionary *info) {
                                  [self.images addObject:photo];
                                  if (i == 4) {
                                      [self setImagesOnToolbar];
                                  }
                              }];
    }



}

- (void)setImagesOnToolbar
{
    NSMutableArray<UIBarButtonItem *> *newButtons = [NSMutableArray<UIBarButtonItem *> new];
    NSArray *currentButtons = [self.toolBar items];
    UIBarButtonItem *cameraButton = [currentButtons objectAtIndex:0];
    [newButtons addObject:cameraButton];

    for (NSUInteger i = 0; i < 5; i++) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45.0, 45.0)];
        UIBarButtonItem* selectedImageBtn = [[UIBarButtonItem alloc] initWithCustomView:button];
        selectedImageBtn.style = UIBarButtonItemStyleBordered;
        [newButtons addObject:selectedImageBtn];

        ScoutPhoto * photo = self.photos[i];
        [photo returnThumbnail:CGSizeMake(45.0, 45.0) completion:^(UIImage *thumbnail){
            [button setBackgroundImage:thumbnail forState:UIControlStateNormal];
        }];
    }
    [self.toolBar setItems:newButtons animated:NO];

    self.bodyText.inputAccessoryView = self.toolBar;
    // Doing this removes the visible border line on the toolbar.
    self.toolBar.clipsToBounds = YES;
    [self.bodyText becomeFirstResponder];
}

- (void)cameraClicked:(id)sender
{
    NSLog(@"Camera Clicked");
    [self.bodyText resignFirstResponder];
}


@end
