//
// Created by Don Clore on 10/6/15.
// Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ScoutPhotoAlbum;
@class PhotoAlbumInfo;
@class ScoutPhoto;
@class PHImageRequestOptions;

@interface ScoutPhotoLibraryManager : NSObject
@property (nonatomic, strong) ScoutPhotoAlbum  *currentAlbum;
@property (nonatomic, strong) NSString *currentAlbumName;
@property (nonatomic, strong)
NSMutableDictionary<NSString *, PhotoAlbumInfo *> *photoAlbumInfos;
-(void)loadAlbumInfos:(CGFloat)cellEdge;
+(void)checkImagePermissionsWithBlock:(UIViewController *)vc completion:(void (^)())completion;
-(ScoutPhotoAlbum *)loadCurrentAlbum:(void (^)(ScoutPhotoAlbum *))completion;
-(BOOL)currentAlbumIsCameraRollOrAllPhotos;
-(void)getLatestImageFromCameraRoll:(void (^)(ScoutPhoto *finalPhoto))completion
                             onError:(void (^)(NSError *error))onError;

// Caching
-(void)resetCache;
-(void)  updateCache:(CGRect)bounds
sortedVisibleIndices:(NSArray<NSIndexPath *> *)sortedVisibleIndices
           imageSize:(CGSize)imageSize;
- (void)cachedPhotoAtIndex:(NSIndexPath *)indexPath
                 imageSize:(CGSize)imageSize
                completion:(void (^)(UIImage * result,
                        NSDictionary * info))completion;

- (void)updateCacheAsync:(CGRect)bounds
    sortedVisibleIndices:(NSArray<NSIndexPath *> *)sortedVisibleIndices
               imageSize:(CGSize)imageSize;
+ (PHImageRequestOptions *)getCachedRequestOptions;
@end