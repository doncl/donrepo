//
// Created by Don Clore on 10/6/15.
// Copyright (c) 2015 Scout. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import "ScoutPhotoAlbum.h"
#import "ScoutPhoto.h"

@import Photos;

@interface ScoutPhotoAlbum()
@property (nonatomic, strong) ALAssetsGroup *oldStyleGroup;
@property (nonatomic, strong) PHAssetCollection *assetColl;
@end

@implementation ScoutPhotoAlbum

- (instancetype)initForPhotoLibrary:(PHAssetCollection *)assetCollection
                               name:(NSString *)albumName
{
    self = [super init];
    if (self) {
        self.assetColl = assetCollection;
        self.name = albumName;
        self.cachedIndices = [[NSMutableArray<NSIndexPath *> alloc] init];
        self.lastCacheFrameCenter = 0.0;
    }
    return self;
}

- (void)loadPhotos:(void (^)(ScoutPhotoAlbum *))completion
{
    [self loadPhotosHelperNewStyle:^(NSMutableArray<ScoutPhoto *> *photos) {
        self.photos = photos;
        if (completion) {
            completion(self);
        }
    }];
}

- (void)loadPhotosHelperNewStyle:(void (^)(NSMutableArray<ScoutPhoto *> *))completion
{
    __block NSMutableArray<ScoutPhoto *> *photos = [[NSMutableArray<ScoutPhoto *> alloc] init];
    PHFetchOptions * options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate"
                                                              ascending:false]];

    PHFetchResult *fetchRes;
    if (self.assetColl) {
        fetchRes = [PHAsset fetchAssetsInAssetCollection:self.assetColl options:options];
    } else if ([self.name isEqualToString:@"All Photos"]){
        // All Photos
        fetchRes = [PHAsset fetchAssetsWithOptions:options];
    }

    PHImageRequestOptions * reqOptions = [[PHImageRequestOptions alloc] init];
    reqOptions.networkAccessAllowed = NO;
    reqOptions.synchronous = YES;

    for (PHAsset *asset in fetchRes) {
        ScoutPhoto *photo = [[ScoutPhoto alloc] initFromPHAsset:asset];
        [photos addObject:photo];
    }
    if (completion) {
        completion(photos);
    }
}

#pragma mark - Cacheing

- (void)resetCache
{
    [self.cachedIndices removeAllObjects];
    self.lastCacheFrameCenter = 0.0;
}

// Returns true if caller needs to call PHCachingManager.stopCachingImagesForAllAssets.
- (void) updateCache:(CGRect)bounds
sortedVisibleIndices:(NSArray<NSIndexPath *> *)sortedVisibleIndices
         cacheingMgr:(PHCachingImageManager *)cachingMgr
           imageSize:(CGSize)imageSize
{
    if (!sortedVisibleIndices.count) {
        return;
    }
    CGFloat currentFrameCenter = CGRectGetMidY(bounds);
    CGFloat scrollDiff = fabs(currentFrameCenter - self.lastCacheFrameCenter);
    CGFloat amountNeededToTriggerCacheChange = CGRectGetHeight(bounds) / 3.0;
    if (scrollDiff < amountNeededToTriggerCacheChange) {
        // Don't recalculate the caching for a very small scroll operation.
        return;
    }

    self.lastCacheFrameCenter = currentFrameCenter;
    static NSInteger cOffScreenAssetsToCache = 60;

    NSInteger firstItemToCache = sortedVisibleIndices[0].item - cOffScreenAssetsToCache / 2;
    firstItemToCache = MAX(firstItemToCache, 1);

    NSInteger lastItemToCache = [sortedVisibleIndices lastObject].item +
             cOffScreenAssetsToCache/2;
    if (self.photos) {
        lastItemToCache = MIN(lastItemToCache, self.photos.count - 1);
    }

    NSMutableArray<NSIndexPath *> *indexesToStopCaching = [NSMutableArray<NSIndexPath *> new];
    NSMutableArray<NSIndexPath *> *indexesToStartCaching = [NSMutableArray<NSIndexPath *> new];

    // Stop caching items we scrolled past
    for (NSIndexPath * index in self.cachedIndices) {
        if (index.item < firstItemToCache || index.item > lastItemToCache) {
            [indexesToStopCaching addObject:index];
        }
    }
    [self.cachedIndices removeObjectsInArray:indexesToStopCaching];
    PHImageRequestOptions *options = [ScoutPhotoLibraryManager getCachedRequestOptions];

    NSArray<PHAsset *> *assetsToStopCaching = [self assetsAtIndexPaths:indexesToStopCaching];
    if (assetsToStopCaching) {
        [cachingMgr stopCachingImagesForAssets:assetsToStopCaching
                                    targetSize:imageSize
                                   contentMode:PHImageContentModeAspectFill
                                       options:options];
    }

    // Start caching new items in range
    for (NSInteger i = firstItemToCache; i < lastItemToCache; i++) {
        NSIndexPath * index = [NSIndexPath indexPathForItem:i inSection:0];
        if (![self.cachedIndices containsObject:index]) {
            [indexesToStartCaching addObject:index];
            [self.cachedIndices addObject:index];
        }
    }

    NSArray<PHAsset *> *assetsToStartCaching = [self assetsAtIndexPaths:indexesToStartCaching];
    if (assetsToStartCaching) {
        [cachingMgr startCachingImagesForAssets:assetsToStartCaching
                                     targetSize:imageSize
                                    contentMode:PHImageContentModeAspectFill
                                        options:options];
    }
}

- (void)cachedPhotoAtIndex:(NSIndexPath *)indexPath
                        cachingMgr:(PHCachingImageManager *)cachingMgr
                         imageSize:(CGSize)imageSize
                        completion:(void (^)(UIImage * result, NSDictionary * info))completion
{
    ScoutPhoto *photo = self.photos[indexPath.item];
    PHAsset *asset = photo.phAsset;
    PHImageRequestOptions *options = [ScoutPhotoLibraryManager getCachedRequestOptions];
    [cachingMgr requestImageForAsset:asset
                          targetSize:imageSize
                         contentMode:PHImageContentModeAspectFill
                             options:options
                       resultHandler:completion];
}

- (NSArray<PHAsset *> *)assetsAtIndexPaths:(NSArray *)indexPaths
{
    if (!indexPaths.count || !self.photos || !self.photos.count) {
        return nil;
    }

    NSMutableArray<PHAsset*> *assets =
            [NSMutableArray<PHAsset *> arrayWithCapacity:indexPaths.count];

    for (NSIndexPath *indexPath in indexPaths) {
        ScoutPhoto *photo = self.photos[indexPath.item - 1];
        [assets addObject:photo.phAsset];
    }
    return assets;
}
@end