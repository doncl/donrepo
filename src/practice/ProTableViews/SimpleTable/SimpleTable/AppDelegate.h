//
//  AppDelegate.h
//  SimpleTable
//
//  Created by Don Clore on 7/21/15.
//  Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;

@end

