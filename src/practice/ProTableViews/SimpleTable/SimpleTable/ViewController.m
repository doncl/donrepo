//
//  ViewController.m
//  SimpleTable
//
//  Created by Don Clore on 7/21/15.
//  Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize tableData;
@synthesize cellCount;

- (void)viewDidLoad {
    [super viewDidLoad];

    self.cellCount = 0;

    // Create the array to hold the table data.
    self.tableData = [[NSMutableArray alloc] init];

    // Create and add 99,999 data items to the table data array.
    for (int i = 0; i < 99999; i++) {
        // The cell will contains a string 'Item X'
        NSString *dataString = [NSString stringWithFormat:@"Item %d", i];

        // Here the new string is added to the end of the array.
        [self.tableData addObject:dataString];
    }

    // Print out the contents of the array into the log.
    NSLog(@"The tabledata array contains %d items", [self.tableData count]);
    NSLog(@"There are %d cells initially", self.cellCount);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    self.tableData = nil;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        self.cellCount++;
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.text = [self.tableData objectAtIndex:indexPath.row];

    NSLog(@"There are now %d cells", self.cellCount);

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Table row %d has been tapped", indexPath.row);

    NSString *messageString = [NSString stringWithFormat:@"You tapped row %d", indexPath.row];

    UIAlertView *alert = [[UIAlertView alloc]
            initWithTitle:@"Row tapped"
                  message:messageString
                 delegate:nil
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil];

    [alert show];
}


@end
