//
//  ViewController.m
//  BabyNames
//
//  Created by Don Clore on 7/21/15.
//  Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "ViewController.h"
#import "BNName.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Baby Names";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.tableData) {
        return [self.tableData count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"BabyNameCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    // Extract the BNNAme object from the tableData.
    BNName *tempName = [self.tableData objectAtIndex:indexPath.row];

    // Update the cell's textLabel.
    cell.textLabel.text = tempName.nameText;

    return cell;
}

@end
