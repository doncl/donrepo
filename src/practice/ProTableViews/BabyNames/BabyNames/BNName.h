//
//  BNName.h
//  BabyNames
//
//  Created by Don Clore on 7/21/15.
//  Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNName : NSObject

@property (nonatomic, strong) NSString *nameText;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *derivation;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSString *notes;

@end
