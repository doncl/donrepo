//
//  main.m
//  BabyNames
//
//  Created by Don Clore on 7/21/15.
//  Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
