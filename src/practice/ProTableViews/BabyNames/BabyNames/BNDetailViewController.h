//
//  BNDetailViewController.h
//  BabyNames
//
//  Created by Don Clore on 7/21/15.
//  Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BNName.h"

@interface BNDetailViewController : UIViewController
@property (nonatomic, retain) BNName *BNNAme;
@property (strong, nonatomic) IBOutlet UILabel *nameTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *genderLabel;
@property (strong, nonatomic) IBOutlet UILabel *derivationLabel;
@property (strong, nonatomic) IBOutlet UILabel *notesLabel;
@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;

@end
