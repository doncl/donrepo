//
//  PinchLayout.h
//  FlickrSearch
//
//  Created by Don Clore on 8/1/15.
//  Copyright (c) 2015 RookSoft Pte. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PinchLayout : UICollectionViewFlowLayout
@property (nonatomic, assign) CGFloat pinchScale;
@property (nonatomic, assign) CGPoint pinchCenter;
@end
