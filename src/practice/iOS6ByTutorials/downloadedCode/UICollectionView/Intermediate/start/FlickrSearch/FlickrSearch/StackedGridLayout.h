//
//  StackedGridLayout.h
//  FlickrSearch
//
//  Created by Don Clore on 8/1/15.
//  Copyright (c) 2015 RookSoft Pte. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StackedGridLayoutDelegate <UICollectionViewDelegate>
- (NSInteger)collectionView:(UICollectionView *)cv
                     layout:(UICollectionViewLayout *)cvl
   numberOfColumnsInSection:(NSInteger)section;

- (CGSize)collectionView:(UICollectionView *)cv
                  layout:(UICollectionViewLayout *)cvl
    sizeForItemWithWidth:(CGFloat)width
             atIndexPath:(NSIndexPath *)indexPath;

- (UIEdgeInsets)collectionView:(UICollectionView *)cv
                        layout:(UICollectionViewLayout *)cvl
   itemInsetsForSectionAtIndex:(NSInteger)section;

@end

@interface StackedGridLayout : UICollectionViewLayout
@property (nonatomic, assign) CGFloat headerHeight;
@end
