//
//  CoverFlowLayout.h
//  FlickrSearch
//
//  Created by Don Clore on 8/10/15.
//  Copyright (c) 2015 RookSoft Pte. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoverFlowLayout : UICollectionViewFlowLayout

@end
