//
//  StackedGridLayoutSection.h
//  FlickrSearch
//
//  Created by Don Clore on 8/1/15.
//  Copyright (c) 2015 RookSoft Pte. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StackedGridLayoutSection : NSObject

@property (nonatomic, assign, readonly) CGRect frame;
@property (nonatomic, assign, readonly) UIEdgeInsets itemInsets;
@property (nonatomic, assign, readonly) CGFloat columnWidth;
@property (nonatomic, assign, readonly) NSInteger numberOfItems;

- (instancetype)initWithOrigin:(CGPoint)origin
                         width:(CGFloat)width
                       columns:(NSInteger)columns
                    itemInsets:(UIEdgeInsets)itemInsets;

- (void)addItemOfSize:(CGSize)size forIndex:(NSInteger)index;

- (CGRect)frameForItemAtIndex:(NSInteger)index;
@end
