//
//  WrapperCollection.m
//  Scout
//
//  Created by Avi Finkel on 1/29/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "WrapperCollection.h"

@interface WrapperCollection ()

@property NSMutableArray *wrappers;

@end

@implementation WrapperCollection

- (id)init
{
    self = [super init];
    if (self) {
        self.wrappers = [NSMutableArray new];
    }
    return self;
}

- (ScoutDataWrapper *)lastWrapperIfOfType:(Class)wrapperClass
{
    NSMutableArray *wrappers = [self descendOpenNestingWrappers];
    if (wrappers.count && [wrappers[wrappers.count - 1] isKindOfClass:wrapperClass]) {
        return wrappers[wrappers.count - 1];
    }
    return nil;
}

- (ScoutDataWrapper *)lastWrapperOrAppendOfType:(Class)wrapperClass
{
    ScoutDataWrapper *lastWrapper = [self lastWrapperIfOfType:wrapperClass];
    if (!lastWrapper) {
        lastWrapper = [[wrapperClass alloc] init];
        [self appendWrapper:lastWrapper];
    }
    return lastWrapper;
}

- (void)appendWrapper:(ScoutDataWrapper *)wrapper
{
    [[self descendOpenNestingWrappers] addObject:wrapper];
}

- (NSMutableArray *)descendOpenNestingWrappers
{
    NestingWrapper *lastNestingWrapper = [self deepestNestingWrapperIfOfType:[NestingWrapper class]];
    if (lastNestingWrapper) {
        return lastNestingWrapper.nestedWrappers;
    }
    return self.wrappers;
}

- (NestingWrapper *)deepestNestingWrapperIfOfType:(Class)wrapperClass
{
    NSMutableArray *wrappers = self.wrappers;
    NestingWrapper *lastWrapper = nil;
    while (wrappers.count && [wrappers[wrappers.count - 1] isKindOfClass:[NestingWrapper class]] && ((NestingWrapper *)wrappers[wrappers.count - 1]).open) {
        lastWrapper = wrappers[wrappers.count - 1];
        wrappers = lastWrapper.nestedWrappers;
    }
    return lastWrapper;
}

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id [])buffer count:(NSUInteger)len
{
    return [self.wrappers countByEnumeratingWithState:state objects:buffer count:len];
}

@end
