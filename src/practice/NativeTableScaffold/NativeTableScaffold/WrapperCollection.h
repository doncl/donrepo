//
//  WrapperCollection.h
//  Scout
//
//  Created by Avi Finkel on 1/29/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ScoutDataWrapper.h"
#import "NestingWrapper.h"

@interface WrapperCollection : NSObject<NSFastEnumeration>

- (ScoutDataWrapper *)lastWrapperIfOfType: (Class)wrapperClass;

- (ScoutDataWrapper *)lastWrapperOrAppendOfType: (Class)wrapperClass;

- (NestingWrapper *)deepestNestingWrapperIfOfType: (Class)wrapperClass;

- (void)appendWrapper: (ScoutDataWrapper *)wrapper;

@end
