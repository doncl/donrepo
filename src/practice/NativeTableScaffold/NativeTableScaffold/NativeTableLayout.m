//
// Created by Don Clore on 8/2/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "NativeTableLayout.h"
#import "NativeTableModel.h"

@interface NativeTableLayout()
@property (nonatomic, assign) NSUInteger rowCount;
@property (nonatomic, assign) NSUInteger colCount;
@end

@implementation NativeTableLayout

- (instancetype)initWithLayoutType:(NativeTableLayoutType)layoutType
                          delegate:(id<NativeTableCellViewDelegate>)delegate
                             model:(NativeTableModel *)model
{
    self = [super init];
    if (self) {
        self.layoutType = layoutType;
        self.delegate = delegate;
        [self calculateCellSizes:model];
    }
    return self;
}

- (UICollectionViewLayoutAttributes *)
layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attrs =
        [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];

    attrs.frame = [self frameForItemAtIndexPath:indexPath];
    return attrs;
}

- (CGRect)frameForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // indexPath.section is the column index.  indexPath.row is the row index.
    // errata:  trying it the other way around. section is row, row is column :).
    NSInteger columnIdx = indexPath.row;
    NSInteger rowIdx = indexPath.section;

    return [self getCellRect:columnIdx rowIdx:rowIdx];
}

- (CGRect)getCellRect:(NSInteger)columnIdx rowIdx:(NSInteger)rowIdx
{
    CGFloat x = 0.0f;
    CGFloat y = rowIdx * self.rowHeight;
    CGFloat width;
    CGFloat height = self.rowHeight;

    // The column widths are variable values, so they have to be added up.
    for (NSInteger i = 0; i < columnIdx; i++) {
        x += [[_columnWidths objectAtIndex:i] floatValue];
    }
    width = [[_columnWidths objectAtIndex:columnIdx] floatValue];

    return CGRectMake(x, y, width, height);
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *attrsArray = [NSMutableArray new];
    NSUInteger cCols = self.colCount;
    NSUInteger cRows = self.rowCount;

    for (NSUInteger iCol = 0; iCol < cCols; iCol++) {
        for (NSUInteger iRow = 0; iRow < cRows; iRow++) {
            CGRect cellRect = [self getCellRect:iCol rowIdx:iRow];
            if (CGRectIntersectsRect(cellRect, rect)) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:iCol inSection:iRow];
                UICollectionViewLayoutAttributes *attrs =
                    [self layoutAttributesForItemAtIndexPath:indexPath];

                [attrsArray addObject:attrs];
            }
        }
    }
    return attrsArray;
}

- (CGSize)collectionViewContentSize
{
    return CGSizeMake([self getContentWidth], self.tableHeight);
}

- (CGFloat)getContentWidth
{
    CGFloat cy = 0.0f;
    for (NSUInteger i = 0; i < self.colCount; i++) {
        cy += [[self.columnWidths objectAtIndex:i] floatValue];
    }
    return cy;
}

//
// calculateCellSizes calls back into the delegate to get information about dimensions needed
// for layout.
//
- (void)calculateCellSizes:(NativeTableModel *)model
{
    NSDictionary *tableDimensions = [self.delegate getTableDimensions:self.layoutType];
    self.colCount = [tableDimensions[@"columnCount"] floatValue];
    self.rowCount = [tableDimensions[@"rowCount"] floatValue];
    self.tableHeight = [tableDimensions[@"tableHeight"] floatValue];
    self.tableWidth = [tableDimensions[@"tableWidth"] floatValue];
    self.rowHeight = [tableDimensions[@"rowHeight"] floatValue];
    self.columnWidths = tableDimensions[@"columnWidths"];
}

@end