//
//  NSString+ParseIntoMediaUrl.m
//  Scout
//
//  Created by Avi Finkel on 1/3/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "NSString+MacroParser.h"
#import "NSNumber+BuildImageUrl.h"
#import "NSString+CategoryAdditions.h"

@implementation NSString (MacroParser)

// One subtlety to this is that this string (self) can be either a Media macro (e.g. [MEDIA:nnnnnn]) or a full Uri.
// parseOutMediaId will just return the original string for non-media macros, then isNumeric will return false.
// In that case, we parse it to an NSURL and return it.   Otherwise, if a numeric id resulted from the parse, then
// we use that number to build a media image url.
- (NSURL *)parseStoryImageURL
{
    NSString *mediaId = [self parseOutMediaId];
    if (![mediaId isNumeric]) {
        // Some story StoryImage values are explicitly Uri's, rather than being media macros.
        NSURL *explicitMediaUrl = [[NSURL alloc] initWithString:self];
        if (!explicitMediaUrl) {
            // This kind of error needs to be caught right away.
            [NSException raise:@"Fatal error" format:@"\"%@\" is not a numeric string - stack = %@", mediaId, [NSThread callStackSymbols]];
        }
        return explicitMediaUrl;
    }

    int mediaIdInt = [mediaId intValue];
    NSNumber *nsNumberMediaId = [NSNumber numberWithInt:mediaIdInt];
    NSURL *mediaUrl = [nsNumberMediaId buildImageUrl];
    return mediaUrl;
}

- (NSString *)parseOutMediaId
{
    NSCharacterSet *endCharSet = [NSMutableCharacterSet characterSetWithCharactersInString:@"]"];
    NSScanner *scanner = [NSScanner scannerWithString:self];

    [scanner scanString:@"[MEDIA:" intoString:NULL];
    NSString * imageId;
    [scanner scanUpToCharactersFromSet:endCharSet intoString:&imageId];
    return imageId;
}

- (NSString *)parseOutYoutubeId
{
    NSRange range = [self rangeOfString:@"[YOUTUBE:"];
    NSRange lastRange = [self rangeOfString:@"]"];
    
    return [self substringWithRange:NSMakeRange(range.location + range.length, lastRange.location - (range.location +range.length))];
}

- (NSString*)parseOutPlayerCardId
{
    //just need to parse string from first quote to laste quote in string of this pattern: "<playercardview id="3423234">"
    NSRange range = [self rangeOfString:@"id=\""];
    NSRange lastRange = [self rangeOfString:@"\">"];
    
    NSString* playerId = [self substringWithRange:NSMakeRange(range.location + range.length, lastRange.location - (range.location +range.length))];
    return playerId;
}

- (NSMutableArray*)parseGalleryIds
{
    NSRange range = [self rangeOfString:@"[GALLERY:" options:NSCaseInsensitiveSearch];
    NSString* ids = [self substringFromIndex:range.location + range.length];
    range = [ids rangeOfString:@"]"];
    ids = [ids substringToIndex:range.location];
    
    NSArray* idArray = [ids componentsSeparatedByString:@","];
    NSMutableArray* trimmedIds = [[NSMutableArray alloc] init];
    
    for(int i = 0; i < idArray.count; i++) {
        [trimmedIds addObject:[[idArray objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    }
    
    return trimmedIds;
}

@end
