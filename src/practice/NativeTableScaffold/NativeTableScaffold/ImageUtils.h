//
//  ImageUtils.h
//  Scout
//
//  Created by Sonu on 4/6/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "SDImageCache.h"
#import "ViewDescriptor.h"

@interface ImageUtils : NSObject

+ (CGSize)resizeImageWithWidth: (CGFloat)width height:(CGFloat)height frameWidth:(CGFloat)frameWidth;
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;

@end
