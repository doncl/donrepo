//
//  NSNumber+BuildImageUrl.m
//  Scout
//
//  Created by Avi Finkel on 1/15/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "NSNumber+BuildImageUrl.h"

@implementation NSNumber (BuildImageUrl)

- (NSURL *)buildImageUrl
{
    return [NSURL URLWithString:[self buildImageString]];
}

- (NSString*)buildImageString
{
    long imageId = [self longValue];
    return [NSString stringWithFormat:@"http://imgix.scout.com/%ld/%ld.jpg", imageId / 10000, imageId];
}

- (NSString*)buildPlayerImageURL
{
    return [NSString stringWithFormat:@"%@?w=200&h=200&fit=crop&crop=faces", [self buildImageString]];
}


@end
