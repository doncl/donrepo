//
//  ViewDescriptor.m
//  Scout
//
//  Created by Avi Finkel on 12/29/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import "ViewDescriptor.h"

@implementation ViewDescriptor

- (ViewDescriptor *)descriptorByApplyingSidePadding: (CGFloat)sidePadding
{
    ViewDescriptor *copy = [[ViewDescriptor alloc] init];
    copy.plainFont = self.plainFont;
    copy.boldFont = self.boldFont;
    copy.italicFont = self.italicFont;
    copy.foregroundColor = self.foregroundColor;
    copy.verticalPadding = self.verticalPadding;
    copy.width = self.width - (sidePadding * 2);
    return copy;
}

@end
