//
//  Player.m
//  Scout
//
//  Created by Sonu on 8/17/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "Player.h"


@implementation Player

@dynamic committed;
@dynamic committedTeam;
@dynamic firstName;
@dynamic forty;
@dynamic height;
@dynamic imageURL;
@dynamic isFortyVerified;
@dynamic lastName;
@dynamic overallRank;
@dynamic playerId;
@dynamic playerNodeId;
@dynamic postitionAbbreviation;
@dynamic rank;
@dynamic schoolsOfInterest;
@dynamic spotlightVideoURL;
@dynamic starRating;
@dynamic weight;
@dynamic canonicalURL;

@end
