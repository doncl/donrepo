//
// Created by Don Clore on 7/30/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "NativeTableCell.h"
#import "CellInfo.h"
#import "CellProperties.h"


@implementation NativeTableCell

#pragma mark - Cell static data initialization
static UIColor *headerColor = nil;

// Header font static variables.
static NSString *headerFontName = nil;
static CGFloat headerFontSize;
static UIColor *headerTextColor = nil;

// Data cell font static variables.
static NSString *dataCellFontName = nil;
static NSString *dataCellBoldFontName = nil;
static NSString *dataCellItalicFontName = nil;
static CGFloat dataCellFontSize;
static UIColor *dataCellTextColor = nil;

static UIColor *oddRowColor = nil;
static UIColor *evenRowColor = nil;

// Border static variables.
static UIColor *borderColor = nil;
static CGFloat borderWidth;


//+-------------------------------------------------------------------------------------------
// STATIC INITIALIZATION - The purpose here is to give valid initial values for cell visual
// properties.  They can be set to custom values by calling the initializeCellProperties class
// method.
//--------------------------------------------------------------------------------------------
+ (void)initialize
{
    headerColor = [UIColor colorWithRed:0.271 green:0.271 blue:0.271 alpha:1];
    headerFontName = @"HelveticaNeue-Bold";
    headerFontSize = 12.0f;
    headerTextColor = [UIColor colorWithRed:0.976 green:0.976 blue:0.976 alpha:1];

    dataCellFontName = @"HelveticaNeue";
    dataCellBoldFontName = headerFontName;
    dataCellItalicFontName = @"HelveticaNeue-Italic";
    dataCellFontSize = 10.0f;
    dataCellTextColor = [UIColor colorWithRed:0.271 green:0.271 blue:0.271 alpha:1];

    oddRowColor =  [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    evenRowColor = [UIColor colorWithRed:0.976 green:0.976 blue:0.976 alpha:1];

    borderColor = [UIColor colorWithRed:223.0/255.0 green:223.0/255.0
                                   blue:223.0/255.0 alpha:1.0];

    borderWidth = 0.5f;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.label = [[UILabel alloc] initWithFrame:self.contentView.bounds];
        [self.label setContentHuggingPriority:0 forAxis:UILayoutConstraintAxisHorizontal];
        [self.label setContentCompressionResistancePriority:1000
                                                    forAxis:UILayoutConstraintAxisHorizontal];
        self.label.numberOfLines = 0;
        self.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        [self setUpCellInfo:dataOdd];
        self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
        self.label.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.label];
        [self setupConstraints];
    }
    return self;
}

- (void)setupConstraints
{
    UILabel *cellLabel = self.label;
    NSDictionary *nameMap = @{@"cellLabel" : cellLabel};
    NSArray* horizontalConstraints =
        [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[cellLabel]-0-|"
                                                options:0 metrics:nil views:nameMap];

    NSLayoutConstraint *centeringXConstraint =
        [NSLayoutConstraint constraintWithItem:self
                                     attribute:NSLayoutAttributeCenterX
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self.label
                                     attribute:NSLayoutAttributeCenterX
                                    multiplier:1.f constant:0.f];

    NSLayoutConstraint *centeringYConstraint =
        [NSLayoutConstraint constraintWithItem:self
                                     attribute:NSLayoutAttributeCenterY
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self.label
                                     attribute:NSLayoutAttributeCenterY
                                    multiplier:1.f constant:0.f];

    NSArray *centeringConstraints = @[centeringXConstraint, centeringYConstraint];
    [self addConstraints:horizontalConstraints];
    [NSLayoutConstraint activateConstraints:horizontalConstraints];
    [NSLayoutConstraint activateConstraints:centeringConstraints];
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
}

- (void)setUpCellInfo:(cellType)cellType
{
    CellInfo *ci = [NativeTableCell getCellInfoForTypeOfCell:cellType];
    if (ci) {
            self.layer.borderWidth = ci.borderWidth;
            self.layer.borderColor = ci.borderColor.CGColor;
            self.label.baselineAdjustment = ci.baselineAdjustment;
            self.label.backgroundColor = ci.backgroundColor;
            self.backgroundColor = ci.backgroundColor;
            self.label.textAlignment = ci.textAlignment;
            self.label.font = [UIFont fontWithName:ci.fontName size:ci.fontsize];
            self.label.textColor = ci.textColor;
            self.label.numberOfLines = 0;
            self.label.lineBreakMode = NSLineBreakByWordWrapping;
        }
}

- (void)setCellText:(NSString *)text
{
    self.label.text = text;
}

+ (CellInfo *)getCellInfoForTypeOfCell:(cellType)typeOfCell
{
    CellInfo *info = [CellInfo new];
    info.borderColor = borderColor;
    info.borderWidth = borderWidth;
    info.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    info.textAlignment = NSTextAlignmentCenter;

    if (typeOfCell == dataOdd || typeOfCell == dataEven) {
        info.textColor = dataCellTextColor;
        info.fontsize = dataCellFontSize;
        info.fontName = dataCellFontName;
        info.italicFontName = dataCellItalicFontName;
        info.boldFontName = dataCellBoldFontName;
        info.backgroundColor = typeOfCell == dataOdd ? oddRowColor : evenRowColor;
    } else if (typeOfCell == columnHeading) {
        info.backgroundColor = headerColor;
        info.textColor = headerTextColor;
        info.fontsize = headerFontSize;
        info.fontName = headerFontName;
    }
    return info;
}

+ (void)initializeCellProperties:(CellProperties *)properties {
    headerColor = properties.headerColor;

    headerFontName = properties.headerFontName;
    headerFontSize = properties.headerFontSize;
    headerTextColor = properties.headerTextColor;

    dataCellFontName = properties.dataCellFontName;
    dataCellBoldFontName = properties.dataCellBoldFontName;
    dataCellItalicFontName = properties.dataCellItalicFontName;
    dataCellFontSize = properties.dataCellFontSize;
    dataCellTextColor = properties.dataCellTextColor;

    oddRowColor = properties.oddRowColor;
    evenRowColor = properties.evenRowColor;

    borderColor = properties.borderColor;
    borderWidth = properties.borderWidth;
}


- (void)layoutSubviews {
    [super layoutSubviews];
}
@end