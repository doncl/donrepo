//
//  ParseContext.h
//  Scout
//
//  Created by Avi Finkel on 2/13/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParseContext : NSObject

@property NSString *subdomain;

@end
