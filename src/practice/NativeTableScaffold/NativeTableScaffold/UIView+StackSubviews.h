//
//  UIView+StackSubviews.h
//  Scout
//
//  Created by Avi Finkel on 1/29/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (StackSubviews)

- (void)stackSubviewsWithSidePadding: (CGFloat)sidePadding withVerticalPadding: (CGFloat)verticalPadding;

- (void)stackSubviewsWithSidePadding: (CGFloat)sidePadding withVerticalPadding: (CGFloat)verticalPadding exceptFor: (NSSet *)ignoreViews;

- (void)stackSubviewsWithSidePadding: (CGFloat)sidePadding withVerticalPadding: (CGFloat)verticalPadding exceptFor: (NSSet *)ignoreViews producingHeight:(CGFloat *)height;

- (void)addSubviews:(NSArray *)views;

- (void)removeSubviews:(NSArray *)views;

@end
