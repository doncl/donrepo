//
//  Media.h
//  Scout
//
//  Created by Don Clore on 5/28/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Media : NSManagedObject

@property (nonatomic, retain) NSString * caption;
@property (nonatomic) int32_t height;
@property (nonatomic, retain) NSString * mediaId;
@property (nonatomic, retain) NSString * thumbnailUri;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * uri;
@property (nonatomic) int32_t width;

@end
