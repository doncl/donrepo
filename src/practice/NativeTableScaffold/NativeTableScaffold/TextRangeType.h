//
//  TextRangeType.h
//  Scout
//
//  Created by Avi Finkel on 12/31/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#ifndef Scout_TextRangeType_h
#define Scout_TextRangeType_h

typedef enum {
    BOLD,
    ITALIC,
    LINK,
    HEADER1,
    HEADER2,
    HEADER3,
    HEADER4,
    HEADER5,
    HEADER6
} TextRangeType;

#endif
