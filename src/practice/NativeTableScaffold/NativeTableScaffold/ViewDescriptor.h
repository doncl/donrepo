//
//  ViewDescriptor.h
//  Scout
//
//  Created by Avi Finkel on 12/29/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ViewDescriptor : NSObject

@property UIFont *plainFont;
@property UIFont *boldFont;
@property UIFont *italicFont;
@property UIColor *foregroundColor;
@property CGFloat width;
@property CGFloat verticalPadding;

- (ViewDescriptor *)descriptorByApplyingSidePadding: (CGFloat)sidePadding;

@end
