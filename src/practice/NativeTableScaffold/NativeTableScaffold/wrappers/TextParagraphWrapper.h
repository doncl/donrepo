//
//  TextParagraphWrapper.h
//  Scout
//
//  Created by Avi Finkel on 12/28/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import "ScoutDataWrapper.h"
#import "TextRangeType.h"

@interface TextParagraphWrapper : ScoutDataWrapper

@property (readonly) NSString *trimmedText;
@property NSString *prefix;

- (void)beginRangeOfType: (TextRangeType)type;
- (void)endRangeOfType: (TextRangeType)type;
- (void)addUrlToLastLinkRange: (NSURL*) url;
- (void)appendText: (NSString *)text;

@end
