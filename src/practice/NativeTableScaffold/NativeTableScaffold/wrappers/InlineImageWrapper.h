//
//  UILabelImageWrapper.h
//  Scout
//
//  Created by Sonu on 1/28/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "ScoutDataWrapper.h"

@interface InlineImageWrapper : ScoutDataWrapper

@property NSURL* url;

@end
