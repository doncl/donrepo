//
//  ListParagraphWrapper.m
//  Scout
//
//  Created by Avi Finkel on 1/5/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "ListParagraphWrapper.h"

@interface ListParagraphWrapper ()

@property NSMutableArray *items;

@end

@implementation ListParagraphWrapper

- (id) init
{
    self = [super init];
    if (self) {
        self.items = [NSMutableArray new];
    }
    return self;
}

- (void) appendText:(NSString *)text
{
    [[self guaranteeParagraph] appendText:text];
}

- (void)beginNewListItem
{
    [self.items addObject:[[TextParagraphWrapper alloc] init]];
}

- (void)beginRangeOfType: (TextRangeType)type
{
    [[self guaranteeParagraph] beginRangeOfType:type];
}

- (void)endRangeOfType: (TextRangeType)type
{
    [[self guaranteeParagraph] endRangeOfType:type];
}

- (void)addUrlToLastLinkRange: (NSURL*) url
{
    [[self guaranteeParagraph] addUrlToLastLinkRange:url];
}

- (TextParagraphWrapper *)guaranteeParagraph
{
    if (!self.items.count) {
        [self beginNewListItem];
    }
    return [self.items lastObject];
}

- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc pendingViewCounter:(int *)pendingViewCounter onPendingViewsResolved:(void (^)())viewsResolved
{
    NSMutableArray *views = [NSMutableArray new];
    int count = 1;
    for (TextParagraphWrapper *paragraph in self.items) {
        if (self.ordered) {
            paragraph.prefix = [NSString stringWithFormat:@"%d. ", count];
            count++;
        }
        else {
            paragraph.prefix = @"\u2022 ";
        }
        [views addObjectsFromArray:[paragraph renderToViewsWithViewDescriptor:viewDesc pendingViewCounter:pendingViewCounter onPendingViewsResolved:viewsResolved]];
    }
    if (viewsResolved) {
        viewsResolved();
    }
    return [views copy];
}

@end
