//
//  ScoutDataWrapper.h
//  Scout
//
//  Created by Avi Finkel on 12/28/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewDescriptor.h"

@interface ScoutDataWrapper : NSObject

- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc;
- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc pendingViewCounter:(int *)pendingViewCounter onPendingViewsResolved:(void (^)())viewsResolved;

@end
