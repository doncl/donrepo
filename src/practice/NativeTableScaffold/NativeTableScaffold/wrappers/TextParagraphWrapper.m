//
//  TextParagraphWrapper.m
//  Scout
//
//  Created by Avi Finkel on 12/28/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import "TextParagraphWrapper.h"
#import "TTTAttributedLabel.h"
#import "ScoutTextRange.h"
#import "MWLogging.h"

@interface TextParagraphWrapper ()

@property NSMutableArray *ranges;
@property NSMutableArray *openRanges;   //a queue that will contain a range that has a location but no length yet, because we haven't seen the closing tag
@property NSString *text;

@end

@implementation TextParagraphWrapper

- (id) init
{
    self = [super init];
    if (self) {
        self.text = @"";
        self.ranges = [NSMutableArray new];
        self.openRanges = [NSMutableArray new];
        self.prefix = @"";
    }
    return self;
}

- (void)appendText:(NSString *)text
{
    if (!self.text.length) {
        NSCharacterSet *whitespaceSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        while (text.length && [whitespaceSet characterIsMember:[text characterAtIndex:0]]) {
            text = [text stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
        }
    }
    self.text = [self.text stringByAppendingString:text];
}

- (void)beginRangeOfType:(TextRangeType)type
{
    //add new range to openRanges array
    ScoutTextRange *newRange = [[ScoutTextRange alloc] init];
    newRange.type = type;
    newRange.range = NSMakeRange(self.text.length, 0);
    [self.openRanges addObject:newRange];
}

- (void)endRangeOfType:(TextRangeType)type
{
    for (int i = 0; i < [self.openRanges count]; i++) {
        ScoutTextRange *currentRange = [self.openRanges objectAtIndex:i];
        if (currentRange.type == type) {
            currentRange.range = NSMakeRange(currentRange.range.location, self.text.length - currentRange.range.location);
            //add this range to ranges and remove from openRanges
            [self.ranges addObject:currentRange];
            [self.openRanges removeObjectAtIndex:i];
            break;
        }
    }
}

- (void)addUrlToLastLinkRange:(NSURL *)url
{
    //get last link range and url - just to be safe I'm going backwards but it is pretty much impossible to have nested links!
    for (int i = (int)[self.openRanges count] -1; i >=0; i--) {
        ScoutTextRange *currentRange = [self.openRanges objectAtIndex:i];
        if (currentRange.type == LINK) {
            currentRange.linkUrl = url;
            break;
        }
    }
}

//- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc
- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc pendingViewCounter:(int *)pendingViewCounter onPendingViewsResolved:(void (^)())viewsResolved;
{
    NSString *finalText = self.trimmedText;
    if (!finalText.length) {
        // Don't render empty paragraphs
        return @[];
    }

    // End all open ranges - need to go backwards so array doesn't get wacky when the ranges are removed
    for (int i = (int)[self.openRanges count] - 1; i >= 0; i--) {
        ScoutTextRange* range = (ScoutTextRange*)[self.openRanges objectAtIndex:i];
        [self endRangeOfType:range.type];
    }
    
    TTTAttributedLabel *paragraph = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(0,0,0,0)];
    UIColor *linkColor = [UIColor colorWithRed:7/255.0 green:137/255.0 blue:231/55.0 alpha:1];
    paragraph.linkAttributes = @{NSForegroundColorAttributeName: linkColor, NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)};
    paragraph.numberOfLines = 0;
    finalText = [self.prefix stringByAppendingString:finalText];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:finalText];
    if (viewDesc.plainFont) {
        [attrString addAttribute:NSFontAttributeName value:viewDesc.plainFont range:NSMakeRange(0, finalText.length)];
    }
    for (ScoutTextRange *range in self.ranges) {
        //skip ranges that are 0 length
        if (range.range.length == 0) {
            continue;
        }

        NSString *attributeName;
        id value = nil;
        if ((range.type == BOLD || range.type >= HEADER1) && viewDesc.boldFont) {
            attributeName = NSFontAttributeName;
            double multiplier = [self getFontMultiplier:range.type];
            CGFloat fontSize = viewDesc.boldFont.pointSize * multiplier;
            value = [viewDesc.boldFont fontWithSize:fontSize];
        }
        else if (range.type == ITALIC && viewDesc.italicFont) {
            attributeName = NSFontAttributeName;
            value = viewDesc.italicFont;
        }
        else if (range.type == LINK && range.linkUrl) {
            attributeName = NSLinkAttributeName;
            value = range.linkUrl;
        }
        
        if (value) {
            
            NSUInteger location = range.range.location + self.prefix.length - [self leadingSpace]; //adjust location to account for removed leading spaces
            
            if (location <= attrString.length) { //else this range is for white space that has been trimmed so IGNORE it
                NSUInteger length = range.range.length;
                //check that range doesn't overflow string
                if (location + length > attrString.length) {
                    length = attrString.length - location;
                }
                
                //MWLogDebug(@"Applying value %@ to range (loc: %d, len: %d)", value, location, length);
                [attrString addAttribute:attributeName value:value range:NSMakeRange(location, length)];
            }
            
        }
    }
    [attrString addAttribute:NSForegroundColorAttributeName value:viewDesc.foregroundColor range:NSMakeRange(0, finalText.length)];
    paragraph.text = [attrString copy];
    CGSize attributedStringSize = [TTTAttributedLabel sizeThatFitsAttributedString:attrString withConstraints:CGSizeMake(viewDesc.width, CGFLOAT_MAX) limitedToNumberOfLines:INT_MAX];
    
    CGRect textRect = CGRectMake(0, 0, attributedStringSize.width, attributedStringSize.height);
    
    CGRect rectIntegral = CGRectIntegral(textRect);  // Rounds height up, necessary to avoid clipping.
    
    @try {
        paragraph.frame = rectIntegral;
    }
    @catch(NSException* exception){
        //do nothing - this wrapper's frame will just be 0,0,0,0 and it wont show up
    }
    
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;  // Do not allow the ellipses.
    paragraph.numberOfLines = 0;

    if (viewsResolved) {
        viewsResolved();
    }

    return @[paragraph];
}

- (double)getFontMultiplier:(TextRangeType)type
{
    if(type == HEADER1){
        return 2.0;
    }
    else if(type == HEADER2){
        return 1.5;
    }
    else if(type == HEADER3){
        return 1.17;
    }
    //no HEADER4 because it's just 1
    else if(type == HEADER5){
        return .83;
    }
    else if(type == HEADER6){
        return .67;
    }
    else{
        return 1;
    }
}

- (NSString *)trimmedText
{
    return [self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSUInteger)leadingSpace
{
    NSUInteger i = 0;
    while ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:[self.text characterAtIndex:i]]) {
        i++;
    }
    return i;
}

@end
