//
//  HorizontalRuleWrapper.h
//  Scout
//
//  Created by Don Clore on 7/27/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//
#ifndef Scout_HorizontalRuleWrapper_h
#define Scout_HorizontalRuleWrapper_h

#import "ScoutDataWrapper.h"

@interface HorizontalRuleWrapper : ScoutDataWrapper
@end

#endif
