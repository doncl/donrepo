//
//  NestingWrapper.m
//  Scout
//
//  Created by Avi Finkel on 1/29/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "NestingWrapper.h"
#import "NSArray+BlocksKit.h"
#import "UIView+StackSubviews.h"

@implementation NestingWrapper

- (id) init
{
    self = [super init];
    if (self) {
        self.open = YES;
        self.nestedWrappers = [NSMutableArray new];
    }
    return self;
}

-(UIView *)outerView
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)] userInfo:nil];
}

-(CGFloat)sidePadding
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)] userInfo:nil];
}

- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc pendingViewCounter:(int *)pendingViewCounter onPendingViewsResolved:(void (^)())viewsResolved
{
    UIView *outer = self.outerView;
    
    ViewDescriptor *paddedViewDesc = [viewDesc descriptorByApplyingSidePadding:self.sidePadding];
    NSMutableArray *subviews = [NSMutableArray new];
    for (ScoutDataWrapper *wrapper in self.nestedWrappers) {
        [subviews addObjectsFromArray:[wrapper renderToViewsWithViewDescriptor:paddedViewDesc pendingViewCounter:pendingViewCounter onPendingViewsResolved:^{
            //update view with updated views
            [self buildView:outer withSubviews:subviews viewDesc:viewDesc];
            viewsResolved();
        }]];
    }
    
    [self buildView:outer withSubviews:subviews viewDesc:viewDesc];
    
    return @[outer];
}

- (void)buildView:(UIView*)outer withSubviews:(NSArray*)subviews viewDesc:(ViewDescriptor*)viewDesc
{
    CGFloat height;
    [outer removeSubviews:subviews];
    [outer addSubviews:subviews];
    [outer stackSubviewsWithSidePadding:self.sidePadding withVerticalPadding:viewDesc.verticalPadding exceptFor:nil producingHeight:&height];
    outer.frame = CGRectMake(0, 0, viewDesc.width, height);
}



@end
