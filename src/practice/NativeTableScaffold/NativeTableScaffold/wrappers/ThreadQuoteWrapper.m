//
//  ThreadQuoteWrapper.m
//  Scout
//
//  Created by Avi Finkel on 1/29/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "ThreadQuoteWrapper.h"

@implementation ThreadQuoteWrapper

- (UIView *)outerView
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor colorWithWhite:215.0/255.0 alpha:0.8];
    return view;
}

- (CGFloat)sidePadding
{
    return 5.0;
}

@end
