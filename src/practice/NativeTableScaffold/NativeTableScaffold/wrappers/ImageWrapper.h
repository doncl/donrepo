//
//  ImageWrapper.h
//  Scout
//
//  Created by Avi Finkel on 1/2/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "ScoutDataWrapper.h"
#import "Media.h"

@interface ImageWrapper : ScoutDataWrapper

- (id) initWithImage: (Media *)image;

@end
