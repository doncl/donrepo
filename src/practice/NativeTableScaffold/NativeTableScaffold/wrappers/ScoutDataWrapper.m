//
//  ScoutDataWrapper.m
//  Scout
//
//  Created by Avi Finkel on 12/28/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import "ScoutDataWrapper.h"

@implementation ScoutDataWrapper

- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc pendingViewCounter:(int *)pendingViewCounter onPendingViewsResolved:(void (^)())viewsResolved {
    return [self renderToViewsWithViewDescriptor:viewDesc];
}

- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)] userInfo:nil];
}

@end
