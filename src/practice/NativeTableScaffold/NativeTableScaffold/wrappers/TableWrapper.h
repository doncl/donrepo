//
// Created by Don Clore on 8/7/15.
// Copyright (c) 2015 Scout. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ScoutDataWrapper.h"

@interface TableWrapper : ScoutDataWrapper
- (instancetype) initWithText:(NSString *)tableText;
@end