//
//  UILabelImageWrapper.m
//  Scout
//
//  Created by Sonu on 1/28/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "InlineImageWrapper.h"
#import "UIImageView+WebCache.h"
#import "ImageUtils.h"

@implementation InlineImageWrapper

- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc pendingViewCounter:(int *)pendingViewCounter onPendingViewsResolved:(void (^)())viewsResolved
{
    if (!self.url) {
        return @[];
    }
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, viewDesc.width, 0)];
    imageView.contentMode = UIViewContentModeScaleAspectFit & UIViewContentModeLeft;
    *pendingViewCounter += 1;
    [imageView sd_setImageWithURL:self.url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        CGSize size = [ImageUtils resizeImageWithWidth:image.size.width height:image.size.height frameWidth:viewDesc.width];
        imageView.frame = CGRectMake(0, 0, size.width, size.height);
         viewsResolved();
     }];
    return @[imageView];
}


@end
