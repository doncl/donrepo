//
//  HorizontalRuleWrapper.m
//  Scout
//
//  Created by Don Clore on 7/27/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HorizontalRuleWrapper.h"

@implementation HorizontalRuleWrapper
//- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc
- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc pendingViewCounter:(int *)pendingViewCounter onPendingViewsResolved:(void (^)())viewsResolved;
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, viewDesc.width, 1)];
    lineView.backgroundColor = viewDesc.foregroundColor;
    return @[lineView];
}
@end
