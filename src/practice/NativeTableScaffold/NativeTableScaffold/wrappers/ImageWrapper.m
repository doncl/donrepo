//
//  ImageWrapper.m
//  Scout
//
//  Created by Avi Finkel on 1/2/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "ImageWrapper.h"
#import "UIImageView+WebCache.h"
#import "NSNumber+BuildImageUrl.h"
#import "Media.h"

@interface ImageWrapper ()

@property Media *image;

@end

@implementation ImageWrapper

- (id)initWithImage:(Media *)image
{
    self = [super init];
    if (self) {
        self.image = image;
    }
    return self;
}

//- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc
- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc pendingViewCounter:(int *)pendingViewCounter onPendingViewsResolved:(void (^)())viewsResolved;
{
    if (!self.image || !self.image.height || !self.image.width) {
        return @[];
    }
    CGFloat height = (viewDesc.width * self.image.height ) / self.image.width;
    UIImageView *view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, viewDesc.width, height)];
    view.contentMode = UIViewContentModeScaleAspectFit;
    NSNumber *numberMediaId = [NSNumber numberWithInt:[self.image.mediaId intValue]];
    NSURL *imageUrl = [numberMediaId buildImageUrl];
    [view sd_setImageWithURL:imageUrl];
    if (viewsResolved) {
        viewsResolved();
    }
    return @[view];
}

@end
