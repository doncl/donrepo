//
// Created by Don Clore on 9/2/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ScoutDataWrapper.h"

@interface TableCellImageWrapper : ScoutDataWrapper
@property (nonatomic, strong) NSURL* url;

// If zero, it'll use 'natural size' of image, otherwise use attributes on cell.
@property (nonatomic, assign) CGSize attributedSize;
@end