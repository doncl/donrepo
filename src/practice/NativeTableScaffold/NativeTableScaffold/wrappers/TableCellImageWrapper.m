//
// Created by Don Clore on 9/2/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "TableCellImageWrapper.h"
#import "UIImageView+WebCache.h"
#import "ImageUtils.h"


@implementation TableCellImageWrapper


- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc
                          pendingViewCounter:(int *)pendingViewCounter
                      onPendingViewsResolved:(void (^)())viewsResolved
{
    if (!self.url) {
        return @[];
    }

    BOOL hasSizeAttrs = self.attributedSize.height > 0.0 && self.attributedSize.width > 0.0;

    UIImageView *imageView;

    // Initially, use the ViewDesc width.
    CGRect initialFrame = CGRectMake(0, 0, viewDesc.width, 0);

    // Just initialize it to something.
    CGSize adjustedSize = {self.attributedSize.width, self.attributedSize.height};

    // However, if HTML author has specified height and width on the cell, then use that,
    // but don't allow it go wider than viewdesc.
    if (hasSizeAttrs) {
        adjustedSize = [ImageUtils resizeImageWithWidth:self.attributedSize.width
                                                 height:self.attributedSize.height
                                             frameWidth:viewDesc.width];

        initialFrame.size = adjustedSize;
    }
    imageView = [[UIImageView alloc] initWithFrame:initialFrame];

    imageView.contentMode = UIViewContentModeScaleAspectFit & UIViewContentModeCenter;
    *pendingViewCounter += 1;
    [imageView sd_setImageWithURL:self.url
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,
                            NSURL *imageURL) {

        // If HTML author specified no size, then we use the 'natural size' of the image, but
        // adjust it for the viewdesc width.
        if (!hasSizeAttrs) {
            CGSize size = [ImageUtils resizeImageWithWidth:image.size.width
                                                    height:image.size.height
                                                frameWidth:viewDesc.width];
            imageView.frame = CGRectMake(0, 0, size.width, size.height);
        } else {
            // Otherwise, use the specified size, again, remembering that it's limited to width
            // of viewdesc.
            UIImage *resizedImage = [ImageUtils imageWithImage:image
                                                  scaledToSize:adjustedSize];
            imageView.image = resizedImage;
        }
        viewsResolved();
    }];
    return @[imageView];
}
@end