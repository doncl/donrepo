//
// Created by Don Clore on 8/7/15.
// Copyright (c) 2015 Scout. All rights reserved.
//
#import "TableWrapper.h"
#import "MWLogging.h"
#import "NativeTableModel.h"
#import "TableProperties.h"
#import "NativeTableViewController.h"
#import "ViewDescriptor.h"

@interface TableWrapper()
@property (strong, nonatomic) NativeTableModel *model;
@property (assign, nonatomic) BOOL succesfulParse;
@end

@implementation TableWrapper
- (instancetype)initWithText:(NSString *)tableText
{
    self.succesfulParse = NO;

    self = [super init];
    if (self) {
        self.model = [NativeTableModel new];
        NSError *error;
        self.succesfulParse = [self.model buildFromText:tableText error:&error];
        if (!self.succesfulParse){
            MWLogDebug(@"Failed to build NativeTableModel from html table text");
        }
    }
    return self;
}

- (NSArray *)renderToViewsWithViewDescriptor:(ViewDescriptor *)viewDesc
                          pendingViewCounter:(int *)pendingViewCounter
                      onPendingViewsResolved:(void (^)())viewsResolved
{
    if (!self.succesfulParse) {
        return @[];  // Need to swallow the text.
    }
    TableProperties *props = [[TableProperties alloc] init];

    props.fixedColumns = 2;
    props.headerColor = [UIColor colorWithRed:0.271 green:0.271 blue:0.271 alpha:1];
    props.headerFontName = @"HelveticaNeue-Bold";
    props.headerFontSize = 12.0;
    props.dataCellFontName = @"HelveticaNeue";
    props.dataCellFontSize = 10.0;
    props.oddRowColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    props.evenRowColor = [UIColor colorWithRed:0.976 green:0.976 blue:0.976 alpha:1];
    props.dataCellTextColor = [UIColor colorWithRed:0.271 green:0.271 blue:0.271 alpha:1];
    props.headerTextColor = [UIColor colorWithRed:0.976 green:0.976 blue:0.976 alpha:1];
    props.borderColor = [UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:223.0/255.0
                                        alpha:1.0];
    props.borderWidth = 0.5f;
    props.tableType = HtmlCells;

    NativeTableViewController *tvc =
        [[NativeTableViewController alloc] initWithLoadMode:CreateView
                                                      width:viewDesc.width
                                            tableProperties:props
                                                      model:self.model];

    return @[tvc.view];
}
@end