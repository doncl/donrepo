//
//  NestingWrapper.h
//  Scout
//
//  Created by Avi Finkel on 1/29/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "ScoutDataWrapper.h"

@interface NestingWrapper : ScoutDataWrapper

@property BOOL open;
@property NSMutableArray *nestedWrappers;

@property (readonly) UIView *outerView;
@property (readonly) CGFloat sidePadding;

@end
