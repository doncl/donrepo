//
//  ListParagraphWrapper.h
//  Scout
//
//  Created by Avi Finkel on 1/5/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "TextParagraphWrapper.h"

@interface ListParagraphWrapper : TextParagraphWrapper

@property BOOL ordered;

- (void)beginNewListItem;

@end
