//
//  NSString+URLParsing.h
//  Scout
//
//  Created by Sonu on 1/28/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URLParsing)

- (NSURL *)parseUrlFromTagAfterString:(NSString *)prefix;
- (NSURL *)parseUrlFromTagAfterString:(NSString *)string forSubdomain:(NSString *)subdomain;
- (BOOL)isScoutURL;
- (NSString *)stringByDecodingURLFormat;
- (NSString *)stringByRemovingTLD;
- (long)getTweetId;
- (NSString*)getYoutubeId;
- (NSString*)getVideoId;
- (BOOL)isPlayerVideoLink;
- (BOOL)isStoryLink;
- (NSString *)getScoutStoryId;
- (NSString *)getBoxxspringVideoId;

@end
