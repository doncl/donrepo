//
// Created by Don Clore on 8/2/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CellProperties.h"

@class TableDimensions;

typedef NS_ENUM(NSInteger, NativeTableType) {
    SimpleText,
    HtmlCells,
};

@interface TableProperties : CellProperties
@property (nonatomic, assign) BOOL hasHeader;

// Typically, the first, or the first two columns are fixed, and don't horizontally scroll.
@property (nonatomic, assign) NSUInteger fixedColumns;
@property (nonatomic, assign) NativeTableType tableType;

// These are overall dimensions; they have to be carved up for the two layout objects.
@property (nonatomic, strong) TableDimensions *dimensions;
@end