//
// Created by Don Clore on 7/30/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class CellInfo;
@class CellProperties;

typedef NS_ENUM(NSInteger, cellType) {columnHeading = 1, dataOdd = 2, dataEven = 4};

@interface NativeTableCell : UICollectionViewCell

// properties:
@property (strong, nonatomic) IBOutlet UILabel *label;

// methods:
- (void)setCellText:(NSString *)text;
- (void)setupConstraints;
- (void)setUpCellInfo:(cellType)cellType;
+ (CellInfo *)getCellInfoForTypeOfCell:(cellType)cellType;
+ (void)initializeCellProperties:(CellProperties *)properties;
@end