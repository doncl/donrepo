//
//  AppDelegate.m
//  NativeTableScaffold
//
//  Created by Don Clore on 7/29/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "AppDelegate.h"
#import "NativeTableViewController.h"
#import "NativeTableModel.h"
#import "TableProperties.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NativeTableModel *model = [[NativeTableModel alloc] init];
    NSError *error;
    //[model buildFromText:joshTable1 error:&error];
    [model buildFromText:motherOfAllTables error:&error];
    //[model buildFromText:smallerTable error:&error];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    TableProperties *props = [self setupTableProperties];

    props.tableType = HtmlCells;
    //props.tableType = SimpleText;

    self.window.rootViewController =
        [[NativeTableViewController alloc] initWithLoadMode:DefaultView
                                                      width:self.window.frame.size.width
                                            tableProperties:props
                                                      model:model];

    [self.window makeKeyAndVisible];

    return YES;
}

-(TableProperties *)setupTableProperties
{
    TableProperties *props = [TableProperties new];
    props.tableType = SimpleText;
    props.headerColor = [UIColor colorWithRed:0.271 green:0.271 blue:0.271 alpha:1];
    props.fixedColumns = 2;
    props.headerFontName = @"HelveticaNeue-Bold";
    props.headerFontSize = 12.0;
    props.dataCellFontName = @"HelveticaNeue";
    props.dataCellFontSize = 10.0;
    props.oddRowColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    props.evenRowColor = [UIColor colorWithRed:0.976 green:0.976 blue:0.976 alpha:1];
    props.dataCellTextColor = [UIColor colorWithRed:0.271 green:0.271 blue:0.271 alpha:1];
    props.headerTextColor = [UIColor colorWithRed:0.976 green:0.976 blue:0.976 alpha:1];
    props.borderColor = [UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:223.0/255.0
                                     alpha:1.0];
    props.borderWidth = 0.5f;
    props.maxCellWidthAsPercentageOfViewWidth = 0.40; // 40 percent maximum.
    props.cellHorizontalPadding = 15.0;
    props.cellVerticalPadding = 15.0;
    return props;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

NSString *joshTable1 =
        @"<table width=\"100%\" class=\"responsive-table\"> <thead> <tr> <td>School</td>"
                "<td>Conference</td> <td>Rank</td> <td>Commits</td> <td>Points</td> <td>AVG</td>"
                "</tr> </thead> <tbody> <tr> <td><img title=\"Ohio State logo\" height=\"40\" width=\"40\" "
                "class=\"media-element file-default\" "
                "src=\"http://img.scout.com/sites/default/files/ohiostate.png\" alt=\"\" /></td> "
                "<td>Big Ten</td> <td>1</td> <td>19</td> <td>3532</td> <td>3.89</td> </tr>"
                "<tr> <td><img title=\"LSU logo\" height=\"40\" width=\"40\" "
                "class=\"media-element file-default\" "
                "src=\"http://img.scout.com/sites/default/files/louisianastate.png\" alt=\"\" /></td> "
                "<td>SEC</td> <td>2</td> <td>19</td> <td>3430</td> <td>3.79</td> </tr> <tr> "
                "<td><img title=\"Alabama Crimson Tide PNG\" height=\"40\" width=\"40\" "
                "class=\"media-element file-default\" "
                "src=\"http://img.scout.com/sites/default/files/sites/alabamact.png\" alt=\"\" /></td>"
                "<td>SEC</td> <td>3</td> <td>19</td> <td>3051</td> <td>3.58</td> </tr> "
                "<tr> <td><img title=\"Michigan State logo\" height=\"40\" width=\"40\" "
                "class=\"media-element file-default\" "
                "src=\"http://img.scout.com/sites/default/files/sites/michiganstate.png\" alt=\"\" /></td>"
                "<td>Big Ten</td> <td>4</td> <td>20</td> <td>2892</td> <td>3.45</td> </tr> "
                "<tr> <td><img title=\"Mississippi State logo\" height=\"40\" width=\"40\" "
                "class=\"media-element file-default\" "
                "src=\"http://img.scout.com/sites/default/files/sites/mississippistate.png\" alt=\"\" /></td>"
                "<td>SEC</td> <td>5</td> <td>19</td> <td>2856</td> <td>3.58</td> </tr> "
                "<tr> <td><img title=\"Florida State logo\" height=\"40\" width=\"40\" "
                "class=\"media-element file-default\" "
                "src=\"http://img.scout.com/sites/default/files/sites/floridastate_0.png\" alt=\"\" /></td> "
                "<td>ACC</td> <td>6</td> <td>18</td> <td>2713</td> <td>3.56</td> </tr> "
                "<tr> <td><img title=\"Penn State logo\" height=\"40\" width=\"40\" "
                "class=\"media-element file-default\" "
                "src=\"http://img.scout.com/sites/default/files/sites/pennstate.png\" alt=\"\" /></td>"
                "<td>Big Ten</td> <td>7</td> <td>18</td> <td>2582</td> <td>3.50</td> </tr>"
                "<tr> <td><img title=\"Georgia logo\" height=\"40\" width=\"40\" "
                "class=\"media-element file-default\" "
                "src=\"http://img.scout.com/sites/default/files/sites/georgia-logo_0.png\" alt=\"\" /></td>"
                "<td>SEC</td> <td>8</td> <td>15</td> <td>2455</td> <td>3.67</td> </tr> <tr> "
                "<td><img title=\"Michigan logo\" height=\"40\" width=\"40\" "
                "class=\"media-element file-default\" "
                "src=\"http://img.scout.com/sites/default/files/sites/michigan2.png\" alt=\"\" />"
                "</td> <td>Big Ten</td> <td>9</td> <td>22</td> <td>2368</td> <td>3.14</td> </tr> "
                "<tr> <td><img title=\"Oregon logo\" height=\"40\" width=\"40\" "
                "class=\"media-element file-default\" "
                "src=\"http://img.scout.com/sites/default/files/sites/oregon1_0.png\" alt=\"\" /></td>"
                "<td>Pac-12</td> <td>10</td> <td>14</td> <td>2364</td> <td>3.64</td> </tr> </tbody> </table> ";


NSString *motherOfAllTables =
    @"<table width=\"100%\" height=\"2260\" class=\"responsive-table\"><thead><tr><td>RankÂ </td>"
        "<td>Team (Record) City, ST</td> </tr></thead><tbody><tr><td> <p>1.</p> <p>"
        "<img src=\"http://media.scout.com/Media/Image/96/963471.jpg\" "
        "alt=\"De La Salle Logo\" width=\"197\" height=\"132\" /></p> </td> <td> <p><strong>De La "
        "Salle (0-0) Concord, Calif.Â </strong><br /><br />De La Salle begins the season No. 1, and "
        "it has the top defenisve tackle in the West in Boss Tagaloa anchoring the line of scrimmage"
        " as it looks to continuing its winning ways.</p> <p><br /><strong>Next:</strong>Â 8/29 at "
        "No. 46 Trinity (0-0)</p> </td> </tr><tr><td> <p>2.</p> <p><img src=\"http://imgix.scout.com/"
        "96/963470.jpg\" width=\"131\" height=\"113\" /></p> </td> <td> <p><strong>Bishop Gorman (0-0)"
        " Las Vegas, Nev.</strong><br /><br /></p> <p>New Texas A&amp;M commit Tate Martell, a "
        "five-star quarterback and the No. 2 quarterback in the nation, leads Bishop Gorman on a trip"
        " south as it tries to extend a 24-game winning streak.<br /><br /><strong>Next</strong>:Â "
        "8/29at Chandler (1-0)</p> </td> </tr><tr><td> <p>3.</p> <p>"
        "<img src=\"http://images.maxpreps.com.edgesuite.net/Mascot/AAAAAAAAAAAAAAAAAAAAAA/vXFA18nfj"
        "UGPtaJL_8ql-g/1,210,89/1380492702_american_heritage_patriots.gif\" alt=\"Patriots mascot "
        "photo.\" width=\"149\" height=\"115\" /></p> </td> <td><strong>American Heritage (1-0) Plant"
        "ation, Fla.</strong><br /><br />Defensive end Brian Burns, who is considering the likes of "
        "Florida, Florida State, LSU and Georgia, led a spirited defensive effort to beat highly-pub"
        "licized IMG in the season opener.<br /><br />Last:Â Beat IMG Academy, 19-7<br /><strong>Nex"
        "t</strong>: 8/29 at Stephenson (2-0)</td> </tr><tr><td> <p>4.</p> <p><img src=\"http://medi"
        "a.scout.com/Media/Image/96/963516.jpg\" alt=\"DeSoto Logo\" width=\"147\" height=\"112\" />"
        "</p> </td> <td> <p><strong>DeSoto (0-0) DeSoto, Texas</strong></p> <p>Ohio State quarterbac"
        "k commit Tristen Wallace and four-star class of 2017 receiver KD Nixon areÂ part of the eno"
        "rmous offensive weapons for DeSoto.<br /><br /><b>Next</b>:Â vs. Martin (0-0)</p> </td> </t"
        "r><tr><td> <p>5.</p> <p><img src=\"http://imgix.scout.com/96/963485.jpg\" width=\"145\" hei"
        "ght=\"107\" /></p> </td> <td> <p><strong>St. Thomas Aquinas (0-0) Fort Lauderdale, Fla.<br "
        "/><br /></strong>Receiver Sam Bruce, who is committed to Miami, had a 44-yard touchdown rec"
        "eption to lead St. Thomas Aquinas to a season-opening win.</p> <p><br />Last: DefÂ Dillard "
        "24-0<br /><strong>Next:</strong>Â 8/28Â Booker T. Washington (1-0)</p> </td> </tr><tr><td> "
        "<p>6.</p> <p><img src=\"http://media.scout.com/Media/Image/96/963484.jpg\" alt=\"Allen Logo"
        "\" /></p> </td> <td> <p><strong>Allen (0-0), Allen, Texas</strong></p> <p>Scout's defending"
        " national champs are also home to the No. 1 offensive tackle in the midwest, Greg Little.</"
        "p> <p><br /><strong>Next:</strong>Â 8/28 vs. No. 42 Guyer (0-0)</p> </td> </tr><tr><td> <p>"
        "7.</p> <p><img src=\"http://imgix.scout.com/137/1370846.jpg\" /></p> </td> <td> <p><strong>"
        "St. Peter's Prep (0-0), Jersey City, N.J.</strong></p> <p><strong><br /></strong>The defend"
        "ing New Jersey non-public Group IV champs are re-tooling, but have Boston College commit K."
        "J. Gray and two-offer 2017 quarterback Jonathan Lewis leading the offense.</p> <p><br /><st"
        "rong>Next:Â </strong>9/3 vs. St. Frances Academy (0-0)</p> </td> </tr><tr><td> <p>8.</p> <p"
        "><img src=\"http://imgix.scout.com/109/1095838.jpg\" /></p> </td> <td><strong>Manvel (0-0) "
        "Manvel, Texas</strong><br /><br />A loaded squad includes Scout 100 member Deontay Anderson"
        ", Texas commit Reggie Hemphill, TCU commit D'Eriq King, Florida commit Kyle Trask, Ole Miss"
        " commit D'Vaughn Pennamon and four-star Jordan Carmouche.<br /><br /><strong>Next:</strong>"
        "Â 8/28 vs. Westfield (0-0)</td> </tr><tr><td> <p>9.</p> <p><img src=\"http://imgix.scout.co"
        "m/145/1452397.jpg\" /></p> </td> <td>Centennial (0-0), Corona, Calif.<br /><br />Scout 100 "
        "receiver Javon McKinley and a trio of defensive backs either committed to Pac-12 or strong "
        "Pac-12 interest.<br /><br /><strong>Next:</strong>Â Â 8/28 vs. East (0-0)</td> </tr><tr><td"
        "> <p>10.Â </p> <p><img src=\"http://imgix.scout.com/137/1370843.jpg\" /></p> </td> <td> <p>"
        "<strong>Chandler (1-0) Chandler, Ariz.<br /></strong><br />Oregon State commit Mason Moran "
        "was 15 of 15 for 195 yards and two touchdowns, and he also ran for a pair of scores in the "
        "season opener.</p> <p><br /><strong>Last</strong><strong>:Â </strong>Def. Pinnacle 56-10<br"
        " /><strong>Next:</strong>Â vs. No. 2 Bishop Gorman 0-0</p> </td> </tr><tr><td> <p>11.</p> <"
        "p><img src=\"http://imgix.scout.com/121/1218329.jpg\" /></p> </td> <td> <p><strong>Paramus "
        "Catholic (0-0), Paramus, N.J.</strong></p> <p><br />The Paladins have a plethora of talent,"
        " including uncommitted receiver Donald Steward and the nation's No. 1 player, defensive tac"
        "kle Rashan Gary.</p> <p><br /><strong>Next:Â </strong>9/4 vs Eastern Christian (0-0)</p> </"
        "td> </tr><tr><td> <p>12.</p> <p><img src=\"http://media.scout.com/Media/Image/111/1118083.j"
        "pg\" alt=\"Saint John Bosco Logo\" /></p> </td> <td> <p><strong>St. John Bosco (0-0) Bellfl"
        "ower, CAÂ <br /><br /></strong>Washington running back commit Sean McGrew leads the offense"
        ", and will run behind five-star class of 2017 offensive tackle Wyatta Davis.</p> <p><br /><"
        "strong>Next:</strong>Â 8/28 at La Mirada (0-0)</p> </td> </tr><tr><td> <p>13.</p> <p><img s"
        "rc=\"http://images.maxpreps.com.edgesuite.net/TeamPhoto/iXD_6zTCg02fl3-U2V_9Nw/nzPce798KEew"
        "yO2JiSnPaA/3,140/1438285499_ascenders_img_academy_boys_varsity_football_fall_15-16.jpg\" al"
        "t=\"IMG Academy Ascenders Boys Varsity Football Fall 15-16 team photo.\" /></p> </td> <td> "
        "<p><strong>IMG Academy (0-1) Bradenton, Fla.</strong></p> <p>IMG is home to more prospects "
        "than any other program in the nation, but got off to a rough start in an unexpected season-"
        "opening loss.</p> <p><strong>Last:</strong>Â Lost to No. 3 American Heritage 19-7</p> <p><s"
        "trong>Next:</strong>Â 8/30 vs. Miramar (1-0)</p> </td> </tr><tr><td> <p>14.Â </p> <p><img s"
        "rc=\"http://imgix.scout.com/96/963519.jpg\" /></p> </td> <td> <p><strong>Archbishop Wood (0"
        "-0) Warminster, Pa.<br /></strong></p> <p>Rutgers quarterback commit Anthony Russo and clas"
        "s of 2017 receiver Mark Webb, who already has a nice offer list, lead the Vikings offense.<"
        "br /></p> <p><strong>Next:</strong>Â 9/5 vs. Allentown, N.J. (0-0)</p> </td> </tr><tr><td> "
        "<p>15.Â </p> <p><img src=\"http://media.scout.com/Media/Image/98/981995.jpg\" alt=\"Miami C"
        "entral Logo\" /></p> </td> <td> <p><strong>Miami Central (1-0), Miami, Fla.</strong></p> <p"
        ">A pair of Florida State commits, Jamel Cook and Keir Thomas, lead Miami Central while Darn"
        "ell Salomon, whose list includes Alabama, Miami and LSU, is also a big-time offensive threa"
        "t.</p> <p><strong>Last:Â </strong>Def. Royal Palm 48-6</p> <p><strong>Next:Â </strong>8/29 "
        "at No. 32 DeMatha (0-0)</p> </td> </tr><tr><td> <p>16.</p> <p><img src=\"https://encrypted-"
        "tbn0.gstatic.com/images?q=tbn:ANd9GcSwGcfPslvthG4h6hi0AQvS3LpW4QIupralGxski98CD6zBsY0U26F_U"
        "A\" /></p> </td> <td> <p><strong>Serra (0-0)Â Gardena, CA</strong></p> <p>Five-star defensi"
        "ve end Oluwole Betiku, who is committed to UCLA, gets to team up with USC commit C.J. Polla"
        "rd, a defensive back. Arizona commit Khalil Tate is the quarterback.</p> <p><strong>Next:Â "
        "</strong>8/28 vs. Lakewood (0-0)</p> </td> </tr><tr><td> <p>17.</p> <p><img src=\"http://im"
        "gix.scout.com/110/1103045.jpg\" /></p> </td> <td><strong>Cedar Hill (0-0) Cedar Hill, Texas"
        "</strong><br /><br />A pair of SMU defensive back commits -- Michael Clark and Eric Sutton "
        "-- patrol the secondary while 2017 four-stars Avery Davis (QB) and Camron Buckley (WR) shou"
        "ld make the offense go.<br /><br /><strong>Next:</strong>Â 8/28 at South Oak Cliff (0-0)</t"
        "d> </tr><tr><td> <p>18.</p> <p><img src=\"https://encrypted-tbn2.gstatic.com/images?q=tbn:A"
        "Nd9GcQYI4CIMmQPQxuIwijFxiwVEtbx27KKX-hP-No6U2tAcxlT5ByO\" alt=\"Image result for archer hig"
        "h football image\" width=\"84\" height=\"59\" /></p> </td> <td> <p>Archer (1-0) Lawrencevil"
        "le, Ga.<br /><br />The headliners for the Tigers are Ohio State safety commit Isaiah Pryor "
        "(2017) and receiver Kyle Davis, who has Auburn, Georgia and LSU on his list.</p> <p><br /><"
        "strong>Last</strong>: Def. North Paulding 49-21</p> <p><strong>Next:</strong>Â 8/29 vs. Pea"
        "chtree Ridge (0-1)</p> </td> </tr><tr><td> <p>19.</p> <p><img src=\"http://imgix.scout.com/"
        "122/1228311.jpg\" /></p> </td> <td><strong>Bingham (1-0) South Jordan, Utah</strong><br /><"
        "br />The top team in Utah in 2014 holds the same position in 2015, and looming is a Sept. 4"
        " showdown with No. 2 Bishop Gorman.<br /><br /><strong>Last</strong><strong>:</strong>Â Def"
        ". Westlake 38-0<br /><strong>Next:</strong>Â 8/28 at Herriman (0-1)</td> </tr><tr><td> <p>2"
        "0.</p> <p><img src=\"http://imgix.scout.com/145/1452402.jpg\" /></p> </td> <td><strong>La S"
        "alle (0-0) Cincinnati, Ohio</strong><br /><br />La Salle has a stacked backfield with runni"
        "ng back commit Jeremy Larkin and class of 2017 running back Jarell White, who holds ofrers."
        "<br /><br /><br /><strong>Next:</strong>Â 8/28 vs. Carmel, Ind. (-0)</td> </tr><tr><td> <p>"
        "21.</p> <p><img src=\"http://www.highschoolreplicas.com/assets/images/Schutt Mini/New J"
        "ersey/schutt-nj-don-bosco-prep-ironment-current-visor.JPG\" width=\"114\" height=\"109\" />"
        "</p> </td> <td> <p>Don Bosco Prep (0-0) Ramsey, N.J.</p> <p>The Ironmen have Iowa defenive "
        "end commit Brandon Simon, Maryland defenisve back commit Tyrone Hill and class of 2017 offe"
        "nsive lineman Liam Fornadel playing for legendary coach Greg Toal.</p> <p><strong>Next:Â </"
        "strong>9/5 vs. No. 23 St. Joseph Regional (0-0)</p> </td> </tr><tr><td> <p>22.</p> <p><img "
        "src=\"http://imgix.scout.com/121/1218310.jpg\" /></p> </td> <td> <p><strong>Folsom (0-0) Fo"
        "lsom, Calif.</strong><br /><br />Folsom's offensive cornerstone is offensive tackle Jonah W"
        "illiams, the No. 1 offensive tackle in the West and an Alabama commit.<br /><br /><strong>N"
        "ext:</strong>Â 8/28 vs. Clayton Valley (0-0)</p> </td> </tr><tr><td> <p>23.</p> <p><img src"
        "=\"http://saintjosephregional.org/athletics/Final_SJR_Artwork CROP.JPG\" width=\"118\" he"
        "ight=\"91\" /></p> </td> <td> <p><strong>St. Joseph Regional (0-0), Montvale, N.J.</strong>"
        "</p> <p><strong></strong></p> <p>A well-coached team will have plenty of work to do along t"
        "he ultra-competitive North Jersey parochial scene, but 2017 athlete Louis Acceus, who holds"
        " a few offers, will have a big say in how the season goes.</p> <p><b></b></p> <p><b>Next:Â "
        "</b>9/5 at No. 21 Don Bosco Prep (0-0)</p> </td> </tr><tr><td> <p>24.</p> <p><img src=\"htt"
        "p://www.mghelmets.com/high school helmets/archbishop-rummell-la.gif\" width=\"143\" hei"
        "ght=\"107\" /></p> </td> <td> <p><strong>Archbishop Rummel (0-0) Metairie, La.</strong></p>"
        " <p><strong></strong></p> <p>Arkansas commit Briston Guidry anchors the defensive line whil"
        "e cornerback Kristian Fulton, who has Florida, LSU and Texas on his list, leads the seconda"
        "ry.</p> <p><strong>Next:Â </strong>9/4 vs. University Lab (0-0)</p> </td> </tr><tr><td> <p>"
        "25.</p> <p><img src=\"https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRgx7kTOskPFfGDU"
        "rvzsK9Rt9_lJ1NZ9fKzo9SHW0WavxrBPWsD\" alt=\"Image result for mceachern football helmet logo"
        "\" /></p> </td> <td> <p><strong>McEachern (1-0) Power Springs, Ga.</strong></p> <p><strong>"
        "</strong></p> <p>Class of 2017 Georgia quarterback commit Bailey Hockman ran for one school"
        " and threw for another in the season-opening win.</p> <p><strong>Next:</strong>Â 9/4 vs. Bu"
        "ford (1-0)</p> </td> </tr><tr><td> <p>26.</p> <p><img src=\"http://imgix.scout.com/112/1129"
        "452.jpg\" /></p> </td> <td> <p><strong>Ocean Lakes (4-0) Virginia Beach, Va.Â </strong><br "
        "/><br />Ocean Lakes is looking to repeat at state champs and has the nation's No. 2 cornerb"
        "ack, Florida State commit Levonta Taylor, to roam the secondary while stud 2017 defensive t"
        "ackle Eric Crosby (double digit offers) anchors the line of scrimmage.</p> <p><br />Last:Â "
        "Def. Hermitage 9-0<br /><strong>Next:</strong>Â 8/28 vs. Salem (0-0)<strong></strong></p> <"
        "/td> </tr><tr><td> <p>27.</p> <p><img src=\"http://imgix.scout.com/96/963467.jpg\" /></p> <"
        "/td> <td> <p><strong>Katy (0-0) Katy, Texas</strong><br /><br />A pair of Northwestern comm"
        "its in Paddy Fisher sand Travis Whillock join Houston commit Collin Wilder on a stacked def"
        "ense.</p> <p><br /><strong>Next:</strong>Â 8/27 vs. Kingwood (0-0)<strong></strong></p> </t"
        "d> </tr><tr><td> <p>28.</p> <p><img src=\"http://floridahsfootball.com/wp-content/uploads/2"
        "014/08/flanaganhelmet.gif\" width=\"107\" height=\"71\" /></p> </td> <td> <p><strong>Flanag"
        "an (0-0) Pembroke Pines, Fla.</strong></p> <p><strong></strong></p> <p>Class of 2016 linema"
        "n Devin Bush Jr. is looking at Auburn, Florida State and Michigan and 2017 cornerback Stanf"
        "ord Samuels has Alabama, Auburn, Georgia and Florida State on his list.</p> <p><strong></st"
        "rong></p> <p><strong>Next:Â </strong>Stoneman Douglas (0-0)</p> </td> </tr><tr><td> <p>29.<"
        "/p> <p><img src=\"http://www.mghelmets.com/high school helmets/bergen-catholic-nj.gif\""
        " width=\"112\" height=\"84\" /></p> </td> <td> <p><strong>Bergen Catholic (0-0) Oradell, N."
        "J.</strong></p> <p>The CrusadersÂ are pushing for a state title and much of it will hinge o"
        "n the play of the East's top quarterback, Jarrett Guarantano, who is committed to Tennessee"
        ".</p> <p><strong></strong></p> <p><strong>Next:Â </strong>9/5 vs. No. 3 American Heritage ("
        "1-0)</p> </td> </tr><tr><td> <p>30.</p> <p><img src=\"http://www.mghelmets.com/high schoo"
        "l helmets/mallard-creek-nc.gif\" width=\"127\" height=\"95\" /></p> </td> <td> <p><b>Mall"
        "ard Creek (1-0) Charlotte, N.C.</b></p> <p><b></b></p> <p>Tight endÂ Thaddeus Moss, the son"
        " of former NFL standout Randy Moss, had a 13-yard touchdown catch in the opener. Mallard Cr"
        "eek is also home to Florida State defensive end commit Josh Brown.</p> <p><b></b></p> <p><b"
        ">Next:Â </b>8/28 at Butler (1-0)</p> </td> </tr><tr><td> <p>31.</p> <p><img src=\"http://ww"
        "w.mghelmets.com/high school helmets/archbishop-moeller-oh.gif\" width=\"131\" height=\""
        "98\" /></p> </td> <td> <p><strong>Archbishop Moeller (0-0) Cincinnati, Ohio</strong></p> <p"
        ">Three Big Ten commits -- Jake Jausman (Ohio State), Ryan Smith (Indiana) and Matt Coughlin"
        " (Michigan State) -- join Pitt commit Thomas Macvittie on a loaded roster.</p> <p><strong>N"
        "ext:Â </strong>8/28 vs. Cathedral, Ind. (0-0)</p> </td> </tr><tr><td> <p>32.</p> <p><img sr"
        "c=\"http://www.mghelmets.com/high school helmets/dematha-md.gif\" width=\"119\" height="
        "\"89\" /></p> </td> <td> <p><strong>DeMatha (0-0) Hyattsville, Md.</strong></p> <p><strong>"
        "</strong></p> <p>The Stags have a tough opener but plenty of talent in Maryland commits Tin"
        "o Ellis and D.J. Turner, Penn State commit Shane Simmons and Scout 300 lineman Terrance Dav"
        "is, who already has four Power 5 official visits set.</p> <p>Next: 8/29 vs. No. 15 Miami Ce"
        "ntral (1-0)</p> </td> </tr><tr><td> <p>33.</p> <p><img src=\"https://encrypted-tbn2.gstatic"
        ".com/images?q=tbn:ANd9GcQay6IqSIibUtVm2X5FkLOsBFVYbycZjohtyW-OQrSY-bPYpO_r2w\" width=\"111\""
        """ height=\"83\" /></p> </td> <td> <p><strong>Bishop Amat (0-0) La Puente, Calif.</strong><"
        "/p> <p><strong></strong></p> <p>USC commit Trevon Sydeny and five-star receiver Tyler Vaugh"
        "ns make Bishop Amat tough to match up with from a talent standpoint, and UNLV commit Matt B"
        "rayton is big on the offensive line.</p> <p><strong></strong></p> <p><strong>Next:Â </stron"
        "g>8/28 vs. Mater Dei (0-0)</p> </td> </tr><tr><td> <p>34.</p> <p><img src=\"http://imgix.sc"
        "out.com/137/1378340.jpg\" /></p> </td> <td> <p><strong>Colquitt County (1-0) Moultrie, Ga.<"
        "/strong><br /><br />Defensive end JaQuain Blakely, a Tennessee commit, made five tackles an"
        "d had 1 1/2 sacks in the opener.<br /><br /><strong>Last</strong><strong>:</strong>Â Def. N"
        "orth Gwinnet 45-14<br /><strong>Next:</strong>Â 8/28 at Plant (0-0)<strong></strong></p> </"
        "td> </tr><tr><td> <p>35.</p> <p><img src=\"http://www.mghelmets.com/high school helmets"
        "/south-panola-ms.gif\" /></p> </td> <td> <p><strong>South Panola (1-0) Batesville, Miss.</s"
        "trong></p> <p><strong></strong></p> <p>Southern Miss commit Demetrius Market returned the o"
        "pening kick 98 yards for a touchdown to get South Panola off to a positive start.</p> <p><s"
        "trong>Last:</strong>Â Def. Clinton 35-25</p> <p><strong>Next:</strong>Â 8/28 vs. Meridian ("
        "0-1)</p> </td> </tr><tr><td> <p>36.</p> <p><img src=\"http://www.ohiohelmetproject.com/png/"
        "stXavier.png\" /></p> </td> <td> <p><strong>St. Xavier (0-0) Cincinnati, Ohio</strong></p> "
        "<p>Class of 2017 quarterback Sean Clifford, a Penn State commit, leads an offense that feat"
        "ures tight end Noah Davis (Michigan State commit) and class of 2017 four-star offensive tac"
        "kle Mat Bockhorst.</p> <p><strong>Next:Â </strong>8/28 vs. Covington Catholic (0-0)</p> </t"
        "d> </tr><tr><td> <p>37.</p> <p><img src=\"http://imgix.scout.com/145/1452405.jpg\" /></p> <"
        "/td> <td> <p><strong>Steele (0-0) Cibolo, TexasÂ Â </strong></p> <p>Steele has a pair of Te"
        "xas Tech commits on offense (Bryson Denley and Antoine Cox) and Texas A&amp;M commit Mark J"
        "ackson on defense.</p> <p><br /><strong>Next:</strong>Â 8/28 vs. Madison (0-0)</p> </td> </"
        "tr><tr><td> <p>38.</p> <p><img src=\"http://imgix.scout.com/96/963540.jpg\" /></p> </td> <t"
        "d> <p><strong>Union (0-0) Tulsa, Okla.<br/></strong><br />Union is home to running back Tyl"
        "er Adams (Navy offer) and class of 2017 four-star cornerback Tre Brown.<br /><br /><strong>"
        "Next:</strong>Â 9/4 Carroll (0-0)</p> </td> </tr><tr><td> <p>39.</p> <p><img src=\"http://i"
        "mgix.scout.com/141/1412522.jpg\" /></p> </td> <td> <p><strong>Clay-Chalkville (1-0) Pinson,"
        " Ala.</strong><br /><br />Dual-threat quarterback Ty Pigrome, who has a handful of offers, "
        "was 17 of 27 for 297 yards and ran for 59 yards.<br /><br /><strong>Last</strong><strong>:<"
        "/strong>Â Def. Blackman 38-7<br /><strong>Next:</strong>Â 8/28 at Minor (1-0)</p> </td> </t"
        "r><tr><td> <p>40.</p> <p><img src=\"http://cahelmetproject.com/wp-content/uploads/2012/04/n"
        "arbonne.png\" /></p> </td> <td> <p><strong>Narbonne (0-0) Harbor City, Calif.</strong></p> "
        "<p>Arizona commits Devaughn Cooper and Sean Riley join Nevada commits Lawson Hall and Danie"
        "l Brown on the Gauchos. Uncommitted Jamal Hicks and offensive lineman Alex Akingbulu (four-"
        "star) give them loads of talent.</p> <p>Â </p> <p><strong>Next:Â </strong>9/3 vs. Long Beac"
        "h Poly (0-0)</p> </td> </tr><tr><td> <p>41.</p> <p><img src=\"http://imgix.scout.com/145/14"
        "52403.jpg\" /></p> </td> <td> <p><strong>Lake Taylor (0-0) Norfolk, Va.</strong><br /><br /"
        ">Cornerback and Ohio State commit Wayne Davis is on one side of the ball and 2017 offensive"
        " lineman Darnell Ewell, who holds double digit offers, is on the other side.<br /><br /><st"
        "rong>Next:</strong>Â 8/28 at Heritage (0-0)</p> </td> </tr><tr><td> <p>42.</p> <p><img src="
        "\"http://imgix.scout.com/109/1095834.jpg\" /></p> </td> <td> <p><strong>Guyer (0-0) Denton,"
        " Texas<br /><br /></strong>Two of Texas' best in the 2017 class are five-star quarterback S"
        "hawn Robinson and tight end Brian Polendey. Over/under on howÂ many times they hook up for "
        "scores?<br /><br /><strong>Next:</strong>Â 8/28 vs. No. 6 Allen (0-0)</p> </td> </tr><tr><t"
        "d> <p>43.</p> <p><img src=\"http://www.mghelmets.com/high school helmets/red-bank-catho"
        "lic-nj.gif\" /></p> </td> <td> <p><strong>Red Bank Catholic (0-0) Red Bank, N.J.</strong></"
        "p> <p>Offensive tackle Liam Smith, who is committed to Duke, will also play on the defensiv"
        "e line while quarterback/safety Eddie Hahn (UConn commit) is on the mend after a knee injur"
        "y in last week's scrimmage.</p> <p><strong>Next:Â </strong>9/11 at Long Branch (0-0)</p> </"
        "td> </tr><tr><td> <p>44.</p> <p><img src=\"http://imgix.scout.com/96/963499.jpg\" /></p> </"
        "td> <td> <p><strong>Trinity (0-0) Euless, Texas.</strong><br /><br />Four-star defensive ta"
        "ckle Chris Daniels, running back Ja'Ron Wilson and athlete Tyler Natee are part of the tale"
        "nted group at Trinity.<br /><br /><strong>Next:</strong>Â 8/29 vs. No. 1 De La Salle (0-0)<"
        "/p> </td> </tr><tr><td> <p>45.Â </p> <p><img src=\"http://images.schoolinsites.com/cache/Sc"
        "hool_5797/8f30a0d6d80ab14848cc82e97df374c4.jpg\" width=\"131\" height=\"98\" /></p> </td> <"
        "td> <p><strong>Park Crossing (1-0) Montgomery, Ala.</strong></p> <p>Defensive backs Jeawon "
        "Taylor (Florida commit) and P.J. Blue (South Carolina commit) and offensive tackle Marcus \""
        """Tank\" Jenkings (2018) make Park Crossing a must-see for Scouts.</p> <p>Next: 8/28 at Lan"
        "ier (0-1)</p> </td> </tr><tr><td> <p>46.</p> <p><img src=\"http://vype.com/wp-content/uploa"
        "ds/2013/08/Hoover-Buccaneers-logo.jpg\" width=\"142\" height=\"71\" /></p> </td> <td> <p><s"
        "trong>Hoover (1-0) Hoover, Ala.</strong></p> <p><strong></strong></p> <p>Linebacker Jeremia"
        "n Moon, who is committed to Florida, will try to lead a Hoover squad that lost loads of tal"
        "ent from the 2014 team.</p> <p><strong>Last:Â </strong>Def. Oakland 24-6</p> <p>Next: 8/28 "
        "vs. Manatee (1-0)</p> </td> </tr><tr><td> <p>47.</p> <p><img src=\"http://imgix.scout.com/1"
        "11/1115735.jpg\" /></p> </td> <td> <p><strong>Ben Davis (1-0) Indianapolis, Ind.</strong></"
        "p> <p>Michigan running back Chris Evans got his season off to a banging start by rushing fo"
        "ur times for 112 yards and three touchdowns ...in the first half.</p> <p><strong>Last:Â </s"
        "trong>Def. Arsenal Tech 63-0</p> <p><strong>Next:Â </strong>8/28 vs.<strong>Â </strong>Avon"
        " (2-0)</p> </td> </tr><tr><td> <p>48.</p> <p><img src=\"http://www.michiganhelmetproject.or"
        "g/images/helmets/2385_l.gif\" width=\"75\" height=\"57\" /></p> </td> <td> <p><strong>St. M"
        "ary's (0-0) Orchard Lake (Mich.)</strong></p> <p>A loaded 2017 class has four-star recruits"
        " K.J. Hamler and James Ross as well as offer-holding Dwayne Chapman and Ricahrd Bowens. Nor"
        "thwestern commit Cameron Kolwich anchor the offensive line.</p> <p><strong>Next:</strong>Â "
        "8/29 vs. Loyola (0-0)</p> </td> </tr><tr><td> <p>49.</p> <p><img src=\"http://s3.amazonaws."
        "com/vnn-aws-sites/3821/files/2014/09/Westfield-Helmet-Logo.jpg\" width=\"82\" height=\"64\""
        " /></p> </td> <td> <p><strong>Westfield (0-0) Houston, Texas</strong></p> <p><strong></stro"
        "ng></p> <p>Westfield has Arizona State quarterback commit Dillono Sterling-Cole to throw to"
        " Scout 100 receiver Tyrie Cleveland. Houston commit Ed Oliver plays defensive tackle.</p> <"
        "p><strong>Next:</strong>Â vs. No. 8 Manvel (0-0)</p> </td> </tr><tr><td> <p>50.</p> <p><img"
        " src=\"http://imgix.scout.com/137/1370536.jpg\" /></p> </td> <td> <p><strong>Bothell (0-0) "
        "Bothell, Wash.</strong><br /><br />Bothell concluded its 2015 season 14-0 and the top team "
        "in the Pacific Northwest, and it is beginning this season atop the perch as well.<br /><br "
        "/><strong>Next:Â </strong>9/4 vs. Eastlake (0-0)</p> </td> </tr></tbody></table>";


NSString *smallerTable =
    @"<table width=\"100%\" height=\"2260\" class=\"responsive-table\">"
        "<thead>"
        "<tr>"
        "<td>Rank¬ </td>"
"<td>Team (Record) City, ST</td>"
"</tr>"
"</thead>"
"<tbody>"
"<tr><td> <p>23.</p> <p><img src"
"=\"http://saintjosephregional.org/athletics/Final_SJR_Artwork CROP.JPG\" width=\"118\" he"
"ight=\"91\" /></p> </td> <td> <p><strong>St. Joseph Regional (0-0), Montvale, N.J.</strong>"
"</p> <p><strong></strong></p> <p>A well-coached team will have plenty of work to do along t"
"he ultra-competitive North Jersey parochial scene, but 2017 athlete Louis Acceus, who holds"
" a few offers, will have a big say in how the season goes.</p> <p><b></b></p> <p><b>Next:Â "
"</b>9/5 at No. 21 Don Bosco Prep (0-0)</p> </td> </tr><tr><td> <p>24.</p> <p><img src=\"htt"
"p://www.mghelmets.com/high school helmets/archbishop-rummell-la.gif\" width=\"143\" hei"
"ght=\"107\" /></p> </td> <td> <p><strong>Archbishop Rummel (0-0) Metairie, La.</strong></p>"
" <p><strong></strong></p> <p>Arkansas commit Briston Guidry anchors the defensive line whil"
"e cornerback Kristian Fulton, who has Florida, LSU and Texas on his list, leads the seconda"
"ry.</p> <p><strong>Next:Â </strong>9/4 vs. University Lab (0-0)</p> </td> </tr>"
"</tbody>"
"</table>";

@end
