//
//  HtmlParser.m
//  Scout
//
//  Created by Sonu on 12/16/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import "HtmlParser.h"
#import "TTTAttributedLabel.h"
#import "ScoutRecognizer.h"
#import "ScoutDataWrapper.h"
#import "NSArray+BlocksKit.h"
#import "EntityReplacementRecognizer.h"
#import "ParagraphRecognizer.h"
#import "LineBreakRecognizer.h"
#import "BalancedRangeRecognizer.h"
#import "AnchorTagRecognizer.h"
#import "ListRecognizer.h"
#import "ListItemRecognizer.h"
#import "ImageTagRecognizer.h"
#import "ThreadQuoteRecognizer.h"
#import "HorizontalRuleRecognizer.h"
#import "TableRecognizer.h"
#import "TableCellImageRecognizer.h"

@implementation HtmlParser

+ (NSArray *)parseHtml:(NSString *)input withViewDescriptor:(ViewDescriptor *)viewDesc itemType:(ItemType)itemType forSubdomain:(NSString *)subdomain finalRender:(void (^)())finalRender
{
    NSArray *recognizers = [HtmlParser recognizersForItemType:itemType onSubdomain:subdomain];
    WrapperCollection *wrappers = [[WrapperCollection alloc] init];
    
    NSUInteger strlen = input.length;
    for (__block int charIdx = 0; charIdx < strlen; charIdx++) {
        __block BOOL found = NO;
        NSRange range = NSMakeRange(charIdx, strlen - charIdx);
        for (ScoutRecognizer *recognizer in recognizers) {
            NSArray *matches = [recognizer.regex matchesInString:input options:NSMatchingAnchored range:range];
            if (matches && matches.count) {
                NSTextCheckingResult *result = matches[0];
                charIdx += result.range.length;
                charIdx--;
                [recognizer modifyWrappers:wrappers forMatch:[input substringWithRange:result.range]];
                found = YES;
                break;
            }
        }
        if (!found) {
            [[[EntityReplacementRecognizer alloc] initWithEntity:nil replacement:[input substringWithRange:NSMakeRange(charIdx, 1)]] modifyWrappers:wrappers forMatch:nil];
        }
    }
    
    __block BOOL parseComplete = NO;
    __block int pendingViewCount = 0;

    NSMutableArray *views = [[NSMutableArray alloc] init];
    for (ScoutDataWrapper *wrapper in wrappers) {
        int generatedPendingViews = 0;
        [views addObjectsFromArray:[wrapper renderToViewsWithViewDescriptor:viewDesc pendingViewCounter:&generatedPendingViews onPendingViewsResolved:^{
            pendingViewCount--;
            if (pendingViewCount == 0 && parseComplete && finalRender) {
                finalRender();
            }
        }]];
        pendingViewCount += generatedPendingViews;
    }
    parseComplete = YES;
    return views;
}

+ (NSArray *)recognizersForItemType: (ItemType)itemType onSubdomain:(NSString *)subdomain
{
    NSArray* recognizers = [self getRecognizersForItemType:itemType];
        
    ParseContext *context = [ParseContext new];
    context.subdomain = subdomain;
    for (ScoutRecognizer *recognizer in recognizers) {
        recognizer.context = context;
    }
        
    return recognizers;
}

+ (NSArray*)getRecognizersForItemType: (ItemType)itemType
{
    static NSArray* storyRecognizers;
    static NSArray* threadRecognizers;
    static NSArray* tableCellRecognizers;
    
    if(itemType == STORY){
        if(!storyRecognizers){
            storyRecognizers = [self buildRecognizersForItemType:itemType];
        }
        return storyRecognizers;
    } else if (itemType == TABLECELL) {
        if (!tableCellRecognizers) {
            tableCellRecognizers = [self buildRecognizersForItemType:TABLECELL];
        }
        return tableCellRecognizers;
    }
    else {
        if(!threadRecognizers){
            threadRecognizers = [self buildRecognizersForItemType:itemType];
        }
        return threadRecognizers;
    }
}
       
+ (NSArray*)buildRecognizersForItemType: (ItemType)itemType
{
    // Set up the pattern recognizers
    NSArray* recognizers = [self initialRecognizers];
    NSArray *typeSpecific = [self typeSpecificRecognizers:itemType];
    recognizers = [recognizers arrayByAddingObjectsFromArray:typeSpecific];
    
    recognizers = [recognizers arrayByAddingObjectsFromArray:[BalancedRangeRecognizer generateRecognizerPairForTagName:@"strong" withRangeType:BOLD]];
    recognizers = [recognizers arrayByAddingObjectsFromArray:[BalancedRangeRecognizer generateRecognizerPairForTagName:@"b" withRangeType:BOLD]];
    recognizers = [recognizers arrayByAddingObjectsFromArray:[BalancedRangeRecognizer generateRecognizerPairForTagName:@"em" withRangeType:ITALIC]];
    recognizers = [recognizers arrayByAddingObjectsFromArray:[BalancedRangeRecognizer generateRecognizerPairForTagName:@"h1" withRangeType:HEADER1]];
    recognizers = [recognizers arrayByAddingObjectsFromArray:[BalancedRangeRecognizer generateRecognizerPairForTagName:@"h2" withRangeType:HEADER2]];
    recognizers = [recognizers arrayByAddingObjectsFromArray:[BalancedRangeRecognizer generateRecognizerPairForTagName:@"h3" withRangeType:HEADER3]];
    recognizers = [recognizers arrayByAddingObjectsFromArray:[BalancedRangeRecognizer generateRecognizerPairForTagName:@"h4" withRangeType:HEADER4]];
    recognizers = [recognizers arrayByAddingObjectsFromArray:[BalancedRangeRecognizer generateRecognizerPairForTagName:@"h5" withRangeType:HEADER5]];
    recognizers = [recognizers arrayByAddingObjectsFromArray:[BalancedRangeRecognizer generateRecognizerPairForTagName:@"h6" withRangeType:HEADER6]];

    recognizers = [recognizers arrayByAddingObjectsFromArray:[AnchorTagRecognizer generateRecognizerPair]];
    recognizers = [recognizers arrayByAddingObject:[ParagraphRecognizer new]];
    
    // Recognizer to match all other html tags. This has to come last so it won't catch tags that we are actually looking for
    recognizers = [recognizers arrayByAddingObject:[[EntityReplacementRecognizer alloc] initWithRegex:[[NSRegularExpression alloc] initWithPattern:@"<[^>]*?>" options:NSRegularExpressionCaseInsensitive error:nil] replacement:@""]];
    return recognizers;
}

+(NSArray *)initialRecognizers
{
    return @[
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&nbsp;" replacement:@" "],
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&amp;" replacement:@"&"],
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&lt;" replacement:@"<"],
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&gt;" replacement:@">"],
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&quot;" replacement:@"\""],
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&ldquo;" replacement:@"\""],
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&rdquo;" replacement:@"\""],
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&lsquo;" replacement:@"‘"],
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&rsquo;" replacement:@"’"],
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&#39;" replacement:@"'"],
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&mdash;" replacement:@"--"],
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&ntilde;" replacement:@"\u00F1"],
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&#147;" replacement:@"“"],
        [[EntityReplacementRecognizer alloc] initWithEntity:@"&#148;" replacement:@"”"]];
}

+(NSArray *)typeSpecificRecognizers:(ItemType)itemType
{
    if (itemType == STORY) {
        return @[[[ImageTagRecognizer alloc] init],
                [TableRecognizer  new],
                [[EntityReplacementRecognizer alloc] initWithEntity:@"</blockquote>" replacement:@""],
                [ListRecognizer new],
                [ListItemRecognizer new]];
    } else if (itemType == TABLECELL) {
        return @[[[TableCellImageRecognizer alloc] init],
                 [[EntityReplacementRecognizer alloc] initWithEntity:@"</blockquote>" replacement:@""]];
    } else {
        return @[[ThreadQuoteRecognizer generateRecognizerPair]];
    }
}

@end
