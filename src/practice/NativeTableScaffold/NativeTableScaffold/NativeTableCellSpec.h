//
// Created by Don Clore on 8/31/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class CellInfo;

@interface NativeTableCellSpec : NSObject
@property (nonatomic, strong) NSArray *views;
@property (nonatomic, assign) CGRect frame;
@property (nonatomic, strong) CellInfo *cellInfo;
@end