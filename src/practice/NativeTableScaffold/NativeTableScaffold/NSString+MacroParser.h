//
//  NSString+ParseIntoMediaUrl.h
//  Scout
//
//  Created by Avi Finkel on 1/3/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MacroParser)

- (NSString *)parseOutMediaId;
- (NSURL *)parseStoryImageURL;
- (NSString *)parseOutYoutubeId;
- (NSMutableArray*)parseGalleryIds;
- (NSString*)parseOutPlayerCardId;

@end
