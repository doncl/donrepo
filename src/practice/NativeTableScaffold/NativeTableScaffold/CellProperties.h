//
// Created by Don Clore on 8/2/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface CellProperties : NSObject
@property (strong, nonatomic) UIColor *headerColor; // Background color of header.

// Header font properties.
@property (strong, nonatomic) NSString *headerFontName;
@property (nonatomic, assign) CGFloat headerFontSize;
@property (strong, nonatomic) UIColor *headerTextColor;

// Data cell font properties.
@property (strong, nonatomic) NSString *dataCellFontName;
@property (strong, nonatomic) NSString *dataCellBoldFontName;
@property (strong, nonatomic) NSString *dataCellItalicFontName;
@property (nonatomic, assign) CGFloat dataCellFontSize;
@property (strong, nonatomic) UIColor *dataCellTextColor;

// Odd and even rows have different colors.  Set the the same if you want them the same.
@property (strong, nonatomic) UIColor *oddRowColor;
@property (strong, nonatomic) UIColor *evenRowColor;

// Border properties.
@property (strong, nonatomic) UIColor *borderColor;
@property (nonatomic, assign) CGFloat borderWidth;

@property (nonatomic, assign) CGFloat maxCellWidthAsPercentageOfViewWidth;

@property (nonatomic, assign) CGFloat cellHorizontalPadding;
@property (nonatomic, assign) CGFloat cellVerticalPadding;
@end