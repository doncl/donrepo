//
//  HtmlParser.h
//  Scout
//
//  Created by Sonu on 12/16/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewDescriptor.h"
#import "FeedItem.h"

@interface HtmlParser : NSObject

+ (NSArray *)parseHtml:(NSString *)input withViewDescriptor:(ViewDescriptor *)viewDesc itemType:(ItemType)itemType forSubdomain:(NSString *)subdomain finalRender:(void (^)())finalRender;

@end
