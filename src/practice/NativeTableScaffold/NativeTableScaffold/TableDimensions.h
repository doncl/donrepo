//
// Created by Don Clore on 8/31/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface TableDimensions : NSObject
@property (nonatomic, strong) NSArray *columnWidths;
@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, assign) CGFloat tableHeight;
@end