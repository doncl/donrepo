//
//  ScoutTextRange.h
//  Scout
//
//  Created by Avi Finkel on 12/31/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TextRangeType.h"

@interface ScoutTextRange : NSObject

@property TextRangeType type;
@property NSRange range;
@property NSURL *linkUrl;

@end
