//
// Created by Don Clore on 7/30/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CellInfo : NSObject
@property (nonatomic, assign) CGFloat fontsize;
@property (nonatomic, strong) NSString *fontName;
@property (nonatomic, strong) NSString *italicFontName;
@property (nonatomic, strong) NSString *boldFontName;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) UIColor *borderColor;
@property (nonatomic, assign) CGFloat borderWidth;
@property (nonatomic, assign) NSTextAlignment textAlignment;
@property (nonatomic, assign) UIBaselineAdjustment baselineAdjustment;
@end