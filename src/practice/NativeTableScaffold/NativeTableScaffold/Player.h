//
//  Player.h
//  Scout
//
//  Created by Sonu on 8/17/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Player : NSManagedObject

@property (nonatomic) BOOL committed;
@property (nonatomic, retain) NSString * committedTeam;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic) double forty;
@property (nonatomic) int32_t height;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic) BOOL isFortyVerified;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic) int32_t overallRank;
@property (nonatomic, retain) NSString * playerId;
@property (nonatomic, retain) NSString * playerNodeId;
@property (nonatomic, retain) NSString * postitionAbbreviation;
@property (nonatomic) int32_t rank;
@property (nonatomic) int32_t schoolsOfInterest;
@property (nonatomic, retain) NSString * spotlightVideoURL;
@property (nonatomic) int32_t starRating;
@property (nonatomic) int32_t weight;
@property (nonatomic, retain) NSString * canonicalURL;

@end
