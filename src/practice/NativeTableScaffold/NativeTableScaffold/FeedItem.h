//
//  FeedItem.h
//  Scout
//
//  Created by Avi Finkel on 6/20/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>

//#import "Story.h"
//#import "Thread.h"

typedef enum {
    STORY, THREAD, TABLECELL,
} ItemType;

