//
//  Media.m
//  Scout
//
//  Created by Don Clore on 5/28/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "Media.h"


@implementation Media

@dynamic caption;
@dynamic height;
@dynamic mediaId;
@dynamic thumbnailUri;
@dynamic type;
@dynamic uri;
@dynamic width;

@end
