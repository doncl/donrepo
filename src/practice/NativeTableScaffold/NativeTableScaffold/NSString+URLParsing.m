//
//  NSString+URLParsing.m
//  Scout
//
//  Created by Sonu on 1/28/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "NSString+URLParsing.h"

#define PLAYER_NODE_PREFIX @"[PlayerNode:"

@implementation NSString (URLParsing)

- (NSURL *)parseUrlFromTagAfterString: (NSString *)prefix
{
    return [self parseUrlFromTagAfterString:prefix forSubdomain:nil];
}

- (NSURL *)parseUrlFromTagAfterString:(NSString *)string forSubdomain:(NSString *)subdomain
{
    NSRange stringRange = [self rangeOfString:string];
    if (stringRange.location != NSNotFound) {
        NSUInteger indexOfFirstQuote = stringRange.location + stringRange.length;
        NSString *quote = [self substringWithRange:NSMakeRange(indexOfFirstQuote, 1)];
        NSRange secondQuoteRange = [self rangeOfString:quote options:0 range:NSMakeRange(indexOfFirstQuote + 1, self.length - (indexOfFirstQuote + 1))];
        if (secondQuoteRange.location != NSNotFound) {
            NSString *urlValue = [self substringWithRange:NSMakeRange(indexOfFirstQuote + 1, secondQuoteRange.location - (indexOfFirstQuote + 1))];
            urlValue = [urlValue stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
            if ([urlValue rangeOfString:@"http" options:NSAnchoredSearch].location != NSNotFound) {
                NSString * urlString = [urlValue stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
                if (!urlString) {
                    return nil;
                }
                return [[NSURL alloc] initWithString:urlString];
            }
            else if (subdomain && [urlValue rangeOfString:PLAYER_NODE_PREFIX options:NSAnchoredSearch].location != NSNotFound) {
                return [[NSURL alloc] initWithScheme:@"http" host:[subdomain stringByAppendingString:@".scout.com"] path:[NSString stringWithFormat:@"/player/%@n", [urlValue substringWithRange:NSMakeRange(PLAYER_NODE_PREFIX.length, [urlValue rangeOfString:@"]"].location - PLAYER_NODE_PREFIX.length)]]];
            }
        }
    }

    return nil;
}

// From chown at http://stackoverflow.com/questions/8088473/url-encode-an-nsstring
- (NSString *)urlencode {
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[self UTF8String];
    NSUInteger sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' ') {
            [output appendString:@"%20"];
        }else {
            [output appendFormat:@"%c", thisChar];
        }
    }
    return output;
}

- (BOOL)isScoutURL {
    if([self rangeOfString:@"scout.com"].location == NSNotFound){
        return NO;
    }
    else{
        return YES;
    }
}

- (NSString *)stringByDecodingURLFormat
{
    NSString *result;
    result = [self stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    result = [result stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return result;
}

- (NSString *)stringByRemovingTLD
{
    NSRange dotComLocation = [self rangeOfString:@".com" options:NSBackwardsSearch & NSCaseInsensitiveSearch];
    if (dotComLocation.location != NSNotFound) {
        return [self stringByReplacingCharactersInRange:dotComLocation withString:@""];
    }
    return self;
}

- (long)getTweetId
{
    static NSRegularExpression *regex = nil;
    if (regex == nil) {
        regex = [NSRegularExpression regularExpressionWithPattern:@"https?://twitter.com/.*?/status/(\\d+)" options:NSRegularExpressionCaseInsensitive error:nil];
    }
    
    NSArray *matches = [regex matchesInString:self options:0 range:NSMakeRange(0, self.length)];
    if (matches.count) {
        long tweetId = [[self substringWithRange:[matches[0] rangeAtIndex:1]] integerValue];
        return tweetId;
    }
    return 0;
}

- (NSString*)getYoutubeId
{
    static NSRegularExpression *regex = nil;
    if (regex == nil) {
        regex = [NSRegularExpression regularExpressionWithPattern:@"https?://(www\\.)?youtube.com/watch\\?v=([^>]*)&?" options:NSRegularExpressionCaseInsensitive error:nil];
    }
    
    NSArray *matches = [regex matchesInString:self options:0 range:NSMakeRange(0, self.length)];
    if(matches.count){
        NSString *youtubeId = [self substringWithRange:[matches[0] rangeAtIndex:2]];
        return youtubeId;
    }
    return nil;
}

- (NSRegularExpression*)getPlayerLinkRegex
{
    static NSRegularExpression *regex = nil;
    if (regex == nil) {
        regex = [NSRegularExpression regularExpressionWithPattern:@"scout.com/player/(.+?)/videos/([0-9a-zA-Z]+)" options:NSRegularExpressionCaseInsensitive error:nil];
    }
    
    return regex;
}

- (BOOL)isPlayerVideoLink
{
    NSArray* matches = [[self getPlayerLinkRegex] matchesInString:self options:0 range:NSMakeRange(0, self.length)];
    if(matches.count){
        return YES;
    }
    else{
        return NO;
    }
}

- (NSString*)getVideoId
{
    NSArray* matches = [[self getPlayerLinkRegex] matchesInString:self options:0 range:NSMakeRange(0, self.length)];
    if(matches.count){
        NSString *youtubeId = [self substringWithRange:[matches[0] rangeAtIndex:2]];
        return youtubeId;
    }
    else{
        return nil;
    }
}

- (NSRegularExpression*)getScoutStoryRegex
{
    static NSRegularExpression *storyRegex = nil;
    if (storyRegex == nil) {
        storyRegex = [NSRegularExpression regularExpressionWithPattern:@"scout.com([^>]*)/story/([0-9a-zA-z]+)-?" options:NSRegularExpressionCaseInsensitive error:nil];
    }
    return storyRegex;
}

- (BOOL)isStoryLink
{
    NSArray* matches = [[self getScoutStoryRegex] matchesInString:self options:0 range:NSMakeRange(0, self.length)];
    if(matches.count){
        return YES;
    }
    else{
        return NO;
    }
}

- (NSString *)getScoutStoryId
{
    NSArray* matches = [[self getScoutStoryRegex] matchesInString:self options:0 range:NSMakeRange(0, self.length)];
    if(matches.count){
        NSString *storyId = [self substringWithRange:[matches[0] rangeAtIndex:2]];
        return storyId;
    }
    else{
        return nil;
    }
}

- (NSString *)getBoxxspringVideoId
{
    static NSRegularExpression *boxxspringRegex = nil;
    if (boxxspringRegex == nil) {
        boxxspringRegex = [NSRegularExpression regularExpressionWithPattern:@"widgets.boxxspring.com/([^>]*)" options:NSRegularExpressionCaseInsensitive error:nil];
    }

    NSArray* matches = [boxxspringRegex matchesInString:self options:0 range:NSMakeRange(0, self.length)];
    if(matches.count){
        NSString *boxxspringId = [self substringWithRange:[matches[0] rangeAtIndex:1]];
        return boxxspringId;
    }
    else{
        return nil;
    }
}

@end
