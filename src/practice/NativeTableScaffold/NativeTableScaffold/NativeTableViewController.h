//
//  NativeTableViewController.h
//
//  Created by Don Clore on 7/29/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <TTTAttributedLabel.h>
#import "NativeTableLayout.h"
#import "TableProperties.h"

typedef NS_ENUM(NSInteger, NativeTableLoadMode) {
    DefaultView,
    CreateView,
};

@class NativeTableModel;
@class CellProperties;
@class TableProperties;

@interface NativeTableViewController : UIViewController<UICollectionViewDataSource,
    UICollectionViewDelegate, NativeTableCellViewDelegate, TTTAttributedLabelDelegate>

#pragma mark - initializers
- (instancetype)initWithLoadMode:(NativeTableLoadMode)loadMode
                           width:(CGFloat)width
                 tableProperties:(TableProperties *)tableProperties
                           model:(NativeTableModel *)model NS_DESIGNATED_INITIALIZER;
@end

