//
//  NativeTableViewController.m
//  NativeTableScaffold
//
//  Created by Don Clore on 7/29/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "NativeTableViewController.h"
#import "NativeTableCell.h"
#import "NativeTableModel.h"
#import "CellInfo.h"
#import "CellProperties.h"
#import "TableProperties.h"
#import "HtmlParser.h"
#import "NativeTableCellSpec.h"
#import "TableDimensions.h"
#import "MWLogging.h"
#import "TTTAttributedLabel.h"
#import "NSString+URLParsing.h"
#import "UIView+StackSubviews.h"

@interface NativeTableViewController ()
#pragma mark - properties
@property (strong, nonatomic) NSMutableArray *cellSpecs;
@property (strong, nonatomic) UICollectionView *nonFixedView;
@property (strong, nonatomic) UICollectionView *fixedView;
@property (strong, nonatomic) NativeTableModel *fixedModel;
@property (strong, nonatomic) NativeTableModel *nonFixedModel;
@property (nonatomic, assign) NativeTableLoadMode loadMode;
@property (nonatomic, assign) CGFloat viewWidth;
@property (nonatomic, assign) CGFloat maxCellWidth;
@property (strong, nonatomic) NSArray *fixedCellViews;
@property (strong, nonatomic) NSArray *nonFixedCellViews;
@property (strong, nonatomic) NativeTableModel *model;
@property (strong, nonatomic) TableProperties *tableProperties;
@property (weak, nonatomic) NativeTableLayout *fixedLayout;
@property (weak, nonatomic) NativeTableLayout *nonFixedLayout;
@end

@implementation NativeTableViewController

#pragma mark - designated initializer
- (instancetype)initWithLoadMode:(NativeTableLoadMode)loadMode
                           width:(CGFloat)width
                 tableProperties:(TableProperties *)tableProperties
                           model:(NativeTableModel *)model
{
    self = [super init];
    if (self) {
        self.loadMode = loadMode;
        self.viewWidth = width;
        self.model = model;
        self.tableProperties = tableProperties;
        self.cellSpecs = nil;
    }
    return self;
}

#pragma mark - creation
- (void)loadView
{
    [self setUpCellProperties];
    [self measureAllDimensions];

    NativeTableModel *fixed;
    NativeTableModel *nonFixed;
    [NativeTableModel buildFixedAndNonFixedModels:self.model
                                 fixedColumnModel:&fixed
                              nonFixedColumnModel:&nonFixed
                                 fixedColumnCount:self.tableProperties.fixedColumns];

    self.fixedModel = fixed;
    self.nonFixedModel = nonFixed;

    self.maxCellWidth = self.viewWidth *
            self.tableProperties.maxCellWidthAsPercentageOfViewWidth;

        NativeTableLayout *fixedLayout =
            [[NativeTableLayout alloc]initWithLayoutType:FixedColumns
                                                delegate:self
                                                   model:self.fixedModel];

        NativeTableLayout *nonFixedLayout =
            [[NativeTableLayout alloc] initWithLayoutType:ScrollingColumns
                                                 delegate:self
                                                    model:self.nonFixedModel];

        if (self.loadMode == DefaultView) {
            [super loadView];
        } else {
            CGRect frame = CGRectMake(0, 0, self.viewWidth, fixedLayout.tableHeight);
            self.view = [[UIView alloc] initWithFrame:frame];
        }

        self.fixedView = [self createCollectionView:self.view
                                             layout:fixedLayout
                                                  x: 0
                                              width:fixedLayout.tableWidth];

        CGFloat nonFixedX = self.view.frame.origin.x + fixedLayout.tableWidth;
        CGFloat nonFixedWidth = self.view.frame.size.width - nonFixedX;

        self.nonFixedView = [self createCollectionView:self.view
                                                layout:nonFixedLayout
                                                     x:nonFixedX
                                                 width:nonFixedWidth];

        self.nonFixedView.scrollEnabled = YES;
        self.nonFixedView.showsHorizontalScrollIndicator = YES;

        [self.view addSubview:self.fixedView];
        [self.view addSubview:self.nonFixedView];
        self.fixedLayout = fixedLayout;
        self.nonFixedLayout = nonFixedLayout;
}

///
///  Calculates cellspecs for the entire table, and table dimensions from that.
///
- (void)measureAllDimensions
{
    NSUInteger rowCount = [self.model numberOfRows];
    NSUInteger colCount = [self.model numberOfColumns];

    if (!self.cellSpecs) {
        self.cellSpecs = [self allocateCellSpecs:rowCount colCount:colCount];
    }

    CGFloat maxCellWidth = self.viewWidth *
        self.tableProperties.maxCellWidthAsPercentageOfViewWidth;

    if (self.tableProperties.tableType == HtmlCells) {
        [self cellSpecsForHtmlCellsTable:self.model
                         tableProperties:self.tableProperties
                                rowCount:rowCount
                                colCount:colCount
                               cellSpecs:self.cellSpecs
                            maxCellWidth:maxCellWidth];
    } else {
        [self cellSpecsForSimpleTextTable:self.model
                                 rowCount:rowCount
                                 colCount:colCount
                                cellSpecs:self.cellSpecs
                             maxCellWidth:maxCellWidth];
    }

    [self normalizeCellSpecsAndCalculateTableDims:self.cellSpecs
                                         rowCount:rowCount
                                         colCount:colCount
                                  tableProperties:self.tableProperties];
}

- (NSMutableArray *)allocateCellSpecs:(NSUInteger)rowCount colCount:(NSUInteger)colCount
{
    NSMutableArray *cellSpecs = [[NSMutableArray alloc] initWithCapacity:rowCount];
    // Prepare space for all the cellSpec objects.
    for (NSUInteger i = 0; i < rowCount; i++) {
        NSMutableArray *row = [[NSMutableArray alloc] initWithCapacity:colCount];
        for (NSUInteger j = 0; j < colCount; j++) {
            [row addObject:[NativeTableCellSpec new]]; // Just to make it big enough.
        }
        [cellSpecs addObject:row];
    }
    return cellSpecs;
}

- (UICollectionView *)createCollectionView:(UIView *)parentView
                                    layout:(NativeTableLayout *)layout
                                         x:(CGFloat)x
                                     width:(CGFloat)width
{
    CGRect frame = CGRectMake(x, parentView.frame.origin.y,
        width, parentView.frame.size.height);

    UICollectionView *collectionView =  [[UICollectionView alloc] initWithFrame:frame
                                                           collectionViewLayout:layout];

    collectionView.bounces = NO;
    collectionView.showsVerticalScrollIndicator = NO;

    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    [collectionView registerClass:[NativeTableCell class]
       forCellWithReuseIdentifier:@"cellIdentifier"];
    collectionView.backgroundColor = [UIColor whiteColor];
    return collectionView;
}

- (void)setUpCellProperties
{
    [NativeTableCell initializeCellProperties:self.tableProperties];
}

#pragma mark - synchronized scrolling
- (void)matchScrollView:(UIScrollView *)first toScrollView:(UIScrollView *)second
{
    CGPoint offset = first.contentOffset;
    offset.y = second.contentOffset.y;
    [first setContentOffset:offset];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([scrollView isEqual:self.fixedView]) {
        [self matchScrollView:self.nonFixedView toScrollView:self.fixedView];
    } else {
        [self matchScrollView:self.fixedView toScrollView:self.nonFixedView];
    }
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    NativeTableModel * model = [self getModelFromCollectionView:collectionView];

    NSUInteger columns = [model numberOfColumns];
    return columns;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    NativeTableModel * model = [self getModelFromCollectionView:collectionView];
    NSUInteger rows = [model numberOfRows];
    return rows;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NativeTableModel * model = [self getModelFromCollectionView:collectionView];

    NativeTableCell *cell =
        [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier"
                                                     forIndexPath:indexPath];

    NativeTableLayout *layout = (NativeTableLayout *)collectionView.collectionViewLayout;
    NativeTableLayoutType type = layout.layoutType;

    NSUInteger columnIdx = indexPath.row;
    NSUInteger rowIdx = indexPath.section;

    NSString *cellData = [model dataAtLocation:rowIdx column:columnIdx];
    cellType cellType = [self getCellType:indexPath.section];
    [cell setUpCellInfo:cellType];

    CGFloat columnWidth = [[layout.columnWidths objectAtIndex:indexPath.row] floatValue];
    cell.contentView.bounds = CGRectMake(0, 0, columnWidth, layout.rowHeight);

    if (self.tableProperties.tableType == SimpleText) {
        [cell setCellText:cellData];
        cell.label.hidden = NO;
    } else {
        for (UIView *subView in cell.contentView.subviews) {
            [subView removeFromSuperview];
        }

        NSUInteger adjustedColumnIdx = type == FixedColumns ?
            columnIdx : columnIdx + self.tableProperties.fixedColumns;

        NSArray *rowSpecs = [self.cellSpecs objectAtIndex:rowIdx];
        NativeTableCellSpec *cellSpec = [rowSpecs objectAtIndex:adjustedColumnIdx];
        [cell.contentView addSubviews:cellSpec.views];
        [self layoutContent:cell];
        cell.label.hidden = YES;
    }
    return cell;
}

- (void)layoutContent:(NativeTableCell *)cell
{
    CGFloat horzPad = self.tableProperties.cellHorizontalPadding;

    if ([cell.contentView.subviews count] == 1) {
        UIView *child = [cell.contentView.subviews objectAtIndex:0];
        CGPoint center = [cell.contentView convertPoint:cell.contentView.center
                                               fromView:cell.contentView.superview];

        // N.B. do we want single view cells to have their content centered, or left aligned?
        // this is where we pick.
#if 0
        child.center = center;
#else
        child.center = CGPointMake(horzPad + child.frame.size.width / 2, center.y);
#endif

    } else {
        for (UIView *view in cell.contentView.subviews) {
            if ([view isKindOfClass:[TTTAttributedLabel class]]) {
                ((TTTAttributedLabel *) view).delegate = self;
            }
        }
    }

    [cell.contentView setNeedsDisplay];
}

- (cellType)getCellType:(NSInteger)rowIdx
{
    cellType cellType;
    if (self.model.hasHeader) {
        rowIdx++; // advance it one, because the first non-header row is treated as an 'even'
                  // row.
        if (rowIdx == 1) {
            return columnHeading;
        }
    }

    if ((rowIdx % 2) == 0) {
        cellType = dataEven;
    } else {
        cellType = dataOdd;
    }
    return cellType;
}

- (CellInfo *)getCellInfoForRow:(NSInteger)rowIdx
{
    cellType type = [self getCellType:rowIdx];
    return [NativeTableCell getCellInfoForTypeOfCell:type];
}

#pragma mark - NativeTableCelViewDelegate implementation

- (NSDictionary *)getTableDimensions:(NativeTableLayoutType)layoutType
{
    NativeTableModel *model = [self getModelFromLayoutType:layoutType];
    NSUInteger colCount = [model numberOfColumns];

    // Get subarray from widths that correspond to this layoutType/layout.
    NSUInteger start = layoutType == FixedColumns ? 0 : self.tableProperties.fixedColumns;
    NSRange r = NSMakeRange(start, colCount);
    NSArray *widths = [self.tableProperties.dimensions.columnWidths subarrayWithRange:r];

    // Sum up the tableWidth.
    NSUInteger tableWidth = 0.0;
    for (NSUInteger iCol = 0; iCol < colCount; iCol++) {
        CGFloat width = [[widths objectAtIndex:iCol] floatValue];
        tableWidth += width;
    }

    return @{@"columnCount": @(colCount),
        @"rowCount": @([model numberOfRows]),
        @"tableHeight": @(self.tableProperties.dimensions.tableHeight),
        @"tableWidth" : @(tableWidth),
        @"rowHeight" : @(self.tableProperties.dimensions.rowHeight),
        @"columnWidths" : widths};
}

#pragma mark - Model helpers
- (NativeTableModel *)getModelFromCollectionView:(UICollectionView *)view
{
    return view == self.nonFixedView ? _nonFixedModel : _fixedModel;
}

- (NativeTableModel *)getModelFromLayoutType:(NativeTableLayoutType)layoutType
{
    return layoutType == ScrollingColumns ? _nonFixedModel : _fixedModel;
}


#pragma mark - measuring (and re-measuring on demand) the dimensions for things
- (void)cellSpecsForHtmlCellsTable:(const NativeTableModel *)model
                   tableProperties:(const TableProperties *)tableProperties
                          rowCount:(NSUInteger)rowCount
                          colCount:(NSUInteger)colCount
                         cellSpecs:(const NSMutableArray *)cellSpecs
                      maxCellWidth:(CGFloat)maxCellWidth
{
    ViewDescriptor *viewDesc = [[ViewDescriptor alloc] init];
    viewDesc.width = maxCellWidth;

    viewDesc.foregroundColor = tableProperties.dataCellTextColor;
    viewDesc.verticalPadding = 15.0;

    for (NSUInteger iRow = 0; iRow < rowCount; iRow++) {
        for (NSUInteger iCol = 0; iCol < colCount; iCol++) {
            NSString *data = [model dataAtLocation:iRow column:iCol];
            NativeTableCellSpec *cellSpec = [NativeTableCellSpec new];
            cellSpec.cellInfo = [self getCellInfoForRow:iRow];
            viewDesc.plainFont = [UIFont fontWithName:cellSpec.cellInfo.fontName
                                                 size:cellSpec.cellInfo.fontsize];
            viewDesc.italicFont = [UIFont fontWithName:cellSpec.cellInfo.italicFontName
                                                  size:cellSpec.cellInfo.fontsize];
            viewDesc.italicFont = [UIFont fontWithName:cellSpec.cellInfo.boldFontName
                                                  size:cellSpec.cellInfo.fontsize];
            viewDesc.foregroundColor = cellSpec.cellInfo.textColor;

            NSArray *views =
            [HtmlParser parseHtml:data
               withViewDescriptor:viewDesc
                         itemType:TABLECELL
                     forSubdomain:nil
                      finalRender:^{
                          [self normalizeCellSpecsAndCalculateTableDims:self.cellSpecs
                                                               rowCount:rowCount
                                                               colCount:colCount
                                                        tableProperties:self.tableProperties];

                          if (iCol < self.tableProperties.fixedColumns) {
                              [self.fixedLayout invalidateLayout];
                          } else {
                              [self.nonFixedLayout invalidateLayout];
                          }
                      }];


            cellSpec.views = views;
            NSMutableArray *row = [cellSpecs objectAtIndex:iRow];
            [row setObject:cellSpec atIndexedSubscript:iCol];
        }
    }
    // Now get the frame rects for each cell spec.  We're on the main queue again, so this can
    // be done.
    [self setFramesForCellSpecsForHtmlCells:cellSpecs
                                   rowCount:rowCount
                                   colCount:colCount
                               maxCellWidth:maxCellWidth];
}

- (NativeTableCellSpec *)specAtRow:(NSUInteger)rowIdx col:(NSUInteger)colIdx
{
    NSArray *row = [self.cellSpecs objectAtIndex:rowIdx];
    return [row objectAtIndex:colIdx];
}


- (void)setFramesForCellSpecsForHtmlCells:(const NSMutableArray *)cellSpecs
                                 rowCount:(NSUInteger)rowCount
                                 colCount:(NSUInteger)colCount
                             maxCellWidth:(CGFloat)maxCellWidth
{
    for (NSUInteger iRow = 0; iRow < rowCount; iRow++) {
        NSMutableArray *row = [cellSpecs objectAtIndex:iRow];
        for (NSUInteger jCol = 0; jCol < colCount; jCol++) {
            NativeTableCellSpec *spec = [row objectAtIndex:jCol];
            if (!spec || !spec.views) {
                MWLogError(@"No views found for table cell row %d col %d", iRow, jCol);
            } else {
                CGFloat vertPad = self.tableProperties.cellVerticalPadding;
                CGFloat horzPad = self.tableProperties.cellHorizontalPadding;
                CGFloat height = vertPad;
                CGFloat width = 0.0;

                if ([spec.views count] == 1) {
                    UIView *onlyView = [spec.views objectAtIndex:0];
                    height = onlyView.frame.size.height;
                    width = onlyView.frame.size.width;
                } else {
                    for (UIView *view in spec.views) {
                        view.frame = CGRectMake(horzPad, height, view.frame.size.width,
                                view.frame.size.height);
                        height += (view.frame.size.height + vertPad);

                        if (view.frame.size.width > width) {
                            width = view.frame.size.width;
                        }
                    }
                    width = MIN(width, maxCellWidth);
                }
                spec.frame  =  CGRectMake(0, 0,
                    width + (horzPad * 2),
                    height + (self.tableProperties.cellVerticalPadding * 2));
            }
        }
    }
}

///
///   This is for the case where the table is being used to display text only.  This may
///   (probably?) not be used for the Scout App, but it's helpful to have it here when
//    refactoring, to retest and break things down to the simplest case.
///
- (void)cellSpecsForSimpleTextTable:(const NativeTableModel *)model
                           rowCount:(NSUInteger)rowCount
                           colCount:(NSUInteger)colCount
                          cellSpecs:(const NSMutableArray *)cellSpecs
                       maxCellWidth:(CGFloat)maxCellWidth
{
    for (NSUInteger iRow = 0; iRow < rowCount; iRow++) {
        NSMutableArray *rowSpecs = [self.cellSpecs objectAtIndex:iRow];
        for (NSUInteger iCol = 0; iCol < colCount; iCol++) {

            CGRect r = [self getRectForSimpleTextCell:model
                                          columnIndex:iCol
                                             rowIndex:iRow
                                         maxCellWidth:maxCellWidth];

            NativeTableCellSpec *cellSpec = [rowSpecs objectAtIndex:iCol];
            cellSpec.views = nil;
            cellSpec.frame = r;
            [rowSpecs addObject:cellSpec];
        }
        [cellSpecs addObject:rowSpecs];
    }
}

//
//  Current requirement for this table is a single cellHeight throughout, each column has
//  separate widths.   Fixup all the cellspecs to reflect this.   Calculate the table
// dimensions when this is complete.
//
- (void)normalizeCellSpecsAndCalculateTableDims:(NSMutableArray *)array
                                       rowCount:(NSUInteger)rowCount
                                       colCount:(NSUInteger)colCount
                                tableProperties:(TableProperties *)tableProperties
{
    tableProperties.dimensions = [TableDimensions new];
    CGFloat maxHeight = 0.0;
    NSMutableArray *widths = [[NSMutableArray alloc] initWithCapacity:colCount];
    for (NSUInteger iRow = 0; iRow < rowCount; iRow++) {
        NSArray *row = [array objectAtIndex:iRow];
        for (NSUInteger iCol = 0; iCol < colCount; iCol++) {
            NativeTableCellSpec *spec = [row objectAtIndex:iCol];
            if (spec.frame.size.height > maxHeight) {
                maxHeight = spec.frame.size.height;
            }
            if ([widths count] < colCount) {
                [widths addObject:[NSNumber numberWithFloat:spec.frame.size.width]];
            } else {
                CGFloat currentColWidth = [[widths objectAtIndex:iCol] floatValue];
                if (spec.frame.size.width > currentColWidth) {
                    [widths setObject:[NSNumber numberWithFloat:spec.frame.size.width]
                   atIndexedSubscript:iCol];
                }
            }
        }
    }

    // Now walk through the cellspecs again, setting each cellspec to a uniform size for the
    // column, and the heights should all be the same.
    for (NSUInteger iRow = 0; iRow < rowCount; iRow++) {
        NSArray *row = [array objectAtIndex:iRow];
        for (NSUInteger iCol = 0; iCol < colCount; iCol++) {
            NativeTableCellSpec *spec = [row objectAtIndex:iCol];
            CGFloat colWidth = [[widths objectAtIndex:iCol] floatValue];
            spec.frame = CGRectMake(spec.frame.origin.x, spec.frame.origin.y, colWidth,
                maxHeight);
        }
    }

    tableProperties.dimensions.columnWidths = widths;
    tableProperties.dimensions.rowHeight = maxHeight;
    tableProperties.dimensions.tableHeight = maxHeight * rowCount;
}

- (CGRect)getRectForSimpleTextCell:(const NativeTableModel *)model
                       columnIndex:(NSUInteger)columnIndex
                          rowIndex:(NSUInteger)rowIndex
                      maxCellWidth:(CGFloat)maxCellWidth
{
    NSString *cellData = [model dataAtLocation:rowIndex column:columnIndex];
    cellType type = [model getCellType:rowIndex column:columnIndex];
    CellInfo *ci = [NativeTableCell getCellInfoForTypeOfCell:type];
    UIFont *font = [UIFont fontWithName:ci.fontName size:ci.fontsize];

    NSString *newString = [cellData stringByReplacingOccurrencesOfString:@" " withString:@"X"];
    NSDictionary *attributes = @{NSFontAttributeName: font};
    CGRect rect = [newString boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:attributes
                                          context:nil];


    CGRect r =  CGRectMake(0, 0,
            rect.size.width + (self.tableProperties.cellHorizontalPadding * 2),
             rect.size.height + (self.tableProperties.cellVerticalPadding * 2));

    if (r.size.width > maxCellWidth) {
        r.size.width = maxCellWidth;
    }
    return r;
}

#pragma mark - TTTattributedLabelDelegate implementation

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if([[url absoluteString] isScoutURL]){
        [self performSegueWithIdentifier:@"webViewSegue" sender:url];
    }
    else{
        [self goToURL:url];
    }
}

- (void)goToURL: (NSURL*)url
{
    //[self trackGAEvent:@"Story" action:@"opened_link" label:url.absoluteString];
    [[UIApplication sharedApplication] openURL:url];
}

@end
