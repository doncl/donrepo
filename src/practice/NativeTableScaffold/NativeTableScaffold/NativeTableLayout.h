//
// Created by Don Clore on 8/2/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class NativeTableModel;

typedef NS_ENUM(NSInteger, NativeTableLayoutType) {
    FixedColumns,
    ScrollingColumns,
};

@protocol NativeTableCellViewDelegate <NSObject>
- (NSDictionary *)getTableDimensions:(NativeTableLayoutType)layoutType;
@end


@interface NativeTableLayout : UICollectionViewLayout
@property (weak, nonatomic)   id<NativeTableCellViewDelegate> delegate;
@property (strong, nonatomic) NSArray *columnWidths;
@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, assign) CGFloat tableHeight;
@property (nonatomic, assign) CGFloat tableWidth;
@property (nonatomic, assign) NativeTableLayoutType layoutType;

- (instancetype)initWithLayoutType:(NativeTableLayoutType)layoutType
                          delegate:(id<NativeTableCellViewDelegate>)delegate
                             model:(NativeTableModel *)model NS_DESIGNATED_INITIALIZER;
@end