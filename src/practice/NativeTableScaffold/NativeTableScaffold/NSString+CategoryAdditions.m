//
//  NSString+CategoryAdditions.m
//  Scout
//
//  Created by Eric Vermeire on 7/20/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "NSString+CategoryAdditions.h"

@implementation NSString (CategoryAdditions)

-(NSString *) cleanAPNSDeviceToken {
    /// By default an APNS device token looks like:
    /// <68c3b56f f9b531cd 44d63078 d2ca9f16 ea4a6ce9 ac6ff3f4 4c48ed64 89d7c3bc>
    /// We like to store in the format of // APNS tokens look like: 68c3b56ff9b531cd44d63078d2ca9f16ea4a6ce9ac6ff3f44c48ed6489d7c3bc
    NSString *cleanToken = [self stringByReplacingOccurrencesOfString:@"<" withString:@""];
    cleanToken = [cleanToken stringByReplacingOccurrencesOfString:@">" withString:@""];
    cleanToken = [cleanToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    return cleanToken;
}

//removes <p> tags, replaces &amp, and other html encoding
- (NSString*)removeCommonTags
{
    NSString* returnVal = [self stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
    returnVal = [returnVal stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
    returnVal = [returnVal stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    returnVal = [returnVal stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
    return returnVal;
}

- (bool)isNumeric
{
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return ([self rangeOfCharacterFromSet:notDigits].location == NSNotFound);
}

@end
