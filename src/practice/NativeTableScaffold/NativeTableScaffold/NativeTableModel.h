//
// Created by Don Clore on 7/30/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NativeTableCell.h"

// Error codes for buildFromText.
typedef NS_ENUM(NSInteger, NativeTableError) {
    ErrorParseTable = 1 << 0,
    ErrorParseHead = 1 << 1,
    ErrorParseHeadCells = 1 << 2,
    ErrorParseBody = 1 << 3,
    ErrorParseBodyRows = 1 << 4,
    ErrorParseBodyCells = 1 << 5,
};

@interface NativeTableModel : NSObject
@property (nonatomic, strong) NSArray *rowData;
@property (nonatomic, assign) BOOL hasHeader;

+ (NSRegularExpression *)regex;

- (BOOL)buildFromText:(NSString *)text error:(NSError **)ppError;
- (NSUInteger)numberOfRows;
- (NSUInteger)numberOfColumns;
- (NSString *)dataAtLocation:(NSUInteger)rowIndex column:(NSUInteger)columnIndex;

- (cellType)getCellType:(NSUInteger)
    row column:(NSUInteger)column;

+ (void)buildFixedAndNonFixedModels:(NativeTableModel *)srcModel
                   fixedColumnModel:(NativeTableModel **)ppFixedColumnModel
                nonFixedColumnModel:(NativeTableModel **)ppNonFixedColumnModel
                   fixedColumnCount:(NSUInteger)fixedColumnCount;
@end