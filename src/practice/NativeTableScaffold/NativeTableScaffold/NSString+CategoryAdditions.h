//
//  NSString+CategoryAdditions.h
//  Scout
//
//  Created by Eric Vermeire on 7/20/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>

// Category Addition(s) to the NSSting framework class
// I plan to refactor all the other NSString category classes to use this class instead.
@interface NSString (CategoryAdditions)

-(NSString *)cleanAPNSDeviceToken;
- (NSString*)removeCommonTags;
- (bool)isNumeric;

@end
