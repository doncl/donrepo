//
// Created by Don Clore on 7/30/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "NativeTableModel.h"

@interface NativeTableModel()
+ (NSRegularExpression *)headRegex;
+ (NSRegularExpression *)bodyRegex;
+ (NSRegularExpression *)rowRegex;
+ (NSRegularExpression *)dataRegex;
@end

#define errDomain @"com.scout.nativeTable"

@implementation NativeTableModel

-(instancetype)init
{
    self = [super init];
    if (self) {
        self.rowData = [self generateRowData];
    }
    return self;
}

#pragma mark - class regexes

+ (NSRegularExpression *)regex {
    static NSRegularExpression *tableRegex = nil;
    if (tableRegex == nil) {
        NSError *error;
        tableRegex = [[NSRegularExpression alloc]
            initWithPattern:@"<table.*?>(.*?)<\\/table>"
                    options:NSRegularExpressionCaseInsensitive |
                            NSRegularExpressionDotMatchesLineSeparators
                      error:&error];
    }
    return tableRegex;
}

+ (NSRegularExpression *)headRegex {
    static NSRegularExpression *_headRegex = nil;
    if (_headRegex == nil) {
        NSError *error;
        _headRegex = [[NSRegularExpression alloc]
            initWithPattern:@"<thead.*?>(.*?)<\\/thead>"
                    options: NSRegularExpressionCaseInsensitive |
                             NSRegularExpressionDotMatchesLineSeparators
                        error:&error];
    }
    return _headRegex;
}

+ (NSRegularExpression *)bodyRegex {
    static NSRegularExpression *_bodyRegex = nil;
    if (_bodyRegex == nil) {
        NSError *error;
        _bodyRegex = [[NSRegularExpression alloc]
            initWithPattern:@"<tbody.*?>(.*?)<\\/tbody>"
                    options:NSRegularExpressionCaseInsensitive |
                            NSRegularExpressionDotMatchesLineSeparators
                      error:&error];
    }
    return _bodyRegex;
}

+ (NSRegularExpression *)rowRegex {
    static NSRegularExpression *_rowRegex = nil;
    if (_rowRegex == nil) {
        NSError *error;
        _rowRegex = [[NSRegularExpression alloc]
            initWithPattern:@"<tr.*?>(.*?)<\\/tr>"
                    options:NSRegularExpressionCaseInsensitive |
                            NSRegularExpressionDotMatchesLineSeparators
                      error:&error];
    }
    return _rowRegex;
}

+ (NSRegularExpression *)dataRegex {
    static NSRegularExpression *_dataRegex = nil;
    if (_dataRegex == nil) {
        NSError *error;
        _dataRegex = [[NSRegularExpression alloc]
            initWithPattern:@"<td><a.*?>(.*?)<\\/a><\\/td>|<td>(.*?)<\\/td>"
                    options:NSRegularExpressionCaseInsensitive |
                            NSRegularExpressionDotMatchesLineSeparators
                      error:&error];
    }
    return _dataRegex;
}

#pragma mark - data parsing and instantiation
- (BOOL)buildFromText:(NSString *)text error:(NSError **)ppError
{
    NSUInteger colCount = 0;
    NSMutableArray *data = [[NSMutableArray alloc] init];

    NSString *tableText = [self strMatch:[NativeTableModel regex] text:text index:1];
    if (!tableText) {
        [self buildError:ErrorParseTable reason:@"Couln't parse table" ptrToError:ppError];
        return NO;
    }
    NSString *head = [self strMatch:[NativeTableModel headRegex] text:tableText index:1];
    if (head) {
        NSString *headRow = [self strMatch:[NativeTableModel rowRegex] text:head index:1];
        if (!headRow) {
            [self buildError:ErrorParseHead reason:@"Couln't parse head" ptrToError:ppError];
            return NO;
        }
        NSArray *headCells = [self strsMatch:[NativeTableModel dataRegex] text:headRow index:2];
        if (!headCells || ![headCells count]) {

            [self buildError:ErrorParseHeadCells
                      reason:@"Couln't parse head cells"
                  ptrToError:ppError];

            return NO;
        }
        colCount = [headCells count];
        NSMutableArray *headArray = [[NSMutableArray alloc] init];
        for (NSString *headCell in headCells) {
            [headArray addObject:headCell];
        }
        [data addObject:headArray];
        self.hasHeader = YES;
    }

    NSString *body = [self strMatch:[NativeTableModel bodyRegex] text:tableText index:1];
    if (!body) {
        [self buildError:ErrorParseBody reason:@"Couldn't parse body" ptrToError:ppError];
        return NO;
    }
    NSArray *bodyRows = [self strsMatch:[NativeTableModel rowRegex] text:body index:1];
    if (!bodyRows || ![bodyRows count]) {

        [self buildError:ErrorParseBodyRows
                  reason:@"Couldn't parse body rows"
              ptrToError:ppError];

        return NO;
    }
    for (NSString *bodyRow in bodyRows) {
        NSArray *bodyCells = [self strsMatch:[NativeTableModel dataRegex] text:bodyRow index:1];
        if (bodyCells == nil || ([bodyCells count] != colCount)) {

            [self buildError:ErrorParseBodyCells
                      reason:@"Couldn't parse body cells"
                  ptrToError:ppError];

            return NO;
        }
        NSMutableArray *dataRow = [[NSMutableArray alloc] init];
        for (NSString *bodyCell in bodyCells) {
            [dataRow addObject:bodyCell];
        }

        // Add the row to the array of arrays.
        [data addObject:dataRow];
    }

    self.rowData = data;
    return YES;
}

- (void) buildError:(NativeTableError)errorCode
             reason:(NSString *)reason
              ptrToError:(NSError **)ppError
{
    if (ppError) {
        NSDictionary *userInfo = @{@"Error reason" : reason};
        *ppError = [NSError errorWithDomain:errDomain code:errorCode userInfo:userInfo];
    }
}

- (NSString *)strMatch:(NSRegularExpression *)pattern
                  text:(NSString *)text
                 index:(NSUInteger)index {
    @try {
        NSString *textToSearch = [text stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];

        NSTextCheckingResult *result =
            [pattern firstMatchInString:textToSearch
                                options:0
                                  range:NSMakeRange(0, [textToSearch length])];

        if (result.range.location == NSNotFound) {
            return nil;
        }

        NSRange matchGroupRange = [result rangeAtIndex:index];
        NSString *matchedResult = [textToSearch substringWithRange:matchGroupRange];
        if (!matchedResult || [matchedResult isEqualToString:@""]) {
            return nil;
        }
        return matchedResult;
    } @catch (NSException *ex) {
        return nil;
    }
}

- (NSArray *)strsMatch:(NSRegularExpression *)pattern
                  text:(NSString *)text
                 index:(NSUInteger) index
{
    @try {
        NSMutableArray *strings = [[NSMutableArray alloc] init];
        NSString *textToSearch = [text stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];

        NSArray *matches = [pattern matchesInString:textToSearch
                                            options:0
                                              range:NSMakeRange(0, [textToSearch length])];

        for (NSTextCheckingResult *match in matches) {
            NSRange matchRange = [match rangeAtIndex:index];
            if (matchRange.location == NSNotFound) {
                matchRange = [match rangeAtIndex:index + 1];
                if (matchRange.location == NSNotFound) {
                    return nil;
                }
            }

            NSString *str = [textToSearch substringWithRange:matchRange];
            [strings addObject:str];
        }
        return strings;
    } @catch (NSException *ex) {
        return nil;
    }
}

#pragma mark - post-data-created instance methods
- (NSUInteger)numberOfRows
{
    return [self.rowData count];
}

- (NSUInteger)numberOfColumns
{
    NSArray *firstRow = [self.rowData objectAtIndex:0];
    NSUInteger count = [firstRow count];
    return count;
}

- (NSString *)dataAtLocation:(NSUInteger)rowIndex column:(NSUInteger)columnIndex {
    NSArray *row = [self.rowData objectAtIndex:rowIndex];
    return [row objectAtIndex:columnIndex];
}

- (cellType)getCellType:(NSUInteger)row
                 column:(NSUInteger)column
{
    if (row == 0) {
        return columnHeading;
    }
    return ((row + 1) %2) == 0 ? dataEven : dataOdd;
}

+ (void)buildFixedAndNonFixedModels:(NativeTableModel *)srcModel
                   fixedColumnModel:(NativeTableModel **)ppFixedColumnModel
                nonFixedColumnModel:(NativeTableModel **)ppNonFixedColumnModel
                   fixedColumnCount:(NSUInteger)fixedColumnCount
{
    NSUInteger rowCount = [srcModel numberOfRows];
    NSUInteger colCount = [srcModel numberOfColumns];
    *ppFixedColumnModel = [NativeTableModel new];
    *ppNonFixedColumnModel = [NativeTableModel new];

    NSMutableArray *fixedOuterArray =
        [[NSMutableArray alloc] initWithCapacity:rowCount];

    NSMutableArray *nonFixedOuterArray = [[NSMutableArray alloc] initWithCapacity:rowCount];

    NSRange fixedRange = NSMakeRange(0, fixedColumnCount);
    NSRange nonFixedRange = NSMakeRange(fixedColumnCount, colCount - fixedColumnCount);

    for (NSUInteger i = 0; i < rowCount; i++) {
        NSArray *srcRow = [srcModel.rowData objectAtIndex:i];

        NSArray *fixedRow = [srcRow subarrayWithRange:fixedRange];
        NSArray *nonFixedRow = [srcRow subarrayWithRange:nonFixedRange];

        [fixedOuterArray insertObject:fixedRow atIndex:i];
        [nonFixedOuterArray insertObject:nonFixedRow atIndex:i];
    }
    (*ppFixedColumnModel).rowData = fixedOuterArray;
    (*ppNonFixedColumnModel).rowData = nonFixedOuterArray;
}


// N.B. This is scaffolding; will be removed as soon as its not needed, or possibly converted
// to a unit test.  Code reviewers:  please don't make me take it out prematurely; it's useful
// to me :).
- (NSArray *)generateRowData
{
    self.hasHeader = YES;
    return
            @[
                    @[@"#",  @"PLAYER",          @"POS",@"TEAM",     @"BYE",@"INJURY",@"AGE",@"ADP",@"AAV",@"PROCJECTED PTS.", @"BOX SCORE"],
                    @[@"1",  @"Antonio Brown",   @"WR", @"Steelers", @"11", @"",    @"27",  @"3.88",  @"$40", @"371.5", @"2363"],
                    @[@"2",  @"Odel Beckham",    @"WR", @"Jets",     @"11", @"Pro", @"22",  @"5.23",  @"$39", @"371.2", @"2360"],
                    @[@"3",  @"Jamaal Charles",  @"RB", @"Chiefs",   @"11", @"",    @"28",  @"8.92",  @"$33", @"313.3", @"2354"],
                    @[@"4",  @"Le'Veon Bell",    @"RB", @"Steelers", @"11", @"Sus", @"22",  @"3.96",  @"$40", @"311.0", @"2341"],
                    @[@"5",  @"Matt Forte",      @"RB", @"Bears",    @"7",  @"",    @"29",  @"19.93", @"$27", @"291.0", @"2131"],
                    @[@"6",  @"Rob Gronkowski",  @"TE", @"Patriots", @"4",  @"",    @"26",  @"9.73",  @"$37", @"303.9", @"2116"],
                    @[@"7",  @"Eddie Lacy",      @"RB", @"Packers",  @"7",  @"",    @"24",  @"7.96",  @"$35", @"288.3", @"2104"],
                    @[@"8",  @"Adrian Peterson", @"RB", @"Vikings",  @"5",  @"",    @"30",  @"11.21", @"$30", @"284.2", @"2063"],
                    @[@"9",  @"Julio Jones",     @"WR", @"Eagles",   @"10", @"",    @"26",  @"7.46",  @"$37", @"339.9", @"2047"],
                    @[@"10", @"C.J. Anderson",   @"RB", @"Broncos",  @"7",  @"",    @"24",  @"21.96", @"$26", @"280.5", @"2026"],
                    @[@"11", @"Arian Foster",    @"RB", @"Texans",   @"9",  @"",    @"28",  @"25.22", @"$26", @"277.1", @"1992"],
                    @[@"12", @"Marshawn Lynch",  @"RB", @"Seahawks", @"9",  @"",    @"29",  @"20.83", @"$29", @"275.8", @"1979"]
            ];
}

@end