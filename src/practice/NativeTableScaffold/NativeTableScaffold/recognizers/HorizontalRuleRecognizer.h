//
//  HorizontalRuleRecognizer.h
//  Scout
//
//  Created by Don Clore on 7/27/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//
#ifndef Scout_HorizontalRuleRecognizer_h
#define Scout_HorizontalRuleRecognizer_h

#import "ScoutRecognizer.h"

@interface HorizontalRuleRecognizer : ScoutRecognizer
@end

#endif
