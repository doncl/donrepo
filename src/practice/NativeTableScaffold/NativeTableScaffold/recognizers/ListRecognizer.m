//
//  ListRecognizer.m
//  Scout
//
//  Created by Avi Finkel on 1/6/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "ListRecognizer.h"
#import "ListParagraphWrapper.h"

@implementation ListRecognizer

- (id) init
{
    self = [super init];
    if (self) {
        self.regex = [[NSRegularExpression alloc] initWithPattern:@"<[ou]l[^>]*?>" options:NSRegularExpressionCaseInsensitive error:nil];
    }
    return self;
}

- (void)modifyWrappers:(WrapperCollection *)wrappers forMatch:(NSString *)matched
{
    ListParagraphWrapper *list = [[ListParagraphWrapper alloc] init];
    list.ordered = [matched rangeOfString:@"<ol"].location == 0;
    [wrappers appendWrapper:list];
}

@end
