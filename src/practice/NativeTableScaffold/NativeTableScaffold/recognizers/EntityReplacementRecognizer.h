//
//  EntityReplacementRecognizer.h
//  Scout
//
//  Created by Avi Finkel on 12/29/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import "ScoutRecognizer.h"

@interface EntityReplacementRecognizer : ScoutRecognizer

@property NSString *replacement;

- (id)initWithEntity: (NSString *)entity replacement:(NSString *)replacement;
- (id) initWithRegex:(NSRegularExpression *)entity replacement:(NSString *)replacement;

@end
