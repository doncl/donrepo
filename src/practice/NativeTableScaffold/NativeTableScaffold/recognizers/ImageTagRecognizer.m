//
//  ImageTagRecognizer.m
//  Scout
//
//  Created by Sonu on 1/27/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "ImageTagRecognizer.h"
#import "TextParagraphWrapper.h"
#import "InlineImageWrapper.h"
#import "NSString+URLParsing.h"

@implementation ImageTagRecognizer


- (id) init
{
    self = [super init];
    if (self) {
        self.regex = [[NSRegularExpression alloc] initWithPattern:@"<img[^>]*?>" options:NSRegularExpressionCaseInsensitive error:nil];
    }
    return self;
}

- (void)modifyWrappers:(WrapperCollection *)wrappers forMatch:(NSString *)matched
{
    InlineImageWrapper *wrapper = [InlineImageWrapper new];
    wrapper.url = [matched parseUrlFromTagAfterString:@"src="];
    [wrappers appendWrapper:wrapper];
}



@end
