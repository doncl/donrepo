//
//  BalancedRangeRecognizer.h
//  Scout
//
//  Created by Avi Finkel on 1/1/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "ScoutRecognizer.h"

#import "TextRangeType.h"

@interface BalancedRangeRecognizer : ScoutRecognizer

+ (NSArray *)generateRecognizerPairForTagName: (NSString *)tagName withRangeType: (TextRangeType)rangeType;
+ (NSArray *)generateRecognizerPairForTagName: (NSString *)tagName withRangeType: (TextRangeType)rangeType withClass: (Class)recognizerClass;

@property BOOL begin;

@end
