//
//  ThreadQuoteRecognizer.h
//  Scout
//
//  Created by Avi Finkel on 1/29/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "BalancedRangeRecognizer.h"

@interface ThreadQuoteRecognizer : BalancedRangeRecognizer

+ (NSArray *)generateRecognizerPair;

@end
