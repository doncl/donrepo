//
//  HorizontalRuleRecognizer.m
//  Scout
//
//  Created by Don Clore on 7/27/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HorizontalRuleRecognizer.h"
#import "HorizontalRuleWrapper.h"

@implementation HorizontalRuleRecognizer

- (id) init
{
    self = [super init];
    if (self) {
        self.regex = [[NSRegularExpression alloc] initWithPattern:@"<hr[^>]*?>" options:NSRegularExpressionCaseInsensitive error:nil];
    }
    return self;
}

- (void)modifyWrappers:(WrapperCollection *)wrappers forMatch:(NSString *)matched
{
    [wrappers appendWrapper:[[HorizontalRuleWrapper alloc] init]];
}

@end