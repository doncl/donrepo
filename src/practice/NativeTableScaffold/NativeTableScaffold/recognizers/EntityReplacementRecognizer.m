//
//  EntityReplacementRecognizer.m
//  Scout
//
//  Created by Avi Finkel on 12/29/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import "EntityReplacementRecognizer.h"
#import "TextParagraphWrapper.h"

@implementation EntityReplacementRecognizer

- (id) initWithEntity:(NSString *)entity replacement:(NSString *)replacement
{
    if (entity) {
        return [self initWithRegex:[[NSRegularExpression alloc] initWithPattern:entity options:NSRegularExpressionIgnoreMetacharacters error:nil] replacement:replacement];
    }
    else {
        return [self initWithRegex:nil replacement:replacement];
    }
}

- (id) initWithRegex:(NSRegularExpression *)regex replacement:(NSString *)replacement
{
    self = [super init];
    if (self) {
        if (regex) {
            self.regex = regex;
        }
        self.replacement = replacement;
    }
    return self;
}

- (void)modifyWrappers:(WrapperCollection *)wrappers forMatch:(NSString *)matched
{
    [((TextParagraphWrapper *)[wrappers lastWrapperOrAppendOfType:[TextParagraphWrapper class]]) appendText:self.replacement];
}

@end
