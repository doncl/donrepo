//
//  BalancedRangeRecognizer.m
//  Scout
//
//  Created by Avi Finkel on 1/1/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "BalancedRangeRecognizer.h"
#import "TextParagraphWrapper.h"

@interface BalancedRangeRecognizer ()

- (id) initWithTagName: (NSString *)tagName rangeType:(TextRangeType)rangeType toBegin:(BOOL)begin;

@property TextRangeType type;

@end

@implementation BalancedRangeRecognizer

+ (NSArray *)generateRecognizerPairForTagName:(NSString *)tagName withRangeType:(TextRangeType)rangeType
{
    return [self generateRecognizerPairForTagName:tagName withRangeType:rangeType withClass:[BalancedRangeRecognizer class]];
}

+ (NSArray *)generateRecognizerPairForTagName:(NSString *)tagName withRangeType:(TextRangeType)rangeType withClass:(Class)recognizerClass
{
    return @[
        [[recognizerClass alloc] initWithTagName:tagName rangeType:rangeType toBegin:YES],
        [[recognizerClass alloc] initWithTagName:tagName rangeType:rangeType toBegin:NO],
    ];
}

- (id) initWithTagName:(NSString *)tagName rangeType:(TextRangeType)rangeType toBegin:(BOOL)begin
{
    self = [super init];
    if (self) {
        self.type = rangeType;
        self.begin = begin;
        NSString *pattern;
        if (begin) {
            pattern = @"<";
            pattern = [pattern stringByAppendingString:[NSRegularExpression escapedPatternForString:tagName]];
            pattern = [pattern stringByAppendingString:@"[^>]*?>"];
        }
        else {
            pattern = @"</";
            pattern = [pattern stringByAppendingString:[NSRegularExpression escapedPatternForString:tagName]];
            pattern = [pattern stringByAppendingString:@">"];
        }
        self.regex = [[NSRegularExpression alloc] initWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
    }
    return self;
}

- (void)modifyWrappers:(WrapperCollection *)wrappers forMatch:(NSString *)matched
{
    if (self.begin) {
        // Here we create one if we don't already have one.
        [((TextParagraphWrapper *)[wrappers lastWrapperOrAppendOfType:[TextParagraphWrapper class]]) beginRangeOfType:self.type];
    }
    else {
        // Here we don't
        [((TextParagraphWrapper *)[wrappers lastWrapperIfOfType:[TextParagraphWrapper class]]) endRangeOfType:self.type];
    }
}

@end
