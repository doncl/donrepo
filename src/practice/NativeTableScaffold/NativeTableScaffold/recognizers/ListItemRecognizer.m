//
//  ListItemRecognizer.m
//  Scout
//
//  Created by Avi Finkel on 1/6/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "ListItemRecognizer.h"
#import "ListParagraphWrapper.h"

@implementation ListItemRecognizer

- (id) init
{
    self = [super init];
    if (self) {
        self.regex = [[NSRegularExpression alloc] initWithPattern:@"<li[^>]*?>" options:NSRegularExpressionCaseInsensitive error:nil];
    }
    return self;
}

- (void)modifyWrappers:(WrapperCollection *)wrappers forMatch:(NSString *)matched
{
    [((ListParagraphWrapper *)[wrappers lastWrapperIfOfType:[ListParagraphWrapper class]]) beginNewListItem];
}

@end
