//
// Created by Don Clore on 9/2/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageTagRecognizer.h"


@interface TableCellImageRecognizer : ImageTagRecognizer

@end