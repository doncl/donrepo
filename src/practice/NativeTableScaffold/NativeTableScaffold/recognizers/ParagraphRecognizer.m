//
//  NewParagraphRecognizer.m
//  Scout
//
//  Created by Avi Finkel on 12/31/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import "ParagraphRecognizer.h"
#import "TextParagraphWrapper.h"

@implementation ParagraphRecognizer

- (id) init
{
    self = [super init];
    if (self) {
        self.regex = [[NSRegularExpression alloc] initWithPattern:@"<(div|p)[^>]*?>" options:NSRegularExpressionCaseInsensitive error:nil];
    }
    return self;
}

- (void)modifyWrappers:(WrapperCollection *)wrappers forMatch:(NSString *)matched
{
    TextParagraphWrapper *lastText = (TextParagraphWrapper *)[wrappers lastWrapperIfOfType:[TextParagraphWrapper class]];
    if (!lastText || lastText.trimmedText.length) {
        // We only want to make a new one if we actually need to
        [wrappers appendWrapper:[[TextParagraphWrapper alloc] init]];
    }
}

@end
