//
//  ScoutRecognizer.m
//  Scout
//
//  Created by Avi Finkel on 12/28/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import "ScoutRecognizer.h"
#import "NestingWrapper.h"

@implementation ScoutRecognizer

- (void) modifyWrappers:(WrapperCollection *)wrappers forMatch:(NSString *)matched
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)] userInfo:nil];
}



@end
