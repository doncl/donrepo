//
//  LineBreakRecognizer.m
//  Scout
//
//  Created by Avi Finkel on 1/26/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "LineBreakRecognizer.h"
#import "TextParagraphWrapper.h"

@implementation LineBreakRecognizer

- (id) init
{
    self = [super init];
    if (self) {
        self.regex = [[NSRegularExpression alloc] initWithPattern:@"<br[^>]*?>" options:NSRegularExpressionCaseInsensitive error:nil];
    }
    return self;
}

- (void)modifyWrappers:(WrapperCollection *)wrappers forMatch:(NSString *)matched
{
    [wrappers appendWrapper:[[TextParagraphWrapper alloc] init]];
}

@end
