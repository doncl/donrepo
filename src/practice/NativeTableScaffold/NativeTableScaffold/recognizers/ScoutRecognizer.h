//
//  ScoutRecognizer.h
//  Scout
//
//  Created by Avi Finkel on 12/28/14.
//  Copyright (c) 2014 Scout. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ScoutDataWrapper.h"
#import "WrapperCollection.h"
#import "ParseContext.h"

@interface ScoutRecognizer : NSObject

@property NSRegularExpression *regex;
@property ParseContext *context;

- (void)modifyWrappers: (WrapperCollection *)wrappers forMatch: (NSString *)matched;

@end
