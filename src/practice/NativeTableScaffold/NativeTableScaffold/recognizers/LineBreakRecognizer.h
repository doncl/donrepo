//
//  LineBreakRecognizer.h
//  Scout
//
//  Created by Avi Finkel on 1/26/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScoutRecognizer.h"

@interface LineBreakRecognizer : ScoutRecognizer

@end
