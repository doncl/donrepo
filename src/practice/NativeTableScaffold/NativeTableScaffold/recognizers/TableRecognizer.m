//
// Created by Don Clore on 8/7/15.
// Copyright (c) 2015 Scout. All rights reserved.
//
#import "TableRecognizer.h"
#import "TableWrapper.h"

@implementation TableRecognizer

- (NSRegularExpression *)regex {
    return nil;
}

- (void)modifyWrappers:(WrapperCollection *)wrappers forMatch:(NSString *)matched
{
    [wrappers appendWrapper:[[TableWrapper alloc] initWithText:matched]];
}
@end