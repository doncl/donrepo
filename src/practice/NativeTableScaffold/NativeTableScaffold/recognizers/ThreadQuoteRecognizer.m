//
//  ThreadQuoteRecognizer.m
//  Scout
//
//  Created by Avi Finkel on 1/29/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "ThreadQuoteRecognizer.h"
#import "ThreadQuoteWrapper.h"

@implementation ThreadQuoteRecognizer

+ (NSArray *)generateRecognizerPair
{
    return [super generateRecognizerPairForTagName:@"blockquote" withRangeType:0 withClass:[ThreadQuoteRecognizer class]];
}

- (void)modifyWrappers:(WrapperCollection *)wrappers forMatch:(NSString *)matched
{
    if (self.begin) {
        [wrappers appendWrapper:[[ThreadQuoteWrapper alloc] init]];
    }
    else {
        ((ThreadQuoteWrapper *)[wrappers deepestNestingWrapperIfOfType:[ThreadQuoteWrapper class]]).open = NO;
    }
}

@end