//
// Created by Don Clore on 9/2/15.
// Copyright (c) 2015 Beer Barrel Poker Studios. All rights reserved.
//

#import "TableCellImageRecognizer.h"
#import "NSString+URLParsing.h"
#import "TableCellImageWrapper.h"

@implementation TableCellImageRecognizer
- (void)modifyWrappers:(WrapperCollection *)wrappers forMatch:(NSString *)matched
{
    TableCellImageWrapper *wrapper=[[TableCellImageWrapper alloc] init];
    wrapper.url = [matched parseUrlFromTagAfterString:@"src="];
    wrapper.attributedSize = [self lookForAttributedSize:matched];

    [wrappers appendWrapper:wrapper];
}

- (CGSize)lookForAttributedSize:(NSString *)text
{
    static NSRegularExpression *heightRegex = nil;
    static NSRegularExpression *widthRegex = nil;

    if (!heightRegex) {
        heightRegex =
            [[NSRegularExpression alloc]
                initWithPattern:@"<img.*?height=\"(\\d+?)\""
                        options:NSRegularExpressionCaseInsensitive |
                            NSRegularExpressionDotMatchesLineSeparators
                          error:nil];
    }

    if (!widthRegex) {
        widthRegex =
            [[NSRegularExpression alloc]
                initWithPattern:@"<img.*?width=\"(\\d+?)\""
                        options:NSRegularExpressionCaseInsensitive |
                            NSRegularExpressionDotMatchesLineSeparators
                          error:nil];
    }

    CGFloat width = [self matchFloat:widthRegex text:text];
    CGFloat height = [self matchFloat:heightRegex text:text];
    return CGSizeMake(width, height);
}

- (CGFloat)matchFloat:(NSRegularExpression *)pattern text:(NSString *)text
{
    @try {
        NSString *textToSearch = [text stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];

        NSRange r = NSMakeRange(0, [textToSearch length]);

        NSTextCheckingResult *result =
            [pattern firstMatchInString:textToSearch options:nil range:r];

        if (result.range.location == NSNotFound) {
            return 0.0;
        }

        if ([result numberOfRanges] < 2) {
            return 0.0;
        }
        NSRange matchGroupRange = [result rangeAtIndex:1];
        NSString *matchedResult = [textToSearch substringWithRange:matchGroupRange];
        if (!matchedResult || [matchedResult isEqualToString:@""]) {
            return 0.0;
        }
        return [matchedResult floatValue];
    } @catch (NSException* exception) {
        return 0.0;
    }
}

@end