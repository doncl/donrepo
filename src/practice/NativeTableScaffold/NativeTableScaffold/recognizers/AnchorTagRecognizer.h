//
//  AnchorTagRecognizer.h
//  Scout
//
//  Created by Avi Finkel on 1/1/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "BalancedRangeRecognizer.h"

@interface AnchorTagRecognizer : BalancedRangeRecognizer

+ (NSArray *)generateRecognizerPair;

@end
