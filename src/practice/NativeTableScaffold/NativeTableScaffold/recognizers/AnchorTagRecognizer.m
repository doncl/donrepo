//
//  AnchorTagRecognizer.m
//  Scout
//
//  Created by Avi Finkel on 1/1/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "AnchorTagRecognizer.h"
#import "TextParagraphWrapper.h"
#import "NSString+URLParsing.h"

@implementation AnchorTagRecognizer

+ (NSArray *)generateRecognizerPair
{
    return [super generateRecognizerPairForTagName:@"a" withRangeType:LINK withClass:[AnchorTagRecognizer class]];
}

- (void)modifyWrappers:(WrapperCollection *)wrappers forMatch:(NSString *)matched
{
    [super modifyWrappers:wrappers forMatch:matched];
    if (self.begin) {
        NSURL *url = [matched parseUrlFromTagAfterString:@"href=" forSubdomain:self.context.subdomain];
        [((TextParagraphWrapper *)[wrappers lastWrapperIfOfType:[TextParagraphWrapper class]]) addUrlToLastLinkRange:url];
    }
}

@end
