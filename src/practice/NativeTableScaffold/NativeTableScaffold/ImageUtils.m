//
//  ImageUtils.m
//  Scout
//
//  Created by Sonu on 4/6/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "ImageUtils.h"

@implementation ImageUtils

+ (CGSize)resizeImageWithWidth: (CGFloat)width height:(CGFloat)height frameWidth:(CGFloat)frameWidth
{
    if(width > frameWidth){
        CGFloat reductionPercentage = frameWidth/width;
        width = frameWidth;
        height = height * reductionPercentage;
    }
    
    return CGSizeMake(width, height);
}

//http://ajourneywithios.blogspot.com/2012/03/resizing-uiimage-in-ios.html
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
