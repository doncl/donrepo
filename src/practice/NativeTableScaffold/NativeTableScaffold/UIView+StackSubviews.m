//
//  UIView+StackSubviews.m
//  Scout
//
//  Created by Avi Finkel on 1/29/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import "UIView+StackSubviews.h"

@implementation UIView (StackSubviews)

- (void)stackSubviewsWithSidePadding:(CGFloat)sidePadding withVerticalPadding:(CGFloat)verticalPadding
{
    CGFloat height;
    [self stackSubviewsWithSidePadding:sidePadding withVerticalPadding:verticalPadding exceptFor:nil producingHeight:&height];
}

- (void)stackSubviewsWithSidePadding:(CGFloat)sidePadding withVerticalPadding:(CGFloat)verticalPadding exceptFor:(NSSet *)ignoreViews
{
    CGFloat height;
    [self stackSubviewsWithSidePadding:sidePadding withVerticalPadding:verticalPadding exceptFor:ignoreViews producingHeight:&height];
}

- (void)stackSubviewsWithSidePadding:(CGFloat)sidePadding withVerticalPadding:(CGFloat)verticalPadding exceptFor:(NSSet *)ignoreViews producingHeight:(CGFloat *)height
{
    *height = verticalPadding;
    for (UIView *subview in self.subviews) {
        if (![ignoreViews containsObject:subview]) {
            subview.frame = CGRectMake(sidePadding, *height, subview.frame.size.width, subview.frame.size.height);
            [subview setNeedsDisplay];
            *height += subview.frame.size.height;
            *height += verticalPadding;
        }
    }
}

- (void)addSubviews:(NSArray *)views
{
    for (UIView *view in views) {
        [self addSubview:view];
    }
}

- (void)removeSubviews:(NSArray *)views
{
    for (UIView* view in views){
        [view removeFromSuperview];
    }
}

@end
