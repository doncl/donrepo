//
//  NSNumber+BuildImageUrl.h
//  Scout
//
//  Created by Avi Finkel on 1/15/15.
//  Copyright (c) 2015 Scout. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSNumber (BuildImageUrl)

- (NSURL *)buildImageUrl;
- (NSString*)buildPlayerImageURL;

@end
