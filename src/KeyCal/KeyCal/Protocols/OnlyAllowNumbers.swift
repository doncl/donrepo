//
//  OnlyAllowNumbers.swift
//  KeyCal
//
//  Created by Don Clore on 12/17/19.
//  Copyright © 2019 Maven Coalition. All rights reserved.
//

import UIKit

protocol OnlyAllowNunbers: class {
  func allowThis(_ textField: UITextField, text: String) -> Bool
}

extension OnlyAllowNunbers {
  func allowThis(_ textField: UITextField, text: String) -> Bool {
    if let textFieldText = textField.text {
      let numberDecimalDotsInString = textFieldText.countInstances(of: ".")
      let numberDecimalDotsInReplacementString = text.countInstances(of: ".")
      if numberDecimalDotsInString + numberDecimalDotsInReplacementString > 1 {
        return false
      }
    }

    let aSet = CharacterSet(charactersIn:"0123456789.").inverted
    let compSepByCharInSet = text.components(separatedBy: aSet)
    let numberFiltered = compSepByCharInSet.joined(separator: "")
    
    return text == numberFiltered
  }
}

extension String {
  /// stringToFind must be at least 1 character.
  func countInstances(of stringToFind: String) -> Int {
    assert(!stringToFind.isEmpty)
    var count = 0
    var searchRange: Range<String.Index>?
    while let foundRange = range(of: stringToFind, options: [], range: searchRange) {
        count += 1
        searchRange = Range(uncheckedBounds: (lower: foundRange.upperBound, upper: endIndex))
    }
    return count
  }
}
