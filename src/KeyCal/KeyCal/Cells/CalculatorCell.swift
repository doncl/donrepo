//
//  CalculatorCell.swift
//  KeyCal
//
//  Created by Don Clore on 12/17/19.
//  Copyright © 2019 Maven Coalition. All rights reserved.
//

import UIKit

protocol CalculatorCellDelegate: class {
  var numericFieldWidth: CGFloat { get }
}

class CalculatorCell: UITableViewCell {
  static let id: String = "CalculatorCellId"
  #if targetEnvironment(macCatalyst)
  static let defaultNumericWidth: CGFloat = 90
  static let fontSize: CGFloat = 18.0
  static let horzBorder: CGFloat = 24.0
  static let vertPad: CGFloat = 12.0
  #else
  static let defaultNumericWidth: CGFloat = 60
  static let fontSize: CGFloat = 12.0
  static let horzBorder: CGFloat = 16.0
  static let vertPad: CGFloat = 8.0
  #endif
  
  
  let leftLabel: UILabel = UILabel()
  let numericLabel: UILabel = UILabel()
  var numericLabelWidth: NSLayoutConstraint = NSLayoutConstraint()
  
  weak var calcCellDelegate: CalculatorCellDelegate?
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    let textColor: UIColor
    if #available(iOS 13.0, *) {
      textColor = UIColor.label
    } else {
      textColor = UIColor.black
    }
    
    [leftLabel, numericLabel].forEach({
      $0.translatesAutoresizingMaskIntoConstraints = false
      contentView.addSubview($0)
      $0.textColor = textColor
      $0.font = UIFont.systemFont(ofSize: CalculatorCell.fontSize)
    })
    
    leftLabel.textAlignment = .left
    numericLabel.textAlignment = .right
    
    numericLabelWidth = numericLabel.widthAnchor.constraint(equalToConstant: CalculatorCell.defaultNumericWidth)
    NSLayoutConstraint.activate(constraints: [
      numericLabelWidth,
      leftLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: CalculatorCell.horzBorder),
      leftLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
    
      numericLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -CalculatorCell.horzBorder),
      numericLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
    ], andSetPriority: UILayoutPriority(rawValue: 999))
      
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension CalculatorCell: UITextFieldDelegate, OnlyAllowNunbers {
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    return allowThis(textField, text: string)
  }
}

extension NSLayoutConstraint {
  class func activate(constraints: [NSLayoutConstraint], andSetPriority priority: UILayoutPriority) {
    constraints.forEach {
      $0.priority = priority
      $0.isActive = true
    }
  }
}
