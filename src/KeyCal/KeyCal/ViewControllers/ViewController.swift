//
//  ViewController.swift
//  KeyCal
//
//  Created by Don Clore on 12/17/19.
//  Copyright © 2019 Maven Coalition. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  enum Section: Int, CaseIterable {
   case keyboardLengths = 0
   case tailWidths = 1
   case cutouts = 2
  }

  @IBOutlet var naturalKeyHeadWidthTextField: UITextField!
  @IBOutlet var sharpKeyWidthTextField: UITextField!
  @IBOutlet var globalGapValueTextField: UITextField!
  @IBOutlet var table: UITableView!
  
  @IBOutlet var numberFieldWidth: NSLayoutConstraint!
  
  var model: KeyCalcModel = KeyCalcModel()
  
  lazy var formatter: NumberFormatter = {
    let f = NumberFormatter()
    f.numberStyle = .decimal
    f.minimumIntegerDigits = 1
    f.maximumFractionDigits = 2
    return f
  }()
    
  override func viewDidLoad() {
    super.viewDidLoad()
    table.dataSource = self
    table.delegate = self
    table.estimatedRowHeight = 44.0
    table.rowHeight = UITableView.automaticDimension
    table.register(CalculatorCell.self, forCellReuseIdentifier: CalculatorCell.id)
    if let nText = naturalKeyHeadWidthTextField.text {
      calculateModelFromTextFieldInputs(fieldInPlay: naturalKeyHeadWidthTextField, text: nText)
    }
   
    table.reloadData()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    #if targetEnvironment(macCatalyst)
    if let _ = navigationItem.rightBarButtonItems?.first {
      navigationItem.rightBarButtonItems?.remove(at: 0)
    }
    #endif
  }
  
  @IBAction func actionButtonTouched(_ sender: UIBarButtonItem) {
    let fileURL: URL = model.writeToFile()
    let items: [URL] = [fileURL]
    
    let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
    //ac.setValue("Key Calculations", forKey: "subject")
    
    present(ac, animated: true)
  }
  
}

extension ViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return Section.allCases.count
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let tableSection = ViewController.Section(rawValue: section) else {
      return 0
    }
    let data = model.sectionData(for: tableSection)
    return data.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let section = Section(rawValue: indexPath.section) else {
      fatalError("must never happen")
    }
    let tuple = model.tuple(atSection: section, andItem: indexPath.item)
    let cell = tableView.dequeueReusableCell(withIdentifier: CalculatorCell.id, for: indexPath) as! CalculatorCell
    cell.leftLabel.text = tuple.key
    cell.numericLabel.text = formatter.string(from: tuple.value)
    return cell
  }  
}

extension ViewController {
  private func calculateModelFromTextFieldInputs(fieldInPlay: UITextField, text: String) {
    
    guard let inputs = getInputs(fieldInPlay: fieldInPlay, text: text) else {
      return
    }
        
    model.naturalKeyHeadWidth = CGFloat(truncating: inputs.n)
    model.sharpKeyWidth = CGFloat(truncating: inputs.s)
    model.globalGap = CGFloat(truncating: inputs.g)
    
    model.reCalc()
    table.reloadData()
  }
  
  private func getInputs(fieldInPlay: UITextField, text: String) -> (n: NSNumber, s: NSNumber, g: NSNumber)? {
    let formatter: NumberFormatter = NumberFormatter()

    guard let inPlayNumber = formatter.number(from: text) else {
      return nil
    }
    if fieldInPlay.tag == 0 {
      guard let s = number(from: sharpKeyWidthTextField), let g = number(from: globalGapValueTextField) else {
        return nil
      }
      return (n: inPlayNumber, s: s, g: g)
    } else if fieldInPlay.tag == 1 {
      guard let n = number(from: naturalKeyHeadWidthTextField), let g = number(from: globalGapValueTextField) else {
        return nil
      }
      return (n: n, s: inPlayNumber, g: g)
    } else {
      guard let n = number(from: naturalKeyHeadWidthTextField), let s = number(from: sharpKeyWidthTextField) else {
        return nil
      }
      return (n: n, s: s, g: inPlayNumber)
    }
  }
  
  private func number(from textField: UITextField) -> NSNumber? {
    guard let text = textField.text else {
      return nil
    }
    let formatter = NumberFormatter()
    return formatter.number(from: text)
  }
}

extension ViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    guard let tableSection = Section(rawValue: section) else {
      fatalError("must never happen")
    }
    switch tableSection {
    case .keyboardLengths:
      return "Keyboard Lengths"
    case .tailWidths:
      return "Tail Widths"
    case .cutouts:
      return "Cutouts"
    }
  }
}

extension ViewController: UITextFieldDelegate, OnlyAllowNunbers {
  func textFieldDidBeginEditing(_ textField: UITextField) {
    print("\(#function) tab = \(textField.tag) ***********************************")
    textField.text = ""
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    print("\(#function) tab = \(textField.tag) ***********************************")
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                 replacementString string: String) -> Bool {

    
    let f = allowThis(textField, text: string)
    if f {
      let fullString = getReplacedString(from: textField, replacementString: string, inRange: range)
      calculateModelFromTextFieldInputs(fieldInPlay: textField, text: fullString)
    }
    return f
  }
  
  private func getReplacedString(from textField: UITextField, replacementString: String, inRange nsRange: NSRange) -> String {
    guard let text = textField.text, let range = Range(nsRange, in: text) else {
      return replacementString
    }
    return text.replacingCharacters(in: range, with: replacementString)
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    defer {
      if let text = textField.text {
        calculateModelFromTextFieldInputs(fieldInPlay: textField, text: text)
      }
    }
    // Try to find next responder
    if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
      nextField.becomeFirstResponder()
    } else {
      // Not found, so remove keyboard.
      textField.resignFirstResponder()
    }
    // Do not add a line break
    return false
  }
}
