//
//  KeyCalcModel.swift
//  KeyCal
//
//  Created by Don Clore on 12/17/19.
//  Copyright © 2019 Maven Coalition. All rights reserved.
//

import UIKit
import XMLParsing

enum ModelKeys: String, CaseIterable, Encodable {
  case quotientnaturalssharps = "Quotient naturals/sharps"
  case octavelength = "Octave length"
  case sixoctavelength = "6 octave keyboard length"
  case sixoctavelengthplus = "6 octave keyboard length+H (1 ext key)"
  case sevenoctavelength = "7 octave keyboard length"
  case sevenoctavelengthplus = "7 octave keyboard length+H (1 ext key)"
  case tailcde = "Tail width of C,D,E"
  case tailfgab = "Tail width of F,G,A,B"
  case rightcutoutcleftcutoute = "Right cutout of C, left cutout of E"
  case lefrrightcutoutd = "Left and right cutout of D"
  case rightcutoutfleftcutoutb = "Right cutout of F, left cutout of B"
  case leftcutoutgrightcutouta = "Left cutout of G, right cutout of A"
  case rightcutoutgleftcutouta = "Right cutout of G, left cutout of A"
}


struct SectionData: Encodable {
  private let keyOrder: [ModelKeys]
  private var dict: [ModelKeys: CGFloat]
  
  var count: Int {
    return keyOrder.count
  }
  
  func getCSV() -> String {
    var ret: String = ""
    let f = NumberFormatter()
    f.numberStyle = .decimal
    f.minimumIntegerDigits = 1
    f.maximumFractionDigits = 2

    for key in keyOrder {
      let cgFloatVal = getValue(forKey: key)
      let doubleVal = Double(cgFloatVal)
      let value = NSNumber(value: doubleVal)
      
      let row: String
      if let formattedValue = f.string(from: value) {
        row = "\"\(key.rawValue)\",\(formattedValue)\n"
      } else {
        row = "\"\(key.rawValue)\",\(cgFloatVal)\n"
      }

      ret.append(row)
    }
    return ret
  }
  
  mutating func set(value: CGFloat, forKey key: ModelKeys) {
    dict[key] = value
  }
  
  func key(forIndex index: Int) -> ModelKeys {
    guard index < keyOrder.count else {
      fatalError("must never happen")
    }
    return keyOrder[index]
  }
  
  func getValue(forKey key: ModelKeys) -> CGFloat {
    guard let val = dict[key] else {
      fatalError("must never happen")
    }
    return val
  }
  
  init(keyOrder: [ModelKeys], dict: [ModelKeys: CGFloat]) {
    self.keyOrder = keyOrder
    self.dict = dict
  }
}

struct KeyCalcModel: Encodable {
  
  var naturalKeyHeadWidth: CGFloat = 20.3
  var sharpKeyWidth: CGFloat = 9.7
  var globalGap: CGFloat = 0.8
  
  var count : Int {
    return ModelKeys.allCases.count
  }
  
  var keyboardLengths: SectionData = SectionData(
    keyOrder: [
      ModelKeys.quotientnaturalssharps,
      ModelKeys.octavelength,
      ModelKeys.sixoctavelength,
      ModelKeys.sixoctavelengthplus,
      ModelKeys.sevenoctavelength,
      ModelKeys.sevenoctavelengthplus,
    ],
    dict: [
      ModelKeys.quotientnaturalssharps: 2.09,
      ModelKeys.octavelength: 147.7,
      ModelKeys.sixoctavelength: 885.4,
      ModelKeys.sixoctavelengthplus: 906.5,
      ModelKeys.sevenoctavelength: 1033.1,
      ModelKeys.sevenoctavelengthplus: 1054.2,
    ]
  )
  
  var tailWidths: SectionData = SectionData(
    keyOrder: [
      ModelKeys.tailcde,
      ModelKeys.tailfgab,
    ],
    dict: [
      ModelKeys.tailcde: 13.3,
      ModelKeys.tailfgab: 12.43,
    ]
  )
  
  var cutouts: SectionData = SectionData(
    keyOrder: [
      ModelKeys.rightcutoutcleftcutoute,
      ModelKeys.lefrrightcutoutd,
      ModelKeys.rightcutoutfleftcutoutb,
      ModelKeys.leftcutoutgrightcutouta,
      ModelKeys.rightcutoutgleftcutouta,
    ],
    dict: [
    ModelKeys.rightcutoutcleftcutoute: 7.0,
    ModelKeys.lefrrightcutoutd: 3.50,
    ModelKeys.rightcutoutfleftcutoutb: 7.88,
    ModelKeys.leftcutoutgrightcutouta: 2.63,
    ModelKeys.rightcutoutgleftcutouta: 5.25,
   ]
  )
    
  func sectionData(for section: ViewController.Section) -> SectionData {
    switch section {
    case .keyboardLengths:
      return keyboardLengths
    case .tailWidths:
      return tailWidths
    case .cutouts:
      return cutouts
    }
  }
      
  func tuple(atSection section: ViewController.Section, andItem item: Int) -> (key: String, value: NSNumber) {
    let data = sectionData(for: section)
    let key = data.key(forIndex: item)
    let cgFloatVal = data.getValue(forKey: key)
    let doubleVal = Double(cgFloatVal)
    let value = NSNumber(value: doubleVal)
    return (key: key.rawValue, value: value)
  }
  
  mutating func reCalc() {
    guard sharpKeyWidth > 0, naturalKeyHeadWidth > 0, globalGap > 0 else { return }
    let quotientNaturalsSharps: CGFloat = naturalKeyHeadWidth / sharpKeyWidth
    keyboardLengths.set(value: quotientNaturalsSharps, forKey: ModelKeys.quotientnaturalssharps)
    let octavelength: CGFloat = 7.0 * (naturalKeyHeadWidth + globalGap)
    keyboardLengths.set(value: octavelength, forKey: ModelKeys.octavelength)
    
    let sixOctaveLength: CGFloat = (6.0 * octavelength) - globalGap
    keyboardLengths.set(value: sixOctaveLength, forKey: ModelKeys.sixoctavelength)
    
    let sixOctaveLengthPlus: CGFloat = (6.0 * octavelength) + naturalKeyHeadWidth
    keyboardLengths.set(value: sixOctaveLengthPlus, forKey: ModelKeys.sixoctavelengthplus)
    
    let sevenOctaveLength: CGFloat = (7.0 * octavelength) - globalGap
    keyboardLengths.set(value: sevenOctaveLength, forKey: ModelKeys.sevenoctavelength)
    
    let sevenOctaveLengthPlus: CGFloat = (7.0 * octavelength) + naturalKeyHeadWidth
    keyboardLengths.set(value: sevenOctaveLengthPlus, forKey: ModelKeys.sevenoctavelengthplus)
    
    let tailWidthCDE: CGFloat = naturalKeyHeadWidth - (((sharpKeyWidth + globalGap) * 2.0) / 3.0)
    tailWidths.set(value: tailWidthCDE, forKey: ModelKeys.tailcde)
    
    let tailWidthFGAB: CGFloat = naturalKeyHeadWidth - ((sharpKeyWidth + globalGap) * 0.75)
    tailWidths.set(value: tailWidthFGAB, forKey: ModelKeys.tailfgab)
    
    let rightCutoutCleftCutOutE: CGFloat = naturalKeyHeadWidth - tailWidthCDE
    cutouts.set(value: rightCutoutCleftCutOutE, forKey: ModelKeys.rightcutoutcleftcutoute)
    
    let leftAndRightCutoutOfD: CGFloat = (naturalKeyHeadWidth - tailWidthCDE) / 2.0
    cutouts.set(value: leftAndRightCutoutOfD, forKey: ModelKeys.lefrrightcutoutd)
    
    let rightCutoutFLeftCutoutB: CGFloat = naturalKeyHeadWidth - tailWidthFGAB
    cutouts.set(value: rightCutoutFLeftCutoutB, forKey: ModelKeys.rightcutoutfleftcutoutb)
    
    let rightCutoutGLeftCutoutA: CGFloat = (sharpKeyWidth + globalGap) / 2.0
    cutouts.set(value: rightCutoutGLeftCutoutA, forKey: ModelKeys.rightcutoutgleftcutouta)
    
    let leftCutoutGRightCutoutA: CGFloat = naturalKeyHeadWidth - tailWidthFGAB - rightCutoutGLeftCutoutA
    cutouts.set(value: leftCutoutGRightCutoutA, forKey: ModelKeys.leftcutoutgrightcutouta)
  }
  
  func encodeToData() -> Data? {
    do {
      let data = try XMLEncoder().encode(self, withRootKey: "keyboardData")
      return data
    } catch let error {
      print(error.localizedDescription)
      return nil
    }
  }
  
  func encodeToString() -> String? {
    guard let data = encodeToData() else {
      return nil
    }
    return String(data: data, encoding: .utf8)
  }
  
  func writeToFile() -> URL {
    let csv = makeCSV()
    let output = "KeyCalculations.csv"
    let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    let fileURL = dir.appendingPathComponent(output)
    
    do {
      try csv.write(to: fileURL, atomically: true, encoding: .utf8)
      return fileURL
    } catch let error {
      fatalError(error.localizedDescription)
    }
  }
  
  func makeCSV() -> String {
    var ret: String = ""
    ret += keyboardLengths.getCSV()
    ret += tailWidths.getCSV()
    ret += cutouts.getCSV()
    return ret
  }
}
