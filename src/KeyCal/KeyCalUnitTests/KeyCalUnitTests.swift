//
//  KeyCalUnitTests.swift
//  KeyCalUnitTests
//
//  Created by Don Clore on 1/2/20.
//  Copyright © 2020 Maven Coalition. All rights reserved.
//

import XCTest
@testable import Key_Dimensions_Calculator

class KeyCalUnitTests: XCTestCase {
  let  model: KeyCalcModel = KeyCalcModel()
  
  func testXMLEncoding() {
    guard let xmlData = model.encodeToData() else {
      XCTFail("couldn't encode model")
      return
    }
    
    model.writeToFile()
//    guard let modelString = model.encodeToString() else {
//      XCTFail("couldn't encode model to string ")
//      return
//    }
//    let output = "output.xml"
//    let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//    let fileURL = dir.appendingPathComponent(output)
//
//    do {
//      try modelString.write(to: fileURL, atomically: true, encoding: .utf8)
//    } catch let error {
//      XCTFail(error.localizedDescription)
//    }
  }
}
