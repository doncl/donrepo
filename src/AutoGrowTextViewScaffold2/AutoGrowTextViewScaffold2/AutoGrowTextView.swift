//
//  AutoGrowTextView.swift
//  AutoGrowTextViewScaffold
//
//  Created by Don Clore on 2/2/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class AutoGrowTextView: UIControl {
  
  private var heightConstraint : NSLayoutConstraint!
  private let _textVue : UITextView = UITextView()
  
  
  override var bounds: CGRect {
    get {
      return super.bounds
    }
    set {
      super.bounds = newValue
      _textVue.frame = newValue
      let size = _textVue.contentSize
      heightConstraint.constant = size.height
      layoutIfNeeded()
    }
  }
  
  var text : String {
    get {
      return _textVue.text
    }
    set {
      _textVue.text = newValue
    }
  }
  
  var attributedText : NSAttributedString {
    get {
      return _textVue.attributedText
    }
    set {
      _textVue.attributedText = newValue
    }
  }
  
  var delegate : UITextViewDelegate?
  
  override var backgroundColor: UIColor? {
    get {
      return _textVue.backgroundColor
    }
    set {
      _textVue.backgroundColor = newValue
    }
  }
  
  override var intrinsicContentSize: CGSize {
    return _textVue.contentSize
  }
  
  init() {
    super.init(frame: CGRect.zero)
    addSubview(_textVue)
    backgroundColor = .clear
    _textVue.isEditable = true
    _textVue.translatesAutoresizingMaskIntoConstraints = false
    
    _textVue.delegate = self
    
    NSLayoutConstraint.activate([
      _textVue.topAnchor.constraint(equalTo: self.topAnchor),
      _textVue.leadingAnchor.constraint(equalTo: self.leadingAnchor),
      _textVue.trailingAnchor.constraint(equalTo: self.trailingAnchor),
      self.bottomAnchor.constraint(equalTo: _textVue.bottomAnchor),
      ])
    
        heightConstraint = _textVue.heightAnchor.constraint(equalToConstant: 100.0)
        heightConstraint.isActive = true
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension AutoGrowTextView : UITextViewDelegate {
  func textViewDidChange(_ textView: UITextView) {
    let contentSize = textView.contentSize
    heightConstraint.constant = contentSize.height
    _textVue.setNeedsLayout()
    setNeedsLayout()
    
    guard let delegate = delegate, let fun = delegate.textViewDidChange else {
      return
    }
    fun(textView)
  }

  func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
    guard let delegate = delegate, let fun = delegate.textViewShouldBeginEditing else {
      return true
    }
    return fun(textView)
  }

  func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
    guard let delegate = delegate, let fun = delegate.textViewShouldEndEditing else {
      return true
    }
    return fun(textView)
  }

  func textViewDidBeginEditing(_ textView: UITextView) {
    guard let delegate = delegate, let fun = delegate.textViewDidBeginEditing else {
      return
    }
    fun(textView)
  }

  func textViewDidEndEditing(_ textView: UITextView) {
    guard let delegate = delegate, let fun = delegate.textViewDidEndEditing else {
      return
    }
    fun(textView)
  }

  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange,
    replacementText text: String) -> Bool {

    guard let delegate = delegate,
      let fun = delegate.textView(_:shouldChangeTextIn:replacementText:) else {
      return true
    }
    return fun(textView, range, text)
  }

  func textViewDidChangeSelection(_ textView: UITextView) {
    guard let delegate = delegate, let fun = delegate.textViewDidChangeSelection else {
      return
    }
    fun(textView)
  }

  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    guard let delegate = delegate, let fun = delegate.scrollViewDidScroll else {
      return
    }
    fun(scrollView)
  }

  func scrollViewDidZoom(_ scrollView: UIScrollView) {
    guard let delegate = delegate, let fun = delegate.scrollViewDidZoom else {
      return
    }
    fun(scrollView)
  }

  func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
    guard let delegate = delegate, let fun = delegate.scrollViewWillBeginDragging else {
      return
    }
    fun(scrollView)
  }
}
