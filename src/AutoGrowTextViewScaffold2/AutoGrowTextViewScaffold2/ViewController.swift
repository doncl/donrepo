//
//  ViewController.swift
//  AutoGrowTextViewScaffold2
//
//  Created by Don Clore on 2/2/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  let textView : AutoGrowTextView = AutoGrowTextView()
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    textView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(textView)
    
    textView.text = "Add some inital text"
    textView.delegate = self
    
    NSLayoutConstraint.activate([
      textView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100.0),
      textView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6, constant: 0),
      textView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      ])

    
  }

}

extension ViewController : UITextViewDelegate {
  func textViewDidChange(_ textView: UITextView) {
    print("foo")
  }
}
