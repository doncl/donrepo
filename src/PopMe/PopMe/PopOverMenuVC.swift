//
//  PopOverMenuVC.swift
//  PopMe
//
//  Created by Don Clore on 2/12/17.
//  Copyright © 2017 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit
import SnapKit
import QuartzCore

protocol PopOverMenuDelegate {
  func shareRoom()
  func shareDirectMessage()
  func shareSocial()
  func copyLink()
  func subscribe()
  func flagSpam()
  func flagAbuse()
  func delete()
}

fileprivate enum PopMenuValue : Int {
  case shareroom = 0
  case sharedm = 1
  case sharesocial = 2
  case copylink = 3
  case subscribe = 4
  case flagspam = 5
  case flagabuse = 6
  case delete = 7
}

class PopOverMenuVC: UIViewController {
  struct Constants {
    static let itemHeight : CGFloat = 45.0
    static let itemCount : Int = 8
    static let menuHeight : CGFloat = (itemHeight * CGFloat(itemCount))
  }

  var table : UITableView
  var delegate : PopOverMenuDelegate
  var menuItems : [MenuItemCell] = []
  fileprivate var delegateHandlers : [PopMenuValue : () -> ()] = [:]
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
    
  init(frame: CGRect, delegate : PopOverMenuDelegate) {
    self.delegate = delegate
    delegateHandlers[.shareroom] = delegate.shareRoom
    delegateHandlers[.sharedm] = delegate.shareDirectMessage
    delegateHandlers[.sharesocial] = delegate.shareSocial
    delegateHandlers[.copylink] = delegate.copyLink
    delegateHandlers[.subscribe] = delegate.subscribe
    delegateHandlers[.flagspam] = delegate.flagSpam
    delegateHandlers[.flagabuse] = delegate.flagAbuse
    delegateHandlers[.delete] = delegate.delete
    
    table = UITableView(frame: CGRect.zero, style: .plain)
    table.separatorStyle = .none
    table.separatorColor = .clear
    super.init(nibName: nil, bundle: nil)
    view.backgroundColor = .white
    view.frame = frame
    table.backgroundColor = .white
    
    view.translatesAutoresizingMaskIntoConstraints = false
    
    view.addSubview(table)
    table.snp.makeConstraints { (make) -> Void in
      make.edges.equalTo(view)
    }
    table.isScrollEnabled = false
    table.delegate = self
    table.dataSource = self
      }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    table.reloadData()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    if let parent = view.superview {
      parent.layer.cornerRadius = 0.0
    }
    setupMenuItems()

    super.viewWillAppear(animated)
  }
    
  fileprivate func setupMenuItems() {
    let width = view.frame.width
    
    var cell = MenuItemCell(title: "Share to a new Room", bold: true,
      cellWidth: width)
    menuItems.append(cell)
    
    cell = MenuItemCell(title: "Share in a Direct Message", bold: true, cellWidth: width)
    menuItems.append(cell)

    let twit = String.fontAwesomeIconStringForEnum(value: .FATwitter)
    let fb = String.fontAwesomeIconStringForEnum(value: .FAFacebookSquare)

    cell = MenuItemCell(title: "Share to ... ", bold: true, awesomeText: "\(twit) \(fb) ",
      cellWidth: width)
    menuItems.append(cell)
    
    cell = MenuItemCell(title: "Copy this link ", bold: true, rightIcon: #imageLiteral(resourceName: "link"),
      cellWidth: width)
    menuItems.append(cell)
    
    cell = MenuItemCell(leftIcon: #imageLiteral(resourceName: "notif"), title: "Turn on notification for this",
      cellWidth: width)
    menuItems.append(cell)
    
    cell = MenuItemCell(leftIcon: #imageLiteral(resourceName: "ic_flag"), title: "Report as spam", cellWidth: width)
    menuItems.append(cell)
    
    cell = MenuItemCell(leftIcon: #imageLiteral(resourceName: "ic_warning"), title: "Report as abuse", cellWidth : width)
    menuItems.append(cell)
    
    cell = MenuItemCell(leftIcon: #imageLiteral(resourceName: "ic_delete"), title: "Delete this...", cellWidth: width)
    menuItems.append(cell)
  }
}

extension PopOverMenuVC : UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return Constants.itemHeight
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    dismiss(animated: true, completion: {
      if let menuCmd = PopMenuValue(rawValue: indexPath.row),
        let handler = self.delegateHandlers[menuCmd] {
        
        handler()
      }
    })
  }
}

extension PopOverMenuVC : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return menuItems.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return menuItems[indexPath.row]
  }
}
