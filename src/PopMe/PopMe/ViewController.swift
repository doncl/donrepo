//
//  ViewController.swift
//  PopMe
//
//  Created by Don Clore on 2/12/17.
//  Copyright © 2017 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit
import QuartzCore

class ViewController: UIViewController {
  
  @IBOutlet var popMeButton: UIBarButtonItem!
  
 
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  
  @IBAction func popMe(_ sender: Any) {
    let height = PopOverMenuVC.Constants.menuHeight
    let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: height)
    
    let vc = PopOverMenuVC(frame: frame, delegate: self)
    vc.modalPresentationStyle = .popover
    vc.popoverPresentationController!.delegate = self
    vc.popoverPresentationController!.barButtonItem = popMeButton
    vc.preferredContentSize = CGSize(width: view.frame.width, height: height)
    present(vc, animated: true, completion: nil)
  }
  
  @IBAction func shareMe(_ sender: Any) {
    invokeActionSheet()
  }
  
  fileprivate func invokeActionSheet() {
    let s = "TheMaven Channel Page"
    let url = URL(string: "https://www.master.themaven.net/the-maven")
    
    let avc = UIActivityViewController(activityItems: [s, url as Any], applicationActivities: nil)
    if let navigationController = navigationController {
      navigationController.present(avc, animated: true, completion: nil)
    }
  }
}



extension ViewController : UIPopoverPresentationControllerDelegate {
  func adaptivePresentationStyle(for controller: UIPresentationController) ->
    UIModalPresentationStyle {
      
    return .none
  }
}

extension ViewController : PopOverMenuDelegate {
  func shareRoom() {
  }

  func shareDirectMessage() {
  }

  func shareSocial() {
    invokeActionSheet()
  }

  func copyLink() {
  }

  func subscribe() {
  }

  func flagSpam() {
  }

  func flagAbuse() {
  }

  func delete() {
  }

}

