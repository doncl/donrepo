//
//  MenuItemCell.swift
//  PopMe
//
//  Created by Don Clore on 2/12/17.
//  Copyright © 2017 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit
import SnapKit
import QuartzCore

class MenuItemCell: UITableViewCell {
  let fontSize : CGFloat = 15.5
  let leftPad : CGFloat = 24.0
  let spacing : CGFloat = 10.0
  
  var stack : UIStackView
  
  var rightIconView : UIImageView?
  
  convenience init(leftIcon : UIImage? = nil, title: String, bold : Bool = false,
    awesomeText : String? = nil, rightIcon : UIImage? = nil, cellWidth : CGFloat? = nil) {
    
    self.init(style: .default, reuseIdentifier : nil)
    backgroundColor = .white
    tintColor = UIColor(white: 151.0 / 255.0, alpha: 1.0)
    
    if let leftIcon = leftIcon {
      let leftIconView = UIImageView(image: leftIcon)
      leftIconView.contentMode = .scaleAspectFit
      stack.addArrangedSubview(leftIconView)
    }
    let titleLabel = UILabel()
    if bold {
      titleLabel.font = UIFont.boldSystemFont(ofSize: fontSize)
    } else {
      titleLabel.font = UIFont.systemFont(ofSize: fontSize)
    }
    titleLabel.text = title
    
    stack.addArrangedSubview(titleLabel)

    if let awesomeText = awesomeText {
      let fontAwesomeLabel = UILabel()
      fontAwesomeLabel.text = awesomeText
      fontAwesomeLabel.font = UIFont(name: "FontAwesome", size: 18.0)
      fontAwesomeLabel.textAlignment = .left
      stack.addArrangedSubview(fontAwesomeLabel)
    }
    
    if let rightIcon = rightIcon {
      let rightIconView = UIImageView(image: rightIcon)
      rightIconView.contentMode = .scaleAspectFit
      stack.addArrangedSubview(rightIconView)
    }
    if let cellWidth = cellWidth {
      let line = CALayer()
      line.frame = CGRect(x: leftPad, y:  PopOverMenuVC.Constants.itemHeight - 1,
        width: cellWidth - (leftPad * 2), height: 1.0)
      
      line.backgroundColor = UIColor(white: 238.0 / 255.0, alpha: 0.8).cgColor
      layer.addSublayer(line)
    }
  }

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    stack = UIStackView()
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    stack.axis = .horizontal
    stack.spacing = spacing
    
    addSubview(stack)
    stack.snp.makeConstraints { (make) -> Void in
      make.top.equalTo(snp.top)
      make.bottom.equalTo(snp.bottom)
      make.left.equalTo(snp.left).offset(leftPad)
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }

}
