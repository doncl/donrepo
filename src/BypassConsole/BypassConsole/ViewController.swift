//
//  ViewController.swift
//  BypassConsole
//
//  Created by Don Clore on 3/20/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet var textViewSrc: UITextView!
  @IBOutlet var textViewDest: UITextView!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


  @IBAction func buttonClicked(_ sender: Any) {
    
    let srcText = textViewSrc.text!
    
    if let parsed =  MarkdownParser.sharedInstance.parseMarkdown(markdown: srcText) {
      textViewDest.attributedText = parsed
    }
    
  }
}

