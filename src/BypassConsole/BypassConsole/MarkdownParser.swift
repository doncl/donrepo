//
//  MarkdownParser.swift
//  Duwamish
//
//  Created by Don Clore on 12/29/16.
//  Copyright © 2016 AmplifyMedia. All rights reserved.
//

import UIKit

public class MarkdownParser {
  static var sharedInstance : MarkdownParser = MarkdownParser()
  
  fileprivate let bypassParser : BPParser
  fileprivate let bypassConverter : BPAttributedStringConverter
  
  private init() {
    bypassParser = BPParser()
    bypassConverter = BPAttributedStringConverter()
    bypassConverter.displaySettings.linkColor = .blue
  }
  
  public func parseMarkdown(markdown: String) -> NSMutableAttributedString? {
    let doc = bypassParser.parse(markdown)
    guard let attributedString = bypassConverter.convert(doc) else {
      return nil
    }
    
    return NSMutableAttributedString(attributedString: attributedString)
    
//    let mutable = NSMutableAttributedString(attributedString: attributedString)
//    mutable.trimCharactersInSet(charSet: .whitespacesAndNewlines, options: [.end])
//    return mutable
  }
}

