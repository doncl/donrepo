//
//  ViewController.swift
//  BypassConsole
//
//  Created by Don Clore on 3/20/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet var textViewSrc: UITextView!
  @IBOutlet var textViewDest: UITextView!
  
  var bulletedList : String!
  var numberedList : String!
  var links : String!
  
  override func viewDidLoad() {
    super.viewDidLoad()
  
    bulletedList = loadString(from: "BulletedList", withExtension: "md")
    numberedList = loadString(from: "NumberedList", withExtension: "md")
    links = loadString(from: "links", withExtension: "md")
    textViewSrc.text = bulletedList
  }

  @IBAction func buttonClicked(_ sender: Any) {
    
    let srcText = textViewSrc.text!
    
    if let parsed =  MarkdownParser.sharedInstance.parseMarkdown(markdown: srcText) {
      textViewDest.attributedText = parsed
    }    
  }
  
  fileprivate func loadString(from name: String, withExtension ext: String) -> String {
    let bundle = Bundle.main
    guard let url = bundle.url(forResource: name, withExtension: ext) else {
      fatalError("foo")
    }
    guard let s  = try? String(contentsOf: url, encoding: .utf8) else {
      fatalError("bar")
    }
    return s
  }

}

