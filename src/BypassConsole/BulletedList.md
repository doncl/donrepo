- Come up with an easily understandable definition for craft that clearly differentiates the value proposition in contrast with industrial chocolate.
- Get all (or a large majority of) makers to agree to use the definition(s) in the same way.
- Aggregate some resources to market the term to consumers effectively.
- Stop marketing using phrases like, “Our chocolate is so good you don’t need to eat very much of it to feel satisfied.” It makes zero business sense to tell consumers they need to buy less of your product.
- Increase transparency about the farm gate price of the beans being used.
- Clearly articulate how many farmers are having their lives materially improved through their efforts. If a small maker is only buying 1 bag of beans from a farmer or co-op per year at a premium of $1000/MT over what others pay then the value of the impact is, in concrete terms, meaningless.
- Address the gap between the purpose and promises of awards programs and the way they are negatively skewing markets.

