1. The price of cocoa beans *is not* a key driver of the retail price of a chocolate bar.
2. The price of labor is a key driver.
3. Batch and bar size are key drivers; small batches cost more; bigger bars *might* be more profitable than smaller ones.
4. The cost of distribution *is* a key driver.
5. And we haven’t even talked about the cost of packaging, the cost of equipment, and the cost of overhead.
Layered into all of the above points is a discussion of scale. How many tonnes of beans does a small chocolate maker need to produce in order to be consistently profitable? 4MT/year? 6MT/year? 12? 25? 50? 

