//
//  UIColorExtensions.swift
//  Duwamish
//
//  Created by Sonu on 23/08/16.
//  Copyright © 2016 AmplifyMedia. All rights reserved.
//

import Foundation
import UIKit

enum ColorType{
  case light
  case dark
}

extension UIColor{
  //from http://stackoverflow.com/questions/24263007/how-to-use-hex-colour-values-in-swift-ios
  //cuz I was too lazy to do it myself
  convenience init(red: Int, green: Int, blue: Int) {
    assert(red >= 0 && red <= 255, "Invalid red component")
    assert(green >= 0 && green <= 255, "Invalid green component")
    assert(blue >= 0 && blue <= 255, "Invalid blue component")
    
    self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
  }
  
  //http://stackoverflow.com/questions/24263007/how-to-use-hex-colour-values-in-swift-ios
  //ethan strider
  convenience init(hex: String) {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
      cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
      self.init(red:0, green:0, blue:0)
      return
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    self.init(
      red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
      green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
      blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
      alpha: CGFloat(1.0)
    )
  }
  
  convenience init(netHex:Int) {
    self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
  }
  
  static func randomColor() -> UIColor {
    // If you wanted a random alpha, just create another
    // random number for that too.
    return UIColor(red:   CGFloat(drand48()),
                   green: CGFloat(drand48()),
                   blue:  CGFloat(drand48()),
                   alpha: 1.0)
  }
  
  //http://stackoverflow.com/questions/28644311/how-to-get-the-rgb-code-int-from-an-uicolor-in-swift
  //http://stackoverflow.com/questions/2509443/check-if-uicolor-is-dark-or-bright
  func getColorType() -> ColorType {
    
    var aRed : CGFloat = 0
    var aGreen : CGFloat = 0
    var aBlue : CGFloat = 0
    
    if getRed(&aRed, green: &aGreen, blue: &aBlue, alpha: nil) == true {
      
      let iRed = Int(aRed * 255.0)
      let iGreen = Int(aGreen * 255.0)
      let iBlue = Int(aBlue * 255.0)
      
      let value = ((iRed * 299) + (iGreen * 587) + (iBlue * 114))/1000
      
      if(value > 200){
        return .light
      } else{
        return .dark
      }
      
    }
    
    return .dark
  }
}

