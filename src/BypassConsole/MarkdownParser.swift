//
//  MarkdownParser.swift
//  Duwamish
//
//  Created by Don Clore on 12/29/16.
//  Copyright © 2016 AmplifyMedia. All rights reserved.
//

import UIKit

public class MarkdownParser {
  static var sharedInstance : MarkdownParser = MarkdownParser()
  
  fileprivate let bypassParser : BPParser
  fileprivate let bypassConverter : BPAttributedStringConverter
  
  fileprivate func tweakDisplaySettings() {
    bypassConverter.displaySettings.linkColor = .black
    bypassConverter.displaySettings.defaultColor = Colors.grey74
    bypassConverter.displaySettings.defaultFont = UIFont(name: FontName.georgia, size: 17.0)
    bypassConverter.displaySettings.italicFont = UIFont(name: FontName.georgiaItalic, size: 17.0)
    bypassConverter.displaySettings.boldFont = UIFont(name: FontName.georgiaBold, size: 17.0)
    bypassConverter.displaySettings.boldItalicFont = UIFont(name: FontName.georgiaBoldItalic, size: 17.0)
    bypassConverter.displaySettings.paragraphLineSpacing = 6
    bypassConverter.displaySettings.quoteFont = UIFont(name: FontName.georgiaItalic, size: 17.0)
    bypassConverter.displaySettings.bulletIndentation = 25.0
    bypassConverter.displaySettings.bulletPointSpacing = 7.0
  }
  
  private init() {
    bypassParser = BPParser()
    bypassConverter = BPAttributedStringConverter()
    tweakDisplaySettings()
  }
  
  public func parseMarkdown(markdown: String) -> NSMutableAttributedString? {
    let doc = bypassParser.parse(markdown)
    tweakDisplaySettings()
    guard let attributedString = bypassConverter.convert(doc) else {
      return nil
    }
    
    return NSMutableAttributedString(attributedString: attributedString)
    
//    let mutable = NSMutableAttributedString(attributedString: attributedString)
//    mutable.trimCharactersInSet(charSet: .whitespacesAndNewlines, options: [.end])
//    return mutable
  }
    
}

