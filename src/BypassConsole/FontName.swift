//
//  FontName.swift
//  BypassConsole
//
//  Created by Don Clore on 2/6/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

struct FontName {  
  public static let georgia = "Georgia"
  public static let georgiaItalic = "Georgia-Italic"
  public static let georgiaBold = "Georgia-Bold"
  public static let georgiaBoldItalic = "Georgia-BoldItalic"
}
