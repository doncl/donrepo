//
//  ViewController.swift
//  HudsonMVVC
//
//  Created by Don Clore on 5/17/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  @IBOutlet var username: BoundTextField!
  
  var user = User(name: Observable("Don Clore"))
  
  override func viewDidLoad() {
    super.viewDidLoad()


    
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    username.bind(to: user.name)
  }
}

