example(of: "initializing a stack from an array literal") {
  
  var stack: Stack = [1.0, 2.0, 3.0, 4.0, 5.0]
  print(stack)
  stack.pop()
}
