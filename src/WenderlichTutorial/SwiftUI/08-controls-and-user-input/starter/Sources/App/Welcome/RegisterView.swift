//
//  RegisterView.swift
//  Kuchi
//
//  Created by Don Clore on 12/31/19.
//  Copyright © 2019 Omnijar. All rights reserved.
//

import SwiftUI

struct RegisterView: View {
  @EnvironmentObject var userManager: UserManager
  @ObservedObject var keyboardHandler: KeyboardFollower
      
  var body: some View {
    VStack {
      WelcomeMessageView()
      TextField("Type your name...", text: $userManager.profile.name)
        .bordered()
      
      HStack {
        Spacer()
        Text("\(userManager.profile.name.count)")
          .font(.caption)
          .foregroundColor(userManager.isUserNameValid() ? .green : .red)
          .padding(.trailing)
      }
      .padding(.bottom)
      
      HStack {
        Spacer()
        Toggle(isOn: $userManager.settings.rememberUser) {
          Text("Remember Me")
            .font(.subheadline)
            .multilineTextAlignment(.trailing)
            .foregroundColor(.gray)
        }.disabled(!userManager.isUserNameValid())
      }
 
      Button(action: self.registerUser) {
        HStack {
          Image(systemName: "checkmark").resizable().frame(width: 16, height: 16, alignment: .center)
          Text("OK")
            .font(.body)
            .bold()
        }
      }
      .disabled(!userManager.isUserNameValid())
    }
    .padding(.bottom, keyboardHandler.keyboardHeight)
    .background(WelcomeBackgroundImage())
    .padding()
  }
  init(keyboardHandler: KeyboardFollower) {
    self.keyboardHandler = keyboardHandler
  }
}

extension RegisterView {
  func registerUser() {
    if userManager.settings.rememberUser {
      userManager.persistProfile()
    } else {
      userManager.clear()
    }
    userManager.persistSettings()
  }
}

struct RegisterView_Previews: PreviewProvider {
  static let user = UserManager(name: "Joe")
  
  static var previews: some View {
    RegisterView(keyboardHandler: KeyboardFollower()).environmentObject(user)
  }
}
