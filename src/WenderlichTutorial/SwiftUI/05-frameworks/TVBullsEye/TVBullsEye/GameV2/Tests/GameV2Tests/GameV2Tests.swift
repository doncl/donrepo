import XCTest
@testable import GameV2

final class GameV2Tests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(GameV2().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
