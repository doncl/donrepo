import XCTest

import GameV2Tests

var tests = [XCTestCaseEntry]()
tests += GameV2Tests.allTests()
XCTMain(tests)
