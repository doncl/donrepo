//
//  WelcomeView.swift
//  Kuchi
//
//  Created by Don Clore on 12/30/19.
//  Copyright © 2019 Omnijar. All rights reserved.
//

import SwiftUI

struct WelcomeView: View {
  var body: some View {
   GeometryReader { proxy in
    ZStack {
      Image("welcome-background", bundle: nil)
        .resizable()
        .aspectRatio(1 / 1, contentMode: .fill)
        .edgesIgnoringSafeArea(.all)
        .saturation(0.5)
        .blur(radius: 5)
        .opacity(0.08)
      

        HStack {
          Image(systemName: "table")
            .resizable()
            .frame(width: 60, height: 60, alignment: .center) .border(Color.gray, width: 1)
            .cornerRadius(60 / 2)
            .background(Color(white: 0.9)) .clipShape(Circle())
            .foregroundColor(.red)
          
          VStack(alignment: .leading) {
            Text("Welcome to")
              .font(.headline)
              .bold()
            Text("Kuchi")
              .font(.largeTitle)
              .bold()
          }
          .foregroundColor(.red)
          .lineLimit(2)
          .multilineTextAlignment(.leading)
          .padding(.horizontal)
        }

      }
    }
  }
}

struct WelcomeView_Previews: PreviewProvider {
  static var previews: some View {
    WelcomeView()
  }
}
