//
//  FlashCard.swift
//  Kuchi
//
//  Created by Don Clore on 1/2/20.
//  Copyright © 2020 Omnijar. All rights reserved.
//

import Foundation
import Learning

struct FlashCard {
  var id = UUID()
  var isActive = true
  var card: WordCard
}

extension FlashCard: Identifiable {
  
}

extension FlashCard: Equatable {
  static func == (lhs: Self, rhs: Self) -> Bool {
    return lhs.card.word.original == rhs.card.word.original
      && lhs.card.word.translation == rhs.card.word.translation
  }
}
