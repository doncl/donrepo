//
//  FlashDeck.swift
//  Kuchi
//
//  Created by Don Clore on 1/2/20.
//  Copyright © 2020 Omnijar. All rights reserved.
//

import Foundation
import Learning
import Combine

final internal class FlashDeck {
  @Published var cards: [FlashCard]
  
  init(from words: [WordCard]) {
    self.cards = words.map {
      FlashCard(card: $0)
    }
  }
}
