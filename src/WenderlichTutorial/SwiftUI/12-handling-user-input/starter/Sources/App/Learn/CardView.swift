//
//  CardView.swift
//  Kuchi
//
//  Created by Don Clore on 1/2/20.
//  Copyright © 2020 Omnijar. All rights reserved.
//

import SwiftUI
import Languages
import Learning

typealias CardDrag = (_ card: FlashCard,
                      _ direction: DiscardedDirection) -> Void

struct CardView: View {
  private let flashCard: FlashCard
  @State private var revealed: Bool = false
  @State private var offset: CGSize = CGSize.zero
  @GestureState private var isLongPressed: Bool = false
  
  private let dragged: CardDrag
  
  var body: some View {
    let drag = DragGesture()
      .onChanged {
        self.offset = $0.translation
      }
      .onEnded {
        if $0.translation.width < -100 {
          self.offset = CGSize(width: -1000, height: 0)
          self.dragged(self.flashCard, .left)
        } else if $0.translation.width > 100 {
          self.offset = CGSize(width: 1000, height: 0)
          self.dragged(self.flashCard, .right)
        } else {
          self.offset = .zero
        }
      }
    
    let longPress = LongPressGesture()
      .updating($isLongPressed) { value, state, transition in
        state = value
    }.simultaneously(with: drag)
    
    return ZStack {
      Rectangle()
        .fill(Color.red)
        .frame(width: 320, height: 210) .cornerRadius(12)
      
      VStack {
        Spacer()
        Text(flashCard.card.word.original)
          .font(.largeTitle)
          .foregroundColor(.white)
        
        if self.revealed {
          Text(flashCard.card.word.translation)
            .font(.caption)
            .foregroundColor(.white)
        }
        Spacer()
      }
    }
    .shadow(radius: 8)
    .frame(width: 320, height: 210)
    .animation(.spring())
    .offset(self.offset)
    .gesture(longPress)
    .scaleEffect(isLongPressed ? 1.1 : 1)
    .gesture(TapGesture()
    .onEnded {
      withAnimation( .easeIn, {
        self.revealed.toggle()
      })
    })
  }
  
  init(_ card: FlashCard,
       onDrag dragged: @escaping CardDrag = {_,_ in }) {
    self.flashCard = card
    self.dragged = dragged
  }
}

struct CardView_Previews: PreviewProvider {
  static var previews: some View {
    let card = FlashCard(
      card: WordCard(
        from: TranslatedWord(
          from: "Apple",
          withPronunciation: "Apple",
          andTranslation: "Omena")))
    
    return CardView(card)
  }
}
