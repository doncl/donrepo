//
//  DiscardedDirection.swift
//  Kuchi
//
//  Created by Don Clore on 1/2/20.
//  Copyright © 2020 Omnijar. All rights reserved.
//

import Foundation

internal enum DiscardedDirection {
  case left
  case right 
}
