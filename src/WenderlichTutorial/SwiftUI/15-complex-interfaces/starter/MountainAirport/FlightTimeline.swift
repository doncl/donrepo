import SwiftUI

class Coordinator: NSObject {
  var flightData: [FlightInformation]
  
  init(flights: [FlightInformation]) {
    self.flightData = flights
  }
}

extension Coordinator: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return flightData.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let timeFormatter = DateFormatter()
    timeFormatter.timeStyle = DateFormatter.Style.short
    timeFormatter.dateStyle = DateFormatter.Style.none
    
    let flight = flightData[indexPath.item]
    let scheduledString = timeFormatter.string(from: flight.scheduledTime)
    let currentString = timeFormatter.string(from: flight.currentTime ?? flight.scheduledTime)
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "TimelineTableViewCell", for: indexPath)
      as! TimelineTableViewCell
    
    let flightInfo = "\(flight.airline) \(flight.number) \(flight.direction == .departure ? "to" : "from")" +
      " \(flight.otherAirport) - \(flight.flightStatus)"
      
    cell.descriptionLabel.text = flightInfo

    if flight.status == .cancelled {
      cell.titleLabel.text = "Cancelled"
    } else if flight.timeDifference != 0 {
      if flight.status == .cancelled {
        cell.titleLabel.text = "Cancelled"
      } else if flight.timeDifference != 0 {
        var title = "\(scheduledString)"
        title = title + " Now: \(currentString)"
        cell.titleLabel.text = title
      } else {
        cell.titleLabel.text = "On Time for \(scheduledString)"
      }
    } else {
      cell.titleLabel.text = "On Time for \(scheduledString)"
    }
    
    cell.titleLabel.textColor = UIColor.black
    cell.bubbleColor = flight.timelineColor
    
    return cell
  }
  
  
}

struct FlightTimeline: UIViewControllerRepresentable {
  var flights: [FlightInformation]
  
  func makeCoordinator() -> Coordinator {
    Coordinator(flights: flights)
  }
  
  func makeUIViewController(context: Context) -> UITableViewController {
    UITableViewController()
  }
  
  func updateUIViewController(_ vc: UITableViewController, context: Context) {
    let timelineTableViewCellNib = UINib(nibName: "TimelineTableViewCell", bundle: Bundle.main)
    
    vc.tableView.register(timelineTableViewCellNib, forCellReuseIdentifier: "TimelineTableViewCell")
    vc.tableView.dataSource = context.coordinator
    
  }
}
