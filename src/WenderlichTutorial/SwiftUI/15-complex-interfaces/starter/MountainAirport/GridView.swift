import SwiftUI

struct GridView<Content, T>: View where Content: View {
  var columns: Int
  var items: [T]
  let content: (CGFloat, T) -> Content
  
  var numberRows: Int {
    return (items.count - 1) / columns
  }
  
  var body: some View {
    GeometryReader { geometry in
      ScrollView {
        VStack {
          ForEach(0...self.numberRows, id: \.self) { row in
            HStack {
              ForEach(0 ..< self.columns, id: \.self) { column in
                Group {
                  if self.elementFor(row: row, column: column) != nil {
                    self.content(
                      geometry.size.width / CGFloat(self.columns),
                      self.items[self.elementFor(row: row, column: column)!]
                    )
                    .frame(width: geometry.size.width / CGFloat(self.columns),
                           height: geometry.size.width / CGFloat(self.columns))
                  } else {
                    Spacer()
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  private func elementFor(row: Int, column: Int) -> Int? {
    let index = row * self.columns + column
    return index < items.count ? index : nil
  }
  
  init(columns: Int, items: [T], @ViewBuilder content: @escaping (CGFloat, T) -> Content) {
    self.columns = columns
    self.items = items
    self.content = content
  }
}

struct GridView_Previews: PreviewProvider {
  static var previews: some View {
    GridView(columns: 3, items: [11, 3, 7, 17, 5, 2, 1]) { width, item in
      Text("\(item)")
    }
  }
}
