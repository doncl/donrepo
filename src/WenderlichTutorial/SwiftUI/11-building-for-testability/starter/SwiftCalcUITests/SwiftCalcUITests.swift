import XCTest

class SwiftCalcUITests: XCTestCase {

  override func setUp() {
    continueAfterFailure = false
  }

  override func tearDown() {
  }

  func testPressMemoryPlusAtAppStartShowZeroInDisplay() {
    let app = XCUIApplication()
    app.launch()
    
    let memoryButton = app.buttons["M+"]
    memoryButton.tap()

    let display = app.staticTexts["display"]
    let displayText = display.label
    XCTAssert(displayText == "0")
  }

  func testLaunchPerformance() {
    if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
      // This measures how long it takes to launch your application.
      measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
          XCUIApplication().launch()
      }
    }
  }
  
  func testAddingTwoDigits() {
    let app = XCUIApplication()
    app.launch()
    
    let threeButton = app.buttons["3"]
    threeButton.tap()
    
    let addButton = app.buttons["+"]
    addButton.tap()
    
    let fiveButton = app.buttons["5"]
    fiveButton.tap()
    
    let equalButton = app.buttons["="]
    equalButton.tap()
    
    let display = app.staticTexts["display"]
    let displayText = display.label
    
    XCTAssert(displayText == "8.0")
  }
  
  func testSwipeToClearMemory() {
    let app = XCUIApplication()
    app.launch()
    
    let threeButton = app.buttons["3"]
    threeButton.tap()
    
    let fiveButton = app.buttons["5"]
    fiveButton.tap()
    
    let memoryButton = app.buttons["M+"]
    memoryButton.tap()
    
    let memoryDisplay = app.staticTexts["memoryDisplay"]
    memoryButton.tap()
    
    XCTAssert(memoryDisplay.exists)
    
    #if targetEnvironment(macCatalyst)
    memoryDisplay.doubleTap()
    #else
    memoryDisplay.swipeLeft()
    #endif
    XCTAssertFalse(memoryDisplay.exists)
  }
}
