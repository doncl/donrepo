//
//  User.swift
//  Kuchi
//
//  Created by Don Clore on 12/31/19.
//  Copyright © 2019 Omnijar. All rights reserved.
//

import Combine

final internal class User {
  @Published var isRegistered: Bool = false
  
  let willChange = PassthroughSubject<User, Never>()
  
  var profile: Profile = Profile() {
    willSet {
      willChange.send(self)
    }
  }
  
  init(name: String) {
    self.profile.name = name
  }
  
  init() {}
}


