//
//  Splitty.swift
//  SplittyScaffold
//
//  Created by Don Clore on 1/13/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import UIKit

class Splitty: UIViewController {
    let splitterDim: CGFloat = 25.0

    let detail: UIViewController
    let master: UIViewController
    let divider: UIImageView = UIImageView(image: #imageLiteral(resourceName: "divider"))
    var position: CGFloat
    private var masterWidth: NSLayoutConstraint?
    private var currMasterWidthScalar: CGFloat = 0

    private var inPan: Bool = false

    init(master: UIViewController, detail: UIViewController, initialPosition: CGFloat) {
        self.master = master
        self.detail = detail
        self.position = initialPosition
        super.init(nibName: nil, bundle: nil)
        divider.tintColor = .black
        divider.backgroundColor = .clear
        divider.isUserInteractionEnabled = true
        let pan = UIPanGestureRecognizer(target: self, action: #selector(Splitty.pan(_:)))
        divider.addGestureRecognizer(pan)
    }

    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        addChild(master)
        view.addSubview(master.view)
        master.didMove(toParent: self)

        addChild(detail)
        view.addSubview(detail.view)
        detail.didMove(toParent: self)

        master.view.translatesAutoresizingMaskIntoConstraints = false
        detail.view.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            master.view.topAnchor.constraint(equalTo: view.topAnchor),
            master.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            master.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            detail.view.topAnchor.constraint(equalTo: view.topAnchor),
            detail.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            detail.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            detail.view.leadingAnchor.constraint(equalTo: master.view.trailingAnchor),
        ])


        view.addSubview(divider)
        divider.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            divider.centerXAnchor.constraint(equalTo: master.view.trailingAnchor),
            divider.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            divider.widthAnchor.constraint(equalToConstant: splitterDim),
            divider.heightAnchor.constraint(equalToConstant: splitterDim),
        ])
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if masterWidth != nil {
            masterWidth?.isActive = false
            masterWidth = nil
        }

        currMasterWidthScalar = view.bounds.width * position
        masterWidth = master.view.widthAnchor.constraint(equalToConstant: currMasterWidthScalar)
        masterWidth?.isActive = true
    }
}

extension Splitty {
    @objc func pan(_ sender: UIPanGestureRecognizer) {
        guard inPan || sender.state == .began else { return }
        let translationInView = sender.translation(in: view)

        switch sender.state {
        case .began:
            inPan = true
            if let masterWidth = masterWidth {
                currMasterWidthScalar = masterWidth.constant
            }
        case UIGestureRecognizer.State.changed:
            masterWidth?.constant = currMasterWidthScalar + translationInView.x
            view.setNeedsLayout()
        case .ended, .cancelled, .failed:
            inPan = false
            if let masterWidth = masterWidth {
                currMasterWidthScalar = masterWidth.constant
            }
        case .possible:
            break
        }
    }
}
