//
//  ViewController.swift
//  SplittyScaffold
//
//  Created by Don Clore on 1/13/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let master = UIViewController(nibName: nil, bundle: nil)
    let detail = UIViewController(nibName: nil, bundle: nil)
    var splitty: Splitty?

    override func viewDidLoad() {
        super.viewDidLoad()

        splitty = Splitty(master: master, detail: detail, initialPosition: 0.30)

        guard let splitty = splitty else {
            return
        }

        addChild(splitty)
        view.addSubview(splitty.view)
        splitty.view.translatesAutoresizingMaskIntoConstraints = false
        splitty.didMove(toParent: self)
        NSLayoutConstraint.activate([
            splitty.view.topAnchor.constraint(equalTo: view.topAnchor),
            splitty.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            splitty.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            splitty.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])

        master.view.backgroundColor = UIColor(red: 1, green: 20 / 255, blue: 147 / 255, alpha: 1)
        detail.view.backgroundColor = .purple
    }



}

