//
//  WebSocketClient.swift
//  Duwamish
//
//  Created by Don Clore on 8/16/16.
//  Copyright © 2016 AmplifyMedia. All rights reserved.
//

import Foundation
import SocketRocket

protocol AmplifySocketClientDelegate {
  func newSocketText(_ text : String)
}

@objc class AmplifySocketClient: NSObject {
  var subscribedConversations : [String] = []
  var atMentionSubscribed : Bool = false
  //var socket: WebSocket?
  var delegate: AmplifySocketClientDelegate?
  var deliberateDisconnection = false
  static let initialReconnectDelay : Int64 = 1
  static let maxReconnectDelay : Int64 = 64
  var currentReconnectDelay : Int64 =  1
  
  var rocket : SRWebSocket?
  
  #if false
  var connected : Bool {
  get {
  guard let socket = socket else {
  return false
  }
  return socket.isConnected
  }
  }
  #else
  var connected : Bool = false
  #endif
  
  var lastError: NSError?
}

//MARK: Subscribing and sending.
extension AmplifySocketClient {
  
  fileprivate func delay(_ delaySeconds : Int64, delayFunc : @escaping (Void) -> Void) {
    let triggerTime = (Int64(NSEC_PER_SEC) * delaySeconds)
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(triggerTime) / Double(NSEC_PER_SEC), execute: {
      delayFunc()
    })
  }
  
  fileprivate func reconnectWithExponentialBackOff() {
    delay(currentReconnectDelay, delayFunc: {
      self.connect()
      self.currentReconnectDelay = self.currentReconnectDelay * 2
      
      // Don't let the reconnect delay get too crazy large.
      if self.currentReconnectDelay > AmplifySocketClient.maxReconnectDelay {
        self.currentReconnectDelay = AmplifySocketClient.initialReconnectDelay
      }
    })
  }
  
  func connect() {
    if let subscriptionURL = URL(string: "http://192.168.0.106:8001/baker") {
      #if false
        socket = WebSocket(url: subscriptionURL)
        socket!.onConnect = {
          Global.log.debug("Web Socket connected to \(subscriptionURL)")
          DispatchQueue.main.async(execute: {
            self.currentReconnectDelay = AmplifySocketClient.initialReconnectDelay
            Global.log.debug("Subscribing to at mention for this user")
            self.subscribeToAtMention()
            for conversationToResubscribe in self.subscribedConversations {
              Global.log.debug("Resubscribing to conversation \(conversationToResubscribe)")
              self.subscribeToConversation(conversationToResubscribe)
            }
          })
        }
        socket!.onDisconnect = { (error : NSError?) in
          if let error = error {
            self.lastError = error
            Global.log.error("Error on disconnecting from Web Socket = \(error.debugDescription)")
          }
          
          if self.deliberateDisconnection {
            Global.log.debug("Web Socket successfully disconnected")
            self.currentReconnectDelay = AmplifySocketClient.initialReconnectDelay
            self.subscribedConversations = []
          }
          else {
            Global.log.warning("Web Socket unintentionally disconnecting, reconnecting with " +
              "delay of \(self.currentReconnectDelay) seconds")
            
            self.reconnectWithExponentialBackOff()
          }
        }
        socket!.onText = { (text: String) in
          Global.log.debug("Got text from web socket")
          if let delegate = self.delegate {
            let textCopy = text.copy() as! String
            delegate.newSocketText(textCopy)
          }
        }
        socket!.onData = { (data: Data) in
          Global.log.debug("Got some data from web socket")
        }
        socket!.connect()
      #else
        rocket = SRWebSocket(url: subscriptionURL)
        rocket!.delegate = self
        rocket!.open()
        
      #endif
      
    } else {
      print("Could not connect to web socket host http://192.168.0.106:8001/baker")
    }
  }
  
  func disconnect() {
    #if false
      if let socket = socket {
        deliberateDisconnection = true
        socket.disconnect()
      }
    #else
      if let rocket = rocket {
        deliberateDisconnection = true
        rocket.close()
      }
    #endif
  }
  
  func subscribeToConversation(_ conversationId: String) {
    #if false
      guard let socket = socket , connected else {
        Global.log.error("Web Socket not intialized")
        return
      }
      socket.write(string: "Subscribe-Conversation:\(conversationId)")
    #else
      guard let rocket = rocket, connected else {
        print("Web Socket not intialized")
        return
      }
      rocket.send("Subscribe-Conversation:\(conversationId)")
    #endif
    subscribedConversations.append(conversationId)
  }
  
  func unsubscribeFromConversation(_ conversationId: String) {
    #if false
      guard let socket = socket , connected else {
        Global.log.error("Web Socket not initialized")
        return
      }
      socket.write(string: "Unsubscribe-Conversation:\(conversationId)")
    #else
      guard let rocket = rocket, connected else {
        print("Web Socket not initialized")
        return
      }
      rocket.send("Unsubscribe-Conversation:\(conversationId)")
    #endif
    if let subscribedConversationIndex =
      subscribedConversations.index(where: {$0 == conversationId}) {
      
      subscribedConversations.remove(at: subscribedConversationIndex)
    }
  }
  
  func subscribeToAtMention() {
    #if false
      
      guard let userData = Global.currentUserData, let socket = socket , connected else {
        Global.log.error("Either Web Socket not intialized or user not logged in")
        return
      }
      socket.write(string: "Login-Token:\(userData.loginToken)")
    #else
      let loginToken = "L684fRqbpLnbOcBwFmj4HgvlX1cr1x3Z6/WNt4U+4xc="
      rocket!.send("Login-Token:\(loginToken)")
    #endif
    
    atMentionSubscribed = true
  }
}

extension AmplifySocketClient : SRWebSocketDelegate {
  func webSocketDidOpen(_ webSocket: SRWebSocket!) {
    print("SocketRocket was opened")
    connected = true
  }
  
  func webSocket(_ webSocket: SRWebSocket!, didReceiveMessage message: Any!) {
    if let delegate = self.delegate, let stringMsg = message as? String {
      let textCopy = stringMsg.copy() as! String
      delegate.newSocketText(textCopy)
    }
  }
  
  func webSocket(_ webSocket: SRWebSocket!, didFailWithError error: Error!) {
    print("SocketRocket error \(error.localizedDescription)")
    lastError = error as NSError
    print("Web Socket unintentionally disconnecting, reconnecting with " +
      "delay of \(self.currentReconnectDelay) seconds")
    

  }
  
  func webSocket(_ webSocket: SRWebSocket!, didCloseWithCode code: Int, reason: String!,
                 wasClean: Bool) {
    connected = false
    print("SocketRocket closing with code: \(code), reason: \(reason)")
    self.currentReconnectDelay = AmplifySocketClient.initialReconnectDelay
    self.subscribedConversations = []
    if !deliberateDisconnection {
      reconnectWithExponentialBackOff()
    }
  }
}




