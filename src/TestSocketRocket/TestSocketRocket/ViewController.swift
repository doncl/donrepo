 //
//  ViewController.swift
//  TestSocketRocket
//
//  Created by Don Clore on 9/15/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  var client : AmplifySocketClient = AmplifySocketClient()
  
  @IBOutlet var textView: UITextView!
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.

    client.delegate = self
    client.connect()
    
  }

  override func viewDidAppear(_ animated: Bool) {
   
  }
  
  @IBAction func connectAndSubscribe(_ sender: AnyObject) {
    client.subscribeToAtMention()
    client.subscribeToConversation("94a0a88d8c7547729906f45ee6d7cba2")
   
  }
}

extension ViewController : AmplifySocketClientDelegate {
  func newSocketText(_ text : String) {
    DispatchQueue.main.async {
      if let oldText = self.textView.text {
        self.textView.text = oldText + "\n" + text
      } else {
        self.textView.text = text
      }
      self.scollTextViewToBottom()
    }

  }
  
  fileprivate func scollTextViewToBottom() {
    if textView.text.characters.count > 0 {
      let bottom = NSMakeRange(textView.text.characters.count - 1, 1)
      textView.scrollRangeToVisible(bottom)
    }
  }
}

