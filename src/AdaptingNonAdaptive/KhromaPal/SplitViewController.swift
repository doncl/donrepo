//
//  SplitViewController.swift
//  KhromaPal
//
//  Created by Don Clore on 4/10/16.
//  Copyright © 2016 RayWenderlich. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController, UISplitViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }

    func splitViewController(splitViewController: UISplitViewController,
                             collapseSecondaryViewController secondaryViewController: UIViewController,
                             ontoPrimaryViewController primaryViewController: UIViewController) -> Bool {

        if let selectionCont = primaryViewController as? PaletteSelectionContainer {
            if let displayCont = secondaryViewController as? PaletteDisplayContainer {
                let selectedPalette = selectionCont.rwt_currentlySelectedPalette()
                let displayedPalette = displayCont.rwt_currentlyDisplayedPalette()

                if selectedPalette != nil && selectedPalette == displayedPalette {
                    return false
                }
            }
        }
        // We don't want anything to happen.  Say we've dealt with it.
        return true
    }

    func splitViewController(splitViewController: UISplitViewController,
        separateSecondaryViewControllerFromPrimaryViewController primaryViewController: UIViewController) -> UIViewController? {
        if let paletteDisplayCont = primaryViewController as? PaletteDisplayContainer {
            if paletteDisplayCont.rwt_currentlyDisplayedPalette() != nil {
                return nil
            }
        }
        let vc = storyboard?.instantiateViewControllerWithIdentifier("NoPaletteSelected") as UIViewController!
        return NavigationController(rootViewController: vc)
    }
}
