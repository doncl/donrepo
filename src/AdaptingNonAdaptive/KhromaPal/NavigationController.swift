//
//  NavigationController.swift
//  KhromaPal
//
//  Created by Don Clore on 4/10/16.
//  Copyright © 2016 RayWenderlich. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController, PaletteDisplayContainer,
        PaletteSelectionContainer {

    func rwt_currentlyDisplayedPalette() -> ColorPalette? {
        if let tvc = topViewController as? PaletteDisplayContainer {
            return tvc.rwt_currentlyDisplayedPalette()
        }
        return nil
    }

    func rwt_currentlySelectedPalette() -> ColorPalette? {
        if let tvc = topViewController as? PaletteSelectionContainer {
            return tvc.rwt_currentlySelectedPalette()
        }
        return nil
    }
}
