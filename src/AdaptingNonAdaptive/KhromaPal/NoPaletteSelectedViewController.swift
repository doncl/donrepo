//
//  NoPaletteSelectedViewController.swift
//  KhromaPal
//
//  Created by Don Clore on 4/10/16.
//  Copyright © 2016 RayWenderlich. All rights reserved.
//

import UIKit

class NoPaletteSelectedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let svc = splitViewController {
            navigationItem.setLeftBarButtonItem(svc.displayModeButtonItem(), animated: true)
        }
    }
}
