//
//  PaletteContainer.swift
//  KhromaPal
//
//  Created by Don Clore on 4/10/16.
//  Copyright © 2016 RayWenderlich. All rights reserved.
//

import UIKit

protocol PaletteDisplayContainer {
    func rwt_currentlyDisplayedPalette() -> ColorPalette?
}

protocol PaletteSelectionContainer {
    func rwt_currentlySelectedPalette() -> ColorPalette?
}