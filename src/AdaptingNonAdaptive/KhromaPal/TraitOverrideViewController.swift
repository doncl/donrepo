//
//  TraitOverrideViewController.swift
//  KhromaPal
//
//  Created by Don Clore on 4/10/16.
//  Copyright © 2016 RayWenderlich. All rights reserved.
//

import UIKit

class TraitOverrideViewController: UIViewController {
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator
        coordinator: UIViewControllerTransitionCoordinator) {

        var traitOverride: UITraitCollection? = nil
        if size.width > 414 {
            traitOverride = UITraitCollection(horizontalSizeClass: .Regular)
        }
        setOverrideTraitCollection(traitOverride, forChildViewController: childViewControllers[0])

        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }

}
