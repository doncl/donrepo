import UIKit

class MasterViewController: UITableViewController, PaletteSelectionContainer {
    var detailViewController: DetailViewController? = nil
    var paletteCollection: ColorPaletteCollection = ColorPaletterProvider().rootCollection!

    override func awakeFromNib() {
        super.awakeFromNib()
        title = "Palettes"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers.last as? UINavigationController)?.topViewController as? DetailViewController
        }

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MasterViewController.handleShowDetailVCTargetChanged(_:)),
                name: UIViewControllerShowDetailTargetDidChangeNotification, object: nil)
    }

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self,
                name: UIViewControllerShowDetailTargetDidChangeNotification, object: nil)
    }

    func handleShowDetailVCTargetChanged(sender: AnyObject?) {
        if let indexes = tableView.indexPathsForVisibleRows {
            for indexPath in indexes {
                let cell = tableView.cellForRowAtIndexPath(indexPath)
                tableView(tableView, willDisplayCell: cell!, forRowAtIndexPath: indexPath)
            }
        }
    }

    // #pragma mark - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paletteCollection.children.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        let object = paletteCollection.children[indexPath.row]
        cell.textLabel?.text = object.name
        return cell
    }

    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,
                            forRowAtIndexPath indexPath: NSIndexPath) {

        var segueWillPush = false
        if rowHasChildrenAtIndex(indexPath) {
            segueWillPush = rwt_showVCWillResultInPush(self)
        } else {
            segueWillPush = rwt_showDetailVCWillResultInPush(self)
        }
        cell.accessoryType = segueWillPush ? .DisclosureIndicator : .None
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(rowHasChildrenAtIndex(indexPath)) {
            let childCollection = paletteCollection.children[indexPath.row] as! ColorPaletteCollection
            let newTable = storyboard?.instantiateViewControllerWithIdentifier("MasterVC") as! MasterViewController
            newTable.paletteCollection = childCollection
            newTable.title = childCollection.name
            showViewController(newTable, sender:self)
        }
    }

    // #pragma mark - Segues
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let detailNav = segue.destinationViewController as! UINavigationController
                let detailVC = detailNav.topViewController as! DetailViewController
                let palette = paletteCollection.children[indexPath.row] as! ColorPalette
                detailVC.colorPalette = palette
            }
        }
    }

    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            return !rowHasChildrenAtIndex(selectedIndexPath)
        }
        return false
    }

    // Private methods
    private func rowHasChildrenAtIndex(indexPath: NSIndexPath) -> Bool {
        let item = paletteCollection.children[indexPath.row]
        if let itemAsPalette = item as? ColorPaletteCollection {
            return true
        }
        return false
    }

    // MARK: - PaletteSelectionContainer
    func rwt_currentlySelectedPalette() -> ColorPalette? {
        if let indexPath = tableView.indexPathForSelectedRow {
            if !rowHasChildrenAtIndex(indexPath) {
                return paletteCollection.children[indexPath.row] as? ColorPalette
            }
        }
        return nil
    }
}
