import UIKit

class DetailViewController: UIViewController, PaletteDisplayContainer {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var colorLabels: [UILabel]!

    var masterPopoverController: UIPopoverController? = nil

    var colorPalette: ColorPalette? {
        didSet {
            // Update the view.
            self.configureView()

            if self.masterPopoverController != nil {
              self.masterPopoverController!.dismissPopoverAnimated(true)
            }
        }
    }

    private func configureView() {
        // Update the user interface for the detail item.
        if let palette = colorPalette {
            makeAllContentHidden(false)
            if (colorLabels != nil) {
                // Update the color panels
                for (color, label) in Zip2Sequence(palette.colors, colorLabels!) {
                    label.backgroundColor = color
                    label.text = color.hexString()
                    label.textColor = color.blackOrWhiteContrastingColor().colorWithAlphaComponent(0.6)
                }

                // And the title label
                title = palette.name
                titleLabel.text = palette.name

                // Central color
                let middleIndex = Int(floor(Double(palette.colors.count - 1) / 2.0))
                let middleColor = palette.colors[middleIndex]
                titleLabel.textColor = middleColor.blackOrWhiteContrastingColor().colorWithAlphaComponent(0.6)
            }
        } else {
            if let empty = storyboard?.instantiateViewControllerWithIdentifier("NoPaletteSelected") {
                showViewController(empty, sender: self)
            }
            makeAllContentHidden(true)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        if let svc = splitViewController {
            navigationItem.setLeftBarButtonItem(svc.displayModeButtonItem(), animated: true)
            navigationItem.leftItemsSupplementBackButton = true
            navigationItem.hidesBackButton = false
        }
    }

    // Private methods
    private func makeAllContentHidden(hidden: Bool) {
        for subview in view.subviews {
            subview.hidden = hidden
        }
        if hidden {
            title = ""
        }
    }

    // MARK: - PaletteDisplayContainer
    func rwt_currentlyDisplayedPalette() -> ColorPalette? {
        return colorPalette
    }
}
