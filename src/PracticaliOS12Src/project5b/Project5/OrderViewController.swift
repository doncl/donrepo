//
//  OrderViewController.swift
//  Project5
//
//  Created by Paul Hudson on 28/08/2018.
//  Copyright © 2018 Hacking with Swift. All rights reserved.
//

import Intents
import UIKit

class OrderViewController: UIViewController {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var details: UILabel!
    @IBOutlet var cost: UILabel!

    var cake: Product!
    var toppings = Set<Product>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /* HACK TO WORK AROUND UIIMAGEVIEW IGNORING TEMPLATE IMAGE TINT COLOR */
        /* See: https://openradar.appspot.com/18448072 */
        let image = imageView.image
        imageView.image = nil
        imageView.image = image
        /* END HACK */

        let newOrder = Order(cake: cake, toppings: toppings)
        showDetails(newOrder)
        send(newOrder)
        donate(newOrder)

        title = "All set!"
        navigationItem.hidesBackButton = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
    }

    // update our user interface with the order's name and price
    func showDetails(_ order: Order) {
        details.text = order.name
        cost.text = "$\(order.price)"
    }

    func send(_ order: Order) {
        // convert our order to JSON
        let encoder = JSONEncoder()

        do {
            let data = try encoder.encode(order)
            // send the order to a server somewhere
            print(data)
        } catch {
            print("Failed to create order.")
        }
    }

    func donate(_ order: Order) {
        let interaction = INInteraction(intent: order.intent, response: nil)

        interaction.donate { error in
            if let error = error {
                print(error)
            }
        }
    }

    @objc func done() {
        navigationController?.popToRootViewController(animated: true)
    }
}
