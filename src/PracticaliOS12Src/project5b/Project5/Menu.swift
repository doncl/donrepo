//
//  Menu.swift
//  Project5
//
//  Created by Paul Hudson on 31/08/2018.
//  Copyright © 2018 Hacking with Swift. All rights reserved.
//

import Foundation

class Menu {
    var cakes: [Product]
    var toppings: [Product]

    static let shared = Menu()

    private init() {
        cakes = Bundle.main.decode(from: "cupcakes.json")
        toppings = Bundle.main.decode(from: "toppings.json")
    }

    func findCake(from name: String?) -> Product? {
        return cakes.first { $0.name == name }
    }

    func findTopping(from name: String?) -> Product? {
        return toppings.first { $0.name.lowercased() == name }
    }
}

extension Bundle {
    func decode(from filename: String) -> [Product] {
        guard let json = url(forResource: filename, withExtension: nil) else {
            fatalError("Failed to locate \(filename) in app bundle.")
        }

        guard let jsonData = try? Data(contentsOf: json) else {
            fatalError("Failed to load \(filename) from app bundle.")
        }

        let decoder = JSONDecoder()

        guard let result = try? decoder.decode([Product].self, from: jsonData) else {
            fatalError("Failed to decode \(filename) from app bundle.")
        }

        return result.sorted {
            return $0.name < $1.name
        }
    }
}
