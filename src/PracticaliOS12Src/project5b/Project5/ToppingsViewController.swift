//
//  ToppingsViewController.swift
//  Project5
//
//  Created by Paul Hudson on 28/08/2018.
//  Copyright © 2018 Hacking with Swift. All rights reserved.
//

import UIKit

class ToppingsViewController: UITableViewController {
    var cake: Product!
    var selectedToppings = Set<Product>()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Add Toppings"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Place Order", style: .plain, target: self, action: #selector(placeOrder))
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Menu.shared.toppings.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let topping = Menu.shared.toppings[indexPath.row]
        cell.textLabel?.text = "\(topping.name) – $\(topping.price)"
        cell.detailTextLabel?.text = topping.description

        if selectedToppings.contains(topping) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else {
            fatalError("Unable to find the cell that was tapped.")
        }

        let topping = Menu.shared.toppings[indexPath.row]

        if cell.accessoryType == .checkmark {
            cell.accessoryType = .none
            selectedToppings.remove(topping)
        } else {
            cell.accessoryType = .checkmark
            selectedToppings.insert(topping)
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }

    @objc func placeOrder() {
        guard let orderViewController = storyboard?.instantiateViewController(withIdentifier: "OrderViewController") as? OrderViewController else {
            fatalError("Unable to load OrderViewController from storyboard.")
        }

        orderViewController.cake = cake
        orderViewController.toppings = selectedToppings
        navigationController?.pushViewController(orderViewController, animated: true)
    }
}
