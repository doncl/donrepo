//
//  Product.swift
//  Project5
//
//  Created by Paul Hudson on 28/08/2018.
//  Copyright © 2018 Hacking with Swift. All rights reserved.
//

import Foundation

struct Product: Codable, Hashable {
    var name: String
    var description: String
    var price: Int
}
