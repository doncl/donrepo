//
//  Order.swift
//  Project5
//
//  Created by Paul Hudson on 28/08/2018.
//  Copyright © 2018 Hacking with Swift. All rights reserved.
//

import Foundation

struct Order: Codable, Hashable {
    var cake: Product
    var toppings: Set<Product>

    var name: String {
        if toppings.count == 0 {
            return cake.name
        } else {
            let toppingNames = toppings.map { $0.name.lowercased() }
            return "\(cake.name), \(toppingNames.joined(separator: ", "))."
        }
    }

    var price: Int {
        return toppings.reduce(cake.price) { $0 + $1.price }
    }
}

extension Order {
    init?(from data: Data?) {
        guard let data = data else { return nil }

        let decoder = JSONDecoder()

        guard let savedOrder = try? decoder.decode(Order.self, from: data) else {
            return nil
        }

        cake = savedOrder.cake
        toppings = savedOrder.toppings
    }
}
