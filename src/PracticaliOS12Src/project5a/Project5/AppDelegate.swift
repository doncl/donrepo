//
//  AppDelegate.swift
//  Project5
//
//  Created by Paul Hudson on 28/08/2018.
//  Copyright © 2018 Hacking with Swift. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if userActivity.activityType == "com.hackingwithswift.project5a.order" {
            guard let navigationController = window?.rootViewController as? UINavigationController else {
                fatalError("Expected navigation controller as root view controller.")
            }

            guard let orderViewController = navigationController.storyboard?.instantiateViewController(withIdentifier: "OrderViewController") as? OrderViewController else {
                fatalError("Unable to load OrderViewController from storyboard.")
            }

            if let order = Order(from: userActivity.userInfo?["order"] as? Data) {
                orderViewController.cake = order.cake
                orderViewController.toppings = order.toppings
                navigationController.pushViewController(orderViewController, animated: false)
            }
        }

        return true
    }
}

