//
//  Painting.swift
//  Project4b
//
//  Created by Paul Hudson on 18/07/2018.
//  Copyright © 2018 Hacking with Swift. All rights reserved.
//

import Foundation

struct Painting: Codable {
    var title: String
    var artist: String
    var year: String
    var url: URL
}
