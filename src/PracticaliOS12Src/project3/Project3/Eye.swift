//
//  Eye.swift
//  Project3
//
//  Created by Paul Hudson on 12/07/2018.
//  Copyright © 2018 Hacking with Swift. All rights reserved.
//

import SceneKit
import UIKit

class Eye: SCNNode {
    let target = SCNNode()

    init(color: UIColor) {
        super.init()

        let geometry = SCNCylinder(radius: 0.005, height: 0.2)
        geometry.firstMaterial?.diffuse.contents = color

        let node = SCNNode(geometry: geometry)
        node.eulerAngles.x = -.pi / 2
        node.position.z = 0.1
        node.opacity = 0.5
        addChildNode(node)

        addChildNode(target)
        target.position.z = 1
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented.")
    }
}
