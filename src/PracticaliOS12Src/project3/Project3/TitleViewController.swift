//
//  TitleViewController.swift
//  Project3
//
//  Created by Paul Hudson on 13/07/2018.
//  Copyright © 2018 Hacking with Swift. All rights reserved.
//

import UIKit

class TitleViewController: UIViewController {
    override func viewDidLoad() {
        UIApplication.shared.isIdleTimerDisabled = true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "ViewController") {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
