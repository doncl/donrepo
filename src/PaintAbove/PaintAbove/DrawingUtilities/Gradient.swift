//
//  Gradient.swift
//  DrawingUtilities
//
//  Created by Don Clore on 7/9/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.

import UIKit
import QuartzCore
import CoreGraphics

typealias InterpolationBlock = (CGFloat) -> (CGFloat)

class Gradient: NSObject {
  
  private static let keepDrawing : CGGradientDrawingOptions =
    [.drawsBeforeStartLocation, .drawsAfterEndLocation]
  
  private static let twoPi : CGFloat = 2.0 * CGFloat.pi
  
  private var _storedGradient : CGGradient
  
  private init(_ wrappedGradient: CGGradient) {
    _storedGradient = wrappedGradient
  }
  
  var gradient : CGGradient {
    return _storedGradient
  }
 
  class func with(colors: [UIColor], andlocations locations: [CGFloat])
    -> Gradient? {
    
    guard colors.count > 0 && locations.count > 0 else {
      return nil
    }
    
    let space = CGColorSpaceCreateDeviceRGB()

    let colorArray = colors.map({$0.cgColor}) as CFArray
    
    // Create the actual gradient object
    guard let gradient = CGGradient(colorsSpace: space, colors: colorArray,
      locations: locations) else {
        
      return nil
    }
    
    let returnedGradient = Gradient(gradient)
    
    return returnedGradient
  }
  
  class func from(color1 : UIColor, to color2: UIColor) -> Gradient? {
    return Gradient.with(colors: [color1, color2], andlocations: [0.0, 1.0])
  }
  
  func drawRadial(from pt1: CGPoint, to pt2: CGPoint, radii: (CGPoint),
    style mask: CGGradientDrawingOptions) {
    
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    context.drawRadialGradient(gradient, startCenter: pt1, startRadius: radii.x,
      endCenter: pt2, endRadius: radii.y, options: mask)
  }
  
  //MARK: Linear
  
  func draw(from p1 : CGPoint, to p2 : CGPoint,
    style mask: CGGradientDrawingOptions) {
  
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    context.drawLinearGradient(gradient, start: p1, end: p2, options: mask)
  }
  
  func drawLeftToRight(rect: CGRect) {
    let p1 = rect.midLeft
    let p2 = rect.midRight
    
    draw(from: p1, to: p2, style: Gradient.keepDrawing)
  }
  
  func drawTopToBottom(rect: CGRect) {
    let p1 = rect.midTop
    let p2 = rect.midBottom
    
    draw(from: p1, to: p2, style: Gradient.keepDrawing)
  }
  
  func drawBottomToTop(rect: CGRect) {
    let p1 = rect.midBottom
    let p2 = rect.midTop
    
    draw(from: p1, to: p2, style: Gradient.keepDrawing)
  }
  
  func draw(from p1: CGPoint, to p2 : CGPoint) {
    draw(from: p1, to: p2, style: Gradient.keepDrawing)
  }
  
  func drawAlong(angle theta : CGFloat, inRect rect: CGRect) {
    let center = rect.center
    let r = center.distance(from: rect.topRight)
    
    var phi : CGFloat = theta + CGFloat.pi
    if phi > Gradient.twoPi {
      phi -= Gradient.twoPi
    }
    
    let dx1 = r * sin(theta)
    let dy1 = r * cos(theta)
    let dx2 = r * sin(phi)
    let dy2 = r * cos(phi)
    
    let p1 = CGPoint(x: center.x + dx1, y: center.y + dy1)
    let p2 = CGPoint(x: center.x + dx2, y: center.y + dy2)
    
    draw(from: p1, to: p2)
  }
  
  //MARK: Radial
  
  func drawBasicRadial(rect: CGRect) {
    let p1 = rect.center
    let r = rect.width / 2.0
    let radii = CGPoint(x: 0.0, y: r)
    
    drawRadial(from: p1, to: p1, radii: radii, style: Gradient.keepDrawing)
  }
  
  func drawRadial(from p1: CGPoint, to p2: CGPoint) {
    let dist = p1.distance(from: p2)
    let radii = CGPoint(x: 0.0, y: dist)
    drawRadial(from: p1, to: p1, radii: radii, style: Gradient.keepDrawing)
  }
  
  //MARK: Prebuilt
  class func rainbow() -> Gradient? {
    var colors : [UIColor] = []
    var locations : [CGFloat] = []
    
    let n = 24
    let nf = CGFloat(n)
    let nfminus1 = CGFloat(n - 1)
    
    for i in 0...n {
      let percent = CGFloat(i) / nf
      let colorDistance = percent * nfminus1 / nf
      let color = UIColor(hue: colorDistance, saturation: 1, brightness: 1,
        alpha: 1)
      
      colors.append(color)
      locations.append(percent)
    }
  
    return with(colors: colors, andlocations: locations)
  }
  
  class func linearGloss(color: UIColor) -> Gradient? {
    var r : CGFloat = 0
    var g : CGFloat = 0
    var b : CGFloat = 0
    var a : CGFloat = 0
    
    color.getRed(&r, green: &g, blue: &b, alpha: &a)
    
    let l : CGFloat = CGFloat(0.299) * r + CGFloat(0.587) * g + CGFloat(0.114) * b
    let gloss : CGFloat = pow(l, 0.2) * 0.5
    
    var h : CGFloat = 0
    var s : CGFloat = 0
    var v : CGFloat = 0
    color.getHue(&h, saturation: &s, brightness: &v, alpha: nil)
    s = min(s, 0.2)
    
    // Rotate by 0.6 PI
    let rHue : CGFloat = ((h < 0.95) && (h > 0.7)) ? 0.67 : 0.17
    let phi = rHue * CGFloat.pi * 2.0
    let theta = h * CGFloat.pi
    
    // Interpolate distance
    var dTheta = theta - phi
    while dTheta < 0.0 {
      dTheta += (CGFloat.pi * 2.0)
    }
    
    while dTheta > (CGFloat.pi * 2.0) {
      dTheta -= (CGFloat.pi / 2.0)
    }
    
    let factor : CGFloat = 0.7 + 0.3 * cos(dTheta)
    
    // Build highlight colors
    let c1 = UIColor(hue: h * factor + (1 - factor) * rHue, saturation: s,
      brightness: v * factor + (1 - factor), alpha: gloss)
    
    let c2 = c1.withAlphaComponent(0.0)
    
    // Build gradient
    let colors : [UIColor] = [
      UIColor(white: 1.0, alpha: gloss),
      UIColor(white: 1.0, alpha: 0.2),
      c2,
      c1,
    ]
    let locations : [CGFloat] = [0.0, 0.5, 0.5, 1.0]
    
    guard let gradient = Gradient.with(colors: colors,
      andlocations: locations) else {
        
      return nil
    }
    return gradient
  }
  
  class func using(interpolationBlock block : InterpolationBlock, between c1 : UIColor,
    and c2: UIColor) -> Gradient? {
    
    var colors : [UIColor] = []
    var locations : [CGFloat] = []

    let numberOfSamples = 24
    let nf = CGFloat(numberOfSamples)
    
    for i in 0...numberOfSamples {
      let amt = CGFloat(i) / nf
      let blockAmt = block(amt)
      let percentage = blockAmt.clamp(min: 0.0, max: 1.0)
      let interpolatedColor = UIColor.interpolate(between: c1, and: c2, by: percentage)
      colors.append(interpolatedColor)
      locations.append(amt)
    }
    
    let returnedGradient = with(colors: colors, andlocations: locations)
    return returnedGradient
  }
  
  class func easeIn(between c1 : UIColor, and c2 : UIColor) -> Gradient? {
    return using(interpolationBlock: { percent in
      return SadunEaseCurves.EaseIn(currentTime: percent, factor: 3)
    }, between: c1, and: c2)
  }
  
  class func easeInOut(between c1: UIColor, and c2: UIColor) -> Gradient? {
    return using(interpolationBlock: { percent in
      return SadunEaseCurves.EaseInOut(currentTime: percent, factor: 3)
    }, between: c1, and: c2)
  }
  
  class func easeOut(between c1: UIColor, and c2: UIColor) -> Gradient? {
    return using(interpolationBlock: { percent in
      return SadunEaseCurves.EaseOut(currentTime: percent, factor: 3)
    }, between: c1, and: c2)
  }
}




























