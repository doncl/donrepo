//
//  CGAffineTransformExtensions.swift
//  DrawingUtilities
//
//  Created by Don Clore on 6/30/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.

import QuartzCore

extension CGAffineTransform {
  var xScale : CGFloat {
    return sqrt(a * a + c * c)
  }
  
  var yScale : CGFloat {
    return sqrt(b * b + d * d)
  }
  
  // in radians
  var rotation : CGFloat {
    // Yes, I know I unroll everything; it makes it easier to debug if anything 
    // goes wrong with my assumptions.
    let floatB = Float(b)
    let floatA = Float(a)
    let floatRotation = atan2f(floatB, floatA)
    return CGFloat(floatRotation)
  }
}
