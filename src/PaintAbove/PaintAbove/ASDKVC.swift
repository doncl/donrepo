//
//  ASDKVC.swift
//  PaintAbove
//
//  Created by Don Clore on 1/30/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit
import AsyncDisplayKit

class ASDKVC: ASViewController<ASTableNode> {
  let items : [String] = [
    "blue",
    "orange",
    "red",
    "black",
    "grey",
    "white",
    "pink",
    ]

  let shadow : CALayer = CALayer()
  var tableNode : ASTableNode = ASTableNode(style: .plain)

  override func viewDidLoad() {
    super.viewDidLoad()
    view.addSubnode(tableNode)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    tableNode.backgroundColor = .white
 
    if tableNode.isNodeLoaded {
      tableNode.view.separatorStyle = .none
      tableNode.view.translatesAutoresizingMaskIntoConstraints = false
      NSLayoutConstraint.activate([
        tableNode.view.topAnchor.constraint(equalTo: view.topAnchor),
        tableNode.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
        tableNode.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        tableNode.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
      tableNode.delegate = self
      tableNode.dataSource = self
      tableNode.reloadData()
    }
    
  }
}


extension ASDKVC : ASTableDataSource {
  func numberOfSections(in tableNode: ASTableNode) -> Int {
    return 1
  }
  func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
    return items.count * 6
  }
  
  func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
    let index = indexPath.row % items.count
    let item = items[index]
    
    return {
      return Cell(item: item)
    }
  }
}

extension ASDKVC : ASTableDelegate {
  func tableNode(_ tableNode: ASTableNode, constrainedSizeForRowAt indexPath: IndexPath) -> ASSizeRange {
    return ASSizeRangeMake(CGSize(width: 375.0, height:60.0), CGSize(width: 414.0, height: 100.0))
  }
  
  func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
    assert(Thread.isMainThread)
    if let cell = tableNode.nodeForRow(at: indexPath) as? Cell {
      cell.isSelected = true
      shadow.frame = tableNode.rectForRow(at: indexPath)
      let targetSize = shadow.frame.size
      let rect = CGRect.from(size: targetSize)
      
      UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
      defer {
        UIGraphicsEndImageContext()
      }
      let path = UIBezierPath(rect: rect.insetBy(dx: 5.0, dy: 7.0))
      path.lineWidth = 5.0
      path.drawShadow(color: UIColor.lightGray, size: .zero, blur: 8.0)
      
      guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
        return
      }
      
      shadow.contents = image.cgImage
      shadow.masksToBounds = false
      
      if nil == shadow.superlayer {
        tableNode.layer.addSublayer(shadow)
        shadow.zPosition = 11.0
      }
      
      if indexPath.row > 0 {
        let aboveIndexPath = IndexPath(row: indexPath.row - 1, section: indexPath.section)
        if let aboveCell = tableNode.nodeForRow(at: aboveIndexPath) as? Cell {
          aboveCell.isAboveSelection = true
        }
      }
    }
  }
  
  func tableNode(_ tableNode: ASTableNode, didDeselectRowAt indexPath: IndexPath) {
    if let cell = tableNode.nodeForRow(at: indexPath) as? Cell {
      cell.isSelected = false
      if indexPath.row > 0 {
        let aboveIndexPath = IndexPath(row: indexPath.row - 1, section: indexPath.section)
        if let aboveCell = tableNode.nodeForRow(at: aboveIndexPath) as? Cell {
          aboveCell.isAboveSelection = false
        }
      }

    }
  }
}

class Cell : ASCellNode {
  var bottomLine : CALayer = CALayer()
  let text : ASTextNode = ASTextNode()
  
  var isAboveSelection : Bool = false {
    didSet {
      setBottomLine()
    }
  }
  
  override var isSelected : Bool {
    get {
      return super.isSelected
    }
    set {
      super.isSelected = newValue
      if newValue {
        bottomLine.removeFromSuperlayer()
      } else {
        setBottomLine()
      }
    }
  }
  
  init(item : String) {
    super.init()
    addSubnode(text)
    bottomLine.borderWidth = 0.3
    bottomLine.borderColor = UIColor.lightGray.cgColor
    selectionStyle = .none
    backgroundColor = .white
    text.attributedText = NSAttributedString(string: item, attributes: [
      NSAttributedStringKey.font : UIFont.preferredFont(forTextStyle: .headline) as Any,
      NSAttributedStringKey.foregroundColor : UIColor.black as Any,
    ])
    text.style.flexGrow = 1.0    
  }
  
  override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
    let hStack = ASStackLayoutSpec(direction: .horizontal, spacing: 0.0, justifyContent: .center,
      alignItems: .center, children: [text])
    
    let asStack = ASStackLayoutSpec(direction: .vertical, spacing: 0.0, justifyContent: .center,
      alignItems: .center, children: [hStack])
    
    return asStack
  }
  
  override func layout() {
    super.layout()
    assert(Thread.isMainThread)
    setBottomLine()
  }
  
  fileprivate func setBottomLine() {
    if isSelected || isAboveSelection {
      bottomLine.removeFromSuperlayer()
      return
    }
    bottomLine.frame = CGRect(x: layer.bounds.origin.x,
                              y: layer.bounds.origin.y + layer.bounds.height - 0.5, width: layer.bounds.width, height: 0.5)
    if nil == bottomLine.superlayer {
      layer.addSublayer(bottomLine)
      bottomLine.setNeedsDisplay()
      layer.setNeedsDisplay()
    }
  }
  
}
