//
//  ViewController.swift
//  PaintAbove
//
//  Created by Don Clore on 1/29/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  let items : [String] = [
    "blue",
    "orange",
    "red",
    "black",
    "grey",
    "white",
    "pink",
    ]

  
  
  @IBOutlet var table: UITableView!
  
  let shadow : CALayer = CALayer()
  let imageView : UIImageView = UIImageView()
  override func viewDidLoad() {
    super.viewDidLoad()
    table.backgroundColor = .white
    table.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    table.register(DrawAboveCell.self, forCellReuseIdentifier: "cellId")
    table.allowsSelection = true
    table.allowsMultipleSelection = false
    table.separatorStyle = .none
    table.delegate = self
    table.dataSource = self
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

extension ViewController : UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60.0
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let cell = tableView.cellForRow(at: indexPath) as? DrawAboveCell {
      cell.isSelected = true
      
      let inset = tableView.contentInset
      let rowRect = tableView.rectForRow(at: indexPath)
      var r = rowRect.offsetBy(dx: -5, dy: 0)
      
      let targetSize = r.size
      let rect = CGRect.from(size: targetSize)
      
      UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
      defer {
        UIGraphicsEndImageContext()
      }
      let path = UIBezierPath(rect: rect.insetBy(dx: 5.0, dy: 7.0))
      path.lineWidth = 5.0
      path.drawShadow(color: UIColor.lightGray, size: .zero, blur: 8.0)

      guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
        return
      }

      shadow.frame = r
      shadow.contents = image.cgImage
      shadow.masksToBounds = false

      if nil == shadow.superlayer {
        tableView.layer.addSublayer(shadow)
        shadow.zPosition = 11.0
      }
//      if nil == imageView.superview {
//        tableView.addSubview(imageView)
//      }
//      imageView.frame = r
//      tableView.bringSubview(toFront: imageView)
//      imageView.image = image
      
      if indexPath.row > 0 {
        let aboveIndexPath = IndexPath(row: indexPath.row - 1, section: indexPath.section)
        if let aboveCell = tableView.cellForRow(at: aboveIndexPath) as? DrawAboveCell {
          aboveCell.isAboveSelection = true
        }
      }
    }
  }
  
  func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    if let cell = tableView.cellForRow(at: indexPath) as? DrawAboveCell {
      cell.isSelected = false
      if indexPath.row > 0 {
        let aboveIndexPath = IndexPath(row: indexPath.row - 1, section: indexPath.section)
        if let aboveCell = tableView.cellForRow(at: aboveIndexPath) as? DrawAboveCell {
          aboveCell.isAboveSelection = false
        }
      }
    }
  }
}

extension ViewController : UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count * 5
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellId") as? DrawAboveCell else {
      fatalError("whoops")
    }
    let itemIdx : Int = indexPath.item % items.count
    cell.val = items[itemIdx]
    return cell
  }
}

