//
//  DrawAboveCell.swift
//  PaintAbove
//
//  Created by Don Clore on 1/29/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit


class DrawAboveCell: UITableViewCell {
  var bottomLine : CALayer = CALayer()
  
  var val : String? {
    set {
      label.text = newValue
    }
    get {
      return label.text
    }
  }
  
  var isAboveSelection : Bool = false {
    didSet {
      setBottomLine()
    }
  }
  
  fileprivate func setBottomLine() {
    if isSelected || isAboveSelection {
      bottomLine.removeFromSuperlayer()
      return
    }
    bottomLine.frame = CGRect(x: layer.bounds.origin.x,
      y: layer.bounds.origin.y + layer.bounds.height - 0.5, width: layer.bounds.width, height: 0.5)
    if nil == bottomLine.superlayer {
      layer.addSublayer(bottomLine)
      bottomLine.setNeedsDisplay()
      layer.setNeedsDisplay()
    }
  }
  
  override var isSelected : Bool {
    get {
      return super.isSelected
    }
    set {
      super.isSelected = newValue
      if newValue {
        bottomLine.removeFromSuperlayer()
      } else {
        setBottomLine()
      }
    }
  }
  
  
  let label : UILabel = UILabel()
  let shadowLayer : CALayer = CALayer()
  let topShadow : CALayer = CALayer()
  let shapeLayer : CAShapeLayer = CAShapeLayer()
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    bottomLine.borderWidth = 0.3
    bottomLine.borderColor = UIColor.lightGray.cgColor
    selectionStyle = .none
    backgroundColor = .white
    label.backgroundColor = .white
    contentView.addSubview(label)
    label.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      contentView.topAnchor.constraint(equalTo: self.topAnchor),
      contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
      contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
      contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
      label.topAnchor.constraint(equalTo: contentView.topAnchor),
      label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
      label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
      label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
    ])
    label.textAlignment = .center
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    setBottomLine()
  }
}
