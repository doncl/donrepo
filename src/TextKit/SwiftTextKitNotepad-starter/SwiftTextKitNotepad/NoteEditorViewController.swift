//
//  NoteEditorViewController.swift
//  SwiftTextKitNotepad
//
//  Created by Gabriel Hauber on 18/07/2014.
//  Copyright (c) 2014 Gabriel Hauber. All rights reserved.
//

import UIKit

class NoteEditorViewController: UIViewController, UITextViewDelegate {

    var textView: UITextView!
    var textStorage: SyntaxHighlightTextStorage!
    
    var timeView: TimeIndicatorView!

    var note: Note!

    override func viewDidLoad() {
        super.viewDidLoad()
        createTextView()
        
        NotificationCenter.default.addObserver(self,
                selector: #selector(NoteEditorViewController.preferredContentSizeChanged(_:)),
                name: UIContentSizeCategory.didChangeNotification, object: nil)

        timeView = TimeIndicatorView(date: note.timestamp)
        textView.addSubview(timeView)

      NotificationCenter.default.addObserver(self, selector: #selector(NoteEditorViewController.keyboardDidShow(_:)),
                                             name: UIResponder.keyboardDidShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(NoteEditorViewController.keyboardDidHide(_:)),
                                               name: UIResponder.keyboardDidHideNotification, object: nil)
    }

    func updateTextViewSizeForKeyboardHeight(keyboardHeight: CGFloat) {
        textView.frame = CGRect(x: 0, y: 0, width: view.frame.width,
                height: view.frame.height - keyboardHeight)
    }

    @objc func keyboardDidShow(_ notification: NSNotification) {
      if let rectValue = notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue {
        let keyboardSize = rectValue.cgRectValue.size
        updateTextViewSizeForKeyboardHeight(keyboardHeight: keyboardSize.height)
        }
    }

    @objc func keyboardDidHide(_ notification: NSNotification) {
      updateTextViewSizeForKeyboardHeight(keyboardHeight: 0)
    }

    func createTextView() {
        // 1. Create the text storage that backs the editor.
      let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)]
        let attrString = NSAttributedString(string: note.contents, attributes: attrs)
        textStorage = SyntaxHighlightTextStorage()
      textStorage.append(attrString)
        
        let newTextViewRect = view.bounds
        
        // 2. Create the layout manager.
        let layoutManager = NSLayoutManager()
        
        // 3. Create a text container
      let containerSize = CGSize(width: newTextViewRect.width, height: CGFloat.greatestFiniteMagnitude)
        let container = NSTextContainer(size: containerSize)
        container.widthTracksTextView = true
        layoutManager.addTextContainer(container)
        textStorage.addLayoutManager(layoutManager)
        
        
        // 4. Create a UITextView
        textView = UITextView(frame: newTextViewRect, textContainer: container)
        textView.delegate = self
        view.addSubview(textView)

      textView.isScrollEnabled = true
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        note.contents = textView.text
    }

    @objc func preferredContentSizeChanged(_ notification: NSNotification) {
        textStorage.update()
        updateTimeIndicatorFrame()
    }

    override func viewDidLayoutSubviews() {
        updateTimeIndicatorFrame()
        textView.frame = view.bounds
    }

    func updateTimeIndicatorFrame() {
        timeView.updateSize()
      timeView.frame = timeView.frame.offsetBy(dx: textView.frame.width - timeView.frame.width, dy: 0)

      let exclusionPath = timeView.curvePathWithOrigin(origin: timeView.center)
        textView.textContainer.exclusionPaths = [exclusionPath]
    }
}
