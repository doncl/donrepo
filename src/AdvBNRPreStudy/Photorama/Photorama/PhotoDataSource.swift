//
//  PhotoDataSource.swift
//  Photorama
//
//  Created by Don Clore on 2/17/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit

class PhotoDataSource: NSObject, UICollectionViewDataSource {
    var photos = [Photo]()

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int)
                    -> Int {
        return photos.count
    }

    func collectionView(collectionView: UICollectionView,
        cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let id = "UICollectionViewCell"
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(id,
                forIndexPath: indexPath) as! PhotoCollectionViewCell

        let photo = photos[indexPath.row]
        cell.updateWithImage(photo.image)

        return cell
    }
}
