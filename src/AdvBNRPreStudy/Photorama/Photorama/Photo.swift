//
//  Photo.swift
//  Photorama
//
//  Created by Don Clore on 2/18/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit
import CoreData

class Photo: NSManagedObject {
    var image: UIImage?
    
    override func awakeFromInsert() {
        super.awakeFromInsert()
        // Give the properties their initial values
        title = ""
        photoID = ""
        remoteURL = NSURL()
        photoKey = NSUUID().UUIDString
        dateTaken = NSDate()
    }

    func addTagObject(tag: NSManagedObject) {
        let currentTags = mutableSetValueForKey("tags")
        currentTags.addObject(tag)
    }

    func removeTagObject(tag: NSManagedObject) {
        let currentTags = mutableSetValueForKey("tags")
        currentTags.removeObject(tag)
    }
}
