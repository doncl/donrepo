//
//  TagsViewController.swift
//  Photorama
//
//  Created by Don Clore on 2/18/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit
import CoreData

class TagsViewController: UITableViewController{
    var store: PhotoStore!
    var photo: Photo!
    
    var selectedIndexPaths = [NSIndexPath]()

    let tagDataSource = TagDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = tagDataSource
        tableView.delegate = self
        updateTags()
    }

    func updateTags() {
        let tags = try! store.fetchMainQueueTags(predicate: nil,
                sortDescriptors: [NSSortDescriptor(key: "name", ascending: true)])

        tagDataSource.tags = tags

        for tag in photo.tags {
            if let index = tagDataSource.tags.indexOf(tag) {
                let indexPath = NSIndexPath(forRow: index, inSection: 0)
                selectedIndexPaths.append(indexPath)
            }
        }
    }

    override func tableView(tableView: UITableView,
                            didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let tag = tagDataSource.tags[indexPath.row]

        if let index = selectedIndexPaths.indexOf(indexPath) {
            selectedIndexPaths.removeAtIndex(index)
            photo.removeTagObject(tag)
        } else {
            selectedIndexPaths.append(indexPath)
            photo.addTagObject(tag)
        }

        tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)

        do {
            try store.coreDataStack.saveChanges()
        } catch let error {
            print("Core Data save failed: \(error)")
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,
        forRowAtIndexPath indexPath: NSIndexPath) {
        
        if selectedIndexPaths.indexOf(indexPath) != nil {
            cell.accessoryType = .Checkmark
        } else {
            cell.accessoryType = .None
        }
    }

    @IBAction func done(sender: AnyObject) {
        presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func addNewTag(sender: AnyObject) {
        let ac = UIAlertController(title: "Add Tag", message:nil, preferredStyle: .Alert)
        ac.addTextFieldWithConfigurationHandler({(textField) -> Void in
            textField.placeholder = "tag name"
            textField.autocapitalizationType = .Words
        })
        let ok = UIAlertAction(title: "OK", style: .Default, handler: {(action) -> Void in
            if let tagName = ac.textFields?.first!.text {
                let context = self.store.coreDataStack.mainQueueContext
                let newTag = NSEntityDescription.insertNewObjectForEntityForName("Tag",
                        inManagedObjectContext: context)
                newTag.setValue(tagName, forKey: "name")

                do {
                    try self.store.coreDataStack.saveChanges()
                } catch let error {
                    print("Core Data save failed: \(error)")
                }
                self.updateTags()
                self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Automatic)
            }
        })
        ac.addAction(ok)

        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        ac.addAction(cancelAction)

        presentViewController(ac, animated: true, completion: nil)
    }
}
