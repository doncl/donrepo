//
//  SimpleTransitioner.swift
//  CustomPresentation
//
//  Created by Don Clore on 4/24/16.
//  Copyright © 2016 Fresh App Factory. All rights reserved.
//

import UIKit

class SimpleAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
    var isPresentation : Bool = false
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?)
        -> NSTimeInterval {
            
        return 0.5
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        let fromView = fromViewController!.view
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
        let toView = toViewController!.view
        
        let containerView: UIView = transitionContext.containerView()!
        if isPresentation {
            containerView.addSubview(toView)
        }
        
        // 1
        let animatingViewController = isPresentation ? toViewController : fromViewController
        let animatingView = animatingViewController!.view
        
        // 2
        let appearedFrame = transitionContext.finalFrameForViewController(animatingViewController!)
        var dismissedFrame = appearedFrame
        dismissedFrame.origin.y += dismissedFrame.size.height
        
        // 3
        let initialFrame = isPresentation ? dismissedFrame : appearedFrame
        let finalFrame = isPresentation ? appearedFrame : dismissedFrame
        animatingView.frame = initialFrame
        
        UIView.animateWithDuration(transitionDuration(transitionContext), delay: 0.0,
            usingSpringWithDamping: 300.0, initialSpringVelocity: 5.0,
            options: [.AllowUserInteraction, .BeginFromCurrentState], animations: {
                animatingView.frame = finalFrame
            }, completion: {(value: Bool) in
                if !self.isPresentation {
                    fromView.removeFromSuperview()
                }
                transitionContext.completeTransition(true)
            })
    }
}

class SimpleTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {

    func presentationControllerForPresentedViewController(presented: UIViewController,
        presentingViewController presenting: UIViewController,
        sourceViewController source: UIViewController) -> UIPresentationController? {

        let presentationController =
        SimplePresentationController(presentedViewController: presented,
                presentingViewController: presenting)
        return presentationController
    }

    func animationControllerForPresentedController(presented: UIViewController,
        presentingController presenting: UIViewController,
        sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = SimpleAnimatedTransitioning()
        animationController.isPresentation = true

        return animationController
    }
}

























