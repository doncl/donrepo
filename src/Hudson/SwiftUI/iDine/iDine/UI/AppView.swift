//
//  AppView.swift
//  iDine
//
//  Created by Don Clore on 12/24/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct AppView: View {
  var body: some View {
    TabView {
      ContentView()
        .tabItem {
          Image(systemName: "list.dash")
          Text("Menu")
      }
      OrderView()
        .tabItem {
          Image(systemName: "square.and.pencil")
          Text("Order")
      }
      FavoritesView()
        .tabItem {
          Image(systemName: "square.and.pencil")
          Text("Favorites")
      }
    }
  }
}

struct AppView_Previews: PreviewProvider {
  static let order = Order()
  
  static var previews: some View {
    AppView().environmentObject(order)
  }
}
