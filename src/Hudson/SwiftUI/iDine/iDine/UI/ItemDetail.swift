//
//  ItemDetail.swift
//  iDine
//
//  Created by Don Clore on 12/24/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ItemDetail: View {
  @EnvironmentObject var order: Order
  
  var item: MenuItem
  
  var body: some View {
    VStack {
      ZStack(alignment: .bottomTrailing) {
        Image(item.mainImage)
        Text("Photo: \(item.photoCredit)")
          .padding(4)
          .background(Color.black)
          .font(.caption)
          .foregroundColor(.white)
          .offset(x: -5, y: -5)
      }
      Text(item.description)
        .padding()
      Spacer()
      Button("Order This") {
        self.order.add(item: self.item)
      }
      .font(.headline)
      .foregroundColor(.blue)
      .background(Color.white)
      Spacer()
    }
    .navigationBarTitle(Text(item.name), displayMode: .inline)
    .navigationBarItems(trailing:
      Button("Add to Favorites") {
        self.order.add(favorite: self.item)
    })
  }
}

struct ItemDetail_Previews: PreviewProvider {
  static let order = Order()
  
  static var previews: some View {
    NavigationView {
      ItemDetail(item: MenuItem.example).environmentObject(order)
    }
  }
}
