//
//  FavoritesView.swift
//  iDine
//
//  Created by Don Clore on 12/25/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

import SwiftUI

struct FavoritesView: View {
  @EnvironmentObject var order: Order
  
  var body: some View {
    NavigationView {
      List {
        Section {
          ForEach(order.favorites) { item in
            HStack {
              Text(item.name)
              Spacer()
              Text("$\(item.price)")
              Button("Add to order") {
                self.order.add(item: item)
              }
            }
          }.onDelete(perform: deleteItems)
        }
        Section {
          NavigationLink(destination: CheckoutView()) {
            Text("Place Order")
          }.disabled(order.favorites.isEmpty)
        }
      }
      .navigationBarTitle("Favorites")
      .listStyle(GroupedListStyle())
      .navigationBarItems(trailing: EditButton())
    }
  }
  
  func deleteItems(at offsets: IndexSet) {
    order.favorites.remove(atOffsets: offsets)
  }
}

struct FavoritesView_Previews: PreviewProvider {
  static let order = Order()
  
  static var previews: some View {
    FavoritesView().environmentObject(order)
  }
}
