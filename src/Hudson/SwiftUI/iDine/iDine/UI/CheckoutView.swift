//
//  CheckoutView.swift
//  iDine
//
//  Created by Don Clore on 12/24/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct CheckoutView: View {
  static let paymentTypes = ["Cash", "Credit Card", "iDine Points"]
  static let pickupTimes = ["Now", "Tonight", "Tomorrow Morning"]
  static let tipAmounts = [10, 15, 20, 25, 0]
  
  @EnvironmentObject var order: Order
  @State private var paymentType: Int = 0
  @State private var addLoyaltyDetails: Bool = false
  @State private var loyaltyNumber: String = ""
  @State private var tipAmount: Int = 1
  @State private var showingPaymentAlert: Bool = false
  @State private var pickupTime: Int = 0
  
  var totalPrice: Double {
    let total = Double(order.total)
    let tipValue = total / 100 * Double(Self.tipAmounts[tipAmount])
    return total + tipValue
  }
  
  var body: some View {
    Form {
      Section {
        Picker("How do you want to pay?", selection: $paymentType) {
          ForEach(0 ..< Self.paymentTypes.count) {
            Text(Self.paymentTypes[$0])
          }
        }
        Toggle(isOn: $addLoyaltyDetails.animation()) {
          Text("Add iDine loyalty card")
        }
        if addLoyaltyDetails {
          TextField("Enter your iDine ID", text: $loyaltyNumber)
        }
      }
      Section(header: Text("Add a tip?")) {
        Picker("Percentage:", selection: $tipAmount) {
          ForEach(0 ..< Self.tipAmounts.count) {
            Text("\(Self.tipAmounts[$0])%")
          }
        }.pickerStyle(SegmentedPickerStyle())
      }
      Section(header: Text("Pickup Time").font(.largeTitle)) {
        Picker("Pickup Time:", selection: $pickupTime) {
          ForEach(0 ..< Self.pickupTimes.count) {
            Text("\(Self.pickupTimes[$0])")
          }
        }.pickerStyle(SegmentedPickerStyle())
      }
      Section(header: Text("TOTAL: $\(totalPrice, specifier: "%.2f")").font(.largeTitle)) {
        Button("Confirm order") {
          self.showingPaymentAlert.toggle()
        }
      }
    }
    .navigationBarTitle(Text("Payment"), displayMode: .inline)
    .alert(isPresented: $showingPaymentAlert, content: {
      Alert(title: Text("Order confirmed"), message: Text("Your total was $\(totalPrice, specifier: "%.2f") - thank you!"), dismissButton: .default(Text("OK")))
    })
  }
}

struct CheckoutView_Previews: PreviewProvider {
  static let order = Order()
  
  static var previews: some View {
    CheckoutView().environmentObject(order)
  }
}
