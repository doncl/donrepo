//
//  ContentView.swift
//  ListWithSections
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    List {
      Section(header: Text("Important tasks")) {
        TaskRow()
        TaskRow()
        TaskRow()
      }
      Section(header: Text("Other tasks"), footer: Text("The End")) {
        TaskRow()
        TaskRow()
        TaskRow()
      }
    }.listStyle(GroupedListStyle())
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
