//
//  TaskRow.swift
//  ListWithSections
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct TaskRow: View {
  var body: some View {
    Text("Task data goes here")
  }
}

struct TaskRow_Previews: PreviewProvider {
  static var previews: some View {
    TaskRow()
  }
}
