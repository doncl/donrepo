//
//  ContentView.swift
//  DynamicList
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    let first = Restaurant(name: "Joe's Original")
    let second = Restaurant(name: "The Real Joe's Original")
    let third = Restaurant(name: "Original Joe's")
    let restaurants = [first, second, third]

    return List(restaurants) { restaurant in
      RestaurantRow(restaurant: restaurant)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
