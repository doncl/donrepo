//
//  Restaurant.swift
//  DynamicList
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import Foundation

struct Restaurant: Identifiable {
  var id = UUID()
  var name: String 
}
