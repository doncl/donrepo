//
//  ContentView.swift
//  StaticList
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    List {
      RestaurantRow(name: "Joe's Original")
      RestaurantRow(name: "Gates & Sons")
      RestaurantRow(name: "Exquisite Vomit-flavors")
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
