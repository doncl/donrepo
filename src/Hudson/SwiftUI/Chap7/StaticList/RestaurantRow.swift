//
//  RestaurantRow.swift
//  StaticList
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct RestaurantRow: View {
  var name: String
  
  var body: some View {
    Text("Restaurant: \(name)")
  }
}

struct RestaurantRow_Previews: PreviewProvider {
    static var previews: some View {
      RestaurantRow(name: "V's")
    }
}
