//
//  ContentView.swift
//  ImplicitStacking
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct User: Identifiable {
  var id = UUID()
  var username = "Anonymous"
}

struct ContentView: View {
  let users: [User] = [User(), User(), User()]
  
  var body: some View {
    List(users) { user in
      Image("tardis2").resizable().frame(width: 40, height: 40)
      Text(user.username)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
