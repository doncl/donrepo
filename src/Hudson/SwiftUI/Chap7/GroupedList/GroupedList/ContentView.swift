//
//  ContentView.swift
//  GroupedList
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ExampleRow: View {
  var body: some View {
    Text("Example Row")
  }
}

struct ContentView: View {
  var body: some View {
    List {
      Section(header: Text("Examples")) {
        ExampleRow()
        ExampleRow()
        ExampleRow()
      }
    }.listStyle(GroupedListStyle())
  }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
