//
//  ContentView.swift
//  EditList
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var users: [String] = ["Paul", "John", "Ringo", "George"]
  
  var body: some View {
    NavigationView {
      List {
        ForEach(users, id: \.self) { user in
          Text(user)
        }
        .onDelete(perform: delete)
      }
    .navigationBarItems(trailing: EditButton())
    }
  }
  
  func delete(at offsets: IndexSet) {
    users.remove(atOffsets: offsets)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
