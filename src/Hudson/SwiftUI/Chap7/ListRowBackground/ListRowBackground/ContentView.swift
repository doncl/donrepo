//
//  ContentView.swift
//  ListRowBackground
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      Text("Red List")
      List {
        ForEach(0..<5) {
          Text("Row \($0)")
        }
        .listRowBackground(Color.red)
      }
      Text("Tardis List")
      List {
        ForEach(0..<5) {
          Text("Row \($0)")
        }
        .listRowBackground(Image("tardis2").aspectRatio(contentMode: .fit).scaleEffect(0.2))
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
