//
//  ContentView.swift
//  InstrumentsSwiftUI
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import Combine
import SwiftUI

class FrequentUpdater: ObservableObject {
  let objectWillChange = PassthroughSubject<Void, Never>()
  var timer: Timer?
  
  init() {
    timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true, block: { _ in
      self.objectWillChange.send()
    })
  }
}

struct ContentView: View {
  @ObservedObject var updater = FrequentUpdater()
  @State private var tapCount = 0
  
  var body: some View {
    VStack {
      Text("\(UUID().uuidString)")
      
      Button(action: {
        self.tapCount += 1
      }) {
        Text("Tap count: \(tapCount)")
      }
    }

  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
