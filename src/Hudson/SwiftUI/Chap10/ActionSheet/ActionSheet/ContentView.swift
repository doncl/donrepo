//
//  ContentView.swift
//  ActionSheet
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var showingSheet: Bool = false
  
  var body: some View {
    Button( action: {
      self.showingSheet = true
    }) {
      Text("Show Action Sheet")
    }.actionSheet(isPresented: $showingSheet) {
      ActionSheet(title: Text("What do you want to do?"), message: Text("There's only one choice..."),
                  buttons: [.default(Text("Dismiss Action Sheet"))])
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
