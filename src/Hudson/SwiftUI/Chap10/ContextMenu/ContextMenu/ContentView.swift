//
//  ContentView.swift
//  ContextMenu
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    Text("Options")
      .contextMenu {
        Button(action: {
          // change country setting
        }) {
          Text("Choose Country")
          Image(systemName: "globe")
        }
        Button(action: {
          // enable geolocation
        }) {
          Text("Detect Location")
          Image(systemName: "location.circle")
        }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
