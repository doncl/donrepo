//
//  ContentView.swift
//  MultipleAlerts
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var showingAlert1: Bool = false
  @State private var showingAlert2: Bool = false
  
  var body: some View {
    VStack {
      Button("Show 1") {
        self.showingAlert1 = true
      } // If you move these Alerts to the VStack, only one will show, which is the point of this example - don't do that. 
      .alert(isPresented: $showingAlert1) {
        Alert(title: Text("one"), message: nil, dismissButton: .cancel())
      }
      Button("Show 2") {
        self.showingAlert2 = true
      }
      .alert(isPresented: $showingAlert2) {
        Alert(title: Text("two"), message: nil, dismissButton: .cancel())
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
