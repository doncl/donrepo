//
//  ContentView.swift
//  BasicAlert
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var showingAlert: Bool = false
  
  var body: some View {
    Button(action: {
      self.showingAlert = true
    }) {
      Text("ShowAlert")
    }
    .alert(isPresented: $showingAlert, content: {
      Alert(title: Text("ImportantMessage"), message: Text("Wear sunscreen"), dismissButton: .default(Text("Got it!")))
    })
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
