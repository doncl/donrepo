//
//  ContentView.swift
//  AddActionsToAlertButtons
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var showingAlert: Bool = false
  
  var body: some View {
    Button(action: {
      self.showingAlert = true
    }) {
      Text("Showing Alert")
    }
    .alert(isPresented: $showingAlert) {
      Alert(title: Text("Are you sure you want to delete this?"),
            message: Text("There is no undo"),
            primaryButton: .destructive(Text("Delete")) {
              print("Deleting...")
        }, secondaryButton: .cancel() )
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
