//
//  ContentView.swift
//  UIBezierPathAndCGPath
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ScaledBezier: Shape {
  let bezierPath: UIBezierPath
  
  func path(in rect: CGRect) -> Path {
    let path = Path(bezierPath.cgPath)
    
    // Figure out how much bigger we need to make our path in order for it to fill the available space without
    // clipping.
    let multiplier = min(rect.width, rect.height)
    
    // Create an affine transform that uses the multipler for both dimensions equally.
    let transform = CGAffineTransform(scaleX: multiplier, y: multiplier)
    
    // Apply that scale and send back the result
    return path.applying(transform)
  }
}

struct ContentView: View {
  var body: some View {
    ScaledBezier(bezierPath: .logo)
      .stroke(lineWidth: 2)
      .frame(width: 200, height: 200)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}


// Copied directly from Hudson's book, there was no learning value for me to retype or think about this.
extension UIBezierPath {
    /// The Unwrap logo as a Bezier path.
    static var logo: UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0.534, y: 0.5816))
        path.addCurve(to: CGPoint(x: 0.1877, y: 0.088), controlPoint1: CGPoint(x: 0.534, y: 0.5816), controlPoint2: CGPoint(x: 0.2529, y: 0.4205))
        path.addCurve(to: CGPoint(x: 0.9728, y: 0.8259), controlPoint1: CGPoint(x: 0.4922, y: 0.4949), controlPoint2: CGPoint(x: 1.0968, y: 0.4148))
        path.addCurve(to: CGPoint(x: 0.0397, y: 0.5431), controlPoint1: CGPoint(x: 0.7118, y: 0.5248), controlPoint2: CGPoint(x: 0.3329, y: 0.7442))
        path.addCurve(to: CGPoint(x: 0.6211, y: 0.0279), controlPoint1: CGPoint(x: 0.508, y: 1.1956), controlPoint2: CGPoint(x: 1.3042, y: 0.5345))
        path.addCurve(to: CGPoint(x: 0.6904, y: 0.3615), controlPoint1: CGPoint(x: 0.7282, y: 0.2481), controlPoint2: CGPoint(x: 0.6904, y: 0.3615))
        return path
    }
}


