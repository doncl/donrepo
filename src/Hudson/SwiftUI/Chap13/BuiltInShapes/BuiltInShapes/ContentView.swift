//
//  ContentView.swift
//  BuiltInShapes
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    ZStack {
      Rectangle()
        .fill(Color.black)
        .frame(width: 200, height: 200)
      
      RoundedRectangle(cornerRadius: 25, style: .continuous)
        .fill(Color.red)
        .frame(width: 200, height: 200)
      
      Capsule()
        .fill(Color.green)
        .frame(width: 100, height: 50)
      
      Ellipse()
        .fill(Color.blue)
        .frame(width: 100, height: 50)
      
      Circle()
        .fill(Color.white)
        .frame(width: 100, height: 50)  // uses the smaller dimension and throws away the larger.
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
