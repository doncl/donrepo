//
//  ContentView.swift
//  DrawCheckerboard
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct Checkerboard: Shape {
  let rows: Int
  let columns: Int
  
  func path(in rect: CGRect) -> Path {
    var path = Path()
    
    // figure out how big each row/column has to be
    let rowSize = rect.height / CGFloat(rows)
    let columnSize = rect.width / CGFloat(columns)
    
    // loop over all rows and columns, making alternating squares colored
    for row in 0 ..< rows {
      for column in 0 ..< columns {
        if (row + column).isMultiple(of: 2) {
          let startX = columnSize * CGFloat(column)
          let startY = rowSize * CGFloat(row)
          
          let rect = CGRect(x: startX, y: startY, width: columnSize, height: rowSize)
          path.addRect(rect)
        }
      }
    }
    return path
  }
}

struct ContentView: View {
  var body: some View {
    Checkerboard(rows: 16, columns: 16)
      .fill(Color.red)
      .frame(width: 200, height: 200)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
