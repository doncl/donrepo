//
//  ContentView.swift
//  DrawCustomPath
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct SpiroSquare: Shape {
  func path(in rect: CGRect) -> Path {
    var path = Path()
    
    let rotations = 5
    let amount = .pi / CGFloat(rotations)
    let transform = CGAffineTransform(rotationAngle: amount)
    
    for _ in 0 ..< rotations {
      path = path.applying(transform)
      path.addRect(CGRect(x: -rect.width / 2, y: -rect.height / 2, width: rect.width, height: rect.height))
    }
    return path
  }
}

struct ContentView: View {
  var body: some View {
    SpiroSquare()
      .stroke()
      .frame(width: 200, height: 200)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
