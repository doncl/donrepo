//
//  ContentView.swift
//  PolygonsAndStars
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct Star: Shape {
  let corners: Int
  let smoothness: CGFloat
  
  func path(in rect: CGRect) -> Path {
    guard corners >= 2 else {
      return Path()
    }
    
    let center = CGPoint(x: rect.width / 2, y: rect.height / 2)
    
    var currentAngle = -CGFloat.pi / 2
    
    // calculate how much we need to move with each start corner
    let angleAdjustment = .pi * 2 / CGFloat(corners * 2)
    
    // figure out how much we need to move X/Y for the innter points of the star
    let innerX = center.x * smoothness
    let innerY = center.y * smoothness
    
    var path = Path()
    
    // move to our initial position
    path.move(to: CGPoint(x: center.x * cos(currentAngle), y: center.y * sin(currentAngle)))
    
    // track the lowest point we draw to, so we can center later
    var bottomEdge: CGFloat = 0
    
    // lop over all our points/inner points
    for corner in 0..<corners * 2 {
      // figure out the location of this point
      let sinAngle = sin(currentAngle)
      let cosAngle = cos(currentAngle)
      let bottom: CGFloat
      
      // if we're a multiple of 2 we are drawing the outer edge of the star
      if corner.isMultiple(of: 2) {
        // store this Y position
        bottom = center.y * sinAngle
        
        // ...and add a line to there
        path.addLine(to: CGPoint(x: center.x * cosAngle, y:bottom))
      } else {
        // We're not a multiple of 2, which means we're drawing an inner point
        //store this Y Position
        bottom = innerY * sinAngle
        
        //.. and add a line to there
        path.addLine(to: CGPoint(x: innerX * cosAngle, y: bottom))
      }
      
      // If this new bottom point is our lowest, stash it away
      if bottom > bottomEdge {
        bottomEdge = bottom
      }
      
      // move on to the next corner
      currentAngle += angleAdjustment
    }
    
    // figure out how much unused space we have at the bottom of our drawing rectangle
    let unusedSpace = (rect.height / 2 - bottomEdge) / 2
    
    // Create and apply a transform that moves our path down by that amount, centering the shape vertically
    let transform = CGAffineTransform(translationX: center.x, y: center.y + unusedSpace)
    
    return path.applying(transform)
  }
}

struct ContentView: View {
  var body: some View {
    List {
      Star(corners: 5, smoothness: 0.45)
        .fill(Color.gray)
        .frame(width: 200, height: 200)
        .background(Color.blue)

      Star(corners: 22, smoothness: 0.5)
        .fill(Color.red)
        .frame(width: 200, height: 200)
        .background(Color.green)
      
      // hexagon
      Star(corners: 3, smoothness: 1.0)
        .fill(Color.orange)
        .frame(width: 200, height: 200)
        .background(Color.purple)
      
      // Rectangle
      Star(corners: 2, smoothness: 1.0)
        .fill(Color.yellow)
        .frame(width: 200, height: 200)
        .background(Color.black)

    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
