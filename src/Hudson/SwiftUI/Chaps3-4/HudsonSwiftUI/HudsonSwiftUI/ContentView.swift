//
//  ContentView.swift
//  HudsonSwiftUI
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    GridStack(rows: 4, columns: 4) { row, col in
      Image(systemName: "\(row * 4 + col).circle")
      Text("R\(row) C\(col)")
        .frame(minWidth: 60)
    }
  }
}


struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}

struct GridStack<Content: View>: View {
  let rows: Int
  let columns: Int
  let content: (Int, Int) -> Content
  
  var body: some View {
    VStack(alignment: .leading) {
      ForEach(0 ..< rows) { row in
        HStack {
          ForEach(0 ..< self.columns) { column in
            self.content(row, column)
          }
        }
      }
    }
  }
  
  init(rows: Int, columns: Int, @ViewBuilder content: @escaping (Int, Int) -> Content) {
    self.rows = rows
    self.columns = columns
    self.content = content
  }
}


struct RotatingRectangleView: View {
  @State var dragAmount = CGSize.zero
  
  var body: some View {
    VStack {
      GeometryReader { geo in
        Rectangle()
          .fill(LinearGradient(gradient: Gradient(colors: [.yellow, .red]), startPoint: .topLeading, endPoint: .bottomTrailing))
          .frame(width: 300, height: 200)
          .clipShape(RoundedRectangle(cornerRadius: 20))
          .rotation3DEffect(.degrees(-Double(self.dragAmount.width) / 20), axis: (x: 0, y: 1, z: 0))
          .rotation3DEffect(.degrees(Double(self.dragAmount.height / 20)), axis: (x: 1, y: 0, z: 0))
          .offset(self.dragAmount)
          .gesture(
            DragGesture()
              .onChanged { self.dragAmount = $0.translation }
              .onEnded { _ in
                withAnimation(.spring()) {
                  self.dragAmount = .zero
                }
              })
          }
        }
    }
}
