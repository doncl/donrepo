//
//  ContentView.swift
//  PickersInForms
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  let strengths: [String] = ["Mild", "Medium", "Mature"]
  @State private var selectedStrength = 0
  
  var body: some View {
    NavigationView {
      Form {
        Section {
          Picker(selection: $selectedStrength, label: Text("Strength")) {
            ForEach(0 ..< self.strengths.count) {
              Text(self.strengths[$0])
            }
          }
        //.pickerStyle(WheelPickerStyle()) // Uncomment this line to see different behavior.
        }
      }
    .navigationBarTitle("Select your cheese...")
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
