//
//  ContentView.swift
//  BasicForm
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var enableLogging: Bool = false
  @State private var selectedColor: Int = 0
  @State private var colors: [String] = ["Red", "Green", "Blue"]
  
  var body: some View {
    NavigationView {
      Form {
        Picker(selection: $selectedColor, label: Text("Select a color")) {
          ForEach(0 ..< colors.count) {
            Text(self.colors[$0]).tag($0)
          }
        }.pickerStyle(SegmentedPickerStyle())
        
        Toggle(isOn: $enableLogging) {
          Text("Enable Logging")
        }
        
        Button(action: {
          // activate theme!
        }) {
          Text("Save Changes")
        }
      }.navigationBarTitle("Settings")
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
