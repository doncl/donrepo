//
//  ContentView.swift
//  EnablingAndDisablingElementsInForms
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var agreedToTerms: Bool = false
  
  var body: some View {
    NavigationView {
      Form {
        Section {
          Toggle(isOn: $agreedToTerms) {
            Text("Agree to terms and conditions")
          }
        }
        Section {
          Button(action: {
            // Show next screen here
          }) {
            Text("Continue")
          }.disabled(!agreedToTerms)
        }
      }
    .navigationBarTitle("Welcome")
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
