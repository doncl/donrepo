//
//  ContentView.swift
//  ShowingAndHidingFormRows
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var showingAdvancedOptions: Bool = false
  @State private var enableLogging: Bool = false
  
  var body: some View {
    Form {
      Section {
        Toggle(isOn: $showingAdvancedOptions.animation()) {
          Text("Show advanced options")
        }
        if showingAdvancedOptions {
          Toggle(isOn: $enableLogging) {
            Text("Enable logging")
          }
        }
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
