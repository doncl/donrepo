//
//  ContentView.swift
//  AddBarItemsToNavView
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    NavigationView {
      Text("SwiftUI")
        .navigationBarTitle("Welcome")
        .navigationBarItems(trailing:
          HStack {
            Button("About") {
              print("About tapped!")
            }
            Button("Help") {
              print("Help tapped!")
            }
          }
      )
    }

  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
