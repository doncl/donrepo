//
//  ContentView.swift
//  EmbedInTabView
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State var selectedView = 0
  
  var body: some View {
    TabView(selection: $selectedView) {
      Text("First View")
        .tabItem {
          Image(systemName: "1.circle")
          Text("First")
      }.tag(0)
      Text("Second View")
      .tabItem {
        Image(systemName: "2.circle")
        Text("Second")
      }.tag(1)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
