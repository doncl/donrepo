//
//  ContentView.swift
//  GroupViews
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      Group {
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
      }
      Group {
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
        Text("Line")
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
