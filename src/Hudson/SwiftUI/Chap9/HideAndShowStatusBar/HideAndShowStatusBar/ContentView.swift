//
//  ContentView.swift
//  HideAndShowStatusBar
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var hidesStatusBar: Bool = false
  
  var body: some View {
    Button("Toggle Status Bar") {
      withAnimation {
        self.hidesStatusBar.toggle()
      }
    }
    .statusBar(hidden: hidesStatusBar)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
