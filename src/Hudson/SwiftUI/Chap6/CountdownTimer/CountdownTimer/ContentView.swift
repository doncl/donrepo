//
//  ContentView.swift
//  CountdownTimer
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State var timeRemaining = 10
  
  let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
  
  var body: some View {
    Text("\(timeRemaining)")
      .onReceive(timer, perform: { input in
        if self.timeRemaining > 0 {
          self.timeRemaining -= 1
        }
      })
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
