//
//  ContentView.swift
//  PushViewWhenListRowTapped
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct Restaurant: Identifiable {
  var id = UUID()
  var name: String
}

struct RestaurantView: View {
  var restaurant: Restaurant

  var body: some View {
    //Text("Come and eat at \(restaurant.name)").font(.largeTitle)
    Text("")
  }
}

struct RestaurantRow: View {
  var restaurant: Restaurant
  
  var body: some View {
    Text(restaurant.name)
  }
}

struct ContentView: View {
  var body: some View {
    let first = Restaurant(name: "Joe's Original")
    let restaurants: [Restaurant] = [first]
    
    return NavigationView {
      List(restaurants) { restaurant in
        NavigationLink(destination: RestaurantView(restaurant: restaurant)) {
          RestaurantRow(restaurant: restaurant)
        }
      }.navigationBarTitle("Select a restaurant")
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
