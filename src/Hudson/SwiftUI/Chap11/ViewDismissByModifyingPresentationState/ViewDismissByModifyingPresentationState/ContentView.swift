//
//  ContentView.swift
//  ViewDismissByModifyingPresentationState
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct DismissibleByModifyingPresentationState: View {
  @Environment(\.presentationMode) var presentationMode
  
  var body: some View {
    VStack {
      Text("DismissibleThroughModifyingPresentationState")
      Button(action: {
        self.presentationMode.wrappedValue.dismiss()
      }) {
        Text("Dismiss")
      }
    }
  }
}

struct DismissibleByPassingBinding: View {
  @Binding var isPresented: Bool
  
  var body: some View {
    VStack {
      Text("Dismissible Through binding")
      Button(action: {
        self.isPresented = false
      }) {
        Text("Dismiss")
      }
    }
  }
}

struct ContentView: View {
  @State private var showingDetail: Bool = false
  @State private var showingView2: Bool = false
  
  var body: some View {
    VStack {
      Spacer()
      Button(action: {
        self.showingDetail.toggle()
      }) {
        Text("Show Detail Dismissable By Modifying Presentation State")
      }.sheet(isPresented: $showingDetail) {
        DismissibleByModifyingPresentationState()
      }
      
      Spacer(minLength: CGFloat(100.0))
      
      Button(action: {
        self.showingView2.toggle()
      }) {
        Text("Show Detail Dismissable by two-way binding")
      }.sheet(isPresented: $showingView2) {
        DismissibleByPassingBinding(isPresented: self.$showingView2)
      }
      Spacer()
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
