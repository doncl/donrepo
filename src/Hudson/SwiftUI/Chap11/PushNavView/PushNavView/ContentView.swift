//
//  ContentView.swift
//  PushNavView
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct DetailView: View {
  var body: some View {
    Text("This is the detail view")
  }
}

struct ContentView: View {
  var body: some View {
    NavigationView {
      VStack {
        NavigationLink(destination: DetailView()) {
          Text("Show Detail View")
        }.navigationBarTitle("Navigation")
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
