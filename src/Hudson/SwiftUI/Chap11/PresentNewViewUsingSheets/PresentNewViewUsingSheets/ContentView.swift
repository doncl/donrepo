//
//  ContentView.swift
//  PresentNewViewUsingSheets
//
//  Created by Don Clore on 12/27/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct DetailView: View {  
  var body: some View {
    Text("Detail")
  }
}

struct ContentView: View {
  @State private var showingDetail: Bool = false
  
  var body: some View {
    Button(action: {
      self.showingDetail.toggle()
    }) {
      Text("Show Detail")
    }.sheet(isPresented: $showingDetail) {
      DetailView()
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
