//
//  ProfilePicture.swift
//  CreateComposeCustomViews
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ProfilePicture: View {
  var imageName: String
  
  var body: some View {
    Image(imageName)
      .resizable()
      .frame(width: 100, height: 100)
      .clipShape(Circle())
  }
}

struct ProfilePicture_Previews: PreviewProvider {
  static var previews: some View {
    ProfilePicture(imageName: "Joe")
  }
}
