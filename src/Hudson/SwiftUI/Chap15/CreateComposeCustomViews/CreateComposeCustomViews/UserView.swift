//
//  UserView.swift
//  CreateComposeCustomViews
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct UserView: View {
  var user: User
    
  var body: some View {
    HStack {
      ProfilePicture(imageName: user.profilePicture)
      UserDetails(user: user)
    }
  }
}

struct UserView_Previews: PreviewProvider {
  static var previews: some View {
    let u: User = User(name: "Spike Spiker", jobTitle: "spiker", emailAddress: "spike@maven.io", profilePicture: "spike")

    return UserView(user: u)
  }
}
