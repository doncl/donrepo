//
//  UserDetails.swift
//  CreateComposeCustomViews
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct UserDetails: View {
  var user: User
  
  var body: some View {
    VStack(alignment: .leading) {
      Text(user.name)
        .font(.largeTitle)
        .foregroundColor(.primary)
      Text(user.jobTitle)
        .foregroundColor(.secondary)
      EmailAddress(address: user.emailAddress)
    }
  }
}

struct UserDetails_Previews: PreviewProvider {
  static var previews: some View {
    let u: User = User(name: "Spike Spiker", jobTitle: "spiker", emailAddress: "spike@maven.io", profilePicture: "spike")
    return UserDetails(user: u)
  }
}
