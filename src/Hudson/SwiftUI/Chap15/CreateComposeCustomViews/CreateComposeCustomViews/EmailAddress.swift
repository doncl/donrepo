//
//  EmailAddress.swift
//  CreateComposeCustomViews
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct EmailAddress: View {
  var address: String
  
  var body: some View {
    HStack {
      Image(systemName: "envelope")
      Text(address)
    }
  }
}

struct EmailAddress_Previews: PreviewProvider {
  static var previews: some View {
    EmailAddress(address: "joe@maven.io")
  }
}
