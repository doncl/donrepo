//
//  ContentView.swift
//  CreateComposeCustomViews
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  let u: User = User(name: "Spike Spiker", jobTitle: "spiker", emailAddress: "spike@maven.io", profilePicture: "spike")

  var body: some View {
    UserView(user: u)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
