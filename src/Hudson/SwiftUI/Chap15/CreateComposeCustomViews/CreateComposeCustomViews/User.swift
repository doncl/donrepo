//
//  User.swift
//  CreateComposeCustomViews
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import Foundation

struct User {
  var name: String
  var jobTitle: String
  var emailAddress: String
  var profilePicture: String 
}
