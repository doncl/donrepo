//
//  ContentView.swift
//  CombineTextViews
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack(alignment: .center, spacing: 30) {
      Text("SwiftUI")
        .font(.largeTitle)
      + Text("is")
        .font(.headline)
      + Text("awesome")
        .font(.footnote)
        
      Text("SwiftUI")
        .foregroundColor(.red)
      + Text("is")
        .foregroundColor(.orange)
        .fontWeight(.black)
      + Text("awesome")
        .foregroundColor(.blue)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
