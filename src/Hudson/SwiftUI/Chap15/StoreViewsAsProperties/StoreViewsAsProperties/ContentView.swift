//
//  ContentView.swift
//  StoreViewsAsProperties
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  let title: Text = Text("Paul Hudson")
    .font(.largeTitle)
  
  let subtitle: Text = Text("Author")
    .foregroundColor(.secondary)
  
  var body: some View {
    VStack {
      title
        .foregroundColor(.red)
      subtitle
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
