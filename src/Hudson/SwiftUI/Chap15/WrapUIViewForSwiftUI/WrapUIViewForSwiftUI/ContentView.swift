//
//  ContentView.swift
//  WrapUIViewForSwiftUI
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import UIKit
import SwiftUI

struct TextView: UIViewRepresentable {
  @Binding var text: String

  func makeUIView(context: Context) -> UITextView {
    return UITextView()
  }
  
  func updateUIView(_ uiView: UITextView, context: Context) {
    uiView.text = text
  }
}

struct ContentView: View {
  @State var text = ""
  
  var body: some View {
    TextView(text: $text)
      .frame(minWidth: 0, maxWidth: .infinity,minHeight: 0, maxHeight: .infinity)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
