//
//  ContentView.swift
//  CustomModifiers
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct PrimaryLabel: ViewModifier {
  func body(content: Content) -> some View {
    content
      .padding()
      .background(Color.red)
      .foregroundColor(Color.white)
      .font(.largeTitle)
  }
}

struct ContentView: View {
  
  
  var body: some View {
    Text("Hello, Primary Label")
      .modifier(PrimaryLabel())
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
