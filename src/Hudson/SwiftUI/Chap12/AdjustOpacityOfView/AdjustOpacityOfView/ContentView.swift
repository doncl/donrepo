//
//  ContentView.swift
//  AdjustOpacityOfView
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    Text("Now you see me")
      .padding()
      .background(Color.red)
      .opacity(0.3)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
