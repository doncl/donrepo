//
//  ContentView.swift
//  ColorPaddingAroundView
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      Text("Padding afterwards")
        .background(Color.black)
        .foregroundColor(.white)
        .padding()

      Text("Padding before")
        .padding()
        .background(Color.black)
        .foregroundColor(.white)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
