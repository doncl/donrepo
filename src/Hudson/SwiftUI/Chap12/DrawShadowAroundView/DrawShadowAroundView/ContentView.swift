//
//  ContentView.swift
//  DrawShadowAroundView
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      Spacer()
      // This adds a very slight shadow with a 5-point blur centered on the text.
      Text("very slight shadow with a 5-point blur centered on the text")
        .padding()
        .shadow(radius: 5)
        .border(Color.red, width: 4)
      
      Spacer()
      
      Text("Very strong red shadow with 5-point blur, centered on text")
        .padding()
        .shadow(color: .red, radius: 5)
        .border(Color.red, width: 4)
      
      Spacer()
      Text("Same red shadow with 20 point x and y offsets")
        .padding()
        .shadow(color: .red, radius: 5, x: 20, y: 20)
        .border(Color.red, width: 4)
      
      Spacer()
      Text("Shadow applies to border by changing modifiers order")
        .padding()
        .border(Color.red, width: 4)
        .shadow(color: .red, radius: 5, x: 20, y: 20)
      
      Spacer()
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
