//
//  ContentView.swift
//  ScaleViewUpOrDown
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      Spacer()
      Text("Scale up 5")
        .scaleEffect(5)
      Spacer()
      Text("Scaleup X1 Y5")
        .scaleEffect(x: 1, y: 5)
      Spacer()
      Text("Scale with bottomTrailing anchor")
        .scaleEffect(1.3, anchor: .bottomTrailing)
      Spacer()
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
