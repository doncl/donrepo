//
//  ContentView.swift
//  TintingDesaturatingAndMore
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    List {
      Image("spike-128")
        .colorMultiply(.red)
      Image("tardis2")
        .saturation(0.1)
      Image("tardis2")
        .saturation(0.3)
      Image("tardis2")
        .contrast(0.5)
      Image("tardis2")
        .contrast(1.5)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
