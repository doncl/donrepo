//
//  ContentView.swift
//  DrawBorderInsideView
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      Circle().strokeBorder(Color.blue, lineWidth: 50) // all within edge.
      Circle().stroke(Color.blue, lineWidth: 50)  // hangs outside of edge
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
