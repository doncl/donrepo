//
//  ContentView.swift
//  AdjustViewPositionUsingOffset
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
//  var body: some View {  // Problem: overlap
//    VStack {
//      Text("Home")
//      Text("Options")
//        .offset(y: 15)
//      Text("Help")
//    }
//  }
  
  // Solution 1
//  var body: some View {
//    VStack {
//      Text("Home")
//      Text("Options")
//        .offset(y: 15)
//        .padding(.bottom, 15)
//      Text("Help")
//    }
//  }

  // Using ZStack
  var body: some View {
    ZStack(alignment: .bottomTrailing) {
      Image("tardis2")
      Text("Photo: icons8")
        .padding(4)
        .background(Color.black)
        .foregroundColor(.white)
        .offset(x: -30, y: -10)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
