//
//  ContentView.swift
//  BlendViewsTogether
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      Spacer()
      ZStack {
        Image("spike").resizable().clipped()
        Image("tardis2")
          .blendMode(.multiply)
      }
      Spacer()
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
