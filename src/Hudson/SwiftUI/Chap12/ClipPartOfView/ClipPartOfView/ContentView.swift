//
//  ContentView.swift
//  ClipPartOfView
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      Button(action: {
        print("Button tapped")
      }) {
        Image(systemName: "bolt.fill")
          .foregroundColor(.white)
          .padding()
          .background(Color.green)
          .clipShape(Circle())
      }
      
      Button(action: {
           print("Button tapped")
         }) {
           Image(systemName: "bolt.fill")
             .foregroundColor(.white)
             .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
             .background(Color.green)
             .clipShape(Capsule())
         }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
