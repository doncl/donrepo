//
//  ContentView.swift
//  RoundCornersOfView
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {  
    Text("Round Me")
      .padding()
      .background(Color.red)
      .cornerRadius(25)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
