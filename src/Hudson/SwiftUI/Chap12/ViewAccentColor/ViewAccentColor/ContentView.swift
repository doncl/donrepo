//
//  ContentView.swift
//  ViewAccentColor
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      Button(action: {
        
      }) {
        Text("Tap here")
      }
    }.accentColor(.orange)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
