//
//  ContentView.swift
//  RotateView
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var rotation: Double = 0.0
  
  var body: some View {
    VStack {
      Spacer()
      Text("using degrees")
        .rotationEffect(.degrees(-90))
      Spacer()
      Text("using radians")
        .rotationEffect(.radians(-.pi / 2))

      Spacer()
      VStack {
        VStack {
          Slider(value: $rotation, in: 0...360, step: 1.0)
          Text("round & round we go")
            .rotationEffect(.degrees(rotation)).padding()
          
          Text("round we go aound topleading")
            .rotationEffect(.degrees(rotation), anchor: .topLeading).padding()
          
          Text("round we go aound bottomTrailing")
            .rotationEffect(.degrees(rotation), anchor: .bottomTrailing).padding()
          
          Text("round we go aound arbitrary point")
            .rotationEffect(.degrees(rotation), anchor: UnitPoint(x: 0.3, y: 0.5)).padding()
        }
      }
      Spacer()
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
