//
//  ContentView.swift
//  RotateView3D
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    Text("EPISODE LLVM")
      .font(.largeTitle)
      .foregroundColor(.yellow)
      .rotation3DEffect(.degrees(45), axis: (x: 1, y: 0, z: 0))
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
