//
//  ContentView.swift
//  DrawBorderAroundView
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      Text("1 point black border")
        .border(Color.black)
      
      Text("1 point black border with padding")
        .padding()
        .border(Color.black)
      
      Text("4 point red border")
        .padding()
        .border(Color.red, width: 4)
      
      Text("Rounded border using .overlay()")
        .padding()
        .overlay(
          RoundedRectangle(cornerRadius: 32)
            .stroke(Color.blue, lineWidth: 4)
        )
    }

  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
