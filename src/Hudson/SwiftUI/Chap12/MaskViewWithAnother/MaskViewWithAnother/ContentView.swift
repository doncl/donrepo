//
//  ContentView.swift
//  MaskViewWithAnother
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    Image("tardis2")
      .resizable()
      .frame(width: 300, height: 300)
      .mask(Text("TARDIS!"))
      .font(Font.system(size: 72).weight(.black))
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
