//
//  ContentView.swift
//  StackModifiers
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    Text("Forecast: Sun")
      .foregroundColor(.white)
      .padding()
      .background(Color.red)
      .padding()
      .background(Color.orange)
      .padding()
      .background(Color.yellow)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
