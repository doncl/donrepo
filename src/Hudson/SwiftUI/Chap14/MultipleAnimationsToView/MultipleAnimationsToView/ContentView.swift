//
//  ContentView.swift
//  MultipleAnimationsToView
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var isEnabled = false
  
  var body: some View {
    Button("Tap Me") {
      self.isEnabled.toggle()
    }
    .foregroundColor(.white)
    .frame(width: 200, height: 200)
    .background(isEnabled ? Color.green : Color.red)
    .animation(nil)
    .clipShape(RoundedRectangle(cornerRadius: isEnabled ? 100 : 0))
    .animation(.default)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
