//
//  ContentView.swift
//  AsymmetricTransitions
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var showDetails: Bool = false
  
  var body: some View {
    VStack(alignment: HorizontalAlignment.center, spacing: 30) {
      Button(action: {
        self.showDetails.toggle()
      }) {
        Text("Tap to show assymetric transition")
      }
      
      if showDetails {
        Text("AssymetricTransition")
          .transition(.asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .bottom)))
          .animation(.easeInOut(duration: 2.0))
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
