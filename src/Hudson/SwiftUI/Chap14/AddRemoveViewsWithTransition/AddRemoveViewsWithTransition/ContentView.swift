//
//  ContentView.swift
//  AddRemoveViewsWithTransition
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var showDetails = false
  @State private var showDetails2 = false
  @State private var showDetails3 = false
  
  var body: some View {
    VStack(alignment: HorizontalAlignment.center, spacing: 30) {
      Button(action: {
        self.showDetails.toggle()
      }) {
        Text("Tap to show details")
      }
      
      if showDetails {
        Text("Details go here.")
          .transition(.move(edge: .bottom)).animation(.easeInOut(duration: 0.9))
      }
      
      Button(action: {
        self.showDetails2.toggle()
      }) {
        Text("Tap to slide")
      }
      
      if showDetails2 {
        Text("slide details.")
          .transition(.slide).animation(.easeInOut(duration: 0.9))
      }
      
      Button(action: {
        self.showDetails3.toggle()
      }) {
        Text("Tap to scale")
      }
      
      if showDetails3 {
        Text("scale details.")
          .transition(.scale).animation(.easeInOut(duration: 2.0))
      }


    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
