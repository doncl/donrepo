//
//  ContentView.swift
//  CustomTransition
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ScaledCircle: Shape {
  var animatableData: CGFloat
  
  func path(in rect: CGRect) -> Path {
    let maximumCircleRadius = sqrt(rect.width * rect.width * rect.height)
    let circleRadius = maximumCircleRadius * animatableData
    
    let x = rect.midX - circleRadius / 2
    let y = rect.midY - circleRadius / 2
    
    let circleRect = CGRect(x: x, y: y, width: circleRadius, height: circleRadius)
    
    return Circle().path(in: circleRect)
  }
}

struct ClipShapeModifier<T: Shape>: ViewModifier {
  let shape: T
  
  func body(content: Content) -> some View {
    content.clipShape(shape)
  }
}

extension AnyTransition {
  static var iris: AnyTransition {
    .modifier(
      active: ClipShapeModifier(shape: ScaledCircle(animatableData: 0)),
      identity: ClipShapeModifier(shape: ScaledCircle(animatableData: 1)))
  }
}

struct ContentView: View {
  @State private var isShowingRed: Bool = false
  
  var body: some View {
    NavigationView {
      ZStack {
        Rectangle()
          .fill(Color.blue)
          .frame(width: 300, height: 300)
        
        if isShowingRed {
          Rectangle()
            .fill(Color.red)
            .frame(width: 300, height: 300)
            .transition(.iris)
            .zIndex(1)
        }
      }
      .navigationBarItems(trailing: Button("Switch") {
        withAnimation(.easeInOut) {
          self.isShowingRed.toggle()
        }
      })
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
