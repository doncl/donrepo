//
//  ContentView.swift
//  BasicAnimations
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var scale0: CGFloat = 1.0
  @State private var scale1: CGFloat = 1.0
  @State private var scale2: CGFloat = 1.0
  
  var body: some View {
    VStack(alignment: .center, spacing: 50) {
      Button(action: {
        self.scale0 += 1
      }) {
        Text("1 second linear")
          .scaleEffect(scale0)
          .animation(.linear)
      }
      
      Button(action: {
        self.scale1 += 1
      }) {
        Text("3 second easeInOut")
          .scaleEffect(scale1)
          .animation(.easeInOut(duration: 3))
      }
      
      Button(action: {
        self.scale2 += 1
      }) {
        Text("2 second easeIn")
          .scaleEffect(scale2)
          .animation(.easeIn(duration: 2))
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
