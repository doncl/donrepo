//
//  ContentView.swift
//  CombineTransitions
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var showDetails: Bool = false
  @State private var showDetails2: Bool = false
  
  var body: some View {
    VStack(alignment: HorizontalAlignment.center, spacing: 30) {
      Button(action: {
        self.showDetails.toggle()
      }) {
        Text("Details go here")
      }
      if showDetails {
        Text("Here's the details")
          .transition(AnyTransition.opacity.combined(with: .slide))
      }
      
      Button(action: {
        self.showDetails2.toggle()
      }) {
        Text("move and scale")
      }
      
      if showDetails2 {
        Text("Move and Scale").transition(.moveAndScale)
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}


extension AnyTransition {
    static var moveAndScale: AnyTransition {
        AnyTransition.move(edge: .bottom).combined(with: .scale)
    }
}


