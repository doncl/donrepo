//
//  ContentView.swift
//  ExplicitAnimation
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var opacity = 1.0
  
  var body: some View {
    Button(action: {
      withAnimation(.linear(duration: 0.6)) {
        self.opacity -= 0.2
      }
    }) {
      Text("Tap Here")
        .padding()
        .opacity(opacity)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
