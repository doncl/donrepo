//
//  ContentView.swift
//  StartAnimationWhenViewAppears
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var scale: CGFloat = 1.0
  
  var body: some View {
    VStack(alignment: HorizontalAlignment.center, spacing: 30.0) {
      
      // Doing animation manually 
      Circle()
      .scale(scale)
      .onAppear {
        let baseAnimation = Animation.easeInOut(duration: 1)
        let repeated = baseAnimation.repeatForever(autoreverses: true)
        return withAnimation(repeated) {
          self.scale = 0.5
        }
      }
      
      // Using the extension
      Circle()
        .fill(Color.blue)
        .scaleEffect(scale)
          .animateForever(autoreverses: true) {
            self.scale = 0.5
        }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}

extension View {
    func animate(using animation: Animation = Animation.easeInOut(duration: 1), _ action: @escaping () -> Void) -> some View {
        return onAppear {
            withAnimation(animation) {
                action()
            }
        }
    }
}

extension View {
    func animateForever(using animation: Animation = Animation.easeInOut(duration: 1), autoreverses: Bool = false, _ action: @escaping () -> Void) -> some View {
        let repeated = animation.repeatForever(autoreverses: autoreverses)

        return onAppear {
            withAnimation(repeated) {
                action()
            }
        }
    }
}
