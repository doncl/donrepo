//
//  ContentView.swift
//  SpringAnimation
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var angle0: Double = 0
  @State private var angle1: Double = 0
  
  var body: some View {
    VStack(alignment: HorizontalAlignment.center, spacing: 40) {
      Button(action: {
        self.angle0 += 45
      }) {
        Text("Regular spring")
          .padding()
          .rotationEffect(.degrees(angle0))
          .animation(.spring())
      }.frame(width: 300, height: 20)
      
      Button(action: {
        self.angle1 += 45
      }) {
        Text("Interpolating spring")
          .padding()
          .rotationEffect(.degrees(angle1))
          .animation(.interpolatingSpring(mass: 0.1, stiffness: 40.0, damping: 0.9, initialVelocity: 40.0))
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
