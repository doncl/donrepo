//
//  ContentView.swift
//  AnimateBindingValueChanges
//
//  Created by Don Clore on 12/28/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var showingWelcome: Bool = false
  @State private var showing2: Bool = false
  
  var body: some View {
    VStack(alignment: HorizontalAlignment.center, spacing: 30) {
      Toggle(isOn: $showingWelcome.animation(.easeInOut(duration: 0.4))) {
        Text("Toggle label")
      }
      if showingWelcome {
        Text("Hello World")
      }
      Toggle(isOn: $showing2.animation(.spring())) {
        Text("Showing other label")
      }
      if showing2 {
        Text("Other Label")
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
