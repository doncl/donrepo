//
//  ContentView.swift
//  DelayAnimation
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var rotation = 0.0
  
  var body: some View {
    Rectangle()
      .fill(Color.red)
      .frame(width: 200, height: 200)
      .rotationEffect(.degrees(rotation))
      .animation(Animation.easeInOut(duration: 3).delay(1))
      .onTapGesture {
        self.rotation += 360
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
