//
//  ContentView.swift
//  DynamicTypeCustomFont
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ScaledFont: ViewModifier {
  @Environment(\.sizeCategory) var sizeCategory
  
  var name: String
  var size: CGFloat
  
  func body(content: Content) -> some View {
    let scaledSize = UIFontMetrics.default.scaledValue(for: size)
    return content.font(.custom(name, size: scaledSize))
  }
}

extension View {
  func scaledFont(name: String, size: CGFloat) -> some View {
    return self.modifier(ScaledFont(name: name, size: size))
  }
}

struct ContentView: View {
    var body: some View {
      List {
        Text("Hello, World!")
        Text("Hello, World")
         .scaledFont(name: "Georgia", size: 12)
      }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
