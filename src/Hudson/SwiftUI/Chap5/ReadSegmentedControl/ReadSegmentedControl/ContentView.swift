//
//  ContentView.swift
//  ReadSegmentedControl
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var favoriteColor: Int = 0
  @State private var favColor2: Int = 0
  
  let colors: [String] = ["Red", "Green", "Blue"]
  
  var body: some View {
    VStack {
      Spacer()
      Picker(selection: $favoriteColor, label: Text("What is your favorite color?")) {
        Text("Red").tag(0)
        Text("Green").tag(1)
        Text("Blue").tag(2)
      }.pickerStyle(SegmentedPickerStyle())
      
      Text("Value: \(favoriteColor)")
      Spacer()
      Spacer()
      Picker(selection: $favColor2, label: Text("What is your second favorite color")) {
        ForEach(0..<colors.count) { i in
          Text(self.colors[i]).tag(i)
        }
      }.pickerStyle(SegmentedPickerStyle())
      Text("Value: \(colors[favColor2])")
      Spacer()
      Spacer()
      Spacer()
    }
    
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
