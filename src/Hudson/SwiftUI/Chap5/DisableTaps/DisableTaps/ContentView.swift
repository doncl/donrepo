//
//  ContentView.swift
//  DisableTaps
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    ZStack {
      Button("Tap Me") {
        print("Button was tapped")
      }
      .frame(width: 100, height: 100)
      .background(Color.white)
      
      Rectangle()
        .fill(Color.red.opacity(0.2))
        .frame(width: 300, height: 300)
       .clipShape(Circle())
        .allowsHitTesting(false)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
