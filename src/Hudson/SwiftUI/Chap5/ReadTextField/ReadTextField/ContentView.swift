//
//  ContentView.swift
//  ReadTextField
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var name: String = "Tim"
  
  var body: some View {
    VStack {
        TextField("Enter your name", text: $name)
          .textFieldStyle(RoundedBorderTextFieldStyle())
          .multilineTextAlignment(.center)
      Text("Hello, \(name)!")
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
