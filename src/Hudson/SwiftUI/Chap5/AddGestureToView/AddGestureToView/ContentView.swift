//
//  ContentView.swift
//  AddGestureToView
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var scale: CGFloat = 1.0
  
  var body: some View {
    Image("tardis2")
      .scaleEffect(scale)
      .gesture(
        TapGesture()
          .onEnded { _ in
            self.scale += 0.1
          }
      )
    .gesture(LongPressGesture(minimumDuration: 2)
      .onEnded{ _ in
        self.scale -= 0.1
      })
      .gesture(DragGesture(minimumDistance: 50)
        .onEnded{ _ in
          print("Dragged!")
    })
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
