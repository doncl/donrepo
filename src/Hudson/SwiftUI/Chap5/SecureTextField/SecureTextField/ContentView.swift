//
//  ContentView.swift
//  SecureTextField
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var password: String = ""
  
  var body: some View {
    VStack {
      SecureField("Enter a password", text: $password)
        .multilineTextAlignment(.center)
      Text("You entered: \(password)")
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
