//
//  ContentView.swift
//  TappableArea
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      Image("tardis2").resizable().frame(width: 50, height: 50)
      Spacer().frame(height: 50)
      Text("Tardis")
    }
    .contentShape(Rectangle())
    .onTapGesture {
      print("Show details for tardis")
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
