//
//  ContentView.swift
//  ReadDatePicker
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var formatter: DateFormatter = {
    let f: DateFormatter = DateFormatter()
    f.dateStyle = .long
    return f
  }()
  
  @State private var birthDate: Date = Date()
  
  var body: some View {
    VStack {
      DatePicker(selection: $birthDate, in: ...Date(), displayedComponents: .date) {
        Text("Select a date")
      }
      
      Text("Date is \(birthDate, formatter: formatter)")
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
