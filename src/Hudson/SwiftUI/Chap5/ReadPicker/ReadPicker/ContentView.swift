//
//  ContentView.swift
//  ReadPicker
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var colors = ["Red", "Green", "Blue", "Tartan"]
  
  @State private var selectedColor: Int = 0
  
  var body: some View {
    VStack {
      Picker(selection: $selectedColor, label: Text("Please choose a color")) {
        ForEach(0 ..< colors.count) {
          Text(self.colors[$0])
        }
      }
      Text("You selected: \(colors[selectedColor])")
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
