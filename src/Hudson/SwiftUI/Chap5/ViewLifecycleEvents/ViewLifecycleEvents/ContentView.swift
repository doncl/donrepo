//
//  ContentView.swift
//  ViewLifecycleEvents
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    NavigationView {
      VStack {
        NavigationLink(destination: DetailView()) {
          Text("Hello, World")
        }
      }
    }.onAppear {
      print("ContentView appeared!")
    }.onDisappear {
      print("ContentView disappeared!")
    }
  }
}

struct DetailView: View {
  var body: some View {
    VStack {
      Text("Second view")
    }.onAppear {
      print("DetailView appeared!")
    }.onDisappear() {
      print("DetailView disappeared")
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
