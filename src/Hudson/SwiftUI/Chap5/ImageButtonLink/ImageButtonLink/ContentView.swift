//
//  ContentView.swift
//  ImageButtonLink
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      Spacer(minLength: 200)
      Button(action: {
        print("foo")
      }) {
        Image("garnet")
      }
      .buttonStyle(PlainButtonStyle())
      NavigationView {
        NavigationLink(destination: Text("Detail view here")) {
          Image("tardis2")
        }
        .buttonStyle(PlainButtonStyle())
      }
      Spacer(minLength: 100)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
