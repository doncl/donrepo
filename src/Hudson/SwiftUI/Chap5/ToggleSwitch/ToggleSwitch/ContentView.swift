//
//  ContentView.swift
//  ToggleSwitch
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var showGreeting: Bool = true
  
  var body: some View {
    VStack {
      Toggle(isOn: $showGreeting) {
        Text("Show welcome message")
      }.padding()
      if showGreeting {
        Text("Hello World!")
      }
    }          .animation(.easeInOut)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
