//
//  ContentView.swift
//  ReadTapAndDoubleTap
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      Text("Tap me!")
        .onTapGesture {
          print("tapped!")
      }
      Image("tardis2")
        .onTapGesture(count: 2) {
          print("Double tapped!")
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
