//
//  ContentView.swift
//  ReadStepper
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
@State private var age = 18

  var body: some View {
    VStack {
      Stepper("Enter your age", value: $age, in: 0...130)
      Text("Your age is \(age)")
      
      Stepper("Enter your age", onIncrement: {
        self.age += 1
      }, onDecrement: {
        self.age -= 1
      })
      
      Text("Your age is \(age)")
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
