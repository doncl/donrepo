//
//  ContentView.swift
//  HideControlLabels
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var selectedNumber: Int = 0
  var body: some View {
    HStack {
      Spacer(minLength: 8)
      Text("Pick a number")
      Picker("Select a number", selection: $selectedNumber) {
        ForEach(0..<10) {
          Text("\($0)")
        }
      }
      .labelsHidden()
      Spacer(minLength: 8)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
