//
//  ContentView.swift
//  TappableButton
//
//  Created by Don Clore on 12/26/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @State private var showDetails: Bool = false
  
  var body: some View {
    VStack {
      Button(action: {
        self.showDetails.toggle()
      }) {
        Image("tardis2")
          .frame(width: 200, height: 160)
          .clipped()
      }
      
      if showDetails {
        Text("Show some more details")
          .font(.largeTitle)
      }
    }
    .animation(.easeInOut)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
