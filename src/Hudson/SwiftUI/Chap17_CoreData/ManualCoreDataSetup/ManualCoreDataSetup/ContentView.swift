//
//  ContentView.swift
//  ManualCoreDataSetup
//
//  Created by Don Clore on 12/29/19.
//  Copyright © 2019 Beer Barrel Poker Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @Environment(\.managedObjectContext) var managedObjectContext
  
  @State private var languageName: String = ""
  @State private var languageCreator: String = ""
  
  @FetchRequest(entity:
    ProgrammingLanguage.entity(),
    sortDescriptors: [
      NSSortDescriptor(keyPath: \ProgrammingLanguage.name, ascending: true)
    ]
    //, predicate: NSPredicate(format: "name == %@", "Python")
  )
  var languages: FetchedResults<ProgrammingLanguage>
  
  var body: some View {
    NavigationView {
      VStack(alignment: .center, spacing: 30) {
        Form {
          TextField("Language Name:", text: $languageName)
          TextField("Language Creator", text: $languageCreator)
          Button(action: {
            let l = ProgrammingLanguage(context: self.managedObjectContext)
            l.name = self.languageName
            l.creator = self.languageCreator
            do {
              try self.managedObjectContext.save()
              self.languageName = ""
              self.languageCreator = ""
            } catch let error {
              fatalError(error.localizedDescription)
            }
          }) {
            Text("Add Language")
          }
        }
        List {
          ForEach(languages) { language in
            HStack {
              Text("\(language.name ?? "")")
              Text("\(language.creator ?? "")")
            }
          }
          .onDelete(perform: delete)
        }
      }.navigationBarTitle("Programming Languages", displayMode: .inline)
        .navigationBarItems(trailing: EditButton())
    }
  }
  
  func delete(at offsets: IndexSet) {
    for index in offsets {
      let language = languages[index]
      managedObjectContext.delete(language)
    }
    do {
      try managedObjectContext.save()
    } catch let error {
      fatalError(error.localizedDescription)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
      ContentView()
  }
}

extension ProgrammingLanguage : Identifiable {
  public var id: UUID {
    return UUID()
  }
}
