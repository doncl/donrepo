//  Copyright (c) 2015 Razeware LLC. All rights reserved.

import Foundation

extension NSAttributedString {
  class func attributedStringForTitleText(_ text: String) -> NSAttributedString {
    let titleAttributes =
    [NSFontAttributeName: UIFont(name: "AvenirNext-Heavy", size: 24)!,
      NSForegroundColorAttributeName: UIColor.white,
      NSShadowAttributeName: NSShadow.titleTextShadow(),
      NSParagraphStyleAttributeName: NSParagraphStyle.justifiedParagraphStyle()]
    return NSAttributedString(string: text, attributes: titleAttributes)
  }
  
  class func attributedStringForSubtitleText(_ text: String) -> NSAttributedString {
    let titleAttributes =
    [NSFontAttributeName: UIFont(name: "AvenirNext-DemiBold", size: 18)!,
      NSForegroundColorAttributeName: UIColor.white,
      NSShadowAttributeName: NSShadow.descriptionTextShadow(),
      NSParagraphStyleAttributeName: NSParagraphStyle.justifiedParagraphStyle()]
    return NSAttributedString(string: text, attributes: titleAttributes)
  }
  
  class func attributedStringForDescriptionText(_ text: String) -> NSAttributedString {
    let descriptionAttributes =
    [NSFontAttributeName: UIFont(name: "AvenirNext-Medium", size: 14)!,
      NSShadowAttributeName: NSShadow.descriptionTextShadow(),
      NSForegroundColorAttributeName: UIColor.white,
      NSBackgroundColorAttributeName: UIColor.clear,
      NSParagraphStyleAttributeName: NSParagraphStyle.justifiedParagraphStyle()]
    return NSAttributedString(string: text, attributes: descriptionAttributes)
  }
}

extension NSParagraphStyle {
  class func justifiedParagraphStyle() -> NSParagraphStyle {
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.alignment = .justified
    return paragraphStyle.copy() as! NSParagraphStyle
  }
}

extension NSShadow {
  class func titleTextShadow() -> NSShadow {
    let shadow = NSShadow()
    shadow.shadowColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0.3)
    shadow.shadowOffset = CGSize(width: 0, height: 2)
    shadow.shadowBlurRadius = 3.0
    return shadow
  }
  
  class func descriptionTextShadow() -> NSShadow {
    let shadow = NSShadow()
    shadow.shadowColor = UIColor(white: 0.0, alpha: 0.3)
    shadow.shadowOffset = CGSize(width: 0, height: 1)
    shadow.shadowBlurRadius = 3.0
    return shadow
  }
}
