//
//  main.m
//  ChatClient
//
//  Created by Don Clore on 8/16/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([ChatClientAppDelegate class]));
  }
}
