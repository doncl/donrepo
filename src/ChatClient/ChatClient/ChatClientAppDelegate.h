//
//  AppDelegate.h
//  ChatClient
//
//  Created by Don Clore on 8/16/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

