//
//  ViewController.m
//  ChatClient
//
//  Created by Don Clore on 8/16/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

#import "ChatClientViewController.h"

@interface ChatClientViewController ()
@end

@implementation ChatClientViewController
{
  NSInputStream *inputStream;
  NSOutputStream *outputStream;
  NSMutableArray<NSString *> *messages;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self initNetworkCommunication];

  messages = [NSMutableArray<NSString *> new];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)joinChat:(UIButton *)sender {
  NSString *response = [NSString stringWithFormat:@"iam:%@", _inputNameField.text];
  NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
  [outputStream write:[data bytes] maxLength:[data length]];

  [self.view bringSubviewToFront:self.chatView];
}

- (void)initNetworkCommunication
{
  CFReadStreamRef readStream;
  CFWriteStreamRef writeStream;
  CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"localhost", 80, &readStream,
                                    &writeStream);
  
  inputStream = (__bridge NSInputStream *)readStream;
  outputStream = (__bridge NSOutputStream *)writeStream;
  
  [inputStream setDelegate:self];
  [outputStream setDelegate:self];
  
  [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
  [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
  
  [inputStream open];
  [outputStream open];
}

- (IBAction)sendMessage:(UIButton *)sender
{
  NSString *response = [NSString stringWithFormat:@"msg:%@", self.inputMessageField.text];
  NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
  [outputStream write:[data bytes] maxLength:[data length]];
  self.inputMessageField.text = @""; // clean the field
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

  static NSString *CellIdentifier = @"ChatCellIdentifier";

  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  if (cell == nil) {
      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
  }

  NSString *s = (NSString *) messages[indexPath.row];
  cell.textLabel.text = s;
  return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return messages.count;
}

#pragma mark - NSStreamDelegate

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)streamEvent
{
  switch (streamEvent) {
    case NSStreamEventOpenCompleted:
      NSLog(@"stream opened");
      break;

    case NSStreamEventHasBytesAvailable:
      [self handleHasBytesAvailable:aStream];
      break;

    case NSStreamEventErrorOccurred:
      NSLog(@"Cannot connect to the host!");
      break;

    case NSStreamEventEndEncountered:
      [aStream close];
      [aStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
      break;

    default:
      NSLog(@"Unknown event %@", @(streamEvent));
  }
}

- (void)handleHasBytesAvailable:(NSStream *)aStream
{
  if (aStream == inputStream) {
    uint8_t buffer[1024];
    long len;

    while ([inputStream hasBytesAvailable]) {
      len = [inputStream read:buffer maxLength:sizeof(buffer)];
      if (len > 0) {
        NSString *output = [[NSString alloc]
          initWithBytes:buffer length:len
               encoding:NSASCIIStringEncoding];

        if (nil != output) {
          NSLog(@"server said: %@", output);
          [self messageReceived:output];
        }
      }
    }
  }
}

- (void)messageReceived:(NSString *)message
{
  message = [message stringByReplacingOccurrencesOfString:@"\r\n"
                                               withString:@""];
  [messages addObject:message];
  [self.tView reloadData];
  NSIndexPath *topIndexPath =
    [NSIndexPath indexPathForRow:messages.count-1
                       inSection:0];
  [self.tView scrollToRowAtIndexPath:topIndexPath
                    atScrollPosition:UITableViewScrollPositionMiddle
                            animated:YES];
}

@end
