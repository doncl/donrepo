//
//  ViewController.h
//  ChatClient
//
//  Created by Don Clore on 8/16/16.
//  Copyright © 2016 Beer Barrel Poker Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatClientViewController : UIViewController <NSStreamDelegate, UITableViewDelegate,
  UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITextField *inputNameField;
- (IBAction)joinChat:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UITextField *inputMessageField;
- (IBAction)sendMessage:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *chatView;
@property (strong, nonatomic) IBOutlet UITableView *tView;

@end

