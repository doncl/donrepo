//
//  ViewController.swift
//  AnimatedThreeRings
//
//  Created by Don Clore on 4/25/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let button1 = RecordSeriesButton()
    let button2 = RecordSeriesButton()

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
 
        var width: CGFloat = 180
        var height: CGFloat = 45
        var x: CGFloat = view.center.x - width / 2
        
        button1.frame = CGRect(x: x, y: 100, width: width, height: height)
        view.addSubview(button1)
        
        width = 360
        height = 90
        x = view.center.x - 240 / 2
        button2.frame = CGRect(x: x, y: 300, width: width, height: height)
        view.addSubview(button2)
    }
}

