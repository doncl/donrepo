//
//  CALayer.swift
//  AnimatedThreeRings
//
//  Created by Don Clore on 4/25/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import QuartzCore

public extension CALayer {
    static let rotationKeyPath = "transform.rotation"
    
    var center: CGPoint {
        return CGPoint(
            x: bounds.width / 2,
            y: bounds.height / 2
        )
    }
}

