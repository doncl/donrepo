//
//  RecordSeriesButton.swift
//  AnimatedThreeRings
//
//  Created by Don Clore on 4/25/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//

import UIKit

class RecordSeriesButton: UIButton {
    enum ButtonState {
        case animating
        case notrecordingseries
        case recordingseries
    }
    
    var recordingState: ButtonState = .notrecordingseries
    var rl = RingLayer()
    let leftLabelEdge: CGFloat = 50
    let innerCircle: CAShapeLayer = CAShapeLayer()
    let outerCircle: CAShapeLayer = CAShapeLayer()
    let blueBall: CAShapeLayer = CAShapeLayer()
    let checkMark: CAShapeLayer = CAShapeLayer()
    let label: UILabel = UILabel()
    
    lazy private var haptic : UINotificationFeedbackGenerator = {
        let gen = UINotificationFeedbackGenerator()
        gen.prepare()
        return gen
    }()
    
    fileprivate func setupRingLayer() {
        rl = RingLayer()
        rl.backgroundColor = UIColor.clear.cgColor
        rl.ringBackgroundColor = UIColor(white: 240.0 / 255.0, alpha: 0.4).cgColor
        rl.ringColor = UIColor.white.cgColor
        rl.ringWidth = 3.0
        rl.value = 0.25
    }
    
    init() {
        super.init(frame: .zero)
        backgroundColor = UIColor.fuboOrange
        innerCircle.fillColor = UIColor.white.cgColor
        outerCircle.fillColor = UIColor.clear.cgColor
        outerCircle.strokeColor = UIColor.white.cgColor
        outerCircle.lineWidth = 2.0
        blueBall.fillColor = UIColor(red: 93 / 255, green: 158 / 255, blue: 206 / 255, alpha: 1.0).cgColor
        
        setupRingLayer()
        
        layer.addSublayer(innerCircle)
        layer.addSublayer(outerCircle)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        label.text = "Record Series".uppercased()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        label.sizeToFit()
        addTarget(self, action: #selector(RecordSeriesButton.buttonPressed(_:)), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2.0
        let innerPath = getCirclePath(inner: true)
        innerCircle.path = innerPath.cgPath
        let outerPath = getCirclePath(inner: false)
        outerCircle.path = outerPath.cgPath
        let circleFrame = getCircleFrame(inner: false)
        rl.frame = circleFrame
        label.textAlignment = .center
        let size = label.intrinsicContentSize
        let labelLeftEdge = getLabelLeftEdge()
        label.frame = CGRect(x: labelLeftEdge,
                             y: bounds.midY - size.height / 2 - 5, width: size.width + 10, height: size.height + 10)
        adjustButtonWidth()
    }
    
    private func getRadius(inner: Bool) -> CGFloat {
        return inner ? bounds.height / 7 : bounds.height / 4
    }
    
    private func getCircleFrame(inner: Bool) -> CGRect {
        let circleCenter: CGPoint = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
        let radius = getRadius(inner: inner)
        let diameter: CGFloat = radius * 2
        var x: CGFloat = bounds.height / 2
        if !inner {
            x -= bounds.height / 9
        }
        let circleFrame = CGRect(x: x, y: circleCenter.y - radius,
                                 width: diameter, height: diameter)
        
        return circleFrame
    }
    
    private func getCirclePath(inner: Bool) -> UIBezierPath {
        let circleFrame = getCircleFrame(inner: inner)
        let path = UIBezierPath(ovalIn: circleFrame)
        return path
    }
    
    private func getArcCenter() -> CGPoint {
        let circleFrame = getCircleFrame(inner: false)
        let arcCenter: CGPoint = CGPoint(x: circleFrame.midX, y: circleFrame.midY)
        return arcCenter
    }
    
    @objc func buttonPressed(_ sender: RecordSeriesButton) {
        toggleState()
    }
    
    fileprivate func fireOffSpinnerAnimation() {
        recordingState = .animating
        setupRingLayer()
        layer.addSublayer(rl)
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.400)
        defer {
            CATransaction.commit()
        }
        rl.simpleAnimation(value: 1.25, delegate: self)
    }
    
    private func toggleState() {
        guard recordingState != .animating else { return }
        
        // Give the phone a little bump, if haptic is available.
        haptic.notificationOccurred(.success)
        
        if recordingState == .recordingseries {
            blueBall.removeFromSuperlayer()
            fireOffSpinnerAnimation()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.400, execute: {
                self.switchToOrange()
            })
        } else {
            innerCircle.removeFromSuperlayer()
            outerCircle.removeFromSuperlayer()
            fireOffSpinnerAnimation()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.400, execute: {
                self.switchToBlue()
            })
        }
        setNeedsDisplay()
    }
    
    private func makeBlueBall() {
        let blueBallPath = getCirclePath(inner: false)
        blueBall.path = blueBallPath.cgPath
        let circleFrame = getCircleFrame(inner: false)
        let checkMarkDimension = circleFrame.width - 2
        let tickPath = UIBezierPath()
        tickPath.move(to: CGPoint(x: checkMarkDimension/6, y: checkMarkDimension/2))
        tickPath.addLine(to: CGPoint(x: ((checkMarkDimension)*2)/6, y: ((checkMarkDimension)*3)/4))
        tickPath.addLine(to: CGPoint(x: ((checkMarkDimension)*5)/6, y: checkMarkDimension/4))
        checkMark.path = tickPath.cgPath
        checkMark.strokeColor = UIColor.white.cgColor
        checkMark.fillColor = nil
        checkMark.lineWidth = 3.0
        checkMark.lineJoin = CAShapeLayerLineJoin.miter
        checkMark.frame = circleFrame
        checkMark.masksToBounds = true
        blueBall.addSublayer(checkMark)
        layer.addSublayer(blueBall)
    }
    
    private func getLabelLeftEdge() -> CGFloat {
        let circleFrame = getCircleFrame(inner: false)
        return circleFrame.origin.x + circleFrame.width + 10
    }
    
    private func adjustButtonWidth() {
        let width = getLabelLeftEdge() + label.intrinsicContentSize.width + bounds.height / 2
        frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: width, height: frame.height)
    }
    
    private func switchToBlue() {
        rl.killSimpleAnimation()
        rl.removeFromSuperlayer()
        UIView.animate(withDuration: 0.2, delay: 0.0, options: [.curveEaseInOut], animations: {
            self.backgroundColor = "304041".color
            self.label.text = "recording series".uppercased()
            self.label.sizeToFit()
            self.adjustButtonWidth()
            self.makeBlueBall()
        }, completion: { _ in
            self.recordingState = .recordingseries
        })
    }
    
    fileprivate func switchToOrange() {
        rl.killSimpleAnimation()
        rl.removeFromSuperlayer()
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: [.curveEaseInOut], animations: {
            self.layer.addSublayer(self.innerCircle)
            self.layer.addSublayer(self.outerCircle)
            self.backgroundColor = UIColor.fuboOrange
            self.label.text = "Record Series".uppercased()
            self.adjustButtonWidth()
        }, completion: { _ in
            self.recordingState = .notrecordingseries
        })
    }
}

extension RecordSeriesButton: CAAnimationDelegate {
}

extension RingLayer {
    func simpleAnimation(value: CGFloat, delegate: CAAnimationDelegate) {
        guard case let angleDelta = (value - self.value) * 2 * .pi,
            abs(angleDelta) > 0.001
            else {return}
        
        for layer in [ringTipLayer, gradientLayer] {
            let keyFrameAnimation = CAKeyframeAnimation(layer: layer, rotationAngle: angleDelta)
            keyFrameAnimation.repeatCount = Float.greatestFiniteMagnitude
            keyFrameAnimation.delegate = delegate
            layer.add(keyFrameAnimation, forKey: CALayer.rotationKeyPath)
            
            layer.setValue(
                getAngle(value: value),
                forKeyPath: CALayer.rotationKeyPath
            )
        }
        _value = value
    }
    
    func killSimpleAnimation() {
        ringTipLayer.removeAllAnimations()
        gradientLayer.removeAllAnimations()
    }
}
