//
//  CAKeyFrameAnimation.swift
//  AnimatedThreeRings
//
//  Created by Don Clore on 4/25/19.
//  Copyright © 2019 Don Clore. All rights reserved.
//
import QuartzCore

extension CAKeyframeAnimation {
    convenience init(layer: CALayer, rotationAngle: CGFloat) {
        self.init(keyPath: CALayer.rotationKeyPath)
        
        let currentAngle =
            layer.value(forKeyPath: CALayer.rotationKeyPath) as? CGFloat
                ?? 0
        
        let keyFrameCount = abs(
            Int(
                floor( abs(rotationAngle) / CGFloat.pi / 4 ) + 2
            )
        )
        
        let timesAndValues = (0...keyFrameCount).map{
            [keyFrameCount = CGFloat(keyFrameCount)]
            keyFrameIndex -> (
            time: CGFloat,
            value: CGFloat
            ) in
            
            let keyFrameIndex = CGFloat(keyFrameIndex)
            return (
                time: keyFrameIndex / keyFrameCount,
                value: rotationAngle / keyFrameCount * keyFrameIndex + currentAngle
            )
        }
        
        keyTimes = timesAndValues.map{$0.time} as [NSNumber]
        values = timesAndValues.map{$0.value}
    }
}
