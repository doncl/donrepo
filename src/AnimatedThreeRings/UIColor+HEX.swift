import UIKit 

extension String {
    var color: UIColor {
        let scanner = Scanner(string: self)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        return UIColor(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}

extension UIColor {
    
    var toHexString: String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return String(
            format: "%02X%02X%02X",
            Int(r * 0xff),
            Int(g * 0xff),
            Int(b * 0xff)
        )
    }
    
    static var random: UIColor {
        return UIColor(
            red: CGFloat(arc4random()) / CGFloat(UInt32.max),
            green: CGFloat(arc4random()) / CGFloat(UInt32.max),
            blue: CGFloat(arc4random()) / CGFloat(UInt32.max),
            alpha: 1
        )
    }
    
    // MARK: - V3 Colors
    static let designRefreshGreyColor = "242424".color
    static let fuboMediumJungleGreen = UIColor(red: 25.0/255.0, green: 44.0/255.0, blue: 63.0/255.0, alpha: 1.0)
    static let fuboBrandColor = UIColor(red: 245.0/255.0, green: 124.0/255.0, blue: 0, alpha: 1.0)
    static let darkColor = UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
    
    static let lightGray = "4A4A4A".color
    static let lightGray2 = "535657".color
    static let almostWhite = "F6F2EB".color
    
    
    // MARK: Main Colors
    static let mainWhite = "FFFFFF".color
    static let fuboOrange = "FA4616".color
    static let fuboBlue = "1C9FD0".color
    static let clouds = "ecf0f1".color
    static let fuboDark = "242424".color
    static let warningRed = "EE2828".color
    
    // MARK: Darks
    static let mobileBlack = "1C1C1C".color
    static let almostBlack = "212121".color
    static let darkBlack = "101010".color
    static let regularDark = "2B2D2E".color
    static let mediumDark = "3A3E3F".color
    static let lightDark = "535657".color
    static let midDarkGray = "7B7B7B".color
    static let ticketSubheadingGray = "9B9B9B".color
    
    // MARK: Text Colors
    static let textExtraLightGray = "C4C0B8".color
    static let textLightGray = "79746C".color
    
    // MARK: - V2 Colors
    static let textBlueGreen = "00ACC1".color
    static let textGrey = "999999".color
    static let evilGrey = "666666".color
    static let fuboLightGrey = "B3B3B3".color
    static let darkBorder = "23262E".color
    static let fuboOrangeV2 = "B3B3B3".color
    static let darkGreyBlue = "2F333D".color
    static let fuboRed = "f44336".color
    static let buttonBorder = "1D2026".color
    //    static let interstitial = "343738".color
    static let skeletonLight = "EBE9E6".color
    static let dvrStatusSpaceBackground = "343637".color
    
    //MARK: SignupScreen colors.
    static let tooltipBackground = "575757".color
    static let tooltipText = "D6D6D6".color
    static let questionMarkCircleBackground = "848484".color
    static let questionMarkCircleForeground = "212124".color
    static let feeSummingViewBackground = "1A1A1A".color
    static let feeSummingLine = "969696".color
    
    //MARK: SplitSearch
    static let dividerHandleColor: UIColor = "999999".color
    
    //MARK: EPG
    static let epgDateBarBackground = "151718".color
    
    final private func isLight() -> Bool {
        // algorithm from: http://www.w3.org/WAI/ER/WD-AERT/#color-contrast
        guard let components = cgColor.components,
            components.count >= 3 else { return false }
        let brightness = ((components[0] * 299) + (components[1] * 587) + (components[2] * 114)) / 1000
        return !(brightness < 0.5)
    }
    
    final var complementaryColor: UIColor {
        return isLight() ? darker : lighter
    }
    
    final private var lighter: UIColor {
        return adjust(by: 1.35)
    }
    
    final private var darker: UIColor {
        return adjust(by: 0.84)
    }
    
    final private func adjust(by percent: CGFloat) -> UIColor {
        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return UIColor(hue: h, saturation: s, brightness: b * percent, alpha: a)
    }
    
    func makeGradient() -> [UIColor] {
        return [self, complementaryColor, self]
    }
}
