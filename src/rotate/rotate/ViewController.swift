//
//  ViewController.swift
//  rotate
//
//  Created by Don Clore on 11/11/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  let childVC : ChildVC = ChildVC()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    addChildViewController(childVC)
    view.addSubview(childVC.view)
    childVC.didMove(toParentViewController: self)
  }

}






