//
//  CancelButton.swift
//  Duwamish
//
//  Created by Don Clore on 5/8/17.
//  Copyright © 2017 theMaven Network. All rights reserved.
//

import UIKit

class CancelButton: UIView {
 
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.backgroundColor = .clear
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func draw(_ rect: CGRect) {
    IOS_icon_special_set.drawCloseButtonRegularWhiteBG()
  }
}
