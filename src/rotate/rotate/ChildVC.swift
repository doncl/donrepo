//
//  ChildVC.swift
//  rotate
//
//  Created by Don Clore on 11/11/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit
import SnapKit

class ChildVC: UIViewController {
  let table : UITableView = UITableView()
  
  var images : [UIImage] = [
    UIImage(named: "Wedding.jpg")!,
    UIImage(named: "Juan.jpg")!,
    UIImage(named: "zombies.jpg")!,
    UIImage(named: "WrigleyPortrait.jpg")!,
    ]
  var cells : [UITableViewCell] = [
    UITableViewCell(style: .default, reuseIdentifier: nil),
    UITableViewCell(style: .default, reuseIdentifier: nil),
    UITableViewCell(style: .default, reuseIdentifier: nil),
    UITableViewCell(style: .default, reuseIdentifier: nil),
    ]
  override func viewDidLoad() {
    super.viewDidLoad()
    
    for i in 0..<images.count {
      cells[i].imageView?.image = images[i]
      cells[i].textLabel?.text = "Label \(i)"
    }
    
    view.addSubview(table)
    table.snp.makeConstraints { make in
      make.edges.equalTo(view.safeAreaLayoutGuide)
    }
    
    table.dataSource = self
    table.delegate = self
    table.reloadData()
  }
}

extension ChildVC: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cells.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    return cells[indexPath.row]
  }
}

extension ChildVC : UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let image = images[indexPath.row]
    guard let viewer = ImageViewerVC(image: image) else {
      return
    }
    present(viewer, animated: true, completion:nil)
  }
}

