//
//  ImageViewerVC.swift
//  Duwamish
//
//  Created by Don Clore on 11/6/17.
//  Copyright © 2017 theMaven Network. All rights reserved.
//

import UIKit
import SnapKit

class ImageViewerVC: UIViewController {
  let cancelFrame : CGRect = CGRect(x: 26, y: 26, width: 26, height: 26)
  let vertOffset : CGFloat = 5.0
  let image : UIImage
  let imageView : UIImageView
  let scrollView : UIScrollView
  let cancelButton : CancelButton
  let enlargedTapArea : UIView
  let extraTapPad : CGFloat = 10.0
  var regularVertAndHorz : Bool = false
  
  init?(image: UIImage) {
    guard let imageCopy = image.deepCopy() else {
      return nil
    }
    self.image = imageCopy
    imageView = UIImageView()
    imageView.contentMode = .topLeft
    imageView.image = imageCopy
    scrollView = UIScrollView()
    if #available(iOS 11.0, *) {
      scrollView.contentInsetAdjustmentBehavior = .never
    }
    self.cancelButton = CancelButton(frame: cancelFrame)
    self.enlargedTapArea = UIView()
    super.init(nibName: nil, bundle: nil)
    scrollView.delegate = self
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .black
    
    view.addSubview(scrollView)

    scrollView.addSubview(imageView)
    imageView.frame.size = image.size
    imageView.snp.makeConstraints { make in
      make.edges.equalTo(scrollView)
    }
    
    enlargedTapArea.backgroundColor = .clear
    view.addSubview(cancelButton)
    view.addSubview(enlargedTapArea)
    
    makeCancelButtonAndEnlargedTapAreaConstraints()
    
    scrollView.snp.makeConstraints { make in
      make.top.equalTo(cancelButton.snp.bottom).offset(vertOffset)
      if #available(iOS 11.0, *) {
        make.leading.trailing.bottom.equalTo(view.safeAreaLayoutGuide)
      } else {
        make.leading.trailing.bottom.equalTo(view)
      }
    }

    cancelButton.isUserInteractionEnabled = true
    let tap = UITapGestureRecognizer(target: self,
      action: #selector(ImageViewerVC.cancelButtonTouched(_:)))
    
    cancelButton.addGestureRecognizer(tap)
    
    enlargedTapArea.isUserInteractionEnabled = true
    let enlargedTap = UITapGestureRecognizer(target: self,
      action: #selector(ImageViewerVC.cancelButtonTouched(_:)))
    
    enlargedTapArea.addGestureRecognizer(enlargedTap)
  }
  
  func setZoomParametersForSize(_ scrollViewSize : CGSize) {
    let imageSize = imageView.bounds.size
    let widthScale = scrollViewSize.width / imageSize.width
    let heightScale = scrollViewSize.height / imageSize.height
    let minScale = min(widthScale, heightScale)
    scrollView.minimumZoomScale = minScale
    scrollView.maximumZoomScale = 3.0
    scrollView.zoomScale = minScale
  }
  
  @objc func cancelButtonTouched(_ sender: UITapGestureRecognizer) {
    dismiss(animated: true, completion: nil)
  }
  
  fileprivate func makeCancelButtonAndEnlargedTapAreaConstraints() {
    cancelButton.snp.remakeConstraints { make in
      if #available(iOS 11.0, *) {
        make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(cancelFrame.origin.y)
        make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading).offset(cancelFrame.origin.x)
      } else {
        make.top.equalTo(view.snp.top).offset(cancelFrame.origin.y)
        make.leading.equalTo(view.snp.leading).offset(cancelFrame.origin.x)
      }
      
      make.width.equalTo(self.cancelFrame.width)
      make.height.equalTo(self.cancelFrame.height)
    }
    
    enlargedTapArea.snp.remakeConstraints { make in
      make.top.equalTo(cancelButton)
      make.leading.equalTo(cancelButton.snp.trailing)
      if #available(iOS 11.0, *) {
        make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing)
      } else {
        make.trailing.equalTo(view.snp.trailing)
      }
      make.height.equalTo(cancelButton).offset(extraTapPad)
    }
  }
  
  func recenterImage() {
    let scrollViewSize = scrollView.bounds.size
    let imageSize = imageView.frame.size
    let horizontalSpace =
      imageSize.width < scrollViewSize.width ?
        (scrollViewSize.width - imageSize.width) / 2 : 0
    
    let verticalSpace =
      imageSize.height < scrollViewSize.height ?
        (scrollViewSize.height - imageSize.height) / 2 : 0
    
    let contentInset = UIEdgeInsets(top: verticalSpace, left: horizontalSpace,
      bottom: verticalSpace, right: horizontalSpace)
    
    scrollView.contentInset = contentInset
  }
  
  override func viewDidLayoutSubviews() {
    setZoomParametersForSize(scrollView.bounds.size)
    recenterImage()
    view.bringSubview(toFront: cancelButton)
    view.bringSubview(toFront: enlargedTapArea)
  }
}

extension ImageViewerVC : UIScrollViewDelegate {
  func viewForZooming(in scrollView: UIScrollView) -> UIView? {
    return imageView
  }
}

extension UIImage  {
  func deepCopy() -> UIImage? {
    UIGraphicsBeginImageContext(size)
    defer {
        UIGraphicsEndImageContext()
    }
    draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
    return  UIGraphicsGetImageFromCurrentImageContext()
  }
}


