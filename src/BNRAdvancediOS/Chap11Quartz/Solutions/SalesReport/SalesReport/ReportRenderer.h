//
//  BNRReportRenderer.h
//  SalesReport
//
//  Created by Jonathan Blocksom on 4/1/13.
//  Copyright (c) 2013 Jonathan Blocksom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportRenderer : NSObject

@property (readonly) NSArray *persons;

- (id)initWithPersons:(NSArray *)persons;

// Assumes the context is flipped
- (void)drawInContext:(CGContextRef)ctx
               bounds:(CGRect)bounds;

// Just a stub that calls drawInContext:bounds:
- (void)drawLayer:(CALayer *)layer
        inContext:(CGContextRef)ctx;

@end
