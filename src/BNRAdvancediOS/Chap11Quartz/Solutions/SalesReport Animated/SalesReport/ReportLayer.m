//
//  BNRReportLayer.m
//  SalesReport
//
//  Created by Jonathan Blocksom on 6/11/13.
//  Copyright (c) 2013 Jonathan Blocksom. All rights reserved.
//

#import "ReportLayer.h"
#import "ReportRenderer.h"

@interface ReportLayer()

@property ReportRenderer *renderer;

@end

@implementation ReportLayer

- (id)initWithRenderer:(ReportRenderer *)renderer
{
    self = [super init];
    if (self) {
        _renderer = renderer;
    }
    return self;
}

- (void)drawInContext:(CGContextRef)ctx
{
    [self.renderer drawInContext:ctx bounds:self.bounds heightRatio:self.heightScale];
}

+ (BOOL)needsDisplayForKey:(NSString *)key
{
    BOOL needsDisplay = NO;
    if ([key isEqualToString:@"heightScale"]) {
        needsDisplay = YES;
    } else {
        needsDisplay = [super needsDisplayForKey:key];
    }
    return needsDisplay;
}

- (id)initWithLayer:(id)layer
{
    self = [super initWithLayer:layer];
    if (self) {
        if ([self isKindOfClass:[ReportLayer class]]) {
            ReportLayer *copyFrom = layer;
            self.heightScale = copyFrom.heightScale;
            self.renderer = copyFrom.renderer;
        }
    }
    return self;
}


@end
