//
//  BNRReportRenderer.m
//  SalesReport
//
//  Created by Jonathan Blocksom on 4/1/13.
//  Copyright (c) 2013 Jonathan Blocksom. All rights reserved.
//

#import "ReportRenderer.h"
#import "Person.h"

#import <CoreText/CoreText.h>

#define RANGE (700.0f)
// RANGE is the range of values that are OK

#define B_MARGIN (10.0f)
// B_MARGIN is the bottom margin in points

#define T_MARGIN (10.0f)
// T_MARGIN is the top margin in points

#define H_GAP (10.4f)
// H_GAP is the gap between the bars and side margins

#define NAME_HEIGHT (95.0f)
// NAME_HEIGHT is the height of the persons name in points

@interface ReportRenderer() {
    CGImageRef backgroundImage;
    CGRect backgroundRect;

    // DEMO 1
    CGFloat _heightRatio;
}

@property (nonatomic, strong) NSArray *persons;

@property (nonatomic, strong) UIFont *amountFont;

@end

@implementation ReportRenderer

- (id)initWithPersons:(NSArray *)persons
{
    self = [super init];
    if (self) {
        self.persons = persons;

        // DEMO 1
        _heightRatio = 1.0;

        UIImage *tempImage = [UIImage imageNamed:@"flowers.jpg"];
        backgroundImage = tempImage.CGImage;
        CGImageRetain(backgroundImage);
        
        backgroundRect.origin = CGPointZero;
        backgroundRect.size = tempImage.size;
        
        self.amountFont = [UIFont fontWithName:@"Verdana-Bold" size:30.0];
    }
    return self;
}

- (void)dealloc
{
    CGImageRelease(backgroundImage);
}

- (CGPathRef)newPathForName:(NSString *)s
                    atPoint:(CGPoint)startPoint
{
    // Get the unicodes for the string
    CFIndex length = [s length];
    UniChar *cBuffer = malloc(length * sizeof(UniChar));
    [s getCharacters:cBuffer range:NSMakeRange(0, length)];
    
    // Figure out the font
    CTFontRef nameFont = CTFontCreateWithName(CFSTR("Palatino-BoldItalic"), NAME_HEIGHT, NULL);
    
    // Get the glyphs
    CGGlyph *gBuffer = malloc(length * sizeof(CGGlyph));
    CTFontGetGlyphsForCharacters(nameFont, cBuffer, gBuffer, length);
    
    // Get the advances between glyphs
    CGSize *advances = malloc(length * sizeof(CGSize));
    CTFontGetAdvancesForGlyphs(nameFont, kCTFontDefaultOrientation, gBuffer, advances, length);
    
    // Create the path that will contain all the letter paths
    CGMutablePathRef fullPath = CGPathCreateMutable();
    CGAffineTransform currentTransform = CGAffineTransformMakeTranslation(startPoint.x, startPoint.y);
    for (int i=0; i<length; i++) {
        // Get the path for a particular glyph
        CGPathRef glyphPath = CTFontCreatePathForGlyph(nameFont, gBuffer[i], &currentTransform);
        
        // Add it to the full path
        CGPathAddPath(fullPath, NULL, glyphPath);
        CGPathRelease(glyphPath);
        
        // Move forward by the advance
        CGSize currentAdvance = advances[i];
        currentTransform = CGAffineTransformTranslate(currentTransform,
                                                      currentAdvance.width,
                                                      currentAdvance.height);
    }
    
    free(cBuffer);
    free(gBuffer);
    free(advances);
    
    return fullPath;
}

- (void)drawInContext:(CGContextRef)ctx
               bounds:(CGRect)bounds
{
    NSLog(@"Drawing in %@", NSStringFromCGRect(bounds));
    
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceRGB();
    
    CGFloat blackRGBA[] = { 0, 0, 0, 1 };
    CGColorRef black = CGColorCreate(cs, blackRGBA);

    CGFloat whiteRGBA[] = { 1, 1, 1, 1 };
    CGColorRef white = CGColorCreate(cs, whiteRGBA);
    
    CGContextSetStrokeColorWithColor(ctx, black);
    CGContextSetLineWidth(ctx, 1);
    
    CGContextSetFillColorWithColor(ctx, white);

    // Prepare a gradient
    CGFloat colors[] = {
        0.2, 0.2, 0.2, 1.0,  // dark gray
        0.8, 0.8, 0.8, 1.0,  // light gray
    };
    CGFloat locations[] = {0, 1};
    CGGradientRef gradient = CGGradientCreateWithColorComponents(cs, colors, locations, 2);
    
    float minY = CGRectGetMinY(bounds) + B_MARGIN;
    float maxY = CGRectGetMaxY(bounds) + T_MARGIN;
    float increment = (maxY - minY) / RANGE;
    
    int personCount = [self.persons count];
    float totalHGaps = (personCount + 2) * H_GAP;
    float barWidth = (bounds.size.width - totalHGaps) / personCount;
    
    // Draw the bars
    CGRect barRect;
    barRect.size.width = barWidth;
    barRect.origin.y = minY;
    
    int idx=0;
    for (Person *person in self.persons) {
        barRect.origin.x = bounds.origin.x + H_GAP + (barWidth + H_GAP) * idx;

        // DEMO 1
        barRect.size.height = _heightRatio * increment * person.sales;
        CGFloat cornerRadius =(barRect.size.height > 14.0) ? 7.0 : 0.0;
        CGPathRef barPath = CGPathCreateWithRoundedRect(barRect, cornerRadius, cornerRadius, NULL);
        
        // Fill the bar with flowers
        CGContextSaveGState(ctx);
        CGContextAddPath(ctx, barPath);
        CGContextClip(ctx);
        CGContextDrawImage(ctx, backgroundRect, backgroundImage);
        CGContextRestoreGState(ctx);
        
        // Fill path clears the current path, so add it again
        CGContextAddPath(ctx, barPath);
        CGContextStrokePath(ctx);
        CGPathRelease(barPath);
        
        NSString *soldString = [NSString stringWithFormat:@"%d units", (int)(_heightRatio * person.sales)];
        NSDictionary *attribs = @{
                                  NSFontAttributeName: self.amountFont,
                                  NSForegroundColorAttributeName: [UIColor whiteColor],
                             };
        NSAttributedString *displayAttrString =
        [[NSAttributedString alloc] initWithString:soldString
                                        attributes:attribs];
        CGSize strSize = displayAttrString.size;
        CGPoint textPos = CGPointMake(CGRectGetMaxX(barRect) - strSize.width,
                                      -CGRectGetMaxY(barRect));

        UIGraphicsPushContext(ctx);
        CGContextSaveGState(ctx);
        CGContextScaleCTM(ctx, 1.0, -1.0);
        [displayAttrString drawAtPoint:textPos];
        CGContextRestoreGState(ctx);
        UIGraphicsPopContext();
        
        //
        // Draw the name with a gradient
        //
        
        // Save state since we're changing clipping region
        CGContextSaveGState(ctx);
        
        // Get path for name
        CGPoint textStart = CGPointMake(barRect.origin.x, CGRectGetMaxY(barRect) + 10.0);
        CGPathRef namePath = [self newPathForName:person.name atPoint:textStart];
        CGContextAddPath(ctx, namePath);
        
        // Clip it
        CGContextClip(ctx);
        
        // Set up points for gradient
        CGPoint p1 = textStart;
        CGPoint p2 = CGPointMake(p1.x, p1.y + NAME_HEIGHT);
        
        // Create and draw gradient
        CGContextDrawLinearGradient(ctx, gradient, p1, p2,
                                    kCGGradientDrawsBeforeStartLocation |
                                    kCGGradientDrawsAfterEndLocation);
        
        // Restore G-State
        CGContextRestoreGState(ctx);
        
        CGContextAddPath(ctx, namePath);
        CGContextStrokePath(ctx);
        
        CGPathRelease(namePath);

        CGContextSaveGState(ctx);
        CGContextSetShadow(ctx, CGSizeMake(8, -9), 4.0);
        
        // Draw the photo
        CGRect photoRect;
        photoRect.origin.x = barRect.origin.x;
        photoRect.origin.y = CGRectGetMaxY(barRect) + NAME_HEIGHT - 10;
        photoRect.size = person.photo.size;
        
        CGContextDrawImage(ctx, photoRect, person.photo.CGImage);
        
        CGContextRestoreGState(ctx);
        
        idx++;
    }
    
    CGColorRelease(white);
    CGColorRelease(black);
    
    CGColorSpaceRelease(cs);
}

- (void)drawLayer:(CALayer *)layer
        inContext:(CGContextRef)ctx
{
    [self drawInContext:ctx bounds:layer.bounds];
}

- (void)drawInContext:(CGContextRef)ctx
               bounds:(CGRect)bounds
          heightRatio:(CGFloat)ratio
{
    _heightRatio = ratio;
    [self drawInContext:ctx bounds:bounds];
}


@end
