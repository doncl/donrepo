//
//  BNRReportLayer.h
//  SalesReport
//
//  Created by Jonathan Blocksom on 6/11/13.
//  Copyright (c) 2013 Jonathan Blocksom. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@class BNRReportRenderer;

// DEMO 1 - add the entire class so we can pass scale into renderer via custom draw method
@interface ReportLayer : CALayer

- (id)initWithRenderer:(BNRReportRenderer *)renderer;

@property (assign) CGFloat heightScale;

@end
