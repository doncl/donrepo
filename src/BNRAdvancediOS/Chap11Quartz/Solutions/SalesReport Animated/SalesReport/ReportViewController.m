//
//  BNRViewController.m
//  SalesReport
//
//  Created by Jonathan Blocksom on 4/1/13.
//  Copyright (c) 2013 Jonathan Blocksom. All rights reserved.
//

#import "ReportViewController.h"

#import "Person.h"
#import "ReportRenderer.h"

// DEMO 1
#import "ReportLayer.h"

@interface ReportViewController ()

// DEMO 1
@property ReportLayer *reportLayer;
@property ReportRenderer *reportRenderer;

@end

@implementation ReportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Set up people
        Person *fred = [[Person alloc] initWithName:@"Fred" image:nil];
        fred.sales = 134;
        
        Person *matt = [[Person alloc] initWithName:@"Matt" image:nil];
        matt.sales = 312;
        
        // Feel free to add your own people!
        
        NSArray *persons = @[fred, matt];
        
        self.reportRenderer = [[ReportRenderer alloc] initWithPersons:persons];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.reportLayer = [[ReportLayer alloc] initWithRenderer:self.reportRenderer];
    self.reportLayer.backgroundColor = [UIColor lightGrayColor].CGColor;
    self.reportLayer.geometryFlipped = YES;
    self.reportLayer.delegate = self.reportRenderer;
    self.reportLayer.heightScale = 0.0; // start at 0.0 to get nice animation with no glitch at start, or start at 1.0 for the naïve implementation.
    
    // Put it atop the view
    [self.view.layer addSublayer:self.reportLayer];

    // DEMO 3 - Create animate button
    const int HEIGHT = 60;
    CGRect buttonRect = CGRectMake((self.view.bounds.size.width - 200)/2.0,
                                   self.view.bounds.size.height - HEIGHT, 200, HEIGHT);
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = buttonRect;
    [button setTitle:@"Animate" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(animateChart:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];

}

- (void)viewDidLayoutSubviews
{
    CGRect bounds = self.view.bounds;
    
    bounds.size.height -= 60; // Leave space for the buttons
    
    self.reportLayer.bounds = bounds;
    self.reportLayer.anchorPoint = CGPointZero;
    self.reportLayer.position = CGPointZero;
    [self.reportLayer setNeedsDisplay];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"position"];
//    anim.toValue = [NSValue valueWithCGPoint:CGPointZero];
//    anim.fromValue = [NSValue valueWithCGPoint:CGPointMake(-600.0f, 0.0f)];
//    [self.reportLayer addAnimation:anim forKey:@"slide"];

    // DEMO 2
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"heightScale"];
    anim.toValue = @1.0;
    anim.fromValue = @0.0;
    anim.duration = 1.0;
    [self.reportLayer addAnimation:anim forKey:@"slide"];

    self.reportLayer.heightScale = 1.0;

}

// DEMO 3
- (void)animateChart:(id)sender {
    // DEMO 3a - Basic linear growth
//        CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"heightScale"];
//        anim.toValue = @1.0;
//        anim.fromValue = @0.0;
//        anim.duration = 1.0;
//        [self.reportLayer addAnimation:anim forKey:@"slide"];
//    
//        [self.reportLayer setValue:@1.0 forKey:@"heightScale"];

    // DEMO 3b - Growth with a little overshoot and bounce back. The best part!
        CAKeyframeAnimation *bounceAnim = [CAKeyframeAnimation animationWithKeyPath:@"heightScale"];
        bounceAnim.values = @[@1.0, @0.0, @1.0, @1.1, @1.0];
        bounceAnim.keyTimes = @[@0.0, @0.3, @0.8, @0.9, @1.0];
        bounceAnim.duration = 1.0;
        bounceAnim.calculationMode = kCAAnimationCubic;
        bounceAnim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        [self.reportLayer addAnimation:bounceAnim forKey:@"grow"];

}

@end
