# Animated Sales Report Demo

## Summary
This version adds a custom CALayer with a scaling property for animation.

## Notes
Areas that have changed from the chapter's solution are marked with
"// DEMO".

A custom subclass of CALayer, *ReportLayer*, has been added. It has a *heightScale* property.
It then calls a new method on the ReportRenderer so it can pass along this scale.

The ReportRenderer uses the scale for the height of the bars, as well as the number
inside.

The developer can then write CAAnimations that change this heightScale property,
just like they can against built-in properties such as position. In this solution,
note that the bars animate when the view first appears. There is also an animate button
which will make the bars zoom down, then bounce back up. This effect can be easily changed.
Take a look in the viewDidAppear: and animateChart: methods of the view controller.

Note that there are other possible ways to achieve this effect, and in fact 
to even have custom CALayer properties without subclassing. These techniques also
make it possible to use UIView animation blocks, unlike the technique shown in this
demo. Here are a couple of useful references:

http://www.objc.io/issue-12/view-layer-synergy.html

https://gist.github.com/nicklockwood/d374033b27c62662ac8d
