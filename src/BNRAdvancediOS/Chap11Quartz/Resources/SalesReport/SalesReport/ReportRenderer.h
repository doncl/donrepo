//
//  ReportRenderer.h
//  SalesReport
//
//  Created by Don Clore on 2/24/16.
//  Copyright © 2016 Jonathan Blocksom. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Person;


@interface ReportRenderer : NSObject
-(id)initWithPersons:(NSArray *)persons;

// Assumes the context is flipped;
-(void)drawInContext:(CGContextRef)context bounds:(CGRect)bounds;

// Just a stub that calls drawInContext:bounds
-(void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx;


@end
