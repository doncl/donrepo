//
//  ReportRenderer.m
//  SalesReport
//
//  Created by Don Clore on 2/24/16.
//  Copyright © 2016 Jonathan Blocksom. All rights reserved.
//

@import CoreText;

#import "ReportRenderer.h"
#import "Person.h"

#define NAME_HEIGHT (95.0f)
// NAME_HEIGHT is the height of the person's name in points

// RANGE is the range of values that are OK
#define RANGE (700.0f)

// B_MARGIN is the bottom margin in points.
#define B_MARGIN (10.0f)

// T_MARGIN is the top margin in points.
#define T_MARGIN (10.0f)

// H_GAP is the gap between the bars and side margins
#define H_GAP (10.4f)

@interface ReportRenderer() {
    CGImageRef backgroundImage;
    CGRect backgroundRect;
}
@property (nonatomic, strong) NSArray *persons;
@property (nonatomic, strong) UIFont *nameFont;
@property (nonatomic, strong) UIFont *amountFont;
@end

@implementation ReportRenderer

- (id)initWithPersons:(NSArray *)persons
{
    self = [super init];
    if (self) {
        self.persons = persons;

        UIImage *tempImage = [UIImage imageNamed:@"flowers.jpg"];
        backgroundImage = tempImage.CGImage;
        CGImageRetain(backgroundImage);

        backgroundRect.origin = CGPointZero;
        backgroundRect.size = tempImage.size;

        self.nameFont = [UIFont fontWithName:@"Palatino-BoldItalic" size:95.0];
        self.amountFont = [UIFont fontWithName:@"Verdana-Bold" size:30.0];
    }
    return self;
}

-(void)dealloc
{
    CGImageRelease(backgroundImage);
}

- (void)drawInContext:(CGContextRef)context bounds:(CGRect)bounds
{
    NSLog(@"Drawing in %@", NSStringFromCGRect(bounds));

    CGColorSpaceRef cs = CGColorSpaceCreateDeviceRGB();

    CGFloat blackRGBA[] = {0, 0, 0, 1};
    CGColorRef black = CGColorCreate(cs, blackRGBA);

    CGFloat whiteRGBA[] = {1, 1, 1, 1};
    CGColorRef white = CGColorCreate(cs, whiteRGBA);

    CGContextSetStrokeColorWithColor(context, black);
    CGContextSetLineWidth(context, 1);

    CGContextSetFillColorWithColor(context, white);

    // Prepare a gradient
    CGFloat colors[] = {
      0.2, 0.2, 0.2, 1.0, // dark grey
      0.8, 0.8, 0.8, 1.0, // light grey
    };

    CGFloat locations[] = {0.0, 1.0};
    CGGradientRef gradient = CGGradientCreateWithColorComponents(cs, colors, locations, 2);

    CGFloat minY = CGRectGetMinY(bounds) + B_MARGIN;
    CGFloat maxY = CGRectGetMaxY(bounds) - T_MARGIN;
    CGFloat increment = (maxY - minY) / RANGE;

    NSInteger personCount = [self.persons count];
    CGFloat totalHGaps = (personCount + 1) * H_GAP;
    CGFloat barWidth = (bounds.size.width - totalHGaps) / personCount;

    // Draw the bars
    CGRect barRect;
    barRect.size.width = barWidth;
    barRect.origin.y = minY;

    NSInteger index = 0;

    for (Person *person in self.persons) {
        barRect.origin.x = bounds.origin.x + H_GAP + (barWidth + H_GAP) * index;
        barRect.size.height = increment * person.sales;

        CGRect converted = CGContextConvertRectToDeviceSpace(context, barRect);
        converted = CGRectIntegral(converted);
        converted.origin.x += 0.5;
        converted.origin.y += 0.5;
        barRect = CGContextConvertRectToUserSpace(context, converted);

        CGPathRef barPath = CGPathCreateWithRoundedRect(barRect, 7.0, 7.0, NULL);

        // Fill the bar with flowers
        CGContextSaveGState(context);
        CGContextAddPath(context, barPath);
        CGContextClip(context);
        CGContextDrawImage(context, backgroundRect, backgroundImage);
        CGContextRestoreGState(context);

        // Fill path cleared the current path, so add it again.
        CGContextAddPath(context, barPath);
        CGContextStrokePath(context);
        CGPathRelease(barPath);

        // Create attributed string for sales label.
        NSString *soldString = [NSString stringWithFormat:@"%ld units", (long)person.sales];
        NSDictionary *attribs = @{
            NSFontAttributeName : self.amountFont,
            NSForegroundColorAttributeName : [UIColor whiteColor],
        };

        NSAttributedString *displayAttrString =
        [[NSAttributedString alloc] initWithString:soldString attributes:attribs];

        // Calculate where string should go
        CGSize strSize = displayAttrString.size;

        // Negative Y because Y coordinate will be flipped.
        CGPoint textPos = CGPointMake(CGRectGetMaxX(barRect) - strSize.width, -CGRectGetMaxY(barRect));

        // Draw string
        UIGraphicsPushContext(context);
        CGContextSaveGState(context);
        CGContextScaleCTM(context, 1.0, -1.0);
        [displayAttrString drawAtPoint:textPos];
        CGContextRestoreGState(context);
        UIGraphicsPopContext();

        // Draw the name with a gradient.

        // Save state since we're changing clipping region.
        CGContextSaveGState(context);

        // Get path for name.
        CGPoint textStart = CGPointMake(barRect.origin.x, CGRectGetMaxY(barRect) + 10.0);
        CGPathRef namePath = [self newPathForName:person.name atPoint:textStart];
        CGContextAddPath(context, namePath);

        // Clip to it.
        CGContextClip(context);

        // Set up points for gradient.
        CGPoint p1 = textStart;
        CGPoint p2 = CGPointMake(p1.x, p1.y + NAME_HEIGHT);

        // Create and draw gradient
        CGContextDrawLinearGradient(context, gradient, p1, p2,
                kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);

        // Restore GState
        CGContextRestoreGState(context);

        // Draw the outline
        CGContextAddPath(context, namePath);
        CGContextStrokePath(context);

        CGPathRelease(namePath);

        CGContextSaveGState(context);
        CGContextSetShadow(context, CGSizeMake(8, -9), 4.0);

        // Draw the photo
        CGRect photoRect;
        photoRect.origin.x = barRect.origin.x;
        photoRect.origin.y = CGRectGetMaxY(barRect) + NAME_HEIGHT - 10;
        photoRect.size = person.photo.size;

        CGContextDrawImage(context, photoRect, person.photo.CGImage);

        CGContextRestoreGState(context);

        index++;
    }

    CGColorRelease(white);
    CGColorRelease(black);

    CGColorSpaceRelease(cs);
}

- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
    [self drawInContext:ctx bounds:layer.bounds];
}

- (CGPathRef)newPathForName:(NSString *)s atPoint:(CGPoint)startPoint
{
    // Get the unicodes for the string
    CFIndex length = [s length];
    UniChar *cBuffer = malloc(length * sizeof(UniChar));
    [s getCharacters:cBuffer range:NSMakeRange(0, length)];

    // Figure out the font
    CTFontRef nameFont = CTFontCreateWithName(CFSTR("Palatino-BoldItalic"), NAME_HEIGHT, NULL);

    // Get the glyphs
    CGGlyph *gBuffer = malloc(length * sizeof(CGGlyph));
    CTFontGetGlyphsForCharacters(nameFont, cBuffer, gBuffer, length);

    // Get the advances between the glyphs.
    CGSize *advances = malloc(length * sizeof(CGSize));
    CTFontGetAdvancesForGlyphs(nameFont, kCTFontDefaultOrientation, gBuffer, advances, length);

    // Create a CGMutablePath to hold the whole string.
    CGMutablePathRef fullPath = CGPathCreateMutable();
    CGAffineTransform currentTransform = CGAffineTransformMakeTranslation(startPoint.x, startPoint.y);

    for (int i = 0; i < length; i++) {
        // Get the path for a particular glyph.
        CGPathRef glyphPath = CTFontCreatePathForGlyph(nameFont, gBuffer[i], &currentTransform);

        // Add it to the larger path
        CGPathAddPath(fullPath, NULL, glyphPath);
        CGPathRelease(glyphPath);

        // Move forward by the advance.
        CGSize currentAdvance = advances[i];

        currentTransform = CGAffineTransformTranslate(currentTransform, currentAdvance.width,
                currentAdvance.height);

    }

    // Cleanup
    free(cBuffer);
    free(gBuffer);
    free(advances);

    return fullPath;
}

@end
