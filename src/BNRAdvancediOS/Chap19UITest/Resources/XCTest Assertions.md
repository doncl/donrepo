# XCTest Assertions

Assertions fall into five categories:

*  Unconditional failure
*  Nil tests
*  Boolean tests
*  Equality tests
*  Exception tests


## Unconditional failure:

### `XCTFail(format ...)`  

Generates a failure unconditionally.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


## Nil tests:

### `XCTAssertNil(expression, format ...)`  

Generates a failure when (`expression` != `nil`).  
`expression`  An expression of id type.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertNotNil(expression, format ...)`  

Generates a failure when (`expression` == `nil`).  
`expression`  An expression of id type.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


## Boolean tests:

### `XCTAssert(expression, format ...)`  

Generates a failure when (`expression` == `false`).  
`expression`  An expression of boolean type.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertTrue(expression, format ...)`  

Generates a failure when (`expression` == `false`).  
`expression`  An expression of boolean type.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertFalse(expression, format ...)`  

Generates a failure when (`expression` != `false`).  
`expression`  An expression of boolean type.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.

## Equality tests:

### `XCTAssertEqualObjects(expression1, expression2, format ...)`  

Generates a failure when (`expression1` not equal to `expression2`).  
`expression1`  An expression of id type.  
`expression2`  An expression of id type.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertNotEqualObjects(expression1, expression2, format ...)`  

Generates a failure when (`expression1` equal to `expression2`).  
`expression1`  An expression of id type.  
`expression2`  An expression of id type.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertEqual(expression1, expression2, format ...)`  

Generates a failure when (`expression1` != `expression2`).  
`expression1` An expression of C scalar type.  
`expression2` An expression of C scalar type.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertNotEqual(expression1, expression2, format ...)`  

Generates a failure when (`expression1` == `expression2`).  
`expression1` An expression of C scalar type.  
`expression2` An expression of C scalar type.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertEqualWithAccuracy(expression1, expression2, accuracy, format ...)`  

Generates a failure when (difference between `expression1` and `expression2` is > `accuracy`).  
`expression1` An expression of C scalar type.  
`expression2` An expression of C scalar type.  
`accuracy` An expression of C scalar type describing the maximum difference between `expression1` and `expression2` for these values to be considered equal.
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertNotEqualWithAccuracy(expression1, expression2, accuracy, format ...)`  

Generates a failure when (difference between `expression1` and `expression2` is <= `accuracy`).  
`expression1` An expression of C scalar type.  
`expression2` An expression of C scalar type.  
`accuracy` An expression of C scalar type describing the maximum difference between `expression1` and `expression2` for these values to be considered equal.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertGreaterThan(expression1, expression2, format ...)`  

Generates a failure when (`expression1` <= `expression2`).  
`expression1` An expression of C scalar type.  
`expression2` An expression of C scalar type.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertGreaterThanOrEqual(expression1, expression2, format ...)`  

Generates a failure when (`expression1` < `expression2`).  
`expression1` An expression of C scalar type.  
`expression2` An expression of C scalar type.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertLessThan(expression1, expression2, format ...)`  

Generates a failure when (`expression1` >= `expression2`).  
`expression1` An expression of C scalar type.  
`expression2` An expression of C scalar type.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertLessThanOrEqual(expression1, expression2, format ...)`  

Generates a failure when (`expression1` > `expression2`).  
`expression1` An expression of C scalar type.  
`expression2` An expression of C scalar type.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


## Exception tests:

### `XCTAssertThrows(expression, format ...)`  

Generates a failure when (`expression` does not throw).  
`expression` An expression.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertThrowsSpecific(expression, exception_class, format ...)`  

Generates a failure when (`expression` does not throw `exception_class`).  
`expression` An expression.  
`exception_class` The class of the exception. Must conform to the ErrorType protocol.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertThrowsSpecificNamed(expression, exception_class, exception_name, format ...)`  

Generates a failure when (`expression` does not throw `exception_class` with `exception_name`).  
`expression` An expression.  
`exception_class` The class of the exception. Must conform to the ErrorType protocol.  
`exception_name` The name of the exception.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertNoThrow(expression, format ...)`  

Generates a failure when (`expression` throws).  
`expression` An expression.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertNoThrowSpecific(expression, exception_class, format ...)`  

Generates a failure when (`expression` throws `exception_class`).  
`expression` An expression.  
`exception_class` The class of the exception. Must conform to the ErrorType protocol.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.


### `XCTAssertNoThrowSpecificNamed(expression, exception_class, exception_name, format ...)`  

Generates a failure when (`expression` throws `exception_class` with `exception_name`).  
`expression` An expression.  
`exception_class` The class of the exception. Must conform to the ErrorType protocol.  
`exception_name` The name of the exception.  
`format` An optional supplementary description of the failure. A literal String, optionally with string format specifiers. This parameter can be completely omitted.

