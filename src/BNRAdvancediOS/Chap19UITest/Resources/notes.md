# UI Testing

Yay ui testing, taking it out of Instruments and JavaScript, and puts it in to
Xcode and Swift/ObjC.


### TL;DR:

* Integrated with existing unit test features, like the test browser

* Unlike logic tests, UI tests operate via proxies to your app
  - XCUIElement

* You can find and filter collections of elements
  - XCUIElementQuery

* Based on accessibility framework

* System classes already work pretty nicely

* Turn on the record mode to capture app actions as Swift code
  - Then you can add specific logic tests

* Can capture screen shots


### Possible Demos

* Jeremy's [blog post](https://www.bignerdranch.com/blog/ui-testing-in-xcode-7-part-1-ui-testing-gotchas/)
is amazing. 
  - This for the demo, minus the race condition stuff (couldn't repro), will be quite compelling

* Show where the xcode logs are and how to find your caveman debugging in there

* Bonus: Turn on code coverage.  [Bork's blog](https://www.bignerdranch.com/blog/weve-got-you-covered/)
  could be a good starting place on grokking it

### Possible exercise

* Boil down Jeremy's blog?

* Do similar stuff but with HomePwner?

* "ok, take your own app, and add UI tests.  Try this. Try that. Try the other"



### Outline

* Testing, something we all know we should do
  - But usually we don't
  - We've had logic testing for a long time
    - XCTest and friends
    - Even things like TDD to encourage writing these tests
  - UI Testing _has_ existed before
    - But it was Instruments and JavaScript
    - and it sucked

* Xcode UI Testing - similar to existing logic tests
  - Works inside of Xcode
  - Write your tests in Swift
  - Use same workflow as with logic tests
    - little green and red icons in the gutter
    - see reports at the end
    - Xcode Server support
      - can actually attach devices and have them run
  - the UI testing is much, much slower
    - so limit it to just UI stuff

* Different than existing logic tests
  - Actually drives your program
  - Your test code does _not_ run inside the app
    - Instead everything is accessed by proxies
  - Your code is at arm's-length from the actual application
    - An (unintended?) side effect is that it makes it more difficult for you to
      do a lot of work in UI tests
    - so you could/should do more testing in your logic tests

* Accessibility is the key
  - UI Testing uses the metadata surfaced by Accessibility to do its thing
  - If you use default CocoaTouch objects (or your own subclasses)
    - You get a lot of this for free
  - Your own custom classes may need extra accessibility work
  - Sometimes even when using factory classes, you may need to do some accessibility work
    to surface information for your tests

* Recording mode
  - (hop out for quick demo of recording mode)
  - You can turn on recording mode
    - Xcode watches what you do with your app
    - Generates code in the test method that has the insertion point
    - This is real-live Swift code, not JavaScript
    - The generated code is actually pretty reasonable / readable
  - The work is primarily finding the _elements_ of interest
    - Such as a button, text field, or label
  - There is API for getting groups of elements
  - And for filtering it down to the element(s) of interest

* XCUIElement
  - Represents a single element
    - Which may or may not be there : e.g. `.exists`
  - There are some attributes you can get
    - type.  There's lots of types
      - Any
      - Other
      - Application
      - Group
      - Window
      - Alert
      - Button
      - Navigation bar
      - Tab Bar
      - Table
      - Cell
    - Unfortunately the types aren't segregated by platform
      - So you'll see OS X types in amongst your iOS types
      - So best to do a quick recording of your app and see what it gives you
      	rather than trying to piece out what belongs to what platform

  - Can base queries off of it
    - what's a query?  _(next)_

* XCUIApplication
  - Use to launch your app
  - A top level element
    - Actually an honest-to-Baud element
  - Good starting point for creating queries

* XCUIElementQuery
  - Represents the location of an element or group of elements
    - think of them like URLs
    - You create them
      - Then when evaluated, reflect the current state of the system
  - For example
    ```
        let firstTable = XCUIApplication().tables.elementAtIndex(0)

        // Current tableview has three cells
        print("ook: \(firstTable.cells.count)")  // prints 3
        XCUIApplication().tables.staticTexts["Due Today"].tap()

        // Pushes a tableview with two cells
        print("ook: \(firstTable.cells.count)")  // prints 2
    ```    
    - Notice that "firstTable" hasn't changed
    - But things based on it do change
  - There are conveniences for each of the different built-in element types
    - e.g. .tables, .staticTexts, .buttons
    - Due to the way things are structured, you can get these off of queries and elements
  - Also a more direct API

* Containment types
  - Elements can be gotten by different containment types
  - Descendants
    - The whole subtree under an element
    - i.e. recursive
    - So say start at a table, this can get you the cells and each cell's containing label
  - Children
    - The set immediate elements under an element
    - non-recursive
    - So say start at a table, this can get you the cells
  - Containment
    - Which is the subtree (?) inside of a particular element
    - So say start at a Cell (Which are pretty interchangeable)
    - containment would mean getting the particular label element in a cell
    - _I'm still fuzzy on what containment means_
  - Can get collections of stuff
    - e.g. `descendantsMatchingType` / `childrenMatchingType`

* Filtering
  - You can take a collection of stuff and boil it down
    - accessing by index - `elementBoundByIndex`
      - the elementAtIndex in the wwdc video is deprecated :-|
    - keyed subscript via query["key:]
    - can match a type and accessibility identifier
    - can also provide an NSPredicate
  - You'll need to get individual elements before you can do stuff with them

* Whacking elements a.k.a. event Synthesis
  - You can kind of manipulate elements event-wise
    - but it's not the full suite of possibilities
    - Like you can't send arbitrary touches and drags
  - Once you have an element, you can
    - tap on it
      - tapWithNumberOfTaps:numberOfTouches:
        - a convenience doubleTap
        - twoFingerTap
    - pressForDuration
      - for doing long-presses
      - optionally drag to a particular coordinate
    - typeText
      - Feed it a complete string
    - swipeUp | Down | Left | Right
    - pinch with velocity
    - rotate with velocity

* One More Thing - code coverage
  - A neat feature also introduced in Xc7
  - Turn it on in your scheme
  - Run your tests
  - get a report on % of lines covered
  - Browser lets you see who is high and low on the amount of coverage
  - in-editor view of code that has no coverage at all
