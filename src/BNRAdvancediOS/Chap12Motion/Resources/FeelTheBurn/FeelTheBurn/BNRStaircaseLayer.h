//
//  BNRStaircaseLayer.h
//  FeelTheBurn
//
//  Created by Don Clore on 2/24/16.
//  Copyright © 2016 Jonathan Blocksom. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface BNRStaircaseLayer : CALayer
@property (nonatomic, assign) NSUInteger stepCount;
@property (nonatomic, assign) NSUInteger dottedStepIndex;
@end
