//
//  BNRFireView.h
//  FeelTheBurn
//
//  Created by Don Clore on 2/24/16.
//  Copyright © 2016 Jonathan Blocksom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNRFireView : UIView
-(void)startBurn;
-(void)stopBurn;
@end
