//
//  BNRStaircaseView.m
//  FeelTheBurn
//
//  Created by Don Clore on 2/24/16.
//  Copyright © 2016 Jonathan Blocksom. All rights reserved.
//

#import "BNRStaircaseView.h"
#import "BNRStaircaseLayer.h"

@implementation BNRStaircaseView
+ (Class)layerClass
{
    return [BNRStaircaseLayer class];
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.layer.backgroundColor = [UIColor clearColor].CGColor;
    }
    return self;
}
@end
