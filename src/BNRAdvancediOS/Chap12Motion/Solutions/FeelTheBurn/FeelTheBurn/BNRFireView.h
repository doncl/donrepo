//
//  BNRFireView.h
//  FeelTheBurn
//
//  Created by Jonathan Blocksom on 10/25/13.
//  Copyright (c) 2013 Jonathan Blocksom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNRFireView : UIView

- (void)startBurn;
- (void)stopBurn;

@end
