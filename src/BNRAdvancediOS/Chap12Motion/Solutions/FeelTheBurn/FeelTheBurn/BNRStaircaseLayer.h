//
//  BNRStaircaseLayer.h
//  FeelTheBurn
//
//  Created by Jonathan Blocksom on 10/28/13.
//  Copyright (c) 2013 Jonathan Blocksom. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface BNRStaircaseLayer : CALayer

@property (nonatomic, assign) NSUInteger stepCount;
@property (nonatomic, assign) NSUInteger dottedStepIndex;

@end
