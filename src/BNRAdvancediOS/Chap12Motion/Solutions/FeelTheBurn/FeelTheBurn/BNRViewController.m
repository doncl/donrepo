//
//  BNRViewController.m
//  FeelTheBurn
//
//  Copyright (c) 2013 Big Nerd Ranch, LLC. All rights reserved.
//

#import "BNRViewController.h"

#import "BNRFireView.h"

@import CoreMotion;

@interface BNRViewController ()

@property (nonatomic, strong) CMPedometer *pedometer;

@property (weak, nonatomic) IBOutlet UILabel *numStepsLabel;
@property (weak, nonatomic) IBOutlet UILabel *stepsLabel;

@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@property (weak, nonatomic) IBOutlet BNRFireView *fireView;
@property (weak, nonatomic) IBOutlet UIView *stairView;

@end

@implementation BNRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.view.layer.contents = (__bridge id)[UIImage imageNamed:@"graynoise.jpg"].CGImage;
    
    UIInterpolatingMotionEffect *buttonMove =
        [[UIInterpolatingMotionEffect alloc]
         initWithKeyPath:@"center.x"
         type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    buttonMove.minimumRelativeValue = @-20.0;
    buttonMove.maximumRelativeValue = @20.0;
    [self.startButton addMotionEffect:buttonMove];
    [self.resetButton addMotionEffect:buttonMove];
    
//    CABasicAnimation *stepAnim = [CABasicAnimation animationWithKeyPath:@"dottedStepIndex"];
//    stepAnim.fromValue = @0;
//    stepAnim.toValue = @10;
//    stepAnim.duration = 3.0;
//    [self.stairView.layer addAnimation:stepAnim forKey:@"climb"];
    
    UIInterpolatingMotionEffect *stairMove =
    [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"dottedStepIndex" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    stairMove.minimumRelativeValue = @0;
    stairMove.maximumRelativeValue = @10;
    [self.stairView addMotionEffect:stairMove];
}

- (IBAction)startCount:(id)sender {
    if ([CMPedometer isStepCountingAvailable]) {
        self.pedometer = [[CMPedometer alloc] init];
        [self.pedometer startPedometerUpdatesFromDate:[NSDate date]
                                          withHandler:
         ^(CMPedometerData *pedometerData, NSError *error) {
            [NSOperationQueue.mainQueue addOperationWithBlock: ^{
                NSInteger numberOfSteps = pedometerData.numberOfSteps.integerValue;
                self.numStepsLabel.text = [NSString stringWithFormat:@"%05ld", (long)numberOfSteps];
                }];
         }];

    } else {
        self.numStepsLabel.text = @"😞";
        self.stepsLabel.text = @"No Counter";
    }
    
    [self.fireView startBurn];
}

- (IBAction)resetCount:(id)sender {
    [self.pedometer stopPedometerUpdates];
    self.pedometer = nil;
    
    [self.fireView stopBurn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
