//
//  BNRDocument.m
//  Notery
//
//  Created by Dillan Laughlin on 1/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Document.h"

NSString *kDocumentContentsDidUpdateNotification = @"kBNRDocumentContentsDidUpdateNotification";

@interface Document ()

@property (nonatomic, assign) BOOL iveBeenClosedOnceAlready;
@end


@implementation Document

-(id)initWithFileURL:(NSURL *)url
{
    self = [super initWithFileURL:url];
    if (self)
    {
        [self setContents:NSLocalizedString(@"New Document", @"New Document")];
    }
    return self;
}

// Disk -> Data Model
-(BOOL)loadFromContents:(id)contents 
                 ofType:(NSString *)typeName 
                  error:(NSError **)outError
{
    NSString *myContents = @"";
    
    if ([contents length] > 0)
    {
        myContents = [[NSString alloc] initWithData:contents 
                                           encoding:NSUTF8StringEncoding];
    }
    [self setContents:myContents];
    [[NSNotificationCenter defaultCenter] postNotificationName:
                                                kDocumentContentsDidUpdateNotification 
                                                        object:self];
    
    return YES;
}

// Data Model -> Disk
-(id)contentsForType:(NSString *)typeName error:(NSError **)outError
{
    return [self.contents dataUsingEncoding:NSUTF8StringEncoding];
}


-(void)openWithCompletionHandler:(void ( ^)(BOOL))completionHandler
{
    NSAssert(![self iveBeenClosedOnceAlready], @"Document objects are really one-offs");
    [super openWithCompletionHandler:completionHandler];
}

-(void)closeWithCompletionHandler:(void ( ^)(BOOL))completionHandler
{
    NSAssert(![self iveBeenClosedOnceAlready], @"Document objects are really one-offs");
    [self setIveBeenClosedOnceAlready:YES];
    [super closeWithCompletionHandler:completionHandler];
}

# pragma mark - Accessors
@synthesize contents = _contents;
-(void)setContents:(NSString *)contents
{
    if (![_contents isEqualToString:contents])
    {
        _contents = [contents copy];
    }
}
@synthesize iveBeenClosedOnceAlready = _iveBeenClosedOnceAlready;

@end
