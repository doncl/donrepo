//
//  BNRDocumentListTableViewController.m
//  Notery
//
//  Created by Dillan Laughlin on 1/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DocumentListTableViewController.h"
#import "SingleDocumentViewController.h"
#import "DocumentStorage.h"
#import "Document.h"

@interface DocumentListTableViewController ()

@property (nonatomic, readonly, weak) NSArray *documentURLs;
-(void)showDocumentURL:(NSURL *)documentURL;

@end

@implementation DocumentListTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Adding docs
-(IBAction)addDocument:(id)sender;
{
    UIAlertView *alertView = [[UIAlertView alloc] 
                              initWithTitle:NSLocalizedString(@"New Document", @"New Document") 
                              message:NSLocalizedString(@"Choose a name for your new document.", 
                                                        @"Choose a name for your new document.") 
                              delegate:self 
                              cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") 
                              otherButtonTitles:NSLocalizedString(@"Create", @"Create"), 
                              nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *field = [alertView textFieldAtIndex:0];
    field.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    [alertView show];
}

-(BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    NSString *name = [[alertView textFieldAtIndex:0] text];
    
    return (name.length > 0);
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSString *name = [[alertView textFieldAtIndex:0] text];
        NSURL *documentURL = [[DocumentStorage sharedDocumentStorage] 
                              addDocumentWithName:name];
        [self showDocumentURL:documentURL];
    }
}

#pragma mark - Navigation
-(void)showDocumentURL:(NSURL *)documentURL;
{
    Document *document = nil;
    if (documentURL)
    {
        document = [[Document alloc] initWithFileURL:documentURL];
        NSInteger indexOfDocument = [self.documentURLs indexOfObject:documentURL];
        if (indexOfDocument != NSNotFound)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexOfDocument inSection:0];
            if (![[self.tableView indexPathForSelectedRow] isEqual:indexPath])
            {
                [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            }
        }
    }
    [self.documentViewController setDocument:document];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateUbiquitousDocuments:)
                                                 name:kDocumentListChangedExternallyNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(documentRemoved:)
                                                 name:kDocumentListChangedRemovalNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(documentAdded:)
                                                 name:kDocumentListChangedAdditionNotification
                                               object:nil];
    
    [self.navigationController setToolbarHidden:NO animated:NO];
    if ([self.documentURLs count])
    {
        NSURL *docUrl = [self.documentURLs objectAtIndex:0];
        [self showDocumentURL:docUrl];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kDocumentListChangedExternallyNotification 
                                                  object:nil];    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kDocumentListChangedRemovalNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kDocumentListChangedAdditionNotification
                                                  object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

-(UIView *)cloudToolbarView
{
    UIView *cloudView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 160.0, 33.0)];
    UILabel *cloudLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 3.0, 80.0, 27.0)];
    
    cloudLabel.text = NSLocalizedString(@"Use iCloud", @"Use iCloud");
    cloudLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    cloudLabel.textColor = [UIColor whiteColor];
    cloudLabel.backgroundColor = [UIColor clearColor];
    cloudLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [cloudView addSubview:cloudLabel];
    UISwitch *cloudSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(81.0, 3.0, 79.0, 27.0)];
    cloudSwitch.enabled = [[DocumentStorage sharedDocumentStorage] hasICloud];
    cloudSwitch.on = [[DocumentStorage sharedDocumentStorage] useiCloud];
    [cloudSwitch addTarget:self 
                    action:@selector(changeiCloud:) 
          forControlEvents:UIControlEventValueChanged];
    [self setCloudSwitch:cloudSwitch];
    [cloudView addSubview:cloudSwitch];
    return cloudView;
}

-(void)awakeFromNib
{
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithCustomView:[self cloudToolbarView]];
    UIBarButtonItem *item3 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    
    self.toolbarItems = [NSArray arrayWithObjects:item1, item2, item3, nil];
}


- (void)changeiCloud:(id)sender
{
    [[DocumentStorage sharedDocumentStorage] setUseiCloud:[sender isOn]];
}


#pragma mark - editing adding removing

-(void)documentAdded:(NSNotification *)note
{
    [self.tableView reloadData];
}

-(void)documentRemoved:(NSNotification *)note
{
    [self.tableView reloadData];
}

-(void)updateUbiquitousDocuments:(NSNotification *)notification
{
    [self.tableView reloadData];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSURL *url = [self.documentURLs objectAtIndex:indexPath.row];
        
        [[DocumentStorage sharedDocumentStorage] removeDocumentURL:url];
    }
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    Document *document = [self.documentViewController document];
    NSURL *documentURL = [document fileURL];
    if (documentURL)
    {
        NSUInteger row = [self.documentURLs indexOfObject:documentURL];
        if (row == NSNotFound)
        {
            NSArray *documentURLs = [self documentURLs];
            if ([documentURLs count] > 0)
            {
                documentURL = [documentURLs objectAtIndex:0];
                [self showDocumentURL:documentURL];
            }
        }
        else
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
            [self.tableView selectRowAtIndexPath:indexPath animated:animated scrollPosition:UITableViewScrollPositionNone];
        }
    }
}


#pragma mark - Table view data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = [self.documentURLs count];
    return count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DocumentListTableViewControllerCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSURL *url = [self.documentURLs objectAtIndex:indexPath.row];
    NSString *cellText = [[url lastPathComponent] stringByDeletingPathExtension];
    
    [cell.textLabel setText:cellText];
}

#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    
    if ([self.documentURLs count] > row)
    {
        id documentURL = [self.documentURLs objectAtIndex:row];
        [self showDocumentURL:documentURL];
    }
}

#pragma mark - Accessors
-(NSArray *)documentURLs
{
    NSArray *docArray = [[DocumentStorage sharedDocumentStorage] documentURLs];
    return docArray;
}

@synthesize addDocumentButton = _addDocumentButton;
@synthesize cloudSwitch = _cloudSwitch;
@synthesize documentViewController = _detailViewController;

@end
