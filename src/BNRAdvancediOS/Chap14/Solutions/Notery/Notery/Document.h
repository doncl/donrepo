//
//  BNRDocument.h
//  Notery
//
//  Created by Dillan Laughlin on 1/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

extern NSString *kDocumentContentsDidUpdateNotification;

@interface Document : UIDocument

@property (copy, nonatomic) NSString *contents;

@end
