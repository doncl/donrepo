//
//  BNRAppDelegate.m
//  Notery
//
//  Created by Dillan Laughlin on 1/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "SingleDocumentViewController.h"
#import "DocumentListTableViewController.h"

@implementation AppDelegate

@synthesize window = _window;

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
    NSArray *viewControllers = [splitViewController viewControllers];
    UINavigationController *navigationController = [viewControllers lastObject];
    SingleDocumentViewController *singleDocViewController = (SingleDocumentViewController *)navigationController.topViewController;
    
    splitViewController.delegate = singleDocViewController;
    
    navigationController = [viewControllers objectAtIndex:0];
    DocumentListTableViewController *docListTableViewController = (DocumentListTableViewController *)navigationController.topViewController;
    [docListTableViewController setDocumentViewController:singleDocViewController];
    
    return YES;
}

@end