//
//  BNRDocumentStorage.m
//  Notery
//
//  Created by Dillan Laughlin on 1/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DocumentStorage.h"
#import "Document.h"

NSString *kDocumentExtension = @"noteryDoc";
NSString *kDocumentListChangedAdditionNotification = @"kDocumentListChangedAdditionNotification";
NSString *kDocumentListChangedRemovalNotification = @"kDocumentListChangedRemovalNotification";
NSString *kDocumentListChangedExternallyNotification = @"kDocumentListChangedExternallyNotification";
NSString *kDocumentListRemovedIndexPathKey = @"kDocumentListRemovedIndexPathKey";
NSString *kDocumentListAddedIndexPathKey = @"kDocumentListAddedIndexPathKey";

static NSString *const kUseiCloud = @"UseiCloud";

@interface DocumentStorage ()
{
    NSMutableArray *_documentURLs;
}

@property (strong, nonatomic) NSMetadataQuery *query;

-(NSURL *)localDocumentsURL;
-(NSURL *)ubiquitousDocumentsURL;
-(void)clearOutAndRefreshDocumentURLs;

@end

@implementation DocumentStorage

+(DocumentStorage *)sharedDocumentStorage
{
    static dispatch_once_t pred;
    static DocumentStorage *docStore = nil;
    
    dispatch_once(&pred, ^{ docStore = [[self alloc] init]; });
    return docStore;
}


-(id)init
{
    if (self = [super init])
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(documentStateChanged:)
                                                     name:UIDocumentStateChangedNotification
                                                   object:nil];
        
        [self setUseiCloud:[[NSUserDefaults standardUserDefaults] boolForKey:kUseiCloud]];
        
        NSURL *ubiquitousDocumentsURL = [self ubiquitousDocumentsURL];
        if (ubiquitousDocumentsURL)
        {
            NSMetadataQuery *query = [[NSMetadataQuery alloc] init];
            query.predicate = [NSPredicate predicateWithFormat:@"%K ENDSWITH[c] %@", NSMetadataItemFSNameKey, kDocumentExtension];
            
            query.searchScopes = [NSArray arrayWithObject:NSMetadataQueryUbiquitousDocumentsScope];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUbiquitousDocuments:) name:NSMetadataQueryDidFinishGatheringNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUbiquitousDocuments:) name:NSMetadataQueryDidUpdateNotification object:nil];
            [query startQuery];
            self.query = query;
        }
    }
    return self;
}

-(void)documentStateChanged:(NSNotification *)notification
{
    UIDocument *document = [notification object];
    UIDocumentState documentState = document.documentState;
    
    if (documentState & UIDocumentStateInConflict)
    {
        // This application uses a basic newest version wins conflict resolution strategy
        NSURL *documentURL = document.fileURL;
        NSArray *conflictVersions =
        [NSFileVersion unresolvedConflictVersionsOfItemAtURL:documentURL];
        for (NSFileVersion *fileVersion in conflictVersions)
        {
            fileVersion.resolved = YES;
        }
        [NSFileVersion removeOtherVersionsOfItemAtURL:documentURL error:nil];
    }
}


#pragma mark - iCloud

@synthesize query = _query;

-(BOOL)hasICloud
{
    BOOL hasiCloud = ([self ubiquitousDocumentsURL] != nil);
    return hasiCloud;
}

@synthesize useiCloud = _useiCloud;
-(void)setUseiCloud:(BOOL)value
{
    if (_useiCloud != value)
    {
        _useiCloud = value;
        [[NSUserDefaults standardUserDefaults] setBool:_useiCloud forKey:kUseiCloud];
        
        dispatch_queue_t defaultQ;
        defaultQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        NSURL *baseURL = (_useiCloud ? [self ubiquitousDocumentsURL] : [self localDocumentsURL]);
        for (NSURL *url in [self.documentURLs copy])
        {
            dispatch_async(defaultQ, ^{
                NSURL *destinationURL = [baseURL URLByAppendingPathComponent:[url lastPathComponent]];
                NSFileManager *fileManager = [[NSFileManager alloc] init];
                NSError *error = nil;
                BOOL success = [fileManager setUbiquitous:_useiCloud
                                                itemAtURL:url
                                           destinationURL:destinationURL
                                                    error:&error];
                if (!success)
                {
                    NSLog (@"%@ unable to save from %@ to %@", self, url, destinationURL);
                }
                else
                {
                    NSLog (@"%@ successful save from %@ to %@", self, url, destinationURL);
                    [self addDocumentURL:destinationURL];
                }
            });
        }
        [self clearOutAndRefreshDocumentURLs];
    }
}

-(void)updateUbiquitousDocuments:(NSNotification *)notification
{
    [self clearOutAndRefreshDocumentURLs];
    [[NSNotificationCenter defaultCenter] postNotificationName:kDocumentListChangedExternallyNotification object:self];
}

#pragma mark - Documents

-(NSURL *)localDocumentsURL
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory 
                                                   inDomains:NSUserDomainMask] lastObject];
}

-(NSURL *)ubiquitousDocumentsURL
{
    // Use your own container identifier below
    return [[[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:
                                            @"CP93YT58M6.com.soundsbroken.Notery"] 
                                            URLByAppendingPathComponent:@"Documents"];
}

-(void)clearOutAndRefreshDocumentURLs
{
    _documentURLs = nil;
    [self documentURLs];
}

-(NSArray *)documentURLs
{
    if (!_documentURLs)
    {
        _documentURLs = [[NSMutableArray alloc] init];
        
        if (self.useiCloud)
        {
            for (NSMetadataItem *item in self.query.results)
            {
                NSURL *url = [item valueForAttribute:NSMetadataItemURLKey];
                [_documentURLs addObject:url];
            }
        }
        else
        {
            NSMutableArray *dirContents = [[[NSFileManager defaultManager] contentsOfDirectoryAtURL:[self localDocumentsURL] includingPropertiesForKeys:nil options:0 error:NULL] mutableCopy];
            NSPredicate *filenameFilter = [NSPredicate predicateWithFormat:@"%K ENDSWITH[c] %@", @"path", kDocumentExtension];
            [dirContents filterUsingPredicate:filenameFilter];
            [_documentURLs addObjectsFromArray:dirContents];
        }
        [_documentURLs sortUsingComparator: ^NSComparisonResult (NSURL * url1, NSURL * url2)
         {
             return [[url1 lastPathComponent] localizedStandardCompare:[url2 lastPathComponent]];
         }];
    }
    return [NSArray arrayWithArray:_documentURLs];
}


-(void)addDocumentURL:(NSURL *)url
{
    [_documentURLs addObject:url];
    [_documentURLs sortUsingComparator: ^NSComparisonResult (NSURL * url1, NSURL * url2)
     {
         return [[url1 lastPathComponent] localizedStandardCompare:[url2 lastPathComponent]];
     }];
    
    NSUInteger row = [self.documentURLs indexOfObject:url];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    NSDictionary *uInfo = [NSDictionary dictionaryWithObject:indexPath 
                                                      forKey:kDocumentListAddedIndexPathKey];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     kDocumentListChangedAdditionNotification
                                                        object:self
                                                      userInfo:uInfo];
}

-(void)removeDocumentURL:(NSURL *)url
{
    NSUInteger row = [self.documentURLs indexOfObject:url];
    
    [_documentURLs removeObject:url];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSFileCoordinator *coordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
        [coordinator coordinateWritingItemAtURL:url 
                                         options:NSFileCoordinatorWritingForDeleting 
                                          error:NULL 
                                     byAccessor: ^(NSURL * newURL)
         {
             [[NSFileManager defaultManager] removeItemAtURL:newURL error:NULL];
         }];
    });
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    NSDictionary *uInfo = [NSDictionary dictionaryWithObject:indexPath 
                                                      forKey:kDocumentListRemovedIndexPathKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:
                                                  kDocumentListChangedRemovalNotification
                                                        object:self
                                                      userInfo:uInfo];
}


-(NSURL *)addDocumentWithName:(NSString *)name
{
    NSString *extension = kDocumentExtension;
    NSURL *baseURL = (self.useiCloud ? [self ubiquitousDocumentsURL] : [self localDocumentsURL]);
    NSURL *url = [[baseURL URLByAppendingPathComponent:name] 
                  URLByAppendingPathExtension:extension];
    if ([self.documentURLs containsObject:url])
    {
        NSUInteger n = 2;
        do
        {
            url = [[baseURL URLByAppendingPathComponent:[name 
                                                stringByAppendingFormat:@" %ld", n]] 
                                                URLByAppendingPathExtension:extension];
            n++;
        } while ([self.documentURLs containsObject:url]);
    }
    Document *document = [[Document alloc] initWithFileURL:url];
    [document saveToURL:url forSaveOperation:UIDocumentSaveForCreating 
                                                    completionHandler: ^(BOOL success)
     {
         if (!success)
         {
             [self removeDocumentURL:url];
         }
     }];
    [self addDocumentURL:url];
    return url;
}

-(void)dealloc;
{
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:NSMetadataQueryDidUpdateNotification 
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:
                                            NSMetadataQueryDidFinishGatheringNotification 
                                                  object:nil];
}

@end
