//
//  Constants.swift
//  SurelyYouGist
//
//  Created by Mark Dalrymple on 2/8/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import Foundation

public let SygistDidReceiveURLNotification = "ApplicationDidReceiveURLNotification"
public let SygistOpenURLInfoKey = "ApplicationOpenURLInfoKey"

public let SharedDefaultsSuiteName = "group.com.bignerdranch.SurelyYouGist"

