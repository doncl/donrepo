//
//  WebError.swift
//  SurelyYouGist
//
//  Created by Michael Ward on 10/6/15.
//  Copyright © 2015 Big Nerd Ranch. All rights reserved.
//

import Foundation

public enum GithubError: ErrorType {
    case JSONSerializationError(String)
    case JSONContentError(String)
    case ConnectionError(String)
    case InvalidDataError(String)
    case UsernameError
    case UnknownHTTPError(Int)
    case AuthenticationError
}
