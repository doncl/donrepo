//
//  AppDelegate.swift
//  SurelyYouGist
//
//  Created by Michael Ward on 7/22/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GithubClientConsumer {
    
    class ConcreteUrlOpener : GithubURLOpener {
        func openURL(url: NSURL) {
            UIApplication.sharedApplication().openURL(url)
        }
    }
                            
    var window: UIWindow?
    var githubClient: GithubClient? = GithubClient(urlOpener: ConcreteUrlOpener())

    func application(application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool
    {
        let navController = self.window!.rootViewController as! UINavigationController
        
        if let clientConsumer = navController.topViewController as? GithubClientConsumer {
            clientConsumer.githubClient = githubClient
        }
        
        return true
    }
    
    func application(app: UIApplication,
             openURL url: NSURL,
                 options: [String : AnyObject]) -> Bool
    {
        // pack up the posted URL into an notification
        let userInfo = [SygistOpenURLInfoKey:url]
        let nc = NSNotificationCenter.defaultCenter()
        nc.postNotificationName(SygistDidReceiveURLNotification,
            object: self, userInfo: userInfo)
        
        return true
    }
    
    
}

