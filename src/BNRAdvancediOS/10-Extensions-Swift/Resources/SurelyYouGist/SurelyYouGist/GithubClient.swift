//
//  GithubClient.swift
//  SurelyYouGist
//
//  Created by Michael Ward on 7/22/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

import UIKit

let GithubUserNameDefaultsKey = "GithubUserNameDefaultsKey"
let GithubAuthTokenDefaultsKey = "GithubAuthTokenDefaultsKey"

public typealias TaskID = NSUUID


public class GithubClient: NSObject {
    
    // MARK: - Vars and Lets
    
    var urlOpener: GithubURLOpener?
    
    // ClientID and ClientSecret and callback URL associated with our registered GitHub app
    public let clientID = "9929be0a6050eebaf4d6"
    private let clientSecret = "e3d2a0f0bb533b05d68542df05b1caefae19e701"
    private let clientRedirectURLString = "sygist://oauth/callback/"
    
    // From documentation at https://developer.github.com/v3/oauth/
    private struct GithubURLs {
        static let authorizeURL = NSURL(string: "https://github.com/login/oauth/authorize")!
        static let tokenURL = NSURL(string: "https://github.com/login/oauth/access_token")!
        static let apiBaseURL = NSURL(string: "https://api.github.com/")!
    }
    
    // Our own stuff
    private var urlSession: NSURLSession = NSURLSession.sharedSession()
    private var inflightTasks: [NSUUID : NSURLSessionTask] = [:]
    private var currentStateString: String = NSUUID().UUIDString
    
    // Github Credentials
    public var userID: String? {
        get {
            let defaults = NSUserDefaults.standardUserDefaults()
            var id = defaults.objectForKey(GithubUserNameDefaultsKey) as? String
            
            if id == nil {
                let defaultsSuite = NSUserDefaults.init(suiteName: SharedDefaultsSuiteName)
                id = defaultsSuite?.objectForKey(GithubUserNameDefaultsKey) as? String
            }
            return id
        }
        set {
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(newValue, forKey: GithubUserNameDefaultsKey)
            defaults.synchronize()
            
            let defaultsSuite = NSUserDefaults.init(suiteName: SharedDefaultsSuiteName)
            defaultsSuite?.setObject(newValue, forKey: GithubUserNameDefaultsKey)
            defaultsSuite?.synchronize()
        }
    }
    private var accessToken: String? {
        get {
            return GithubKeychain.token()
        }
        set {
            GithubKeychain.storeToken(newValue)
            configureSession()
        }
    }
    
    public init(urlOpener: GithubURLOpener? = nil) {
        self.urlOpener =  urlOpener
        super.init()
        configureSession()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    // MARK: - Public Requests

    public func fetchGistsWithCompletion(completion: (GithubResult<[GithubGist]>) -> Void ) -> TaskID? {
        
        guard let username = self.userID else {
            completion(.Failure(GithubError.UsernameError))
            return nil
        }
        
        let url = GithubURLs.apiBaseURL.URLByAppendingPathComponent("users/\(username)/gists")
        let request = NSURLRequest(URL: url)
        return fetchDataWithRequest(request) { result in
            switch result {
            case .Success(let data):
                do {
                    let gists = try GithubImporter.gistsFromData(data)
                    completion(.Success(gists))
                } catch (let parseError) {
                    completion(.Failure(parseError as! GithubError))
                }
            case .Failure(let fetchError):
                completion(.Failure(fetchError))
            }
        }
    }
    
    func fetchUsernameWithCompletion(completion: (GithubResult<String>) -> Void ) -> TaskID? {
        let url = GithubURLs.apiBaseURL.URLByAppendingPathComponent("user")
        let request = NSURLRequest(URL: url)
        return fetchDataWithRequest(request) { result in
            switch result {
            case .Success(let data):
                do {
                    let userID = try GithubImporter.userIDFromData(data)
                    completion(.Success(userID))
                } catch (let parseError) {
                    completion(.Failure(parseError as! GithubError))
                }
            case .Failure(let fetchError):
                completion(.Failure(fetchError))
            }
        }
    }
    
    public func fetchStringAtURL(url: NSURL, withCompletion completion: (GithubResult<String>) -> Void ) -> TaskID? {
        let request = NSURLRequest(URL: url)
        return fetchDataWithRequest(request) { result in
            switch result {
            case .Success(let data):
                do {
                    let string = try GithubImporter.stringFromData(data)
                    completion(.Success(string))
                } catch (let parseError) {
                    completion(.Failure(parseError as! GithubError))
                }
            case .Failure(let fetchError):
                completion(.Failure(fetchError))
            }
        }
    }
    
    public func postGist(gist: String, description: String, isPublic: Bool, withCompletion completion: (GithubResult<String>) -> Void) -> Void {
        
        let newGist = [ 
            "description": description,
            "public": NSNumber(bool: isPublic),
            "files": [ "gist.txt" : [ "content" : gist ] ]
        ]
        
        let newGistJSON = try! NSJSONSerialization.dataWithJSONObject(newGist, 
            options: .PrettyPrinted)
        
        let url = GithubURLs.apiBaseURL.URLByAppendingPathComponent("gists")
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.HTTPBody = newGistJSON
        
        self.fetchDataWithRequest(request) { result in
            switch result {
            case .Success(let data):
                do {
                    let string = try GithubImporter.stringFromData(data)
                    completion(.Success(string))
                } catch (let parseError) {
                    completion(.Failure(parseError as! GithubError))
                }
            case .Failure(let fetchError):
                completion(.Failure(fetchError))
            }
        }
    }

    // MARK: - Funnel fetcher (all the other fetchers route through here)
    
    private func fetchDataWithRequest(request: NSURLRequest,
                    withCompletion completion: (GithubResult<NSData>) -> Void ) -> TaskID
    {
        let taskID = TaskID()

        let task = urlSession.dataTaskWithRequest(request) {
            (data: NSData?, response: NSURLResponse?, error: NSError?) in
            
            defer { self.removeTaskWithID(taskID) }
            
            guard let data = data, response = response as? NSHTTPURLResponse else {
                completion(.Failure(GithubError.ConnectionError(error!.localizedDescription)))
                return
            }
            
            switch response.statusCode {
            case 200, // OK
                 201: // Created
                completion(.Success(data))
            case 401: // unauthorized
                completion(.Failure(GithubError.AuthenticationError))
            default:
                completion(.Failure(GithubError.UnknownHTTPError(response.statusCode)))
            }
            
            print("Received status code \(response.statusCode) and \(data.length) bytes of data from \(request.URL)")
            
        }
        
        inflightTasks[taskID] = task
        task.resume()
        return taskID
    }
    
    // MARK: - Task and Session Management
    
    func removeTaskWithID(id: TaskID) {
        if let task = inflightTasks[id] {
            task.cancel()
        }
        inflightTasks[id] = nil
    }
    
    private func configureSession() {
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        config.HTTPAdditionalHeaders = [
            "Accept" : "application/json"
        ]
        if let token = accessToken {
            config.HTTPAdditionalHeaders!["Authorization"] = "Bearer \(token)"
        }
        urlSession = NSURLSession(configuration: config)
    }
    
    // MARK: - OAuth
    
    // OAuth step 1: we request an "authorization grant"
    // code via the browser
    public func beginAuthorizationByFetchingGrant() {
        currentStateString = NSUUID().UUIDString
        
        // Prepare the URL
        let urlComponents = NSURLComponents(URL: GithubURLs.authorizeURL,
                        resolvingAgainstBaseURL: true)!
        
        let queryItems = [
            NSURLQueryItem(name: "client_id", value: clientID),
            NSURLQueryItem(name: "redirect_uri", value: clientRedirectURLString),
            NSURLQueryItem(name: "state", value: currentStateString),
            NSURLQueryItem(name: "scope", value: "gist")
        ]
        
        urlComponents.queryItems = queryItems

        // Open the URL in MobileSafari
        if let url = urlComponents.URL {
            urlOpener?.openURL(url)
//            UIApplication.sharedApplication().openURL(url)
        }
        
        // Register for open URL notifications
        NSNotificationCenter.defaultCenter().addObserver(self,
                selector: "observeSygistDidOpenURLNotification:",
                name: SygistDidReceiveURLNotification,
                object: nil)
    }
    
    // OAuth step 2: user accepts, which calls back to our app with the request code
    // Note that we're posting this notification from -application:openURL:::
    func observeSygistDidOpenURLNotification(note: NSNotification) {
        
        NSNotificationCenter.defaultCenter().removeObserver(self,
                name: SygistDidReceiveURLNotification,
                object: nil)
        
        guard let url = note.userInfo?[SygistOpenURLInfoKey] as? NSURL,
            queryItems = NSURLComponents(URL: url, resolvingAgainstBaseURL: true)?.queryItems
        where url.host == "oauth" && url.path == "/callback" else {
            print("SygistDidReceiveURLNotification had invalid or non-URL SygistOpenURLInfoKey")
            return
        }

        for queryItem in queryItems {
            if queryItem.name == "code" {
                let grantCode = queryItem.value!
                fetchTokenUsingGrant(grantCode)
            }
        }
    }
    
    // OAuth step 3: we post the request code back to Github,
    // requsting an actual OAuth token for future use
    func fetchTokenUsingGrant(grantCode:String) {

        let bodyDict = [
            "client_id"    : clientID,
            "client_secret": clientSecret,
            "code"         : grantCode,
            "redirect_uri" : clientRedirectURLString
        ]
        
        let url = GithubURLs.tokenURL
        let request = NSMutableURLRequest(URL: url)
        let bodyData = try! NSJSONSerialization.dataWithJSONObject(bodyDict, options: [])
        request.HTTPMethod = "POST"
        request.HTTPBody = bodyData
        request.setValue(nil, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        fetchDataWithRequest(request) { result in
            switch result {
            case .Success(let data):
                do {
                    let token = try GithubImporter.tokenFromData(data)
                    self.accessToken = token
                    print("Fetched access token \(token)")
                } catch (let parseError) {
                    print("Error: Can't parse token data: \(parseError)")
                }
            case .Failure(let error):
                print("Error: No token data: \(error)")
            }
        }
    }
}
