//
//  ViewController.swift
//  NerdCam
//
//  Created by Jeremy on 2014-09-23.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

import AVFoundation
import UIKit

class ViewController: UIViewController {

    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var featureOutlineLayer: CAShapeLayer!

    lazy var synthesizer = AVSpeechSynthesizer()

    func newVideoCaptureSession() -> AVCaptureSession? {
        // Set up a capture input for the default video camera
        let videoCamera = AVCaptureDevice.defaultDeviceWithMediaType(
            AVMediaTypeVideo)
        var inputError: NSError?
        let videoInput: AVCaptureDeviceInput?
        do {
            videoInput = try AVCaptureDeviceInput(
                        device: videoCamera)
        } catch let error as NSError {
            inputError = error
            videoInput = nil
        }
        if (videoInput == nil) {
            print("\(__FUNCTION__): failed to init capture device " +
                "with video camera \(videoCamera): error \(inputError)")
            return nil
        }

        // Attach the input to a capture session
        let captureSession = AVCaptureSession()
        if (!captureSession.canAddInput(videoInput)) {
            print("\(__FUNCTION__): cannot add input \(videoCamera)")
            return nil
        }
        captureSession.addInput(videoInput)
        return captureSession
    }

    func addPreviewLayerForSession(session: AVCaptureSession) {
        let layer = AVCaptureVideoPreviewLayer(session: session)
        let rootLayer = self.view.layer
        layer.frame = rootLayer.bounds
        rootLayer.addSublayer(layer)

        previewLayer = layer
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Crash if we can't get a capture session
        captureSession = newVideoCaptureSession()
        addPreviewLayerForSession(captureSession)
        addMetadataOutputToSession(captureSession)
        addFeatureOutlineLayer()
    }

    func addFeatureOutlineLayer() {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.greenColor().CGColor
        shapeLayer.fillColor = nil

        let frame = view.bounds
        shapeLayer.frame = frame

        let path = UIBezierPath(rect: frame.insetBy(dx: 5, dy: 5))
        shapeLayer.path = path.CGPath

        view.layer.addSublayer(shapeLayer)
        featureOutlineLayer = shapeLayer
    }

    func addMetadataOutputToSession(session: AVCaptureSession) {
        let metadataOutput = AVCaptureMetadataOutput()
        if (!session.canAddOutput(metadataOutput)) {
            print("\(__FUNCTION__): cannot add output \(metadataOutput)")
            return
        }

        session.addOutput(metadataOutput)

        let queue = dispatch_queue_create(
            "com.bignerdranch.advios.NerdCam.MetadataOutput",
            DISPATCH_QUEUE_SERIAL)
        metadataOutput.setMetadataObjectsDelegate(self, queue: queue)
        metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        captureSession.startRunning()
    }

    override func viewDidDisappear(animated: Bool) {
        captureSession.stopRunning()
        super.viewDidDisappear(animated)
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        previewLayer.frame = CGRect(origin: CGPointZero, size: size)
        let currentOrientation = UIDevice.currentDevice().orientation
        let videoOrientation = AVCaptureVideoOrientation(rawValue: 
            currentOrientation.rawValue)!
        previewLayer.connection.videoOrientation = videoOrientation
    }
}

extension ViewController : AVCaptureMetadataOutputObjectsDelegate {
    func captureOutput(captureOutput: AVCaptureOutput!,
        didOutputMetadataObjects metadataObjects: [AnyObject]!,
        fromConnection connection: AVCaptureConnection!) {
            let firstObject = metadataObjects.first as? AVMetadataObject
            let rect = firstObject.map {
                self.previewLayer.rectForMetadataOutputRectOfInterest(
                    $0.bounds)
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.outlineRect(rect)
            }

            if synthesizer.speaking {
                return
            }
            if let qrCode = firstObject
                    as? AVMetadataMachineReadableCodeObject {
                let utterance = AVSpeechUtterance(string: qrCode.stringValue)
                let enVoices = (AVSpeechSynthesisVoice.speechVoices()
                    ).filter { $0.language.hasPrefix("en") }
                utterance.voice = enVoices.first
                print(utterance)
                synthesizer.speakUtterance(utterance)
            }
    }

    func outlineRect(rect: CGRect?) {
        let path: CGPath? = rect.map {
            rect in
            let outline = UIBezierPath(rect: rect)
            outline.lineWidth = 5.0
            return outline.CGPath
        }
        featureOutlineLayer.path = path
    }
}
