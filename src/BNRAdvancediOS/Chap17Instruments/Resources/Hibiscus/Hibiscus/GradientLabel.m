//
//  GradientLabel.m
//  Hibiscus
//
//  Created by Michael Ward on 10/6/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

#import "GradientLabel.h"
@import UIKit;

@interface GradientLabel ()

@property (strong, nonatomic) UIColor *topColor;
@property (strong, nonatomic) UIColor *bottomColor;

@end

@implementation GradientLabel

- (void)setTopColor:(UIColor *)topColor
{
    _topColor = topColor;
    [self configureGradient];
}

- (void)setBottomColor:(UIColor *)bottomColor
{
    _bottomColor = bottomColor;
    [self configureGradient];
}

- (void)configureGradient
{
    CGSize gradientSize = CGSizeMake(1.0, self.frame.size.height);
    UIGraphicsBeginImageContext(gradientSize);
    CGContextRef imageContext = UIGraphicsGetCurrentContext();
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    NSArray *colors = @[(__bridge id)self.topColor.CGColor, (__bridge id)self.bottomColor.CGColor];
    CGGradientRef colorGradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)colors, nil);
    CGPoint startPoint = CGPointZero;
    CGPoint endPoint = CGPointMake(0.0, self.frame.size.height);
    CGContextDrawLinearGradient(imageContext, colorGradient, startPoint, endPoint, 0);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIColor *imageColor = [UIColor colorWithPatternImage:image];
    self.textColor = imageColor;
}

@end
