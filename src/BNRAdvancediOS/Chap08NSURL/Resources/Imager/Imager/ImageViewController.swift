//
//  ImageViewController.swift
//  Imager
//
//  Created by Michael Ward on 7/22/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var imageView: UIImageView!
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        fetchImage(textField.text ?? "")
        return true
    }
    
    func fetchImage(urlString: String) {
        guard let url = NSURL(string: urlString) else {
            alertFailure("That doesn't seem to be a valid image URL.")
            return
        }
        
        let request = NSURLRequest(URL: url)

        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request,
            completionHandler: {(data: NSData?, response: NSURLResponse?, error: NSError?) in
                guard let data = data, _ = response as? NSHTTPURLResponse else {
                    dispatch_async(dispatch_get_main_queue(), {
                      self.alertFailure("I asked for the image but the server hung up.")
                    })
                    return
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    let image = UIImage(data: data)
                    self.imageView.image = image
                })
        })
        
        task.resume()
    }
    
    func alertFailure(message: String) {
        let alertController = UIAlertController(
            title: "Download Failed",
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert )
        let dismissAction = UIAlertAction(title: "Oh Well", style: UIAlertActionStyle.Default) {
            (action:UIAlertAction)->() in
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(dismissAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
}

