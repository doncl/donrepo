//
//  ShareViewController.swift
//  Gistizer
//
//  Created by Don Clore on 2/28/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit
import Social
import GithubAccess

class ShareViewController: SLComposeServiceViewController {
    var isPublic: Bool = true
    var publicPrivateConfigurationItem : SLComposeSheetConfigurationItem!
    
    override func viewDidLoad() {
        #if false
        let token = GithubKeychain.token()
        print("Did we get a token? \(token == nil ? "no" : "yes")")
        let defs = NSUserDefaults.init(suiteName: SharedDefaultsSuiteName)
        let username = defs?.stringForKey(GithubUserNameDefaultsKey)
        
        print("Got username: \(username)")
        #else
            loadSafariSelection()
        #endif
    }
    
    func pushPublicPrivateGistChooser() {
        let storyboard = UIStoryboard(name: "MainInterface", bundle: nil)
        let publicPrivateTVC = storyboard.instantiateViewControllerWithIdentifier("PublicPrivateGist")
            as! PublicPrivateGistChooserTableViewController
        publicPrivateTVC.completion = {isPublic in
            self.isPublic = isPublic
            self.publicPrivateConfigurationItem.value = isPublic ? "Public" : "Private"
        }
        pushConfigurationViewController(publicPrivateTVC)
    }

    override func isContentValid() -> Bool {
        // Do validation of contentText and/or NSExtensionContext attachments here
        return true
    }

    override func didSelectPost() {
        // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
        let plainString = self.textView.text
        
        let githubClient = GithubClient()
        githubClient.postGist(plainString, description: "hello, extension!", isPublic: isPublic) {
            _ in
            self.extensionContext?.completeRequestReturningItems([], completionHandler: nil)
        }
        
    
        // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.

    }
    
    private func loadSafariSelection() {
        guard let context = self.extensionContext,
        let items = context.inputItems as? [NSExtensionItem],
        let item = items.first,
        let attachments = item.attachments as? [NSItemProvider],
        let provider = attachments.first else {
            return
        }
        
        if provider.hasItemConformingToTypeIdentifier("public.plain-text") {
            provider.loadItemForTypeIdentifier("public.plain-text", options: [:]) { item, error in
                if let selectedText = item as? String {
                    print("got selected text \(selectedText)")
                    NSOperationQueue.mainQueue().addOperationWithBlock() {
                        self.textView.text = selectedText
                    }
                }
            }
        }
    }

    override func configurationItems() -> [AnyObject]! {
        let githubClient = GithubClient()

        let accountNameItem = SLComposeSheetConfigurationItem()
        accountNameItem.title = "Account"
        accountNameItem.value = githubClient.userID

        publicPrivateConfigurationItem = SLComposeSheetConfigurationItem()
        publicPrivateConfigurationItem.title = "Access"
        publicPrivateConfigurationItem.value = isPublic ? "Public" : "Private"

        publicPrivateConfigurationItem.tapHandler = {[unowned self] in
            self.pushPublicPrivateGistChooser()
        }

        return [accountNameItem, publicPrivateConfigurationItem]
    }

}
