//
//  PublicPrivateGistChooserTableViewController.swift
//  SurelyYouGist
//
//  Created by Don Clore on 2/28/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

class PublicPrivateGistChooserTableViewController: UITableViewController {
    var isPublic: Bool = true
    var completion: (isPublic: Bool) -> Void = {(isPublic: Bool) in
    }

    
    let kPrivateIndex = 0
    let kPublicIndex = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Gist Privacy"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAtIndexPath:indexPath)
        
        if isPublic && indexPath.row == kPublicIndex ||
            !isPublic && indexPath.row == kPrivateIndex {
            cell.accessoryType = .Checkmark
        } else {
            cell.accessoryType = .None
        }

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        isPublic = indexPath.row == kPublicIndex
        tableView.reloadData()
        
        completion(isPublic:isPublic)
    }

}
