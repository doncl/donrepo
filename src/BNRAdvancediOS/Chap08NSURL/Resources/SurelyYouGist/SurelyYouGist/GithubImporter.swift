//
//  GithubImporter.swift
//  SurelyYouGist
//
//  Created by Michael Ward on 7/23/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

import UIKit

private typealias JSONDictionary = [String : AnyObject]

class GithubImporter {

    class func gistsFromData(data: NSData) throws -> [GithubGist]  {
        do {
            if let gistDicts = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [JSONDictionary] {
                return try gistDicts.map { try gistFromJSONGistDict($0) }
            } else {
                throw GithubError.JSONContentError("JSON top level object wasn't an array of gist dictionaries as expected")
            }
        } catch (let jsonError as NSError) {
            throw GithubError.JSONSerializationError(jsonError.localizedDescription)
        }
    }
    
    class func userIDFromData(data: NSData) throws -> String {
        do {
            if let userDict = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? JSONDictionary {
                return try userIDFromJSONUserDict(userDict)
            } else {
                throw GithubError.JSONContentError("JSON top level object wasn't an array of gist dictionaries as expected")
            }
        } catch (let jsonError as NSError) {
            throw GithubError.JSONSerializationError(jsonError.localizedDescription)
        }
    }
    
    class func stringFromData(data: NSData) throws -> String {
        if let string = String(data: data, encoding: NSUTF8StringEncoding) {
            return string
        } else {
            throw GithubError.InvalidDataError("Could not decode data as UTF8 string")
        }
    }
    
    private class func tokenFromJSONTokenDict(tokenDict: JSONDictionary) throws -> String {
        guard let token = tokenDict["access_token"] as? String else {
            throw GithubError.JSONContentError("Failed to unpack content of auth token dictionary")
        }
        
        return token
    }
    
    class func tokenFromData(data: NSData) throws -> String {
        do {
            if let tokenDict = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? JSONDictionary {
                return try tokenFromJSONTokenDict(tokenDict)
            } else {
                throw GithubError.JSONContentError("JSON top level object wasn't a token dictionary as expected")
            }
        } catch (let jsonError as NSError) {
            throw GithubError.JSONSerializationError(jsonError.localizedDescription)
        }
    }
}

// MARK: - Private helper methods
extension GithubImporter {
    
    private class func gistFromJSONGistDict(gistDict: JSONDictionary) throws -> GithubGist {
        guard let gistURLString = gistDict["url"] as? String,
            gistURL = NSURL(string: gistURLString),
            userDescription = gistDict["description"] as? String,
            fileDictDict = gistDict["files"] as? [String : JSONDictionary]
            else {
                throw GithubError.JSONContentError("Failed to unpack a gist dictionary")
        }
        
        let newGist = GithubGist(url: gistURL)
        newGist.userDescription = (userDescription == "") ? "(untitled)" : userDescription
        newGist.files = try fileDictDict.values.map { try fileFromJSONFileDict($0) }
        return newGist
    }
    
    private class func fileFromJSONFileDict(fileDict: JSONDictionary) throws -> GithubFile {
        guard let fileURLString = fileDict["raw_url"] as? String,
            fileURL = NSURL(string: fileURLString),
            fileName = fileDict["filename"] as? String,
            fileSize = fileDict["size"] as? Int
            else {
                throw GithubError.JSONContentError("Failed to unpack content of a file dictionary")
        }
        
        let newFile = GithubFile(fileName: fileName, url: fileURL, size: fileSize)
        return newFile
    }
    
    private class func userIDFromJSONUserDict(userDict: JSONDictionary) throws -> String {
        guard let userID = userDict["login"] as? String
            else {
                throw GithubError.JSONContentError("Failed to unpack content of a user dictionary")
        }

        return userID
    }
    
}
