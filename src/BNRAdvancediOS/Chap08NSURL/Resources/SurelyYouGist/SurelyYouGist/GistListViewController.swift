//
//  ViewController.swift
//  SurelyYouGist
//
//  Created by Michael Ward on 7/22/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

import UIKit
import GithubAccess

class GistListViewController: UITableViewController, UITextFieldDelegate, GithubClientConsumer {

    @IBOutlet weak var usernameField: UITextField!

    var gistsToShow = [GithubGist]()
    var githubClient: GithubClient?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameField.delegate = self
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: "refreshControlPulled:", forControlEvents: .ValueChanged)
        
        refreshData()
    }
    
    override func viewWillAppear(animated: Bool) {
        usernameField.text = githubClient?.userID
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if let fileListVC = segue.destinationViewController as? FileListViewController {
            fileListVC.githubClient = githubClient
            let gistIndexPath = self.tableView?.indexPathForSelectedRow
            if let index = gistIndexPath?.row {
                let gist = gistsToShow[index]
                fileListVC.filesToShow = gist.files
            } else {
                print("ERROR: couldn't find gist for fileListVC to show")
            }
        }
    }
    
    // MARK: - Table View Delegate / Data Source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gistsToShow.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("GistCell", forIndexPath: indexPath) 
        let gist = gistsToShow[indexPath.row]
        cell.textLabel?.text = gist.userDescription
        cell.detailTextLabel?.text = "\(gist.files.count) file(s)"
        return cell
    }
    
    // MARK: - Actions

    func refreshControlPulled(sender: UIRefreshControl) {
        refreshData()
    }
    
    // MARK: - Data Fetching
    
    func refreshData() {
        githubClient?.fetchGistsWithCompletion() { result in
            dispatch_async(dispatch_get_main_queue(), {
                self.refreshControl?.endRefreshing()
            })
            switch result {
            case .Success(let gists):
                dispatch_async(dispatch_get_main_queue()) {
                    self.gistsToShow = gists
                    print("fetched \(gists.count) gists.")
                    self.tableView.reloadData()
                }
            case .Failure(let error):
                print("Couldn't fetch gists: \(error)")
            }
        }
    }
    
    // MARK: - Text Field Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        githubClient?.userID = textField.text
        textField.resignFirstResponder()
        refreshControl?.beginRefreshing()
        refreshData()
        return true
    }
    @IBAction func loginButtonPressed(sender: AnyObject) {
        githubClient?.beginAuthorizationByFetchingGrant()
    }
    
}
