//
//  GithubKeychain.swift
//  SurelyYouGist
//
//  Created by Don Clore on 2/25/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit
import Security

let GithubKeychainService = "com.bignerdranch.github.oauth"
//let GithubKeychainService = "com.beerbarrelpoker.github.oauth"

class GithubKeychain: NSObject {
    class func storeToken(token: String?) -> Bool {
        print("Storing token in keychain")
        
        forgetToken()
    
        guard let token = token, tokenData = token.dataUsingEncoding(NSUTF8StringEncoding,
            allowLossyConversion: false) else {
                print("Failed to store token; no/invalid token provided")
                return false
        }
        let creationDate = NSDate()
        let itemQuery: [NSString: AnyObject] = [
            kSecClass : kSecClassGenericPassword,
            kSecAttrService : GithubKeychainService,
            kSecAttrCreationDate : creationDate,
            kSecValueData: tokenData
        ]
        
        let status = SecItemAdd(itemQuery, nil)
        
        switch status {
        case errSecSuccess:
            print("Stored token in keychain")
            return true
        default:
            print("ERROR: Failed to store token, code \(status)")
            return false
        }
    }
    
    class func token() -> String? {
        print("Fetching token from keychain")
        
        let query: [NSString: AnyObject] = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrService: GithubKeychainService,
            kSecReturnData: NSNumber(bool: true)
        ]
        
        var cfTypeResult: AnyObject? = nil // should be an NSData-encoded String
        let status = SecItemCopyMatching(query, &cfTypeResult)
        
        switch status {
        case errSecSuccess:
            if let tokenData = cfTypeResult as? NSData, token = NSString(data:tokenData, encoding: NSUTF8StringEncoding) {
                print("Found token in keychain: \(token)")
                return token as String?
            } else {
                print("Failed to get the data: \(cfTypeResult), status: \(status)")
                return nil
            }
        case errSecItemNotFound:
            print("No token found, returning nil")
            return nil
        default:
            print("Failed to fetch token; keychain error \(status)")
            return nil
        }
    }
    
    class func forgetToken() -> Bool {
        let query: [NSString:NSString] = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrService: GithubKeychainService
        ]
        
        let status = SecItemDelete(query)
        
        switch status {
        case errSecSuccess, errSecItemNotFound:
            print("Token deleted or already gone")
            return true
        default:
            print("Failed to fetch token; keychain error: \(status)")
            return false
        }
        
    }
}





















