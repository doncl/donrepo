//
//  Gist.swift
//  SurelyYouGist
//
//  Created by Michael Ward on 7/22/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

import Foundation

public class GithubGist: NSObject {
    let remoteURL: NSURL
    public var userDescription: String = ""
    public var files: [GithubFile] = []
    
    init(url remoteURL: NSURL) {
        self.remoteURL = remoteURL
    }
}
