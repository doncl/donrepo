
import Foundation

class ScheduleFetcher {
    
    enum FetchCoursesResult {
        case Success([Course])
        case Failure(NSError)
    }
    
    let session: NSURLSession
    
    init(urlSession: NSURLSession? = nil) {
        if let desiredSession = urlSession {
            session = desiredSession
        } else {
            let config = NSURLSessionConfiguration.defaultSessionConfiguration()
            session = NSURLSession(configuration: config)
        }
    }

    func fetchCoursesUsingCompletionHandler(completionHandler: (FetchCoursesResult) -> (Void)) {
        let url = NSURL(string: "https://bookapi.bignerdranch.com/courses.json")!
        let request = NSURLRequest(URL: url)
        let task = session.dataTaskWithRequest(request, completionHandler: {
            (data, response, error) -> Void in
            let result: FetchCoursesResult = self.resultFromData(data, response: response, error: error)
            NSOperationQueue.mainQueue().addOperationWithBlock({
                completionHandler(result)
            })
        })
        task.resume()
    }
    
    func resultFromData(data: NSData) -> FetchCoursesResult {
        do {
            guard let topLevelDict = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String: AnyObject]
            else {
                let jsonError = errorWithCode(1, localizedDescription: "JSON top level object not a dictionary.")
                return .Failure(jsonError)
            }
            
            let courseDicts = topLevelDict["courses"] as! [NSDictionary]
            var courses:[Course] = []
            for courseDict in courseDicts {
                if let course = courseFromDictionary(courseDict) {
                    courses.append(course)
                }
            }
            return .Success(courses)
            
        } catch {
            print("Unable to deserialize JSON: \(error)")
            return .Failure(error as NSError)
        }
    }
    
    func courseFromDictionary(courseDict: NSDictionary) -> Course? {
        let title = courseDict["title"] as! String
        let urlString = courseDict["url"] as! String
        let upcomingArray = courseDict["upcoming"] as! [NSDictionary]
        let nextUpcomingDict = upcomingArray.first!
        let nextStartDateString = nextUpcomingDict["start_date"] as! String
        
        let url = NSURL(string: urlString)!
        
        let df = NSDateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let nextStartDate = df.dateFromString(nextStartDateString)!
        
        return Course(title: title, url: url, nextStartDate: nextStartDate)
    }
    
    func errorWithCode(code: Int, localizedDescription: String) -> NSError {
        return NSError(domain: "ScheduleFetcher", code: code, userInfo: [NSLocalizedDescriptionKey: localizedDescription])
    }
    
    func resultFromData(data: NSData!, response: NSURLResponse!, error: NSError!) -> FetchCoursesResult {
        
        let result: FetchCoursesResult
        
        guard let data = data, response = response as? NSHTTPURLResponse else {
            let myError = self.errorWithCode(1, localizedDescription: "Unexpected response: \(error)")
            result = .Failure(myError)
            return result
        }
        
        print("Received \(data.length) bytes with status code \(response.statusCode).")
        
        if response.statusCode == 200 {
            result = self.resultFromData(data)
        }
        else {
            let statusError = self.errorWithCode(1, localizedDescription: "Bad status code \(response.statusCode)")
            result = .Failure(statusError)
        }
        
        return result
    }
    
}


