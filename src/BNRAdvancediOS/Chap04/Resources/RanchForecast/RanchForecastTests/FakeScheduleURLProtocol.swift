//
//  FakeScheduleURLProtocol.swift
//  RanchForecast
//
//  Created by Don Clore on 2/22/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import Foundation

class FakeScheduleURLProtocol : NSURLProtocol {
    override class func canInitWithRequest(request: NSURLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequestForRequest(request: NSURLRequest) -> NSURLRequest {
        return request
    }
    
    override func startLoading() {
        print("Vending fake URL response.")
        
        client?.URLProtocol(self, didReceiveResponse: Constants.okResponse, cacheStoragePolicy: .NotAllowed)
        
        client?.URLProtocol(self, didLoadData: Constants.jsonData)
        
        client?.URLProtocolDidFinishLoading(self)
    }
    
    override func stopLoading() {
        print("Done vending fake URL response")
    }
}
