//
//  PhotosViewController.swift
//  Photodex
//
//  Created by Michael Ward on 8/25/15.
//  Copyright © 2015 Big Nerd Ranch. All rights reserved.
//

import Foundation
import UIKit
import Photos

let PhotoCellIdentifier = "PhotoCell"

protocol PhotoLayoutType {
    var currentIndex: Int { get }
    var imageSize: CGSize { get }
}

class PhotosViewController: UICollectionViewController {
    
    var photoLayout: PhotoLayoutType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let photoLayout = collectionViewLayout as? PhotoLayoutType else {
            fatalError("PhotosViewController requires a Layout which conforms to PhotoLayoutType")
        }
        
        self.photoLayout = photoLayout
        
        let pinchGR = UIPinchGestureRecognizer(target: self, action: "pinched:")
        collectionView!.addGestureRecognizer(pinchGR)
        
        PHPhotoLibrary.requestAuthorization {
            (status) -> Void in
            switch status {
            case .Authorized:
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in

                    self.cameraAssetsResult = self.fetchCameraAssets()
                    self.updateCachedAssets()
                    
                    self.collectionView!.reloadData()
                })
            default:
                break
            }
        }
    }
    
    // MARK: - Photos
    
    private var cameraAssetsResult: PHFetchResult!
    private var imageRequestIDs: [NSIndexPath:PHImageRequestID] = [:]
    private let imageManager = PHCachingImageManager()
    private var cachedAssets: Set<PHAsset> = []
    
    private let imageFetchingOptions: PHImageRequestOptions = {
        let fetchOptions = PHImageRequestOptions()
        fetchOptions.networkAccessAllowed = false
        return fetchOptions
    }()

    private let networkImageFetchingOptions: PHImageRequestOptions = {
        let fetchOptions = PHImageRequestOptions()
        fetchOptions.networkAccessAllowed = true
        return fetchOptions
    }()
    
    private let placeholderImage: UIImage = {
        let rect = CGRectMake(0, 0, 300, 300)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        UIColor.redColor().setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }()
    
    private func fetchCameraAssets() -> PHFetchResult? {
        
        guard PHPhotoLibrary.authorizationStatus() == .Authorized else {
            return nil
        }

        let result = PHAssetCollection.fetchAssetCollectionsWithType(.SmartAlbum, subtype: .SmartAlbumUserLibrary, options: nil)
        
        let cameraCollection = result.firstObject! as! PHAssetCollection
        let fetchPredicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.Image.rawValue)
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = fetchPredicate
        let assetsResult = PHAsset.fetchAssetsInAssetCollection(cameraCollection, options: fetchOptions)
        
        print("found assets: \(assetsResult)")
        
        return assetsResult
    }
    
    private func updateCachedAssets() {
        
        let distance = 25
        
        let currentIndex = photoLayout.currentIndex
        
        let totalItemCount = collectionView!.numberOfItemsInSection(0)
        guard totalItemCount > 0 else { return }
        
        var range = (currentIndex - distance)...(currentIndex + distance)
        range.startIndex = max(range.startIndex , 0)
        range.endIndex = min(range.endIndex, totalItemCount - 1)

        print("ideal asset range: \(range)")
        
        let idealAssets: [PHAsset] = range.map { index -> PHAsset in
            return cameraAssetsResult.objectAtIndex(index) as! PHAsset
        }
        let idealAssetSet = Set(idealAssets)
        
        let assetsToCache: Set<PHAsset> = idealAssetSet.subtract(cachedAssets)
        imageManager.startCachingImagesForAssets(Array(assetsToCache), targetSize: photoLayout.imageSize, contentMode: .AspectFill, options: imageFetchingOptions)
        
        let assetsToUnCache: Set<PHAsset> = cachedAssets.subtract(idealAssetSet)
        imageManager.stopCachingImagesForAssets(Array(assetsToUnCache), targetSize: photoLayout.imageSize, contentMode: .AspectFill, options: imageFetchingOptions)
        
        cachedAssets = idealAssetSet
        print("Adding \(assetsToCache.count) assets and removing \(assetsToUnCache.count) in cache")
    }
    
    
    // MARK: - Handling Pinches
    
    private var initialScale: CGFloat = 1.0
    private var isTransitioning: Bool = false
    private var transitionLayout: UICollectionViewTransitionLayout?
    
    @objc private func pinched(gr: UIPinchGestureRecognizer) {
        switch gr.state {
        case .Began:
            print("gesture began with scale \(gr.scale) velocity \(gr.velocity)")
            let currentLayout = collectionView!.collectionViewLayout
            let nextLayout = (currentLayout is FlipLayout) ? SpaceFillingFlowLayout() : FlipLayout()
            transitionLayout = collectionView!.startInteractiveTransitionToCollectionViewLayout(nextLayout, completion: { (animationCompleted, transitionCompleted) -> Void in
                print("interactive transition completion executing")
            })
        case .Changed:
            print("gesture changed to \(gr.scale) at \(gr.velocity))")
            let progress: CGFloat
            let startingScale: CGFloat = 1.0
            let currentScale = gr.scale
            let targetScale: CGFloat
            if transitionLayout?.nextLayout is FlipLayout {
                targetScale = 0.25
                progress = (startingScale - currentScale) / (startingScale - targetScale)
            } else {
                targetScale = 4.0
                progress = (currentScale - startingScale) / (targetScale - startingScale)
            }
            transitionLayout!.transitionProgress = progress
        case .Ended:
            print("gesture ended")
            if transitionLayout!.transitionProgress > 0.7 {
                collectionView!.finishInteractiveTransition()
            } else {
                collectionView!.cancelInteractiveTransition()
            }
        default:
            print("gesture state \(gr.state)")
        }
    }
}

// MARK: - Collection View Data Source
extension PhotosViewController {
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let cameraAssetsResult = cameraAssetsResult
        where PHPhotoLibrary.authorizationStatus() == .Authorized else {
            return 0
        }
        return cameraAssetsResult.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(PhotoCellIdentifier, forIndexPath: indexPath) as! PhotoCell
        cell.imageView.image = UIImage(named: "bnr_hat")
        return cell
    }
    
    
    override func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
    
        guard let currentCell = cell as? PhotoCell else {
            print("unexpectedly non-PhotoCell cell")
            return
        }
        

        let imageAsset = cameraAssetsResult.objectAtIndex(indexPath.row) as! PHAsset
        
        let beginningTimestamp = mach_absolute_time()
        let placeholder = self.placeholderImage
        
        let networkRequestHandler: (UIImage?, [NSObject:AnyObject]?) -> Void = {
            [weak self] (maybeImage, maybeInfo) -> Void in
            guard let image = maybeImage
                else {
                    print("Bailing on image fill second time: \(maybeImage), \(maybeInfo)")
                      return
            }
            
            
            func fill() {
                currentCell.imageView.image = image ?? placeholder
                print("Filling cell \(indexPath.row): \(maybeInfo)")
            }
            
            if self?.imageRequestIDs[indexPath] != nil {
                NSOperationQueue.mainQueue().addOperationWithBlock(fill)
            } else {
                fill()
            }
        }
        
        let imageRequestHandler: (UIImage?, [NSObject:AnyObject]?) -> Void = {
            [weak self] (maybeImage, maybeInfo) -> Void in
            
            let endingTimestamp = mach_absolute_time()
            
            var timebase : mach_timebase_info = mach_timebase_info(numer: 0, denom: 0)
            mach_timebase_info(&timebase)
            let scale = CGFloat(timebase.numer) / CGFloat(timebase.denom)
            let elapsedTimeNanos = CGFloat(endingTimestamp - beginningTimestamp) * scale
            let elapsedTime = elapsedTimeNanos / CGFloat(NSEC_PER_SEC)
            
            print("ELAPSED TIME \(elapsedTime)")
            
            let isIcloud = (maybeInfo?[PHImageResultIsInCloudKey] as? NSNumber)?.boolValue ?? false
            
            guard let image = maybeImage
                else {
                    print("Bailing on image fill: \(maybeImage), \(maybeInfo), trying network styles: \(isIcloud)")
                    
                    if isIcloud {
                        
                        self?.imageRequestIDs[indexPath] = self?.imageManager.requestImageForAsset( imageAsset,
                            targetSize: self!.photoLayout.imageSize,
                            contentMode: .AspectFill,
                            options: self?.networkImageFetchingOptions,
                            resultHandler: networkRequestHandler)
                        print("kicking orff request \(self?.imageRequestIDs[indexPath])")
                    }
                    
                    return
            }
            
            func fill() {
                currentCell.imageView.image = image ?? placeholder
                // print("Filling cell \(indexPath.row): \(maybeInfo)")
            }
            
            if self?.imageRequestIDs[indexPath] != nil {
                NSOperationQueue.mainQueue().addOperationWithBlock(fill)
            } else {
                fill()
            }

        }

        imageRequestIDs[indexPath] = imageManager.requestImageForAsset( imageAsset,
                                                                    targetSize: photoLayout.imageSize,
                                                                    contentMode: .AspectFill,
                                                                    options: imageFetchingOptions,
                                                                    resultHandler: imageRequestHandler)
        
    }
    
    override func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        guard let oldRequestID = imageRequestIDs.removeValueForKey(indexPath) else { return }
        PHImageManager.defaultManager().cancelImageRequest(oldRequestID)
    }
    
    override func collectionView(collectionView: UICollectionView,
        transitionLayoutForOldLayout fromLayout: UICollectionViewLayout,
        newLayout toLayout: UICollectionViewLayout) -> UICollectionViewTransitionLayout {
            
            return UICollectionViewTransitionLayout(currentLayout: fromLayout, nextLayout: toLayout)
            
    }
    
}

// MARK: - Scroll View Delegate
extension PhotosViewController {
    
    override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        updateCachedAssets()
    }
    
}
