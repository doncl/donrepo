//
//  PhotoCell.swift
//  Photodex
//
//  Created by Don Clore on 2/23/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {

    override func applyLayoutAttributes(layoutAttributes: UICollectionViewLayoutAttributes) {
        defer {
            super.applyLayoutAttributes(layoutAttributes)
        }
        
        guard let anchorableAttributes = layoutAttributes as? AnchorableAttributes else {
            layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            return
        }
        
        layer.anchorPoint = anchorableAttributes.anchorPoint
    }
}
