#import <Foundation/Foundation.h>

@interface BNRNotificationSpy : NSObject

+ (void) startSpying;
+ (void) stopSpying;

@end

