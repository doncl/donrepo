//
//  ViewController.swift
//  FaceFuzzer
//
//  Created by Jeremy on 2014-09-15.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var selectorView: UIView!
    @IBAction func toggleImageSelectorAction() {
        print("TODO: Toggle image selector here")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
