//
//  Copyright © 2015 Big Nerd Ranch
//

import UIKit

class PhotoInfoViewController: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    
    var photo: Photo! {
        didSet {
            navigationItem.title = photo.title
        }
    }
    var store: PhotoStore!
    var imageProcessor: ImageProcessor!
    var activeFilter: ImageFilter!
    var request: ImageProcessingRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        store.fetchImageForPhoto(photo) { result in
            switch result {
            case let .Success(image):
                let fuzzAction = ImageAction.PixellateFaces
                let filterAction = ImageAction.Filter(self.activeFilter)
                let actions = [fuzzAction, filterAction]
                
                self.request = self.imageProcessor.processImage(image, actions: actions, priority: .High) {result in
                    let filteredImage: UIImage
                    
                    switch result {
                    case let .Success(resultImage):
                        filteredImage = resultImage
                    case let .Failure(error):
                        print("Error: failed to process photo \(self.photo.photoKey): \(error)")
                        filteredImage = image
                        // TODO: Present an alert to the user, maybe?
                    }
                    
                    NSOperationQueue.mainQueue().addOperationWithBlock {
                        self.imageView.image = filteredImage
                    }
                }
                
            case let .Failure(error):
                print("Error fetching image for photo: \(error)")
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowTags" {
            let navController = segue.destinationViewController as! UINavigationController
            let tagController = navController.topViewController as! TagsViewController
            
            tagController.store = store
            tagController.photo = photo
        }
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        request?.cancel()
    }

}
