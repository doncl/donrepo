//
//  ImageProcessor.swift
//  Photorama
//
//  Created by Don Clore on 2/23/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

enum ImageAction {
    case Scale(maxSize: CGSize)
    case PixellateFaces
    case Filter(ImageFilter)
}

enum ImageFilter {
    case None
    case Gloom(intensity: Double, radius: Double)
    case Sepia(intensity: Double)
    case Blur(radius: Double)
}

enum ImageProcessingError: ErrorType {
    case BadInputImage(UIImage)
    case BadFilterConfig(name: String, params: [String: AnyObject]?)
    case Canceled
}

enum ImageProcessingPriority {
    case High
    case Normal
}

enum ImageProcessingResult {
    case Success(UIImage)
    case Failure(ErrorType)
}

typealias ImageProcessingHandler = (ImageProcessingResult) -> Void

class ImageProcessor {
    private let processingQueue = NSOperationQueue()
    
    init() {
        processingQueue.maxConcurrentOperationCount = 10
    }
    
    func processImage(image: UIImage, actions: [ImageAction], priority: ImageProcessingPriority, completion: ImageProcessingHandler) -> ImageProcessingRequest {
        let operation = ImageProcessingOperation(image: image, actions: actions, priority: priority, completion: completion)

        let request = ImageProcessingRequest(operation: operation, processor: self)
        processingQueue.addOperation(operation)
        return request
    }
}

private class ImageProcessingOperation : NSOperation {
    var image: UIImage?
    let actions: [ImageAction]
    let completion: ImageProcessingHandler
    
    init(image: UIImage, actions: [ImageAction], priority: ImageProcessingPriority = .Normal,
         completion: ImageProcessingHandler) {

        self.image = image
        self.actions = actions
        self.completion = completion
        super.init()

        switch priority {
            case .High:
                qualityOfService = .UserInitiated
                queuePriority = .High
            case .Normal:
                qualityOfService = .Utility
                queuePriority = .Normal
        }
    }

    convenience init(operation: ImageProcessingOperation, priority: ImageProcessingPriority = .Normal) {

        guard let image = operation.image else {
            preconditionFailure("FATAL: Attempt to clone an operation with nil image.")
        }

        self.init(image: image, actions: operation.actions, priority: priority, completion: operation.completion)
    }

    override func cancel() {
        super.cancel()
        if cancelled {
            image = nil
        }
    }
    
    override func main() {

        guard let image = image else {
            completion(.Failure(ImageProcessingError.Canceled))
            return
        }

        do {
            let processedImage = try imageByApplyingActions(actions, toImage: image)
            completion(.Success(processedImage))
        } catch (let error) {
            completion(.Failure(error))
        }

        // now what?
    }
    
    func imageByApplyingActions(actions: [ImageAction], toImage image:UIImage) throws ->UIImage {

        guard cancelled == false else {
            throw ImageProcessingError.Canceled
        }


        // Set up the CIImag and context
        
        guard var workingImage = CIImage(image: image) else {
            let error = ImageProcessingError.BadInputImage(image)
            throw error
        }
        let context = CIContext(options: nil)
        
        //  Apply requested processing
        for action in actions {

            guard cancelled == false else {
                throw ImageProcessingError.Canceled
            }

            switch action {
            case .PixellateFaces:
                workingImage = try self.pixellatedFacesImage(workingImage, context: context)
            case .Scale(let maxSize):
                workingImage = self.scaledImage(workingImage, maxSize: maxSize)
            case .Filter(let filter):
                workingImage = try self.imageByApplyingFilter(filter, toImage: workingImage)
            }
        }
        
        // Extract the resultant UIImage and handle it
        let renderedImage = context.createCGImage(workingImage, fromRect: workingImage.extent)
        
        let resultImage = UIImage(CGImage: renderedImage)

        guard cancelled == false else {
            throw ImageProcessingError.Canceled
        }

        return resultImage
    }
    
    private func imageByApplyingFilter(filter: ImageFilter, toImage image: CIImage) throws -> CIImage {
        let parameters: [String: AnyObject]
        let filterName: String
        
        let shouldCrop: Bool
        
        // Configure the CIFilter() inputs based on the chosen filter
        switch filter {
        case .None:
            return image
        case .Gloom(let intensity, let radius):
            parameters = [kCIInputImageKey: image, kCIInputIntensityKey: intensity, kCIInputRadiusKey: radius]
            filterName = "CIGloom"
            shouldCrop = true
        case .Sepia(let intensity):
            parameters = [kCIInputImageKey: image, kCIInputIntensityKey: intensity]
            filterName = "CISepiaTone"
            shouldCrop = false
        case .Blur(let radius):
            parameters = [kCIInputImageKey: image, kCIInputRadiusKey: radius]
            filterName = "CIGaussianBlur"
            shouldCrop = true
        }
        // Actually create and apply the filter
        guard let filter = CIFilter(name: filterName, withInputParameters: parameters), output = filter.outputImage else {
            throw ImageProcessingError.BadFilterConfig(name: filterName, params: parameters)
        }
        
        if shouldCrop {
            let croppedImage = output.imageByCroppingToRect(image.extent)
            return croppedImage
        }
        return output
    }
    
    private func pixellatedImage(inImage image: CIImage) throws -> CIImage {
        let inputParams = [kCIInputImageKey: image, kCIInputScaleKey: 45.0, kCIInputCenterKey: CIVector(x: 0, y:0)]
        
        guard let filter = CIFilter(name: "CIPixellate", withInputParameters: inputParams), output = filter.outputImage else {
            throw ImageProcessingError.BadFilterConfig(name: "CIPixellate", params: inputParams)
        }
        return output
    }
    
    private func compositedImage(image: CIImage, overImage background: CIImage) throws -> CIImage {
        let inputParams = [kCIInputImageKey: image, kCIInputBackgroundImageKey: background]
        
        guard let filter = CIFilter(name: "CISourceOverCompositing", withInputParameters: inputParams), output = filter.outputImage else {
            throw ImageProcessingError.BadFilterConfig(name: "CISourceOverCompositing", params: inputParams)
        }
        return output
    }
    
    private func faceFeaturesInImage(image: CIImage, context: CIContext) -> [CIFaceFeature] {
        let detectorOptions = [CIDetectorAccuracy : CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: context, options: detectorOptions)
        let features = faceDetector.featuresInImage(image) as! [CIFaceFeature]
        return features
    }
    
    private func scaledImage(image: CIImage, maxSize: CGSize) -> CIImage {
        let aspectRatio = image.extent.width / image.extent.height
        let scale: CGFloat
        
        if aspectRatio > 1.0 {
            scale = maxSize.width / image.extent.width
        } else {
            scale = maxSize.height / image.extent.height
        }
        let scaleTransform = CGAffineTransformMakeScale(scale, scale)
        
        let outputImage = image.imageByApplyingTransform(scaleTransform)
        return outputImage
    }
    
    private func pixellatedFacesImage(image: CIImage, context: CIContext) throws -> CIImage {
        let faceFeatures = faceFeaturesInImage(image, context: context)
        if faceFeatures.isEmpty {
            return image
        }
        
        let resultImage = try faceFeatures.reduce(image) {inputImage, face in
            let faceImage = image.imageByCroppingToRect(face.bounds)
            let pixellatedFaceImage = try pixellatedImage(inImage: faceImage)
            let compositedFaceImage = try compositedImage(pixellatedFaceImage, overImage: inputImage)
            
            return compositedFaceImage
        }
        return resultImage
    }

}

class ImageProcessingRequest {
    private var operation: ImageProcessingOperation
    private let processor: ImageProcessor

    var priority: ImageProcessingPriority = .Normal {
        didSet(oldPriority) {
            if operation.executing {
                return
            }

            operation.cancel()
            operation = ImageProcessingOperation(operation: operation, priority: priority)

            processor.processingQueue.addOperation(operation)
        }
    }

    private init(operation: ImageProcessingOperation, processor: ImageProcessor) {
        self.operation = operation
        self.processor = processor
    }

    func cancel() {
        operation.cancel()
    }
}