//
//  ThumbnailStore.swift
//  Photorama
//
//  Created by Don Clore on 2/23/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

class ThumbnailStore {
    private let thumbnailCache = NSCache()
    
    func thumbnailForKey(key: String) -> UIImage? {
        return thumbnailCache.objectForKey(key) as? UIImage
    }
    
    func setThumbnail(image: UIImage, forKey key: String) {
        thumbnailCache.setObject(image, forKey: key)
    }
    
    func clearThumbnails() {
        thumbnailCache.removeAllObjects()
    }
}
