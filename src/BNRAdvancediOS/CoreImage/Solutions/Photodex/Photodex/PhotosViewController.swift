//
//  PhotosViewController.swift
//  Photodex
//
//  Created by Michael Ward on 8/25/15.
//  Copyright © 2015 Big Nerd Ranch. All rights reserved.
//

import UIKit

let PhotoCellIdentifier = "PhotoCell"

protocol PreSizedCellLayout {
    var cellSize: CGSize { get }
}

class PhotosViewController: UICollectionViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var thumbnail: UIImage? = nil {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    let photoProcessor = PhotoProcessor()
    var filters: [PhotoFilter] = []
    let cellCount = 25
    lazy var thumbnailSize: CGSize = {
        let layout = self.collectionViewLayout as! PreSizedCellLayout
        let size = layout.cellSize
        return size
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pinchGR = UIPinchGestureRecognizer(target: self, action: "pinched:")
        collectionView?.addGestureRecognizer(pinchGR)

        filters = (0..<cellCount).map { i in
            return PhotoFilter.None
        }
        
        collectionView?.reloadData()
    }
    
    // MARK: - Actions
    
    @IBAction func filterChanged(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0: // None
            filters = (0..<cellCount).map { i in
                return PhotoFilter.None
            }
        case 1: // Gloom
            filters = (0..<cellCount).map { i in
                let maxIntensity = 2.5
                let minIntensity = 0.5
                let intensity = (Double(i)/Double(cellCount)) * ((maxIntensity - minIntensity) + minIntensity)
                return PhotoFilter.Gloom(intensity: intensity, radius: 20.0)
            }
        case 2: // Sepia
            filters = (0..<cellCount).map { i in
                let maxIntensity = 7.5
                let minIntensity = 0.0
                let intensity = (Double(i)/Double(cellCount)) * ((maxIntensity - minIntensity) + minIntensity)
                return PhotoFilter.Sepia(intensity: intensity)
            }
        case 3: // Blur
            filters = (0..<cellCount).map { i in
                let maxRadius = 20.0
                let minRadius = 0.0
                let radius = (Double(i)/Double(cellCount)) * ((maxRadius - minRadius) + minRadius)
                return PhotoFilter.Blur(radius: radius)
            }
        default:
            fatalError("Segmented control changed to an index out of bounds.")
        }
        
        collectionView?.reloadData()
    }
    
    @IBAction func cameraButtonPressed(sender: UIBarButtonItem) {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .SavedPhotosAlbum
        imagePicker.delegate = self
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - Image Picker
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        dismissViewControllerAnimated(true, completion: {
            do {
                let pickedImage = info[UIImagePickerControllerOriginalImage]! as! UIImage
                self.thumbnail = try self.photoProcessor.scaledPixellatedImage(pickedImage, maxSize: self.thumbnailSize)
            }
            
            catch {
                print("Error: \(error)")
            }
        })
    }
    
    // MARK: - Handling Pinches
    
    private var initialScale: CGFloat = 1.0
    private var isTransitioning: Bool = false
    private var transitionLayout: UICollectionViewTransitionLayout?
    
    @objc private func pinched(gr: UIPinchGestureRecognizer) {
        switch gr.state {
        case .Began:
            print("gesture began with scale \(gr.scale) velocity \(gr.velocity)")
            let currentLayout = collectionView!.collectionViewLayout
            let nextLayout = (currentLayout is FlipLayout) ? SpaceFillingFlowLayout() : FlipLayout()
            transitionLayout = collectionView!.startInteractiveTransitionToCollectionViewLayout(nextLayout, completion: { (animationCompleted, transitionCompleted) -> Void in
                print("interactive transition completion executing")
            })
        case .Changed:
            print("gesture changed to \(gr.scale) at \(gr.velocity))")
            let progress: CGFloat
            let startingScale: CGFloat = 1.0
            let currentScale = gr.scale
            let targetScale: CGFloat
            if transitionLayout?.nextLayout is FlipLayout {
                targetScale = 0.25
                progress = (startingScale - currentScale) / (startingScale - targetScale)
            } else {
                targetScale = 4.0
                progress = (currentScale - startingScale) / (targetScale - startingScale)
            }
            transitionLayout!.transitionProgress = progress
        case .Ended:
            print("gesture ended")
            if transitionLayout!.transitionProgress > 0.7 {
                collectionView!.finishInteractiveTransition()
            } else {
                collectionView!.cancelInteractiveTransition()
            }
        default:
            print("gesture state \(gr.state)")
        }
    }

}

// MARK: - Collection View Data Source
extension PhotosViewController {
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellCount
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(PhotoCellIdentifier, forIndexPath: indexPath) as! PhotoCell
        
        guard let thumbnail = thumbnail else {
            return cell
        }
        
        do {
            let image = try photoProcessor.processedImage(thumbnail, filter: filters[indexPath.item])
            cell.imageView.image = image
        }
        catch {
            print("Error: \(error)")
        }
        
        return cell

    }
    
    override func collectionView(collectionView: UICollectionView,
        transitionLayoutForOldLayout fromLayout: UICollectionViewLayout,
        newLayout toLayout: UICollectionViewLayout) -> UICollectionViewTransitionLayout {
            
            return UICollectionViewTransitionLayout(currentLayout: fromLayout, nextLayout: toLayout)
            
    }
    
}
