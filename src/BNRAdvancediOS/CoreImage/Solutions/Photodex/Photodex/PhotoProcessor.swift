//
//  PhotoProcessor.swift
//  Photodex
//
//  Created by Michael Ward on 11/9/15.
//  Copyright © 2015 Big Nerd Ranch. All rights reserved.
//

import UIKit
import CoreImage

enum PhotoProcessingError: ErrorType {
    case BadInputImage(String)
    case BadFilter(String)
}

enum PhotoFilter {
    case None
    case Gloom(intensity: Double, radius: Double)
    case Sepia(intensity: Double)
    case Blur(radius: Double)
}

class PhotoProcessor: NSObject {

    func processedImage(image: UIImage, filter: PhotoFilter) throws -> UIImage
    {
        print("Preparing image: filter=\(filter)")
        guard let cgImage = image.CGImage else {
            throw PhotoProcessingError.BadInputImage("Got nil CGImage from UIImage: \(image)")
        }
        
        let context = CIContext(options: nil)
        var workingImage = CIImage(CGImage: cgImage)
        
        workingImage = try filteredImage(workingImage, selectedFilter: filter)

        let renderedImage = context.createCGImage(workingImage, fromRect: workingImage.extent)
        let resultImage = UIImage(CGImage: renderedImage)
        return resultImage
        
    }
    
    private func filteredImage(image: CIImage, selectedFilter: PhotoFilter) throws -> CIImage
    {
        let parameters: [String: AnyObject]
        let filter: CIFilter?
        
        let shouldCrop: Bool
        
        switch selectedFilter {
        case .None:
            return image
        case .Gloom (let intensity, let radius):
            parameters =
                  [ kCIInputImageKey: image,
                    kCIInputIntensityKey: intensity,
                    kCIInputRadiusKey: radius ]
            filter = CIFilter(name: "CIGloom", withInputParameters: parameters)
            shouldCrop = true
        case .Sepia (let intensity):
            parameters =
                  [ kCIInputImageKey: image,
                    kCIInputIntensityKey: intensity ]
            filter = CIFilter(name: "CISepiaTone", withInputParameters: parameters)
            shouldCrop = false
        case .Blur (let radius):
            parameters =
                  [ kCIInputImageKey: image,
                    kCIInputRadiusKey: radius ]
            filter = CIFilter(name: "CIGaussianBlur", withInputParameters: parameters)
            shouldCrop = true
        }
        
        guard let filteredImage = filter?.outputImage else {
            throw PhotoProcessingError.BadFilter("Failed to create working filter \(selectedFilter) with parameters \(parameters)")
        }
        
        if shouldCrop {
            let cropRect = image.extent
            let croppedImage = filteredImage.imageByCroppingToRect(cropRect)
            return croppedImage
        } else {
            return filteredImage
        }

    }
    
    func scaledPixellatedImage(image: UIImage, maxSize: CGSize) throws -> UIImage
    {
        guard let cgImage = image.CGImage else {
            throw PhotoProcessingError.BadInputImage("Got nil CGImage from UIImage: \(image)")
        }
        
        let context = CIContext(options: nil)
        var workingImage = CIImage(CGImage: cgImage)
        
        workingImage = scaledImage(workingImage, maxSize: maxSize)
        workingImage = pixellatedFacesImage(workingImage, context: context)
        
        let renderedImage = context.createCGImage(workingImage, fromRect: workingImage.extent)
        let resultImage = UIImage(CGImage: renderedImage)
        return resultImage
    }
    
    private func scaledImage(image: CIImage, maxSize: CGSize) -> CIImage
    {
        let aspectRatio = image.extent.width / image.extent.height
        let scale: CGFloat
        if aspectRatio > 1.0 {
            scale = maxSize.width / image.extent.width
        } else {
            scale = maxSize.height / image.extent.width
        }
        let scaleTransform = CGAffineTransformMakeScale(scale, scale)
        
        let outputImage = image.imageByApplyingTransform(scaleTransform)
        return outputImage
    }
    
    private func pixellatedFacesImage(image: CIImage, context: CIContext) -> CIImage
    {
        let faceFeatures = faceFeaturesInImage(image, context: context)
        if faceFeatures.isEmpty {
            return image
        }
        
        let resultImage = faceFeatures.reduce(image) { inputImage, face in
            let pixellatedFace = pixellatedSingleFaceImage(inImage: inputImage, fromRect: face.bounds)
            let parameters = [ kCIInputImageKey: pixellatedFace,
                               kCIInputBackgroundImageKey: inputImage ]
            let overFilter = CIFilter(name: "CISourceOverCompositing", withInputParameters: parameters)
            let imageWithPixellatedFace = overFilter!.outputImage!
            let renderedImage = context.createCGImage(imageWithPixellatedFace, fromRect: imageWithPixellatedFace.extent)
            let nextImage = CIImage(CGImage: renderedImage)
            return nextImage
        }
        
        return resultImage
    }
    
    private func pixellatedSingleFaceImage(inImage image: CIImage, fromRect rect: CGRect) -> CIImage
    {
        let imageInBounds = image.imageByCroppingToRect(rect)
        let pixellateOptions =
            [ kCIInputImageKey: imageInBounds,
              kCIInputScaleKey: 8.0,
              kCIInputCenterKey: CIVector(x: 0, y: 0) ]
        
        let filter = CIFilter(name: "CIPixellate", withInputParameters: pixellateOptions)
        
        let outputImage = filter!.outputImage!
        return outputImage
    }
    
    private func faceFeaturesInImage(image: CIImage, context: CIContext) -> [CIFaceFeature]
    {
        let detectorOptions = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace,
                                     context: context,
                                     options: detectorOptions)
        let features = faceDetector.featuresInImage(image)
        let faceFeatures = features.filter {
            (feature) -> Bool in
            feature is CIFaceFeature
        } as! [CIFaceFeature]
        
        return faceFeatures
    }
    
    class func printAllImageFilters()
    {
        let filterNames = CIFilter.filterNamesInCategories([kCICategoryStillImage])
        for filterName in filterNames {
            let filter = CIFilter(name: filterName)!
            print(filter)
        }
    }
}
