//
//  Copyright © 2015 Big Nerd Ranch
//

import UIKit

class PhotoInfoViewController: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    
    var photo: Photo! {
        didSet {
            navigationItem.title = photo.title
        }
    }
    var store: PhotoStore!
    var imageProcessor: ImageProcessor!
    var activeFilter: ImageFilter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        store.fetchImageForPhoto(photo) { result in
            switch result {
            case let .Success(image):
                
                let fuzzAction = ImageAction.PixellateFaces
                let filterAction = ImageAction.Filter(self.activeFilter)
                let actions = [fuzzAction, filterAction]
                
                var filteredImage: UIImage
                do {
                    filteredImage = try self.imageProcessor.imageByApplyingActions(actions, toImage: image)
                } catch {
                    print("Error: unable to filter image for \(self.photo): \(error)")
                    filteredImage = image
                }
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.imageView.image = filteredImage
                }
            case let .Failure(error):
                print("Error fetching image for photo: \(error)")
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowTags" {
            let navController = segue.destinationViewController as! UINavigationController
            let tagController = navController.topViewController as! TagsViewController
            
            tagController.store = store
            tagController.photo = photo
        }
    }
}
