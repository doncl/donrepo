//
//  Copyright © 2015 Big Nerd Ranch
//

import UIKit

class PhotosViewController: UIViewController, UICollectionViewDelegate {
    
    @IBOutlet var collectionView: UICollectionView!
    
    var store: PhotoStore!
    let photoDataSource = PhotoDataSource()
    let imageProcessor = ImageProcessor()
    let thumbnailStore = ThumbnailStore()
    var selectedFilter = ImageFilter.None
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = photoDataSource
        collectionView.delegate = self
        
        store.fetchInterestingPhotos { photosResult in
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                switch photosResult {
                case let .Success(photos):
                    print("Successfully found \(photos.count) interesting photos.")
                    self.photoDataSource.photos = photos
                case let .Failure(error):
                    self.photoDataSource.photos.removeAll()
                    print("Error fetching interesting photos: \(error)")
                }
                self.collectionView.reloadSections(NSIndexSet(index: 0))
            }
            
        }
    }
    
    func collectionView(collectionView: UICollectionView,
        willDisplayCell cell: UICollectionViewCell,
        forItemAtIndexPath indexPath: NSIndexPath) {

        let photo = photoDataSource.photos[indexPath.item]
        
        // First, check the thumbnail cache
        if let thumbnail = thumbnailStore.thumbnailForKey(photo.photoKey),
                cell = cell as? PhotoCollectionViewCell {
            cell.updateWithImage(thumbnail)
            return
        }

        // Download the image data, which could take some time
        store.fetchImageForPhoto(photo) { result in
            
            // Don't do any filtering if we don't have an image
            guard case let .Success(image) = result else {
                print("No image fetched for index \(indexPath.item)")
                return
            }
            
            
            // Prepare the actions for thumbnail creation
            let maxSize = CGSize(width: 200, height: 200)
            let scaleAction = ImageAction.Scale(maxSize: maxSize)
            let faceFuzzAction = ImageAction.PixellateFaces
            let filterAction = ImageAction.Filter(self.selectedFilter)
            let actions = [faceFuzzAction, filterAction, scaleAction]
            
            // Actually process the available photo into a thumbnail
            let thumbnail: UIImage
            do {
                thumbnail = try self.imageProcessor.imageByApplyingActions(actions, toImage: image)
            } catch {
                print("Error: unable to generate filtered thumbnail for \(photo): \(error)")
                thumbnail = image
            }
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.thumbnailStore.setThumbnail(thumbnail, forKey: photo.photoKey)
                
                // The index path for the photo might have changed between the
                // time the request started and finished, so find the most
                // recent index path
                
                // (Note: You will have an error on the next line; you will fix it shortly)
                let photoIndex = self.photoDataSource.photos.indexOf(photo)!
                let photoIndexPath = NSIndexPath(forItem: photoIndex, inSection: 0)
                
                // When the request finishes, only update the cell if it's still visible
                if let cell = self.collectionView.cellForItemAtIndexPath(photoIndexPath)
                    as? PhotoCollectionViewCell {
                    cell.updateWithImage(thumbnail)
                }
            }

        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showPhoto" {
            if let selectedIndexPath = collectionView.indexPathsForSelectedItems()?.first {
                    
                let photo = photoDataSource.photos[selectedIndexPath.item]
                
                let destinationVC =
                    segue.destinationViewController as! PhotoInfoViewController
                destinationVC.photo = photo
                destinationVC.store = store
                destinationVC.imageProcessor = imageProcessor
                destinationVC.activeFilter = selectedFilter
            }
        }
    }
    
    // MARK: - Actions

    @IBAction func filterChoiceChanged(sender: UISegmentedControl) {

        enum FilterChoice: Int {
            case None = 0, Gloom, Sepia, Blur
        }

        guard let choice = FilterChoice(rawValue: sender.selectedSegmentIndex) else {
            fatalError("Impossible segment selected: \(sender.selectedSegmentIndex)")
        }

        switch choice {
        case .None:
            selectedFilter = ImageFilter.None
        case .Gloom:
            selectedFilter = ImageFilter.Gloom(intensity: 3.0, radius: 30.0)
        case .Sepia:
            selectedFilter = ImageFilter.Sepia(intensity: 3.0)
        case .Blur:
            selectedFilter = ImageFilter.Blur(radius: 10.0)
        }
        
        thumbnailStore.clearThumbnails()
        collectionView.reloadData()

    }
    
}
