//
//  ThumbnailStore.swift
//  Photorama
//
//  Created by Michael Ward on 2/15/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit

class ThumbnailStore {
    
    private let thumbnailCache = NSCache()
    
    func thumbnailForKey(key: String) -> UIImage? {
        return thumbnailCache.objectForKey(key) as? UIImage
    }
    
    func setThumbnail(image: UIImage, forKey key:String) {
        thumbnailCache.setObject(image, forKey: key)
    }
    
    func clearThumbnails() {
        thumbnailCache.removeAllObjects()
    }

}
