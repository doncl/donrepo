//
//  ImageSelectorViewController.swift
//  FaceFuzzer
//
//  Created by Jeremy on 2014-09-16.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

import UIKit

class ImageSelectorViewController:
    UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var selectedImage: UIImage? {
        didSet {
            imageView.image = selectedImage

            let hasImage = (nil != selectedImage)
            imageView.hidden = !hasImage
            libraryButton.hidden = hasImage
        }
    }

    @IBAction func selectFromLibrary() {
        presentViewController(picker, animated: true, completion: nil)
    }

    func imagePickerController(
            picker: UIImagePickerController,
            didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        dismissViewControllerAnimated(true, completion: nil)
        selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
    }

    @IBOutlet weak var libraryButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    lazy var picker: UIImagePickerController = {
        let picker = UIImagePickerController()
        picker.sourceType = .PhotoLibrary
        picker.delegate = self
        return picker
    }()
}
