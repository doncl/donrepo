#import "QuizSet.h"

#import "QuizItem.h"

@interface QuizSet ()

@property (copy, readwrite, nonatomic) NSString *title;
@property (assign, readwrite, nonatomic) BNRQuizSetCategory category;
@property (copy, readwrite, nonatomic) NSArray *items;

@end // extension


@implementation QuizSet

- (NSString *) localizedNameForCategory {
    static const char *categoryNames[] = {
        "Science Fiction",
        "Music",
        "Mathematics",
        "Movies",
        "Last"
    };

    NSString *name = @( categoryNames[self.category] );
    return name;
} // localizedNameForCategory


+ (instancetype) quizSetFromPlistWithName: (NSString *) name {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *path = [bundle pathForResource: name  ofType: @"quiz"];
    assert (path);
    
    NSError *error;
    NSData *data = [NSData dataWithContentsOfFile: path
                           options: 0
                           error: &error];
    if (!data) {
        NSLog (@"error reading from path: %@", error);
        return nil;
    }

    id plist = [NSPropertyListSerialization propertyListWithData: data
                                            options: NSPropertyListImmutable
                                            format: NULL
                                            error: &error];
    if (!plist) {
        NSLog (@"error plisting: %@", error);
        return nil;
    }

    NSString *title = plist[@"Title"];
    NSString *categoryString = plist[@"Category"];
    NSUInteger category = [categoryString intValue];

    NSArray *rawItems = plist[@"Items"];
    NSArray *quizItems = [self makeQuizItemsFromRawItems: rawItems];

    QuizSet *quizSet = [[QuizSet alloc] initWithTitle: title
                                              category: category
                                              items: quizItems];
    return quizSet;
}


+ (NSArray *) makeQuizItemsFromRawItems: (NSArray *) rawItems {
    NSMutableArray *quizItems = [NSMutableArray new];

    for (NSDictionary *rawItem in rawItems) {
        NSString *question = rawItem[@"Q"];
        NSString *answer = rawItem[@"A"];
        NSString *imageName = rawItem[@"I"];

        QuizItem *item = [QuizItem new];
        item.question = question;
        item.answer = answer;
        item.imageName = imageName;

        [quizItems addObject: item];
    }

    return [quizItems copy];

} // makeQuizItemsFromRawItems


- (instancetype) initWithTitle: (NSString *) title
                      category: (NSUInteger) category
                         items: (NSArray *) items {
    self = [super init];
    if (self) {
        _title = [title copy];
        _category = category;
        _items = [items copy];
    }
    return self;
}


- (void) setTitle: (NSString *) title {

    // Work around oscar's empty title bug.  ++md(3/10/2012)
    if (title.length == 0) title = nil;

    _title = [title copy];

} // setTitle


- (NSUInteger) highScore {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *key = [NSString stringWithFormat: @"highscore-%@", self.title];
    NSUInteger highScore = [defaults integerForKey: key];

    return highScore;

} // highScore


- (void) setHighScore: (NSUInteger) newHighScore {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *key = [NSString stringWithFormat: @"highscore-%@", self.title];
    [defaults setInteger: newHighScore   forKey: key];
    
    [defaults synchronize];

} // setHighScore


- (NSString *) description {
    NSString *description = [NSString stringWithFormat: @"<%@: %p> %@, %d(%@), %@",
                                      self.class, self, self.title, self.category,
                                      self.localizedNameForCategory,
                                      self.items];

    return description;
} // description


@end // BNRQuizSet
