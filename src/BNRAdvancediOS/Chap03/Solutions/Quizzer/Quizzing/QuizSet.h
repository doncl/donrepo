#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,
                BNRQuizSetCategory) {
    kCategoryScienceFiction = 0,
    kCategoryMusic,
    kCategoryMathematics,
    kCategoryMovies,
    kCategoryLast
};

@interface QuizSet : NSObject

@property (copy, readonly, nonatomic) NSString *title;
@property (assign, readonly, nonatomic) BNRQuizSetCategory category;
@property (copy, readonly, nonatomic) NSArray *items;
@property (assign, nonatomic) NSUInteger highScore;

+ (instancetype) quizSetFromPlistWithName: (NSString *) name;

- (NSString *) localizedNameForCategory;

@end // BNRQuizSet
