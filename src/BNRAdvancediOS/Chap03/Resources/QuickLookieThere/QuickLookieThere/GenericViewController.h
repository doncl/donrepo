#import <UIKit/UIKit.h>

@class CLLocation;

@interface GenericViewController : UIViewController

@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) UIColor *color;
@property (strong, nonatomic) UIBezierPath *bezierPath;
@property (strong, nonatomic) CLLocation *location;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (copy, nonatomic) NSString *string;
@property (copy, nonatomic) NSAttributedString *attributedString;
@property (copy, nonatomic) NSData *data;
@property (strong, nonatomic) NSURL *url;

@end
