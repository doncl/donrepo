//
//  BNRAppDelegate.h
//  Bugspray
//
//  Created by Michael Ward on 1/24/12.
//  Copyright (c) 2012 Big Nerd Ranch, Inc. All rights reserved.
//


@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
