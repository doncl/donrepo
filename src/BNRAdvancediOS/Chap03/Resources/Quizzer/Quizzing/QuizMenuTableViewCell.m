//
//  BNRQuizMenuTableViewCell.m
//  Quizzing
//
//  Created by Mark Dalrymple on 4/2/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

#import "QuizMenuTableViewCell.h"

@implementation QuizMenuTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
