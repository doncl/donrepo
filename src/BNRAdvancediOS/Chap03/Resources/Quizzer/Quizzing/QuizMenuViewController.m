#import "QuizMenuViewController.h"

#import "QuizSet.h"
#import "QuizViewController.h"
#import "QuizMenuTableViewCell.h"

@interface QuizMenuViewController ()
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@end // extension


@implementation QuizMenuViewController

- (id) initWithNibName: (NSString *) nibNameOrNil
                bundle: (NSBundle *) nibBundleOrNil {
    self = [super initWithNibName: nibNameOrNil bundle: nibBundleOrNil];
    if (self)  {
        self.title = @"Quizzing";
    }
    return self;
} // initWithNibName


- (void) viewDidLoad {
    [super viewDidLoad];

    UINib *nib = [UINib nibWithNibName: @"QuizMenuTableViewCell"  bundle: nil];
    assert(nib);
    [self.menuTableView registerNib: nib  forCellReuseIdentifier: @"MenuTableViewCell"];

} // viewDidLoad


- (void) viewWillAppear: (BOOL) animated {
    // Update any high score changes.
    [self.menuTableView reloadData];
} // viewWillAppear


- (NSInteger) numberOfSectionsInTableView: (UITableView *) tableView {
    return 1;
} // numberOfSectionsInTableView


- (NSInteger) tableView: (UITableView *) tableView
  numberOfRowsInSection: (NSInteger) section {
    return self.quizzes.count;
} // numberOfRowsInSection


- (UITableViewCell *) tableView: (UITableView *) tableView
          cellForRowAtIndexPath: (NSIndexPath *) indexPath {

    QuizMenuTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier: @"MenuTableViewCell"
                   forIndexPath: indexPath];

    if (!cell) {
        cell = (QuizMenuTableViewCell *)[[QuizMenuTableViewCell alloc]
                                               initWithStyle: UITableViewCellStyleDefault
                                               reuseIdentifier: @"MenuTableViewCell"];
        assert(cell);
    }

    QuizSet *quizSet = self.quizzes[indexPath.row];

    NSString *title = quizSet.title;
    NSString *category = quizSet.localizedNameForCategory;
    NSUInteger count = quizSet.items.count;
    NSString *questionCount = [NSString stringWithFormat: (count == 1) ? @"%d question" : @"%d questions", quizSet.items.count];
    NSString *highScore = [NSString stringWithFormat: @"High Score: %d", quizSet.highScore];

    cell.titleLabel.text = title;
    cell.categoryLabel.text = category;
    cell.questionCountLabel.text = questionCount;
    cell.highScoreLabel.text = highScore;

    return cell;

} // cellForRowAtIndexPath


- (void) tableView: (UITableView *) tableView  didSelectRowAtIndexPath: (NSIndexPath *) indexPath {
    [self pushQuizAtIndex: indexPath.row];

} // didSelectRowAtIndexPath


- (void) pushQuizAtIndex: (NSUInteger) quizIndex {
    QuizViewController *qvc = [QuizViewController new];
    QuizSet *quizSet = self.quizzes[quizIndex];
    qvc.quizSet = quizSet;
    [self.navigationController pushViewController: qvc  animated: YES];

} // pushQuizAtIndex

@end // BNRQuizMenuViewController
