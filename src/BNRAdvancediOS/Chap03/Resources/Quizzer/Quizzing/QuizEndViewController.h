#import <UIKit/UIKit.h>

@class QuizSet;

@interface QuizEndViewController : UIViewController

@property (strong, nonatomic) QuizSet *quizSet;
@property (assign, nonatomic) NSUInteger score;
@property (assign, nonatomic) NSUInteger oldHighScore;

@end // BNRQuizEndViewController

