//
//  ViewController.swift
//  CatalogWrapper
//
//  Created by Don Clore on 2/25/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIWebViewDelegate {
    let indexURL = NSURL(string: "http://localhost:5000")
    var request: NSURLRequest
    
    @IBOutlet var webView: UIWebView!

    required init?(coder aDecoder: NSCoder) {
        request = NSURLRequest(URL: indexURL!)
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.loadRequest(request)
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @available(iOS 2.0, *) func webView(webView: UIWebView,
                                        didFailLoadWithError error: NSError?) {
        let complaint = "Could not load \(indexURL!).  Have you run run.py?"
        let alert = UIAlertView(title: "Could not load page", message: complaint,
                delegate: nil, cancelButtonTitle: "OK")

        alert.show()
    }

    @available(iOS 2.0, *) func webViewDidFinishLoad(webView: UIWebView) {
        let pageTitle = webView.stringByEvaluatingJavaScriptFromString("document.title")
        print("We just loaded \(pageTitle!)")
        navigationItem.title = pageTitle ?? "Catalog"
    }

    @available(iOS 2.0, *) func webView(webView: UIWebView, shouldStartLoadWithRequest
        newRequest: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {

        if request.URL!.absoluteString == newRequest.URL!.absoluteString {
            return true
        }

        if newRequest.URL!.absoluteString == "ios:addProductLocally" {
            addCurrentProduct(webView)
            return false
        }
        if navigationType == .LinkClicked {
            let vc = storyboard?.instantiateViewControllerWithIdentifier("WebViewController")
                as! ViewController

            vc.request = newRequest
            showViewController(vc, sender: self)
            return false
        }
        return true
    }

    func addCurrentProduct(webView: UIWebView) {
        let productName = webView.stringByEvaluatingJavaScriptFromString("getProductName()") ?? ""
        let catalogId = webView.stringByEvaluatingJavaScriptFromString("getCatalogId()") ?? ""
        let productId = webView.stringByEvaluatingJavaScriptFromString("getProductId()") ?? ""
        print("user wants to add \(catalogId):\(productId) (\(productName)) to local list")
    }
}

