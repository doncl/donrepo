//
//  ViewController.swift
//  LayerFunSwift
//
//  Copyright (c) 2015 BNR. All rights reserved.
//

import UIKit
import QuartzCore

class ViewController: UIViewController {

  @IBOutlet weak var subView: UIView!
  override func viewDidLoad() {
    super.viewDidLoad()
     self.subView.layer.contents = UIImage(named: "boardwalk.jpg")?.CGImage
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func buttonPressed(sender: AnyObject) {

    // Part 1
    //    let layer: CALayer = self.subView.layer
    //    println("buttonPressed") // Add breakpoint here
    //    self.subView.setNeedsDisplay()

    // Part 2
    //    let layer = sender.layer
    //    let anim: CABasicAnimation = CABasicAnimation(keyPath: "position")
    //    anim.fromValue = NSValue(CGPoint: layer.position)
    //    anim.toValue = NSValue(CGPoint: CGPointMake(layer.position.x, 0.0))
    //    anim.duration = 2.0
    //    layer.addAnimation(anim, forKey: "buttonAnimation")
    //    // buttonAnimation is a key which can be used later
    //    // to retrieve this animation
    //
    //
    //    layer.position = CGPointMake(layer.position.x, 0.0)
    
    
    // Part 3
    //    let layer = self.subView.layer
    //    let animation: CABasicAnimation = CABasicAnimation(keyPath: "transform")
    //    animation.fromValue = NSValue(CATransform3D: layer.transform)
    //    animation.toValue = NSValue(CATransform3D: CATransform3DMakeRotation(CGFloat(M_PI_2), 0.0, 0.0, 1.0))
    //    animation.duration = 1.0
    //    animation.autoreverses = true
    //    animation.repeatCount = Float.infinity
    //    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    //    layer.addAnimation(animation, forKey: "xform")

    
    // Part 4
    //    let layer = self.subView.layer
    //    let animation: CAKeyframeAnimation = CAKeyframeAnimation(keyPath: "transform")
    //    let scaleXForm = CATransform3DScale(layer.transform, 0.5, 0.5, 0.5)
    //
    //    let rotateTransform = NSValue(CATransform3D: CATransform3DRotate(scaleXForm, CGFloat(M_PI_2), 0.0, 0.0, 1.0))
    //
    //    animation.values = [NSValue(CATransform3D:layer.transform),NSValue(CATransform3D:scaleXForm),rotateTransform]
    //    animation.duration = 1.0
    //    animation.repeatCount = Float.infinity
    //    animation.autoreverses = true
    //    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    //    layer.addAnimation(animation, forKey: "xform")

    // Part 5
    //    let layer = self.subView.layer
    //    var perspective = CATransform3DIdentity
    //    perspective.m34 = -1.0/1000.0
    //    layer.transform = perspective
    //
    //    let animation: CAKeyframeAnimation = CAKeyframeAnimation(keyPath: "transform")
    //    let curXForm = layer.transform
    //    let rotXForm = CATransform3DRotate(curXForm, CGFloat(M_PI_2), 0.0, 1.0, 0.0)
    //    animation.values = [NSValue(CATransform3D:curXForm),
    //                            NSValue(CATransform3D:rotXForm)]
    //    animation.duration = 1.0
    //    animation.repeatCount = Float.infinity
    //    animation.autoreverses = true
    //    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    //    layer.addAnimation(animation, forKey: "xform")
 
    // Part 6
    let layer = self.subView.layer
    var perspective = CATransform3DIdentity
    perspective.m34 = -1.0/1000.0
    layer.transform = perspective
    
    let animation: CAKeyframeAnimation = CAKeyframeAnimation(keyPath: "transform")
    let curXForm = layer.transform
    let rotXForm = CATransform3DRotate(curXForm, CGFloat(M_PI_2), 0.0, 1.0, 0.0)
    let rotAndMoveLeft = CATransform3DTranslate(rotXForm, 0.0, 0.0, -100.0)
    let rotAndMoveRight = CATransform3DTranslate(rotXForm, 0.0, 0.0, 100.0)

    animation.values = [NSValue(CATransform3D:curXForm),
                        NSValue(CATransform3D:rotXForm),
                        NSValue(CATransform3D:rotAndMoveLeft),
                        NSValue(CATransform3D:rotAndMoveRight),
                        NSValue(CATransform3D:curXForm)]
    animation.duration = 2.0
    animation.repeatCount = Float.infinity
    animation.autoreverses = false
    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    layer.addAnimation(animation, forKey: "xform")

    
    

  
  
  }

}

