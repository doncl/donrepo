//
//  ViewController.swift
//  LayerFunSwift
//
//  Created by Rod Strougo on 5/11/15.
//  Copyright (c) 2015 BNR. All rights reserved.
//

import UIKit
import QuartzCore

class ViewController: UIViewController {

  @IBOutlet weak var subView: UIView!
  override func viewDidLoad() {
    super.viewDidLoad()

    subView.layer.contents = UIImage(named: "boardwalk.jpg")?.CGImage
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func buttonPressed(sender: AnyObject) {
    let layer = self.subView.layer
    var perspective = CATransform3DIdentity
    perspective.m34 = -1.0/1000.0
    layer.transform = perspective

    let animation: CAKeyframeAnimation = CAKeyframeAnimation(keyPath: "transform")

    let curXForm = layer.transform
    let rotXForm = CATransform3DRotate(curXForm, CGFloat(M_PI_2), 0.0, 1.0, 0.0)
    let rotAndMoveLeft = CATransform3DTranslate(rotXForm, 0.0, 0.0, -100.0)
    let rotAndMoveRight = CATransform3DTranslate(rotXForm, 0.0, 0.0, 100.0)
    animation.values =
    [
      NSValue(CATransform3D:curXForm),
      NSValue(CATransform3D:rotXForm),
      NSValue(CATransform3D:rotAndMoveLeft),
      NSValue(CATransform3D:rotAndMoveRight),
      NSValue(CATransform3D:curXForm)
    ]

    animation.duration = 2.0
    animation.autoreverses = false
    animation.repeatCount = Float.infinity
    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    layer.addAnimation(animation, forKey: "xform")
  }
}

