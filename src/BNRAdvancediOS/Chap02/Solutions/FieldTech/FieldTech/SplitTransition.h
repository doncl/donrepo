//
//  BNRSplitTransition.h
//  FieldTech
//

#import <Foundation/Foundation.h>

@interface SplitTransition : NSObject <UIViewControllerAnimatedTransitioning>

@end
