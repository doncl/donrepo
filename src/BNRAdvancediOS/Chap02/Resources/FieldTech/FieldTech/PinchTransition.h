//
//  PinchTransition.h
//  FieldTech
//
//  Created by Don Clore on 2/22/16.
//  Copyright © 2016 Jonathan Blocksom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PinchTransition : UIPercentDrivenInteractiveTransition
@property (nonatomic, assign, getter=isInteractive) BOOL interactive;
- (void)addInteractionToViewController:(UIViewController *)viewController;

@end
