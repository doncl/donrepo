//
//  PinchTransition.m
//  FieldTech
//
//  Created by Don Clore on 2/22/16.
//  Copyright © 2016 Jonathan Blocksom. All rights reserved.
//

#import "PinchTransition.h"

@interface PinchTransition() <UIGestureRecognizerDelegate>

@property (nonatomic, weak) UINavigationController *navController;
@property (nonatomic, assign) CGFloat firstScale;
@property (nonatomic, assign) CGFloat lastPercent;
@end

@implementation PinchTransition
- (void)addInteractionToViewController:(UIViewController *)viewController
{
    self.navController = viewController.navigationController;

    UIPinchGestureRecognizer *pinchRecognizer =
            [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [viewController.view addGestureRecognizer:pinchRecognizer];
}

- (void)handlePinch:(UIPinchGestureRecognizer *)pinchRecognizer
{
    switch (pinchRecognizer.state) {
        case UIGestureRecognizerStateBegan:
            self.interactive = YES;
            self.firstScale = pinchRecognizer.scale;
            [self.navController popViewControllerAnimated:YES];
            break;

        case UIGestureRecognizerStateChanged:
        {
            CGFloat percentComplete = 1.0 - (pinchRecognizer.scale / self.firstScale);
            NSLog(@"Percent complete is %f", percentComplete);
            self.lastPercent = percentComplete;
            [self updateInteractiveTransition:percentComplete];
            break;
        }

        case UIGestureRecognizerStateCancelled:
            self.interactive = NO;
            [self cancelInteractiveTransition];
            break;

        case UIGestureRecognizerStateEnded:
            self.interactive = NO;
            if (self.lastPercent > 0.5) {
                [self finishInteractiveTransition];
            } else {
                [self cancelInteractiveTransition];
            }            break;

        case UIGestureRecognizerStatePossible:
            
 UIGestureRecognizerStateFailed:
            break;
    }
}


@end
