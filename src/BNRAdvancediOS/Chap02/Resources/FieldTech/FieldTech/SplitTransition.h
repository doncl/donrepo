//
//  SplitTransition.h
//  FieldTech
//
//  Created by Don Clore on 2/22/16.
//  Copyright © 2016 Jonathan Blocksom. All rights reserved.
//

#ifndef SplitTransition_h
#define SplitTransition_h

@interface SplitTransition : NSObject<UIViewControllerAnimatedTransitioning>
@end

#endif /* SplitTransition_h */
