#!/usr/bin/ruby
# Message server that listens on +PORT+ (defaults to 8081).
# Responds to connection with several newline-terminated text messages,
# then closes the connection.
#
# Intended for use with the TempChat project of the Grand Central Dispatch
# chapter of Big Nerd Ranch's Advanced iOS Programming Guide.
#
# Author:: Big Nerd Ranch
# Copyright:: Copyright (c) 2014 Big Nerd Ranch
# License:: All rights reserved

require 'socket'

# Port to listen on. Set by environment variable +PORT+. Defaults to 8081.
PORT = ENV.fetch('PORT', 8081)
# Number of seconds to wait after sending each message.
NAP_SECONDS = 3

puts "#{$0}: listening on port #{PORT}"

server = TCPServer.new(PORT)
loop do
  Thread.start(server.accept) do |client|
    ['Hi from Ruby',
     'I said hello',
     'Helllloooooooo',
     'CAN YOU HEAR ME?'].each {
        |phrase|
        # Avoid +puts+ so data is written in one chunk.
        # Otherwise, client has to parse for newline.
        client.print(phrase + "\n")
        sleep(NAP_SECONDS)
    }
    client.close
  end
end

