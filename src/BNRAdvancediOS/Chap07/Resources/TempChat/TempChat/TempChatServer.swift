//
//  TempChatServer.swift
//  TempChat
//
//  Created by Jeremy on 2014-10-02.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

import Foundation

class TempChatServer {
    /// Nil string means chat session over.
    typealias MessageHandler = (String?) -> Void
    var messageHandler: MessageHandler
    var handlerQueue: dispatch_queue_t
    var closed: Bool { return socket == nil }

    init(socket: NSSocketNativeHandle, handlerQueue: dispatch_queue_t, messageHandler: MessageHandler) {
        self.messageHandler = messageHandler
        self.handlerQueue = handlerQueue
        self.socket = socket
        let queue = dispatch_get_global_queue(qos(QOS_CLASS_UTILITY), UInt(0))
        dispatch_async(queue) {
            self.read()
        }
    }

    private var socket: NSSocketNativeHandle?

    func send(message: String?) {
        let handler = self.messageHandler
        dispatch_async(handlerQueue) {
            handler(message)
        }
    }
}



private extension TempChatServer {
    func read() {
        precondition(self.socket != nil, "must have a valid socket")
        let socket = self.socket!
        let buffer = NSMutableData(length: 1024)!
        while true {
            let nread = Darwin.read(socket,
                buffer.mutableBytes, Int(buffer.length))
            print("\(self): read \(nread) bytes")
            if (nread > 0) {
                let maybeMessage = String.fromCString(
                    UnsafePointer<CChar>(buffer.mutableBytes))
                if let message = maybeMessage {
                    send(message)
                }
                buffer.resetBytesInRange(NSRange(location: 0, length: nread))
            } else if (nread == 0) {
                closeSocket()
                break
            } else {
                if errno == EINTR {
                    continue
                }
                perror("read")
                closeSocket()
                break
            }
        }
    }

    func closeSocket() {
        self.socket = nil
        send(nil)
    }
}
