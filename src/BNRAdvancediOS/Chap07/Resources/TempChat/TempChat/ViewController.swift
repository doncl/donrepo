//
//  ViewController.swift
//  TempChat
//
//  Created by Jeremy on 2014-10-01.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

import UIKit

/// Represents a message received from `TempChatServer`.
struct Message {
    let text: String
    let receivedDate: NSDate

    static var maxAge: NSTimeInterval {
        return 10
    }
}

class ViewController: UIViewController {
    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var connectButton: UIButton!
    @IBAction func connectAction() {
        let text = addressField.text ?? ""
        let parts = text.componentsSeparatedByString(":")
        if parts.count != 2 {
            return
        }
        
        let userInitiatedQueue = dispatch_get_global_queue(qos(QOS_CLASS_USER_INITIATED), UInt(0))
        dispatch_async(userInitiatedQueue) {
            let (ok, err) = self.connectToHost(parts[0], port: parts[1])
            if ok {
                return
            }
            
            dispatch_async(dispatch_get_main_queue()) {
                let alert = UIAlertController(
                title: NSLocalizedString("Failed to Connect",
                        comment: "alert title"),
                        message: err.localizedDescription,
                        preferredStyle: .Alert)
                alert.addAction(UIAlertAction(
                title: NSLocalizedString("OK", comment: "alert button title"),
                        style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }

    var messages = [Message]()
    var expirySource : dispatch_source_t! = nil
    var tempChatServer: TempChatServer! {
        didSet {
            let enabled = tempChatServer == nil
            for control in [addressField, connectButton] {
                control.enabled = enabled
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        messageTableView.registerClass(UITableViewCell.self,
            forCellReuseIdentifier: self.dynamicType.cellIdentifierMessage)
    }

    /// If successful, sets `tempChatServer`.
    func connectToHost(host: String, port: String) -> (Bool, NSError!) {
        var err: Int32 = 0
        let hints = addrinfo(
            ai_flags: AI_ADDRCONFIG | AI_NUMERICSERV,
            ai_family: PF_UNSPEC, ai_socktype: SOCK_STREAM,
            ai_protocol: IPPROTO_TCP,
            ai_addrlen: 0, ai_canonname: nil, ai_addr: nil, ai_next: nil)
        var infoPtr: UnsafeMutablePointer<addrinfo> = nil
        err = getaddrinfo(host, port, [hints], &infoPtr)

        // Simulate DNS latency
        sleep(3)

        if err != 0 {
            let description = String.fromCString(gai_strerror(err))!
            let info = userInfoWithLocalizedDescription(description)
            let error = NSError(domain: "TempChat", code: Int(err),
                userInfo: info)
            return (false, error)
        }

        var currentInfo: addrinfo = infoPtr.memory
        let addressInfos = anyGenerator {
            () -> addrinfo? in
            if currentInfo.ai_next != nil {
                currentInfo = currentInfo.ai_next.memory
                return currentInfo
            }
            return nil
        }

        for info in addressInfos {
            let (resolvedHost, resolvedPort) = stringsFromAddressInfo(info)
            print("trying to connect to \(resolvedHost) \(resolvedPort)")
            print("ai_next is \(info.ai_next)")

            let s = socket(info.ai_family, info.ai_socktype, info.ai_protocol)
            if s < 0 {
                err = errno
                perror("socket")
                continue
            }

            err = connect(s, info.ai_addr, info.ai_addrlen)
            if err != 0 {
                err = errno
                perror("connect")
                _ = close(s)
                continue
            }

            print("connected!")
            self.tempChatServer = TempChatServer(socket: s, handlerQueue: dispatch_get_main_queue()) {
                [unowned self]
                message in
                self.handleMessage(message)
            }
            break
        }

        let success = tempChatServer != nil
        var error: NSError?
        if !success {
            let description = String.fromCString(strerror(err))!
            let info = userInfoWithLocalizedDescription(description)
            error = NSError(domain: "TempChat", code: Int(err),
                userInfo: info)
        }
        return (success, error)
    }

    func removeExpiredMessages() {
        let expiredBeforeDate = NSDate(timeIntervalSinceNow: -Message.maxAge)
        let expired = messages.enumerate().filter {
            $0.element.receivedDate.compare(expiredBeforeDate) == NSComparisonResult.OrderedAscending
        }
        let indexes = expired.map {
            $0.index
        }

        messageTableView.beginUpdates()
        let indexPaths = indexes.map {
            NSIndexPath(forRow: $0, inSection:0)
        }

        messageTableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)

        for (removedCount, index) in indexes.enumerate() {
            messages.removeAtIndex(index - removedCount)
        }

        messageTableView.endUpdates()
        print("removed \(indexes.count) messages")
    }
}

/// Message Handling
private extension ViewController {
    /// Called when a new message arrives from `tempChatServer`.
    func handleMessage(maybeText: String?) {
        if maybeText == nil {
            tempChatServer = nil
            return
        }

        let text = maybeText!
        addMessageWithText(text)
    }

    /// Adds a message both to `messages` and to `messageTableView`.
    func addMessageWithText(text: String) {
        let message = Message(text: text, receivedDate: NSDate())
        print("\(self): adding message \(message.text)")
        messages.append(message)

        let indexPath = NSIndexPath(forRow: messages.count - 1, inSection: 0)
        messageTableView.insertRowsAtIndexPaths([indexPath],
            withRowAnimation: .Automatic)

        updateTimer()
    }

    func updateTimer() {
        expirySource = nil

        if messages.count == 0 {
            print("no messages left - not creating new timer")
            return
        }

        let earliestExpiry = messages.map {
            $0.receivedDate.timeIntervalSince1970 + Message.maxAge
        }.minElement()!

        expirySource = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue())
        let source = expirySource!

        var ts = timespec(tv_sec: __darwin_time_t(earliestExpiry), tv_nsec: 0)
        let nextFireTime = dispatch_walltime(&ts, 0)
        let repeatInterval = UInt64(Message.maxAge) * NSEC_PER_SEC
        let leeway = UInt64(1) * NSEC_PER_SEC
        dispatch_source_set_timer(source, nextFireTime, repeatInterval, leeway)

        dispatch_source_set_event_handler(source) {
            let actualExpiry = NSDate().timeIntervalSince1970
            let leeway = actualExpiry - earliestExpiry
            let direction = leeway > 0 ? "late" : "EARLY"
            print("timer fired \(direction) by \(abs(leeway)) seconds")

            self.removeExpiredMessages()
            self.updateTimer()
        }
        dispatch_resume(expirySource)
    }
}

extension ViewController: UITableViewDataSource {
    // XXX: Computed property, because "Class variables not yet supported"
    class var cellIdentifierMessage: String { return "Message" }

    func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int)
    -> Int {
        return messages.count
    }

    func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath)
    -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(
            ViewController.cellIdentifierMessage, forIndexPath: indexPath)
            
        let message = messages[indexPath.row]
        cell.textLabel!.text = message.text
        return cell
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return true
    }
}

/// Returns a dictionary suitable for use as NSError.userInfo.
func userInfoWithLocalizedDescription(description: String)
-> [NSObject:AnyObject] {
    let info = [
        NSLocalizedDescriptionKey as NSObject : description as AnyObject
    ]
    return info
}

/// Returns host and port from `info` formatted for presentation.
func stringsFromAddressInfo(info: addrinfo) ->(host: String, port: String) {
    let port = { () -> (UInt16) in
        if info.ai_family == AF_INET {
            let inaddr = UnsafePointer<sockaddr_in>(info.ai_addr)
            return UInt16(bigEndian: inaddr.memory.sin_port)
        } else {
            let in6addr = UnsafePointer<sockaddr_in6>(info.ai_addr)
            return UInt16(bigEndian: in6addr.memory.sin6_port)
        }
    }()
    let buffer = NSMutableData(length: numericCast(INET6_ADDRSTRLEN))!
    var result: UnsafePointer<Int8>
    if info.ai_family == AF_INET {
        let inaddr = UnsafePointer<sockaddr_in>(info.ai_addr)
        var address = inaddr.memory.sin_addr
        result = inet_ntop(info.ai_family, &address,
            UnsafeMutablePointer<Int8>(buffer.mutableBytes),
            socklen_t(buffer.length))
    } else {
        let in6addr = UnsafePointer<sockaddr_in6>(info.ai_addr)
        var address = in6addr.memory.sin6_addr
        result = inet_ntop(info.ai_family, &address,
            UnsafeMutablePointer<Int8>(buffer.mutableBytes),
            socklen_t(buffer.length))
    }
    let presentation = String.fromCString(result)
    if nil == presentation {
        perror("inet_ntop")
    }
    let host = presentation ?? "ERROR"
    return (host, "\(port)")
}

func qos(qos: qos_class_t) -> Int {
    return Int(qos.rawValue)
}
