//clang -o resolv resolv.c
/**
 * @file resolv.c
 * @author Jeremy W. Sherman (Big Nerd Ranch)
 * @license Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
 *
 * Prints all hosts returned by getaddrinfo() for the first argument,
 * or "localhost" if none provided.
 */
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>

int
main(int argc, const char *argv[]) {
    struct addrinfo hints = {
        .ai_flags = AI_ADDRCONFIG | AI_NUMERICSERV,
        .ai_family = PF_UNSPEC,
        .ai_socktype = SOCK_STREAM,
        .ai_protocol = IPPROTO_TCP,
    };
    struct addrinfo *infoList = NULL;
    const char *host = (argc >= 2) ? argv[1] : "localhost";
    int err = getaddrinfo(host, "8081", &hints, &infoList);
    if (err) {
        perror("getaddrinfo");
        exit(EXIT_FAILURE);
    }

    printf("looking up: %s\n", host);
    for (struct addrinfo *info = infoList;
            info != NULL; info = info->ai_next) {
        const void *src = NULL;
        if (AF_INET == info->ai_family) {
            struct sockaddr_in *addr = (void *)info->ai_addr;
            src = &addr->sin_addr;
        } else {
            struct sockaddr_in6 *addr = (void *)info->ai_addr;
            src = &addr->sin6_addr;
        }
        char buffer[INET6_ADDRSTRLEN];
        const char *name = inet_ntop(
            info->ai_family, src, buffer, sizeof(buffer));
        printf("%s\n", name);
    }
}
