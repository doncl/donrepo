//
//  PhoneCommunicator.swift
//  Moody
//
//  Created by Don Clore on 2/25/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import WatchKit

class PhoneCommunicator: BaseCommunicator {

    // MARK: - Notification Center
    override func loggedMoodWasAdded(notification: NSNotification) {
        super.loggedMoodWasAdded(notification)
        
        let loggedMood = notification.userInfo?[LoggedMoodManager.LoggedMoodUserInfoKey] as? LoggedMood
        
        sendLoggedMood(loggedMood)
    }
    
    func sendLoggedMood(loggedMood: LoggedMood?) {
        guard let loggedMoodData = LoggedMood.archiveLoggedMood(loggedMood) else {
            return
        }
        
        session?.transferUserInfo([BaseCommunicator.NewLoggedMoodUserInfoKey : loggedMoodData])
    }
}
