// 
// Copyright (c) 2015 Big Nerd Ranch
//

import UIKit

import WatchConnectivity

class LoggedMoodsViewController: UITableViewController, MoodViewControllerDelegate {
    var loggedMoodManager: LoggedMoodManager? = nil {
        willSet {
            removeNotificationObservers()
        }

        didSet {
            addNotificationObservers()
        }
    }

    
    // MARK: - Lifecycle
    deinit {
        removeNotificationObservers()
    }


    // MARK: - Table view data source
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let loggedMoodManager = loggedMoodManager else {
            return 0
        }

        return loggedMoodManager.countOfLoogedMoods()
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell       = tableView.dequeueReusableCellWithIdentifier("LoggedMoodCell", forIndexPath: indexPath)
        let loggedMood = loggedMoodManager?.loggedMoodAtIndex(indexPath.row)

        guard loggedMood != nil else {
            return cell
        }

        cell.textLabel?.text       = loggedMood!.mood.text
        cell.detailTextLabel?.text = NSDateFormatter.localizedStringFromDate(loggedMood!.date, dateStyle: .LongStyle, timeStyle: .MediumStyle)
        cell.imageView?.image      = loggedMood!.mood.emojiImageForSize(CGSize(width: 36.0, height: 36.0))

        return cell
    }

    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "NewLoggedMood" {
            let navVC  = segue.destinationViewController as? UINavigationController
            let moodVC = navVC?.viewControllers[0] as? MoodViewController

            moodVC?.delegate = self
        }
    }


    // MARK: - MoodViewControllerDelegate
    func moodViewControllerDidCancel(moodViewController: MoodViewController) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    func moodViewControllerDidSave(moodViewController: MoodViewController, mood: Mood) {
        loggedMoodManager?.logMood(mood)

        dismissViewControllerAnimated(true, completion: nil)
    }


    // MARK: - Notifications
    private func addNotificationObservers() {
        loggedMoodManager?.notificationCenter.addObserver( self,
                                                 selector: Selector("loggedMoodWasAdded:"),
                                                     name: LoggedMoodManager.LoggedMoodAddedNotification,
                                                   object: loggedMoodManager)

        loggedMoodManager?.notificationCenter.addObserver( self,
                                                 selector: Selector("loggedMoodsReset:"),
                                                     name: LoggedMoodManager.LoggedMoodsResetNotification,
                                                   object: loggedMoodManager)
    }

    private func removeNotificationObservers() {
        loggedMoodManager?.notificationCenter.removeObserver( self,
                                                        name: LoggedMoodManager.LoggedMoodAddedNotification,
                                                      object: loggedMoodManager)

        loggedMoodManager?.notificationCenter.removeObserver( self,
                                                        name: LoggedMoodManager.LoggedMoodsResetNotification,
                                                      object: loggedMoodManager)
    }

    @objc private func loggedMoodWasAdded(notification: NSNotification) {
        let newLoggedMoodIndex = notification.userInfo?[LoggedMoodManager.LoggedMoodIndexUserInfoKey] as? Int

        guard newLoggedMoodIndex != nil else {
            return
        }

        self.tableView.insertRowsAtIndexPaths( [NSIndexPath(forRow: newLoggedMoodIndex!, inSection: 0)],
                             withRowAnimation: UITableViewRowAnimation.Automatic)
    }

    @objc private func loggedMoodsReset(notification: NSNotification) {
        self.tableView.reloadData()
    }
}
