// 
// Copyright (c) 2015 Big Nerd Ranch
//

import Foundation
import WatchConnectivity

class BaseCommunicator: NSObject, WCSessionDelegate {
    static let NewLoggedMoodUserInfoKey     = "NewLoggedMoodUserInfoKey"
    static let RecentLoggedMoodsUserInfoKey = "RecentLoggedMoodsUserInfoKey"
    static let RecentTimeFrame              = 60.0 * 60.0 * 24.0 * 2.0

    let loggedMoodManager: LoggedMoodManager
    let session: WCSession?

    init(loggedMoodManager: LoggedMoodManager) {
        self.loggedMoodManager = loggedMoodManager
        self.session = WCSession.isSupported() ? WCSession.defaultSession() : nil

        super.init()

        session?.delegate = self
        session?.activateSession()

        addNotificationObservers()
    }

    deinit {
        removeNotificationObservers()
    }


    // MARK: - NotificationCenter
    private func addNotificationObservers() {
        loggedMoodManager.notificationCenter.addObserver( self,
                                                selector: Selector("loggedMoodWasAdded:"),
                                                    name: LoggedMoodManager.LoggedMoodAddedNotification,
                                                  object: loggedMoodManager)

        loggedMoodManager.notificationCenter.addObserver( self,
                                                selector: Selector("loggedMoodsReset:"),
                                                    name: LoggedMoodManager.LoggedMoodsResetNotification,
                                                  object: loggedMoodManager)
    }

    private func removeNotificationObservers() {
        loggedMoodManager.notificationCenter.removeObserver( self,
                                                       name: LoggedMoodManager.LoggedMoodAddedNotification,
                                                     object: loggedMoodManager)

        loggedMoodManager.notificationCenter.removeObserver( self,
                                                       name: LoggedMoodManager.LoggedMoodsResetNotification,
                                                     object: loggedMoodManager)

    }

    @objc func loggedMoodWasAdded(notification: NSNotification) {

    }

    @objc func loggedMoodsReset(notification: NSNotification) {

    }
}
