//
//  WatchCommunicator.swift
//  Moody
//
//  Created by Don Clore on 2/25/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

import UIKit
import WatchConnectivity

class WatchCommunicator: BaseCommunicator {
    @available(iOS 9.0, *) func session(session: WCSession,
                                        didReceiveUserInfo userInfo: [String:AnyObject]) {

        let newLoggedMoodData = userInfo[BaseCommunicator.NewLoggedMoodUserInfoKey] as? NSData
        
        let newLoggedMood = LoggedMood.unarchiveLoggedMood(newLoggedMoodData)
        if let newLoggedMood = newLoggedMood {
            loggedMoodManager.addLoggedMood(newLoggedMood)
            loggedMoodManager.addLoggedMood(newLoggedMood)
        }
    }

}
