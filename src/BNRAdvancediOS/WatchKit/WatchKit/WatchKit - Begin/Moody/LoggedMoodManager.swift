// 
// Copyright (c) 2015 Big Nerd Ranch
//

import Foundation


class LoggedMoodManager: NSObject {
    static let LoggedMoodAddedNotification  = "LoggedMoodAddedNotification"
    static let LoggedMoodsResetNotification = "LoggedMoodsResetNotification"

    static let LoggedMoodUserInfoKey        = "LoggedMoodUserInfoKey"
    static let LoggedMoodIndexUserInfoKey   = "LoggedMoodIndexUserInfoKey"

    private static let ArchiveFileName      = "logged_moods.archive"

    let notificationCenter: NSNotificationCenter
    let fileManager: NSFileManager

    private var loggedMoods = [LoggedMood]()

    var latestLoggedMood: LoggedMood? {
        get {
            return loggedMoods.first
        }
    }

    var earliestLoggedMood: LoggedMood? {
        get {
            return loggedMoods.last
        }
    }


    private lazy var archiveURL: NSURL = {
        let urls           = self.fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        let documentDirURL = urls[0]
        let archiveURL     = documentDirURL.URLByAppendingPathComponent(LoggedMoodManager.ArchiveFileName)

        return archiveURL
    }()

    convenience override init() {
        self.init(fileManager: NSFileManager.defaultManager(), notificationCenter: NSNotificationCenter.defaultCenter())
    }

    init(fileManager: NSFileManager, notificationCenter: NSNotificationCenter) {
        self.fileManager        = fileManager
        self.notificationCenter = notificationCenter

        super.init()

        loadFromArchive()
    }


    // MARK: - Access the managed LoggedMoods
    func countOfLoogedMoods() -> Int {
        return loggedMoods.count
    }

    func indexOfLoggedMood(loggedMood: LoggedMood) -> Int? {
        return loggedMoods.indexOf(loggedMood)
    }

    func loggedMoodAtIndex(index: Int) -> LoggedMood? {
        guard loggedMoods.count > index && index >= 0 else {
            return nil
        }

        return loggedMoods[index]
    }

    func loggedMoodsAfterDate(earliestDate: NSDate) -> [LoggedMood] {
        return loggedMoods.filter { earliestDate.compare($0.date) == .OrderedAscending }
    }

    func loggedMoodsBeforeDate(latestDate: NSDate) -> [LoggedMood] {
        return loggedMoods.filter { latestDate.compare($0.date) == .OrderedDescending }
    }


    // MARK: - Modify the managed LoggedMoods
    func addLoggedMood(newLoggedMood: LoggedMood) {
        guard !loggedMoods.contains(newLoggedMood) else {
            return
        }

        // Provide some thread safety by forcing this to always happen on the main thread
        dispatch_async(dispatch_get_main_queue()) {
            self.loggedMoods.append(newLoggedMood)
            self.loggedMoods.sortInPlace { $0.date.compare($1.date) == NSComparisonResult.OrderedDescending }

            let newLoggedMoodIndex = self.loggedMoods.indexOf(newLoggedMood)!

            self.saveToArchive()

            self.notificationCenter.postNotificationName( LoggedMoodManager.LoggedMoodAddedNotification,
                object: self,
                userInfo: [LoggedMoodManager.LoggedMoodUserInfoKey      : newLoggedMood,
                           LoggedMoodManager.LoggedMoodIndexUserInfoKey : newLoggedMoodIndex])
        }
    }

    func logMood(mood: Mood, date: NSDate = NSDate()) {
        let newLoggedMood = LoggedMood(mood: mood, date: date)

        addLoggedMood(newLoggedMood)
    }

    func resetAllLoggedMoods(loggedMoods: [LoggedMood]) {
        // Provide some thread safety by forcing this to always happen on the main thread
        dispatch_async(dispatch_get_main_queue()) {
            self.loggedMoods = loggedMoods.sort { $0.date.compare($1.date) == NSComparisonResult.OrderedDescending }

            self.saveToArchive()

            self.notificationCenter.postNotificationName( LoggedMoodManager.LoggedMoodsResetNotification,
                                                  object: self)
        }
    }


    // MARK: - Persistence
    func loadFromArchive() -> Bool {
        let loadedLoggedMoods = NSKeyedUnarchiver.unarchiveObjectWithFile(archiveURL.path!) as? [LoggedMood]

        guard let loggedMoods = loadedLoggedMoods else {
            return false
        }

        resetAllLoggedMoods(loggedMoods)

        return true
    }

    func saveToArchive() -> Bool {
        return NSKeyedArchiver.archiveRootObject(loggedMoods, toFile: archiveURL.path!)
    }
}
