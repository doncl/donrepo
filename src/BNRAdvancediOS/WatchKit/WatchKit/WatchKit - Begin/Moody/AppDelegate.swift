// 
// Copyright (c) 2015 Big Nerd Ranch
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var loggedMoodManager: LoggedMoodManager?
    var communicator: WatchCommunicator?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        loggedMoodManager = LoggedMoodManager()
        communicator = WatchCommunicator(loggedMoodManager : loggedMoodManager!)

        let navigationVC  = window?.rootViewController as? UINavigationController
        let loggedMoodsVC = navigationVC?.viewControllers[0] as? LoggedMoodsViewController

        loggedMoodsVC?.loggedMoodManager = loggedMoodManager

        return true
    }
}

