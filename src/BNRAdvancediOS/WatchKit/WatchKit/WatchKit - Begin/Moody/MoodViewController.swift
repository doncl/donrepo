// 
// Copyright (c) 2015 Big Nerd Ranch
//

import UIKit

protocol MoodViewControllerDelegate: NSObjectProtocol {
    func moodViewControllerDidCancel(moodViewController: MoodViewController);
    func moodViewControllerDidSave(moodViewController: MoodViewController, mood: Mood);
}

class MoodViewController: UIViewController {
    var moods = Mood.allMoods

    weak var delegate: MoodViewControllerDelegate?

    private var currentMoodIndex = 0

    @IBOutlet private var moodTextLabel: UILabel!
    @IBOutlet var moodEmojiImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        updateUI()
    }

    private func updateUI() {
        let shortestEdge = fmin(moodEmojiImageView.bounds.width, moodEmojiImageView.bounds.height)
        moodEmojiImageView.image = moods[currentMoodIndex].emojiImageForSize(CGSize(width: shortestEdge, height: shortestEdge)) 
        moodTextLabel.text  = "...\(moods[currentMoodIndex].text)."
    }

    // MARK - Actions
    @IBAction func viewTapped(sender: UITapGestureRecognizer) {
        currentMoodIndex = (currentMoodIndex + 1) % moods.count

        updateUI()
    }

    @IBAction func cancelTapped(sender: UIBarButtonItem) {
        delegate?.moodViewControllerDidCancel(self)
    }

    @IBAction func saveTapped(sender: UIButton) {
        delegate?.moodViewControllerDidSave(self, mood: moods[currentMoodIndex])
    }
}
