// 
// Copyright (c) 2015 Big Nerd Ranch
//

import Foundation

// NOTE: Using @objc(class_name) so that at runtime, the ModuleName is not prefixed
//       on the class_name
@objc(Mood) class Mood: NSObject, NSCoding {
    static let allMoods = [Mood(text: "happy", emoji: ":)"),
                           Mood(text: "meh",   emoji: ":|"),
                           Mood(text: "sad",   emoji: ":(")]

    let text:  String
    let emoji: String

    init(text: String, emoji: String) {
        self.text  = text
        self.emoji = emoji
    }

    required init?(coder aDecoder: NSCoder) {
        text  = aDecoder.decodeObjectForKey("text")  as! String
        emoji = aDecoder.decodeObjectForKey("emoji") as! String
    }

    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(text,  forKey: "text")
        aCoder.encodeObject(emoji, forKey: "emoji")
    }



    override func isEqual(rhs: AnyObject?) -> Bool {
        guard let rhs = rhs as? Mood else {
            return false
        }

        return self.text == rhs.text && self.emoji == rhs.emoji
    }

    override var hash: Int {
        return self.text.hash
    }
}


import UIKit

extension Mood {
    func emojiImageForSize(size: CGSize) -> UIImage {
        return emojiImageForSize(size, backgroundColor: UIColor(white: 0.15, alpha: 1.0))
    }

    func emojiImageForSize(size: CGSize, backgroundColor: UIColor) -> UIImage {
        let canvasRect             = CGRect(origin: CGPointZero, size: size)
        let backgroundRect         = canvasRect
        let backgroundCornerRadius = fmin(backgroundRect.width, backgroundRect.height) * 0.05
        let textRect               = canvasRect.insetBy(dx: backgroundCornerRadius * 2, dy: backgroundCornerRadius * 2)

        let textColor              = UIColor.whiteColor()

        let screenScale            = CGFloat(2.0)

        let fontName       = "Courier"
        var fontSize       = textRect.height
        var font           = UIFont(name: fontName, size: fontSize)!
        var textAttributes = [NSFontAttributeName            : font,
                              NSForegroundColorAttributeName : textColor]
        var attributedText = NSAttributedString(string: emoji, attributes: textAttributes)
        

        while textRect.size.width < attributedText.size().width || textRect.size.height < attributedText.size().height {
            fontSize -= 1

            font = font.fontWithSize(fontSize)
            textAttributes[NSFontAttributeName] = font
            attributedText = NSAttributedString(string: emoji, attributes: textAttributes)
        }

        let drawAtX = (textRect.size.width - attributedText.size().width) / 2.0 + textRect.origin.x
        let drawAtY = (textRect.size.height - attributedText.size().height) / 2.0 + textRect.origin.y


        // Do the drawing and create the image
        UIGraphicsBeginImageContextWithOptions(canvasRect.size, false, screenScale)

        let background = UIBezierPath(roundedRect: backgroundRect, cornerRadius: backgroundCornerRadius)

        backgroundColor.setFill()
        background.fill()

        attributedText.drawAtPoint(CGPoint(x: drawAtX, y: drawAtY))

        let image = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()
        
        return image
    }
}
