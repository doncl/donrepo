// 
// Copyright (c) 2015 Big Nerd Ranch
//

import WatchKit

class ExtensionDelegate: NSObject, WKExtensionDelegate {
    private(set) var communicator: PhoneCommunicator?

    private(set) lazy var loggedMoodManager: LoggedMoodManager = {
        let loggedMoodManager = LoggedMoodManager()

        self.communicator = PhoneCommunicator(loggedMoodManager: loggedMoodManager)

        return loggedMoodManager
    }()


    func applicationDidFinishLaunching() {
        // Perform any final initialization of your application.
    }

    func applicationDidBecomeActive() {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }

}
