// 
// Copyright (c) 2015 Big Nerd Ranch
//

import WatchKit
import Foundation


class GlanceController: WKInterfaceController {
    lazy var loggedMoodManager: LoggedMoodManager = {
        let extensionDelegate = WKExtension.sharedExtension().delegate as! ExtensionDelegate

        return extensionDelegate.loggedMoodManager
    }()

    @IBOutlet var emojiImageGroup: WKInterfaceGroup!
    @IBOutlet var moodTextLabel: WKInterfaceLabel!
    @IBOutlet var dateLabel: WKInterfaceLabel!
    
    override func willActivate() {
        super.willActivate()

        addNotificationObservers()
        updateUI()
    }

    override func didDeactivate() {
        super.didDeactivate()

        removeNotificationObservers()
    }

    private func updateUI() {
        let latestLoggedMood = loggedMoodManager.latestLoggedMood

        var moodImage:  UIImage? = nil
        var moodText:   String?  = "No moods logged."
        var dateText:   String?  = nil

        if let latestLoggedMood = latestLoggedMood {
            moodImage = latestLoggedMood.mood.emojiImageForSize(CGSize(width: 100, height: 100))
            moodText  = latestLoggedMood.mood.text
            dateText  = NSDateFormatter.localizedStringFromDate(latestLoggedMood.date, dateStyle: .MediumStyle, timeStyle: .MediumStyle)
        }

        emojiImageGroup.setBackgroundImage(moodImage)
        moodTextLabel.setText(moodText)
        dateLabel.setText(dateText)
    }

    // MARK: - Notification Center
    private func addNotificationObservers() {
        loggedMoodManager.notificationCenter.addObserver( self,
                                                selector: Selector("loggedMoodsChanged:"),
                                                    name: LoggedMoodManager.LoggedMoodAddedNotification,
                                                  object: loggedMoodManager)

        loggedMoodManager.notificationCenter.addObserver( self,
                                                selector: Selector("loggedMoodsChanged:"),
                                                    name: LoggedMoodManager.LoggedMoodsResetNotification,
                                                  object: loggedMoodManager)
    }

    private func removeNotificationObservers() {
        loggedMoodManager.notificationCenter.removeObserver( self,
                                                       name: LoggedMoodManager.LoggedMoodAddedNotification,
                                                     object: loggedMoodManager)

        loggedMoodManager.notificationCenter.removeObserver( self,
                                                       name: LoggedMoodManager.LoggedMoodsResetNotification,
                                                     object: loggedMoodManager)
    }

    @objc private func loggedMoodsChanged(notification: NSNotification) {
        self.updateUI()
    }
}
