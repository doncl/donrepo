// 
// Copyright (c) 2015 Big Nerd Ranch
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    private let allMoods = Mood.allMoods
    private var currentMood: Mood?

    lazy var loggedMoodManager: LoggedMoodManager = {
        let extensionDelegate = WKExtension.sharedExtension().delegate as! ExtensionDelegate

        return extensionDelegate.loggedMoodManager
    }()

    @IBOutlet var moodPicker: WKInterfacePicker!

    
    // MARK: - Interface Controller Lifecycle
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)

        var moodItems = [WKPickerItem]()

        for mood in allMoods {
            let moodItem   = WKPickerItem()
            let emojiImage = mood.emojiImageForSize(CGSize(width: 100, height: 100))

            moodItem.contentImage = WKImage(image: emojiImage)
            moodItems.append(moodItem)
        }

        moodPicker.setItems(moodItems)
    }

    override func willActivate() {
        super.willActivate()

        currentMood = allMoods[0]
        moodPicker.setSelectedItemIndex(0)
    }

    @IBAction func saveTapped() {
        guard let currentMood = currentMood else {
            return
        }

        loggedMoodManager.logMood(currentMood)
    }

    @IBAction func moodPickerChanged(value: Int) {
        currentMood = allMoods[value]
    }
}
