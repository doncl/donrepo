// 
// Copyright (c) 2015 Big Nerd Ranch
//

import ClockKit
import WatchKit


class ComplicationController: NSObject, CLKComplicationDataSource {
    lazy var loggedMoodManager: LoggedMoodManager = {
        let extensionDelegate = WKExtension.sharedExtension().delegate as! ExtensionDelegate

        return extensionDelegate.loggedMoodManager
    }()

    private func templateForMood(mood: Mood, complication: CLKComplication) -> CLKComplicationTemplate? {
        let template: CLKComplicationTemplate?

        switch complication.family {
        case .UtilitarianSmall:
            let utilitarianTemplate = CLKComplicationTemplateUtilitarianSmallSquare()
            let emojiImage          = mood.emojiImageForSize(CGSize(width: 40, height: 40), backgroundColor: UIColor.clearColor())

            utilitarianTemplate.imageProvider = CLKImageProvider(onePieceImage: emojiImage)

            template = utilitarianTemplate
        case .UtilitarianLarge:
            let utilitarianTemplate          = CLKComplicationTemplateUtilitarianLargeFlat()
            utilitarianTemplate.textProvider = CLKSimpleTextProvider(text: mood.text, shortText: mood.emoji)

            template = utilitarianTemplate
        default:
            template = nil
        }

        return template
    }

    private func timelineEntryForLoggedMood(loggedMood: LoggedMood?, complication: CLKComplication) -> CLKComplicationTimelineEntry? {
        guard let loggedMood = loggedMood, let template = templateForMood(loggedMood.mood, complication: complication) else {
            return nil
        }

        return CLKComplicationTimelineEntry(date: loggedMood.date, complicationTemplate: template)
    }

    // MARK: - Timeline Configuration
    func getSupportedTimeTravelDirectionsForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTimeTravelDirections) -> Void) {
        handler([.Backward])
    }
    
    func getTimelineStartDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        let earliestDate = loggedMoodManager.earliestLoggedMood?.date
        
        handler(earliestDate)
    }
    
    func getTimelineEndDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        let latestDate = loggedMoodManager.latestLoggedMood?.date
        
        handler(latestDate)
    }
    
    func getPrivacyBehaviorForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationPrivacyBehavior) -> Void) {
        handler(.ShowOnLockScreen)
    }
    
    // MARK: - Timeline Population
    func getCurrentTimelineEntryForComplication(complication: CLKComplication, withHandler handler: ((CLKComplicationTimelineEntry?) -> Void)) {
        let entry = timelineEntryForLoggedMood(loggedMoodManager.latestLoggedMood, complication: complication)

        handler(entry)
    }

    func getTimelineEntriesForComplication(complication: CLKComplication, beforeDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {
        let filteredLoggedMoods = loggedMoodManager.loggedMoodsBeforeDate(date)
        var timelineEntries     = filteredLoggedMoods.flatMap { timelineEntryForLoggedMood($0, complication: complication) }

        if timelineEntries.count > limit {
            timelineEntries = [CLKComplicationTimelineEntry](timelineEntries[0..<limit])
        }

        handler(timelineEntries.reverse())
    }

    func getTimelineEntriesForComplication(complication: CLKComplication, afterDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {
        let filteredLoggedMoods = loggedMoodManager.loggedMoodsAfterDate(date)
        var timelineEntries     = filteredLoggedMoods.flatMap { timelineEntryForLoggedMood($0, complication: complication) }

        if timelineEntries.count > limit {
            timelineEntries = [CLKComplicationTimelineEntry](timelineEntries[0..<limit])
        }

        handler(timelineEntries.reverse())
    }
    
    // MARK: - Update Scheduling
    func getNextRequestedUpdateDateWithHandler(handler: (NSDate?) -> Void) {
        // Call the handler with the date when you would next like to be given the opportunity to update your complication content
        handler(nil);
    }
    
    // MARK: - Placeholder Templates
    func getPlaceholderTemplateForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTemplate?) -> Void) {
        let template = templateForMood(Mood.allMoods[0], complication: complication)

        handler(template)
    }
}
