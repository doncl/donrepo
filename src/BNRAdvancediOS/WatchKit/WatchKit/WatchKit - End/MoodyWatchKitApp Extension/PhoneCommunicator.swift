// 
// Copyright (c) 2015 Big Nerd Ranch
//

import WatchKit
import ClockKit
import WatchConnectivity

class PhoneCommunicator: BaseCommunicator {
    override init(loggedMoodManager: LoggedMoodManager) {
        super.init(loggedMoodManager: loggedMoodManager)

        updateLoggedMoodsFromApplicationContext()
    }

    private func refreshComplicationTimeline(fullReload: Bool = false) {
        let server = CLKComplicationServer.sharedInstance()

        for complication in server.activeComplications {
            if fullReload {
                server.reloadTimelineForComplication(complication)
            }
            else {
                server.extendTimelineForComplication(complication)
            }
        }
    }

    private func updateLoggedMoodsFromApplicationContext() {
        let applicationContext    = session?.receivedApplicationContext
        let recentLoggedMoodsData = applicationContext?[BaseCommunicator.RecentLoggedMoodsUserInfoKey] as? NSData
        let recentLoggedMoods     = LoggedMood.unarchiveLoggedMoods(recentLoggedMoodsData)

        loggedMoodManager.resetAllLoggedMoods(recentLoggedMoods)
    }

    override func loggedMoodWasAdded(notification: NSNotification) {
        super.loggedMoodWasAdded(notification)

        refreshComplicationTimeline()
    }

    override func loggedMoodsReset(notification: NSNotification) {
        super.loggedMoodsReset(notification)

        refreshComplicationTimeline(true)
    }

    // MARK: - WCSessionDelegate
    func session(session: WCSession, didReceiveApplicationContext applicationContext: [String : AnyObject]) {
        updateLoggedMoodsFromApplicationContext()
    }
}
