// 
// Copyright (c) 2015 Big Nerd Ranch
//

import Foundation
import WatchConnectivity

class BaseCommunicator: NSObject, WCSessionDelegate {
    static let NewLoggedMoodUserInfoKey     = "NewLoggedMoodUserInfoKey"
    static let RecentLoggedMoodsUserInfoKey = "RecentLoggedMoodsUserInfoKey"
    static let RecentTimeFrame              = 60.0 * 60.0 * 24.0 * 2.0

    let loggedMoodManager: LoggedMoodManager
    let session: WCSession?

    init(loggedMoodManager: LoggedMoodManager) {
        self.loggedMoodManager = loggedMoodManager
        self.session = WCSession.isSupported() ? WCSession.defaultSession() : nil

        super.init()

        session?.delegate = self
        session?.activateSession()

        addNotificationObservers()
    }

    deinit {
        removeNotificationObservers()
    }

    func sendLoggedMood(loggedMood: LoggedMood?) {
        guard let loggedMoodData = LoggedMood.archiveLoggedMood(loggedMood) else {
            return
        }

        let userInfo = [BaseCommunicator.NewLoggedMoodUserInfoKey : loggedMoodData]

        #if os(iOS)
            session?.transferCurrentComplicationUserInfo(userInfo)
        #else
            session?.transferUserInfo(userInfo)
        #endif
    }
    

    // MARK: - WCSessionDelegate
    func session(session: WCSession, didReceiveUserInfo userInfo: [String : AnyObject]) {
        let newLoggedMoodData = userInfo[BaseCommunicator.NewLoggedMoodUserInfoKey] as? NSData
        let newLoggedMood     = LoggedMood.unarchiveLoggedMood(newLoggedMoodData)

        if let newLoggedMood = newLoggedMood {
            loggedMoodManager.addLoggedMood(newLoggedMood)
        }
    }
    

    // MARK: - NotificationCenter
    private func addNotificationObservers() {
        loggedMoodManager.notificationCenter.addObserver( self,
                                                selector: Selector("loggedMoodWasAdded:"),
                                                    name: LoggedMoodManager.LoggedMoodAddedNotification,
                                                  object: loggedMoodManager)

        loggedMoodManager.notificationCenter.addObserver( self,
                                                selector: Selector("loggedMoodsReset:"),
                                                    name: LoggedMoodManager.LoggedMoodsResetNotification,
                                                  object: loggedMoodManager)
    }

    private func removeNotificationObservers() {
        loggedMoodManager.notificationCenter.removeObserver( self,
                                                       name: LoggedMoodManager.LoggedMoodAddedNotification,
                                                     object: loggedMoodManager)

        loggedMoodManager.notificationCenter.removeObserver( self,
                                                       name: LoggedMoodManager.LoggedMoodsResetNotification,
                                                     object: loggedMoodManager)

    }

    @objc func loggedMoodWasAdded(notification: NSNotification) {
        let loggedMood = notification.userInfo?[LoggedMoodManager.LoggedMoodUserInfoKey] as? LoggedMood

        sendLoggedMood(loggedMood)
    }

    @objc func loggedMoodsReset(notification: NSNotification) {

    }
}
