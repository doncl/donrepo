// 
// Copyright (c) 2015 Big Nerd Ranch
//

import Foundation

// NOTE: Using @objc(class_name) so that at runtime, the ModuleName is not prefixed
//       on the class_name
@objc(LoggedMood) class LoggedMood: NSObject, NSCoding {
    let mood: Mood
    let date: NSDate

    init(mood: Mood, date: NSDate = NSDate()) {
        self.mood = mood
        self.date = date
    }

    required init?(coder aDecoder: NSCoder) {
        mood = aDecoder.decodeObjectForKey("mood") as! Mood
        date = aDecoder.decodeObjectForKey("date") as! NSDate
    }

    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(mood, forKey: "mood")
        aCoder.encodeObject(date, forKey: "date")
    }

    override func isEqual(rhs: AnyObject?) -> Bool {
        guard let rhs = rhs as? LoggedMood else {
            return false
        }

        return self.date == rhs.date && self.mood == rhs.mood
    }

    override var hash: Int {
        return date.hash
    }
}


extension LoggedMood {
    // MARK: - Archiving Convenience Methods
    class func archiveLoggedMood(loggedMood: LoggedMood?) -> NSData? {
        guard let loggedMood = loggedMood else {
            return nil
        }

        return NSKeyedArchiver.archivedDataWithRootObject(loggedMood)
    }

    class func archiveLoggedMoods(loggedMoods: [LoggedMood]?) -> NSData? {
        guard let loggedMoods = loggedMoods else {
            return nil
        }

        return NSKeyedArchiver.archivedDataWithRootObject(loggedMoods)
    }

    class func unarchiveLoggedMood(loggedMoodData: NSData?) -> LoggedMood? {
        guard let loggedMoodData = loggedMoodData else {
            return nil
        }

        return NSKeyedUnarchiver.unarchiveObjectWithData(loggedMoodData) as? LoggedMood
    }

    class func unarchiveLoggedMoods(loggedMoodsData: NSData?) -> [LoggedMood] {
        guard let loggedMoodsData = loggedMoodsData else {
            return []
        }

        guard let loggedMoods = NSKeyedUnarchiver.unarchiveObjectWithData(loggedMoodsData) as? [LoggedMood] else {
            return []
        }
        
        return loggedMoods
    }
}
