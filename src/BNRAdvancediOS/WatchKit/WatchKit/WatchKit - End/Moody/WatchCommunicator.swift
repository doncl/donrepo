// 
// Copyright (c) 2015 Big Nerd Ranch
//

import UIKit
import WatchConnectivity

class WatchCommunicator: BaseCommunicator {
    override init(loggedMoodManager: LoggedMoodManager) {
        super.init(loggedMoodManager: loggedMoodManager)

        updateWatchApplicationContext()
    }

    private func updateWatchApplicationContext() {
        var applicationContext = [String : AnyObject]()

        // Get the most recent logged moods
        let earliestDate      = NSDate().dateByAddingTimeInterval(-BaseCommunicator.RecentTimeFrame)
        let recentLoggedMoods = loggedMoodManager.loggedMoodsAfterDate(earliestDate)

        if let recentLoggedMoodsData = LoggedMood.archiveLoggedMoods(recentLoggedMoods) {
            applicationContext[BaseCommunicator.RecentLoggedMoodsUserInfoKey] = recentLoggedMoodsData
        }

        // Update the application context
        do {
            try session?.updateApplicationContext(applicationContext)
        }
        catch {
            print("Unable to update application context for watch: ", error)
        }
    }

    // MARK: - NotificationCenter
    override func loggedMoodWasAdded(notification: NSNotification) {
        super.loggedMoodWasAdded(notification)

        updateWatchApplicationContext()
    }

    override func loggedMoodsReset(notification: NSNotification) {
        super.loggedMoodsReset(notification)

        updateWatchApplicationContext()
    }
}
