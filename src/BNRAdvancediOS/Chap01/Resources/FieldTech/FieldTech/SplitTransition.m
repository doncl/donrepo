//
//  SplitTransition.m
//  FieldTech
//
//  Created by Don Clore on 2/22/16.
//  Copyright © 2016 Jonathan Blocksom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SplitTransition.h"


@implementation SplitTransition

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return 1.0;
}


-(id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                 animationControllerForOperation:(UINavigationControllerOperation)operation
                                              fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    NSLog(@"Asking for animation controller");
    return self;

}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    // Get transition context view and things
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView *fromView = fromVC.view;

    // Get the destination view controller
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *toView = toVC.view;

    UIView *inView = [transitionContext containerView];

    // Create snapshot view of the halves of the current view.
    CGRect topHalf, bottomHalf;
    CGRectDivide(fromView.bounds, &topHalf, &bottomHalf, fromView.bounds.size.height / 2.0,
            CGRectMinYEdge);

    UIView *topHalfView = [fromView resizableSnapshotViewFromRect:topHalf
                                               afterScreenUpdates:NO
                                                    withCapInsets:UIEdgeInsetsZero];

    UIView *bottomHalfView = [fromView resizableSnapshotViewFromRect:bottomHalf
                                                  afterScreenUpdates:NO
                                                       withCapInsets:UIEdgeInsetsZero];

    bottomHalfView.frame = bottomHalf;

    // Add the split views and destination view to the container.
    [inView addSubview:toView];
    [inView addSubview:topHalfView];
    [inView addSubview:bottomHalfView];

#if 0
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
         animations:^{
            topHalfView.center = CGPointMake(-topHalfView.center.x, topHalfView.center.y);
             bottomHalfView.center = CGPointMake(bottomHalfView.center.x + bottomHalf.size.width, bottomHalfView.center.y);
         } completion:^(BOOL finished){
            [topHalfView removeFromSuperview];
            [bottomHalfView removeFromSuperview];

             [transitionContext completeTransition:YES];
            }];
#else
    [UIView animateKeyframesWithDuration:1.0
                                   delay:0.0
                                 options:0
        animations:^{
            [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.5 animations:^{
                topHalfView.center = CGPointMake(0.0, topHalfView.center.y);
                bottomHalfView.center = CGPointMake(bottomHalf.size.width, bottomHalfView.center.y);
            }];


            [UIView addKeyframeWithRelativeStartTime:0.5 relativeDuration:0.5 animations:^{
                topHalfView.center = CGPointMake(0.0, inView.bounds.size.height + topHalf.size.height / 2.0);
                bottomHalfView.center = CGPointMake(bottomHalf.size.width, -bottomHalf.size.height / 2.0);
            }];
        } completion:^(BOOL finished){
                [topHalfView removeFromSuperview];
                [bottomHalfView removeFromSuperview];

                [transitionContext completeTransition:YES];
        }];
#endif
}

@end