//
//  BNRAppDelegate.m
//  FieldTech
//

#import "AppDelegate.h"

#import "SplitTransition.h"

@interface AppDelegate() <UIViewControllerAnimatedTransitioning>

@end

@implementation AppDelegate

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC
{
    SplitTransition *animController = [[SplitTransition alloc] init];
    return animController;
}

- (id<UIViewControllerAnimatedTransitioning>)tabBarController:(UITabBarController *)tabBarController
           animationControllerForTransitionFromViewController:(UIViewController *)fromVC
                                             toViewController:(UIViewController *)toVC
{
    return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    // Return how long, in seconds, the animation should take
    return 1.0;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    // Get the destination view controller
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    // Get a snapshot of the destination view and make it fully transparent
    toVC.view.frame = [[UIScreen mainScreen] bounds];
    UIView *toViewSnapshot = [toVC.view snapshotViewAfterScreenUpdates:YES];
    toViewSnapshot.alpha = 0.0;
    // Put the view in the container of the animation
    UIView *container = transitionContext.containerView;
    [container addSubview:toViewSnapshot];
    
    // Animate from the existing view to the snapshot of the new one
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                     animations:^{
                         toViewSnapshot.alpha = 1.0;
                     } completion:^(BOOL finished) {
                         [toViewSnapshot removeFromSuperview];
                         [container addSubview:toVC.view];
                         
                         [transitionContext completeTransition:YES];
                         NSLog(@"Completion block");
                     }];
}

- (void)animationEnded:(BOOL)transitionCompleted
{
    NSLog(@"Animation ended %d", transitionCompleted);
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UITabBarController *mainVC = (UITabBarController *)self.window.rootViewController;
    mainVC.delegate = self;
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
