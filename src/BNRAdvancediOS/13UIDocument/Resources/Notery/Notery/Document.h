//
//  BNRDocument.h
//  Notery
//
//  Created by Dillan Laughlin on 4/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *kDocumentContentsDidUpdateNotification;

@interface Document : UIDocument

@property (nonatomic, copy) NSString *contents;

-(id)initWithFileURL:(NSURL *)url;
@end
