//
//  BNRSingleDocumentViewController.m
//  Notery
//
//  Created by Dillan Laughlin on 1/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SingleDocumentViewController.h"
#import "Document.h"

@import JavaScriptCore;

@interface SingleDocumentViewController ()

@property (nonatomic, strong) UIPopoverController *documentPopoverController;
@property (assign, nonatomic) CGFloat keyboardHeight;
@property (nonatomic, strong) JSContext *scriptContext;

// copies the text from the view into the document
-(void)syncTextViewWithDocument;
-(void)openDocument;
-(void)closeDocument;

@end

@implementation SingleDocumentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    //    self.view.layer.borderWidth = 4.0f;
    //    self.view.layer.borderColor = [UIColor redColor].CGColor;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupScripts];

    double width = self.view.bounds.size.width;
    UIToolbar *scriptToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, 0.0, width, 44.0f)];

    NSMutableArray *scriptButtons = [NSMutableArray new];
    NSArray *scripts = [self.scriptContext[@"noteryScripts"] init];
    for (NSString *scriptName in scripts) {
        UIBarButtonItem *button =
                [[UIBarButtonItem alloc] initWithTitle:scriptName
                                                 style:UIBarButtonItemStylePlain
                                                target:self
                                                action:@selector(scriptButtonPressed:)];

        [scriptButtons addObject:button];
    }

    scriptToolbar.items = scriptButtons;
    self.textView.inputAccessoryView = scriptToolbar;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillUpdate:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillUpdate:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillUpdate:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
}

-(void)scriptButtonPressed:(id)sender
{
    NSString *scriptName = [(UIBarButtonItem *)sender title];
    NSLog(@"Script %@ pressed", scriptName);
    // Get the text we willl send in to the JS func.
    JSValue *inputText = [JSValue valueWithObject:self.textView.text inContext:self.scriptContext];

    // Look up the function in the context.
    JSValue *func = self.scriptContext[scriptName];

    // Call the function
    JSValue *result = [func callWithArguments:@[inputText]];

    // Get string result and replace text in view with it.
    NSString *newText = [result toString];
    self.textView.text = newText;
}

-(void)setupScripts
{
    NSURL *scriptPath = [[NSBundle mainBundle] URLForResource:@"scripts" withExtension:@"js"];
    
    NSError *err;
    NSString *scriptData = [NSString stringWithContentsOfURL:scriptPath
                                                    encoding:NSUTF8StringEncoding
                                                       error:&err];
    
    if (scriptData == nil) {
        NSLog(@"Error loading scripts: %@", err);
        return;
    }
    
    self.scriptContext = [JSContext new];
    [self.scriptContext evaluateScript:scriptData];
    
    NSLog(@"Script functions: %@", self.scriptContext[@"noteryScripts"]);
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.textView flashScrollIndicators];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillChangeFrameNotification
                                                  object:nil];
    // belt and suspenders
    [self syncTextViewWithDocument];
    [self.document saveToURL:[self.document fileURL] 
            forSaveOperation:UIDocumentSaveForOverwriting completionHandler:NULL];
    self.keyboardHeight = 0.0;
    
    [super viewWillDisappear:animated];
}

-(void)viewDidUnload
{
    [super viewDidUnload];
    [self setTextView:nil];
    [self setDocument:nil];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma mark - Split view

-(void)splitViewController:(UISplitViewController *)splitController
    willHideViewController:(UIViewController *)viewController
         withBarButtonItem:(UIBarButtonItem *)barButtonItem
      forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = @"Documents";
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.documentPopoverController = popoverController;
}

-(void)splitViewController:(UISplitViewController *)splitController
    willShowViewController:(UIViewController *)viewController
 invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.documentPopoverController = nil;
}

#pragma mark - Document

-(void)documentContentsDidUpdate;
{
    self.textView.text = self.document.contents;
}

-(void)documentUpdated:(NSNotification *)note
{
    [self view]; // makes sure our textView is ready
    [self documentContentsDidUpdate];
}

-(void)documentStateChanged:(NSNotification *)notification
{
    UIDocumentState documentState = self.document.documentState;
    BOOL editable = ((documentState & UIDocumentStateEditingDisabled) != 
                     UIDocumentStateEditingDisabled);
    
    self.textView.editable = editable;
}

-(void)openDocument
{
    if (self.document)
    {
        NSURL *docURL = [self.document fileURL];
        self.title = [[docURL lastPathComponent] stringByDeletingPathExtension];
        [self.textView resignFirstResponder];
        self.textView.hidden = YES;
        [self.textView.undoManager removeAllActions];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(documentStateChanged:)
                                                     name:UIDocumentStateChangedNotification
                                                   object:self.document];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(documentUpdated:)
                                                     name:kDocumentContentsDidUpdateNotification
                                                   object:self.document];
        Document *docSnapshot = [self document];
        [self.document openWithCompletionHandler: ^(BOOL success)
         {
             if ([self.document isEqual:docSnapshot])
             {
                 [self documentContentsDidUpdate];
                 [self.document setUndoManager:self.textView.undoManager];
                 [self.textView setHidden:NO];
             }
         }];
    }
}


-(void)closeDocument;
{
    if (self.document)
    {
        [self.document setUndoManager:nil];
        [self.document closeWithCompletionHandler: ^(BOOL success) {
            NSLog (@"Closed");
        }];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIDocumentStateChangedNotification
                                                      object:self.document];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:kDocumentContentsDidUpdateNotification
                                                      object:self.document];
    }
}

#pragma mark - text sync with UI

-(void)syncTextViewWithDocument
{
    self.document.contents = self.textView.text;
}

-(void)textViewDidChange:(UITextView *)textView
{
    [self syncTextViewWithDocument];
}


#pragma mark - Keyboard

@synthesize keyboardHeight = _keyboardHeight;
-(void)setKeyboardHeight:(CGFloat)value
{
    _keyboardHeight = value;
    
    CGFloat parentHeight = self.view.bounds.size.height;
    CGRect frame = self.textView.frame;
    frame.size.height = parentHeight - _keyboardHeight;
    self.textView.frame = frame;
}

-(void)keyboardWillUpdate:(NSNotification *)notification
{
    NSTimeInterval animationDuration = [[[notification userInfo] 
                        objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve animationCurve = [[[notification userInfo] 
                        objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    CGRect endFrame = [[[notification userInfo] 
                        objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    endFrame = [self.view convertRect:[[self.view window] convertRect:endFrame 
                                                           fromWindow:nil] fromView:nil];
    endFrame = CGRectIntersection(endFrame, self.view.bounds);
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    self.keyboardHeight = endFrame.size.height;
    [UIView commitAnimations];
}


#pragma mark - Accessors
@synthesize documentPopoverController = _documentPopoverController;

@synthesize textView = _textView;
-(void)setTextView:(UITextView *)textView
{
    if (_textView != textView)
    {
        _textView = textView;
        [self performSelector:@selector(documentContentsDidUpdate) withObject:nil afterDelay:0.0f];
    }
}

@synthesize document = _document;
- (void)setDocument:(Document *)document
{
    if (_document != document) 
    {
        [self closeDocument];
        _document = document;
        [self openDocument];
    }
}

@end
