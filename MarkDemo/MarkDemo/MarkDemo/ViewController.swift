//
//  ViewController.swift
//  MarkDemo
//
//  Created by Don Clore on 3/23/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPageViewControllerDataSource {

    let pageImages : [String] = ["page1.png", "page2.png", "page3.png", "landscape.png"]
    var pageViewController: UIPageViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        pageViewController = storyboard!.instantiateViewControllerWithIdentifier("PageViewController") as? UIPageViewController
        pageViewController!.dataSource = self

        let startingViewController = viewControllerAtIndex(0)!

        let viewControllers = [startingViewController]
        pageViewController!.setViewControllers(viewControllers, direction: .Forward, animated: false, completion: nil)

        pageViewController!.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height - 30)

        addChildViewController(pageViewController!)

        view.addSubview(pageViewController!.view)
        pageViewController!.didMoveToParentViewController(self)

    }

    @IBAction func startAgainButtonTouched(sender: AnyObject) {
        let startingViewController = viewControllerAtIndex(0)! as UIViewController
        let viewControllers = [startingViewController]
        pageViewController!.setViewControllers(viewControllers, direction: .Reverse, animated:false, completion: nil)
    }

    // MARK: - UIPageViewControllerDataSource
    func pageViewController(pageViewController: UIPageViewController,
      viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if let index = (viewController as? PageContentViewController)?.index {
            if index == 0 {
                return nil
            }
            var mutIdx = index
            mutIdx = mutIdx - 1

            return viewControllerAtIndex(mutIdx)
        }

        return nil
    }

    func pageViewController(pageViewController: UIPageViewController,
                            viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {

        if let index = (viewController as? PageContentViewController)?.index {
            var mutIdx = index
            mutIdx = mutIdx + 1

            return viewControllerAtIndex(mutIdx)
        }

        return nil
    }

    func viewControllerAtIndex(index: Int) -> PageContentViewController? {
        if index >= pageImages.count {
            return nil
        }

        let pageContentViewController = storyboard!.instantiateViewControllerWithIdentifier("PageContentViewController") as? PageContentViewController
        pageContentViewController!.imageFile = pageImages[index]
        pageContentViewController!.index = index

        return pageContentViewController
    }

    @available(iOS 6.0, *) func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return pageImages.count
    }

    @available(iOS 6.0, *) func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }

}

