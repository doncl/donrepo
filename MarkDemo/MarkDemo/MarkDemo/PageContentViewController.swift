//
//  PageContentViewController.swift
//  MarkDemo
//
//  Created by Don Clore on 3/23/16.
//  Copyright © 2016 Don Clore. All rights reserved.
//

import UIKit
import QuartzCore

class PageContentViewController: UIViewController {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var imageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet var imageViewHeightConstraint: NSLayoutConstraint!

    var index: Int = 0
    var imageFile: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        //imageView.frame = CGRect(x: 0, y: 194, width: 375, height: 280)
        //imageView.center = view.center

        if let imageFile = imageFile {
            imageView.image = UIImage(named: imageFile)
            imageView.hidden = false

            let pinch = UIPinchGestureRecognizer(target: self, action: "pinch:")

            imageView.addGestureRecognizer(pinch)
        }
    }


    func pinch(pinchy: UIPinchGestureRecognizer) {
        let currentScale = pinchy.view!.layer.valueForKeyPath("transform.scale.x") as! CGFloat

        if pinchy.state == .Began || pinchy.state == .Changed {

            let minScale: CGFloat = 0.6
            let maxScale: CGFloat = 1.0
            let zoomSpeed: CGFloat = 0.5

            var deltaScale = pinchy.scale

            if deltaScale > 0.01 {
                let inverse: CGFloat = 1.0 / deltaScale
                //imageView.transform = CGAffineTransformScale(imageView.transform,
                //inverse, inverse)


                imageViewWidthConstraint.constant *= deltaScale

                imageViewHeightConstraint.constant *= deltaScale
            }

            pinchy.scale = 1

        }
    }
}

