//
//  DayHeader.swift
//  SchedulerPOC
//
//  Created by Don Clore on 11/21/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import UIKit

class DayHeader: UIView {
  weak var tabStopsProvider: ScheduleTabStopsProvider?
  let vertPad: CGFloat = 25.0
  #if targetEnvironment(macCatalyst)
  let dayLabelFontSize: CGFloat = 33.0
  let columnHeaderLabelSize: CGFloat = 18.0
  #else
  let dayLabelFontSize: CGFloat = 22.0
  let columnHeaderLabelSize: CGFloat = 12.0
  #endif
  let labelHeight: CGFloat = 15.0
  let textColor: UIColor = UIColor(white: 104 / 244, alpha: 0.9)
  
  let dayLabel: UILabel = UILabel()
  let awayLabel: UILabel = UILabel()
  let homeLabel: UILabel = UILabel()
  let timeLabel: UILabel = UILabel()
  let infoLabel: UILabel = UILabel()
  
  var awayX: NSLayoutConstraint!
  var homeX: NSLayoutConstraint!
  var timeX: NSLayoutConstraint!
  var infoX: NSLayoutConstraint!
  
  init(day: ScheduleModel.Day) {
    super.init(frame: .zero)
    backgroundColor = .white
    dayLabel.text = day.dateString()
    dayLabel.translatesAutoresizingMaskIntoConstraints = false
    addSubview(dayLabel)
    dayLabel.font = UIFont(name: "Nunito-Light", size: dayLabelFontSize)
    dayLabel.textAlignment = .center
    dayLabel.textColor = textColor
        
    [awayLabel, homeLabel, timeLabel, infoLabel].forEach({
      $0.translatesAutoresizingMaskIntoConstraints = false
      addSubview($0)
      $0.topAnchor.constraint(equalTo: dayLabel.bottomAnchor, constant: vertPad).isActive = true
      $0.heightAnchor.constraint(equalToConstant: labelHeight).isActive = true
      $0.widthAnchor.constraint(equalToConstant: MatchCell.stackWidth).isActive = true
      $0.font = UIFont(name: "Nunito-Medium", size: columnHeaderLabelSize)
      $0.textAlignment = .left
      $0.textColor = textColor
    })
    
    awayLabel.text = "AWAY"
    homeLabel.text = "HOME"
    timeLabel.text = "TIME"
    infoLabel.text = "INFO"
   
    awayX = awayLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 0)
    homeX = homeLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 0)
    timeX = timeLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 0)
    infoX = infoLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 0)
    
    NSLayoutConstraint.activate(constraints: [
      dayLabel.topAnchor.constraint(equalTo: topAnchor, constant: vertPad),
      dayLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
      
      awayX, homeX, timeX, infoX,
      
    ], andSetPriority: UILayoutPriority(rawValue: 999))
    
    
    dayLabel.sizeToFit()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    if let tabStopsProvider = tabStopsProvider {
      let tabs = tabStopsProvider.getTabStops()
      awayX.constant = tabs.awayX
      homeX.constant = tabs.homeX
      timeX.constant = tabs.timeX
      infoX.constant = tabs.infoX
    }
    [awayLabel, homeLabel, timeLabel, infoLabel].forEach({ $0.setNeedsLayout()})
    super.layoutSubviews()
  }
  
}
