//
//  DayFooter.swift
//  SchedulerPOC
//
//  Created by Don Clore on 11/21/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import UIKit

class DayFooter: UIView {
  #if targetEnvironment(macCatalyst)
  let fontSize: CGFloat = 21.0
  #else
  let fontSize: CGFloat = 21.0
  #endif
  let label: UILabel = UILabel()
  init() {
    super.init(frame: .zero)
    backgroundColor = .white
    label.text = "ALL TIMES IN EASTERN STANDARD"
    label.translatesAutoresizingMaskIntoConstraints = false
    addSubview(label)
    label.font = UIFont(name: "Nunito-ExtraLight", size: fontSize)
    label.textAlignment = .center
    label.textColor = UIColor(white: 104 / 244, alpha: 0.9)
    
    NSLayoutConstraint.activate(constraints: [
      label.centerYAnchor.constraint(equalTo: centerYAnchor),
      label.leftAnchor.constraint(equalTo: leftAnchor, constant: SchedulerHeader.Constants.borderPad),
    ], andSetPriority: UILayoutPriority(rawValue: 999))
    
    label.sizeToFit()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
