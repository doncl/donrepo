//
//  MatchCell.swift
//  SchedulerPOC
//
//  Created by Don Clore on 11/20/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import UIKit

class MatchCell: UITableViewCell {
  static let id: String = "MatchCellReuseIdentifier"
  #if targetEnvironment(macCatalyst)
  static let height: CGFloat = 66.0
  static let stackWidth: CGFloat = 150.0
  static let logoDim: CGFloat = 42.0
  static let timeFontSize: CGFloat = 21.0
  #else
  static let height: CGFloat = 44.0
  static let stackWidth: CGFloat = 100.0
  static let logoDim: CGFloat = 28
  static let timeFontSize: CGFloat = 14.0
  #endif
  
  weak var tabStopsProvider: ScheduleTabStopsProvider?
  
  class TeamStack: UIStackView {
    #if targetEnvironment(macCatalyst)
    static let fontSize: CGFloat = 21.0
    static let spacing: CGFloat = 18.0
    #else
    static let fontSize: CGFloat = 14.0
    static let spacing: CGFloat = 12.0
    #endif
    
    let logo: UIImageView = UIImageView()
    let abbrev: UILabel = UILabel()
    
    init(teamAbbreviation: TeamAbbreviation) {
      logo.contentMode = .scaleAspectFit
      logo.image = teamAbbreviation.team.image
      abbrev.text = teamAbbreviation.team.abbrev
      abbrev.font = UIFont(name: "Nunito-Bold", size: TeamStack.fontSize)
      super.init(frame: .zero)
      logo.translatesAutoresizingMaskIntoConstraints = false
      NSLayoutConstraint.activate(constraints: [
        logo.widthAnchor.constraint(equalToConstant: MatchCell.logoDim),
        logo.heightAnchor.constraint(equalToConstant: MatchCell.logoDim),
      ], andSetPriority: UILayoutPriority(rawValue: 999))
      addArrangedSubview(logo)
      addArrangedSubview(abbrev)
      axis = .horizontal
      spacing = TeamStack.spacing
    }
    
    required init(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }
  }
  
  var match: Match = Match(DateComponents(year: 2019, month: 12, day: 25, hour: 13), away: .det, home: .was) {
    didSet {
      away.logo.image = self.match.away.team.image
      away.abbrev.text = self.match.away.team.abbrev
      home.logo.image = self.match.home.team.image
      home.abbrev.text = self.match.home.team.abbrev
      setTimeLabel(match.date)
      away.setNeedsDisplay()
      home.setNeedsDisplay()
      
      // TODO peel off time and stick in the time label.
    }
  }
  
  
  let away: TeamStack = TeamStack(teamAbbreviation: .det)
  let home: TeamStack = TeamStack(teamAbbreviation: .was)
  let time: UILabel = UILabel()

  var awayX: NSLayoutConstraint!
  var homeX: NSLayoutConstraint!
  var timeX: NSLayoutConstraint!
    
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = UITableViewCell.SelectionStyle.none
    accessibilityIdentifier = "matchCell"
    contentView.accessibilityIdentifier = "matchContentView"
    
    time.text = "1:00 pm"
    
    away.translatesAutoresizingMaskIntoConstraints = false
    home.translatesAutoresizingMaskIntoConstraints = false
    time.translatesAutoresizingMaskIntoConstraints = false
    
    contentView.addSubview(away)
    contentView.addSubview(home)
    contentView.addSubview(time)
    
    awayX = away.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: SchedulerHeader.Constants.borderPad)
    homeX = home.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 150)
    timeX = time.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 300)
    
    NSLayoutConstraint.activate(constraints: [
      awayX,
      homeX,
      timeX,
      
      away.widthAnchor.constraint(equalToConstant: MatchCell.stackWidth),
      home.widthAnchor.constraint(equalToConstant: MatchCell.stackWidth),
      time.widthAnchor.constraint(equalToConstant: MatchCell.stackWidth),
      
      away.heightAnchor.constraint(equalToConstant: MatchCell.height),
      home.heightAnchor.constraint(equalToConstant: MatchCell.height),
      time.heightAnchor.constraint(equalToConstant: MatchCell.height),
      
      away.topAnchor.constraint(equalTo: contentView.topAnchor),
      home.topAnchor.constraint(equalTo: contentView.topAnchor),
      time.topAnchor.constraint(equalTo: contentView.topAnchor),
            
    ], andSetPriority: UILayoutPriority(rawValue: 999))
    
    time.font = UIFont(name: "Nunito-Regular", size: MatchCell.timeFontSize)
    time.textColor = .black
    time.textAlignment = .left
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    if let tabStopsProvider = tabStopsProvider {
      let tabs = tabStopsProvider.getTabStops()
      awayX.constant = tabs.awayX
      homeX.constant = tabs.homeX
      timeX.constant = tabs.timeX
      //TODO: Info, what the heck is this?  It's in comps, but no explanation.
    }
    super.layoutSubviews()
  }
  
  private func setTimeLabel(_ date: Date) {
    let df = DateFormatter()
    df.locale = Locale.current
    df.dateFormat = "h:mm a"
    let s = df.string(from: date)
    time.text = s
  }
}


extension NSLayoutConstraint {
  class func activate(constraints: [NSLayoutConstraint], andSetPriority priority: UILayoutPriority) {
    constraints.forEach {
      $0.priority = priority
      $0.isActive = true
    }
  }
}

