//
//  ScheduleModel.swift
//  SchedulerPOC
//
//  Created by Don Clore on 11/21/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import Foundation

struct ScheduleModel {
  struct Day {
    let date: Date
    let matches: [Match]
    
    func dateString() -> String {
      let df: DateFormatter = DateFormatter()
      df.dateStyle = DateFormatter.Style.full
      return df.string(from: date).uppercased()
    }
    
    init(_ dateComponents: DateComponents, matches: [Match] ) {
      date = Calendar.current.date(from: dateComponents)!
      self.matches = matches
    }
  }
  struct Week {
    let days: [Day]
  }
  
  var weeks: [Week]
  
  static let fauxSchedule: ScheduleModel = ScheduleModel(weeks: [
    // Week1
    Week(days: [
      Day(DateComponents(year: 2019, month: 9, day: 5), matches: [
        Match(DateComponents(year: 2019, month: 9, day: 5, hour: 20, minute: 20), away: .gb, home: .chi),
      ]),
      Day(DateComponents(year: 2019, month: 9, day: 8), matches: [
        Match(DateComponents(year: 2019, month: 9, day: 8, hour: 13), away: .lar, home: .car),
        Match(DateComponents(year: 2019, month: 9, day: 8, hour: 13), away: .was, home: .phi),
        Match(DateComponents(year: 2019, month: 9, day: 8, hour: 14), away: .buf, home: .nyj),
        Match(DateComponents(year: 2019, month: 9, day: 8, hour: 13), away: .atl, home: .min),
        Match(DateComponents(year: 2019, month: 9, day: 8, hour: 17), away: .bal, home: .mia),
        Match(DateComponents(year: 2019, month: 9, day: 8, hour: 14), away: .kc, home: .jax),
        
        Match(DateComponents(year: 2019, month: 9, day: 8, hour: 13), away: .ten, home: .cle),
        Match(DateComponents(year: 2019, month: 9, day: 8, hour: 14), away: .ind, home: .lac),
        Match(DateComponents(year: 2019, month: 9, day: 8, hour: 13), away: .cin, home: .sea),
        Match(DateComponents(year: 2019, month: 9, day: 8, hour: 17), away: .sf, home: .tb),
        Match(DateComponents(year: 2019, month: 9, day: 8, hour: 14), away: .nyg, home: .dal),
        
        Match(DateComponents(year: 2019, month: 9, day: 8, hour: 13), away: .det, home: .ari),
        Match(DateComponents(year: 2019, month: 9, day: 8, hour: 14), away: .pit, home: .ne),
      ]),
      Day(DateComponents(year: 2019, month: 9, day: 9), matches: [
        Match(DateComponents(year: 2019, month: 9, day: 9, hour: 20), away: .hou, home: .no),
        Match(DateComponents(year: 2019, month: 9, day: 9, hour: 21), away: .den, home: .oak),
      ])
    ]),
    // Week 2
    Week(days: [
      Day(DateComponents(year: 2019, month: 9, day: 12), matches: [
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 20), away: .tb, home: .car),
      ]),
      Day(DateComponents(year: 2019, month: 9, day: 12), matches: [
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 14), away: .sf, home: .cin),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 15), away: .lac, home: .det),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .min, home: .gb),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .ind, home: .ten),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .ne, home: .mia),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .buf, home: .nyg),
        // ... more
      ])

    ]),
    // Week 3
    Week(days: [
      Day(DateComponents(year: 2019, month: 9, day: 19), matches: [
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 20), away: .tb, home: .car),
      ]),
      Day(DateComponents(year: 2019, month: 9, day: 22), matches: [
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 14), away: .cin, home: .buf),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 15), away: .det, home: .phi),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .nyj, home: .ne),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .oak, home: .min),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .bal, home: .kc),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .atl, home: .ind),
        
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .den, home: .gb),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .mia, home: .dal),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .nyg, home: .tb),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .car, home: .ari),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .pit, home: .sf),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .no, home: .sea),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .hou, home: .lac),
        Match(DateComponents(year: 2019, month: 9, day: 12, hour: 13), away: .lar, home: .cle),
      ]),
      Day(DateComponents(year: 2019, month: 9, day: 23), matches: [
        Match(DateComponents(year: 2019, month: 9, day: 23, hour: 20), away: .chi, home: .was),
      ])
    ]),
    
    // Week 4
    Week(days: [
      Day(DateComponents(year: 2019, month: 9, day: 26), matches: [
        Match(DateComponents(year: 2019, month: 9, day: 26, hour: 20), away: .phi, home: .gb),
      ]),
      Day(DateComponents(year: 2019, month: 9, day: 29), matches: [
        Match(DateComponents(year: 2019, month: 9, day: 29, hour: 14), away: .ten, home: .atl),
        Match(DateComponents(year: 2019, month: 9, day: 29, hour: 15), away: .was, home: .nyg),
        Match(DateComponents(year: 2019, month: 9, day: 29, hour: 13), away: .lac, home: .mia),
        Match(DateComponents(year: 2019, month: 9, day: 29, hour: 13), away: .oak, home: .ind),
        Match(DateComponents(year: 2019, month: 9, day: 29, hour: 13), away: .car, home: .hou),
        Match(DateComponents(year: 2019, month: 9, day: 29, hour: 13), away: .kc, home: .det),
        
        Match(DateComponents(year: 2019, month: 9, day: 29, hour: 13), away: .cle, home: .bal),
        Match(DateComponents(year: 2019, month: 9, day: 29, hour: 13), away: .ne, home: .buf),
        Match(DateComponents(year: 2019, month: 9, day: 29, hour: 13), away: .tb, home: .lar),
        Match(DateComponents(year: 2019, month: 9, day: 29, hour: 13), away: .sea, home: .ari),
        Match(DateComponents(year: 2019, month: 9, day: 29, hour: 13), away: .min, home: .chi),
        Match(DateComponents(year: 2019, month: 9, day: 29, hour: 13), away: .jax, home: .den),
        Match(DateComponents(year: 2019, month: 9, day: 29, hour: 13), away: .dal, home: .no),
      ]),
      Day(DateComponents(year: 2019, month: 9, day: 30), matches: [
        Match(DateComponents(year: 2019, month: 9, day: 30, hour: 20), away: .cin, home: .pit),
      ])
    ]),

    
    // Week 5
    Week(days: [
      Day(DateComponents(year: 2019, month: 10, day: 3), matches: [
        Match(DateComponents(year: 2019, month: 10, day: 3, hour: 20), away: .lar, home: .sea),
      ]),
      Day(DateComponents(year: 2019, month: 10, day: 6), matches: [
        Match(DateComponents(year: 2019, month: 10, day: 6, hour: 14), away: .jax, home: .car),
        Match(DateComponents(year: 2019, month: 10, day: 6, hour: 15), away: .ne, home: .was),
        Match(DateComponents(year: 2019, month: 10, day: 6, hour: 13), away: .buf, home: .ten),
        Match(DateComponents(year: 2019, month: 10, day: 6, hour: 13), away: .bal, home: .pit),
        Match(DateComponents(year: 2019, month: 10, day: 6, hour: 13), away: .ari, home: .cin),
        Match(DateComponents(year: 2019, month: 10, day: 6, hour: 13), away: .atl, home: .hou),
        
        Match(DateComponents(year: 2019, month: 10, day: 6, hour: 13), away: .tb, home: .no),
        Match(DateComponents(year: 2019, month: 10, day: 6, hour: 13), away: .min, home: .nyg),
        Match(DateComponents(year: 2019, month: 10, day: 6, hour: 13), away: .chi, home: .oak),
        Match(DateComponents(year: 2019, month: 10, day: 6, hour: 13), away: .nyj, home: .phi),
        Match(DateComponents(year: 2019, month: 10, day: 6, hour: 13), away: .den, home: .lac),
        Match(DateComponents(year: 2019, month: 10, day: 6, hour: 13), away: .gb, home: .dal),
        Match(DateComponents(year: 2019, month: 10, day: 6, hour: 13), away: .ind, home: .kc),
      ]),
      Day(DateComponents(year: 2019, month: 10, day: 7), matches: [
        Match(DateComponents(year: 2019, month: 10, day: 7, hour: 20), away: .cle, home: .sf),
      ])
    ]),

    // Week 6
    Week(days: [
      Day(DateComponents(year: 2019, month: 10, day: 10), matches: [
        Match(DateComponents(year: 2019, month: 10, day: 10, hour: 20), away: .nyg, home: .ne),
      ]),
      Day(DateComponents(year: 2019, month: 10, day: 6), matches: [
        Match(DateComponents(year: 2019, month: 10, day: 13, hour: 14), away: .car, home: .tb),
        Match(DateComponents(year: 2019, month: 10, day: 13, hour: 15), away: .was, home: .mia),
        Match(DateComponents(year: 2019, month: 10, day: 13, hour: 13), away: .phi, home: .min),
        Match(DateComponents(year: 2019, month: 10, day: 13, hour: 13), away: .hou, home: .kc),
        Match(DateComponents(year: 2019, month: 10, day: 13, hour: 13), away: .no, home: .jax),
        Match(DateComponents(year: 2019, month: 10, day: 13, hour: 13), away: .sea, home: .cle),
        
        Match(DateComponents(year: 2019, month: 10, day: 13, hour: 13), away: .cin, home: .bal),
        Match(DateComponents(year: 2019, month: 10, day: 13, hour: 13), away: .sf, home: .lar),
        Match(DateComponents(year: 2019, month: 10, day: 13, hour: 13), away: .atl, home: .ari),
        Match(DateComponents(year: 2019, month: 10, day: 13, hour: 13), away: .dal, home: .nyj),
        Match(DateComponents(year: 2019, month: 10, day: 13, hour: 13), away: .ten, home: .den),
        Match(DateComponents(year: 2019, month: 10, day: 13, hour: 13), away: .pit, home: .lac),
      ]),
      Day(DateComponents(year: 2019, month: 10, day: 14), matches: [
        Match(DateComponents(year: 2019, month: 10, day: 14, hour: 20), away: .det, home: .gb),
      ])
    ]),

    // Week 7
    Week(days: [
      Day(DateComponents(year: 2019, month: 10, day: 17), matches: [
        Match(DateComponents(year: 2019, month: 10, day: 17, hour: 20), away: .kc, home: .den),
      ]),
      Day(DateComponents(year: 2019, month: 10, day: 6), matches: [
        Match(DateComponents(year: 2019, month: 10, day: 20, hour: 14), away: .mia, home: .buf),
        Match(DateComponents(year: 2019, month: 10, day: 20, hour: 15), away: .jax, home: .cin),
        Match(DateComponents(year: 2019, month: 10, day: 20, hour: 13), away: .min, home: .det),
        Match(DateComponents(year: 2019, month: 10, day: 20, hour: 13), away: .oak, home: .gb),
        Match(DateComponents(year: 2019, month: 10, day: 20, hour: 13), away: .lar, home: .atl),
        Match(DateComponents(year: 2019, month: 10, day: 20, hour: 13), away: .hou, home: .ind),
        
        Match(DateComponents(year: 2019, month: 10, day: 20, hour: 13), away: .sf, home: .was),
        Match(DateComponents(year: 2019, month: 10, day: 20, hour: 13), away: .ari, home: .nyg),
        Match(DateComponents(year: 2019, month: 10, day: 20, hour: 13), away: .lac, home: .ten),
        Match(DateComponents(year: 2019, month: 10, day: 20, hour: 13), away: .no, home: .chi),
        Match(DateComponents(year: 2019, month: 10, day: 20, hour: 13), away: .bal, home: .sea),
        Match(DateComponents(year: 2019, month: 10, day: 20, hour: 13), away: .phi, home: .dal),
      ]),
      Day(DateComponents(year: 2019, month: 10, day: 21), matches: [
        Match(DateComponents(year: 2019, month: 10, day: 21, hour: 20), away: .ne, home: .nyj),
      ])
    ]),
    
    // Week 8
    Week(days: [
      Day(DateComponents(year: 2019, month: 10, day: 24), matches: [
        Match(DateComponents(year: 2019, month: 10, day: 24, hour: 20), away: .was, home: .min),
      ]),
      Day(DateComponents(year: 2019, month: 10, day: 6), matches: [
        Match(DateComponents(year: 2019, month: 10, day: 27, hour: 14), away: .sea, home: .atl),
        Match(DateComponents(year: 2019, month: 10, day: 27, hour: 15), away: .den, home: .ind),
        Match(DateComponents(year: 2019, month: 10, day: 27, hour: 13), away: .tb, home: .ten),
        Match(DateComponents(year: 2019, month: 10, day: 27, hour: 13), away: .ari, home: .no),
        Match(DateComponents(year: 2019, month: 10, day: 27, hour: 13), away: .cin, home: .lar),
        Match(DateComponents(year: 2019, month: 10, day: 27, hour: 13), away: .phi, home: .buf),
        
        Match(DateComponents(year: 2019, month: 10, day: 27, hour: 13), away: .lac, home: .chi),
        Match(DateComponents(year: 2019, month: 10, day: 27, hour: 13), away: .nyg, home: .det),
        Match(DateComponents(year: 2019, month: 10, day: 27, hour: 13), away: .nyj, home: .jax),
        Match(DateComponents(year: 2019, month: 10, day: 27, hour: 13), away: .car, home: .sf),
        Match(DateComponents(year: 2019, month: 10, day: 27, hour: 13), away: .oak, home: .hou),
        Match(DateComponents(year: 2019, month: 10, day: 27, hour: 13), away: .cle, home: .ne),
        Match(DateComponents(year: 2019, month: 10, day: 27, hour: 13), away: .gb, home: .kc),
      ]),
      Day(DateComponents(year: 2019, month: 10, day: 28), matches: [
        Match(DateComponents(year: 2019, month: 10, day: 28, hour: 20), away: .mia, home: .pit),
      ])
    ]),

    // Week 9
    Week(days: [
      Day(DateComponents(year: 2019, month: 10, day: 31), matches: [
        Match(DateComponents(year: 2019, month: 10, day: 31, hour: 20), away: .sf, home: .ari),
      ]),
      Day(DateComponents(year: 2019, month: 11, day: 3), matches: [
        Match(DateComponents(year: 2019, month: 11, day: 3, hour: 14), away: .hou, home: .jax),
        Match(DateComponents(year: 2019, month: 11, day: 3, hour: 15), away: .chi, home: .phi),
        Match(DateComponents(year: 2019, month: 11, day: 3, hour: 13), away: .ind, home: .pit),
        Match(DateComponents(year: 2019, month: 11, day: 3, hour: 13), away: .nyj, home: .mia),
        Match(DateComponents(year: 2019, month: 11, day: 3, hour: 13), away: .min, home: .kc),
        Match(DateComponents(year: 2019, month: 11, day: 3, hour: 13), away: .ten, home: .car),
        
        Match(DateComponents(year: 2019, month: 11, day: 3, hour: 13), away: .was, home: .buf),
        Match(DateComponents(year: 2019, month: 11, day: 3, hour: 13), away: .tb, home: .sea),
        Match(DateComponents(year: 2019, month: 11, day: 3, hour: 13), away: .det, home: .oak),
        Match(DateComponents(year: 2019, month: 11, day: 3, hour: 13), away: .gb, home: .lac),
        Match(DateComponents(year: 2019, month: 11, day: 3, hour: 13), away: .cle, home: .den),
        Match(DateComponents(year: 2019, month: 11, day: 3, hour: 13), away: .ne, home: .bal),
      ]),
      Day(DateComponents(year: 2019, month: 11, day: 4), matches: [
        Match(DateComponents(year: 2019, month: 11, day: 4, hour: 20), away: .dal, home: .nyg),
      ])
    ]),

    // Week 10
    Week(days: [
      Day(DateComponents(year: 2019, month: 11, day: 7), matches: [
        Match(DateComponents(year: 2019, month: 11, day: 7, hour: 20), away: .lac, home: .oak),
      ]),
      Day(DateComponents(year: 2019, month: 11, day: 10), matches: [
        Match(DateComponents(year: 2019, month: 11, day: 10, hour: 14), away: .bal, home: .cin),
        Match(DateComponents(year: 2019, month: 11, day: 10, hour: 15), away: .buf, home: .cle),
        Match(DateComponents(year: 2019, month: 11, day: 10, hour: 13), away: .det, home: .chi),
        Match(DateComponents(year: 2019, month: 11, day: 10, hour: 13), away: .nyg, home: .nyj),
        Match(DateComponents(year: 2019, month: 11, day: 10, hour: 13), away: .kc, home: .ten),
        Match(DateComponents(year: 2019, month: 11, day: 10, hour: 13), away: .ari, home: .tb),
        
        Match(DateComponents(year: 2019, month: 11, day: 10, hour: 13), away: .atl, home: .no),
        Match(DateComponents(year: 2019, month: 11, day: 10, hour: 13), away: .mia, home: .ind),
        Match(DateComponents(year: 2019, month: 11, day: 10, hour: 13), away: .lar, home: .pit),
        Match(DateComponents(year: 2019, month: 11, day: 10, hour: 13), away: .car, home: .gb),
        Match(DateComponents(year: 2019, month: 11, day: 10, hour: 13), away: .min, home: .dal),
      ]),
      Day(DateComponents(year: 2019, month: 11, day: 11), matches: [
        Match(DateComponents(year: 2019, month: 11, day: 11, hour: 20), away: .sea, home: .phi),
      ])
    ]),

    
    // Week 11
    Week(days: [
      Day(DateComponents(year: 2019, month: 11, day: 14), matches: [
        Match(DateComponents(year: 2019, month: 11, day: 14, hour: 20), away: .pit, home: .cle),
      ]),
      Day(DateComponents(year: 2019, month: 11, day: 17), matches: [
        Match(DateComponents(year: 2019, month: 11, day: 17, hour: 14), away: .atl, home: .car),
        Match(DateComponents(year: 2019, month: 11, day: 17, hour: 15), away: .dal, home: .det),
        Match(DateComponents(year: 2019, month: 11, day: 17, hour: 13), away: .jax, home: .ind),
        Match(DateComponents(year: 2019, month: 11, day: 17, hour: 13), away: .buf, home: .mia),
        Match(DateComponents(year: 2019, month: 11, day: 17, hour: 13), away: .hou, home: .bal),
        Match(DateComponents(year: 2019, month: 11, day: 17, hour: 13), away: .den, home: .min),
        
        Match(DateComponents(year: 2019, month: 11, day: 17, hour: 13), away: .nyj, home: .was),
        Match(DateComponents(year: 2019, month: 11, day: 17, hour: 13), away: .no, home: .tb),
        Match(DateComponents(year: 2019, month: 11, day: 17, hour: 13), away: .ari, home: .sf),
        Match(DateComponents(year: 2019, month: 11, day: 17, hour: 13), away: .cin, home: .oak),
        Match(DateComponents(year: 2019, month: 11, day: 17, hour: 13), away: .ne, home: .phi),
        Match(DateComponents(year: 2019, month: 11, day: 17, hour: 13), away: .chi, home: .lar),
      ]),
      Day(DateComponents(year: 2019, month: 11, day: 18), matches: [
        Match(DateComponents(year: 2019, month: 11, day: 18, hour: 20), away: .kc, home: .lac),
      ])
    ]),

    // Week 12
    Week(days: [
      Day(DateComponents(year: 2019, month: 11, day: 21), matches: [
        Match(DateComponents(year: 2019, month: 11, day: 21, hour: 20, minute: 20), away: .ind, home: .hou),
      ]),
      Day(DateComponents(year: 2019, month: 11, day: 24), matches: [
        Match(DateComponents(year: 2019, month: 11, day: 24, hour: 13), away: .den, home: .buf),
        Match(DateComponents(year: 2019, month: 11, day: 24, hour: 13), away: .nyg, home: .chi),
        Match(DateComponents(year: 2019, month: 11, day: 24, hour: 13), away: .pit, home: .cin),
        Match(DateComponents(year: 2019, month: 11, day: 24, hour: 13), away: .mia, home: .cle),
        Match(DateComponents(year: 2019, month: 11, day: 24, hour: 13), away: .tb, home: .atl),
        Match(DateComponents(year: 2019, month: 11, day: 24, hour: 13), away: .car, home: .no),
        
        Match(DateComponents(year: 2019, month: 11, day: 24, hour: 13), away: .sea, home: .phi),
        Match(DateComponents(year: 2019, month: 11, day: 24, hour: 13), away: .det, home: .was),
        Match(DateComponents(year: 2019, month: 11, day: 24, hour: 13), away: .oak, home: .nyj),
        Match(DateComponents(year: 2019, month: 11, day: 24, hour: 16, minute: 5), away: .jax, home: .ten),
        Match(DateComponents(year: 2019, month: 11, day: 24, hour: 16, minute: 25), away: .dal, home: .ne),
        Match(DateComponents(year: 2019, month: 11, day: 24, hour: 20, minute: 20), away: .gb, home: .sf),
      ]),
      Day(DateComponents(year: 2019, month: 11, day: 25), matches: [
        Match(DateComponents(year: 2019, month: 11, day: 25, hour: 20, minute: 15), away: .bal, home: .lar),
      ])
    ]),

    // Week 13
    Week(days: [
      Day(DateComponents(year: 2019, month: 11, day: 28), matches: [
        Match(DateComponents(year: 2019, month: 11, day: 28, hour: 12, minute: 30), away: .chi, home: .det),
        Match(DateComponents(year: 2019, month: 11, day: 28, hour: 16, minute: 30), away: .buf, home: .dal),
        Match(DateComponents(year: 2019, month: 11, day: 28, hour: 20, minute: 20), away: .no, home: .atl),
      ]),
      Day(DateComponents(year: 2019, month: 12, day: 1), matches: [
        Match(DateComponents(year: 2019, month: 12, day: 1, hour: 13), away: .ten, home: .ind),
        Match(DateComponents(year: 2019, month: 12, day: 1, hour: 13), away: .nyj, home: .cin),
        Match(DateComponents(year: 2019, month: 12, day: 1, hour: 13), away: .was, home: .car),
        Match(DateComponents(year: 2019, month: 12, day: 1, hour: 13), away: .sf, home: .bal),
        Match(DateComponents(year: 2019, month: 12, day: 1, hour: 13), away: .tb, home: .jax),
        Match(DateComponents(year: 2019, month: 12, day: 1, hour: 13), away: .cle, home: .pit),
        
        Match(DateComponents(year: 2019, month: 12, day: 1, hour: 13), away: .gb, home: .nyg),
        Match(DateComponents(year: 2019, month: 12, day: 1, hour: 13), away: .phi, home: .mia),
        Match(DateComponents(year: 2019, month: 12, day: 1, hour: 16, minute: 5), away: .lar, home: .ari),
        Match(DateComponents(year: 2019, month: 12, day: 1, hour: 16, minute: 25), away: .lac, home: .den),
        Match(DateComponents(year: 2019, month: 12, day: 1, hour: 16, minute: 25), away: .oak, home: .kc),
        Match(DateComponents(year: 2019, month: 12, day: 1, hour: 20, minute: 20), away: .ne, home: .hou),
      ]),
      Day(DateComponents(year: 2019, month: 12, day: 2), matches: [
        Match(DateComponents(year: 2019, month: 12, day: 2, hour: 20, minute: 15), away: .min, home: .sea),
      ])
    ]),

    // Week 14
    Week(days: [
      Day(DateComponents(year: 2019, month: 12, day: 5), matches: [
        Match(DateComponents(year: 2019, month: 12, day: 5, hour: 20, minute: 20), away: .dal, home: .chi),
      ]),
      Day(DateComponents(year: 2019, month: 12, day: 8), matches: [
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 13), away: .car, home: .atl),
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 13), away: .ind, home: .tb),
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 13), away: .mia, home: .nyj),
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 13), away: .sf, home: .no),
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 13), away: .det, home: .min),
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 13), away: .den, home: .hou),
        
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 13), away: .bal, home: .buf),
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 13), away: .cin, home: .cle),
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 13), away: .was, home: .gb),
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 16, minute: 5), away: .lac, home: .jax),
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 16, minute: 25), away: .pit, home: .ari),
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 16, minute: 25), away: .ten, home: .oak),
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 16, minute: 25), away: .kc, home: .ne),
        Match(DateComponents(year: 2019, month: 11, day: 8, hour: 20, minute: 20), away: .sea, home: .lar),
      ]),
      Day(DateComponents(year: 2019, month: 12, day: 9), matches: [
        Match(DateComponents(year: 2019, month: 13, day: 9, hour: 20, minute: 15), away: .nyg, home: .phi),
      ])
    ]),

    // Week 15
    Week(days: [
      Day(DateComponents(year: 2019, month: 12, day: 12), matches: [
        Match(DateComponents(year: 2019, month: 12, day: 12, hour: 20, minute: 20), away: .nyj, home: .bal),
      ]),
      Day(DateComponents(year: 2019, month: 12, day: 15), matches: [
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 13), away: .sea, home: .car),
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 13), away: .phi, home: .was),
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 13), away: .hou, home: .ten),
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 13), away: .buf, home: .pit),
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 13), away: .mia, home: .nyg),
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 13), away: .den, home: .kc),
        
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 13), away: .ne, home: .cin),
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 13), away: .tb, home: .det),
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 13), away: .chi, home: .gb),
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 16, minute: 5), away: .jax, home: .oak),
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 16, minute: 5), away: .cle, home: .ari),
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 16, minute: 25), away: .atl, home: .sf),
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 16, minute: 25), away: .lar, home: .dal),
        Match(DateComponents(year: 2019, month: 12, day: 15, hour: 20, minute: 20), away: .min, home: .lac),
      ]),
      Day(DateComponents(year: 2019, month: 12, day: 16), matches: [
        Match(DateComponents(year: 2019, month: 12, day: 16, hour: 20, minute: 15), away: .ind, home: .no),
      ])
    ]),

    // Week 16
    Week(days: [
      Day(DateComponents(year: 2019, month: 12, day: 21), matches: [
        Match(DateComponents(year: 2019, month: 12, day: 21, hour: 13), away: .hou, home: .tb),
        Match(DateComponents(year: 2019, month: 12, day: 21, hour: 16, minute: 30), away: .buf, home: .ne),
        Match(DateComponents(year: 2019, month: 12, day: 21, hour: 20, minute: 15), away: .lar, home: .sf),
      ]),
      Day(DateComponents(year: 2019, month: 12, day: 22), matches: [
        Match(DateComponents(year: 2019, month: 12, day: 22, hour: 13), away: .cin, home: .mia),
        Match(DateComponents(year: 2019, month: 12, day: 22, hour: 13), away: .pit, home: .nyj),
        Match(DateComponents(year: 2019, month: 12, day: 22, hour: 13), away: .nyg, home: .was),
        Match(DateComponents(year: 2019, month: 12, day: 22, hour: 13), away: .car, home: .ind),
        Match(DateComponents(year: 2019, month: 12, day: 22, hour: 13), away: .bal, home: .cle),
        Match(DateComponents(year: 2019, month: 12, day: 22, hour: 13), away: .jax, home: .atl),
        
        Match(DateComponents(year: 2019, month: 12, day: 22, hour: 13), away: .no, home: .ten),
        Match(DateComponents(year: 2019, month: 12, day: 22, hour: 16, minute: 5), away: .oak, home: .lac),
        Match(DateComponents(year: 2019, month: 12, day: 22, hour: 16, minute: 5), away: .det, home: .den),
        Match(DateComponents(year: 2019, month: 12, day: 22, hour: 16, minute: 25), away: .ari, home: .sea),
        Match(DateComponents(year: 2019, month: 12, day: 22, hour: 16, minute: 25), away: .dal, home: .phi),
        Match(DateComponents(year: 2019, month: 12, day: 22, hour: 20, minute: 20), away: .kc, home: .chi),
      ]),
      Day(DateComponents(year: 2019, month: 12, day: 23), matches: [
        Match(DateComponents(year: 2019, month: 12, day: 23, hour: 20, minute: 15), away: .gb, home: .min),
      ])
    ]),

    // Week 17
    Week(days: [
      Day(DateComponents(year: 2019, month: 12, day: 29), matches: [
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 13), away: .pit, home: .bal),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 13), away: .nyj, home: .buf),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 13), away: .atl, home: .tb),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 13), away: .phi, home: .nyg),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 13), away: .no, home: .car),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 13), away: .cle, home: .cin),
        
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 13), away: .was, home: .dal),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 13), away: .gb, home: .det),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 13), away: .ten, home: .hou),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 13), away: .ind, home: .jax),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 13), away: .lac, home: .kc),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 13), away: .chi, home: .min),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 13), away: .mia, home: .ne),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 16, minute: 25), away: .oak, home: .den),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 16, minute: 25), away: .ari, home: .lar),
        Match(DateComponents(year: 2019, month: 12, day: 29, hour: 16, minute: 25), away: .sf, home: .sea),
      ]),
    ]),
  ])
}


