//
//  Match.swift
//  SchedulerPOC
//
//  Created by Don Clore on 11/20/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import UIKit

enum TeamAbbreviation: String, Codable, JSONCoding {
  typealias codableType = TeamAbbreviation
  
  case ari = "ARI"  // Cardinals
  case atl = "ATL"  // Falcons
  case bal = "BAL"  // Ravens
  case buf = "BUF"  // Bills
  case car = "CAR"  // Panthers
  case chi = "CHI"  // Bears
  case cin = "CIN"  // Bengals
  case cle = "CLE"  // Browns
  case dal = "DAL"  // Cowboys
  case den = "DEN"  // Broncos
  case det = "DET"  // Lions
  case gb = "GB"    // Packers
  case hou = "HOU"  // Texans
  case ind = "IND"  // Indianapolis Colts
  case jax = "JAX"  // Jacksonville Jaguars
  case kc = "KC"    // Kansas City Chiefs
  case lac = "LAC"  // Los Angeles Chargers
  case lar = "LAR"  // Los Angeles Rams
  case mia = "MIA"  // Miami Dolphins
  case min = "MIN"  // Minnesota Vikings
  case ne = "NE"    // New England Patriots
  case no = "NO"    // New Orleans Saints
  case nyg = "NYG"  // Giants
  case nyj = "NYJ"  // Jets
  case oak = "OAK"  // Raiders
  case phi = "PHI"  // Eagles
  case pit = "PIT"  // Steelers
  case sea = "SEA"  // Seahawks
  case sf = "SF"    // 49ers
  case tb = "TB"    // Tampa Bay Buccaneers
  case ten = "TEN"  // Tennessee Titans
  case was = "WAS"  // Washington Redskins
  
  var team: Team {
    switch self {
      
    case .ari:
      return Team(name: "Arizona Cardinals", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .atl:
      return Team(name: "Atlanta Falcons", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .bal:
      return Team(name: "Baltimore Ravens", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .buf:
      return Team(name: "Buffalo Bills", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .car:
      return Team(name: "Carolina Panthers", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .chi:
      return Team(name: "Chicago Bears", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .cin:
      return Team(name: "Cincinnati Bengals", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .cle:
      return Team(name: "Cleveland Bears", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .dal:
      return Team(name: "Dallas Cowboys", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .den:
      return Team(name: "Denver Broncos", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .det:
      return Team(name: "Detroit Lions", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .gb:
      return Team(name: "Green Bay Packers", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .hou:
      return Team(name: "Houston Texans", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .ind:
      return Team(name: "Indianapolis Colts", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .jax:
      return Team(name: "Jacksonville Jaguars", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .kc:
      return Team(name: "Kansas City Chiefs", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .lac:
      return Team(name: "Los Angeles Chargers", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .lar:
      return Team(name: "Los Angeles Rams", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .mia:
      return Team(name: "Miami Dolphins", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .min:
      return Team(name: "Minnesota Vikings", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .ne:
      return Team(name: "New England Patriots", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .no:
      return Team(name: "New Orleans Saints", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .nyg:
      return Team(name: "New York Giants", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .nyj:
      return Team(name: "New York Jets", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .oak:
      return Team(name: "Oakland Raiders", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .phi:
      return Team(name: "Philadelphia Eagles", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .pit:
      return Team(name: "Pittsburgh Steelers", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .sea:
      return Team(name: "Seattle Seahawks", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .sf:
      return Team(name: "San Francisco 49ers", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .tb:
      return Team(name: "Tampa Bay Buccaneers", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .ten:
      return Team(name: "Tennessee Titans", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    case .was:
      return Team(name: "Washington Redskins", abbrev: self.rawValue, image: UIImage(named: self.rawValue))
    }
  }
}

struct Team {
  let name: String
  let abbrev: String
  let image: UIImage?
}

struct Match {
  
  let away: TeamAbbreviation
  let home: TeamAbbreviation
  let date: Date
  
  init(_ components: DateComponents, away: TeamAbbreviation, home: TeamAbbreviation) {
    date = Calendar.current.date(from: components)!
    self.away = away
    self.home = home
  }
}
