//
//  Scheduler.swift
//  SchedulerPOC
//
//  Created by Don Clore on 11/20/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import UIKit

class Scheduler: UIViewController {
  let header = SchedulerHeader()
  let scheduleTable = WeekMatchSchedule(frame: .zero, style: .grouped)
  
  var currentWeekIndex: Int = 0
 
  override func viewDidLoad() {
    super.viewDidLoad()
    view.accessibilityIdentifier = "schedule"
    view.backgroundColor = .white
    header.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(header)
    header.backgroundColor = .white
    header.delegate = self
    header.dataSource = self
    
    scheduleTable.translatesAutoresizingMaskIntoConstraints = false
    scheduleTable.backgroundColor = .white
    scheduleTable.tabStopsProvider = self 

    view.addSubview(scheduleTable)
    
    NSLayoutConstraint.activate(constraints: [
      header.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      header.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      header.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      scheduleTable.topAnchor.constraint(equalTo: header.bottomAnchor),
      scheduleTable.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      scheduleTable.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      scheduleTable.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ], andSetPriority: UILayoutPriority(rawValue: 999))
    scheduleTable.scheduleDataProvider = self
    scheduleTable.reloadData()
  }
}

extension Scheduler: SchedulerHeaderDelegate {
  func indexSelected(week: Int) {
    currentWeekIndex = week
    scheduleTable.reloadData()
  }
}

extension Scheduler: WeekMatchScheduleDataProvider {
  func getCurrentWeek() -> (dropdownIndex: Int, week: ScheduleModel.Week) {
    let now = Date()
    let weeks = ScheduleModel.fauxSchedule.weeks
    guard let indexOfFirst = weeks.firstIndex(where: { week in
      guard let firstDay = week.days.first, let lastDay = week.days.last else {
        return false
      }
      return firstDay.date <= now && lastDay.date >= now
    }) else {
      // just return the last one if we're off the end.
      let index = weeks.count - 1
      let veryLastWeek = weeks[weeks.count - 1]
      return (dropdownIndex: index, week: veryLastWeek)
    }
    let firstWeek = weeks[indexOfFirst]
    return (dropdownIndex: indexOfFirst, week: firstWeek)
  }
  
  func getWeekForUserSelection() -> ScheduleModel.Week {
    let weeks = ScheduleModel.fauxSchedule.weeks
    guard weeks.count > currentWeekIndex else {
      return ScheduleModel.Week(days: [])
    }
    return ScheduleModel.fauxSchedule.weeks[currentWeekIndex]
  }
}

extension Scheduler: ScheduleTabStopsProvider {
  func getTabStops() -> (awayX: CGFloat, homeX: CGFloat, timeX: CGFloat, infoX: CGFloat) {
    let awayX = SchedulerHeader.Constants.borderPad
    let availableSpace = view.bounds.width - (SchedulerHeader.Constants.borderPad * 2)
    let usedSpace = MatchCell.stackWidth * 4
    let remainingSpace = availableSpace - usedSpace
    let spaceBetween = remainingSpace / 3.0
    
    let homeX = awayX + MatchCell.stackWidth + spaceBetween
    let timeX = homeX + MatchCell.stackWidth + spaceBetween
    let infoX = timeX + MatchCell.stackWidth + spaceBetween
    
    return (awayX: awayX, homeX: homeX, timeX: timeX, infoX: infoX)
  }
  
  
}
