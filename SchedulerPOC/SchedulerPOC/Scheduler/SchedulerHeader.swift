//
//  SchedulerHeader.swift
//  SchedulerPOC
//
//  Created by Don Clore on 11/20/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import UIKit

protocol SchedulerHeaderDelegate: class {
  func indexSelected(week: Int)
}

class SchedulerHeader: UIView {
  struct Constants {
    static let topPad: CGFloat = 40.0
    #if targetEnvironment(macCatalyst)
    static let borderPad: CGFloat = 22.0
    #else
    static let borderPad: CGFloat = 15.0
    #endif
    static let vertPad: CGFloat = 15.0
    static let dropdownHeight: CGFloat = 50.0
    
    #if targetEnvironment(macCatalyst)
    static let titleHeight: CGFloat = 62.0
    static let titleFontSize: CGFloat = 48.0
    #else
    static let titleHeight: CGFloat = 41.0
    static let titleFontSize: CGFloat = 32.0
    #endif
  }
  
  let weeks: [String] = [
    "WEEK 1",
    "WEEK 2",
    "WEEK 3",
    "WEEK 4",
    "WEEK 5",
    "WEEK 6",
    "WEEK 7",
    "WEEK 8",
    "WEEK 9",
    "WEEK 10",
    "WEEK 11",
    "WEEK 12",
    "WEEK 13",
    "WEEK 14",
    "WEEK 15",
    "WEEK 16",
    "WEEK 17",
  ]
  
  let titleLabel: UILabel = UILabel()
  let dropdown: BBPDropdown = BBPDropdown()
  var dropdownHeightConstraint: NSLayoutConstraint!
  weak var delegate: SchedulerHeaderDelegate?
  weak var dataSource: WeekMatchScheduleDataProvider? {
    didSet {
      if let dataSource = dataSource {
        let tuple = dataSource.getCurrentWeek()
        let index = tuple.dropdownIndex
        dropdown.selections = [weeks[index]]
        dropdown.select(index: index)
      } else {
        dropdown.selections = [weeks[0]]
        dropdown.select(index: 0)
      }
    }
  }
  
  init() {
    super.init(frame: CGRect.zero)
    accessibilityIdentifier = "scheduleHeader"
    backgroundColor = .white
    titleLabel.text = "NFL Schedule"
    titleLabel.font = UIFont.init(name: "Nunito-Light", size: Constants.titleFontSize)
    titleLabel.textColor = #colorLiteral(red: 0.1450980392, green: 0.1450980392, blue: 0.1450980392, alpha: 1)
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    addSubview(titleLabel)
    dropdown.translatesAutoresizingMaskIntoConstraints = false
    addSubview(dropdown)
    
    dropdown.data = weeks
    dropdown.isMultiple = false
    dropdown.delegate = self    
    dropdownHeightConstraint = dropdown.heightAnchor.constraint(equalToConstant: Constants.dropdownHeight)
    
    NSLayoutConstraint.activate([
      titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: Constants.topPad),
      titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: Constants.borderPad),
      titleLabel.heightAnchor.constraint(equalToConstant: Constants.titleHeight),
      dropdown.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Constants.vertPad),
      dropdown.leftAnchor.constraint(equalTo: titleLabel.leftAnchor),
      dropdown.rightAnchor.constraint(equalTo: rightAnchor, constant: -Constants.borderPad),
      dropdownHeightConstraint,
      bottomAnchor.constraint(equalTo: dropdown.bottomAnchor, constant: Constants.vertPad)
    ])
  }
    
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }   
}

extension SchedulerHeader: BBPDropDownDelegate {
  func requestNewHeight(_ dropDown: BBPDropdown, newHeight: CGFloat) {
    UIView.animate(withDuration: 0.6, delay:0.2, options:UIView.AnimationOptions(), animations: {
      self.dropdownHeightConstraint.constant = newHeight
    }, completion: nil)
  }
  
  func dropDownView(_ dropDown: BBPDropdown, didSelectedItem item: String) {
    guard let index = weeks.firstIndex(of: item), let delegate = delegate else {
      return
    }
    delegate.indexSelected(week: index)
  }
  
  func dropDownView(_ dropDown: BBPDropdown, dataList: [String]) {
    
  }
  
  func dropDownWillAppear(_ dropDown: BBPDropdown) {
  }
}
