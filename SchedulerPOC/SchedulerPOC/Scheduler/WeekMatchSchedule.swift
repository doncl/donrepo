//
//  WeekMatchSchedule.swift
//  SchedulerPOC
//
//  Created by Don Clore on 11/20/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import UIKit

protocol WeekMatchScheduleDataProvider: class {
  func getWeekForUserSelection() -> ScheduleModel.Week
  func getCurrentWeek() -> (dropdownIndex: Int, week: ScheduleModel.Week)
}

protocol ScheduleTabStopsProvider: class {
  func getTabStops() -> (awayX: CGFloat, homeX: CGFloat, timeX: CGFloat, infoX: CGFloat)
}

class WeekMatchSchedule: UITableView {
  override init(frame: CGRect, style: UITableView.Style) {
    super.init(frame: frame, style: .grouped)
    register(MatchCell.self, forCellReuseIdentifier: MatchCell.id)
    rowHeight = MatchCell.height
    separatorStyle = .none
    backgroundColor = .white
    delegate = self
    dataSource = self
    allowsSelection = false 
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  weak var scheduleDataProvider: WeekMatchScheduleDataProvider?
  weak var tabStopsProvider: ScheduleTabStopsProvider?  
}

extension WeekMatchSchedule: UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    guard let dataProvider = scheduleDataProvider else {
      return 0
    }
    return dataProvider.getWeekForUserSelection().days.count
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let dataProvider = scheduleDataProvider else {
      return 0
    }
    let week = dataProvider.getWeekForUserSelection()
    guard week.days.count > section else {
      return 0
    }
    let day = week.days[section]
    return day.matches.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: MatchCell.id, for: indexPath) as? MatchCell else {
      fatalError("should not happen")
    }
    cell.tabStopsProvider = self.tabStopsProvider
    guard let dataProvider = scheduleDataProvider else { return cell }
    let week = dataProvider.getWeekForUserSelection()
    guard week.days.count > indexPath.section else { return cell }
    let day = week.days[indexPath.section]
    guard day.matches.count > indexPath.item else { return cell }
    let match = day.matches[indexPath.item]
    
    cell.match = match
    
    if let tabStopsProvider = tabStopsProvider {
      let tabs = tabStopsProvider.getTabStops()
      cell.awayX.constant = tabs.awayX
      cell.homeX.constant = tabs.homeX
      cell.timeX.constant = tabs.timeX
      // nothing for infoX yet.
    }
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    #if targetEnvironment(macCatalyst)
    return 150.0
    #else
    return 100.0
    #endif
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    #if targetEnvironment(macCatalyst)
    return 60.0
    #else
    return 40.0
    #endif
  }  
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return DayFooter()
  }

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    guard let day = getDay(for: section) else {
      return nil
    }
    let header = DayHeader(day: day)
    header.tabStopsProvider = self.tabStopsProvider
    return header
  }
  
  private func getDay(for section: Int) -> ScheduleModel.Day? {
    guard let dataProvider = scheduleDataProvider else { return nil }
    let week = dataProvider.getWeekForUserSelection()
    guard week.days.count > section else {
      return nil
    }
    let day = week.days[section]
    return day
  }
  
}

extension WeekMatchSchedule: UITableViewDelegate {
  
}

extension WeekMatchSchedule {

}
