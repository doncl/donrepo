//
//  ViewController.swift
//  SchedulerPOC
//
//  Created by Don Clore on 11/20/19.
//  Copyright © 2019 BeerBarrelPoker Studios. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  let scheduler: Scheduler
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    scheduler = Scheduler()
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  required init?(coder: NSCoder) {
    scheduler = Scheduler()
    super.init(coder: coder)
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    
    addChild(scheduler)
    view.addSubview(scheduler.view)
    scheduler.didMove(toParent: self)
    
    scheduler.view.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      scheduler.view.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      scheduler.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      scheduler.view.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      scheduler.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
  }
}

